# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/khatran/Library/Android/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-dontwarn javax.management.**
-dontwarn java.beans.**
-dontwarn java.lang.management.**
-dontwarn org.xmlpull.v1.**
-dontwarn android.util.Xml
-dontwarn com.actionbarsherlock.**
-ignorewarnings

-keep public class mconnectv3.common.** {
  public protected *;
  public void set*(***);
  public *** get*();
}

-keepclassmembers class mconnectv3.common.** { *; }

