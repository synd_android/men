package com.fcscs.android.mconnectv3.http;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.conn.ssl.AllowAllHostnameVerifier;
import org.ksoap2.HeaderProperty;
import org.ksoap2.transport.ServiceConnection;

import android.util.Log;

public class FcsServiceConnectionSE implements ServiceConnection {

    private static final String TAG = FcsServiceConnectionSE.class.getSimpleName();

    private HttpURLConnection connection;

    public static TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
            return new java.security.cert.X509Certificate[0];
        }

        public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
        }

        public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
        }
    }};

    public FcsServiceConnectionSE(String url, int connectTimeout, int readTimeout) throws IOException {
        if (url.startsWith("https:")) {
            try {
                SSLContext sc = SSLContext.getInstance("TLS");
                sc.init(null, trustAllCerts, new java.security.SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            } catch (Exception e) {
                e.getMessage();
            }
            connection = (HttpsURLConnection) new URL(url).openConnection();
            ((HttpsURLConnection) connection).setHostnameVerifier(new AllowAllHostnameVerifier());
        } else {
            connection = (HttpURLConnection) (new URL(url)).openConnection();
        }

        connection.setUseCaches(false);
        connection.setDoOutput(true);
        connection.setDoInput(true);
        connection.setConnectTimeout(connectTimeout);
        connection.setReadTimeout(readTimeout);
        Log.d(TAG, "connect timeout=" + connectTimeout);
        Log.d(TAG, "read timeout=" + readTimeout);
    }

    public void connect() throws IOException {
        connection.connect();
    }

    public void disconnect() {
        connection.disconnect();
    }

    public List<HeaderProperty> getResponseProperties() {
        Map<String, List<String>> properties = connection.getHeaderFields();
        Set<String> keys = properties.keySet();
        List<HeaderProperty> retList = new LinkedList<HeaderProperty>();

        for (Iterator<String> i = keys.iterator(); i.hasNext(); ) {
            String key = (String) i.next();
            List<String> values = (List<String>) properties.get(key);

            int j = 0;
            while (j < values.size()) {
                retList.add(new HeaderProperty(key, (String) values.get(j)));
                j++;
            }
        }
        return retList;
    }

    public void setRequestProperty(String string, String soapAction) {
        connection.setRequestProperty(string, soapAction);
    }

    public void setRequestMethod(String requestMethod) throws IOException {
        connection.setRequestMethod(requestMethod);
    }

    public OutputStream openOutputStream() throws IOException {
        return connection.getOutputStream();
    }

    public InputStream openInputStream() throws IOException {
        return connection.getInputStream();
    }

    public InputStream getErrorStream() {
        return connection.getErrorStream();
    }

    public String getHost() {
        return connection.getURL().getHost();
    }

    public int getPort() {
        return connection.getURL().getPort();
    }

    public String getPath() {
        return connection.getURL().getPath();
    }

    public int getResponseCode() throws IOException {
        return connection.getResponseCode();
    }

    public String getResponseMessage() throws IOException {
        return connection.getResponseMessage();
    }

    @Override
    public void setFixedLengthStreamingMode(int arg0) {

    }
}
