package com.fcscs.android.mconnectv3.common;

import java.lang.Thread.UncaughtExceptionHandler;

import android.content.Context;

public class McExceptionHandler extends McExceptionHandlerBase implements UncaughtExceptionHandler {

    private UncaughtExceptionHandler defaultHandler;

    public McExceptionHandler(Context ctx) {
        super(ctx);
        if (defaultHandler == null) {
            defaultHandler = Thread.getDefaultUncaughtExceptionHandler();
        }
    }

    public void uncaughtException(Thread t, Throwable e) {

        if (McConstants.SAVE_CRASH_REPORT) {
            saveThrowableToSDCard(e);
        }

        defaultHandler.uncaughtException(t, e);

    }

}
