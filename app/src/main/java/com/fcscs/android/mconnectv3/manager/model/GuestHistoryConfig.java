package com.fcscs.android.mconnectv3.manager.model;

import java.io.Serializable;

public class GuestHistoryConfig implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5357581469963931502L;

    private boolean day1 = false;
    private boolean week1 = false;
    private boolean month1 = false;
    private boolean month3 = false;
    private boolean month6 = false;
    private boolean year1 = false;

    public boolean isDay1() {
        return day1;
    }

    public void setDay1(boolean day1) {
        this.day1 = day1;
    }

    public boolean isWeek1() {
        return week1;
    }

    public void setWeek1(boolean week1) {
        this.week1 = week1;
    }

    public boolean isMonth1() {
        return month1;
    }

    public void setMonth1(boolean month1) {
        this.month1 = month1;
    }

    public boolean isMonth3() {
        return month3;
    }

    public void setMonth3(boolean month3) {
        this.month3 = month3;
    }

    public boolean isMonth6() {
        return month6;
    }

    public void setMonth6(boolean month6) {
        this.month6 = month6;
    }

    public boolean isYear1() {
        return year1;
    }

    public void setYear1(boolean year1) {
        this.year1 = year1;
    }

}
