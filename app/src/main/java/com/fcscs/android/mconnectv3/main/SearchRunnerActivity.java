package com.fcscs.android.mconnectv3.main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils.TruncateAt;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.BaseActivity;
import com.fcscs.android.mconnectv3.common.ui.HomeTopBar;
import com.fcscs.android.mconnectv3.common.ui.McBackgroundAsyncTask;
import com.fcscs.android.mconnectv3.common.ui.McProgressAsyncTask;
import com.fcscs.android.mconnectv3.common.ui.RoundCornerListAdapter;
import com.fcscs.android.mconnectv3.dao.entity.Department;
import com.fcscs.android.mconnectv3.dao.entity.Runner;
import com.fcscs.android.mconnectv3.ws.cconnect.AdhocService;
import com.fcscs.android.mconnectv3.ws.cconnect.CoreService;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.FindRunnerResponse;
import com.fcscs.android.mconnectv3.ws.econnect.ECService;

public class SearchRunnerActivity extends BaseActivity {

    public static final String KEY_RUNNER = "KEY_RUNNER";
    public static final String KEY_RESULT = "KEY_RESULT";
    public static final String KEY_TERM = "KEY_TERM";
    public static final String KEY_DEP_ID = "KEY_DEP_ID";

    private HomeTopBar homeTopBar;

    private List<Department> currDepartments;
    private Spinner departmentSp;
    protected Department currDepartment;
    private TextView tvAdd;
    private EditText etTerm;

    private ListView lvMain;
    private List<Runner> runnerList;
    private RunnerListAdapter runnerAdapter;
    private FindRunnerResponse response;
    private String term;
    private boolean firstLoading;
    private DepartmentLoader task;
    private boolean isSingleChoice;
    protected RunnerLoader task1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_runner);
        firstLoading = true;

        isSingleChoice = getIntent().getBooleanExtra("isSingleChoice", false);

        departmentSp = (Spinner) findViewById(R.id.sp_department);

        if (task == null || AsyncTask.Status.RUNNING.equals(task.getStatus()) == false) {
            task = new DepartmentLoader(getCurrentContext());
            task.exec();
        }

        etTerm = (EditText) findViewById(R.id.et_term);
        etTerm.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:

                            String term = etTerm.getText().toString().trim();
                            Long deptId = currDepartment == null ? null : currDepartment.getDepartmentId();

                            if (task1 == null || AsyncTask.Status.RUNNING.equals(task1.getStatus()) == false) {
                                task1 = new RunnerLoader(getCurrentContext(), term, deptId);
                                task1.exec();
                            }

                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        });

        runnerList = new ArrayList<Runner>();
        runnerAdapter = new RunnerListAdapter(this, runnerList, R.layout.runner_search_list_item);
        Bundle bundle = getIntent().getExtras();
        term = bundle.getString(KEY_TERM);
        response = (FindRunnerResponse) bundle.getSerializable(KEY_RUNNER);
        runnerList.clear();
        runnerAdapter.notifyDataSetChanged();
        addRunners(response);

        lvMain = (ListView) findViewById(R.id.lv_runners);
        lvMain.setAdapter(runnerAdapter);
        lvMain.setEmptyView(findViewById(R.id.tvrs_empty));


        if (term != null) {
            etTerm.setText(term);
        }

        homeTopBar = (HomeTopBar) findViewById(R.id.top_bar);
        homeTopBar.getTitleTv().setText(getString(R.string.runner_search));

        tvAdd = (TextView) findViewById(R.id.btn_add_msg);
        tvAdd.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                setResult(RESULT_OK, createResultIntent(new ArrayList<Runner>(runnerAdapter.getRunners().values())));
                finish();
            }
        });

        firstLoading = false;

        if (isSingleChoice) {
            findViewById(R.id.ll_bottom).setVisibility(View.GONE);
        } else {
            findViewById(R.id.ll_bottom).setVisibility(View.VISIBLE);
        }

    }

    public static Intent createResultIntent(ArrayList<Runner> runners) {
        Intent i = new Intent();
        i.putExtra(KEY_RESULT, runners);
        return i;
    }

    public class RunnerListAdapter extends RoundCornerListAdapter<Runner> {

        private Map<Long, Runner> runners = new HashMap<Long, Runner>();

        public RunnerListAdapter(Context context, List<Runner> list, int layout) {
            super(context, list, layout);
        }

        public Map<Long, Runner> getRunners() {
            return runners;
        }

        @Override
        public void onBindView(View view, Context context, int position, final Runner item) {
            final RunnerTag tag = (RunnerTag) view.getTag();
            tag.tv.setText(item.getFullname());
            tag.tv.setEllipsize(TruncateAt.MIDDLE);
            tag.tv.setMaxEms(15);
            final OnClickListener l = new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (!isSingleChoice) {
                        selectRunner(item);
                    }
                }
            };
            tag.tv.setOnClickListener(l);

            view.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (isSingleChoice) {
                        selectRunner(item);
                    }
                }
            });

            final OnCheckedChangeListener listener = new OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        runners.put(item.getRunnerId(), item);
                    } else {
                        runners.remove(item.getRunnerId());
                    }
                }
            };
            tag.cb.setOnCheckedChangeListener(listener);

            // fix checkbox weird issue
            tag.cb.setChecked(runners.containsKey(item.getRunnerId()));

            if (isSingleChoice) {
                tag.cb.setVisibility(View.INVISIBLE);
                tag.arrow.setVisibility(View.VISIBLE);
            } else {
                tag.cb.setVisibility(View.VISIBLE);
                tag.arrow.setVisibility(View.GONE);
            }

        }

        @Override
        public void beforeBindView(View v, Context context, int position, Runner item) {
            RunnerTag tag = new RunnerTag();
            tag.tv = (TextView) v.findViewById(R.id.tv_runner);
            tag.cb = (CheckBox) v.findViewById(R.id.cb_runner);
            tag.arrow = (ImageView) v.findViewById(R.id.goto_image);
            v.setTag(tag);
        }

        private void selectRunner(final Runner item) {
            ArrayList<Runner> list = new ArrayList<Runner>();
            list.add(item);
            setResult(RESULT_OK, createResultIntent(list));
            finish();
        }

        class RunnerTag {
            CheckBox cb;
            TextView tv;
            ImageView arrow;
        }

    }

    public void addRunners(FindRunnerResponse response) {
        if (response != null) {
            if (response.getRunners().size() != 0) {
                for (final Runner r : response.getRunners()) {
                    runnerList.add(r);
                }
            }
            runnerAdapter.notifyDataSetChanged();
        }
        hideSoftInput(etTerm);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            setResult(RESULT_CANCELED);
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    class DepartmentLoader extends McBackgroundAsyncTask {

        private List<Department> resp;

        public DepartmentLoader(Context context) {
            super(context);
        }

        @Override
        public void doInBackground() {
            currDepartments = null;

            if (isCConnect()) {
                resp = CoreService.getInstance().getDepartments(getCurrentContext());
            } else {
                resp = ECService.getInstance().getDepartmentList(getCurrentContext());
            }
        }

        @Override
        public void onPostExecute() {
            if (resp != null) {
                currDepartments = resp;

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getCurrentContext(),
                        android.R.layout.simple_spinner_item);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                adapter.add(getString(R.string.all));
                for (Department dept : currDepartments) {
                    adapter.add(dept.getDepartmentName());
                }
                departmentSp.setAdapter(adapter);
                departmentSp.setOnItemSelectedListener(new OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                        int index = pos - 1;
                        if (currDepartments != null && index > -1 && index < currDepartments.size()) {
                            currDepartment = currDepartments.get(index);
                        } else {
                            currDepartment = null;
                        }

                        Long deptId = null;
                        if (currDepartment != null) {
                            deptId = currDepartment.getDepartmentId();
                        }
                        if (!firstLoading) {
                            if (task1 == null || AsyncTask.Status.RUNNING.equals(task1.getStatus()) == false) {
                                task1 = new RunnerLoader(getCurrentContext(), term, deptId);
                                task1.exec();
                            }
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {

                    }
                });

            }
        }

    }

    class RunnerLoader extends McProgressAsyncTask {

        public RunnerLoader(Context context, String term, Long deptId) {
            super(context);
            this.term = term;
            this.deptId = deptId;
        }

        private String term;
        private Long deptId;
        private FindRunnerResponse res;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            runnerList.clear();
            runnerAdapter.getRunners().clear();
            runnerAdapter.notifyDataSetChanged();
        }

        @Override
        public void doInBackground() {
            if (isCConnect()) {
                res = AdhocService.getInstanceNonUI().findRunners(SearchRunnerActivity.this, term, deptId, 0, 1000);
            } else {
                res = ECService.getInstance().getAdHocUserList(SearchRunnerActivity.this, term, deptId, null);
            }
        }

        @Override
        public void onPostExecute() {
            if (res != null) {
                addRunners(res);
            }
        }

    }

    @Override
    public boolean isLoginActivity() {
        return true;
    }

    @Override
    public boolean isHomeActivity() {
        return false;
    }

}
