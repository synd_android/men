package com.fcscs.android.mconnectv3.common.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fcscs.android.mconnectv3.R;

public class TopBar extends LinearLayout {

    protected Button leftBtn;
    private TextView titleTv;
    private Button rightBtn;
    private Context context;
    protected Button btnRightInsp;

    public TopBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.common_top_bar_layout, this);
        if (!isInEditMode()) {
            leftBtn = (Button) findViewById(R.id.btn_top_home);
            leftBtn.setVisibility(View.VISIBLE);
            titleTv = (TextView) findViewById(R.id.tv_top_title);
            rightBtn = (Button) findViewById(R.id.btn_top_menu);
            btnRightInsp = (Button) findViewById(R.id.btn_top_menu_insp);
            rightBtn.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View view) {
                    onClickRightBtn(view);
                }
            });
        }
    }

    public void onClickRightBtn(View view) {
    }

    public void onClickLeftBtn(View view) {
    }

    public Button getLeftBtn() {
        return leftBtn;
    }

    public TextView getTitleTv() {
        return titleTv;
    }

    public void showBtnNotInsp(boolean isShow) {
        if (isShow) {
            btnRightInsp.setVisibility(VISIBLE);
            rightBtn.setVisibility(GONE);
        } else {
            btnRightInsp.setVisibility(GONE);
            rightBtn.setVisibility(VISIBLE);
        }
    }

    public Button getbtnRightInsp() {
        return btnRightInsp;
    }

    public Button getRightBtn() {
        return rightBtn;
    }

}
