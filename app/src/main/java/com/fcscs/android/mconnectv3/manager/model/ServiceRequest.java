package com.fcscs.android.mconnectv3.manager.model;

import com.fcscs.android.mconnectv3.common.util.McUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by FCS on 2014/6/19.
 */
public class ServiceRequest implements Serializable {

    private static ServiceRequest instance = null;

    // guest request
    private String roomNo;
    private String guestName;
    private Date scheduledDate;

    // other request
    private String locationCode;
    private String requestor;

    private ServiceRequestItem item = null;
    private ArrayList<ServiceRequestItem> items = new ArrayList<ServiceRequestItem>();

    private ServiceRequest() {

    }

    public synchronized static ServiceRequest getInstance() {
        if (instance == null) {
            instance = new ServiceRequest();
        }
        return instance;
    }

    public boolean isItemSelected() {
        if (item == null || McUtils.isNullOrEmpty(item.getItemId())) {
            return false;
        }
        return true;
    }

    public boolean isLocationSelected() {
        if (McUtils.isNullOrEmpty(locationCode)) {
            return true;
        }
        return false;
    }

    public static boolean isAvailable() {
        if (instance == null) {
            return false;
        }
        return true;
    }

    public static void destroyInstance() {
        if (instance == null || instance.getItems() == null) {
            return;
        }
        for (ServiceRequestItem item : instance.getItems()) {
            if (item.getCapturedImage() != null) {
                item.getCapturedImage().delete();
            }
            if (item.getRecordedVoice() != null) {
                item.getRecordedVoice().delete();
            }
        }
        instance = null;
    }

    public String getRoomNo() {
        return roomNo;
    }

    public void setRoomNo(String roomNo) {
        this.roomNo = roomNo;
    }

    public Date getScheduledDate() {
        return scheduledDate;
    }

    public void setScheduledDate(Date scheduledDate) {
        this.scheduledDate = scheduledDate;
    }

    public String getGuestName() {
        return guestName;
    }

    public void setGuestName(String guestName) {
        this.guestName = guestName;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public String getRequestor() {
        return requestor;
    }

    public void setRequestor(String requestor) {
        this.requestor = requestor;
    }

    public ArrayList<ServiceRequestItem> getItems() {
        return items;
    }

    public void removeItemAtIndex(int index) {
        if (items.size() > index) items.remove(index);
    }

    public void setItems(ArrayList<ServiceRequestItem> items) {
        this.items = items;
    }

    public ServiceRequestItem getItem() {
        return item;
    }

    public void setItem(ServiceRequestItem item) {
        this.item = item;
    }
}
