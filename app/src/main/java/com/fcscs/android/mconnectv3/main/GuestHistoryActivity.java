package com.fcscs.android.mconnectv3.main;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.BaseActivity;
import com.fcscs.android.mconnectv3.common.McConstants;
import com.fcscs.android.mconnectv3.common.ui.HomeTopBar;
import com.fcscs.android.mconnectv3.common.ui.McProgressAsyncTask;
import com.fcscs.android.mconnectv3.common.util.PrefsUtil;
import com.fcscs.android.mconnectv3.main.JobSummaryActivity.JobSummaryRoundAdapter;
import com.fcscs.android.mconnectv3.manager.model.GuestHistoryConfig;
import com.fcscs.android.mconnectv3.manager.model.GuestInfo;
import com.fcscs.android.mconnectv3.manager.model.JobModel;
import com.fcscs.android.mconnectv3.ws.econnect.ECService;

public class GuestHistoryActivity extends BaseActivity implements OnClickListener {

    private HomeTopBar homeTopBar;
    private TextView tvName;
    private TextView tvRoom;
    private GuestInfo guest;
    private TextView tvEmpty;
    private ListView lvList;
    private TextView tv1day;
    private TextView tv1week;
    private TextView tv1month;
    private TextView tv3month;
    private TextView tv6month;
    private TextView tv1year;
    private JobSummaryRoundAdapter adp;
    private GuestHistoryConfig config;
    private boolean initial;
    public ArrayList<JobModel> models;
    private GetJobsTask task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        guest = (GuestInfo) getIntent().getSerializableExtra(McConstants.KEY_GUEST_INFO);
        if (guest == null) {
            finish();
            return;
        }

        setContentView(R.layout.guest_history);
        models = new ArrayList<JobModel>();

        homeTopBar = (HomeTopBar) findViewById(R.id.top_bar);
        homeTopBar.getTitleTv().setText(R.string.guest_history);

        tvName = (TextView) findViewById(R.id.tv_name);
        tvRoom = (TextView) findViewById(R.id.tv_room);

        String name = getString(R.string.name_) + guest.getGuestUsername();
        tvName.setText(name);

        String vip = guest.getVipMsg() == null ? "" : getString(R.string.vip) + guest.getVipMsg();
        String room = getString(R.string.room) + guest.getRoomNum() + "\t\t" + vip;
        tvRoom.setText(room);

        tvEmpty = (TextView) findViewById(R.id.tv_empty);
        tvEmpty.setText(R.string.no_jobs);

        lvList = (ListView) findViewById(R.id.lv_list);
        lvList.setEmptyView(tvEmpty);

        tv1day = (TextView) findViewById(R.id.btn_d);
        tv1week = (TextView) findViewById(R.id.btn_w);
        tv1month = (TextView) findViewById(R.id.btn_m);
        tv3month = (TextView) findViewById(R.id.btn_3m);
        tv6month = (TextView) findViewById(R.id.btn_6m);
        tv1year = (TextView) findViewById(R.id.btn_y);

        initial = false;
        config = PrefsUtil.getGuestHistoryConfig(this);
        if (!config.isDay1()) {
            tv1day.setEnabled(false);
        } else {
            getJobs("1");
            initial = true;
        }

        if (!config.isWeek1()) {
            tv1week.setEnabled(false);
        } else if (!initial) {
            getJobs("2");
            initial = true;
        }

        if (!config.isMonth1()) {
            tv1month.setEnabled(false);
        } else if (!initial) {
            getJobs("3");
            initial = true;
        }

        if (!config.isMonth3()) {
            tv3month.setEnabled(false);
        } else if (!initial) {
            getJobs("4");
            initial = true;
        }

        if (!config.isMonth6()) {
            tv6month.setEnabled(false);
        } else if (!initial) {
            getJobs("5");
            initial = true;
        }

        if (!config.isYear1()) {
            tv1year.setEnabled(false);
        } else if (!initial) {
            getJobs("6");
            initial = true;
        }

        tv1day.setOnClickListener(this);
        tv1week.setOnClickListener(this);
        tv1month.setOnClickListener(this);
        tv3month.setOnClickListener(this);
        tv6month.setOnClickListener(this);
        tv1year.setOnClickListener(this);

    }

    private void getJobs(String type) {
        if (task == null || AsyncTask.Status.RUNNING.equals(task.getStatus()) == false) {
            task = new GetJobsTask(this, type);
            task.exec();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_d:
                getJobs("1");
                break;
            case R.id.btn_w:
                getJobs("2");
                break;
            case R.id.btn_m:
                getJobs("3");
                break;
            case R.id.btn_3m:
                getJobs("4");
                break;
            case R.id.btn_6m:
                getJobs("5");
                break;
            case R.id.btn_y:
                getJobs("6");
                break;
        }
    }

    @Override
    public boolean isLoginActivity() {
        return false;
    }

    @Override
    public boolean isHomeActivity() {
        return true;
    }

    private class GetJobsTask extends McProgressAsyncTask {

        private String type;
        private List<JobModel> list;

        public GetJobsTask(Context context, String type) {
            super(context);
            this.type = type;
        }

        @Override
        public void doInBackground() {
            list = ECService.getInstance().getGuestHistory(getCurrentContext(), guest.getProfileID(), type);
        }

        @Override
        public void onPostExecute() {

            if (list == null) {
                list = new ArrayList<JobModel>();
            }

            models.clear();
            models.addAll(list);

            adp = new JobSummaryRoundAdapter(GuestHistoryActivity.this, models, false, false);
            lvList.setAdapter(adp);
            setSelection(type);
        }

    }

    private void setSelection(String type) {

        if (config.isDay1()) {
            tv1day.setSelected(false);
        }
        if (config.isWeek1()) {
            tv1week.setSelected(false);
        }
        if (config.isMonth1()) {
            tv1month.setSelected(false);
        }
        if (config.isMonth3()) {
            tv3month.setSelected(false);
        }
        if (config.isMonth6()) {
            tv6month.setSelected(false);
        }
        if (config.isYear1()) {
            tv1year.setSelected(false);
        }

        if ("1".equals(type)) {
            tv1day.setSelected(true);
        }
        if ("2".equals(type)) {
            tv1week.setSelected(true);
        }
        if ("3".equals(type)) {
            tv1month.setSelected(true);
        }
        if ("4".equals(type)) {
            tv3month.setSelected(true);
        }
        if ("5".equals(type)) {
            tv6month.setSelected(true);
        }
        if ("6".equals(type)) {
            tv1year.setSelected(true);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == JobSummaryRoundAdapter.RC_DETAIL) {
            JobModel job = (JobModel) data.getSerializableExtra("job");
            int i = -1;
            for (JobModel j : models) {
                i++;
                if (j.getJobId() == job.getJobId()) {
                    models.remove(i);
                    models.add(i, job);
                    break;
                }
            }
            adp.notifyDataSetChanged();
        }
    }

}
