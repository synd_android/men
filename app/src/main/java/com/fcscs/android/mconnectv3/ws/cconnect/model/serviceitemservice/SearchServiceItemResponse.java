package com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.ksoap2.serialization.SoapObject;


public class SearchServiceItemResponse implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7346682385149288200L;

    private int total;
    private List<ServiceItemViewModel> serviceItems = null;

    public SearchServiceItemResponse() {
        serviceItems = new ArrayList<ServiceItemViewModel>();
        total = 0;
    }

    public SearchServiceItemResponse(SoapObject soap) {
        serviceItems = new ArrayList<ServiceItemViewModel>();
        this.total = Integer.valueOf(soap.getAttribute("total").toString());
        if (soap.getPropertyCount() > 0) {
            for (int i = 0; i < soap.getPropertyCount(); i++) {
                serviceItems.add(new ServiceItemViewModel((SoapObject) soap.getProperty(i)));
            }
        }
    }

    public int getTotal() {
        return total;
    }

    public List<ServiceItemViewModel> getServiceItems() {
        return serviceItems;
    }

    public void setServiceItems(List<ServiceItemViewModel> serviceItems) {
        this.serviceItems = serviceItems;
    }

    public void setTotal(int total) {
        this.total = total;
    }

}
