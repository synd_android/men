package com.fcscs.android.mconnectv3.ws.econnect.model.commonservice;

import java.io.Serializable;

import org.ksoap2.serialization.SoapObject;

import com.fcscs.android.mconnectv3.common.util.SoapHelper;

public class GetCheckLoginResponse implements Serializable {

    /**
     *
     */
//    private static final long serialVersionUID = 2846090543654867439L;

    private int returnCode;
    private String errorMsg;


    public GetCheckLoginResponse() {

    }

    public GetCheckLoginResponse(SoapObject soap) {
        SoapObject status = (SoapObject) soap.getProperty("ResponseStatus");
        returnCode = SoapHelper.getIntegerProperty(status, "ReturnCode", 1);
        errorMsg = SoapHelper.getStringProperty(status, "ErrorMsg", "");
    }

    public int getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(int returnCode) {
        this.returnCode = returnCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
