package com.fcscs.android.mconnectv3.common;

import java.sql.SQLException;
import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.manager.model.LanguageDetail;
import com.fcscs.android.mconnectv3.manager.model.LanguageResource;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.ServiceItemViewModel;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
    private static final String TAG = DatabaseHelper.class.getName();
    private static DatabaseHelper databaseHelperInstance = null;

    public static DatabaseHelper getInstance(Context mContext) {
        if (databaseHelperInstance == null) {
            Context context = null;
            if (mContext.getApplicationContext() != null) {
                context = mContext.getApplicationContext();
            } else {
                context = mContext;
            }
            databaseHelperInstance = new DatabaseHelper(context);
        }
        return databaseHelperInstance;
    }

    ;

    private static final String DATABASE_NAME = "econnect.db";
    private static int DATABASE_VERSION = 1;

    private RuntimeExceptionDao<ServiceItemViewModel, Long> serviceItemDao;
    private RuntimeExceptionDao<LanguageDetail, String> languageModelDao;
    private RuntimeExceptionDao<LanguageResource, String> languageResourceDao;

    private DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            Log.i(TAG, "onCreate");
            TableUtils.createTable(connectionSource, ServiceItemViewModel.class);
            TableUtils.createTable(connectionSource, LanguageDetail.class);
            TableUtils.createTable(connectionSource, LanguageResource.class);
        } catch (SQLException e) {
            Log.e(TAG, "Can't create database", e);
            throw new RuntimeException(e);
        }

    }

    public void clear() {
        try {
            Log.i(TAG, "clear()");
            TableUtils.clearTable(getConnectionSource(), ServiceItemViewModel.class);
            TableUtils.clearTable(getConnectionSource(), LanguageDetail.class);
            TableUtils.clearTable(getConnectionSource(), LanguageResource.class);
        } catch (SQLException e) {
            Log.e(TAG, "Can't clear database", e);
            throw new RuntimeException(e);
        }
    }

    public void clear(Class<?> model) {
        try {
            TableUtils.clearTable(getConnectionSource(), model);
        } catch (SQLException e) {
            Log.e(TAG, "clear database error", e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            Log.i(TAG, "onUpgrade");
            TableUtils.dropTable(connectionSource, ServiceItemViewModel.class, true);
            TableUtils.dropTable(connectionSource, LanguageResource.class, true);
            TableUtils.dropTable(connectionSource, LanguageDetail.class, true);
            onCreate(db, connectionSource);
        } catch (SQLException e) {
            Log.e(TAG, "Can't drop databases", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() {
        super.close();
        serviceItemDao = null;
    }

    public RuntimeExceptionDao<ServiceItemViewModel, Long> getServiceItemDao() {
        if (serviceItemDao == null) {
            serviceItemDao = getRuntimeExceptionDao(ServiceItemViewModel.class);
        }
        return serviceItemDao;
    }

    public RuntimeExceptionDao<LanguageDetail, String> getLanguageModelDao() {
        if (languageModelDao == null) {
            languageModelDao = getRuntimeExceptionDao(LanguageDetail.class);
        }
        return languageModelDao;
    }

    public RuntimeExceptionDao<LanguageResource, String> getLanguageResourceDao() {
        if (languageResourceDao == null) {
            languageResourceDao = getRuntimeExceptionDao(LanguageResource.class);
        }
        return languageResourceDao;
    }

    public String getIosCodeFromCode(String code) {
        String returnCode = code;

        RuntimeExceptionDao<LanguageDetail, String> dao = getLanguageModelDao();
        List<LanguageDetail> languageDetails = dao.queryForAll();
        for (LanguageDetail d : languageDetails) {
            if (d.getCode().equalsIgnoreCase(code)) {
                returnCode = d.getIosValue();
                break;
            }
        }

        return returnCode;
    }
}