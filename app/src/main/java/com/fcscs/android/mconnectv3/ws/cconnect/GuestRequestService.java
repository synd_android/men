package com.fcscs.android.mconnectv3.ws.cconnect;

import java.util.Date;
import java.util.List;

import org.ksoap2.serialization.SoapObject;

import android.content.Context;

import com.fcscs.android.mconnectv3.common.McEnums.JobStatus;
import com.fcscs.android.mconnectv3.common.SessionContext;
import com.fcscs.android.mconnectv3.common.util.DateTimeHelper;
import com.fcscs.android.mconnectv3.common.util.SoapHelper;
import com.fcscs.android.mconnectv3.http.WebServiceBase;
import com.fcscs.android.mconnectv3.manager.model.GuestItemModel;
import com.fcscs.android.mconnectv3.manager.model.JobModel;
import com.fcscs.android.mconnectv3.ws.cconnect.model.guestrequestservice.FindRoomGuestResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.guestrequestservice.GuestInfoResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.guestrequestservice.GuestRequestResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.GetJobSummaryResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.JobViewResponse;

public class GuestRequestService extends WebServiceBase {

    private static GuestRequestService instance;
    private static GuestRequestService instanceNonUI;

    private GuestRequestService() {
    }

    public static GuestRequestService getInstance() {
        if (instance == null) {
            instance = new GuestRequestService();
        }
        instance.setEnableUI(true);
        return instance;
    }

    public static GuestRequestService getInstanceNonUI() {
        if (instanceNonUI == null) {
            instanceNonUI = new GuestRequestService();
        }
        instanceNonUI.setEnableUI(false);
        return instanceNonUI;
    }

    private static final String WEBSERVICE_URL_SUFFIX = "ws/guestRequestService";
    private static final String NAMESPACE = "http://fcscs.com/ws/schemas/guestrequestservice";

    private static final String FIND_GUEST_By_NAME_REQ = "FindGuestsByNameRequest";
    private static final String FIND_GUEST_BY_NAME_REPS = "FindGuestsByNameResponse";
    private static final String FIND_ROOM_GUEST_REQ_NAME = "FindRoomGuestRequest";
    private static final String FIND_ROOM_GUEST_RESP_NAME = "FindRoomGuestResponse";
    private static final String GUEST_REQUEST_REQ_NAME = "GuestRequestRequest";
    private static final String INTER_DEPARTMENT_REQ_NAME = "InterDepartmentRequestRequest";
    private static final String INTER_DEPARTMENT_RESP_NAME = "InterDepartmentRequestResponse";
    private static final String GUEST_REQUEST_RESP_NAME = "GuestRequestResponse";

    private static final String FIND_REQUEST_JOB_REQ_NAME = "FindRequestJobsRequest";
    private static final String FIND_REQUEST_JOB_RESP_NAME = "FindRequestJobsResponse";
    private static final String GET_REQUEST_JOB_REQ_NAME = "GetRequestJobRequest";
    private static final String GET_REQUEST_JOB_RESP_NAME = "GetRequestJobResponse";
    //
    private static final String UPDATE_JOB_STATUS_REQ_NAME = "UpdateJobStatusRequest";
    private static final String UPDATE_JOB_STATUS_RESP_NAME = "UpdateJobStatusResponse";

    private static final String GET_JOB_SUMMARY_REQ_NAME = "GetJobSummaryRequest";
    private static final String GET_JOB_SUMMARY_RESP_NAME = "GetJobSummaryResponse";
    private static final String GET_GUEST_DETAI_REQ_NAME = "GetGuestDetailRequest";
    private static final String GET_GUEST_DETAI_RESP_NAME = "GetGuestDetailResponse";


    // private static final SimpleDateFormat DATE_FORMATTER = new
    // SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public GuestRequestResponse createGuestRequest(Context ctx, long guestRoomLinkId,
//			public CallResult<GuestRequestResponse> createGuestRequest(long guestRoomLinkId,
                                                   List<GuestItemModel> items) {

        String itemName = "item";

        SoapObject request = new SoapObject(NAMESPACE, GUEST_REQUEST_REQ_NAME);
        request.addAttribute("fcsUserId", SessionContext.getInstance().getFcsUserId());
        request.addAttribute("wsSessionId", SessionContext.getInstance().getWsSessionId());
        request.addAttribute("guestRoomLinkId", guestRoomLinkId);

        for (GuestItemModel model : items) {
            SoapObject item = new SoapObject(NAMESPACE, itemName);
            item.addAttribute("serviceItemId", model.getServiceItemId());
            item.addAttribute("serviceItemCategoryId", model.getServiceItemCategoryId());
            item.addAttribute("deliverToLocationId", model.getDeliverToLocationId());
            item.addAttribute("quantity", model.getQuantity());
            item.addAttribute("runnerId", model.getRunnerId());
            if (model.getRemarks() != null) {
                item.addProperty("remarks", model.getRemarks());
            }
            request.addSoapObject(item);
        }
        SoapObject soap = callWebService(ctx, request, WEBSERVICE_URL_SUFFIX, NAMESPACE, GUEST_REQUEST_RESP_NAME);
        if (soap != null) {
            return new GuestRequestResponse(soap);
        } else {
            return null;
        }

    }

//	public InterDepartmentRequestResponse createInterDepartmentRequest(Context ctx, String deliverLocationId,List<NonGuestItemModel> servictItemList) {
//
//		String itemName = "item";
//		SoapObject request = new SoapObject(NAMESPACE, INTER_DEPARTMENT_REQ_NAME);
//		request.addAttribute("fcsUserId", SessionContext.getInstance().getFcsUserId());
//		request.addAttribute("wsSessionId", SessionContext.getInstance().getWsSessionId());
//		request.addAttribute("locationId", deliverLocationId);
//
//		for (NonGuestItemModel model : servictItemList) {
//			SoapObject item = new SoapObject(NAMESPACE, itemName);
//			item.addAttribute("serviceItemId", model.getServiceItemId());
//			item.addAttribute("serviceItemCategoryId", model.getServiceItemCategoryId());
//			item.addAttribute("deliverToLocationId", model.getLocationId());
//			item.addAttribute("runnerId", model.getRunnerId());
//			item.addAttribute("quantity", model.getQty());
//			// **********************************************************
//			// TODO
//			// item.addAttribute("remarks",model.getRemarks());
//			// **********************************************************
//			if (model.getRemarks() != null) {
//				item.addProperty("remarks", model.getRemarks());
//			}
//			request.addSoapObject(item);
//		}
//		SoapObject soap = callWebService(ctx, request, WEBSERVICE_URL_SUFFIX, NAMESPACE, INTER_DEPARTMENT_RESP_NAME);
//		if(soap != null){
//			return new InterDepartmentRequestResponse(soap);
//		}else{
//			return null;
//		}
//
//	}

    public GuestInfoResponse getGuestDetail(Context ctx, long guestRoomLinkId) {
        SoapObject request = new SoapObject(NAMESPACE, GET_GUEST_DETAI_REQ_NAME);
        request.addAttribute("fcsUserId", SessionContext.getInstance().getFcsUserId());
        request.addAttribute("wsSessionId", SessionContext.getInstance().getWsSessionId());
        request.addAttribute("guestRoomLinkId", guestRoomLinkId);

        SoapObject soap = callWebService(ctx, request, WEBSERVICE_URL_SUFFIX, NAMESPACE, GET_GUEST_DETAI_RESP_NAME);
        if (soap != null) {
            return new GuestInfoResponse(soap);
        } else {
            return null;
        }
    }

    public GuestInfoResponse findGuestByName(Context ctx, String name) {
        SoapObject request = new SoapObject(NAMESPACE, FIND_GUEST_By_NAME_REQ);
        request.addAttribute("fcsUserId", SessionContext.getInstance().getFcsUserId());
        request.addAttribute("wsSessionId", SessionContext.getInstance().getWsSessionId());
        if (name != null) {
            request.addProperty("term", name);
        }

        SoapObject soap = callWebService(ctx, request, WEBSERVICE_URL_SUFFIX, NAMESPACE, FIND_GUEST_BY_NAME_REPS);
        if (soap != null) {
            return new GuestInfoResponse(soap);
        } else {
            return null;
        }

    }

    public FindRoomGuestResponse findRoomGuest(Context ctx, String roomNum) {

        SoapObject request = new SoapObject(NAMESPACE, FIND_ROOM_GUEST_REQ_NAME);
        request.addAttribute("fcsUserId", SessionContext.getInstance().getFcsUserId());
        request.addAttribute("wsSessionId", SessionContext.getInstance().getWsSessionId());
        if (roomNum != null) {
            request.addProperty("roomNum", roomNum);
        }

        SoapObject soap = callWebService(ctx, request, WEBSERVICE_URL_SUFFIX, NAMESPACE, FIND_ROOM_GUEST_RESP_NAME);
        if (soap != null) {
            return new FindRoomGuestResponse(soap);
        } else {
            return null;
        }
    }

    public JobViewResponse findJobView(Context ctx, Date date, String roomNo, Long departmentId, int start,
                                       int numResults) {

        SoapObject request = new SoapObject(NAMESPACE, FIND_REQUEST_JOB_REQ_NAME);
        request.addAttribute("fcsUserId", SessionContext.getInstance().getFcsUserId());
        request.addAttribute("wsSessionId", SessionContext.getInstance().getWsSessionId());
        String[] statu = JobStatus.getMyJobAllStatuses();
        request.addAttribute("start", start);
        request.addAttribute("numResults", numResults);
        if (date != null) {
            request.addProperty("date", DateTimeHelper.FORMATTER_DATETIME_24H.format(date));
        }
        if (statu != null) {
            for (int i = 0; i < statu.length; i++) {
                request.addProperty("status", statu[i]);
            }
        }
        if (roomNo != null) {
            request.addProperty("roomNo", roomNo.trim());
        }
        if (departmentId != null) {
            request.addProperty("deptartmentId", departmentId);
        }

        SoapObject soap = callWebService(ctx, request, WEBSERVICE_URL_SUFFIX, NAMESPACE, FIND_REQUEST_JOB_RESP_NAME);
        if (soap != null) {
            return new JobViewResponse(soap);
        } else {
            return null;
        }
    }

    public Boolean updateJobStatus(Context ctx, long jobViewId, JobStatus status2, Boolean acknowledge, String remarks) {

        SoapObject request = new SoapObject(NAMESPACE, UPDATE_JOB_STATUS_REQ_NAME);
        request.addAttribute("fcsUserId", SessionContext.getInstance().getFcsUserId());
        request.addAttribute("wsSessionId", SessionContext.getInstance().getWsSessionId());
        request.addAttribute("jobId", jobViewId);
        if (status2 != null) {
            request.addAttribute("status", status2.getCConnectName());
        }
        request.addAttribute("remarks", remarks);
        request.addAttribute("acknowledge", acknowledge);

        SoapObject response = callWebService(ctx, request, WEBSERVICE_URL_SUFFIX, NAMESPACE, UPDATE_JOB_STATUS_RESP_NAME);
        if (response != null) {
            return Boolean.valueOf(response.getAttributeAsString("result"));
        } else {
            return null;
        }

    }


    public JobModel getGuestRequestViewByJobId(Context ctx, long jobViewId) {

        SoapObject request = new SoapObject(NAMESPACE, GET_REQUEST_JOB_REQ_NAME);
        request.addAttribute("fcsUserId", SessionContext.getInstance().getFcsUserId());
        request.addAttribute("wsSessionId", SessionContext.getInstance().getWsSessionId());
        request.addAttribute("jobId", jobViewId);

        SoapObject response = callWebService(ctx, request, WEBSERVICE_URL_SUFFIX, NAMESPACE, GET_REQUEST_JOB_RESP_NAME);
        if (response != null) {
            JobModel jm = new JobModel();
            jm.setRequestorName(SoapHelper.getStringAttribute(response, "requestorName", ""));
            jm.setRemarks(SoapHelper.getStringProperty(response, "remarks", ""));
            SoapObject soap = (SoapObject) response.getProperty(0);
            if (soap != null) {
                Date dateNow = DateTimeHelper.getDateByTimeZone(new Date());
                jm.setJobId(SoapHelper.getLongAttribute(soap, "jobId", -1L));
                jm.setJobNum(SoapHelper.getStringAttribute(soap, "jobCode", ""));
                jm.setRoomNum(SoapHelper.getStringProperty(soap, "roomNum", ""));
                jm.setStatus(JobStatus.convertFromCCStatus(SoapHelper.getStringAttribute(soap, "status", "")));
                jm.setDeadline(SoapHelper.getDateProperty(soap, "deadline", DateTimeHelper.DEFAULT_FORMATTER_DATETIME_24H, dateNow));
                jm.setDate(SoapHelper.getDateProperty(soap, "created", DateTimeHelper.DEFAULT_FORMATTER_DATETIME_24H, dateNow));
                jm.setQuantity(SoapHelper.getIntegerAttribute(soap, "quantity", 0));
                jm.setServiceItemName(SoapHelper.getStringAttribute(soap, "serviceItemName", ""));
                jm.setPriorityName(SoapHelper.getStringProperty(soap, "priorityName", ""));
                jm.setIsAcked(SoapHelper.getBooleanAttribute(soap, "acknowledge", false));
                jm.setLocationDesc(SoapHelper.getStringAttribute(soap, "locationDesc", ""));
            }

            return jm;
        } else {
            return null;
        }

    }

    public GetJobSummaryResponse GetJobSummaryStatusCount(Context ctx, Date date) {

        SoapObject request = new SoapObject(NAMESPACE, GET_JOB_SUMMARY_REQ_NAME);
        request.addAttribute("fcsUserId", SessionContext.getInstance().getFcsUserId());
        request.addAttribute("wsSessionId", SessionContext.getInstance().getWsSessionId());
        String createDate = DateTimeHelper.getStrDateByTimeZone(date, DateTimeHelper.DEFAULT_FORMATTER_DATETIME_24H);
        request.addAttribute("createDate", createDate);
//		request.addAttribute("createDate", DateTimeHelper.FORMATTER_DATETIME_24H.format(date));

        SoapObject soap = callWebService(ctx, request, WEBSERVICE_URL_SUFFIX, NAMESPACE, GET_JOB_SUMMARY_RESP_NAME);
        if (soap != null) {
            return new GetJobSummaryResponse(soap);
        } else {
            return null;
        }

    }
}
