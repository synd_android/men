package com.fcscs.android.mconnectv3.ws.econnect;

import java.io.File;
import java.io.InputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
//import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.serialization.SoapObject;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xmlpull.v1.XmlPullParser;

import android.content.Context;
import android.util.Log;
import android.util.Xml;
import android.webkit.URLUtil;

import com.fcscs.android.mconnectv3.McApplication;
import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.ConfigContext;
import com.fcscs.android.mconnectv3.common.DatabaseHelper;
import com.fcscs.android.mconnectv3.common.McConstants;
import com.fcscs.android.mconnectv3.common.McEnums;
import com.fcscs.android.mconnectv3.common.McEnums.FavoritesEnum;
import com.fcscs.android.mconnectv3.common.McEnums.JobStatus;
import com.fcscs.android.mconnectv3.common.SessionContext;
import com.fcscs.android.mconnectv3.common.XMLParser;
import com.fcscs.android.mconnectv3.common.util.CoderSHA;
import com.fcscs.android.mconnectv3.common.util.DateTimeHelper;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.common.util.PrefsUtil;
import com.fcscs.android.mconnectv3.common.util.SoapConverter;
import com.fcscs.android.mconnectv3.common.util.SoapHelper;
import com.fcscs.android.mconnectv3.dao.entity.DBHelper;
import com.fcscs.android.mconnectv3.dao.entity.Department;
import com.fcscs.android.mconnectv3.dao.entity.Floor;
import com.fcscs.android.mconnectv3.dao.entity.Location;
import com.fcscs.android.mconnectv3.dao.entity.Runner;
import com.fcscs.android.mconnectv3.http.FcsServiceConnectionSE;
import com.fcscs.android.mconnectv3.http.NullHostNameVerifier;
import com.fcscs.android.mconnectv3.http.WebServiceBase;
import com.fcscs.android.mconnectv3.manager.model.EquipmentModel;
import com.fcscs.android.mconnectv3.manager.model.FavouriteLink;
import com.fcscs.android.mconnectv3.manager.model.GuestHistoryConfig;
import com.fcscs.android.mconnectv3.manager.model.GuestInfo;
import com.fcscs.android.mconnectv3.manager.model.JobModel;
import com.fcscs.android.mconnectv3.manager.model.LanguageDetail;
import com.fcscs.android.mconnectv3.manager.model.LanguageResource;
import com.fcscs.android.mconnectv3.manager.model.MediaLibraryConfig;
import com.fcscs.android.mconnectv3.manager.model.MessageModel;
import com.fcscs.android.mconnectv3.manager.model.MessageWrapper;
import com.fcscs.android.mconnectv3.manager.model.MinibarItem;
import com.fcscs.android.mconnectv3.manager.model.PostUpdateEngineeringJobDetailsRequestJobDetails;
import com.fcscs.android.mconnectv3.manager.model.RunnerModel;
import com.fcscs.android.mconnectv3.manager.model.ServiceCodeModel;
import com.fcscs.android.mconnectv3.manager.model.ServiceRequestItem;
import com.fcscs.android.mconnectv3.manager.model.StatusModel;
import com.fcscs.android.mconnectv3.smartpush.SmartPush;
import com.fcscs.android.mconnectv3.ws.cconnect.model.guestrequestservice.EngineeringRequestResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.guestrequestservice.FindGroupLocationResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.guestrequestservice.FindItemNameResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.guestrequestservice.FindItemResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.guestrequestservice.FindLocationResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.guestrequestservice.FindRoomGuestResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.guestrequestservice.Guest;
import com.fcscs.android.mconnectv3.ws.cconnect.model.guestrequestservice.GuestInfoResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.guestrequestservice.GuestRequestResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.CreateIndividualMsgResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.FindMsgResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.FindRunnerGroupResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.FindRunnerResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.GetGroupRunnerListResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.GetNotifyInfoResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.GetNotifyInfoResponse.Notify;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.GetTemplateListResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.Group;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.Template;
import com.fcscs.android.mconnectv3.ws.cconnect.model.securityservice.LoginResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.securityservice.LogoutResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.GetEquipmentResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.GetFavouriteLinkResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.GetFloorsResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.GetJobSummaryResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.GetLocationsResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.GetMinbarItemsResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.InterDepartmentRequestResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.JobViewResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.SearchMyJobResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.ServiceItemViewModel;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.TopServiceItemResponse;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.AckAdHocResponse;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.CommonConfigurationsDetails;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.EngineeringChecklistDetails;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.EngineeringRunnersDetails;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.GetCheckLoginResponse;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.GetCommonConfigurationsResponse;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.GetEngineeringChecklistResponse;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.GetEngineeringRunnersResponse;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.GetEquipmentsResponse;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.GetFreeRunnersResponse;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.GetNotificationStatusResponse;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.GetPendingNotificationResponse;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.GetStatusColorResponse;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.GetUpdateJobStatusResponse;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.ModuleLicense;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.PMCondition;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.UpdateableJobModel;
import com.google.gson.Gson;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.Where;

public class ECService extends WebServiceBase {

    private static final String TAG = ECService.class.getSimpleName();

    private static final String GET_AD_HOC_GROUP_LIST = "getAdHocGroupList";
    private static final String GET_MY_JOB_SUMMARY = "GetMyJobSummary";
    private static final String GET_LOCATION_LIST = "getLocationList";
    private static final String GET_TOP_SERVICE_ITEM = "getTopServiceItem";
    private static final String ACK_JOBS = "ackJobs";
    private static final String UPDATE_JOB_REMARK = "updateJobRemark";
    private static final String UPDATE_ENGINEERING_JOB = "UpdateJob";
    private static final String UPDATE_ENGINEERING_JOB_REMARK = "updateEngineeringJobRemark";
    private static final String CLOSE_JOBS = "closeJobs";
    private static final String CLOSE_JOBS2 = "closeJobs2";
    private static final String CLOSE_ENGINEERING_JOBS = "closeEngineeringJobs";
    private static final String CREATE_AD_HOC = "createAdHoc";
    private static final String GET_ADHOC_DETAIL = "GetAdhocDetail";
    private static final String UPDATE_TOKEN = "updateToken";
    private static final String GET_SUPPORT_EMAIL_ADDRESSES = "GetSupportEmailAddresses";
    private static final String GET_WS_VERSION = "GetWSVersion";
    private static final String UPDATE_ROOM_STATUS = "UpdateRoomStatus";
    private static final String POST_MINIBAR = "PostMinibar";
    private static final String GET_FAVOURITE_LINK = "getFavouriteLink";
    private static final String GET_BADGE = "GetBadge";
    private static final String GET_ROOMS_LISTING = "GetRoomsListing";
    private static final String GET_JOBS = "GetJobs";
    private static final String GET_JOB_Q_REQUEST_HISTORY = "GetJobQRequestHistory";
    private static final String GET_GUEST_LISTING_REQUEST = "GetGuestListingRequest";
    private static final String GET_GROUP_LOCATION_LISTING_REQUEST = "GetLocationGroupRequest";
    private static final String GET_LOCATION_LISTING_REQUEST = "GetLocationCodeRequest";
    private static final String GET_ITEM_LISTING_REQUEST = "GetJobQEngineeringRequestRequest";
    private static final String GET_ITEM_NAME_LISTING_REQUEST = "GetJobQEngineeringRequestServiceItemsRequest";
    private static final String GET_MINIBAR_CONFIGURATION_ITEMS = "GetMinibarConfigurationItems";
    private static final String GET_JOB_Q_SERVICE_CODES = "GetJobQServiceCodes";
    private static final String GET_JOB_Q_SERVICE_ITEMS_REQUEST = "GetJobQServiceItemsRequest";
    private static final String GET_PANIC_BUTTON_CONFIG = "GetPanicButtonConfig";
    private static final String GET_ROOM_STATUS = "GetRoomStatus";
    private static final String POST_PANIC_BUTTON_ALERT = "PostPanicButtonAlert";
    private static final String GET_MEDIA_LIBRARY_CONFIG = "GetMediaLibraryConfig";
    private static final String POST_MEDIA_LIBRARY_CONTENT_REQUEST = "PostMediaLibraryContentRequest";
    private static final String GET_MEDIA_LIBRARY_CONTENT_REQUEST = "GetMediaLibraryContentRequest";
    private static final String GET_DEPARTMENT_LIST = "getDepartmentList";
    private static final String GET_MESSAGE_TEMPLATES = "getMessageTemplates";
    private static final String ACK_AD_HOC = "AckAdHoc";
    private static final String GET_AC_HOC_LIST = "getAcHocList";
    private static final String GET_AD_HOC_USER_LIST = "getAdHocUserList";
    private static final String GET_LOGOUT_RESPONSE = "getLogoutResponse";
    private static final String GET_LOGOUT = "getLogout";
    private static final String CREATE_JOBREQUEST = "createJobRequest";
    private static final String CREATE_JOBREQUEST2 = "createJobRequest2";
    private static final String GET_PENDING_NOTIFICATIONS = "GetPendingNotifications";
    private static final String GET_CHECK_LOGIN = "GetStillLoggedIn";
    private static final String GET_UPDATE_JOB_STATUS = "GetUpdateJobStatus";
    private static final String GET_GUEST_LISTING = "GetGuestListing";
    private static final String GET_GROUP_LOCATION_LISTING = "GetLocationGroup";
    private static final String GET_LOCATION_LISTING = "GetLocationCode";
    private static final String GET_ITEM_LISTING = "GetJobQEngineeringRequest";
    private static final String GET_ITEM_NAME_LISTING = "GetJobQEngineeringRequestServiceItems";
    private static final String CREATE_ENGINEERING_REQUEST = "ValidateCreateJobRequest";
    private static final String GET_ADHOCLISTS = "GetAdhocLists";
    private static final String GET_COMMON_CONF = "GetCommonConfigurations";
    private static final String GET_STATUS_COLOR = "getStatusColor";
    private static final String GET_EQUIPMENT_DETAIL = "GetEquipmentDetail";
    private static final String GET_ACK_ENGINEERING_JOBS = "ackEngineeringJobs";
    private static final String POST_UPDATE_ENGINEERING_JOBDETAILS = "PostUpdateEngineeringJobDetails";
    private static final String POST_UPDATE_JOBDETAILS = "PostUpdateJobDetails";
    private static final String GET_ENGINEERING_CHECKLIST = "GetEngineeringChecklist";
    private static final String GET_ENGINEERING_RUNNER = "GetEngineeringRunners";
    private static final String GET_FREE_RUNNERS = "GetFreeRunners";
    private static final String UPDATE_REASSIGN_RUNNER = "UpdateReAssignRunner";
    private static final String UPDATE_CHECKLIST = "UpdateChecklist";
    private static final String GET_JOB_QSERVICE_ITEMS = "GetJobQServiceItems";
    private static final String GET_EQUIPMENTS = "GetEquipments";
    private static final String NAMESPACE = "http://eConnect/";
    private static final String UPDATE_ACCEPT_JOB = "UpdateAcceptJob";
    private static final String UPDATE_INSPECTED_JOB = "UpdateInspectedJob";
    private static final String UPDATE_REASSIGN_FAILED_INSPECTIONJOB = "UpdateReAssignFailedInspectionJob";


    private static ECService instance;
    private static List<Guest> guests;

    private static final SimpleDateFormat jobdf = new SimpleDateFormat(DateTimeHelper.DEFAULT_FORMATTER_DATETIME_24H_T, Locale.US);

    private static Integer TIMEOUT;

    private int intPageSize;
    private int PageIndex;
    private List<ServiceItemViewModel> vListItems = null;

    private ECService() {
    }

    public static ECService getInstance() {
        if (instance == null) {
            instance = new ECService();
        }
        instance.setEnableUI(true);
        if (TIMEOUT == null) {
            TIMEOUT = PrefsUtil.getTimeout(McApplication.application);
        }
        instance.setConnectTimeout(TIMEOUT * 1000);
        instance.setReadTimeout((TIMEOUT + 10) * 1000);
        return instance;
    }

    public static ECService getInstance(int connectTimeout, int readTimeout) {
        if (instance == null) {
            instance = new ECService();
        }
        instance.setEnableUI(true);
        instance.setConnectTimeout(connectTimeout);
        instance.setReadTimeout(readTimeout);
        return instance;
    }

    private SoapObject getResultSoapObject(SoapObject soap) {
        if (soap.getPropertyCount() > 0 && soap.getProperty(0) instanceof SoapObject) {
            SoapObject result = (SoapObject) soap.getProperty(0);
            if (result.getPropertyCount() > 0 && result.getProperty(0) instanceof SoapObject) {
                SoapObject res = (SoapObject) result.getProperty(0);
                return res;
            }
        }
        return null;
    }

    public static boolean isReachable(String url) {
        try {
            URL myUrl = new URL(url);
            URLConnection connection = myUrl.openConnection();
            connection.setConnectTimeout(3000);
            connection.connect();
            return true;
        } catch (Exception e) {
            System.out.println("Exception .. " + e.getMessage());
        }
        return false;
    }

    private Date getDate(int hour, int minute, int second) {

        Calendar cal = Calendar.getInstance(Locale.US);
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, minute);
        cal.set(Calendar.SECOND, second);

        return cal.getTime();
    }

    public GetStatusColorResponse getStatusColor(Context ctx) {

        if (McConstants.OFFLINE_MODE) {
            return null;
        }

        SoapObject request = new SoapObject(NAMESPACE, GET_STATUS_COLOR);
        SoapObject o = new SoapObject(NAMESPACE, "getStatusColor");
        String soapAction = NAMESPACE + GET_STATUS_COLOR;

        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "getStatusColorResponse");
        if (response != null) {
            GetStatusColorResponse resp = null;
            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    if (soapresult.hasProperty("getStatusColor")) {
                        resp = new GetStatusColorResponse(soapresult);
                        if (resp != null) {
                            SessionContext.getInstance().setStatusColorMap(resp.getMap());
                        }
                    }
                }
            }
            return resp;
        }
        return null;
    }

    public GetCommonConfigurationsResponse getCommonConfigurations(Context ctx) {

        if (McConstants.OFFLINE_MODE) {
            PrefsUtil.setAutoAckMsg(ctx, false);
            PrefsUtil.setAutoAckMyJob(ctx, false);
            PrefsUtil.setJobPullingInterval(ctx, 0);
            PrefsUtil.setMsgPullingInterval(ctx, 0);
            PrefsUtil.setMinibarType(ctx, 2);
            return null;
        }

        SoapObject request = new SoapObject(NAMESPACE, GET_COMMON_CONF);
        SoapObject o = new SoapObject(NAMESPACE, "GetCommonConfigurationsRequest");
        o.addProperty("UserID", SessionContext.getInstance().getFcsUserId());
        request.addSoapObject(o);
        String soapAction = NAMESPACE + GET_COMMON_CONF;

        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "GetCommonConfigurationsResponse");
        if (response != null) {
            GetCommonConfigurationsResponse resp = null;
            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    if (soapresult.hasProperty("ResponseStatus") && soapresult.hasProperty("CommonConfigurationsListing")) {
                        resp = new GetCommonConfigurationsResponse(soapresult);
                        if (resp.getReturnCode() == 0) {
                            List<CommonConfigurationsDetails> list = resp.getList();
                            boolean autoAckMsg = false;
                            boolean autoAckJob = false;
                            int jobInterval = 0;
                            int msgInterval = 0;
                            int minibarPostingType = 0;
                            int enablePTT = 0;
                            for (CommonConfigurationsDetails d : list) {
                                if (CommonConfigurationsDetails.EC_DATE_FORMAT.equalsIgnoreCase(d.getModule())) {
                                    ConfigContext.getInstance().setDateFormat(d.getValue());
                                }
                                if (CommonConfigurationsDetails.EC_AUTO_ACK_MSG.equalsIgnoreCase(d.getModule())
                                        && "yes".equalsIgnoreCase(d.getValue())) {
                                    autoAckMsg = true;
                                }
                                if (CommonConfigurationsDetails.EC_AUTO_ACK_JOB.equalsIgnoreCase(d.getModule())
                                        && "yes".equalsIgnoreCase(d.getValue())) {
                                    autoAckJob = true;
                                }
                                if (CommonConfigurationsDetails.EC_JOB_PULLING_DURATION.equalsIgnoreCase(d.getModule())) {
                                    try {
                                        jobInterval = Integer.valueOf(d.getValue());
                                    } catch (NumberFormatException e) {
                                    }
                                }
                                if (CommonConfigurationsDetails.EC_MSG_PULLING_DURATION.equalsIgnoreCase(d.getModule())) {
                                    try {
                                        msgInterval = Integer.valueOf(d.getValue());
                                    } catch (NumberFormatException e) {
                                    }
                                }
                                if ("mCONNECT_LONG_DATE_FORMAT".equalsIgnoreCase(d.getModule())
                                        && McUtils.isNullOrEmpty(d.getValue()) == false) {
                                    String fmt = d.getValue();
                                    try {
                                        new SimpleDateFormat(fmt);
                                        PrefsUtil.setDateFormat(ctx, fmt);
                                    } catch (Exception e) {
                                        // 如果时间格式不对，则会抛错
                                        Log.e(TAG, fmt, e);
                                    }
                                }
                                if ("mCONNECT_LONG_DATE_TIME_FORMAT".equalsIgnoreCase(d.getModule())
                                        && McUtils.isNullOrEmpty(d.getValue()) == false) {
                                    String fmt = d.getValue();
                                    try {
                                        new SimpleDateFormat(fmt);
                                        PrefsUtil.setDateTimeFormat(ctx, fmt);
                                    } catch (Exception e) {
                                        // 如果时间格式不对，则会抛错
                                        Log.e(TAG, fmt, e);
                                    }
                                }
                                if ("mCONNECT_MINIBAR_POSTING_TYPE".equalsIgnoreCase(d.getModule())) {
                                    try {
                                        minibarPostingType = Integer.valueOf(d.getValue());
                                    } catch (NumberFormatException e) {
                                    }
                                }
                                if ("mCONNECT_ENABLE_PTT".equalsIgnoreCase(d.getModule())) {
                                    try {
                                        enablePTT = Integer.valueOf(d.getValue());
                                    } catch (NumberFormatException e) {
                                    }
                                }
                            }
                            PrefsUtil.setAutoAckMsg(ctx, autoAckMsg);
                            PrefsUtil.setAutoAckMyJob(ctx, autoAckJob);
                            PrefsUtil.setJobPullingInterval(ctx, jobInterval);
                            PrefsUtil.setMsgPullingInterval(ctx, msgInterval);
                            PrefsUtil.setMinibarType(ctx, minibarPostingType);
                            PrefsUtil.setEnablePTT(ctx, enablePTT);
                        }
                    }
                }
            }
            return resp;
        }
        return null;
    }


    public LoginResponse getCompositeLogin(Context ctx, String username, String password) {

        DBHelper.clearCache(ctx);
        DatabaseHelper.getInstance(ctx).clear();
        String demoUsr = "demo";
        String demoPwd = ctx.getString(R.string.fcs_demo_pwd);
        if (McConstants.COMPILATION_PRODUCT == McEnums.CompilationProduct.PERCIPIA) {
            demoPwd = ctx.getString(R.string.per_demo_pwd);
        }
        //SHA256
        String key = ctx.getString(R.string.secure_pref_key);
        if (demoUsr.equals(username) && demoPwd.equals(CoderSHA.hmacSHA256Digest(password, key))) {

            PrefsUtil.setOfflineMode(ctx, true);
            LoginResponse login = new LoginResponse();
            login.setUserId(9999999L);
            login.setName("demo");
            login.setOrgId(-1L);
            login.setPropId(-1L);
            login.setSessionId("");
            login.setReturnCode("0");


            TIMEOUT = 15;
            PrefsUtil.setTimeout(ctx, 15);
            PrefsUtil.setAutoAckMsg(ctx, false);
            PrefsUtil.setAutoAckMyJob(ctx, false);
            PrefsUtil.setDateFormat(ctx, DateTimeHelper.YYYY_MM_DD);
            PrefsUtil.setDateTimeFormat(ctx, DateTimeHelper.DEFAULT_FORMATTER_DATETIME_24H);
            PrefsUtil.setJobPullingInterval(ctx, 60);
            PrefsUtil.setMsgPullingInterval(ctx, 120);

            ModuleLicense license = new ModuleLicense();
            license.setEngineeringRequest(true);
            license.setFavorite(true);
            license.setGuestDetail(true);
            license.setGuestRequest(true);
            license.setNonGuestRequest(true);
            license.setInterDeptRequest(true);
            license.setEngineeringRequest(false);
            license.setJobRequest(true);
            license.setJobSearch(true);
            license.setMyJob(true);
            license.setMinibar(true);
            license.setMyMessage(true);
            license.setSendMessage(true);
            license.setRoomStatus(true);
            SessionContext session = SessionContext.getInstance();
            session.setModule(license);
            session.persist(ctx);

            return login;
        } else {
            PrefsUtil.setOfflineMode(ctx, false);
        }

        String methodName = "GetCompositeLogin";
        SoapObject request = new SoapObject(NAMESPACE, "GetCompositeLoginRequest");
        request.addProperty("UserName", username);
        request.addProperty("Password", password);
        request.addProperty("LanguageCode", McUtils.getECServiceLang());

//		try {
//			String version = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0).versionName;
//			request.addProperty("ApplicationVersion", version);
//		} catch (NameNotFoundException e) {
//		}
        String version = McConstants.APP_VERSION_NAME;
        request.addProperty("ApplicationVersion", version);
        String address = McUtils.getMacAddr();
        request.addProperty("DeviceIdentifier", address);

        //		request.addProperty("DeviceType", "2");
        /**
         * 0 = iOS 1 = Android Socket Push 2 = Android C2DM
         */
        if (SmartPush.getConfigPushTypePrefs(ctx) == SmartPush.PUSH_TYPE_GCM) {
            request.addProperty("DeviceType", "2");
        } else {
            request.addProperty("DeviceType", "4");
            request.addProperty("DeviceToken", address);
        }

        SoapObject req = new SoapObject(NAMESPACE, methodName);
        req.addSoapObject(request);

        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(req, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "GetCompositeLoginResponse");

        if (response != null) {

            SoapObject result = SoapHelper.getInnerSoapObject(response, "GetCompositeLoginResult");
            SoapObject status = SoapHelper.getInnerSoapObject(result, "ResponseStatus");
            String returnCode = SoapHelper.getStringProperty(status, "ReturnCode", "-1");
            String errorMsg = SoapHelper.getStringProperty(status, "ErrorMsg", "");

            LoginResponse login = new LoginResponse();
            login.setErrorMsg(errorMsg);
            login.setReturnCode(returnCode);

            if ("0".equals(returnCode)) {
                SoapObject user = SoapHelper.getInnerSoapObject(result, "LoggedInUser");
                if (user != null) {
                    login.setUserId(SoapHelper.getLongProperty(user, "UserID", -1L));
                    login.setName(SoapHelper.getStringProperty(user, "UserName", ""));
                    login.setOrgId(-1L);
                    login.setPropId(-1L);
                    login.setSessionId("");
                    login.setReturnCode(returnCode);
                    login.setUserTimeZone(SoapHelper.getStringProperty(user, "UserTimeZone", ""));
                    login.setUserType(SoapHelper.getStringProperty(user, "UserType", ""));
                    login.setReAssignRunner(SoapHelper.getIntegerProperty(user, "ReAssignRunner", 0));
                    login.setUserDepartment(SoapHelper.getStringProperty(user, "UserDepartment", ""));
                }
                SoapObject config = SoapHelper.getInnerSoapObject(result, "CommonConfigurationsListing");
                if (config != null && config.getPropertyCount() > 0) {
                    boolean autoAckMsg = false;
                    boolean autoAckJob = false;
                    int jobInterval = 0;
                    int msgInterval = 0;
                    int timeout = 15;
                    int minibarPostingType = 0; //show both
                    int enablePTT = 0;
                    int enableEngReq = 0;
                    int enableGuestRequest = 0;
                    int enableNonGuestRequest = 0;
                    int enableEng = 0;
                    for (int i = 0; i < config.getPropertyCount(); i++) {
                        if ("CommonConfigurationsDetails".equalsIgnoreCase(SoapHelper.getPropertyName(config, i))) {
                            SoapObject s = (SoapObject) config.getProperty(i);
                            String module = SoapHelper.getStringProperty(s, "Module", "");
                            String value = SoapHelper.getStringProperty(s, "Value", "");
                            if ("mCONNECT_ADHOC_MSG_AUTO_ACK".equalsIgnoreCase(module)) {
                                autoAckMsg = McUtils.equalStr(false, value, "YES");
                            }

                            if ("mCONNECT_JOBQ_AUTO_ACK".equalsIgnoreCase(module)) {
                                autoAckJob = McUtils.equalStr(false, value, "YES");
                            }
                            if ("mCONNECT_NOTIFICATION_PULL_JOBQ_DURATION".equalsIgnoreCase(module)) {
                                try {
                                    jobInterval = Integer.parseInt(value);
                                } catch (Exception e) {
                                }
                            }
                            if ("mCONNECT_NOTIFICATION_PULL_AHMSG_DURATION".equalsIgnoreCase(module)) {
                                try {
                                    msgInterval = Integer.parseInt(value);
                                } catch (Exception e) {
                                }
                            }
                            if ("mCONNECT_WS_API_TIMEOUT".equalsIgnoreCase(module)) {
                                try {
                                    timeout = Integer.parseInt(value);
                                } catch (Exception e) {
                                }
                            }
                            if ("mCONNECT_SHORT_DATE_FORMAT".equalsIgnoreCase(module) && McUtils.isNullOrEmpty(value) == false) {
                                try {
                                    new SimpleDateFormat(value);
                                    PrefsUtil.setShortDateFormat(ctx, value);
                                } catch (Exception e) {
                                    // 如果时间格式不对，则会抛错
                                    Log.e(TAG, value, e);
                                }
                            }
                            if ("mCONNECT_SHORT_DATE_TIME_FORMAT".equalsIgnoreCase(module) && McUtils.isNullOrEmpty(value) == false) {
                                try {
                                    new SimpleDateFormat(value);
                                    PrefsUtil.setShortDateTimeFormat(ctx, value);
                                } catch (Exception e) {
                                    // 如果时间格式不对，则会抛错
                                    Log.e(TAG, value, e);
                                }
                            }
                            if ("mCONNECT_LONG_DATE_FORMAT".equalsIgnoreCase(module) && McUtils.isNullOrEmpty(value) == false) {
                                try {
                                    new SimpleDateFormat(value);
                                    PrefsUtil.setDateFormat(ctx, value);
                                } catch (Exception e) {
                                    // 如果时间格式不对，则会抛错
                                    Log.e(TAG, value, e);
                                }
                            }
                            if ("mCONNECT_LONG_DATE_TIME_FORMAT".equalsIgnoreCase(module) && McUtils.isNullOrEmpty(value) == false) {
                                try {
                                    new SimpleDateFormat(value);
                                    PrefsUtil.setDateTimeFormat(ctx, value);
                                } catch (Exception e) {
                                    // 如果时间格式不对，则会抛错
                                    Log.e(TAG, value, e);
                                }
                            }
                            if ("mCONNECT_MINIBAR_POSTING_TYPE".equalsIgnoreCase(module)) {
                                try {
                                    minibarPostingType = Integer.valueOf(value);
                                } catch (NumberFormatException e) {
                                }
                            }
                            if ("mCONNECT_ENABLE_PTT".equalsIgnoreCase(module)) {
                                try {
                                    enablePTT = Integer.valueOf(value);
                                } catch (NumberFormatException e) {
                                }
                            }
                            if ("mCONNECT_ENABLE_ENGINEERINGREQUEST".equalsIgnoreCase(module)) {
                                try {

                                    enableEngReq = Integer.valueOf(value);
                                } catch (NumberFormatException e) {
                                }
                            }
                            if ("mCONNECT_ENABLE_GUEST_REQUEST".equalsIgnoreCase(module)) {
                                try {
                                    enableGuestRequest = Integer.valueOf(value);
                                } catch (NumberFormatException e) {
                                }
                            }
                            if ("mCONNECT_ENABLE_NONGUEST_REQUEST".equalsIgnoreCase(module)) {
                                try {
                                    enableNonGuestRequest = Integer.valueOf(value);
                                } catch (NumberFormatException e) {
                                }
                            }
                            if ("mCONNECT_ENABLE_MENGINEERING".equalsIgnoreCase(module)) {
                                try {

                                    enableEng = Integer.valueOf(value);
                                } catch (NumberFormatException e) {
                                }
                            }
                            if ("mCONNECT_QR_CODE_LOGIN".equalsIgnoreCase(module)) {
                                boolean v = McUtils.equalStr(false, value, "YES");
                                PrefsUtil.setQRCodeLogin(ctx, v);
                            }
                            if ("mCONNECT_QR_CODE_LOGOUT".equalsIgnoreCase(module)) {
                                boolean v = McUtils.equalStr(false, value, "YES");
                                PrefsUtil.setQRCodeLogout(ctx, v);
                            }
                            if ("mCONNECT_QR_CODE_LICENSE_REGISTRATION".equalsIgnoreCase(module)) {
                                boolean v = McUtils.equalStr(false, value, "YES");
                                PrefsUtil.setQRCodeLicenseRegistration(ctx, v);
                            }
                            if ("mCONNECT_QR_CODE_SEARCH_JOB".equalsIgnoreCase(module)) {
                                boolean v = McUtils.equalStr(false, value, "YES");
                                PrefsUtil.setQRCodeSearchJob(ctx, v);
                            }
                            if ("mCONNECT_QR_CODE_GUEST_DETAIL".equalsIgnoreCase(module)) {
                                boolean v = McUtils.equalStr(false, value, "YES");
                                PrefsUtil.setQRCodeGuestDetail(ctx, v);
                            }
                            if ("mCONNECT_QR_CODE_NEW_REQUEST".equalsIgnoreCase(module)) {
                                boolean v = McUtils.equalStr(false, value, "YES");
                                PrefsUtil.setQRCodeNewRequest(ctx, v);
                            }
                            if ("mCONNECT_QR_CODE_ROOM_STATUS".equalsIgnoreCase(module)) {
                                boolean v = McUtils.equalStr(false, value, "YES");
                                PrefsUtil.setQRCodeRoomStatus(ctx, v);
                            }
                            if ("mCONNECT_QR_CODE_MINIBAR".equalsIgnoreCase(module)) {
                                boolean v = McUtils.equalStr(false, value, "YES");
                                PrefsUtil.setQRCodeMinibar(ctx, v);
                            }
                            if ("mCONNECT_JOB_STATUS".equalsIgnoreCase(module)) {
                                boolean v = value.contains("10");
                                PrefsUtil.setConnectJobStatus(ctx, v);
                            }
                            if ("ENGINEERING_DEPARTMENT_ID".equalsIgnoreCase(module)) {
                                int v;
                                try {
                                    v = Integer.valueOf(value);
                                } catch (NumberFormatException e) {
                                    v = 0;
                                }

                                PrefsUtil.setEnDerpId(ctx, v);
                            }
                            if ("ALLOW_JOB_EDIT_AFTER_COMPLETED".equalsIgnoreCase(module)) {
                                int v;
                                try {
                                    v = Integer.valueOf(value);
                                } catch (NumberFormatException e) {
                                    v = 0;
                                }
                                PrefsUtil.setAllowJobEditAfterCompleted(ctx, v);
                            }
                            if ("ENABLE_CONNECT_PLUS".equalsIgnoreCase(module)) {
                                int v = 0;
                                try {
                                    v = Integer.valueOf(value);
                                } catch (NumberFormatException e) {
                                    v = 0;
                                }

                                PrefsUtil.setEnableConnectPlus(ctx, v);
                            }
                        }
                    }
                    PrefsUtil.setAutoAckMsg(ctx, autoAckMsg);
                    PrefsUtil.setAutoAckMyJob(ctx, autoAckJob);
                    PrefsUtil.setJobPullingInterval(ctx, jobInterval);
                    PrefsUtil.setMsgPullingInterval(ctx, msgInterval);
                    PrefsUtil.setMinibarType(ctx, minibarPostingType);
                    PrefsUtil.setEnablePTT(ctx, enablePTT);
                    PrefsUtil.setEnableEngReq(ctx, enableEngReq);
                    PrefsUtil.setEnableEng(ctx, enableEng);
                    PrefsUtil.setEnableGuestRequestReq(ctx, enableGuestRequest);
                    PrefsUtil.setEnableNonGuestRequestReq(ctx, enableNonGuestRequest);
                    if (timeout > 5) {
                        PrefsUtil.setTimeout(ctx, timeout);
                        TIMEOUT = timeout;
                    }
                }
                String module = SoapHelper.getStringProperty(result, "Modules", null);
                if (McUtils.isNullOrEmpty(module) == false) {
                    try {
                        module = "<root>" + module + "</root>";
                        SAXParserFactory spf = SAXParserFactory.newInstance();
                        SAXParser sp = spf.newSAXParser();
                        XMLReader xr = sp.getXMLReader();
                        ModuleHandler dataHandler = new ModuleHandler();
                        xr.setContentHandler(dataHandler);
                        xr.parse(new InputSource(new StringReader(module)));
                        ModuleLicense license = dataHandler.getLicence();
                        SessionContext session = SessionContext.getInstance();
                        session.setModule(license);
                        session.persist(ctx);
                    } catch (Exception ioe) {
                        Log.e(TAG, "parsing Modules error", ioe);
                    }
                }

                String history = SoapHelper.getStringProperty(result, "GuestHistoryRange", null);
                if (McUtils.isNullOrEmpty(history) == false) {
                    try {
                        history = "<root>" + history + "</root>";
                        SAXParserFactory spf = SAXParserFactory.newInstance();
                        SAXParser sp = spf.newSAXParser();
                        XMLReader xr = sp.getXMLReader();
                        HistoryHandler dataHandler = new HistoryHandler();
                        xr.setContentHandler(dataHandler);
                        xr.parse(new InputSource(new StringReader(history)));
                        GuestHistoryConfig his = dataHandler.getHistory();
                        PrefsUtil.setGuestHistoryConfig(ctx, his);
                    } catch (Exception ioe) {
                        Log.e(TAG, "parsing GuestHistoryRange error", ioe);
                    }
                }
                SoapObject conditionList = SoapHelper.getInnerSoapObject(result, "PMConditionsListing");
                List<PMCondition> _listCondition = new ArrayList<PMCondition>();
                if (conditionList != null && conditionList.getPropertyCount() > 0) {
                    for (int i = 0; i < conditionList.getPropertyCount(); i++) {
                        if ("PMConditions".equalsIgnoreCase(SoapHelper.getPropertyName(conditionList, i))) {
                            PMCondition pmCondition = new PMCondition();
                            SoapObject s = (SoapObject) conditionList.getProperty(i);
                            int ConditionID = SoapHelper.getIntegerProperty(s, "ConditionID", -1);
                            String ConditionName = SoapHelper.getStringProperty(s, "ConditionName", "");
                            String ConditionLang = SoapHelper.getStringProperty(s, "ConditionLang", "");
                            String EmailAlert = SoapHelper.getStringProperty(s, "EmailAlert", "");
                            String EmailTemplate = SoapHelper.getStringProperty(s, "EmailTemplate", "");
                            pmCondition.setConditionID(ConditionID);
                            pmCondition.setConditionName(ConditionName);
                            pmCondition.setConditionLang(ConditionLang);
                            pmCondition.setEmailAlert(EmailAlert);
                            pmCondition.setEmailTemplate(EmailTemplate);
                            _listCondition.add(pmCondition);

                        }
                    }
                    String jsonListconfition = new Gson().toJson(_listCondition);
                    PrefsUtil.setConditionList(ctx, jsonListconfition);
                }

            } else {
                login.setReturnCode("1");
            }
            return login;
        }
        return null;

    }

    public LogoutResponse getLogout(Context ctx, String userId) {

        if (McConstants.OFFLINE_MODE) {
            LogoutResponse logout = new LogoutResponse();
            logout.setResult(true);
            return logout;
        }

        String methodName = GET_LOGOUT;
        SoapObject request = new SoapObject(NAMESPACE, methodName);
        request.addProperty("userID", userId);

        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                GET_LOGOUT_RESPONSE);

        if (response != null) {
            SoapObject result = getResultSoapObject(response);
            Integer returnCode = SoapHelper.getIntegerProperty(result, "ReturnCode", 1);
            if (result != null) {
                LogoutResponse logout = new LogoutResponse();
                logout.setResult(returnCode == 0);
                return logout;
            }
        }
        return null;
    }

    public FindRunnerGroupResponse getAdHocGroupList(Context ctx, String term) {

        FindRunnerGroupResponse resp = new FindRunnerGroupResponse();
        List<Group> groupList = new ArrayList<Group>();

        if (McConstants.OFFLINE_MODE) {
            groupList = getOfflineGroups(term);
            resp.setGroupList(groupList);
            return resp;
        }

        String methodName = GET_AD_HOC_GROUP_LIST;
        SoapObject request = new SoapObject(NAMESPACE, methodName);
        request.addProperty("UserID", SessionContext.getInstance().getFcsUserId());

        if (term == null) {
            term = "";
        }
        request.addProperty("Term", term);

        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "getAdHocGroupListResponse");

        if (response != null) {

            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject result = (SoapObject) response.getProperty(0);
                if (result.getPropertyCount() > 0 && result.getProperty(0) instanceof SoapObject) {
                    SoapObject res = (SoapObject) result.getProperty(0);
                    for (int i = 0; i < res.getPropertyCount(); i++) {
                        if (res.getProperty(i) instanceof SoapObject) {
                            SoapObject node = (SoapObject) res.getProperty(i);
                            Group g = new Group();
                            g.setGroupId(SoapHelper.getLongProperty(node, "ID", -1L));
                            g.setGroupName(SoapHelper.getStringProperty(node, "Name", ""));
                            groupList.add(g);
                        }
                    }
                }
            }

            resp.setGroupList(groupList);
            return resp;
        }
        return null;
    }

    private List<Group> getOfflineGroups(String term) {
        List<Group> groupList = new ArrayList<Group>();

        Group g = new Group();
        g.setGroupId(1L);
        g.setGroupName("Fire Alarm");
        groupList.add(g);

        g = new Group();
        g.setGroupId(2L);
        g.setGroupName("False Alarm");
        groupList.add(g);

        g = new Group();
        g.setGroupId(3L);
        g.setGroupName("Evacuation");
        groupList.add(g);

        List<Group> gs = new ArrayList<Group>();
        for (Group p : groupList) {
            if (p.getGroupName().contains(term)) {
                gs.add(p);
            }
        }

        return gs;
    }

    public FindRunnerResponse getAdHocUserList(Context ctx, String term, Long deptId, Long grpId) {

        FindRunnerResponse resp = new FindRunnerResponse();
        List<Runner> runners = new ArrayList<Runner>();

        if (McConstants.OFFLINE_MODE) {

            runners = getOfflineRunners(term);

            resp.setRunners(runners);
            return resp;
        }

        long runnerCount = DBHelper.getRunnerCount(ctx);
        if (runnerCount > 0) {
            List<Runner> list = DBHelper.queryRunners(ctx, term, deptId);
            resp.setRunners(list);
            resp.setTotal((int) runnerCount);
            return resp;
        }

        List<Department> deps = getDepartmentList(ctx);
        Map<String, Long> depIdMap = new HashMap<String, Long>();
        for (Department d : deps) {
            depIdMap.put(d.getDepartmentName().toLowerCase(Locale.US), d.getDepartmentId());
        }

        String methodName = GET_AD_HOC_USER_LIST;
        SoapObject request = new SoapObject(NAMESPACE, methodName);
        request.addProperty("UserID", SessionContext.getInstance().getFcsUserId());

        request.addProperty("Term", "");
        request.addProperty("DeptID", "");

        String gid = "";
        if (grpId != null) {
            gid = gid + grpId;
        }
        request.addProperty("GrpID", gid);

        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "getAdHocUserListResponse");

        if (response != null) {

            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject result = (SoapObject) response.getProperty(0);
                if (result.getPropertyCount() > 0 && result.getProperty(0) instanceof SoapObject) {
                    SoapObject res = (SoapObject) result.getProperty(0);
                    for (int i = 0; i < res.getPropertyCount(); i++) {
                        if (res.getProperty(i) instanceof SoapObject) {
                            SoapObject node = (SoapObject) res.getProperty(i);
                            Runner r = new Runner();
                            r.setFullname(SoapHelper.getStringProperty(node, "Fullname", ""));
                            r.setRunnerId(SoapHelper.getLongProperty(node, "ID", -1L));
                            String department = SoapHelper.getStringProperty(node, "Department", "").trim();
                            r.setDepartmentId(depIdMap.get(department.toLowerCase(Locale.US)));
                            if (r.getRunnerId() > 0) {
                                runners.add(r);
                            }
                        }
                    }
                }
            }

            DBHelper.saveRunners(ctx, runners);

            List<Runner> list = DBHelper.queryRunners(ctx, term, deptId);
            resp.setRunners(list);
            resp.setTotal(runners.size());

            return resp;
        }
        return null;
    }

    private List<Runner> getOfflineRunners(String term) {
        List<Runner> runners = new ArrayList<Runner>();

        Runner r = new Runner();
        r.setFullname("Billy Chew");
        r.setRunnerId(1L);
        r.setDepartmentId(0L);
        runners.add(r);

        r = new Runner();
        r.setFullname("David Chan");
        r.setRunnerId(2L);
        r.setDepartmentId(0L);
        runners.add(r);

        r = new Runner();
        r.setFullname("Sonia Lim");
        r.setRunnerId(3L);
        r.setDepartmentId(0L);
        runners.add(r);

        List<Runner> rs = new ArrayList<Runner>();
        for (Runner ru : runners) {
            if (ru.getFullname().contains(term)) {
                rs.add(ru);
            }
        }

        return rs;
    }

    public GetGroupRunnerListResponse getAdhocRunenrListForGroup(Context ctx, Long grpId) {

        GetGroupRunnerListResponse resp = new GetGroupRunnerListResponse();
        List<Runner> runners = new ArrayList<Runner>();

        if (McConstants.OFFLINE_MODE) {
            runners = getOfflineRunners("");
            resp.setRunnerList(runners);
            return resp;
        }

        String methodName = GET_AD_HOC_USER_LIST;
        SoapObject request = new SoapObject(NAMESPACE, methodName);
        request.addProperty("UserID", SessionContext.getInstance().getFcsUserId());

        request.addProperty("Term", "");
        request.addProperty("DeptID", "");

        String gid = "";
        if (grpId != null) {
            gid = gid + grpId;
        }
        request.addProperty("GrpID", gid);

        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "getAdHocUserListResponse");

        if (response != null) {

            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject result = (SoapObject) response.getProperty(0);
                if (result.getPropertyCount() > 0 && result.getProperty(0) instanceof SoapObject) {
                    SoapObject res = (SoapObject) result.getProperty(0);
                    for (int i = 0; i < res.getPropertyCount(); i++) {
                        if (res.getProperty(i) instanceof SoapObject) {
                            SoapObject node = (SoapObject) res.getProperty(i);
                            Runner r = new Runner();
                            r.setFullname(SoapHelper.getStringProperty(node, "Fullname", ""));
                            r.setRunnerId(SoapHelper.getLongProperty(node, "ID", -1L));
                            if (r.getRunnerId() > 0) {
                                runners.add(r);
                            }
                        }
                    }
                }
            }
            resp.setRunnerList(runners);
            return resp;
        }
        return null;

    }

    public GetJobSummaryResponse getJobSummary(Context ctx, Date myDate, String searchDays) {

        GetJobSummaryResponse resp = new GetJobSummaryResponse();
        ArrayList<StatusModel> statusModels = new ArrayList<StatusModel>();
        Map<JobStatus, Integer> statusIdMap = new HashMap<JobStatus, Integer>();

        if (McConstants.OFFLINE_MODE) {

            statusIdMap = getOfflineStatusIdMap();
            statusModels = getOfflineStatusModel();

            resp.setStatusModels(statusModels);
            resp.setStatusIdMap(statusIdMap);

            return resp;
        }

        String methodName = GET_MY_JOB_SUMMARY;

        SoapObject request = new SoapObject(NAMESPACE, methodName);
        request.addProperty("userID", SessionContext.getInstance().getFcsUserId());

        SimpleDateFormat df = new SimpleDateFormat(DateTimeHelper.FORMAT_DATEYEAR);
        String strMyDate = DateTimeHelper.getStrDateByTimeZone(myDate, DateTimeHelper.FORMAT_DATEYEAR);
//        request.addProperty("myDate", df.format(strMyDate));
        request.addProperty("myDate", (strMyDate));
        if (PrefsUtil.getEnableEng(ctx) == 1) {
            request.addProperty("includeEngineeringJobs", 1);
            request.addProperty("searchDays", searchDays);
        }

        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "GetMyJobSummaryResponse");

        if (response != null) {

            int pending = 0;
            int complete = 0;
            int cancel = 0;
            int escalate = 0;
            int timeout = 0;
            int delayed = 0;

            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject result = (SoapObject) response.getProperty(0);
                if (result.getPropertyCount() > 0 && result.getProperty(0) instanceof SoapObject) {
                    SoapObject res = (SoapObject) result.getProperty(0);
                    for (int i = 0; i < res.getPropertyCount(); i++) {
                        if (res.getProperty(i) instanceof SoapObject) {
                            SoapObject node = (SoapObject) res.getProperty(i);

                            String statusName = SoapHelper.getStringProperty(node, "Desc", "");
                            Integer id = SoapHelper.getIntegerProperty(node, "StatusID", -1);
                            int count = SoapHelper.getIntegerProperty(node, "Count", 0);

                            JobStatus js = JobStatus.getJobStatusFromStatusId(id);
                            if (JobStatus.PENDING.equals(js)) {
                                pending = count;
                            } else if (JobStatus.COMPLETED.equals(js)) {
                                complete = count;
                            } else if (JobStatus.CANCELLED.equals(js)) {
                                cancel = count;
                            } else if (JobStatus.TIMEOUT.equals(js)) {
                                timeout = count;
                            } else if (JobStatus.DELAYED.equals(js)) {
                                delayed = count;
                            }

                            statusIdMap.put(js, id);
                        }
                    }
                }
            }

            int all = pending + complete + cancel + escalate + timeout + delayed;
            statusModels.add(new StatusModel(pending, JobStatus.PENDING));
            statusModels.add(new StatusModel(complete, JobStatus.COMPLETED));
            statusModels.add(new StatusModel(delayed, JobStatus.DELAYED));
            statusModels.add(new StatusModel(cancel, JobStatus.CANCELLED));
            statusModels.add(new StatusModel(timeout, JobStatus.TIMEOUT));

            statusModels.add(new StatusModel(all, JobStatus.ALL));

            resp.setStatusModels(statusModels);
            resp.setStatusIdMap(statusIdMap);
            return resp;
        }
        return null;
    }

    public SearchMyJobResponse searchJobs(Context ctx, int PageIndex, String roomNum, String guestName, String assignedBy, String assignedTo,
                                          String location, String jobNo, Date myDate, String equipmentNo, boolean isShowOpenJob, String searchDays) {

        int intPageIndex = PageIndex;
        SearchMyJobResponse resp = new SearchMyJobResponse();
        ArrayList<StatusModel> statusModels = new ArrayList<StatusModel>();

        if (McConstants.OFFLINE_MODE) {
            statusModels = getOfflineStatusModel();
            List<JobModel> models = getOfflineJobModels(null);

            resp.setStatusModels(statusModels);
            resp.setJobViews(models);
            resp.setTotal(models.size());

            return resp;
        }

        SoapObject request = new SoapObject(NAMESPACE, "GetJobsRequest");

        request.addProperty("UserID", SessionContext.getInstance().getFcsUserId());
        request.addProperty("UserLanguage", McUtils.getECServiceLang());
        request.addProperty("RoomNo", roomNum);
        request.addProperty("GuestName", guestName);
        request.addProperty("AssignedByName", assignedBy);
        request.addProperty("AssignedToName", assignedTo);
        request.addProperty("JobNo", jobNo);
        request.addProperty("Location", location);
        if (intPageIndex > 0) {
            request.addProperty("PageIndex", intPageIndex);
        }
        if (PrefsUtil.getEnableEng(ctx) == 1) {
            request.addProperty("IncludeEngineeringJobs", 1);

        } else {
            request.addProperty("IncludeEngineeringJobs", 0);
        }
        request.addProperty("EquipmentNo", equipmentNo);
        if (isShowOpenJob) {
            request.addProperty("IncludeOpenStatusJobs", 1);
        }
        if (myDate == null) {
            myDate = SoapHelper.getDefaultDate();
        }
//        request.addProperty("SearchDate", DateTimeHelper.getStrDateByTimeZone(myDate, DateTimeHelper.DEFAULT_FORMATTER_DATETIME_24H_T));
        request.addProperty("SearchDate", jobdf.format(myDate));
        request.addProperty("SearchDays", searchDays);

        String method = GET_JOBS;
        SoapObject req = new SoapObject(NAMESPACE, method);
        req.addSoapObject(request);

        String soapAction = NAMESPACE + method;
        SoapObject response = callWebService(req, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE, "GetJobsResponse");

        JobViewResponse res = parseGetJobsResponse(response);

        if (res != null) {

            int open = 0;
            int pending = 0;
            int complete = 0;
            int cancel = 0;
            int timeout = 0;
            int schedule = 0;

            List<JobModel> models = new ArrayList<JobModel>();
            if (res.getJobViews() != null) {
                for (JobModel jm : res.getJobViews()) {
                    models.add(jm);
                    JobStatus js = jm.getStatus();
                    if (JobStatus.PENDING.equals(js)) {
                        pending++;
                    } else if (JobStatus.COMPLETED.equals(js)) {
                        complete++;
                    } else if (JobStatus.CANCELLED.equals(js)) {
                        cancel++;
                    } else if (JobStatus.OPEN.equals(js)) {
                        open++;
                    } else if (JobStatus.TIMEOUT.equals(js)) {
                        timeout++;
                    } else if (JobStatus.SCHEDULED.equals(js)) {
                        schedule++;
                    }
                }
            }

            int all = open + pending + complete + cancel + timeout + schedule;
            statusModels.add(new StatusModel(open, JobStatus.OPEN));
            statusModels.add(new StatusModel(pending, JobStatus.PENDING));
            statusModels.add(new StatusModel(complete, JobStatus.COMPLETED));
            statusModels.add(new StatusModel(cancel, JobStatus.CANCELLED));
            statusModels.add(new StatusModel(timeout, JobStatus.TIMEOUT));
            statusModels.add(new StatusModel(schedule, JobStatus.SCHEDULED));
            statusModels.add(new StatusModel(all, JobStatus.ALL));

            resp.setJobViews(models);
            resp.setTotal(models.size());
            resp.setStatusModels(statusModels);

        }
        return resp;
    }

    private ArrayList<StatusModel> getOfflineStatusModel() {
        ArrayList<StatusModel> statusModels = new ArrayList<StatusModel>();
        statusModels.add(new StatusModel(2, JobStatus.PENDING));
        statusModels.add(new StatusModel(1, JobStatus.COMPLETED));
        statusModels.add(new StatusModel(0, JobStatus.CANCELLED));
        statusModels.add(new StatusModel(0, JobStatus.TIMEOUT));
        statusModels.add(new StatusModel(3, JobStatus.ALL));
        return statusModels;
    }

    private Map<JobStatus, Integer> getOfflineStatusIdMap() {
        Map<JobStatus, Integer> statusIdMap = new HashMap<JobStatus, Integer>();
        statusIdMap.put(JobStatus.PENDING, 1);
        statusIdMap.put(JobStatus.COMPLETED, 2);
        statusIdMap.put(JobStatus.CANCELLED, 3);
        statusIdMap.put(JobStatus.TIMEOUT, 4);
        return statusIdMap;
    }

    private List<JobModel> getOfflineJobModels(Integer statusID) {
        List<JobModel> models = new ArrayList<JobModel>();

        if (statusID == null || 1 == statusID.intValue()) {
            JobModel jm = new JobModel();
            jm.setDate(getDate(16, 15, 0));
            jm.setRequestedDate(getDate(15, 15, 0));
            jm.setDeadline(getDate(16, 35, 0));
            jm.setStatus(JobStatus.PENDING);
            jm.setIsAcked(false);
            jm.setJobId(600003L);
            jm.setJobNum("600003");
            jm.setServiceItemName("Bath Towel");
            jm.setQuantity(1);
            jm.setRequestorName("David Chan");
            jm.setGuestName("Mr. King");
            jm.setPriorityName("");
            jm.setReportedBy("David Chan");
            jm.setRoomNum("RN1001");
            jm.setVip("V1");
            jm.setRemarks("Urgent");
            models.add(jm);

            jm = new JobModel();
            jm.setDate(getDate(15, 20, 0));
            jm.setRequestedDate(getDate(15, 20, 0));
            jm.setDeadline(getDate(15, 55, 0));
            jm.setStatus(JobStatus.PENDING);
            jm.setIsAcked(false);
            jm.setJobId(600002L);
            jm.setJobNum("600002");
            jm.setServiceItemName("Ceiling Leaking");
            jm.setQuantity(1);
            jm.setRequestorName("David Chan");
            jm.setGuestName("Jacky Lam");
            jm.setPriorityName("");
            jm.setReportedBy("David Chan");
            jm.setRoomNum("Kitchen L3");
            jm.setVip("V1");
            jm.setRemarks("GM is there");
            models.add(jm);
        }

        if (statusID == null || 2 == statusID.intValue()) {

            JobModel jm = new JobModel();
            jm.setDate(getDate(15, 10, 0));
            jm.setRequestedDate(getDate(15, 10, 0));
            jm.setDeadline(getDate(15, 35, 0));
            jm.setCompletedTime(getDate(15, 30, 0));
            jm.setStatus(JobStatus.COMPLETED);
            jm.setIsAcked(true);
            jm.setJobId(600001L);
            jm.setJobNum("600001");
            jm.setServiceItemName("A/C too cold");
            jm.setQuantity(1);
            jm.setRequestorName("David Chan");
            jm.setGuestName("Mr. Martin");
            jm.setPriorityName("");
            jm.setReportedBy("David Chan");
            jm.setRoomNum("RN1008");
            jm.setVip("V1");
            jm.setRemarks("Guest is waiting in the room");
            models.add(jm);

        }

        return models;
    }

    public GetFloorsResponse findFloors(Context ctx) {

        GetFloorsResponse resp = new GetFloorsResponse();
        resp.setFloorList(DBHelper.queryFloors(ctx, null));
        return resp;

    }

    //intPageSize, intPageIndex
    //public GetLocationsResponse findLocations(Context ctx, String desc, String floor) {
    //public GetLocationsResponse findLocations(Context ctx, int PageSize, int PageIndex, String desc, String floor) {
    public GetLocationsResponse findLocations(Context ctx, int PageIndex, String desc, String floor) {

        GetLocationsResponse resp = new GetLocationsResponse();
        List<Location> list = new ArrayList<Location>();
        List<Location> vListLoc = null;

        if (McConstants.OFFLINE_MODE) {

            list = getOfflineLocations(desc);
            resp.setLocationList(list);
            resp.setTotal(list.size());

            return resp;
        }

        //if (DBHelper.getLocationCount(ctx) == 0L) {

        long intDebugUserID = SessionContext.getInstance().getFcsUserId();
        //String strMethodName = GET_LOCATION_LIST;
        String MethodName = GET_LOCATION_LIST;

        SoapObject request = new SoapObject(NAMESPACE, MethodName);
        request.addProperty("UserID", SessionContext.getInstance().getFcsUserId());
        if (desc == null)
            request.addProperty("Desc", "");
        else
            request.addProperty("Desc", desc);
        if (floor == null)
            request.addProperty("Floor", "");
        else
            request.addProperty("Floor", floor);
        String soapAction = NAMESPACE + MethodName;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE, "getLocationListResponse");

        if (response != null) {
            //List<Location> locationList = new ArrayList<Location>();
            vListLoc = new ArrayList<Location>();
            Set<Floor> floorList = new HashSet<Floor>();

            SoapObject res = SoapHelper.getInnerSoapObject(response, 0, 0);
            for (int i = 0; res != null && i < res.getPropertyCount(); i++) {
                if (res.getProperty(i) instanceof SoapObject) {
                    SoapObject node = (SoapObject) res.getProperty(i);
                    Location l = new Location();
                    l.setName(SoapHelper.getStringProperty(node, "Name", ""));
                    l.setLocationCode(SoapHelper.getStringProperty(node, "ID", ""));
                    l.setLocationId(-1L);
                    l.setFloorId(SoapHelper.getStringProperty(node, "Floor", ""));
                    //locationList.add(l);
                    vListLoc.add(l);

                    Floor f = new Floor();
                    f.setFloorId(-1L);
                    f.setName(l.getFloorId());
                    floorList.add(f);

                }
            }

            //DBHelper.saveLocations(ctx, locationList);
            DBHelper.saveLocations(ctx, vListLoc);

            List<Floor> floorl = new ArrayList<Floor>();
            for (Floor f : floorList) {
                floorl.add(f);
            }

            Collections.sort(floorl, new Comparator<Floor>() {

                @Override
                public int compare(Floor lhs, Floor rhs) {
                    return lhs.getName().compareTo(rhs.getName());
                }
            });
            DBHelper.saveFloors(ctx, floorl);

        }
        //}

        //..Modify By 	: Chua Kam Hoong
        //..Date 		: 20 May 2015
        //list = DBHelper.queryLocations(ctx, null, floor, desc);
        //list = DBHelper.queryLocations(ctx, PageSize, PageIndex, null, floor, desc);
        //list = DBHelper.queryLocations(ctx, PageIndex, null, floor, desc);
        list = vListLoc;
        resp = new GetLocationsResponse();
        resp.setLocationList(list);
        resp.setTotal(list.size());

        return resp;

    }

    private List<Location> getOfflineLocations(String desc) {
        List<Location> list = new ArrayList<Location>();
        Location l = new Location();
        l.setName("Lobby Lift");
        l.setLocationId(1L);
        l.setLocationCode("" + l.getLocationId());
        l.setFloorId("");
        list.add(l);

        l = new Location();
        l.setName("Kitchen L3");
        l.setLocationId(2L);
        l.setLocationCode("" + l.getLocationId());
        l.setFloorId("");
        list.add(l);

        l = new Location();
        l.setName("Parking Area B5");
        l.setLocationId(3L);
        l.setLocationCode("" + l.getLocationId());
        l.setFloorId("");
        list.add(l);

        l = new Location();
        l.setName("Pantry L10");
        l.setLocationId(4L);
        l.setLocationCode("" + l.getLocationId());
        l.setFloorId("");
        list.add(l);

        List<Location> ls = new ArrayList<Location>();
        for (Location lo : list) {
            if (lo.getName().contains(desc)) {
                ls.add(lo);
            }
        }

        return ls;
    }

    public GuestRequestResponse createGuestLocationRequest(Context ctx, String location, List<ServiceRequestItem> items,
                                                           Date scheduledDate, String guestName, Boolean isGuestRequest) {

        GuestRequestResponse resp = new GuestRequestResponse();
        if (McConstants.OFFLINE_MODE) {
            resp.setReturnCode("0");
            resp.setErrorMessage("");
            resp.setReturnCode1("0");
            resp.setErrorMessage1("0030001");
            resp.setReturnCode2("0");
            resp.setErrorMessage2("");
            resp.setReturnCode3("0");
            resp.setErrorMessage3("");
            return resp;
        }

        String methodName = CREATE_JOBREQUEST;
        SoapObject request = new SoapObject(NAMESPACE, methodName);
        request.addProperty("userID", SessionContext.getInstance().getFcsUserId());
        request.addProperty("RoomNo", "");
        request.addProperty("sLocation", location);
        request.addProperty("sType", isGuestRequest ? "Guest" : "InterDept");
        if (scheduledDate != null) {
            String schedule = DateTimeHelper.formatDateToStr(scheduledDate, DateTimeHelper.FORMAT_DATETIME_FULL, "");
            request.addProperty("schedule", schedule);
        } else {
            request.addProperty("schedule", "");
        }

        //..Chua :

        for (int i = 0; i < items.size(); i++) {
            ServiceRequestItem si = items.get(i);
            int count = i + 1;
            request.addProperty("Item" + count, si.getItemId());
            request.addProperty("Item" + count + "Qty", si.getQuantity());
            request.addProperty("Item" + count + "Remark", si.getRemarks());
        }

		/*
        for (int i = 0; i < items.size(); i++) {
			ServiceItemModel si = items.get(i);
			if (i == 0) {
				request.addProperty("Item1", si.getServiceItem().getServiceItemId());
				request.addProperty("Item1Qty", si.getQty());
				request.addProperty("Item1Remark", si.getRemarks());
			} else if (i == 1) {
				request.addProperty("Item2", si.getServiceItem().getServiceItemId());
				request.addProperty("Item2Qty", si.getQty());
				request.addProperty("Item2Remark", si.getRemarks());
			} else if (i == 2) {
				request.addProperty("Item3", si.getServiceItem().getServiceItemId());
				request.addProperty("Item3Qty", si.getQty());
				request.addProperty("Item3Remark", si.getRemarks());
			}
		}
		*/

        request.addProperty("GuestName", "");
        request.addProperty("Requestor", guestName);

        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "createJobRequestResponse");
        if (response != null) {
            SoapObject result = getResultSoapObject(response);
            resp.setReturnCode(SoapHelper.getStringProperty(result, "ReturnCode", "1"));
            resp.setErrorMessage(SoapHelper.getStringProperty(result, "ErrorMsg", ""));
            resp.setReturnCode1(SoapHelper.getStringProperty(result, "ReturnCode1", "1"));
            resp.setErrorMessage1(SoapHelper.getStringProperty(result, "ErrorMsg1", ""));
            resp.setReturnCode2(SoapHelper.getStringProperty(result, "ReturnCode2", "1"));
            resp.setErrorMessage2(SoapHelper.getStringProperty(result, "ErrorMsg2", ""));
            resp.setReturnCode3(SoapHelper.getStringProperty(result, "ReturnCode3", ""));
            resp.setErrorMessage3(SoapHelper.getStringProperty(result, "ErrorMsg3", ""));
            return resp;
        }
        return null;
    }

    public GuestRequestResponse createGuestRequest(Context ctx, String roomNo, List<ServiceRequestItem> items,
                                                   Date scheduledDate, String guestName) {

        GuestRequestResponse resp = new GuestRequestResponse();
        if (McConstants.OFFLINE_MODE) {
            resp.setReturnCode("0");
            resp.setErrorMessage("");
            resp.setReturnCode1("0");
            resp.setErrorMessage1("0030001");
            resp.setReturnCode2("0");
            resp.setErrorMessage2("");
            resp.setReturnCode3("0");
            resp.setErrorMessage3("");
            return resp;
        }

        String methodName = CREATE_JOBREQUEST;
        SoapObject request = new SoapObject(NAMESPACE, methodName);
        request.addProperty("userID", SessionContext.getInstance().getFcsUserId());
        request.addProperty("RoomNo", roomNo);
        request.addProperty("sLocation", "");
        request.addProperty("sType", "");
        if (scheduledDate != null) {
            String schedule = DateTimeHelper.formatDateToStr(scheduledDate, DateTimeHelper.FORMAT_DATETIME_FULL, "");
            request.addProperty("schedule", schedule);
        } else {
            request.addProperty("schedule", "");
        }

        //..Chua :

        for (int i = 0; i < items.size(); i++) {
            ServiceRequestItem si = items.get(i);
            int count = i + 1;
            request.addProperty("Item" + count, si.getItemId());
            request.addProperty("Item" + count + "Qty", si.getQuantity());
            request.addProperty("Item" + count + "Remark", si.getRemarks());
        }

		/*
        for (int i = 0; i < items.size(); i++) {
			ServiceItemModel si = items.get(i);
			if (i == 0) {
				request.addProperty("Item1", si.getServiceItem().getServiceItemId());
				request.addProperty("Item1Qty", si.getQty());
				request.addProperty("Item1Remark", si.getRemarks());
			} else if (i == 1) {
				request.addProperty("Item2", si.getServiceItem().getServiceItemId());
				request.addProperty("Item2Qty", si.getQty());
				request.addProperty("Item2Remark", si.getRemarks());
			} else if (i == 2) {
				request.addProperty("Item3", si.getServiceItem().getServiceItemId());
				request.addProperty("Item3Qty", si.getQty());
				request.addProperty("Item3Remark", si.getRemarks());
			}
		}
		*/

        request.addProperty("GuestName", guestName);
        request.addProperty("Requestor", guestName);

        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "createJobRequestResponse");
        if (response != null) {
            SoapObject result = getResultSoapObject(response);
            resp.setReturnCode(SoapHelper.getStringProperty(result, "ReturnCode", "1"));
            resp.setErrorMessage(SoapHelper.getStringProperty(result, "ErrorMsg", ""));
            resp.setReturnCode1(SoapHelper.getStringProperty(result, "ReturnCode1", "1"));
            resp.setErrorMessage1(SoapHelper.getStringProperty(result, "ErrorMsg1", ""));
            resp.setReturnCode2(SoapHelper.getStringProperty(result, "ReturnCode2", "1"));
            resp.setErrorMessage2(SoapHelper.getStringProperty(result, "ErrorMsg2", ""));
            resp.setReturnCode3(SoapHelper.getStringProperty(result, "ReturnCode3", ""));
            resp.setErrorMessage3(SoapHelper.getStringProperty(result, "ErrorMsg3", ""));
            return resp;
        }
        return null;
    }

    public InterDepartmentRequestResponse createInterDepartmentRequest(Context ctx, String location,
                                                                       List<ServiceRequestItem> items, Date scheduledDate, String requestor,
                                                                       boolean isAckKnow, int intPic, int intSign, String equipmentID) {

        InterDepartmentRequestResponse resp = new InterDepartmentRequestResponse();
        if (McConstants.OFFLINE_MODE) {
            resp.setReturnCode("0");
            resp.setErrorMessage("");
            resp.setReturnCode1("0");
            resp.setErrorMessage1("0030001");
            resp.setReturnCode2("0");
            resp.setErrorMessage2("");
            resp.setReturnCode3("0");
            resp.setErrorMessage3("");
            return resp;
        }

        String methodName = CREATE_JOBREQUEST;
        String strRespone = "createJobRequestResponse";
        if (isAckKnow) {
            methodName = CREATE_JOBREQUEST2;
            strRespone = "createJobRequest2Response";
        }
        SoapObject request = new SoapObject(NAMESPACE, methodName);
        request.addProperty("userID", SessionContext.getInstance().getFcsUserId());
        request.addProperty("RoomNo", "");
        request.addProperty("sLocation", location);
        request.addProperty("sType", "InterDept");
        request.addProperty("EquipmentID", equipmentID);
        for (int i = 0; i < items.size(); i++) {
            ServiceRequestItem si = items.get(i);
            int count = i + 1;
            request.addProperty("Item" + count, si.getItemId());
            request.addProperty("Item" + count + "Qty", si.getQuantity());
            request.addProperty("Item" + count + "Remark", si.getRemarks());
        }
        if (requestor != null) {
            request.addProperty("Requestor", requestor);
            request.addProperty("GuestName", requestor);
        }

        if (scheduledDate != null) {
            String schedule = (DateTimeHelper.formatDateToStr(scheduledDate, DateTimeHelper.FORMAT_DATETIME_FULL, ""));
            request.addProperty("schedule", schedule);
        } else {
            request.addProperty("schedule", "");
        }
        if (isAckKnow) {
            request.addProperty("RequiredPicture", intPic);
            request.addProperty("RequiredESignature", intSign);
        }
        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                strRespone);
        if (response != null) {
            SoapObject result = getResultSoapObject(response);

            resp.setReturnCode(SoapHelper.getStringProperty(result, "ReturnCode", "1"));
            resp.setErrorMessage(SoapHelper.getStringProperty(result, "ErrorMsg", ""));
            resp.setReturnCode1(SoapHelper.getStringProperty(result, "ReturnCode1", "1"));
            resp.setErrorMessage1(SoapHelper.getStringProperty(result, "ErrorMsg1", ""));
            resp.setReturnCode2(SoapHelper.getStringProperty(result, "ReturnCode2", "1"));
            resp.setErrorMessage2(SoapHelper.getStringProperty(result, "ErrorMsg2", ""));
            resp.setReturnCode3(SoapHelper.getStringProperty(result, "ReturnCode3", ""));
            resp.setErrorMessage3(SoapHelper.getStringProperty(result, "ErrorMsg3", ""));
            return resp;
        }
        return null;
    }

    public JobViewResponse getMyJobsList(Context ctx, Date myDate, Integer statusID, String searchDays) {

        JobViewResponse job = new JobViewResponse();
        List<JobModel> models = new ArrayList<JobModel>();

        if (McConstants.OFFLINE_MODE) {
            models = getOfflineJobModels(statusID);
            job.setJobViews(models);
            return job;
        }

        SoapObject request = new SoapObject(NAMESPACE, "GetJobsRequest");

        request.addProperty("UserID", SessionContext.getInstance().getFcsUserId());
        request.addProperty("UserLanguage", McUtils.getECServiceLang());
        request.addProperty("JobStatus", statusID == null ? "" : statusID);
        request.addProperty("Location", "");
        request.addProperty("RoomNo", "");
        request.addProperty("GuestName", "");
        request.addProperty("ReportedBy", "");
        request.addProperty("JobNo", "");
        request.addProperty("AssignedByName", "");
        request.addProperty("AssignedToName", "");
        request.addProperty("Ack", "");
        //..Chua : Add Page Index, 0 mean no paging...
        request.addProperty("PageIndex", 0);
        if (PrefsUtil.getEnableEng(ctx) == 1) {
            request.addProperty("IncludeEngineeringJobs", 1);
            request.addProperty("SearchDays", searchDays);
        } else {
            request.addProperty("IncludeEngineeringJobs", 0);
        }

        SimpleDateFormat df = new SimpleDateFormat(DateTimeHelper.DEFAULT_FORMATTER_DATETIME_24H_T, Locale.US);
        if (myDate == null) {
            myDate = SoapHelper.getDefaultDate();
        }
        request.addProperty("SearchDate", DateTimeHelper.getStrDateByTimeZone(myDate, DateTimeHelper.DEFAULT_FORMATTER_DATETIME_24H_T));

        String method = GET_JOBS;
        SoapObject req = new SoapObject(NAMESPACE, method);
        req.addSoapObject(request);

        String soapAction = NAMESPACE + method;
        SoapObject response = callWebService(req, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "GetJobsResponse");

        JobViewResponse res = parseGetJobsResponse(response);

        if (res != null) {
            if (res.getJobViews() != null) {
                for (JobModel jm : res.getJobViews()) {
                    if (JobStatus.PENDING.getStatusId() == jm.getStatus().getStatusId() || JobStatus.COMPLETED.getStatusId() == jm.getStatus().getStatusId()
                            || JobStatus.CANCELLED.getStatusId() == jm.getStatus().getStatusId() || JobStatus.TIMEOUT.getStatusId() == jm.getStatus().getStatusId()
                            || JobStatus.DELAYED.getStatusId() == jm.getStatus().getStatusId()) {
                        models.add(jm);
                    }
                }
            }
            job.setJobViews(models);
            job.setTotal(models.size());
            return job;

        }

        return null;

    }

    private List<ServiceItemViewModel> getOfflineServiceItems(String itemDescTerm) {
        List<ServiceItemViewModel> sv = new ArrayList<ServiceItemViewModel>();
        ServiceItemViewModel s = new ServiceItemViewModel();

        s.setServiceItemId(1L);
        s.setName("A/C too cold");
        s.setDisplayName(s.getName());
        sv.add(s);

        s = new ServiceItemViewModel();
        s.setServiceItemId(2L);
        s.setName("A/C too hot");
        s.setDisplayName(s.getName());
        sv.add(s);

        s = new ServiceItemViewModel();
        s.setServiceItemId(3L);
        s.setName("Adaptor");
        s.setDisplayName(s.getName());
        sv.add(s);

        s = new ServiceItemViewModel();
        s.setServiceItemId(4L);
        s.setName("Bath Towel");
        s.setDisplayName(s.getName());
        sv.add(s);

        s = new ServiceItemViewModel();
        s.setServiceItemId(5L);
        s.setName("Water Leaking");
        s.setDisplayName(s.getName());
        sv.add(s);

        s = new ServiceItemViewModel();
        s.setServiceItemId(6L);
        s.setName("Floor Dirty");
        s.setDisplayName(s.getName());
        sv.add(s);

        s = new ServiceItemViewModel();
        s.setServiceItemId(7L);
        s.setName("Coke");
        s.setDisplayName(s.getName());
        sv.add(s);

        s = new ServiceItemViewModel();
        s.setServiceItemId(8L);
        s.setName("DVD Player");
        s.setDisplayName(s.getName());
        sv.add(s);

        s = new ServiceItemViewModel();
        s.setServiceItemId(9L);
        s.setName("Envelope(A4)");
        s.setDisplayName(s.getName());
        sv.add(s);

        s = new ServiceItemViewModel();
        s.setServiceItemId(10L);
        s.setName("Fax Paper");
        s.setDisplayName(s.getName());
        sv.add(s);

        s = new ServiceItemViewModel();
        s.setServiceItemId(11L);
        s.setName("Glasses (Wine)");
        s.setDisplayName(s.getName());
        sv.add(s);

        s = new ServiceItemViewModel();
        s.setServiceItemId(12L);
        s.setName("Hair Dryer");
        s.setDisplayName(s.getName());
        sv.add(s);

        List<ServiceItemViewModel> l = new ArrayList<ServiceItemViewModel>();
        for (ServiceItemViewModel m : sv) {
            if (m.getName().contains(itemDescTerm)) {
                l.add(m);
            }
        }

        return l;
    }

    public TopServiceItemResponse getTopServiceItems(Context ctx) {

        TopServiceItemResponse job = new TopServiceItemResponse();
        List<ServiceItemViewModel> sv = new ArrayList<ServiceItemViewModel>();

        if (McConstants.OFFLINE_MODE) {
            sv = getOfflineTopServiceItems();
            job.setServiceItems(sv);
            return job;
        }

        String methodName = GET_TOP_SERVICE_ITEM;

        SoapObject request = new SoapObject(NAMESPACE, methodName);
        request.addProperty("UserID", SessionContext.getInstance().getFcsUserId());

        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "getTopServiceItemResponse");

        if (response != null) {

            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject result = (SoapObject) response.getProperty(0);
                if (result.getPropertyCount() > 0 && result.getProperty(0) instanceof SoapObject) {
                    SoapObject res = (SoapObject) result.getProperty(0);
                    for (int i = 0; i < res.getPropertyCount(); i++) {
                        if (res.getProperty(i) instanceof SoapObject) {
                            SoapObject node = (SoapObject) res.getProperty(i);
                            ServiceItemViewModel s = new ServiceItemViewModel();
                            s.setServiceItemId(SoapHelper.getLongProperty(node, "ID", -1L));
                            s.setName(SoapHelper.getStringProperty(node, "Name", ""));
                            s.setDisplayName(s.getName());
                            sv.add(s);
                        }
                    }
                }
            }
            job.setServiceItems(sv);

            return job;
        }
        return null;

    }

    private List<ServiceItemViewModel> getOfflineTopServiceItems() {
        List<ServiceItemViewModel> sv = new ArrayList<ServiceItemViewModel>();

        ServiceItemViewModel s = new ServiceItemViewModel();
        s.setServiceItemId(1L);
        s.setName("A/C too cold");
        s.setDisplayName(s.getName());
        sv.add(s);

        s = new ServiceItemViewModel();
        s.setServiceItemId(2L);
        s.setName("A/C too hot");
        s.setDisplayName(s.getName());
        sv.add(s);

        s = new ServiceItemViewModel();
        s.setServiceItemId(3L);
        s.setName("Adaptor");
        s.setDisplayName(s.getName());
        sv.add(s);

        s = new ServiceItemViewModel();
        s.setServiceItemId(4L);
        s.setName("Bath Towel");
        s.setDisplayName(s.getName());
        sv.add(s);

        s = new ServiceItemViewModel();
        s.setServiceItemId(5L);
        s.setName("Water Leaking");
        s.setDisplayName(s.getName());
        sv.add(s);

        s = new ServiceItemViewModel();
        s.setServiceItemId(6L);
        s.setName("Floor Dirty");
        s.setDisplayName(s.getName());
        sv.add(s);

        return sv;
    }

    public Boolean ackJob(Context ctx, String jobId) {

        if (McConstants.OFFLINE_MODE) {
            return true;
        }

        String methodName = ACK_JOBS;

        SoapObject request = new SoapObject(NAMESPACE, methodName);
        request.addProperty("userID", SessionContext.getInstance().getFcsUserId());
        request.addProperty("password", SessionContext.getInstance().getPassword());
        request.addProperty("jobID", jobId);

        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "ackJobsResponse");

        Boolean result = false;
        if (response != null) {

            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    SoapObject res = (SoapObject) soapresult.getProperty(0);
                    String returncODE = SoapHelper.getStringProperty(res, "ReturnCode", "1");
                    if ("0".equals(returncODE)) {
                        result = true;
                    }
                }
            }
        }
        return result;
    }

    public Boolean updateInspectedJob(String JobID, String IsInspected, String serviceTypeId) {
        if (McConstants.OFFLINE_MODE) {
            return true;
        }

        String methodName = UPDATE_INSPECTED_JOB;

        SoapObject request = new SoapObject(NAMESPACE, methodName);

        SoapObject o = new SoapObject(NAMESPACE, "UpdateInspectedJobRequest");

        o.addProperty("UserID", SessionContext.getInstance().getFcsUserId());
        o.addProperty("JobID", JobID);
        o.addProperty("IsInspected", IsInspected);
        o.addProperty("ServiceTypeID", serviceTypeId);
        request.addSoapObject(o);

        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "UpdateInspectedJobResponse");


        Boolean result = false;
        if (response != null) {

            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    SoapObject res = (SoapObject) soapresult.getProperty(0);
                    String returncODE = SoapHelper.getStringProperty(res, "ReturnCode", "1");
                    if ("0".equals(returncODE)) {
                        result = true;
                    }
                }
            }
        }
        return result;

    }

    public Boolean updateReAssignFailedInspectionJob(String JobID, String serviceTypeId, String ServiceItemCode, String LocationCode,
                                                     String AssignedByID, String AssignedToID, String AssignedToName, String AssignedToDepartment, Date Deadline) {
        if (McConstants.OFFLINE_MODE) {
            return true;
        }

        String methodName = UPDATE_REASSIGN_FAILED_INSPECTIONJOB;

        SoapObject request = new SoapObject(NAMESPACE, methodName);

        SoapObject o = new SoapObject(NAMESPACE, "UpdateReAssignFailedInspectionJobRequest");

        o.addProperty("JobID", JobID);
        o.addProperty("ServiceTypeID", serviceTypeId);
        o.addProperty("ServiceItemCode", ServiceItemCode);
        o.addProperty("LocationCode", LocationCode);
        o.addProperty("AssignedByID", AssignedByID);
        o.addProperty("AssignedToID", AssignedToID);
        o.addProperty("AssignedToName", AssignedToName);
        o.addProperty("AssignedToDepartment", AssignedToDepartment);
        if (Deadline != null) {
            String schedule = DateTimeHelper.formatDateToStr(Deadline, DateTimeHelper.FORMAT_DATETIME_FULL_UPDATE_REASSIGN, "");
            o.addProperty("Deadline", schedule);
        } else {
            o.addProperty("Deadline", "");
        }

        request.addSoapObject(o);

        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "UpdateReAssignFailedInspectionJobResponse");

        Boolean result = false;
        if (response != null) {

            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    SoapObject res = (SoapObject) soapresult.getProperty(0);
                    String returncODE = SoapHelper.getStringProperty(res, "ReturnCode", "1");
                    if ("0".equals(returncODE)) {
                        result = true;
                    }
                }
            }
        }
        return result;

    }


    public Boolean updateAcceptJob(Context ctx, String jobId, String AssignedToID, String AssignedToName, String AssignedToDepartment) {

        if (McConstants.OFFLINE_MODE) {
            return true;
        }

        String methodName = UPDATE_ACCEPT_JOB;

        SoapObject request = new SoapObject(NAMESPACE, methodName);

        SoapObject o = new SoapObject(NAMESPACE, "UpdateAcceptJobRequest");
        o.addProperty("JobID", jobId);
        o.addProperty("AssignedToID", AssignedToID);
        o.addProperty("AssignedToName", AssignedToName);
        o.addProperty("AssignedToDepartment", AssignedToDepartment);
        request.addSoapObject(o);

        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "UpdateAcceptJobResponse");

        Boolean result = false;
        if (response != null) {

            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    SoapObject res = (SoapObject) soapresult.getProperty(0);
                    String returncODE = SoapHelper.getStringProperty(res, "ReturnCode", "1");
                    if ("0".equals(returncODE)) {
                        result = true;
                    }
                }
            }
        }
        return result;
    }

    public GetEquipmentsResponse getEquipments(Context ctx, String LocationCode) {

        if (McConstants.OFFLINE_MODE) {
            return null;
        }

        String methodName = GET_EQUIPMENTS;

        SoapObject request = new SoapObject(NAMESPACE, methodName);

        SoapObject o = new SoapObject(NAMESPACE, "GetEquipmentsRequest");
        o.addProperty("UserID", SessionContext.getInstance().getFcsUserId());
        o.addProperty("LocationCode", LocationCode);
        o.addProperty("EquipmentID", "");
        request.addSoapObject(o);


        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "GetEquipmentsResponse");

        if (response != null) {
            GetEquipmentsResponse resp = null;
            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    if (soapresult.hasProperty("ResponseStatus") && soapresult.hasProperty("EquipmentDetailsListing")) {
                        resp = new GetEquipmentsResponse(soapresult);
                    }
                }
            }
            return resp;
        }
        return null;

    }

    public Boolean ackEngineeringJobs(Context ctx, String jobId) {

        if (McConstants.OFFLINE_MODE) {
            return true;
        }

        String methodName = GET_ACK_ENGINEERING_JOBS;

        SoapObject request = new SoapObject(NAMESPACE, methodName);
        request.addProperty("userID", SessionContext.getInstance().getFcsUserId());
        request.addProperty("password", SessionContext.getInstance().getPassword());
        request.addProperty("jobID", jobId);

        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "ackEngineeringJobsResponse");

        Boolean result = false;
        if (response != null) {

            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    SoapObject res = (SoapObject) soapresult.getProperty(0);
                    String returncODE = SoapHelper.getStringProperty(res, "ReturnCode", "1");
                    if ("0".equals(returncODE)) {
                        result = true;
                    }
                }
            }
        }
        return result;
    }

    public Boolean postUpdateNotificationStatus(Context ctx, String jobNo, int notificationType, int readStatus) {

        if (McConstants.OFFLINE_MODE) {
            return true;
        }

        String methodName = "PostUpdateNotificationStatus";

        SoapObject request = new SoapObject(NAMESPACE, methodName);
        SoapObject o = new SoapObject(NAMESPACE, "PostUpdateNotificationStatusRequest");
        o.addProperty("UserID", SessionContext.getInstance().getFcsUserId());
        o.addProperty("JobNo", jobNo);
        o.addProperty("NotificationType", notificationType);
        o.addProperty("StatusID", readStatus);

        request.addSoapObject(o);
        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "PostUpdateNotificationStatusResponse");

        Boolean result = false;
        if (response != null) {

            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    SoapObject res = (SoapObject) soapresult.getProperty(0);
                    String returncODE = SoapHelper.getStringProperty(res, "ReturnCode", "1");
                    if ("0".equals(returncODE)) {
                        result = true;
                    }
                }
            }
        }
        return result;
    }

    public Boolean postUpdateEngineeringJobDetails(Context ctx, List<PostUpdateEngineeringJobDetailsRequestJobDetails> _list) {

        if (McConstants.OFFLINE_MODE) {
            return true;
        }

        String methodName = POST_UPDATE_ENGINEERING_JOBDETAILS;

        SoapObject request = new SoapObject(NAMESPACE, methodName);
        SoapObject o = new SoapObject(NAMESPACE, "PostUpdateEngineeringJobDetailsRequest");
        SoapObject list = new SoapObject(NAMESPACE, "PostUpdateEngineeringJobDetailsRequestJobDetailsListing");
        for (int i = 0; i < _list.size(); i++) {
            SoapObject detail = new SoapObject(NAMESPACE, "PostUpdateEngineeringJobDetailsRequestJobDetails");
            detail.addProperty("UserID", SessionContext.getInstance().getFcsUserId());
            detail.addProperty("JobNo", _list.get(i).getJobNo());
            detail.addProperty("ActionTaken", _list.get(i).getActionTaken());
            detail.addProperty("EquipmentID", _list.get(i).getEquipmentID());
            detail.addProperty("PMConditionID", _list.get(i).getPmConditionID());
            detail.addProperty("ManHours", _list.get(i).getManHours());
            detail.addProperty("Cost", _list.get(i).getCost());
            if (_list.get(i).getDeadline() != null) {
                detail.addProperty("Deadline", DateTimeHelper.getStrDateByTimeZone(_list.get(i).getDeadline(), DateTimeHelper.FORMAT_DATETIME_FULL_2));
            }
            list.addSoapObject(detail);
        }
        o.addSoapObject(list);
        request.addSoapObject(o);
        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "PostUpdateEngineeringJobDetailsResponse");

        Boolean result = false;
        if (response != null) {

            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    SoapObject res = (SoapObject) soapresult.getProperty(0);
                    String returncODE = SoapHelper.getStringProperty(res, "ReturnCode", "1");
                    if ("0".equals(returncODE)) {
                        result = true;
                    }
                }
            }
        }
        return result;
    }

    public Boolean PostUpdateJobDetails(Context ctx, List<PostUpdateEngineeringJobDetailsRequestJobDetails> _list) {

        if (McConstants.OFFLINE_MODE) {
            return true;
        }

        String methodName = POST_UPDATE_JOBDETAILS;

        SoapObject request = new SoapObject(NAMESPACE, methodName);
        SoapObject o = new SoapObject(NAMESPACE, "PostUpdateJobDetailsRequest");
        SoapObject list = new SoapObject(NAMESPACE, "PostUpdateJobDetailsRequestJobDetailsListing");
        for (int i = 0; i < _list.size(); i++) {
            SoapObject detail = new SoapObject(NAMESPACE, "PostUpdateJobDetailsRequestJobDetails");
            detail.addProperty("UserID", SessionContext.getInstance().getFcsUserId());
            detail.addProperty("JobNo", _list.get(i).getJobNo());
            detail.addProperty("ActionTaken", _list.get(i).getActionTaken());
            detail.addProperty("EquipmentID", _list.get(i).getEquipmentID());
            detail.addProperty("PMConditionID", _list.get(i).getPmConditionID());
            detail.addProperty("ManHours", _list.get(i).getManHours());
            detail.addProperty("Cost", _list.get(i).getCost());
            if (_list.get(i).getDeadline() != null) {
                detail.addProperty("Deadline", DateTimeHelper.getStrDateByTimeZone(_list.get(i).getDeadline(), DateTimeHelper.FORMAT_DATETIME_FULL_2));
            }
            list.addSoapObject(detail);
        }
        o.addSoapObject(list);
        request.addSoapObject(o);
        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "PostUpdateJobDetailsResponse");

        Boolean result = false;
        if (response != null) {

            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    SoapObject res = (SoapObject) soapresult.getProperty(0);
                    String returncODE = SoapHelper.getStringProperty(res, "ReturnCode", "1");
                    if ("0".equals(returncODE)) {
                        result = true;
                    }
                }
            }
        }
        return result;
    }


    public Boolean updateChecklist(Context ctx, List<EngineeringChecklistDetails> _list, String jobId) {

        if (McConstants.OFFLINE_MODE) {
            return true;
        }

        String methodName = UPDATE_CHECKLIST;

        SoapObject request = new SoapObject(NAMESPACE, methodName);
        SoapObject o = new SoapObject(NAMESPACE, "UpdateChecklistRequest");
        o.addProperty("JobID", jobId);

        SoapObject list = new SoapObject(NAMESPACE, "ChecklistValues");
        for (int i = 0; i < _list.size(); i++) {
            EngineeringChecklistDetails detailChecklist = _list.get(i);
            SoapObject detail = new SoapObject(NAMESPACE, "ChecklistValues");
            detail.addProperty("ContentID", detailChecklist.getContentId());
            detail.addProperty("AnswerID", _list.get(i).getAnswerID());
            detail.addProperty("InputAnswer", _list.get(i).getInputAnswer());
            detail.addProperty("RemarksAnswer", _list.get(i).getRemarksAnswer());
            list.addProperty("ChecklistValues", detail);
        }
        o.addProperty("ChecklistValues", list);

        request.addSoapObject(o);
        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "UpdateChecklistResponse");
        Boolean result = false;
        if (response != null) {

            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    SoapObject res = (SoapObject) soapresult.getProperty(0);
                    String returncODE = SoapHelper.getStringProperty(res, "ReturnCode", "1");
                    if ("0".equals(returncODE)) {
                        result = true;
                    }
                }
            }
        }
        return result;
    }

    public GetNotificationStatusResponse getNotificationStatus(Context ctx, List<JobModel> listJobInfo) {

        if (McConstants.OFFLINE_MODE) {
            return null;
        }

        String methodName = "GetNotificationStatus";

        SoapObject request = new SoapObject(NAMESPACE, methodName);
        SoapObject o = new SoapObject(NAMESPACE, "GetNotificationStatusRequest");
        o.addProperty("UserID", SessionContext.getInstance().getFcsUserId());

        SoapObject oListing = new SoapObject(NAMESPACE, "NotificationStatusRequestJobDetailsListing");

        for (JobModel jm : listJobInfo) {
            SoapObject oDetails = new SoapObject(NAMESPACE, "NotificationStatusRequestJobDetails");
            oDetails.addProperty("JobNo", jm.getJobNum());
            oDetails.addProperty("JobType", jm.getNotificationType());
            oListing.addProperty("NotificationStatusRequestJobDetails", oDetails);
        }

        o.addProperty("NotificationStatusRequestJobDetailsListing", oListing);
        request.addSoapObject(o);
        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "GetNotificationStatusResponse");

        if (response != null) {
            GetNotificationStatusResponse resp = null;
            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    if (soapresult.hasProperty("ResponseStatus") && soapresult.hasProperty("NotificationStatusResponseJobDetailsListing")) {
                        resp = new GetNotificationStatusResponse(soapresult);
                    }
                }
            }
            return resp;
        }
        return null;
    }

    public Boolean updateJobRemark(Context ctx, long jobId, String remarks) {

        if (McConstants.OFFLINE_MODE) {
            return true;
        }

        String methodName = UPDATE_JOB_REMARK;

        SoapObject request = new SoapObject(NAMESPACE, methodName);
        request.addProperty("UserID", SessionContext.getInstance().getFcsUserId());
        request.addProperty("password", SessionContext.getInstance().getPassword());
        request.addProperty("jobID", jobId);
        request.addProperty("remark", remarks);

        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "updateJobRemarkResponse");
        Boolean result = false;
        if (response != null) {
            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    SoapObject res = (SoapObject) soapresult.getProperty(0);
                    String returncODE = SoapHelper.getStringProperty(res, "ReturnCode", "1");
                    if ("0".equals(returncODE)) {
                        result = true;
                    }
                }
            }
        }
        return result;
    }

    public Boolean updateEngineeringJob(Context ctx, long jobId, String action ,boolean IsPMJob) {

        if (McConstants.OFFLINE_MODE) {
            return true;
        }

        String methodName = UPDATE_ENGINEERING_JOB;

        SoapObject request = new SoapObject(NAMESPACE, methodName);
        SoapObject request1 = new SoapObject(NAMESPACE, "xmln");
        SoapObject request2 = new SoapObject(NAMESPACE, "UpdateJob");
        request2.addProperty("SystemDateTime", DateTimeHelper.getStrDateByTimeZone(new Date(), DateTimeHelper.FORMAT_DATETIME_FULL_2));
        request2.addProperty("Sender", SessionContext.getInstance().getUserDepartment());
        request2.addProperty("UserName", SessionContext.getInstance().getUsername());
        request2.addProperty("Password", SessionContext.getInstance().getPassword());
        request2.addProperty("JobID", jobId);
        request2.addProperty("Action", action);
        if (IsPMJob) {
            request2.addProperty("IsPMJob", "1");
        }
        request1.addSoapObject(request2);
        request.addSoapObject(request1);

        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "UpdateJobResponse");
        Boolean result = false;
        if (response != null) {
            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    SoapObject res = (SoapObject) soapresult.getProperty(0);
                    String returncODE = SoapHelper.getStringProperty(res, "ReturnCode", "1");
                    if ("0".equals(returncODE)) {
                        result = true;
                    }
                }
            }
        }
        return result;
    }

    public Boolean updateEngineeringJobRemark(Context ctx, long jobId, String remarks) {

        if (McConstants.OFFLINE_MODE) {
            return true;
        }

        String methodName = UPDATE_ENGINEERING_JOB_REMARK;

        SoapObject request = new SoapObject(NAMESPACE, methodName);
        request.addProperty("UserID", SessionContext.getInstance().getFcsUserId());
        request.addProperty("password", SessionContext.getInstance().getPassword());
        request.addProperty("jobID", jobId);
        request.addProperty("remark", remarks);

        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "updateEngineeringJobRemarkResponse");
        Boolean result = false;
        if (response != null) {
            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    SoapObject res = (SoapObject) soapresult.getProperty(0);
                    String returncODE = SoapHelper.getStringProperty(res, "ReturnCode", "1");
                    if ("0".equals(returncODE)) {
                        result = true;
                    }
                }
            }
        }
        return result;
    }

    public Date closeJobs(Context ctx, long jobId, String jobNo, String eSignatureName, File picture, File eSignature) {

        if (McConstants.OFFLINE_MODE) {
            return DateTimeHelper.getDateByTimeZone(new Date());
        }
        String methodName = CLOSE_JOBS;
//        if (eSignature != null) {
//             methodName = CLOSE_JOBS2;
//        }
        SoapObject request = new SoapObject(NAMESPACE, methodName);
        request.addProperty("userID", SessionContext.getInstance().getFcsUserId());
        request.addProperty("password", SessionContext.getInstance().getPassword());
        request.addProperty("jobID", jobId);
//        request.addProperty("jobNo", jobNo);
//        request.addProperty("eSignatureName", eSignatureName);
//        if (picture != null) {
//            request.addProperty("picture", McUtils.encodeFileToBase64(picture));
//            request.addProperty("pictureExtension", McUtils.getExtension(picture));
//        }
//        if (eSignature != null) {
//            request.addProperty("eSignature", McUtils.encodeFileToBase64(eSignature));
//            request.addProperty("eSignatureExtension", McUtils.getExtension(eSignature));
//        }

//        request.addProperty("ContentExtension",   ncodeFileToBase64(file));
//         <userID>string</userID>
//      <password>string</password>
//      <jobID>string</jobID>
//      <jobNo>string</jobNo>
//      <eSignatureName>string</eSignatureName>
//      <picture>base64Binary</picture>
//      <pictureExtension>string</pictureExtension>
//      <eSignature>base64Binary</eSignature>
//      <eSignatureExtension>string</eSignatureExtension>
//
        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "closeJobsResponse");

        if (response != null) {
            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    SoapObject res = (SoapObject) soapresult.getProperty(0);
                    String returncODE = SoapHelper.getStringProperty(res, "ReturnCode", "1");
                    if ("0".equals(returncODE)) {
                        return SoapHelper.getDateProperty(res, "SystemDateTime", DateTimeHelper.FORMAT_DATETIME_FULL_DASH, null);
                    }
                }
            }
        }
        return null;
    }

    public Date closeJobs2(Context ctx, long jobId, String jobNo, String eSignatureName, File picture, File eSignature, boolean IsPMJob) {

        if (McConstants.OFFLINE_MODE) {
            return DateTimeHelper.getDateByTimeZone(new Date());
        }
        if (jobNo == null) {
            jobNo = "";
        }
        if (eSignatureName == null) {
            eSignatureName = "";
        }
        String methodName = CLOSE_JOBS2;
        SoapObject request = new SoapObject(NAMESPACE, methodName);
        request.addProperty("userID", SessionContext.getInstance().getFcsUserId());
        request.addProperty("password", SessionContext.getInstance().getPassword());
        request.addProperty("jobID", jobId);
        request.addProperty("jobNo", jobNo);
        request.addProperty("eSignatureName", eSignatureName);
        if (IsPMJob) {
            request.addProperty("IsPMJob", "1");
        }
        if (picture != null) {
            request.addProperty("picture", McUtils.encodeFileToBase64(picture));
            request.addProperty("pictureExtension", McUtils.getExtension(picture));
        }
        if (eSignature != null) {
            request.addProperty("eSignature", McUtils.encodeFileToBase64(eSignature));
            request.addProperty("eSignatureExtension", McUtils.getExtension(eSignature));
        }

        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "closeJobs2Response");

        if (response != null) {
            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    SoapObject res = (SoapObject) soapresult.getProperty(0);
                    String returncODE = SoapHelper.getStringProperty(res, "ReturnCode", "1");
                    if ("0".equals(returncODE)) {
                        return SoapHelper.getDateProperty(res, "SystemDateTime", DateTimeHelper.FORMAT_DATETIME_FULL_DASH, null);
                    }
                }
            }
        }
        return null;
    }


    public Date closeEngineeringJobs(Context ctx, long jobId) {

        if (McConstants.OFFLINE_MODE) {
            return DateTimeHelper.getDateByTimeZone(new Date());
        }

        String methodName = CLOSE_ENGINEERING_JOBS;

        SoapObject request = new SoapObject(NAMESPACE, methodName);
        request.addProperty("userID", SessionContext.getInstance().getFcsUserId());
        request.addProperty("password", SessionContext.getInstance().getPassword());
        request.addProperty("jobID", jobId);

        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "closeEngineeringJobsResponse");

        if (response != null) {
            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    SoapObject res = (SoapObject) soapresult.getProperty(0);
                    String returncODE = SoapHelper.getStringProperty(res, "ReturnCode", "1");
                    if ("0".equals(returncODE)) {
                        return SoapHelper.getDateProperty(res, "SystemDateTime", DateTimeHelper.FORMAT_DATETIME_FULL_DASH, null);
                    }
                }
            }
        }
        return null;
    }


    public CreateIndividualMsgResponse sendAdHocMsg(Context ctx) {

        CreateIndividualMsgResponse resp = new CreateIndividualMsgResponse();
        Date dateNow = DateTimeHelper.getDateByTimeZone(new Date());
        if (McConstants.OFFLINE_MODE) {
            resp.setReturnCode("0");
            resp.setSuccess(true);
            resp.setAdHocMsgId("005001");
            resp.setCreated(dateNow);
            return resp;
        }

        String methodName = CREATE_AD_HOC;
        SoapObject message = new SoapObject(NAMESPACE, methodName);
        message.addProperty("Creator", SessionContext.getInstance().getFcsUserId());
        message.addProperty("Sender", SessionContext.getInstance().getFcsUserId());

        StringBuffer recipient = new StringBuffer();
        for (RunnerModel runner : MessageWrapper.getInstance().getRunners()) {
            if (recipient.length() > 0) {
                recipient.append(";");
            }
            recipient.append(runner.getId());
        }
        message.addProperty("Receipient", recipient.toString());
        message.addProperty("msg", MessageWrapper.getInstance().getContent());

        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(message, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "createAdHocResponse");

        if (response != null) {
            SoapObject result = getResultSoapObject(response);
            resp.setReturnCode(SoapHelper.getStringProperty(result, "ReturnCode", ""));
            resp.setSuccess("0".equals(resp.getReturnCode()));
            resp.setAdHocMsgId(SoapHelper.getStringProperty(result, "AdHocMsgID", ""));
            resp.setCreated(SoapHelper.getDateProperty(result, "SystemDateTime", DateTimeHelper.FORMAT_DATETIME_FULL_DASH, dateNow));
            return resp;
        }

        return null;
    }

    private static final SimpleDateFormat adhocSd = new SimpleDateFormat(DateTimeHelper.FORMAT_DATEYEAR);

    public FindMsgResponse getAcHocList(Context ctx, Date sendDate) {

        FindMsgResponse resp = new FindMsgResponse();
        List<MessageModel> guests = new ArrayList<MessageModel>();

        if (McConstants.OFFLINE_MODE) {

            guests.addAll(getOfflineMyMessages(false));
            resp.setMessageList(guests);
            return resp;
        }

        String methodName = GET_AC_HOC_LIST;
        SoapObject request = new SoapObject(NAMESPACE, methodName);
        request.addProperty("UserID", SessionContext.getInstance().getFcsUserId());
        request.addProperty("SendDate", DateTimeHelper.getStrDateFromDateByTimeZone(sendDate, DateTimeHelper.FORMAT_DATEYEAR));
        request.addProperty("AckDate", DateTimeHelper.getStrDateFromDateByTimeZone(sendDate, DateTimeHelper.FORMAT_DATEYEAR));

        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "getAcHocListResponse");

        if (response != null) {

            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject result = (SoapObject) response.getProperty(0);
                if (result.getPropertyCount() > 0 && result.getProperty(0) instanceof SoapObject) {
                    SoapObject res = (SoapObject) result.getProperty(0);
                    for (int i = 0; i < res.getPropertyCount(); i++) {
                        if (res.getProperty(i) instanceof SoapObject) {
                            SoapObject node = (SoapObject) res.getProperty(i);
                            MessageModel m = converToAdhoc(node);
                            guests.add(m);
                        }
                    }
                }
            }
            resp.setMessageList(guests);
            return resp;
        }

        return null;
    }

    private List<MessageModel> getOfflineMyMessages(Boolean showPendingOnly) {
        List<MessageModel> guests = new ArrayList<MessageModel>();

        MessageModel m = null;
        Date dateNow = DateTimeHelper.getDateByTimeZone(new Date());
        if (showPendingOnly == null || !showPendingOnly.booleanValue()) {

            m = new MessageModel();
            m.setCreatedDate(dateNow);
            m.setMsgDoneId(900001L);
            m.setFromUsername("David Chan");
            m.setTitle("Typhoon Signal 8 at 16:30");
            m.setAcknowledged(true);
            m.setAckDate(dateNow);
            guests.add(m);
        }

        m = new MessageModel();
        m.setCreatedDate(dateNow);
        m.setMsgDoneId(900002L);
        m.setFromUsername("David Chan");
        m.setTitle("VVIP8 is going to arrive at lobby 17:00");
        m.setAcknowledged(false);
        guests.add(m);

        m = new MessageModel();
        m.setCreatedDate(dateNow);
        m.setMsgDoneId(900003L);
        m.setFromUsername("David Chan");
        m.setTitle("RN1005 is checked out at 18:00");
        m.setAcknowledged(false);
        guests.add(m);
        return guests;
    }

    public MessageModel getAcHocDetail(Context ctx, long usrerId, String adhocId) {

        String methodName = GET_ADHOC_DETAIL;
        SoapObject request = new SoapObject(NAMESPACE, methodName);
        request.addProperty("UserID", usrerId);
        request.addProperty("AdHocID", adhocId);

        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "GetAdhocDetailResponse");

        if (response != null) {
            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject node = (SoapObject) response.getProperty(0);

                if (node.getPropertyCount() > 0 && node.getProperty(0) instanceof SoapObject) {
                    SoapObject node1 = (SoapObject) node.getProperty(0);

                    if (node1.hasProperty("AdHocDetail") && node1.getProperty("AdHocDetail") instanceof SoapObject) {

                        SoapObject soap = (SoapObject) node1.getProperty("AdHocDetail");
                        MessageModel m = converToAdhoc(soap);

                        return m;
                    }

                }

            }

        }

        return null;
    }

    private MessageModel converToAdhoc(SoapObject node) {

        MessageModel m = new MessageModel();
        m.setCreatedDate(SoapHelper.getDateProperty(node, "CreateDate", DateTimeHelper.FORMAT_DATETIME_NONSECOND, DateTimeHelper.getDateByTimeZone(new Date())));
        m.setMsgDoneId(SoapHelper.getLongProperty(node, "MsgID", -1L));
        m.setFromUsername(SoapHelper.getStringProperty(node, "CreateBy", ""));
        m.setTitle(SoapHelper.getStringProperty(node, "Detail", ""));
        Date ackDate = SoapHelper.getDateProperty(node, "AckDate", DateTimeHelper.FORMAT_DATE_AA, null);
        m.setAckDate(ackDate);
        if (null != ackDate) {
            m.setAcknowledged(true);
        } else {
            m.setAcknowledged(false);
        }
        return m;

    }

    public AckAdHocResponse ackAdHoc(Context ctx, String msgId) {

        AckAdHocResponse resp = new AckAdHocResponse();

        if (McConstants.OFFLINE_MODE) {
            resp.setReturnCode("0");
            resp.setSuccess("0".equals(resp.getReturnCode()));
            return resp;
        }

        String methodName = ACK_AD_HOC;
        SoapObject request = new SoapObject(NAMESPACE, methodName);
        request.addProperty("userID", SessionContext.getInstance().getFcsUserId());
        request.addProperty("password", SessionContext.getInstance().getPassword());
        request.addProperty("jobID", msgId);

        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "AckAdHocResponse");

        if (response != null) {
            SoapObject result = getResultSoapObject(response);
            resp.setReturnCode(SoapHelper.getStringProperty(result, "ReturnCode", "1"));
            resp.setSuccess("0".equals(resp.getReturnCode()));
            return resp;
        }

        return null;
    }

    public GetTemplateListResponse getMessageTemplates(Context ctx) {

        GetTemplateListResponse resp = new GetTemplateListResponse();
        List<Template> templates = new ArrayList<Template>();

        if (McConstants.OFFLINE_MODE) {

            Template t = new Template();
            t.setTemplateId(-1L);
            t.setTemplateName("Typhoon Sign#");
            t.setDetail(t.getTemplateName());
            templates.add(t);

            t = new Template();
            t.setTemplateId(-1L);
            t.setTemplateName("Black Rain");
            t.setDetail(t.getTemplateName());
            templates.add(t);

            t = new Template();
            t.setTemplateId(-1L);
            t.setTemplateName("Fire False Alarm");
            t.setDetail(t.getTemplateName());
            templates.add(t);

            resp.setTemplates(templates);
            return resp;
        }

        String methodName = GET_MESSAGE_TEMPLATES;
        SoapObject request = new SoapObject(NAMESPACE, methodName);
        request.addProperty("UserID", SessionContext.getInstance().getFcsUserId());

        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "getMessageTemplatesResponse");

        if (response != null) {

            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject result = (SoapObject) response.getProperty(0);
                if (result.getPropertyCount() > 0 && result.getProperty(0) instanceof SoapObject) {
                    SoapObject res = (SoapObject) result.getProperty(0);
                    for (int i = 0; i < res.getPropertyCount(); i++) {
                        if (res.getProperty(i) instanceof SoapObject) {
                            SoapObject node = (SoapObject) res.getProperty(i);
                            Template t = new Template();
                            t.setTemplateId(-1L);
                            t.setTemplateName(SoapHelper.getStringProperty(node, "Code", ""));
                            t.setDetail(SoapHelper.getStringProperty(node, "Detail", ""));
                            templates.add(t);
                        }
                    }
                }
            }

            resp.setTemplates(templates);
            return resp;
        }

        return null;
    }

    public boolean updateToken(Context ctx, long userId, String registrationId) {

        if (McConstants.OFFLINE_MODE) {
            return true;
        }

        String methodName = UPDATE_TOKEN;
        SoapObject request = new SoapObject(NAMESPACE, methodName);
        request.addProperty("userID", userId);
        request.addProperty("newToken", registrationId);
        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "updateTokenResponse");
        if (response != null) {
            return true;
        }
        return false;
    }

//	private ModuleLicense getModuleLicense(Context ctx, long userId) {
//		String methodName = GET_MODULE_LICENSE;
//		SoapObject request = new SoapObject(NAMESPACE, methodName);
//		request.addProperty("UserID", userId);
//		String soapAction = NAMESPACE + methodName;
//		SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
//				"getModuleLicenseResponse");
//
//		if (response != null) {
//			ModuleLicense resp = null;
//			SoapObject result = getResultSoapObject(response, GET_MODULE_LICENSE);
//			resp = getModuleLicenseFromSoapObject(result);
//			return resp;
//		}
//		return null;
//	}
//
//	public ModuleLicense getModuleLicense(Context ctx, long userId, boolean version2_1) {
//
//		if (McConstants.OFFLINE_MODE) {
//			ModuleLicense resp = new ModuleLicense();
//			resp.setMyJob(true);
//			resp.setMyMessage(true);
//			resp.setJobSearch(true);
//			resp.setSendMessage(true);
//			resp.setJobRequest(true);
//			if (resp.isJobRequest()) {
//				resp.setGuestRequest(true);
//				resp.setInterDeptRequest(true);
//				resp.setNonGuestRequest(true);
//			}
//			resp.setGuestDetail(true);
//			resp.setMinibar(true);
//			resp.setRoomStatus(true);
//			resp.setFavorite(true);
//
//			GuestHistoryConfig config = new GuestHistoryConfig();
//			config.setDay1(true);
//			config.setWeek1(true);
//			config.setMonth1(true);
//			config.setMonth3(true);
//			config.setMonth6(true);
//			config.setYear1(true);
//			PrefsUtil.setGuestHistoryConfig(ctx, config);
//
//			return resp;
//		}
//
//		if (!version2_1) {
//			return getModuleLicense(ctx, userId);
//		} else {
//			String methodName = GETM_CONNECT_USER_CONFIG;
//			SoapObject request = new SoapObject(NAMESPACE, methodName);
//			request.addProperty("UserID", userId);
//			String soapAction = NAMESPACE + methodName;
//			SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction,
//					NAMESPACE, "GetmConnectUserConfigResponse");
//
//			if (response != null) {
//				ModuleLicense resp = null;
//				SoapObject result = getResultSoapObject(response, "Modules");
//				resp = getModuleLicenseFromSoapObject(result);
//				SoapObject guestHistoryRange = getResultSoapObject(response, "GuestHistoryRange");
//				getGuestHistoryRange(ctx, guestHistoryRange);
//				return resp;
//			}
//			return null;
//		}
//	}
//
//	private ModuleLicense getModuleLicenseFromSoapObject(SoapObject result) {
//		ModuleLicense resp = new ModuleLicense();
//		String yes = "YES";
//		resp.setMyJob(yes.equalsIgnoreCase(SoapHelper.getStringProperty(result, "MyJob", "NO")));
//		resp.setMyMessage(yes.equalsIgnoreCase(SoapHelper.getStringProperty(result, "AdHoc", "NO")));
//		resp.setJobSearch(yes.equalsIgnoreCase(SoapHelper.getStringProperty(result, "ViewJob", "NO")));
//		resp.setSendMessage(yes.equalsIgnoreCase(SoapHelper.getStringProperty(result, "SendMessage", "NO")));
//		resp.setJobRequest(yes.equalsIgnoreCase(SoapHelper.getStringProperty(result, "JobRequest", "NO")));
//		if (resp.isJobRequest()) {
//			resp.setGuestRequest(true);
//			resp.setInterDeptRequest(true);
//			resp.setNonGuestRequest(true);
//		}
//		resp.setGuestDetail(yes.equalsIgnoreCase(SoapHelper.getStringProperty(result, "GuestDetail", "NO")));
//		resp.setMinibar(yes.equalsIgnoreCase(SoapHelper.getStringProperty(result, "Minibar", "NO")));
//		resp.setRoomStatus(yes.equalsIgnoreCase(SoapHelper.getStringProperty(result, "RoomStatus", "NO")));
//		resp.setFavorite(yes.equalsIgnoreCase(SoapHelper.getStringProperty(result, "Favourite", "NO")));
//		return resp;
//	}
//
//	private void getGuestHistoryRange(Context context, SoapObject range) {
//		GuestHistoryConfig config = new GuestHistoryConfig();
//		String yes = "YES";
//		config.setDay1(yes.equalsIgnoreCase(SoapHelper.getStringProperty(range, "D1", "NO")));
//		config.setWeek1(yes.equalsIgnoreCase(SoapHelper.getStringProperty(range, "W1", "NO")));
//		config.setMonth1(yes.equalsIgnoreCase(SoapHelper.getStringProperty(range, "M1", "NO")));
//		config.setMonth3(yes.equalsIgnoreCase(SoapHelper.getStringProperty(range, "M3", "NO")));
//		config.setMonth6(yes.equalsIgnoreCase(SoapHelper.getStringProperty(range, "M6", "NO")));
//		config.setYear1(yes.equalsIgnoreCase(SoapHelper.getStringProperty(range, "Y1", "NO")));
//		PrefsUtil.setGuestHistoryConfig(context, config);
//	}

    public List<Department> getDepartmentList(Context ctx) {

        if (McConstants.OFFLINE_MODE) {
            return new ArrayList<Department>();
        }

        List<Department> list = DBHelper.queryDepartments(ctx);
        if (list.size() > 0) {
            return list;
        }

        String methodName = GET_DEPARTMENT_LIST;
        SoapObject request = new SoapObject(NAMESPACE, methodName);
        request.addProperty("UserID", SessionContext.getInstance().getFcsUserId());

        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "getDepartmentListResponse");

        List<Department> deps = new ArrayList<Department>();
        if (response != null) {

            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject result = (SoapObject) response.getProperty(0);
                if (result.getPropertyCount() > 0 && result.getProperty(0) instanceof SoapObject) {
                    SoapObject res = (SoapObject) result.getProperty(0);
                    for (int i = 0; i < res.getPropertyCount(); i++) {
                        if (res.getProperty(i) instanceof SoapObject) {
                            SoapObject node = (SoapObject) res.getProperty(i);
                            Department dep = new Department();
                            dep.setCode("");
                            dep.setDepartmentNameSec("");
                            dep.setDepartmentName(SoapHelper.getStringProperty(node, "Name", "").trim());
                            dep.setDepartmentId(SoapHelper.getLongProperty(node, "ID", -1L));
                            deps.add(dep);
                        }
                    }
                }
            }
            DBHelper.saveDepartmentss(ctx, deps);
        }

        return deps;
    }

    public List<String> getSupportEmailList(Context ctx) {

        String methodName = GET_SUPPORT_EMAIL_ADDRESSES;
        SoapObject request = new SoapObject(NAMESPACE, "GetSupportEmailAddressesRequest");
        request.addProperty("UserID", 0);

        SoapObject req = new SoapObject(NAMESPACE, GET_SUPPORT_EMAIL_ADDRESSES);
        request.addSoapObject(request);

        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(req, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "GetSupportEmailAddressesResponse");

        List<String> deps = new ArrayList<String>();
        if (response != null) {

            SoapObject objs = SoapHelper.getInnerSoapObject(response, 0, "EmailAddressesListing");
            for (int i = 0; i < objs.getPropertyCount(); i++) {
                if (objs.getProperty(i) instanceof SoapObject) {
                    SoapObject node = (SoapObject) objs.getProperty(i);
                    if (node.hasProperty("EmailAddress")) {
                        String email = SoapHelper.getStringProperty(node, "EmailAddress", null);
                        deps.add(email);
                    }
                }
            }

        }

        return deps;
    }

    public String getWebserviceVersion(Context ctx) {

        String methodName = GET_WS_VERSION;
        SoapObject req = new SoapObject(NAMESPACE, methodName);

        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(req, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "GetWSVersionResponse");

        if (response != null) {
            SoapObject objs = SoapHelper.getInnerSoapObject(response, 0, 0);
            return SoapHelper.getStringProperty(objs, "WSVersion", "");
        }
        return "";
    }

    /**
     * in Config page, test whether service url is available
     *
     * @param serviceUrl
     * @return
     */
    private boolean httpsConnect(URLConnection conn, String serviceUrl) {
        try {
            HttpsURLConnection httpConn = (HttpsURLConnection) conn;
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setRequestMethod("GET");
            httpConn.addRequestProperty("user-agent", "Mozilla/5.0");
            httpConn.connect();

            int response = httpConn.getResponseCode();
            if (response == HttpURLConnection.HTTP_OK) {
                InputStream in = httpConn.getInputStream();
                XmlPullParser parser = Xml.newPullParser();
                parser.setInput(in, null);
                int eventType = parser.getEventType();
                while (eventType != XmlPullParser.END_DOCUMENT) {
                    if ("definitions".equals(parser.getName())
                            && "http://eConnect/".equals(parser.getAttributeValue("", "targetNamespace"))) {
                        return true;
                    }
                    eventType = parser.next();
                }
                in.close();
            }
        } catch (Exception e) {
            if (URLUtil.isHttpsUrl(serviceUrl) || URLUtil.isHttpUrl(serviceUrl)) {
                if (serviceUrl.contains("\n") || serviceUrl.contains("\r")) {
                    serviceUrl = serviceUrl.replace("\n", "").replace("\r", "");
                }
                Log.e(TAG, "failed to validate service url: " + serviceUrl, e);
            }
        }
        return false;
    }

    public boolean connect(Context ctx, String serviceUrl) {
        serviceUrl = serviceUrl + "?WSDL";

        URL url = null;
        try {
            url = new URL(serviceUrl);
        } catch (MalformedURLException e) {
            return false;
        }

        if (serviceUrl.startsWith("https:")) {
            try {
                HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
                SSLContext sc = SSLContext.getInstance("TLS");
                sc.init(null, FcsServiceConnectionSE.trustAllCerts, new java.security.SecureRandom());

                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            } catch (Exception e) {
                e.getMessage();
            }
        }

        URLConnection conn = null;
        try {
            conn = url.openConnection();
            conn.setConnectTimeout(McConstants.WS_CONNECT_TIMEOUT);
        } catch (Exception e) {
            return false;
        }

        try {
            if (serviceUrl.startsWith("https:")) {
                return httpsConnect(conn, serviceUrl);
            } else {
                HttpURLConnection httpConn = (HttpURLConnection) conn;
                httpConn.setAllowUserInteraction(false);
                httpConn.setInstanceFollowRedirects(true);
                httpConn.setRequestMethod("GET");
                httpConn.addRequestProperty("user-agent", "Mozilla/5.0");
                httpConn.connect();

                int response = httpConn.getResponseCode();
                if (response == HttpURLConnection.HTTP_OK) {
                    InputStream in = httpConn.getInputStream();
                    XmlPullParser parser = Xml.newPullParser();
                    parser.setInput(in, null);
                    int eventType = parser.getEventType();
                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        if ("definitions".equals(parser.getName())
                                && "http://eConnect/".equals(parser.getAttributeValue("", "targetNamespace"))) {
                            return true;
                        }
                        eventType = parser.next();
                    }
                    in.close();
                }
            }
        } catch (Exception e) {
            if (URLUtil.isHttpsUrl(serviceUrl) || URLUtil.isHttpUrl(serviceUrl)) {
                if (serviceUrl.contains("\n") || serviceUrl.contains("\r")) {
                    serviceUrl = serviceUrl.replace("\n", "").replace("\r", "");
                }
                Log.e(TAG, "failed to validate service url: " + serviceUrl, e);
            }
        }
        return false;
    }

    public Boolean updateRoomStatus(Context ctx, String roomNum, String status) {

        if (McConstants.OFFLINE_MODE) {
            return true;
        }

        String methodName = UPDATE_ROOM_STATUS;

        SoapObject request = new SoapObject(NAMESPACE, methodName);
        request.addProperty("userID", SessionContext.getInstance().getFcsUserId());
        request.addProperty("RoomID", roomNum);
        request.addProperty("Status", status);

        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "UpdateRoomStatusResponse");
        Boolean result = false;
        if (response != null) {
            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    SoapObject res = (SoapObject) soapresult.getProperty(0);
                    String returncODE = SoapHelper.getStringProperty(res, "ReturnCode", "1");
                    if ("0".equals(returncODE)) {
                        result = true;
                    }
                }
            }
        }
        return result;
    }

    public Boolean saveTotalPosting(Context ctx, String roomNo, String amount) {

        if (McConstants.OFFLINE_MODE) {
            return true;
        }

        String methodName = "PostMinibarTotal";
        SoapObject request = new SoapObject(NAMESPACE, methodName);

        SoapObject req = new SoapObject(NAMESPACE, "PostMinibarTotalRequest");
        req.addProperty("UserID", SessionContext.getInstance().getFcsUserId());
        req.addProperty("RoomNo", roomNo);
        req.addProperty("TotalAmount", amount);

        request.addSoapObject(req);

        String soapAction = NAMESPACE + methodName;

        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "PostMinibarTotalResponse");
        Boolean result = false;
        if (response != null) {
            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    SoapObject res = (SoapObject) soapresult.getProperty(0);
                    String returncODE = SoapHelper.getStringProperty(res, "ReturnCode", "1");
                    if ("0".equals(returncODE)) {
                        result = true;
                    }
                }
            }
        }
        return result;
    }

    public Boolean postMinibar(Context ctx, String roomNum, List<MinibarItem> minibarItems) {

        if (McConstants.OFFLINE_MODE) {
            return true;
        }

        String methodName = POST_MINIBAR;
        SoapObject request = new SoapObject(NAMESPACE, methodName);
        String item = "";
        for (MinibarItem e : minibarItems) {
            if ("".equals(item)) {
                item = item + e.getId() + "|" + String.valueOf(e.getQty());
            } else {
                item = item + ";" + e.getId() + "|" + String.valueOf(e.getQty());
            }
        }
        request.addProperty("userID", SessionContext.getInstance().getFcsUserId());
        request.addProperty("RoomID", roomNum);
        request.addProperty("Item", item.trim());
        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "PostMinibarResponse");
        Boolean result = false;
        if (response != null) {
            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    SoapObject res = (SoapObject) soapresult.getProperty(0);
                    String returncODE = SoapHelper.getStringProperty(res, "ReturnCode", "1");
                    if ("0".equals(returncODE)) {
                        result = true;
                    }
                }
            }
        }
        return result;
    }

    /**
     * @param ctx
     * @param message
     * @param type    1 = WS Logging Messages 2 = Error Messages
     * @return
     */
    public Boolean postWSLog(Context ctx, String message, int type) {

        if (McConstants.OFFLINE_MODE) {
            return true;
        }

        String methodName = "PostWSLog";
        SoapObject request = new SoapObject(NAMESPACE, "PostWSLogRequest");

        SessionContext.getInstance().populate(ctx);
        ConfigContext.getInstance().populate(ctx);

        request.addProperty("UserID", SessionContext.getInstance().getFcsUserId());
        request.addProperty("HTID", ConfigContext.getInstance().getPropertyId());
        request.addProperty("SystemIdentity", "1");
        request.addProperty("Message", message);
        request.addProperty("MessageType", type);

        SoapObject req = new SoapObject(NAMESPACE, methodName);
        req.addSoapObject(request);

        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(req, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "PostWSLogResponse");
        Boolean result = false;
        if (response != null) {
            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    SoapObject res = (SoapObject) soapresult.getProperty(0);
                    String returncODE = SoapHelper.getStringProperty(res, "ReturnCode", "1");
                    if ("0".equals(returncODE)) {
                        result = true;
                    }
                }
            }
        }
        return result;
    }

    public GetFavouriteLinkResponse getFavouriteLink(Context ctx, FavoritesEnum type) {

        GetFavouriteLinkResponse resp = new GetFavouriteLinkResponse();
        List<FavouriteLink> links = new ArrayList<FavouriteLink>();

        if (McConstants.OFFLINE_MODE) {
            links = getOfflineFavorites(type);
            resp.setFavouriteLinks(links);
            return resp;
        }

        String methodName = GET_FAVOURITE_LINK;

        SoapObject request = new SoapObject(NAMESPACE, methodName);
        request.addProperty("Type", type.geteValue());

        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "getFavouriteLinkResponse");
        if (response != null) {
            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    SoapObject res = (SoapObject) soapresult.getProperty(0);
                    for (int i = 0; i < res.getPropertyCount(); i++) {
                        FavouriteLink item = new FavouriteLink();
                        if (res.getProperty(i) instanceof SoapObject) {
                            SoapObject node = (SoapObject) res.getProperty(i);
                            String id = SoapHelper.getStringProperty(node, "ID", "0");
                            String title = SoapHelper.getStringProperty(node, "Title", " ");
                            int typeValue = SoapHelper.getIntegerProperty(node, "Type", -1);
                            String url = SoapHelper.getStringProperty(node, "URL", " ");
                            item.setId(id);
                            item.setTitle(title);
                            if (typeValue == FavoritesEnum.INTERNAL.geteValue()) {
                                item.setType(FavoritesEnum.INTERNAL);
                            } else if (typeValue == FavoritesEnum.EXTERNAL.geteValue()) {
                                item.setType(FavoritesEnum.EXTERNAL);
                            } else if (typeValue == FavoritesEnum.GUEST_STUFF.geteValue()) {
                                item.setType(FavoritesEnum.GUEST_STUFF);
                            }
                            item.setUrl(url);
                            links.add(item);
                        }
                    }
                    resp.setFavouriteLinks(links);
                }
            }
        }
        return resp;
    }

    private List<FavouriteLink> getOfflineFavorites(FavoritesEnum type) {
        List<FavouriteLink> links = new ArrayList<FavouriteLink>();
        String url = "about:blank";

        FavouriteLink item = new FavouriteLink();
        item.setId("1");
        item.setTitle("Staff Directory");
        item.setType(FavoritesEnum.INTERNAL);
        item.setUrl(url);
        links.add(item);

        item = new FavouriteLink();
        item.setId("2");
        item.setTitle("Staff Memo");
        item.setType(FavoritesEnum.INTERNAL);
        item.setUrl(url);
        links.add(item);

        item = new FavouriteLink();
        item.setId("3");
        item.setTitle("Staff Canteen");
        item.setType(FavoritesEnum.INTERNAL);
        item.setUrl(url);
        links.add(item);

        item = new FavouriteLink();
        item.setId("3");
        item.setTitle("Weather Information");
        item.setType(FavoritesEnum.EXTERNAL);
        item.setUrl(url);
        links.add(item);

        item = new FavouriteLink();
        item.setId("4");
        item.setTitle("Airline Directory");
        item.setType(FavoritesEnum.EXTERNAL);
        item.setUrl(url);
        links.add(item);

        item = new FavouriteLink();
        item.setId("5");
        item.setTitle("Useful Contact");
        item.setType(FavoritesEnum.EXTERNAL);
        item.setUrl(url);
        links.add(item);

        item = new FavouriteLink();
        item.setId("6");
        item.setTitle("Travel Agent");
        item.setType(FavoritesEnum.GUEST_STUFF);
        item.setUrl(url);
        links.add(item);

        item = new FavouriteLink();
        item.setId("7");
        item.setTitle("Restaurant Directory");
        item.setType(FavoritesEnum.GUEST_STUFF);
        item.setUrl(url);
        links.add(item);

        item = new FavouriteLink();
        item.setId("8");
        item.setTitle("Flight Information");
        item.setType(FavoritesEnum.GUEST_STUFF);
        item.setUrl(url);
        links.add(item);

        List<FavouriteLink> ls = new ArrayList<FavouriteLink>();
        for (FavouriteLink f : links) {
            if (type.equals(f.getType())) {
                ls.add(f);
            }
        }

        return ls;
    }

    public GetNotifyInfoResponse getBadge(Context ctx, long userId, int bIncludeEngineeringJobs) {

        if (McConstants.OFFLINE_MODE) {

            List<JobModel> jobs = getOfflineJobModels(1);
            int jcount = jobs.size();

            List<MessageModel> msgs = getOfflineMyMessages(false);
            int mcount = 0;
            for (MessageModel m : msgs) {
                if (m.isAcknowledged() == false) {
                    mcount++;
                }
            }

            List<Notify> notifyList = new ArrayList<Notify>();
            GetNotifyInfoResponse resp = new GetNotifyInfoResponse();
            Notify notify1 = resp.new Notify();
            Notify notify2 = resp.new Notify();
            notify1.setCount(mcount);
            notify1.setType("MSG_COUNT".toString());
            notifyList.add(notify1);
            notify2.setCount(jcount);
            notify2.setType("JOB_COUNT".toString());
            notifyList.add(notify2);
            resp.setNotifyList(notifyList);

            return resp;
        }

        String methodName = GET_BADGE;

        SoapObject request = new SoapObject(NAMESPACE, methodName);
        request.addProperty("UserID", userId);
        request.addProperty("bIncludeEngineeringJobs", bIncludeEngineeringJobs);

        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "GetBadgeResponse");
        GetNotifyInfoResponse resp = null;
        if (response != null) {
            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    SoapObject res = (SoapObject) soapresult.getProperty(0);

                    List<Notify> notifyList = new ArrayList<Notify>();
                    resp = new GetNotifyInfoResponse();
                    Notify notify1 = resp.new Notify();
                    Notify notify2 = resp.new Notify();
                    if (res.hasProperty("AdhocMessage")) {
                        notify1.setCount(Integer.valueOf(res.getPropertyAsString("AdhocMessage")));
                        notify1.setType("MSG_COUNT".toString());
                    }
                    notifyList.add(notify1);
                    if (res.hasProperty("MyJobs")) {
                        notify2.setCount(Integer.valueOf(res.getPropertyAsString("MyJobs")));
                        notify2.setType("JOB_COUNT".toString());
                    }
                    notifyList.add(notify2);
                    resp.setNotifyList(notifyList);
                }
            }
            return resp;
        }
        return resp;
    }

    public FindRoomGuestResponse getRoomList(Context ctx, long userId, String roomNo, String guest) {

        FindRoomGuestResponse resp = new FindRoomGuestResponse();

        if (McConstants.OFFLINE_MODE) {
            List<Guest> gl = getOfflineGuests(roomNo);
            resp.setGuests(gl);
            return resp;
        }

        String string = GET_ROOMS_LISTING;
        String methodName = string;

        SoapObject request = new SoapObject(NAMESPACE, "GetRoomsListingRequest");
        request.addProperty("UserID", userId);
        request.addProperty("RoomNo", roomNo);

        SoapObject req = new SoapObject(NAMESPACE, string);
        req.addSoapObject(request);

        String soapAction = NAMESPACE + methodName;
        SoapObject response = callWebService(req, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "GetRoomsListingResponse");

        if (response != null) {

            List<Guest> guests = new ArrayList<Guest>();
            SoapObject res = (SoapObject) SoapHelper.getInnerSoapObject(response, 0, "RoomsListing");
            for (int i = 0; res != null && i < res.getPropertyCount(); i++) {
                if (res.getProperty(i) instanceof SoapObject) {
                    SoapObject node = (SoapObject) res.getProperty(i);
                    Guest g = new Guest();
                    g.setGuestName(SoapHelper.getStringProperty(node, "GuestName1", ""));
                    g.setRoomNum(SoapHelper.getStringProperty(node, "RoomNo", ""));
                    g.setVipMsg(SoapHelper.getStringProperty(node, "VIPCode", ""));
                    g.setCheckinDate(SoapHelper.getDateProperty(node, "Arrival", DateTimeHelper.FORMAT_DATEYEAR_NON, null));
                    g.setCheckoutDate(SoapHelper.getDateProperty(node, "Departure", DateTimeHelper.FORMAT_DATEYEAR_NON, null));
                    g.setVip(false);
                    guests.add(g);
                }
            }
            resp.setGuests(guests);
            return resp;
        }
        return null;
    }

    private List<Guest> getOfflineGuests(String roomNo) {

        if (guests == null || guests.size() == 0) {

            guests = new ArrayList<Guest>();

            Guest g = new Guest();
            g.setGuestName("Mr. Tom");
            g.setRoomNum("1001");
            g.setVipMsg("V1");
            g.setVip(false);
            guests.add(g);

            g = new Guest();
            g.setGuestName("Mr. King");
            g.setRoomNum("1002");
            g.setVipMsg("V1");
            g.setVip(false);
            guests.add(g);

            g = new Guest();
            g.setGuestName("Mr. Jack");
            g.setRoomNum("1003");
            g.setVipMsg("V1");
            g.setVip(false);
            guests.add(g);
        }

        List<Guest> gl = new ArrayList<Guest>();
        for (Guest g : guests) {
            if (g.getRoomNum().contains(roomNo)) {
                gl.add(g);
            }
        }

        return gl;
    }

    public GetPendingNotificationResponse getPendingNotification(Context ctx) {
        SoapObject request = new SoapObject(NAMESPACE, GET_PENDING_NOTIFICATIONS);
        SoapObject o = new SoapObject(NAMESPACE, "GetPendingNotificationsRequest");
        o.addProperty("UserID", SessionContext.getInstance().getFcsUserId());
        request.addSoapObject(o);
        String soapAction = NAMESPACE + GET_PENDING_NOTIFICATIONS;

        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "GetPendingNotificationsResponse");
        if (response != null) {
            GetPendingNotificationResponse resp = null;
            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    if (soapresult.hasProperty("ResponseStatus") && soapresult.hasProperty("NotificationsListing")) {
                        resp = new GetPendingNotificationResponse(soapresult);
                    }
                }
            }
            return resp;
        }
        return null;
    }

    public GetEngineeringChecklistResponse GetEngineeringChecklist(Context ctx, String jobID) {
        SoapObject request = new SoapObject(NAMESPACE, GET_ENGINEERING_CHECKLIST);
        SoapObject o = new SoapObject(NAMESPACE, "GetEngineeringChecklistRequest");
        o.addProperty("UserID", SessionContext.getInstance().getFcsUserId());
        o.addProperty("JobID", jobID);
        request.addSoapObject(o);
        String soapAction = NAMESPACE + GET_ENGINEERING_CHECKLIST;

        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "GetEngineeringChecklistResponse");
        if (response != null) {
            GetEngineeringChecklistResponse resp = null;
            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    if (soapresult.hasProperty("ResponseStatus") && soapresult.hasProperty("EngineeringChecklistDetailsListing")) {
                        resp = new GetEngineeringChecklistResponse(soapresult);
                    }
                }
            }
            return resp;
        }
        return null;
    }

    public GetEngineeringRunnersResponse GetEngineeringRunners(Context ctx, String engineeringDepartmentID, String jobId) {
        SoapObject request = new SoapObject(NAMESPACE, GET_ENGINEERING_RUNNER);
        SoapObject o = new SoapObject(NAMESPACE, "GetEngineeringRunnersRequest");
        o.addProperty("UserID", SessionContext.getInstance().getFcsUserId());
        o.addProperty("EngineeringDepartmentID", engineeringDepartmentID);
        o.addProperty("JobID", jobId);
        request.addSoapObject(o);
        String soapAction = NAMESPACE + GET_ENGINEERING_RUNNER;

//        <GetFreeRunners xmlns="http://eConnect/">
//      <GetFreeRunnersRequest>
//        <UserID>int</UserID>
//        <ServiceItemCode>string</ServiceItemCode>
//        <LocationCode>string</LocationCode>
//      </GetFreeRunnersRequest>
//    </GetFreeRunners>


        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "GetEngineeringRunnersResponse");
        if (response != null) {
            GetEngineeringRunnersResponse resp = null;
            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    if (soapresult.hasProperty("ResponseStatus") && soapresult.hasProperty("EngineeringRunnersDetailsListing")) {
                        resp = new GetEngineeringRunnersResponse(soapresult);
                    }
                }
            }
            return resp;
        }
        return null;
    }

    public GetFreeRunnersResponse getFreeRunners(Context ctx, String ServiceItemCode, String LocationCode) {
        SoapObject request = new SoapObject(NAMESPACE, "GetFreeRunners");
        SoapObject o = new SoapObject(NAMESPACE, "GetFreeRunnersRequest");
        o.addProperty("UserID", SessionContext.getInstance().getFcsUserId());
        o.addProperty("ServiceItemCode", ServiceItemCode);
        o.addProperty("LocationCode", LocationCode);
        request.addSoapObject(o);
        String soapAction = NAMESPACE + GET_FREE_RUNNERS;


        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "GetFreeRunnersResponse");
        if (response != null) {
            GetFreeRunnersResponse resp = null;
            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    if (soapresult.hasProperty("ResponseStatus") && soapresult.hasProperty("FreeRunnersDetailsListing")) {
                        resp = new GetFreeRunnersResponse(soapresult);
                    }
                }
            }
            return resp;
        }
        return null;
    }


    public GetCheckLoginResponse getCheckLogin(Context ctx, String userName) {
        SoapObject request = new SoapObject(NAMESPACE, "GetStillLoggedInByUserName");
        SoapObject o = new SoapObject(NAMESPACE, "GetStillLoggedInByUserNameRequest");
        o.addProperty("UserName", userName);
        String address = McUtils.getMacAddr();
//		o.addProperty("DeviceIdentifier", address);
//		o.addProperty("DeviceType", "2");
        request.addSoapObject(o);
        String soapAction = NAMESPACE + "GetStillLoggedInByUserName";

        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "GetStillLoggedInByUserNameResponse");
        if (response != null) {
            GetCheckLoginResponse resp = null;
            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    if (soapresult.hasProperty("ResponseStatus")) {
                        resp = new GetCheckLoginResponse(soapresult);
                    }
                }
            }
            return resp;
        }
        return null;
    }

    public GetCheckLoginResponse getCheckLogin(Context ctx) {
        SoapObject request = new SoapObject(NAMESPACE, GET_CHECK_LOGIN);
        SoapObject o = new SoapObject(NAMESPACE, "GetStillLoggedInRequest");
        o.addProperty("UserID", SessionContext.getInstance().getFcsUserId());
        String address = McUtils.getMacAddr();
        o.addProperty("DeviceIdentifier", address);
        o.addProperty("DeviceType", "2");
        request.addSoapObject(o);
        String soapAction = NAMESPACE + GET_CHECK_LOGIN;

        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "GetStillLoggedInResponse");
        if (response != null) {
            GetCheckLoginResponse resp = null;
            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    if (soapresult.hasProperty("ResponseStatus")) {
                        resp = new GetCheckLoginResponse(soapresult);
                    }
                }
            }
            return resp;
        }
        return null;

    }

    public JobViewResponse getJobs(Context ctx, String jobNo, Date searchDate, String location, String roomNo, String guestName,
                                   String reportedBy, String assignedBy, String assignedTo, String status, String ack) {
        SoapObject request = new SoapObject(NAMESPACE, GET_JOBS);
        SoapObject o = new SoapObject(NAMESPACE, "GetJobsRequest");
        o.addProperty("UserID", SessionContext.getInstance().getFcsUserId());
        o.addProperty("UserLanguage", McUtils.getECServiceLang());
        o.addProperty("Location", location);
        o.addProperty("RoomNo", roomNo);
        o.addProperty("GuestName", guestName);
        o.addProperty("ReportedBy", reportedBy);
        o.addProperty("JobNo", jobNo);
        o.addProperty("AssignedByName", assignedBy);
        o.addProperty("AssignedToName", assignedTo);
        o.addProperty("JobStatus", status);
        o.addProperty("Ack", ack);

//        PrefsUtil.setEnableEngReq(ctx, enableEngReq);
        if (PrefsUtil.getEnableEng(ctx) == 1) {
            o.addProperty("IncludeEngineeringJobs", 1);
        } else {
            o.addProperty("IncludeEngineeringJobs", 0);
        }
        //..Chua : Add pageIndex
        o.addProperty("PageIndex", 0);

        if (searchDate == null) {
            searchDate = SoapHelper.getDefaultDate();
        }
//		016-09-22T12:33:00
        o.addProperty("SearchDate", DateTimeHelper.getStrDateByTimeZone(searchDate, DateTimeHelper.DEFAULT_FORMATTER_DATETIME_24H_T));
//		o.addProperty("SearchDate", "016-09-21T12:33:00");
        request.addSoapObject(o);

        String soapAction = NAMESPACE + GET_JOBS;

        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "GetJobsResponse");

        return parseGetJobsResponse(response);
    }

    public boolean updateReAssignRunner(Context ctx, String jobNo, EngineeringRunnersDetails assignBy, EngineeringRunnersDetails assignTo) {
        SoapObject request = new SoapObject(NAMESPACE, UPDATE_REASSIGN_RUNNER);
        SoapObject o = new SoapObject(NAMESPACE, "UpdateReAssignRunnerRequest");
        o.addProperty("JobID", jobNo);
        o.addProperty("AssignedByID", assignBy.getRunnerUserID());
        o.addProperty("AssignedByName", assignBy.getRunnerUserFullName());
        o.addProperty("AssignedByDepartment", assignBy.getRunnerDepartment());
        o.addProperty("AssignedToID", assignTo.getRunnerUserID());
        o.addProperty("AssignedToName", assignTo.getRunnerUserFullName());
        o.addProperty("AssignedToDepartment", assignTo.getRunnerDepartment());
        request.addSoapObject(o);


        String soapAction = NAMESPACE + UPDATE_REASSIGN_RUNNER;

        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "UpdateReAssignRunnerResponse");
        if (response != null) {
            SoapObject res = SoapHelper.getInnerSoapObject(response, 0, "ResponseStatus");
            int ret = SoapHelper.getIntegerProperty(res, "ReturnCode", 1);
            if (ret == 0) {
                return true;
            }
        }

        return false;
    }

    public GetEquipmentResponse getEquipmentDetail(Context ctx, String jobId) {
        SoapObject request = new SoapObject(NAMESPACE, GET_EQUIPMENT_DETAIL);
        SoapObject o = new SoapObject(NAMESPACE, "GetEquipmentDetailRequest");
        o.addProperty("UserID", SessionContext.getInstance().getFcsUserId());
        o.addProperty("JobID", jobId);
        request.addSoapObject(o);

        String soapAction = NAMESPACE + GET_EQUIPMENT_DETAIL;

        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "GetEquipmentDetailResponse");
        EquipmentModel equipmentModel = new EquipmentModel();
        GetEquipmentResponse resp = new GetEquipmentResponse();
        resp.setEquipmentModel(equipmentModel);
        if (response != null) {
            SoapObject res = null;
            SoapObject rs = SoapHelper.getInnerSoapObject(response, 0);
            for (int i = 0; rs != null && i < rs.getPropertyCount(); i++) {
                if (i == 0) {
                    SoapObject so = (SoapObject) rs.getProperty(i);
                    int code = SoapHelper.getIntegerProperty(so, "ReturnCode", 0);
                    resp.setCode(code);
                    if (code == 1) {
                        return null;
                    }
                }
                equipmentModel = SoapConverter.getEquipmentDetail(rs);
                if (equipmentModel != null) {
                    resp.setEquipmentModel(equipmentModel);
                }

            }
        }
        return resp;
    }

    private JobViewResponse parseGetJobsResponse(SoapObject response) {
        List<JobModel> models = new ArrayList<JobModel>();
        JobViewResponse resp = new JobViewResponse();
        resp.setJobViews(models);
        if (response != null) {

            SoapObject res = null;
            SoapObject rs = SoapHelper.getInnerSoapObject(response, 0);
            int intPropertyCount = rs.getPropertyCount();
            for (int i = 0; rs != null && i < rs.getPropertyCount(); i++) {
                if (i == 0) {
                    SoapObject so = (SoapObject) rs.getProperty(i);
                    int code = SoapHelper.getIntegerProperty(so, "ReturnCode", 0);
                    resp.setCode(code);
                    if (code == 1) {
                        return null;
                    }
                } else if (i == 2) {
                    res = (SoapObject) rs.getProperty(i);
                }
            }

            for (int i = 0; res != null && i < res.getPropertyCount(); i++) {
                if (res.getProperty(i) instanceof SoapObject) {
                    SoapObject node = (SoapObject) res.getProperty(i);
                    JobModel jm = SoapConverter.getECJob(node);
                    models.add(jm);
                }
            }

            resp.setJobViews(models);
            resp.setTotal(models.size());
            return resp;

        }

        return null;
    }

    public List<JobModel> getGuestHistory(Context ctx, String profileId, String filterRange) {

        if (McConstants.OFFLINE_MODE) {
            return getOfflineJobModels(null);
        }

        List<JobModel> models = new ArrayList<JobModel>();

        SoapObject o = new SoapObject(NAMESPACE, "GetJobQRequestHistoryRequest");
        o.addProperty("UserID", SessionContext.getInstance().getFcsUserId());
        o.addProperty("ProfileID", profileId);
        o.addProperty("FilterRange", filterRange);

        String method = GET_JOB_Q_REQUEST_HISTORY;
        SoapObject req = new SoapObject(NAMESPACE, method);
        req.addSoapObject(o);

        String soapAction = NAMESPACE + method;

        SoapObject response = callWebService(req, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "GetJobQRequestHistoryResponse");

        if (response != null) {

            SoapObject obj = SoapHelper.getInnerSoapObject(response, 0, "JobListing");
            if (obj == null) {
                return models;
            }

            for (int i = 0; i < obj.getPropertyCount(); i++) {
                Object property = obj.getProperty(i);
                if (property instanceof SoapObject) {
                    SoapObject job = (SoapObject) property;
                    if (job.hasProperty("RoomNo")) {
                        JobModel jm = SoapConverter.getECJob(job);
                        models.add(jm);
                    }
                }
            }

        }
        return models;
    }

    public GetUpdateJobStatusResponse getUpdateJobStatus(Context ctx, String jobNo) {

        GetUpdateJobStatusResponse resp = null;
        if (McConstants.OFFLINE_MODE) {
            resp = new GetUpdateJobStatusResponse();
            List<UpdateableJobModel> list = new ArrayList<UpdateableJobModel>();
            UpdateableJobModel m = new UpdateableJobModel();
            m.setJobNumber(jobNo);
            m.setUpdateable(UpdateableJobModel.IS_RUNNER_JOB);
            list.add(m);
            resp.setList(list);
            resp.setReturnCode(0);
            return resp;
        }

        SoapObject request = new SoapObject(NAMESPACE, GET_UPDATE_JOB_STATUS);
        SoapObject o = new SoapObject(NAMESPACE, "GetUpdateJobStatusRequest");
        o.addProperty("UserID", SessionContext.getInstance().getFcsUserId());
        o.addProperty("JobNo", jobNo);
        if (PrefsUtil.getEnableEng(ctx) == 1) {
            o.addProperty("IncludeEngineeringJobs", 1);
        } else {
            o.addProperty("IncludeEngineeringJobs", 0);
        }
        request.addSoapObject(o);
        String soapAction = NAMESPACE + GET_UPDATE_JOB_STATUS;

        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "GetUpdateJobStatusResponse");
        if (response != null) {

            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    if (soapresult.hasProperty("ResponseStatus") && soapresult.hasProperty("UpdateJobStatusListing")) {
                        resp = new GetUpdateJobStatusResponse(soapresult);
                    }
                }
            }
            return resp;
        }
        return null;
    }


    public FindGroupLocationResponse getGroupLocationListing(Context ctx, String term) {
        FindGroupLocationResponse resp = null;
//		if (McConstants.OFFLINE_MODE) {
//			resp = new FindGroupLocationResponse();
//			List<GuestInfo> list = getOfflineGuests(ctx, RoomNo);
//
//			resp.setGuestList(list);
//			return resp;
//		}
        SoapObject request = new SoapObject(NAMESPACE, GET_GROUP_LOCATION_LISTING);
        SoapObject o = new SoapObject(NAMESPACE, GET_GROUP_LOCATION_LISTING_REQUEST);
        o.addProperty("UserID", SessionContext.getInstance().getFcsUserId());
        o.addProperty("WildcardSearchTerm", term);
        request.addSoapObject(o);
        String soapAction = NAMESPACE + GET_GROUP_LOCATION_LISTING;

        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "GetLocationGroupResponse");
        if (response != null) {
            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    if (soapresult.hasProperty("ResponseStatus") && soapresult.hasProperty("LocationGroupListing")) {
                        SoapObject soap = (SoapObject) soapresult.getProperty("LocationGroupListing");
                        resp = new FindGroupLocationResponse(soap);
                    }
                }
            }
            return resp;
        }
        return null;
    }

    public FindLocationResponse getLocationListing(Context ctx, int requestType, String locationGroup, String locationName, String qrCode) {
        FindLocationResponse resp = null;
//		if (McConstants.OFFLINE_MODE) {
//			resp = new FindGroupLocationResponse();
//			List<GuestInfo> list = getOfflineGuests(ctx, RoomNo);
//
//			resp.setGuestList(list);
//			return resp;
//		}
        SoapObject request = new SoapObject(NAMESPACE, GET_LOCATION_LISTING);
        SoapObject o = new SoapObject(NAMESPACE, GET_LOCATION_LISTING_REQUEST);
        o.addProperty("UserID", SessionContext.getInstance().getFcsUserId());
        o.addProperty("RequestType", requestType);
        if (locationGroup.length() > 0)
            o.addProperty("LocationGroup", locationGroup);
        if (qrCode.length() > 0)
            o.addProperty("QrCode", qrCode);
        o.addProperty("LocationCode", locationName);
        request.addSoapObject(o);
        String soapAction = NAMESPACE + GET_LOCATION_LISTING;

        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "GetLocationCodeResponse");
        if (response != null) {
            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    if (soapresult.hasProperty("ResponseStatus") && soapresult.hasProperty("LocationCodeListing")) {
                        SoapObject soap = (SoapObject) soapresult.getProperty("LocationCodeListing");
                        resp = new FindLocationResponse(soap);
                    }
                }
            }
            return resp;
        }
        return null;
    }

    public FindItemResponse getItemListing(Context ctx, int requestType, String locationCode) {
        FindItemResponse resp = null;
//		if (McConstants.OFFLINE_MODE) {
//			resp = new FindGroupLocationResponse();
//			List<GuestInfo> list = getOfflineGuests(ctx, RoomNo);
//
//			resp.setGuestList(list);
//			return resp;
//		}
        SoapObject request = new SoapObject(NAMESPACE, GET_ITEM_LISTING);
        SoapObject o = new SoapObject(NAMESPACE, GET_ITEM_LISTING_REQUEST);
        o.addProperty("UserID", SessionContext.getInstance().getFcsUserId());
        o.addProperty("RequestType", requestType);
        o.addProperty("LocationCode", locationCode);
        request.addSoapObject(o);
        String soapAction = NAMESPACE + GET_ITEM_LISTING;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "GetJobQEngineeringRequestResponse");
        if (response != null) {
            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    if (soapresult.hasProperty("ResponseStatus") && soapresult.hasProperty("TopServiceItemListing") && soapresult.hasProperty("ItemGroupListing")) {
                        SoapObject soapTop = (SoapObject) soapresult.getProperty("TopServiceItemListing");
                        SoapObject soapGroup = (SoapObject) soapresult.getProperty("ItemGroupListing");
                        resp = new FindItemResponse(soapTop, soapGroup, SoapHelper.getStringProperty(soapresult, "Requestor", ""));
                    }
                }
            }
            return resp;
        }
        return null;
    }

    public FindItemNameResponse getItemNameListing(Context ctx, int requestType, String locationCode, String itemGroupID) {
        FindItemNameResponse resp = null;
//		if (McConstants.OFFLINE_MODE) {
//			resp = new FindGroupLocationResponse();
//			List<GuestInfo> list = getOfflineGuests(ctx, RoomNo);
//
//			resp.setGuestList(list);
//			return resp;
//		}
        SoapObject request = new SoapObject(NAMESPACE, GET_ITEM_NAME_LISTING);
        SoapObject o = new SoapObject(NAMESPACE, GET_ITEM_NAME_LISTING_REQUEST);
        o.addProperty("UserID", SessionContext.getInstance().getFcsUserId());
        o.addProperty("RequestType", requestType);
        o.addProperty("LocationCode", locationCode);
        o.addProperty("ItemGroupID", itemGroupID);
        request.addSoapObject(o);
        String soapAction = NAMESPACE + GET_ITEM_NAME_LISTING;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "GetJobQEngineeringRequestServiceItemsResponse");
        if (response != null) {
            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    if (soapresult.hasProperty("ResponseStatus") && soapresult.hasProperty("ServiceItemListing")) {
                        SoapObject soap = (SoapObject) soapresult.getProperty("ServiceItemListing");
                        resp = new FindItemNameResponse(soap);
                    }
                }
            }
            return resp;
        }
        return null;
    }

    public EngineeringRequestResponse createEngineeringRequest(Context ctx, List<ServiceRequestItem> items,
                                                               Date scheduledDate, String requestor, String location, String type) {
        EngineeringRequestResponse resp = null;
//		if (McConstants.OFFLINE_MODE) {
//			resp = new FindGroupLocationResponse();
//			List<GuestInfo> list = getOfflineGuests(ctx, RoomNo);
//
//			resp.setGuestList(list);
//			return resp;
//		}
        SoapObject request = new SoapObject(NAMESPACE, CREATE_ENGINEERING_REQUEST);
        request.addProperty("userID", SessionContext.getInstance().getFcsUserId());

        if (type.equals("0")) {
            request.addProperty("RoomNo", location);
            request.addProperty("sLocation", "");
        } else {
            request.addProperty("RoomNo", "");
            request.addProperty("sLocation", location);
        }

        request.addProperty("sType", "");
        request.addProperty("GuestName", "");
        request.addProperty("Requestor", requestor);
        if (scheduledDate != null) {
            request.addProperty("schedule", DateTimeHelper.formatDateToStr(scheduledDate, DateTimeHelper.FORMAT_DATETIME_FULL, ""));
        } else {
            request.addProperty("schedule", "");
        }
        for (int i = 0; i < items.size(); i++) {
            ServiceRequestItem si = items.get(i);
            int count = i + 1;
            request.addProperty("Item" + count, si.getItemId());
            request.addProperty("Item" + count + "Qty", si.getQuantity());
            request.addProperty("Item" + count + "Remark", si.getRemarks());
        }
//		request.addSoapObject(o);
        String soapAction = NAMESPACE + CREATE_ENGINEERING_REQUEST;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "ValidateCreateJobRequestResponse");
        if (response != null) {
            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    if (soapresult.hasProperty("CreateJob")) {
                        SoapObject soap = (SoapObject) soapresult.getProperty("CreateJob");
                        resp = new EngineeringRequestResponse(soap);
                    }
                }
            }
            return resp;
        }
        return null;
    }

    public GuestInfoResponse getGuestListing(Context ctx, int PageIndex, String RoomNo, String GuestName, String FirstName, String LastName,
                                             String MembershipNo) {

        int intPageIndex = PageIndex;
        GuestInfoResponse resp = null;
        if (McConstants.OFFLINE_MODE) {
            resp = new GuestInfoResponse();
            List<GuestInfo> list = getOfflineGuests(ctx, RoomNo);

            resp.setGuestList(list);
            return resp;
        }

        SoapObject request = new SoapObject(NAMESPACE, "GetGuestListing");
        SoapObject o = new SoapObject(NAMESPACE, GET_GUEST_LISTING_REQUEST);
        o.addProperty("UserID", SessionContext.getInstance().getFcsUserId());
        o.addProperty("UserLanguage", McUtils.getECServiceLang());

        if (RoomNo != null) {
            o.addProperty("RoomNo", RoomNo);
        } else {
            o.addProperty("RoomNo", "");
        }

        if (GuestName != null) {
            o.addProperty("GuestName", GuestName);
        } else {
            o.addProperty("GuestName", "");
        }

        if (FirstName != null) {
            o.addProperty("FirstName", FirstName);
        } else {
            o.addProperty("FirstName", "");
        }

        if (LastName != null) {
            o.addProperty("LastName", LastName);
        } else {
            o.addProperty("LastName", "");
        }

        if (MembershipNo != null) {
            o.addProperty("MembershipNo", MembershipNo);
        } else {
            o.addProperty("MembershipNo", "");
        }

        //..Date : 03 June 2015
        //..Chua
        if (intPageIndex > 0) {
            o.addProperty("PageIndex", intPageIndex);
        }

        request.addSoapObject(o);
        String soapAction = NAMESPACE + GET_GUEST_LISTING;

        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "GetGuestListingResponse");
        if (response != null) {
            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    if (soapresult.hasProperty("ResponseStatus") && soapresult.hasProperty("GuestListing")) {
                        SoapObject soap = (SoapObject) soapresult.getProperty("GuestListing");
                        resp = new GuestInfoResponse(ctx, soap, true);
                    }
                }
            }
            return resp;
        }
        return null;
    }

    public List<LanguageDetail> getLanguageList(Context ctx) {

        ArrayList<LanguageDetail> languageList = new ArrayList<LanguageDetail>();

        if (McConstants.OFFLINE_MODE) {
            return languageList;
        }

        SoapObject request = new SoapObject(NAMESPACE, "GetOnlineTranslationLanguageListing");
        SoapObject o = new SoapObject(NAMESPACE, "GetOnlineTranslationLanguageListingRequest");
        o.addProperty("UserID", SessionContext.getInstance().getFcsUserId());
        o.addProperty("HTID", ConfigContext.getInstance().getPropertyId());

        request.addSoapObject(o);
        String soapAction = NAMESPACE + "GetOnlineTranslationLanguageListing";

        DatabaseHelper helper = DatabaseHelper.getInstance(ctx);
        RuntimeExceptionDao<LanguageDetail, String> dao = helper.getLanguageModelDao();

        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "GetOnlineTranslationLanguageListingResponse");
        if (response != null) {
            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    if (soapresult.hasProperty("ResponseStatus") && soapresult.hasProperty("LanguageListing")) {
                        SoapObject soap = (SoapObject) soapresult.getProperty("LanguageListing");
                        if (soap.getPropertyCount() > 0) {
                            for (int i = 0; i < soap.getPropertyCount(); i++) {
                                languageList.add(new LanguageDetail((SoapObject) soap.getProperty(i)));
                            }
                        }
                        if (languageList.size() > 0) {
                            helper.clear(LanguageDetail.class);
                            for (LanguageDetail detail : languageList) {
                                dao.create(detail);
                            }
                        }
                        return languageList;
                    }
                }
            }
        }

        return dao.queryForAll();
    }

    public void getLanguageResourceFiles(Context ctx) {


        SoapObject request = new SoapObject(NAMESPACE, "GetLanguageResource");
        SoapObject o = new SoapObject(NAMESPACE, "GetLanguageResourceRequest");
        // device type: 1 android, 0 iOS
        o.addProperty("DeviceType", "1");
        request.addSoapObject(o);

        String soapAction = NAMESPACE + "GetLanguageResource";
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "GetLanguageResourceResponse");
        if (response != null) {
            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    if (soapresult.hasProperty("ResponseStatus")) {
                        if (soapresult.hasProperty("LanguageListing")) {
                            SoapObject soap = (SoapObject) soapresult.getProperty("LanguageListing");
                            List<LanguageDetail> list = new ArrayList<LanguageDetail>();
                            if (soap.getPropertyCount() > 0) {
                                for (int i = 0; i < soap.getPropertyCount(); i++) {
                                    SoapObject resource = (SoapObject) soap.getProperty(i);
                                    String code = SoapHelper.getStringProperty(resource, "Code", "");
                                    String path = SoapHelper.getStringProperty(resource, "Desc", "");
                                    String transCode = SoapHelper.getStringProperty(resource, "OnlineTranslationCode", "");
                                    String iosValue = "";
                                    if ("CHS".equalsIgnoreCase(code)) {
                                        iosValue = "zh_CN";
                                    } else if ("CHT".equalsIgnoreCase(code)) {
                                        iosValue = "zh_TW";
                                    } else if ("JPN".equalsIgnoreCase(code)) {
                                        iosValue = "ja_JP";
                                    } else if ("KOR".equalsIgnoreCase(code)) {
                                        iosValue = "ko_KR";
                                    } else if ("THA".equalsIgnoreCase(code)) {
                                        iosValue = "th_TH";
                                    } else if ("FRA".equalsIgnoreCase(code)) {
                                        iosValue = "fr_FR";
                                    } else if ("SPA".equalsIgnoreCase(code)) {
                                        iosValue = "es_ES";
                                    } else {
                                        iosValue = "en_US";
                                    }
                                    LanguageDetail detail = new LanguageDetail();
                                    detail.setCode(code);
                                    detail.setDescription(path);
                                    detail.setTranslationCode(transCode);
                                    detail.setIosValue(iosValue);
                                    list.add(detail);
                                }
                            }
                            if (list.size() > 0) {
                                DatabaseHelper helper = DatabaseHelper.getInstance(ctx);
                                RuntimeExceptionDao<LanguageDetail, String> dao = helper.getLanguageModelDao();
                                dao.delete(dao.queryForAll());
                                for (LanguageDetail d : list) {
                                    dao.create(d);
                                }
                            }
                        }
                        if (soapresult.hasProperty("LanguageFileListing")) {
                            SoapObject soap = (SoapObject) soapresult.getProperty("LanguageFileListing");
                            if (soap.getPropertyCount() > 0) {
                                for (int i = 0; i < soap.getPropertyCount(); i++) {
                                    SoapObject resource = (SoapObject) soap.getProperty(i);
                                    String code = SoapHelper.getStringProperty(resource, "Code", "");
                                    String path = SoapHelper.getStringProperty(resource, "Path", "");

                                    // Convert code to Ios_Code, for example 'eng' to 'en_US'
                                    DatabaseHelper helper = DatabaseHelper.getInstance(ctx);
                                    String iosCode = helper.getIosCodeFromCode(code);

                                    saveLanguageResourceToDB(ctx, path, iosCode, ConfigContext.getInstance().getPropertyId());
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private Collection<LanguageResource> parseStringResourceXML(String xmlContent, String code, long propertyId) {

        XMLParser parser = new XMLParser();
        Document doc = parser.getDomElement(xmlContent);
        NodeList nl = doc.getElementsByTagName("string");
        Collection<LanguageResource> resList = new HashSet<LanguageResource>();
        for (int i = 0; i < nl.getLength(); i++) {
            Element e = (Element) nl.item(i);
            Attr nameNode = e.getAttributeNode("name");
            String name = nameNode.getNodeValue();
            String value = "";
            NodeList nodes = e.getChildNodes();
            if (nodes.getLength() > 0) {
                Node item = nodes.item(0);
                value = item.getNodeValue();
            }
            LanguageResource r = new LanguageResource();
            r.setCode(code);
            r.setName(name);
            r.setValue(value);
            r.setPropertyId((int) propertyId);
            resList.add(r);

        }
        return resList;
    }

    private void saveLanguageResourceToDB(Context ctx, String urlAddress, String code, long propertyId) {

        if (code == null || code.trim().isEmpty()) {
            return;
        }

        XMLParser parser = new XMLParser();
        String xml = parser.getXmlFromUrl(urlAddress);
        if (xml == null || xml.trim().isEmpty()) {
            return;
        }

        Collection<LanguageResource> resList = parseStringResourceXML(xml, code, propertyId);

        if (resList != null && resList.size() > 0) {
            DatabaseHelper helper = DatabaseHelper.getInstance(ctx);
            RuntimeExceptionDao<LanguageResource, String> dao = helper.getLanguageResourceDao();
            List<LanguageResource> list = dao.queryForEq("code", code);

            if (list != null) {
                dao.delete(list);
            }

            for (LanguageResource r : resList) {
                r.setCode(code);
                r.setPropertyId((int) propertyId);
                dao.create(r);
            }
        }

    }

    public String getTranslatedText(Context ctx, String fromCode, String toCode, String text) {

        if (McConstants.OFFLINE_MODE) {
            return "";
        }

        SoapObject request = new SoapObject(NAMESPACE, "GetOnlineTranslatedText");
        SoapObject o = new SoapObject(NAMESPACE, "GetOnlineTranslatedTextRequest");
        o.addProperty("LanguageCodeFrom", fromCode);
        o.addProperty("LanguageCodeTo", toCode);
        o.addProperty("Text", text);

        request.addSoapObject(o);
        String soapAction = NAMESPACE + "GetOnlineTranslatedText";

        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "GetOnlineTranslatedTextResponse");
        if (response != null) {
            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    if (soapresult.hasProperty("ResponseStatus") && soapresult.hasProperty("Text")) {
                        String result = SoapHelper.getStringProperty(soapresult, "Text", "");
                        return result;
                    }
                }
            }
        }
        return null;
    }

    public String GetEngineeringTaskFile(Context ctx, String jobID) {

        if (McConstants.OFFLINE_MODE) {
            return "";
        }

        SoapObject request = new SoapObject(NAMESPACE, "GetEngineeringTaskFile");
        SoapObject o = new SoapObject(NAMESPACE, "GetEngineeringTaskFileRequest");
        o.addProperty("UserID", SessionContext.getInstance().getFcsUserId());
        o.addProperty("JobID", jobID);

        request.addSoapObject(o);
        String soapAction = NAMESPACE + "GetEngineeringTaskFile";

        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "GetEngineeringTaskFileResponse");
        if (response != null) {
            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    if (soapresult.hasProperty("ResponseStatus") && soapresult.hasProperty("TaskFileLink")) {
                        String result = SoapHelper.getStringProperty(soapresult, "TaskFileLink", "");
                        return result;
                    }
                }
            }
        }
        return "";
    }


    private List<GuestInfo> getOfflineGuests(Context ctx, String roomNo) {
        String yes = ctx.getString(R.string.yes);
        String no = ctx.getString(R.string.no);

        List<GuestInfo> list = new ArrayList<GuestInfo>();

        GuestInfo g = new GuestInfo();
        g.setRoomNum("1001");
        g.setGuestUsername("Mr. Tom");
        g.setLanguage("English");
        g.setVipMsg("1");
        g.setCheckinDate("20 Sep 2011");
        g.setCheckoutDate("25 Sep 2011");
        g.setDnd(no);
        g.setAwu(no);
        g.setTextMsg(yes);
        g.setVoiceMsg(yes);
        list.add(g);

        g = new GuestInfo();
        g.setRoomNum("1002");
        g.setGuestUsername("Mr. King");
        g.setLanguage("English");
        g.setVipMsg("1");
        g.setCheckinDate("20 Sep 2011");
        g.setCheckoutDate("28 Sep 2011");
        g.setDnd(no);
        g.setAwu(no);
        g.setTextMsg(no);
        g.setVoiceMsg(no);
        list.add(g);

        g = new GuestInfo();
        g.setRoomNum("1003");
        g.setGuestUsername("Mr. Jack");
        g.setLanguage("English");
        g.setVipMsg("1");
        g.setCheckinDate("20 Sep 2011");
        g.setCheckoutDate("28 Sep 2011");
        g.setDnd(yes);
        g.setAwu(no);
        g.setTextMsg(no);
        g.setVoiceMsg(no);
        list.add(g);

        List<GuestInfo> ls = new ArrayList<GuestInfo>();
        for (GuestInfo i : list) {
            if (i.getRoomNum().contains(roomNo)) {
                ls.add(i);
            }
        }

        return ls;
    }

//	public void postUpdateLoginLanguage(Context ctx) {
//
//		if (McConstants.OFFLINE_MODE) {
//			return;
//		}
//
//		SoapObject request = new SoapObject(NAMESPACE, POST_UPDATE_LANG);
//		SoapObject o = new SoapObject(NAMESPACE, "PostUpdateLoginLanguageRequest");
//		o.addProperty("UserID", SessionContext.getInstance().getFcsUserId());
//		o.addProperty("LanguageCode", McUtils.getECServiceLang());
//		request.addSoapObject(o);
//		String soapAction = NAMESPACE + POST_UPDATE_LANG;
//		SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
//				"PostUpdateLoginLanguageResponse");
//		if (response != null) {
//			if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
//				response.getProperty(0);
//			}
//		}
//	}

    public FindMsgResponse getAdhocLists(Context ctx, Date searchDate, int allNak) {

        FindMsgResponse resp = new FindMsgResponse();
        List<MessageModel> guests = new ArrayList<MessageModel>();

        if (McConstants.OFFLINE_MODE) {

            guests.addAll(getOfflineMyMessages(true));
            resp.setMessageList(guests);
            return resp;
        }

        SoapObject request = new SoapObject(NAMESPACE, GET_ADHOCLISTS);
        request.addProperty("UserID", SessionContext.getInstance().getFcsUserId());
        if (searchDate == null) {
            request.addProperty("SearchDate", "");
        } else {
            request.addProperty("SearchDate", DateTimeHelper.getStrDateByTimeZone(searchDate, DateTimeHelper.FORMAT_DATEYEAR));
        }
        request.addProperty("AllNak", allNak);

        String soapAction = NAMESPACE + GET_ADHOCLISTS;
        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "GetAdhocListsResponse");
        if (response != null) {

            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject result = (SoapObject) response.getProperty(0);
                if (result.getPropertyCount() > 0 && result.getProperty(0) instanceof SoapObject) {
                    SoapObject res = (SoapObject) result.getProperty(0);
                    for (int i = 0; i < res.getPropertyCount(); i++) {
                        if (res.getProperty(i) instanceof SoapObject) {
                            SoapObject node = (SoapObject) res.getProperty(i);
                            MessageModel m = converToAdhoc(node);
                            guests.add(m);
                        }
                    }
                }
            }
            resp.setMessageList(guests);
            return resp;
        }
        return null;
    }

    public GetMinbarItemsResponse getMinibarConfigurationItems(Context ctx, String roomNo) {

        GetMinbarItemsResponse resp = new GetMinbarItemsResponse();
        List<MinibarItem> items = new ArrayList<MinibarItem>();

        if (McConstants.OFFLINE_MODE) {

            MinibarItem item = new MinibarItem();
            item.setId(1L);
            item.setName("Coke");
            item.setPrice(10);
            items.add(item);

            item = new MinibarItem();
            item.setId(2L);
            item.setName("Sprite");
            item.setPrice(10);
            items.add(item);

            item = new MinibarItem();
            item.setId(3L);
            item.setName("Nuts");
            item.setPrice(25);
            items.add(item);

            item = new MinibarItem();
            item.setId(4L);
            item.setName("Wine");
            item.setPrice(60);
            items.add(item);

            resp.setMinibarItems(items);

            return resp;
        }

        String methodName = GET_MINIBAR_CONFIGURATION_ITEMS;

        SoapObject request = new SoapObject(NAMESPACE, GET_MINIBAR_CONFIGURATION_ITEMS);
        SoapObject o = new SoapObject(NAMESPACE, "GetMinibarConfigurationItemsRequest");
        o.addProperty("UserID", SessionContext.getInstance().getFcsUserId());
        o.addProperty("RoomNo", roomNo);
        request.addSoapObject(o);
        String soapAction = NAMESPACE + methodName;

        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "GetMinibarConfigurationItemsResponse");

        if (response != null) {
            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    if (soapresult.hasProperty("ResponseStatus") && soapresult.hasProperty("MinibarItemsListing")) {
                        SoapObject responseStatus = (SoapObject) soapresult.getProperty("ResponseStatus");
                        // String errorMsg =
                        // SoapHelper.getStringProperty(responseStatus,
                        // "ErrorMsg", "");
                        int returnCode = SoapHelper.getIntegerProperty(responseStatus, "ReturnCode", -1);
                        if (returnCode == 1) {
                            // hasError
                        } else {
                            SoapObject minibarItemsListing = (SoapObject) soapresult.getProperty("MinibarItemsListing");
                            for (int i = 0; i < minibarItemsListing.getPropertyCount(); i++) {
                                SoapObject node = (SoapObject) minibarItemsListing.getProperty(i);
                                String id = SoapHelper.getStringProperty(node, "ID", "1");
                                String name = SoapHelper.getStringProperty(node, "Name", " ");
                                String price = SoapHelper.getStringProperty(node, "Price", "0.00");
                                MinibarItem item = new MinibarItem();
                                item.setId(Long.valueOf(id));
                                item.setName(name);
                                item.setPrice(Double.parseDouble(price));
                                items.add(item);
                            }
                        }
                    }
                }
            }
            resp.setMinibarItems(items);
            return resp;
        }
        return resp;
    }

    public List<ServiceCodeModel> getServiceCodes(Context ctx) {

        List<ServiceCodeModel> codes = new ArrayList<ServiceCodeModel>();

        if (McConstants.OFFLINE_MODE) {
            return codes;
        }

        SoapObject request = new SoapObject(NAMESPACE, "GetJobQServiceCodesRequest");
        request.addProperty("UserID", SessionContext.getInstance().getFcsUserId());

        String method = GET_JOB_Q_SERVICE_CODES;
        SoapObject o = new SoapObject(NAMESPACE, method);
        o.addSoapObject(request);

        String soapAction = NAMESPACE + method;
        SoapObject response = callWebService(o, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "GetJobQServiceCodesResponse");
        if (response != null) {

            SoapObject listing = SoapHelper.getInnerSoapObject(response, 0, "ServiceCodesListing");

            if (listing != null && listing.getPropertyCount() > 0) {
                for (int i = 0; i < listing.getPropertyCount(); i++) {
                    if (listing.getProperty(i) instanceof SoapObject) {
                        SoapObject node = (SoapObject) listing.getProperty(i);
                        ServiceCodeModel m = new ServiceCodeModel();
                        m.setCode(SoapHelper.getStringProperty(node, "ServiceCodeCode", ""));
                        m.setName(SoapHelper.getStringProperty(node, "ServiceCodeName", ""));
                        codes.add(m);
                    }
                }

            }

        }
        return codes;
    }

    //..Modify By 	: Chua Kam Hoong
    //..Date 		: 20 May 2015
    //public List<ServiceItemViewModel> searchServiceItem(Context ctx, String term, String serviceCode) {
    //public List<ServiceItemViewModel> searchServiceItem(Context ctx, List<ServiceItemViewModel> pList, int PageSize, String PageIndex, String term, String serviceCode) {
    public List<ServiceItemViewModel> searchServiceItem(Context ctx, List<ServiceItemViewModel> pList, int PageIndex, String term, String serviceCode) {

        int intPageIndex = PageIndex;
        //String strItemCodeOrName = term;
        String strServiceCode = serviceCode;
        //int intPageIndex = PageIndex;
        //StringBuilder sb = new StringBuilder();
        //sb.append("");
        //sb.append(intPageIndex);
        //String strPageIndex = sb.toString();

        //List<ServiceItemViewModel> vListItems = new ArrayList<ServiceItemViewModel>();
        //if(vListItems == null){
        //vListItems = new ArrayList<ServiceItemViewModel>();
        //}
        vListItems = pList;

        if (McConstants.OFFLINE_MODE) {
            vListItems = getOfflineServiceItems(term);
            return vListItems;
        }

        if (serviceCode == null) {
            serviceCode = "";
            strServiceCode = "";
        }
        if (term == null) {
            term = "";
            //strItemCodeOrName = "";
        }


        //DatabaseHelper helper = DatabaseHelper.getInstance(ctx);
        //RuntimeExceptionDao<ServiceItemViewModel, Long> dao = helper.getServiceItemDao();

        long lngDebugUserID = SessionContext.getInstance().getFcsUserId();
        List<ServiceItemViewModel> list = null;

        ////long count = 0;
        //long count = dao.countOf();
        //if (count > 0) {
        //..Amended by : Chua Kam Hoong
        //List<ServiceItemViewModel> list = queryServiceItem(term, serviceCode, dao);
        //List<ServiceItemViewModel> list = queryServiceItem(dao, PageSize, PageIndex, term, serviceCode);
        //List<ServiceItemViewModel> list = queryServiceItem(dao, 0, 0, term, serviceCode);
        //return list;

        //list = queryServiceItem(dao, PageSize, PageIndex, term, serviceCode);
        //list = queryServiceItem(dao, PageSize, PageIndex, term, serviceCode);
        //return list;
        //}

        //return list;


        //long lngDebugUserID = SessionContext.getInstance().getFcsUserId();

        SoapObject request = new SoapObject(NAMESPACE, GET_JOB_Q_SERVICE_ITEMS_REQUEST);
        /*
        request.addProperty("UserID", SessionContext.getInstance().getFcsUserId());
		request.addProperty("ItemCode", "");
		request.addProperty("ServiceCodeCode", "");
		request.addProperty("ServiceCodeName", "");
		request.addProperty("WildcardSearchTerm", "");
		request.addProperty("PageIndex", "");		*/
        request.addProperty("UserID", SessionContext.getInstance().getFcsUserId());
        //if(strItemCodeOrName.length() == 0){
        //	request.addProperty("ItemCode", "");
        //}else{
        //	request.addProperty("ItemCode", strItemCodeOrName);
        //}
        request.addProperty("ItemCode", "");
        if (strServiceCode.length() == 0) {
            request.addProperty("ServiceCodeCode", "");
        } else {
            request.addProperty("ServiceCodeCode", strServiceCode);
        }
//		if(term.length() == 0){
//			request.addProperty("WildcardSearchTerm", "");
//		}else{
//			request.addProperty("WildcardSearchTerm", term);
//		}
        if (term.length() == 0) {
            request.addProperty("ItemName", "");
        } else {
            request.addProperty("ItemName", term);
        }
        request.addProperty("WildcardSearchTerm", "");
        if (intPageIndex > 0) {
            request.addProperty("PageIndex", PageIndex);
        }


        String method = "GetJobQServiceItems";
        SoapObject o = new SoapObject(NAMESPACE, method);
        o.addSoapObject(request);

        String soapAction = NAMESPACE + method;
        SoapObject response = callWebService(o, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "GetJobQServiceItemsResponse");
        if (response != null) {

            SoapObject listing = SoapHelper.getInnerSoapObject(response, 0, "ServiceItemsListing");

            if (listing != null && listing.getPropertyCount() > 0) {
                for (int i = 0; i < listing.getPropertyCount(); i++) {
                    if (listing.getProperty(i) instanceof SoapObject) {
                        SoapObject node = (SoapObject) listing.getProperty(i);
                        ServiceItemViewModel m = new ServiceItemViewModel();
                        m.setEnabledQuantity(SoapHelper.getIntegerProperty(node, "EnabledQuantity", 0));
                        m.setServiceCodeCode(SoapHelper.getStringProperty(node, "ServiceCodeCode", ""));
                        m.setServiceCodeName(SoapHelper.getStringProperty(node, "ServiceCodeName", ""));
                        m.setCode(SoapHelper.getStringProperty(node, "ItemCode", ""));
                        m.setName(SoapHelper.getStringProperty(node, "ItemName", ""));
                        m.setServiceItemId(SoapHelper.getLongProperty(node, "ItemID", -1L));
                        vListItems.add(m);
                    }
                }
            }
        }

        //if (vListItems.size() > 0) {
        //helper.clear(ServiceItemViewModel.class);
        //for (ServiceItemViewModel m : vListItems) {
        //dao.create(m);
        //}
        //}

        //List<ServiceItemViewModel> list = queryServiceItem(term, serviceCode, dao);
        //List<ServiceItemViewModel> list = queryServiceItem(dao, PageSize, PageIndex, term, serviceCode);
        //list = queryServiceItem(dao, PageSize, PageIndex, term, serviceCode);
        //list = queryServiceItem(dao, PageSize, 0, term, serviceCode);
        list = prQueryServiceItem(vListItems, term, serviceCode);

        return list;

    }

    //..Create By : Chua...
    private List<ServiceItemViewModel> prQueryServiceItem(List<ServiceItemViewModel> pListItems, String term, String serviceCode) {

        return pListItems;
    }

    private List<ServiceItemViewModel> queryServiceItem(RuntimeExceptionDao<ServiceItemViewModel, Long> dao
            , int PageSize, int PageIndex, String term, String serviceCode) {

		/*
        String strPageSize = String.valueOf(PageSize);
		String strPageIndex = String.valueOf(PageIndex);
		int intTmpPageSize = 0;
		String strTmpPageSize = "";
		int intOffset = PageIndex;
		if(PageIndex == 0){
			intTmpPageSize = PageSize;
			strTmpPageSize = String.valueOf(intTmpPageSize);
		}else{
			if(PageIndex > 0){
				intTmpPageSize = (PageSize * (PageIndex + 1));
				strTmpPageSize = String.valueOf(intTmpPageSize);
			}
		}
		*/

        try {

            if (McUtils.isNullOrEmpty(term) && McUtils.isNullOrEmpty(serviceCode)) {
                //..Modify By 	: Chua Kam Hoong
                //..Date 		: 20 May 2015
                //List<ServiceItemViewModel> list = dao.queryForAll();
                List<ServiceItemViewModel> list = null;
                /*
                if((PageSize == 0) && (PageIndex == 0)){
					list = dao.queryForAll();
				}else{
					//List<ServiceItemViewModel> list = dao.queryBuilder().offset(Long.parseLong("0")).limit(Long.parseLong("10")).query();
					//list = dao.queryBuilder().offset(Long.parseLong(strPageIndex)).limit(Long.parseLong(strPageSize)).query();
					//list = dao.queryBuilder().offset(Long.parseLong(strPageIndex)).limit(Long.parseLong(strPageSize)).orderBy("code", true).query();
					//list = dao.queryBuilder().offset(Long.parseLong(strOffset)).limit(Long.parseLong(strPageSize)).query();
					//list = dao.queryBuilder().offset(Long.parseLong(strOffset)).limit(Long.parseLong(strPageSize)).orderBy("code", true).query();
					list = dao.queryBuilder().offset(Long.parseLong("0")).limit(Long.parseLong(strTmpPageSize)).orderBy("code", true).query();
				}
				*/
                list = dao.queryForAll();
                return list;
            }

            //queryBuilder.offset(startRow).limit(20);

            Where<ServiceItemViewModel, Long> where = dao.queryBuilder().where();
            //Where<ServiceItemViewModel, Long> where = dao.queryBuilder().offset(Long.parseLong("0")).limit(Long.parseLong("10")).where();

            if (!McUtils.isNullOrEmpty(term)) {
                where.like("name", "%" + term + "%");
                where.or();
                where.like("code", "%" + term + "%");
            }

            if (!McUtils.isNullOrEmpty(serviceCode)) {
                if (!McUtils.isNullOrEmpty(term)) {
                    where.and();
                }
                where.eq("serviceCodeCode", serviceCode);
            }

            List<ServiceItemViewModel> list = where.query();
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ArrayList<ServiceItemViewModel>();
    }

    private List<ServiceItemViewModel> queryServiceItem(String term, String serviceCode,
                                                        RuntimeExceptionDao<ServiceItemViewModel, Long> dao) {

        try {

            if (McUtils.isNullOrEmpty(term) && McUtils.isNullOrEmpty(serviceCode)) {
                //List<ServiceItemViewModel> list = dao.queryForAll();
                List<ServiceItemViewModel> list = dao.queryBuilder().offset(Long.parseLong("0")).limit(Long.parseLong("10")).query();
                return list;
            }

            //queryBuilder.offset(startRow).limit(20);

            Where<ServiceItemViewModel, Long> where = dao.queryBuilder().where();
            //Where<ServiceItemViewModel, Long> where = dao.queryBuilder().offset(Long.parseLong("0")).limit(Long.parseLong("10")).where();

            if (!McUtils.isNullOrEmpty(term)) {
                where.like("name", "%" + term + "%");
                where.or();
                where.like("code", "%" + term + "%");
            }

            if (!McUtils.isNullOrEmpty(serviceCode)) {
                if (!McUtils.isNullOrEmpty(term)) {
                    where.and();
                }
                where.eq("serviceCodeCode", serviceCode);
            }

            List<ServiceItemViewModel> list = where.query();
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ArrayList<ServiceItemViewModel>();
    }

    public String getRoomStatus(Context mContext) {

        ConfigContext config = ConfigContext.getInstance();
        config.populate(mContext);

        SoapObject request = new SoapObject(NAMESPACE, GET_ROOM_STATUS);
        SoapObject o = new SoapObject(NAMESPACE, "GetRoomStatusRequest");
        o.addProperty("HTID", "" + config.getPropertyId());
        o.addProperty("UserID", SessionContext.getInstance().getFcsUserId());
        request.addSoapObject(o);
        String soapAction = NAMESPACE + GET_ROOM_STATUS;

        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "GetRoomStatusResponse", true);
        if (response != null) {
            if (response.getPropertyCount() > 0 && response.getProperty(0) instanceof SoapObject) {
                SoapObject soapresult = (SoapObject) response.getProperty(0);
                if (soapresult.getPropertyCount() > 0 && soapresult.getProperty(0) instanceof SoapObject) {
                    if (soapresult.hasProperty("ResponseStatus") && soapresult.hasProperty("RoomStatusListing")) {
                        SoapObject responseStatus = (SoapObject) soapresult.getProperty("ResponseStatus");
                        String returnCode = SoapHelper.getStringProperty(responseStatus, "ReturnCode", "");
                        String ErrorMsg = SoapHelper.getStringProperty(responseStatus, "ErrorMsg", "");
                        if (returnCode.equalsIgnoreCase("0")) {
                            SoapObject soap = (SoapObject) soapresult.getProperty("RoomStatusListing");
                            try {
                                if (soap.getPropertyCount() > 0) {
                                    JSONArray jsonArray = new JSONArray();
                                    for (int i = 0; i < soap.getPropertyCount(); i++) {
                                        SoapObject soapObject = (SoapObject) soap.getProperty(i);
                                        JSONObject jsonObject = new JSONObject();
                                        jsonObject.put("Code", SoapHelper.getStringProperty(soapObject, "Code", ""));
                                        jsonObject.put("Name", SoapHelper.getStringProperty(soapObject, "Name", ""));
                                        jsonArray.put(jsonObject);
                                    }
                                    PrefsUtil.setRoomStatus(mContext, jsonArray);
                                    return "";
                                }
                            } catch (JSONException e) {
                                return e.getMessage();
                            }
                        } else {
                            return ErrorMsg;
                        }
                    }
                }
            }
        }
        return "null error";
    }


    public ConfigContext getPanicButtonConfig(Context mContext) {

        ConfigContext config = ConfigContext.getInstance();
        config.populate(mContext);

        if (config.getPropertyId() < 1) {
            config.setEnableOnMainPage(false);
            config.setEnableOnLoginPage(false);
            config.persist(mContext);
            return config;
        }

        SoapObject request = new SoapObject(NAMESPACE, GET_PANIC_BUTTON_CONFIG);
        SoapObject o = new SoapObject(NAMESPACE, "GetPanicButtonConfigRequest");
        o.addProperty("HTID", "" + config.getPropertyId());
        request.addSoapObject(o);
        String soapAction = NAMESPACE + GET_PANIC_BUTTON_CONFIG;

        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "GetPanicButtonConfigResponse", true);
        if (response != null) {
            SoapObject res = SoapHelper.getInnerSoapObject(response, 0);
            int EnableOnLoginPage = SoapHelper.getIntegerProperty(res, "EnableOnLoginPage", 0);
            int EnableOnMainPage = SoapHelper.getIntegerProperty(res, "EnableOnMainPage", 0);
            int EnablePanicCall = SoapHelper.getIntegerProperty(res, "EnablePanicCall", 0);
            int EnablePanicJob = SoapHelper.getIntegerProperty(res, "EnablePanicJob", 0);
            int AutoAlert = SoapHelper.getIntegerProperty(res, "AutoAlert", 1);
            String PanicCallContactNumber = SoapHelper.getStringProperty(res, "PanicCallContactNumber", "");

            config.setEnableOnLoginPage(EnableOnLoginPage > 0);
            config.setEnableOnMainPage(EnableOnMainPage > 0);
            config.setEnablePanicJob(EnablePanicJob > 0);
            config.setEnablePanicCall(EnablePanicCall > 0);
            config.setPanicCallContactNumber(PanicCallContactNumber);
            config.setAutoAlert(AutoAlert > 0);
            config.persist(mContext);
        }

        return config;
    }

    public boolean postPanicButtonAlert(Context mContext, String telNo, String secretKey, String locationCode) {

        ConfigContext config = ConfigContext.getInstance();
        config.populate(mContext);

        if (config.getPropertyId() < 1) {
            return false;
        }

        SoapObject request = new SoapObject(NAMESPACE, POST_PANIC_BUTTON_ALERT);
        SoapObject o = new SoapObject(NAMESPACE, "PostPanicButtonAlertRequest");
        o.addProperty("HTID", "" + config.getPropertyId());
        o.addProperty("RequestorNumber", telNo);
        o.addProperty("RequestorID", SessionContext.getInstance().getFcsUserId());
        o.addProperty("RequestorLocation", locationCode);
        o.addProperty("SecretPanicKey", secretKey);
        request.addSoapObject(o);
        String soapAction = NAMESPACE + POST_PANIC_BUTTON_ALERT;

        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "PostPanicButtonAlertResponse", true);
        if (response != null) {
            SoapObject res = SoapHelper.getInnerSoapObject(response, 0, "ResponseStatus");
            int ret = SoapHelper.getIntegerProperty(res, "ReturnCode", 1);
            if (ret == 0) {
                return true;
            }
        }

        return false;
    }

    public MediaLibraryConfig getMediaLibraryConfig(Context mContext) {

        MediaLibraryConfig config = new MediaLibraryConfig();

        if (ConfigContext.getInstance().getPropertyId() < 1) {
            Log.w(TAG, "property ID is not set");
            return config;
        }

        SoapObject request = new SoapObject(NAMESPACE, GET_MEDIA_LIBRARY_CONFIG);
        SoapObject o = new SoapObject(NAMESPACE, "GetMediaLibraryConfigRequest");
        o.addProperty("HTID", "" + ConfigContext.getInstance().getPropertyId());
        request.addSoapObject(o);
        String soapAction = NAMESPACE + GET_MEDIA_LIBRARY_CONFIG;

        SoapObject response = callWebService(request, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "GetMediaLibraryConfigResponse", true);
        if (response != null) {
            SoapObject res = SoapHelper.getInnerSoapObject(response, 0);

            int EnableCaptureImage = SoapHelper.getIntegerProperty(res, "EnableCaptureImage", 0);
            int EnableVoiceMessage = SoapHelper.getIntegerProperty(res, "EnableVoiceMessage", 0);
            config.setEnableCaptureImage(EnableCaptureImage > 0);
            config.setEnableVoiceImage(EnableVoiceMessage > 0);

            SoapObject tag = SoapHelper.getInnerSoapObject(res, "TagListing");

            ArrayList<MediaLibraryConfig.Tag> imageTag = new ArrayList<MediaLibraryConfig.Tag>();
            ArrayList<MediaLibraryConfig.Tag> voiceTag = new ArrayList<MediaLibraryConfig.Tag>();
            if (tag != null) {
                for (int i = 0; i < tag.getPropertyCount(); i++) {
                    if (tag.getProperty(i) instanceof SoapObject) {
                        SoapObject so = (SoapObject) tag.getProperty(i);

                        String description = SoapHelper.getStringProperty(so, "Description", "");
                        long id = SoapHelper.getLongProperty(so, "ID", -1L);
                        String type = SoapHelper.getStringProperty(so, "Type", "");

                        if (id > 0 && "1".equals(type)) {
                            MediaLibraryConfig.Tag t = new MediaLibraryConfig.Tag();
                            t.setDescription(description);
                            t.setId(id);
                            imageTag.add(t);
                        }

                        if (id > 0 && "2".equals(type)) {
                            MediaLibraryConfig.Tag t = new MediaLibraryConfig.Tag();
                            t.setDescription(description);
                            t.setId(id);
                            voiceTag.add(t);
                        }
                    }
                }
            }
            config.setImageTagList(imageTag);
            config.setVoiceTagList(voiceTag);
        }

        return config;
    }

    /**
     * @param ctx
     * @param tag
     * @param file
     * @param jobType     1= adhoc message 2=job request 3=PM
     * @param contentType 1 = Image 2 = Voice 3 = Others
     * @return
     */
    public boolean postMediaLibraryContent(Context ctx, MediaLibraryConfig.Tag tag, File file, int jobType, int contentType,
                                           String jobId) {

        if (McConstants.OFFLINE_MODE || file == null || file.exists() == false || file.length() <= 0) {
            return true;
        }

        String methodName = POST_MEDIA_LIBRARY_CONTENT_REQUEST;
        SoapObject request = new SoapObject(NAMESPACE, methodName);
        request.addProperty("HTID", ConfigContext.getInstance().getPropertyId());
        request.addProperty("TagID", tag == null ? "-1" : tag.getId());
        request.addProperty("JobType", jobType);
        request.addProperty("JobID", jobId);
        request.addProperty("ContentType", contentType);
        request.addProperty("ContentExtension", McUtils.getExtension(file));
        request.addProperty("Content", McUtils.encodeFileToBase64(file));

        SoapObject req = new SoapObject(NAMESPACE, "PostMediaLibraryContent");
        req.addSoapObject(request);

        String soapAction = NAMESPACE + "PostMediaLibraryContent";
        SoapObject response = callWebService(req, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "PostMediaLibraryContentResponse", true);
        if (response != null) {
            SoapObject result = SoapHelper.getInnerSoapObject(response, 0, 0);
            String re = SoapHelper.getStringProperty(result, "ReturnCode", "1");
            if ("0".equalsIgnoreCase(re)) {
                return true;
            }
        }

        return false;
    }

    public static class MediaWrapper {
        public File image;
        public File voice;

        public ArrayList<File> imageList;
        public ArrayList<File> voiceList;

        public MediaWrapper() {
            imageList = new ArrayList<>();
            voiceList = new ArrayList<>();
        }

        public String[] getImages() {
            try {
                String[] array = new String[imageList.size()];
                int i = 0;
                for (File file : imageList) {
                    if (file != null) {
                        array[i++] = file.getAbsolutePath();
                    }
                }
                return array;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        public String[] getVoices() {
            try {
                String[] array = new String[voiceList.size()];
                int i = 0;
                for (File file : voiceList) {
                    if (file != null) {
                        array[i++] = file.getAbsolutePath();
                    }
                }
                return array;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    /**
     * @param ctx
     * @param id
     * @param type 1 = Adhoc Message 2 = Job Request
     * @return
     */
    public MediaWrapper getMediaLibraryContent(Context ctx, String id, int type) {

        if (McConstants.OFFLINE_MODE) {
            return null;
        }

        SoapObject voice = new SoapObject(NAMESPACE, "JobRequestDetails");
        voice.addProperty("ID", id);
        voice.addProperty("Type", type);

        MediaWrapper wrap = new MediaWrapper();

        SoapObject listing = new SoapObject(NAMESPACE, "JobRequestListing");
        listing.addSoapObject(voice);

        String methodName = GET_MEDIA_LIBRARY_CONTENT_REQUEST;
        SoapObject request = new SoapObject(NAMESPACE, methodName);
        request.addProperty("HTID", ConfigContext.getInstance().getPropertyId());
        request.addProperty("UserID", SessionContext.getInstance().getFcsUserId());
        request.addSoapObject(listing);

        SoapObject req = new SoapObject(NAMESPACE, "GetMediaLibraryContent");
        req.addSoapObject(request);

        String soapAction = NAMESPACE + "GetMediaLibraryContent";
        SoapObject response = callWebService(req, ConfigContext.getInstance().getServiceUrl(), null, soapAction, NAMESPACE,
                "GetMediaLibraryContentResponse", true);
        if (response != null) {
            SoapObject result = SoapHelper.getInnerSoapObject(response, 0, 0);
            String re = SoapHelper.getStringProperty(result, "ReturnCode", "1");
            if ("0".equalsIgnoreCase(re)) {
                SoapObject mediaList = SoapHelper.getInnerSoapObject(response, 0, "MediaListing");
                if (mediaList != null && mediaList.getPropertyCount() > 0) {
                    for (int i = 0; i < mediaList.getPropertyCount(); i++) {
                        if (mediaList.getProperty(i) instanceof SoapObject) {
                            SoapObject media = (SoapObject) mediaList.getProperty(i);
                            String ctype = SoapHelper.getStringProperty(media, "ContentType", "3");
                            // 1 = Image 2 = Voice 3 = Others
                            if ("1".equals(ctype) || "3".equals(ctype)) {
                                String ext = SoapHelper.getStringProperty(media, "ContentExtension", "jpg");
                                String base64 = SoapHelper.getStringProperty(media, "Content", null);
                                File file = McUtils.decodeToFile(ctx, base64, ext);
                                if (file != null) {
                                    wrap.image = file;
                                    wrap.imageList.add(file);
                                }
                            } else if ("2".equals(ctype)) {
                                String ext = SoapHelper.getStringProperty(media, "ContentExtension", "mp3");
                                String base64 = SoapHelper.getStringProperty(media, "Content", null);
                                File file = McUtils.decodeToFile(ctx, base64, ext);
                                if (file != null) {
                                    wrap.voice = file;
                                    wrap.voiceList.add(file);
                                }
                            }
                        }
                    }
                }

            }
        }

        return wrap;
    }

}
