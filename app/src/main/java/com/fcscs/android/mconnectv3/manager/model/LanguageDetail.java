package com.fcscs.android.mconnectv3.manager.model;

import java.io.Serializable;

import org.ksoap2.serialization.SoapObject;

import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.common.util.SoapHelper;
import com.j256.ormlite.field.DatabaseField;

public class LanguageDetail implements Serializable {

    @DatabaseField(id = true)
    private String code = "";
    @DatabaseField
    private String description = "";
    @DatabaseField
    private String translationCode = "";
    @DatabaseField
    private String iosValue = "";

    private static final long serialVersionUID = 1L;

    public LanguageDetail() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTranslationCode() {
        return translationCode;
    }

    public void setTranslationCode(String translationCode) {
        this.translationCode = translationCode;
    }

    public LanguageDetail(SoapObject soap) {
        code = SoapHelper.getStringProperty(soap, "Code", "");
        description = SoapHelper.getStringProperty(soap, "Desc", "");
        translationCode = SoapHelper.getStringProperty(soap, "OnlineTranslationCode", "");
    }

    @Override
    public String toString() {
        if (McUtils.isNullOrEmpty(description)) {
            return super.toString();
        } else {
            return description;
        }
    }

    public String getIosValue() {
        return iosValue;
    }

    public void setIosValue(String iosValue) {
        this.iosValue = iosValue;
    }

}
