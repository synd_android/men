package com.fcscs.android.mconnectv3.main;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.BaseActivity;
import com.fcscs.android.mconnectv3.common.McConstants;
import com.fcscs.android.mconnectv3.common.QRCodeParserFactory;
import com.fcscs.android.mconnectv3.common.SessionContext;
import com.fcscs.android.mconnectv3.common.ui.HomeTopBar;
import com.fcscs.android.mconnectv3.common.ui.McProgressAsyncTask;
import com.fcscs.android.mconnectv3.common.ui.RoundCornerListAdapter;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.common.util.PrefsUtil;
import com.fcscs.android.mconnectv3.manager.model.MinibarItem;
import com.fcscs.android.mconnectv3.ws.cconnect.GuestRequestService;
import com.fcscs.android.mconnectv3.ws.cconnect.model.guestrequestservice.FindRoomGuestResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.guestrequestservice.Guest;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.GetMinbarItemsResponse;
import com.fcscs.android.mconnectv3.ws.econnect.ECService;
import com.fcscs.android.mconnectv3.zxing.CaptureActivity;

import java.util.ArrayList;
import java.util.List;

public class ItemizedMinbarActivity extends BaseActivity {
    private HomeTopBar homeTopBar;
    private EditText where;
    private EditText who;
    private EditText qtyET;
    private Spinner allMinbarItemsSp;
    private TextView totalTV;

    private TextView add;
    private TextView submit;
    private ListView itemLv;
    private Guest currentGuest;
    private List<MinibarItem> allMinibarItems = new ArrayList<MinibarItem>();
    private List<MinibarItem> minibarItems = new ArrayList<MinibarItem>();
    MinibarAdapter adapter;
    int selectedPositon;
    protected PostMinibarLoader task;
    private GuestLoader task1;
    private MinibarItemLoader task2;
    ImageView qrButton;

    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.itemized_minibar);
        homeTopBar = (HomeTopBar) findViewById(R.id.top_bar);
        homeTopBar.getTitleTv().setText(R.string.itemized_minibar);

        where = (EditText) findViewById(R.id.itemized_minibar_room_num);
        where.setClickable(true);
        where.setHint(R.string.room_num);
        where.setInputType(InputType.TYPE_CLASS_NUMBER);
        where.setOnKeyListener(new OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                String term = where.getText().toString().trim();
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:
                            findGuestByRoomnum(term, false);
                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        });
        qrButton = (ImageView) findViewById(R.id.qr_camera);
        qrButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent i = new Intent(ItemizedMinbarActivity.this, CaptureActivity.class);
                i.putExtra("type", QRCodeParserFactory.MINIBAR_REQUEST);
                startActivityForResult(i, QR_CODE);
            }
        });
        qrButton.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                boolean searchAll = where.getText().toString().length() == 0;
                findGuestByRoomnum(where.getText().toString(), searchAll);
                return true;
            }
        });
        boolean enable = PrefsUtil.getQRCodeMinibar(this);
        if (enable == false) {
            qrButton.setVisibility(View.GONE);
        }

        who = (EditText) findViewById(R.id.itemized_minibar_guestName);
        who.setHint(getString(R.string.guestname_isvip));
        who.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (currentGuest == null) {
                    toast(getString(R.string.please_select_guest));
                } else {
                    Intent i = new Intent(ItemizedMinbarActivity.this, GuestDetails.class);
                    i.putExtra("guest", currentGuest);
                    startActivity(i);
                }
            }
        });
        who.setEnabled(false);

        allMinbarItemsSp = (Spinner) findViewById(R.id.itemized_minibar_items_spinner);

        allMinbarItemsSp.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                selectedPositon = pos;
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });

        qtyET = (EditText) findViewById(R.id.itemized_minibar_qty);
        qtyET.setText("1");

        itemLv = (ListView) findViewById(R.id.itemized_minibar_itemListView);
        itemLv.setSelector(R.drawable.spacebar);
        itemLv.setClickable(true);
        itemLv.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
            }
        });
        itemLv.setEmptyView(findViewById(R.id.tv_empty));

        adapter = new MinibarAdapter(getCurrentContext(), minibarItems, R.layout.itemized_minbar_list_item);
        itemLv.setAdapter(adapter);

        totalTV = (TextView) findViewById(R.id.itemized_minbar_total);

        add = (TextView) findViewById(R.id.itemized_minibar_add);
        add.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isCConnect()) {
                    // todo
                } else {
                    MinibarItem selectedItem = allMinibarItems.get(selectedPositon);

                    MinibarItem newItem = new MinibarItem();
                    newItem.setId(selectedItem.getId());
                    newItem.setName(selectedItem.getName());
                    newItem.setPrice(selectedItem.getPrice());
                    Integer qty = 1;
                    try {
                        qty = Integer.valueOf(qtyET.getText().toString());
                        if (qty < 1 || qty > 10000) {
                            qtyET.requestFocus();
                            toast(R.string.please_input_a_valid_integer);
                            return;
                        }
                    } catch (Exception e) {
                        qtyET.requestFocus();
                        toast(R.string.please_input_a_valid_integer);
                        return;
                    }
                    newItem.setQty(qty);
                    newItem.setCost(newItem.getPrice() * newItem.getQty());

                    boolean existed = false;
                    for (MinibarItem m : minibarItems) {
                        if (m.getId() == selectedItem.getId()) {
                            existed = true;
                            m.setQty(m.getQty() + newItem.getQty());
                            m.setCost((double) m.getPrice() * m.getQty());
                        }
                    }
                    if (!existed) {
                        minibarItems.add(newItem);
                    }
                    adapter.notifyDataSetChanged();
                    McUtils.adjustListViewHeight(itemLv, adapter);

                    updateTotalCost();

                    qtyET.setText("1");
                    if (allMinbarItemsSp.getCount() > 0) {
                        allMinbarItemsSp.setSelection(0);
                    }
                }
            }
        });

        submit = (TextView) findViewById(R.id.itemized_minibar_submit);
        submit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (currentGuest == null) {
                    where.requestFocus();
                    toast(R.string.please_choose_a_valid_guest);
                    return;
                }

                if (minibarItems == null || minibarItems.size() == 0) {
                    toast(R.string.please_add_at_least_one_item);
                    return;
                }

                if (task == null || AsyncTask.Status.RUNNING.equals(task.getStatus()) == false) {
                    task = new PostMinibarLoader(getCurrentContext(), currentGuest.getRoomNum(), minibarItems);
                    task.exec();
                }
            }
        });

        if (task2 == null || AsyncTask.Status.RUNNING.equals(task2.getStatus()) == false) {
            task2 = new MinibarItemLoader(getCurrentContext());
            task2.exec();
        }
        updateTotalCost();

    }

    final static int QR_CODE = 1001;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == QR_CODE && resultCode == RESULT_OK) {
            Bundle bundle = data.getExtras();
            String result = bundle.getString("result");
            findGuestByRoomnum(result, false);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void updateTotalCost() {
        double totalCost = 0.0;
        for (MinibarItem e : minibarItems) {
            totalCost = totalCost + e.getCost();
        }
        totalTV.setText("$" + String.format("%.2f", totalCost));
    }

    private void findGuestByRoomnum(String term, boolean searchAll) {

        if (!searchAll) {
            if (term == null || term.trim().length() < 1) {
                populateInfo(null);
                return;
            }
        }

        if (task1 == null || AsyncTask.Status.RUNNING.equals(task1.getStatus()) == false) {
            task1 = new GuestLoader(getCurrentContext(), term);
            task1.exec();
        }

    }

    private void populateInfo(Guest guest) {
        if (guest != null) {

            if (isCConnect()) {
                where.setText(guest.getExtNo());
            } else {
                where.setText(guest.getRoomNum());
            }
            who.setText(guest.getGuestName() + "(" + guest.getVipMsg() + ")");
            qtyET.requestFocus();
            currentGuest = guest;
        } else {
            who.setText("");
            currentGuest = null;
            where.requestFocus();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            setResult(RESULT_OK);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    class MinibarItemLoader extends McProgressAsyncTask {

        private GetMinbarItemsResponse response;

        public MinibarItemLoader(Context context) {
            super(context);
        }

        @Override
        public void doInBackground() {
            if (isEconnect()) {
//				response = ECService.getInstance().getMinibarItems(getCurrentContext());
                response = ECService.getInstance().getMinibarConfigurationItems(getCurrentContext(), "");
            }
        }

        @Override
        public void onPostExecute() {
            if (response != null) {
                allMinibarItems = response.getMinibarItems();
                final ArrayAdapter<String> allMinbarItemsAdapter = new ArrayAdapter<String>(getCurrentContext(),
                        android.R.layout.simple_spinner_item);
                allMinbarItemsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                for (MinibarItem item : allMinibarItems) {
                    allMinbarItemsAdapter.add(item.getName() + " $" + item.getPrice());
                }
                allMinbarItemsSp.setAdapter(allMinbarItemsAdapter);
            }
        }

    }

    class PostMinibarLoader extends McProgressAsyncTask {

        private String roomNo;
        private List<MinibarItem> items;
        private Boolean result;

        public PostMinibarLoader(Context context, String roomNo, List<MinibarItem> items) {
            super(context);
            this.roomNo = roomNo;
            this.items = items;
        }

        @Override
        public void doInBackground() {
            if (isCConnect()) {
                // todo
            } else {
                result = ECService.getInstance().postMinibar(getCurrentContext(), roomNo, items);
            }
        }

        @Override
        public void onPostExecute() {
            if (result == true) {
                toast(getString(R.string.update_successfully));
                finish();
            } else {
                toast(getString(R.string.update_fail));
            }
        }

    }

    class GuestLoader extends McProgressAsyncTask {

        private String roomNo;
        private FindRoomGuestResponse res;

        public GuestLoader(Context context, String roomNo) {
            super(context);
            this.roomNo = roomNo;
        }

        @Override
        public void doInBackground() {
            if (isCConnect()) {
                res = GuestRequestService.getInstance().findRoomGuest(getCurrentContext(), roomNo);
            } else {
//				res = ECService.getInstance().getRoomList(getCurrentContext(), roomNo, true);
                if (McConstants.UNIQUE_GETROOMSLISTING) {
                    if (roomNo.matches("[0-9]+")) {
                        res = ECService.getInstance().getRoomList(getCurrentContext(), SessionContext.getInstance().getFcsUserId(), roomNo, "");
                    } else {
                        res = ECService.getInstance().getRoomList(getCurrentContext(), SessionContext.getInstance().getFcsUserId(), "", roomNo);
                    }
                } else {
                    res = ECService.getInstance().getRoomList(getCurrentContext(), SessionContext.getInstance().getFcsUserId(), roomNo, roomNo); //According to Phua, both RoomNo and GuestName should pass RoomNo
                }
            }

        }

        @Override
        public void onPostExecute() {
            if (res != null) {
                final List<Guest> guests = res.getGuests();
                if (guests.size() > 1) {
                    List<String> list = new ArrayList<String>();
                    for (Guest guest : guests) {
                        String name = guest.getGuestName().trim();
                        if (name != null && !name.equalsIgnoreCase("")) {
                            list.add("(" + guest.getRoomNum() + ")\n" + name);
                        } else {
                            list.add("(" + guest.getRoomNum() + ")");
                        }
                    }
                    String[] items = new String[list.size()];
                    items = (String[]) list.toArray(items);

                    AlertDialog.Builder builder = new AlertDialog.Builder(getCurrentContext());
                    builder.setTitle(getString(R.string.label_select_guest));
                    builder.setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (which > -1 && which < guests.size()) {
                                populateInfo(guests.get(which));
                                dialog.cancel();
                                hideSoftInput(where);
                            }
                        }
                    });
                    builder.show();
                } else if (guests.size() == 1) {
                    populateInfo(guests.get(0));
                    hideSoftInput(where);
                } else if (guests.size() == 0) {
                    populateInfo(null);
                    Toast.makeText(getCurrentContext(), R.string.please_input_correctnum, Toast.LENGTH_LONG).show();
                }
            }
        }

    }

    class MinibarAdapter extends RoundCornerListAdapter<MinibarItem> {

        public MinibarAdapter(Context context, List<MinibarItem> list, int layout) {
            super(context, list, layout);
        }

        @Override
        public void onBindView(View view, Context context, int position, final MinibarItem item) {
            String nameQty = item.getName() + " * " + item.getQty();
            TextView name = (TextView) view.findViewById(R.id.minbar_item_name);
            name.setText(nameQty);

            String price = "$" + String.format("%.2f", item.getCost());
            TextView priceTV = (TextView) view.findViewById(R.id.minbar_item_price);
            priceTV.setText(price);

            ImageView del = (ImageView) view.findViewById(R.id.item_del);

            del.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    getItemList().remove(item);
                    updateTotalCost();
                    notifyDataSetChanged();

                }
            });

        }

    }

    @Override
    public boolean isLoginActivity() {
        return true;
    }

    @Override
    public boolean isHomeActivity() {
        return false;
    }

}
