package com.fcscs.android.mconnectv3.engineering;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.BaseActivity;
import com.fcscs.android.mconnectv3.common.McConstants;
import com.fcscs.android.mconnectv3.common.util.McUtils;
//import com.github.barteksc.pdfviewer.PDFView;
//import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
//import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
//import com.github.barteksc.pdfviewer.listener.OnRenderListener;
//import com.shockwave.pdfium.PdfDocument;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

public class PDFReaderActivity extends BaseActivity {
//    PDFView pdfView;
//    Integer pageNumber = 0;
//    String pdfFileName="";
//
//    @Override
//    public void loadComplete(int nbPages) {
//        PdfDocument.Meta meta = pdfView.getDocumentMeta();
//        printBookmarksTree(pdfView.getTableOfContents(), "-");
//    }
//    public void printBookmarksTree(List<PdfDocument.Bookmark> tree, String sep) {
//        for (PdfDocument.Bookmark b : tree) {
//
//            Log.e("PDFReaderActivity", String.format("%s %s, p %d", sep, b.getTitle(), b.getPageIdx()));
//
//            if (b.hasChildren()) {
//                printBookmarksTree(b.getChildren(), sep + "-");
//            }
//        }
//    }
//    @Override
//    public void onPageChanged(int page, int pageCount) {
//        pageNumber = page;
//        setTitle(String.format("%s %s / %s", pdfFileName, page + 1, pageCount));
//    }

    @Override
    public boolean isLoginActivity() {
        return false;
    }

    @Override
    public boolean isHomeActivity() {
        return false;
    }

    public static final String KEY_PATH_FILE = "KEY_PATH_FILE_PDF";
    String urlFile;
    public static final int PERMISSION_CODE = 42042;

    public static final String READ_EXTERNAL_STORAGE = "android.permission.READ_EXTERNAL_STORAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdfreader);
//        pdfView = (PDFView) findViewById(R.id.pdfView);
        Bundle bundle = getIntent().getExtras();
        urlFile = bundle.getString(KEY_PATH_FILE);
        int permissionCheck = ContextCompat.checkSelfPermission(this,
                READ_EXTERNAL_STORAGE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{READ_EXTERNAL_STORAGE},
                    PERMISSION_CODE
            );

            return;
        }
//        pdfFromFile();
        pdfFromUrl();
//        openOtherApp();
//        pdfFromFile();
    }

    private void pdfFromUrl() {
        String url = "https://s3-ap-southeast-1.amazonaws.com/fcscconnect/1_FSDH/Report/Report/00f92e0e-1cdb-4875-a11f-4db2d0a18170.pdf";
        Uri uri = Uri.parse(url);
//        pdfView.fromUri(uri);
    }

    private void pdfFromFile() {
        File pdfFile = new File(McUtils.getPathDownload(this, urlFile));
        try {
            FileInputStream fileInputStream = new FileInputStream(pdfFile);
//            pdfView.fromStream(fileInputStream);
        } catch (FileNotFoundException ex) {
            Log.e("pdfFromFile", ex.getMessage());
        }


//        if (!TextUtils.isEmpty(urlFile)) {
//            if(McUtils.fileExists(this, urlFile)){
//                String path = McUtils.getPathDownload(this, urlFile);
//                File file = new File(path);
//                pdfView.fromFile(file);
//            }
//
//        }
    }

    private void openOtherApp() {
//        String fileName = "00f92e0e-1cdb-4875-a11f-4db2d0a18170.pdf";  // -> maven.pdf
//        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
//        File folder = new File(extStorageDirectory, "testthreepdf");
//        File pdfFile = new File(folder, fileName);
        File pdfFile = new File(McUtils.getPathDownload(this, urlFile));
//        String path = McUtils.getPathDownload(this, urlFile);
//        File file = new File(path);
        Intent target = new Intent(Intent.ACTION_VIEW);
        target.setDataAndType(Uri.fromFile(pdfFile), "application/pdf");
        target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

        Intent intent = Intent.createChooser(target, "Open File");
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            // Instruct the user to install a PDF reader here, or something
            Log.e("openOtherApp", e.getMessage());
        }
    }
}
