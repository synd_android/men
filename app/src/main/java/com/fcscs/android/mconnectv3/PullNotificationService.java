package com.fcscs.android.mconnectv3;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.fcscs.android.mconnectv3.common.McConstants;
import com.fcscs.android.mconnectv3.common.McEnums;
import com.fcscs.android.mconnectv3.common.ServiceExceptionHandler;
import com.fcscs.android.mconnectv3.common.SessionContext;
import com.fcscs.android.mconnectv3.main.HomeActivity;

public class PullNotificationService extends Service {
    private final String TAG = "Pulling Service";
    private static Context ctx;
    private PullJobReceiver jobRecv;
    private PullMsgReceiver msgRecv;
//    private CheckLoginReceiver checkLoginRecv;

    private static Boolean started = true;

    public static void register(Context c) {
        if (c != null) {
            ctx = c.getApplicationContext();
            ServiceExceptionHandler.setHandler(ctx);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "OnBind()");
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        synchronized (started) {
            started = false;
        }
        jobRecv = new PullJobReceiver();
        msgRecv = new PullMsgReceiver();
//        checkLoginRecv = new CheckLoginReceiver();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        synchronized (started) {
            started = false;
        }
        jobRecv.cancelAlarm(ctx);
        msgRecv.cancelAlarm(ctx);
//        checkLoginRecv.cancelAlarm(ctx);
        stopForeground(true);
    }

    @SuppressLint("StringFormatInvalid")
    @Override
    public void onStart(Intent intent, int startId) {

        synchronized (started) {
            if (started) {
                return;
            }
            started = true;
        }

        super.onStart(intent, startId);

        jobRecv.setAlarm(ctx);
        msgRecv.setAlarm(ctx);
//        checkLoginRecv.setAlarm(ctx);

//        String name = getString(R.string.app_name);
//        String login = getString(R.string.user_is_logged_in);
//        String username = SessionContext.getInstance().getUsername();
//        String contentText = String.format(login, username);
//
//        int drawableIcon = R.drawable.pro_icon;
//        if (McConstants.COMPILATION_PRODUCT == McEnums.CompilationProduct.PERCIPIA) {
//            drawableIcon = R.drawable.pro_icon_green_percipia;
//        } else if (McConstants.COMPILATION_PRODUCT == McEnums.CompilationProduct.MENGINEERING) {
//            drawableIcon = R.drawable.pro_icon_en;
//        }
//
//        Intent i = new Intent(this, HomeActivity.class);
//        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//
//
//        PendingIntent pi = PendingIntent.getActivity(this, 0, i, 0);
//
//        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
//                .setSmallIcon(drawableIcon)
//                .setContentTitle(name)
//                .setContentText(contentText)
//                .setContentIntent(pi);
//        Notification note = mBuilder.build();
//        note.flags |= Notification.FLAG_NO_CLEAR;
//        startForeground(1337, note);


        regisPush();
    }

    @SuppressLint("StringFormatInvalid")
    private void regisPush() {


        int drawableIcon = R.drawable.pro_icon;
        if (McConstants.COMPILATION_PRODUCT == McEnums.CompilationProduct.PERCIPIA) {
            drawableIcon = R.drawable.pro_icon_green_percipia;
        } else if (McConstants.COMPILATION_PRODUCT == McEnums.CompilationProduct.MENGINEERING) {
            drawableIcon = R.drawable.pro_icon_en;
        }

        String name = getString(R.string.app_name);
        String login = getString(R.string.user_is_logged_in);
        String username = SessionContext.getInstance().getUsername();
        String contentText = String.format(login, username);


        Intent i = new Intent(this, HomeActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, i, 0);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String CHANNEL = "1337";
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL, "BlueBolt Pocket Mode", NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(notificationChannel);

            Notification.Builder builder = new Notification.Builder(this, CHANNEL);
            builder.setContentTitle(name)
                    .setContentText(contentText)
                    .setSmallIcon(drawableIcon)
                    .setContentIntent(pendingIntent)
                    .setOngoing(true);
            Notification note = builder.build();
            note.flags |= Notification.FLAG_NO_CLEAR;
            startForeground(1337, note);
        } else {
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(drawableIcon)
                    .setContentTitle(name)
                    .setContentText(contentText)
                    .setContentIntent(pendingIntent);
            Notification note = mBuilder.build();
            note.flags |= Notification.FLAG_NO_CLEAR;
            startForeground(1337, note);
        }
    }

}
