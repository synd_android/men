package com.fcscs.android.mconnectv3.manager.model;

import java.io.Serializable;

public class GroupMessageIdModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2887394992085112834L;
    private Long groupId;
    private long runnerId;

    public GroupMessageIdModel() {
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public long getRunnerId() {
        return runnerId;
    }

    public void setRunnerId(long runnerId) {
        this.runnerId = runnerId;
    }

}
