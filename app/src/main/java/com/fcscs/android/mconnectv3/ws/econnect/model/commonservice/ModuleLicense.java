package com.fcscs.android.mconnectv3.ws.econnect.model.commonservice;

import java.io.Serializable;

public class ModuleLicense implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2108944487081427158L;

    private boolean myJob = false;
    private boolean adhoc = false;
    private boolean viewJob = false;
    private boolean sendMessage = false;
    private boolean jobRequest = false;
    private boolean guestRequest = false;
    private boolean nonGuestRequest = false;
    private boolean interDeptRequest = false;
    private boolean engineeringRequest = true;
    private boolean guestDetail = false;
    private boolean minibar = false;
    private boolean roomStatus = false;
    private boolean favorite = false;

    public boolean isMyJob() {
        return myJob;
    }

    public void setMyJob(boolean myJob) {
        this.myJob = myJob;
    }

    public boolean isMyMessage() {
        return adhoc;
    }

    public void setMyMessage(boolean adhoc) {
        this.adhoc = adhoc;
    }

    public boolean isJobSearch() {
        return viewJob;
    }

    public void setJobSearch(boolean viewJob) {
        this.viewJob = viewJob;
    }

    public boolean isSendMessage() {
        return sendMessage;
    }

    public void setSendMessage(boolean sendMessage) {
        this.sendMessage = sendMessage;
    }

    public boolean isJobRequest() {
        return jobRequest;
    }

    public void setJobRequest(boolean jobRequest) {
        this.jobRequest = jobRequest;
    }

    public boolean isGuestDetail() {
        return guestDetail;
    }

    public void setGuestDetail(boolean guestDetail) {
        this.guestDetail = guestDetail;
    }

    public boolean isMinibar() {
        return minibar;
    }

    public void setMinibar(boolean minibar) {
        this.minibar = minibar;
    }

    public boolean isRoomStatus() {
        return roomStatus;
    }

    public void setRoomStatus(boolean roomStatus) {
        this.roomStatus = roomStatus;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public boolean isGuestRequest() {
        return guestRequest;
    }

    public void setGuestRequest(boolean guestRequest) {
        this.guestRequest = guestRequest;
    }

    public boolean isNonGuestRequest() {
        return nonGuestRequest;
    }

    public boolean isEngineeringRequest() {
        return engineeringRequest;
    }

    public void setEngineeringRequest(boolean engineeringRequest) {
        this.engineeringRequest = engineeringRequest;
    }

    public void setNonGuestRequest(boolean nonGuestRequest) {
        this.nonGuestRequest = nonGuestRequest;
    }

    public boolean isInterDeptRequest() {
        return interDeptRequest;
    }

    public void setInterDeptRequest(boolean interDeptRequest) {
        this.interDeptRequest = interDeptRequest;
    }

}
