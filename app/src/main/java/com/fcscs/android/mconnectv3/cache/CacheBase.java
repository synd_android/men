package com.fcscs.android.mconnectv3.cache;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public abstract class CacheBase<K, V> {

    private int maxSize = 0;

    private Map<K, V> cache = Collections.synchronizedMap(new LinkedHashMap<K, V>(101, .75F, true) {

        /**
         *
         */
        private static final long serialVersionUID = -4060400315317558439L;

        @SuppressWarnings("rawtypes")
        protected boolean removeEldestEntry(Map.Entry eldest) {
            return size() > maxSize;
        }

    });

    public void clear() {
        cache.clear();
    }

    public CacheBase(int maxSize) {
        this.maxSize = maxSize;
    }

    public V get(K key) {
        return cache.get(key);
    }

    public void put(K key, V value) {
        cache.put(key, value);
    }

    public void remove(K key) {
        cache.remove(key);
    }

    public boolean containsKey(K key) {
        return cache.containsKey(key);
    }

}
