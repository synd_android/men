package com.fcscs.android.mconnectv3.manager.model;

import java.io.Serializable;

@SuppressWarnings("unused")
public class MinibarItem implements Serializable {

//	<getMinibarItemss>
//	<ID>99</ID>
//	<Name>Local Beer</Name>
//	<Price>25.4</Price>
//	</getMinibarItemss>

    private static final long serialVersionUID = -4745865410748683800L;

//	public static final String ID = "ID";
//	public static final String NAME = "Name";
//	public static final String PRICE = "Price";

    private long id;
    private String name;
    private double price;
    private int qty;
    private double cost;

    public MinibarItem() {
        qty = 1;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getQty() {
        return qty;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public double getCost() {
        return cost;
    }


}
