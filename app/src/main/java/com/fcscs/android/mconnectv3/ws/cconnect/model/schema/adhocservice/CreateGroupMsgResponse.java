package com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice;

import java.io.Serializable;
import java.util.Date;

import org.ksoap2.serialization.SoapObject;

import com.fcscs.android.mconnectv3.common.util.DateTimeHelper;
import com.fcscs.android.mconnectv3.common.util.SoapHelper;

public class CreateGroupMsgResponse implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5959400841441639917L;
    private String adHocMsgId;

    //econnect
    private boolean success;
    private String returnCode;
    private String error;
    private Date created;

    public CreateGroupMsgResponse() {
    }

    public CreateGroupMsgResponse(SoapObject soap) {

        Long id = SoapHelper.getLongAttribute(soap, "adHocMsgId", 0L);
        this.adHocMsgId = SoapHelper.getStringAttribute(soap, "adHocMsgId", "");
        this.success = id > 0;
        this.setError(SoapHelper.getStringProperty(soap, "error", ""));
        this.setCreated(SoapHelper.getDateProperty(soap, "createdTime", DateTimeHelper.DEFAULT_FORMATTER_DATETIME_24H, DateTimeHelper.getDateByTimeZone(new Date())));
    }

    public String getAdHocMsgId() {
        return adHocMsgId;
    }

    public void setAdHocMsgId(String adHocMsgId) {
        this.adHocMsgId = adHocMsgId;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

}
