package com.fcscs.android.mconnectv3.common;

import java.io.Serializable;
import java.util.HashMap;

import android.content.Context;

import com.fcscs.android.mconnectv3.McApplication;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.dao.entity.DBHelper;
import com.fcscs.android.mconnectv3.dao.entity.DBSessionContext;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.ModuleLicense;

public class SessionContext implements Serializable {


    /**
     *
     */
    private static final long serialVersionUID = -8209819259134469087L;

    private static SessionContext instance;

    private long fcsUserId;
    private String wsSessionId;
    private long orgId;
    private Long propId;
    private String username;
    private String password;
    private String name;
    private boolean isRemember;
    private boolean isLogin;
    private int messageCount;
    private int jobCount;
    private int reAssignRunner = 0;
    private String userType;
    private String userDepartment;

    public String getUserDepartment() {
        return userDepartment;
    }

    public void setUserDepartment(String userDepartment) {
        this.userDepartment = userDepartment;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserTimeZone() {
        return userTimeZone;
    }

    public void setUserTimeZone(String userTimeZone) {
        this.userTimeZone = userTimeZone;
    }

    private String userTimeZone;
    private HashMap<Integer, McEnums.StatusColor> statusColorMap;

    private ModuleLicense module = null;

    private SessionContext() {
        resetSession();
    }

    public static SessionContext getInstance() {
        if (instance == null) {
            instance = new SessionContext();
            instance.populate(McApplication.application);
        }
        return instance;
    }

    public SessionContext populate(Context context) {

        resetSession();

        DBSessionContext ss = DBHelper.getSessionContext(context);
        this.fcsUserId = McUtils.isNullOrEmpty(ss.getUserId()) ? 0L : Long.valueOf(ss.getUserId());
        this.wsSessionId = ss.getSessionToken();
        this.username = ss.getUsername();
        this.name = ss.getFullname();
        this.password = ss.getPassword();
        this.isLogin = ss.getLogin();
        this.isRemember = ss.getRemember();
        this.module = new ModuleLicense();

        this.statusColorMap = new HashMap<Integer, McEnums.StatusColor>();
        this.statusColorMap.put(1, McEnums.StatusColor.GREEN);
        this.statusColorMap.put(2, McEnums.StatusColor.RED);
        this.statusColorMap.put(3, McEnums.StatusColor.BLUE);
        this.statusColorMap.put(4, McEnums.StatusColor.PURPLE);
        this.statusColorMap.put(5, McEnums.StatusColor.BROWN);
        this.statusColorMap.put(6, McEnums.StatusColor.DARKCYAN);
        this.statusColorMap.put(7, McEnums.StatusColor.ORANGE);
        this.statusColorMap.put(8, McEnums.StatusColor.LIMEADE);

        if (fcsUserId > 0) {
            module.setMyMessage(ss.getViewMessage());
            module.setFavorite(ss.getFavorite());
            module.setGuestDetail(ss.getGuestDetail());
            module.setGuestRequest(ss.getGuestReuqest());
            module.setInterDeptRequest(ss.getInterDepartmentRequest());
            module.setNonGuestRequest(ss.getNonGuestRequest());
            module.setJobRequest(ss.getNewRequest());
            module.setMinibar(ss.getMinibar());
            module.setMyJob(ss.getMyJob());
            module.setRoomStatus(ss.getRoomStatus());
            module.setSendMessage(ss.getNewMessage());
            module.setJobSearch(ss.getJobSearch());
        }

        return this;
    }

    public int getReAssignRunner() {
        return reAssignRunner;
    }

    public void setReAssignRunner(int reAssignRunner) {
        this.reAssignRunner = reAssignRunner;
    }

    public void persist(Context context) {

        DBSessionContext ss = DBHelper.getSessionContext(context);
        ss.setUserId("" + this.fcsUserId);
        ss.setUsername(username == null ? "" : username);
        ss.setPassword(password == null ? "" : password);
        ss.setFullname(name == null ? ss.getUsername() : name);
        ss.setSessionToken(wsSessionId == null ? "" : wsSessionId);
        ss.setLogin(this.isLogin);
        ss.setRemember(this.isRemember);

        if (module != null) {

            ss.setMyJob(module.isMyJob());
            ss.setMyMessage(module.isMyMessage());
            ss.setJobSearch(module.isJobSearch());
            ss.setNewMessage(module.isSendMessage());
            ss.setNewRequest(module.isJobRequest());
            ss.setGuestReuqest(module.isGuestRequest());
            ss.setNonGuestRequest(module.isNonGuestRequest());
            ss.setInterDepartmentRequest(module.isInterDeptRequest());
            ss.setFavorite(module.isFavorite());
            ss.setGuestDetail(module.isGuestDetail());
            ss.setMinibar(module.isMinibar());
            ss.setRoomStatus(module.isRoomStatus());

            if (ss.getGuestReuqest() || ss.getNonGuestRequest() || ss.getInterDepartmentRequest()) {
                ss.setNewRequest(true);
            }

        }

        DBHelper.saveSessionContext(context, ss);

    }

    /**
     * reset all the fields to default
     */
    public void resetSession() {
        fcsUserId = -1;
        wsSessionId = "";
        username = "";
        password = "";
        isLogin = false;
        isRemember = false;
        orgId = -1;
        propId = null;
        module = new ModuleLicense();
        messageCount = 0;
        jobCount = 0;
        statusColorMap = null;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isRemember() {
        return isRemember;
    }

    public void setRemember(boolean isRemember) {
        this.isRemember = isRemember;
    }

    public boolean isLogin() {
        return isLogin;
    }

    public void setLogin(boolean isLogin) {
        this.isLogin = isLogin;
    }

    public long getFcsUserId() {
        return fcsUserId;
    }

    public void setFcsUserId(long fcsUserId) {
        this.fcsUserId = fcsUserId;
    }

    public String getWsSessionId() {
        return wsSessionId;
    }

    public void setWsSessionId(String wsSessionId) {
        this.wsSessionId = wsSessionId;
    }

    public long getOrgId() {
        return orgId;
    }

    public void setOrgId(long orgId) {
        this.orgId = orgId;
    }

    public Long getPropId() {
        return propId;
    }

    public void setPropId(Long propId) {
        this.propId = propId;
    }

    public int getMessageCount() {
        return messageCount;
    }

    public void setMessageCount(int messageCount) {
        this.messageCount = messageCount;
    }

    public int getJobCount() {
        return jobCount;
    }

    public void setJobCount(int jobCount) {
        this.jobCount = jobCount;
    }

    public ModuleLicense getModule() {
        return module;
    }

    public void setModule(ModuleLicense module) {
        this.module = module;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HashMap<Integer, McEnums.StatusColor> getStatusColorMap() {
        return statusColorMap;
    }

    public void setStatusColorMap(HashMap<Integer, McEnums.StatusColor> statusColorMap) {
        this.statusColorMap = statusColorMap;
    }
}
