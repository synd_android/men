package com.fcscs.android.mconnectv3.manager.model;

import java.io.Serializable;
import java.util.Date;

import org.ksoap2.serialization.SoapObject;

import com.fcscs.android.mconnectv3.common.util.DateTimeHelper;
import com.fcscs.android.mconnectv3.common.util.SoapHelper;

public class MessageModel implements Serializable {

    // <xs:element name="message" minOccurs="0" maxOccurs="unbounded">
    // <xs:complexType>
    // <xs:sequence>
    // <xs:element type="xs:string" name="FromFirstName" minOccurs="0" />
    // <xs:element type="xs:string" name="FromLastName" minOccurs="0" />
    // <xs:element type="xs:string" name="FromUserName" minOccurs="0" />
    // <xs:element type="xs:dateTime" name="createdDate" minOccurs="0" />
    // <xs:element type="xs:string" name="title" minOccurs="0" />
    // </xs:sequence>
    // <xs:attribute name="msgDoneId" type="xs:long" use="required" />
    // </xs:complexType>
    // </xs:element>

    private static final long serialVersionUID = -4745865410748683800L;

    private String fromUsername;
    private Date createdDate;
    private String title;
    private long msgDoneId;
    private Date ackDate;
    private boolean acknowledged = false;

    private String strNotificationContent;

    public MessageModel() {
    }

    public MessageModel(SoapObject soap) {
        this.fromUsername = SoapHelper.getStringProperty(soap, "FromUserName", "");
        createdDate = SoapHelper.getDateProperty(soap, "createdDate", DateTimeHelper.DEFAULT_FORMATTER_DATETIME_24H, DateTimeHelper.getDateByTimeZone(new Date()));
        this.title = SoapHelper.getStringProperty(soap, "title", "");
        ackDate = SoapHelper.getDateProperty(soap, "AckDate", DateTimeHelper.FORMAT_DATETIME_AA, null);
        this.msgDoneId = SoapHelper.getLongAttribute(soap, "msgDoneId", -1L);
        if (null != ackDate) {
            acknowledged = true;
        } else {
            acknowledged = false;
        }
    }

    public String getFromUsername() {
        return fromUsername;
    }

    public void setFromUsername(String fromUsername) {
        this.fromUsername = fromUsername;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getMsgDoneId() {
        return msgDoneId;
    }

    public void setMsgDoneId(long msgDoneId) {
        this.msgDoneId = msgDoneId;
    }

    public Date getAckDate() {
        return ackDate;
    }

    public void setAckDate(Date ackDate) {
        this.ackDate = ackDate;
    }

    public boolean isAcknowledged() {
        return acknowledged;
    }

    public void setAcknowledged(boolean acknowledged) {
        this.acknowledged = acknowledged;
    }

    //..Create By : Chua Kam Hoong
    public String getNotificationContent() {
        return strNotificationContent;
    }

    //..Create By : Chua Kam Hoong
    public void setNotificationContent(String NotificationContent) {
        strNotificationContent = NotificationContent;
    }
}
