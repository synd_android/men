package com.fcscs.android.mconnectv3.main;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.BaseActivity;
import com.fcscs.android.mconnectv3.common.ui.HomeTopBar;
import com.fcscs.android.mconnectv3.common.ui.ImagePreviewLayout;
import com.fcscs.android.mconnectv3.common.ui.RecordPlayLayout;
import com.fcscs.android.mconnectv3.common.util.DialogHelper;
import com.fcscs.android.mconnectv3.manager.model.MessageWrapper;

public class NewMessageItemDetailActivity extends BaseActivity {


    private HomeTopBar homeTopBar;
    private TextView tvMessage;
    private TextView tvRunner;
    private TextView tvImageTag;
    private TextView tvVoiceTag;
    private RecordPlayLayout recordPlay;
    private int index;
    private MessageWrapper message;
    private ImagePreviewLayout imagePreview;
    private View llImageTag;
    private View llVoiceTag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.common_message_item_detail);

        homeTopBar = (HomeTopBar) findViewById(R.id.top_bar);
        homeTopBar.getTitleTv().setText(R.string.guestdetails);
        homeTopBar.getLeftBtn().setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });
        tvMessage = (TextView) findViewById(R.id.message);
        tvRunner = (TextView) findViewById(R.id.runner);

        llImageTag = findViewById(R.id.ll_image_tag);
        tvImageTag = (TextView) findViewById(R.id.image_tag);
        llImageTag.setVisibility(View.GONE);

        llVoiceTag = findViewById(R.id.ll_voice_tag);
        tvVoiceTag = (TextView) findViewById(R.id.voice_tag);
        llVoiceTag.setVisibility(View.GONE);

        recordPlay = (RecordPlayLayout) findViewById(R.id.ll_record_play);
        imagePreview = (ImagePreviewLayout) findViewById(R.id.ll_image_preview);

        index = getIntent().getIntExtra("index", -1);
        message = MessageWrapper.getInstance();

        if (index < 0 || message == null) {
            finish();
        } else {
            recordPlay.setRecordFile(message.getRecordedVoice());
            tvMessage.setText(message.getContent());
            tvRunner.setText(message.getName());

            if (message.getImageTag() != null) {
                llImageTag.setVisibility(View.VISIBLE);
                tvImageTag.setText(message.getImageTag().getDescription());
            }

            if (message.getVoiceTag() != null) {
                tvVoiceTag.setText(message.getVoiceTag().getDescription());
                llVoiceTag.setVisibility(View.VISIBLE);
            }

            recordPlay.setAllowDelete(true);
            recordPlay.setListener(new RecordPlayLayout.OnClickRecordPlay() {
                @Override
                public void clickPlay(RecordPlayLayout play) {
                }

                @Override
                public void clickPause(RecordPlayLayout play) {
                }

                @Override
                public void clickDelete(RecordPlayLayout play) {
                    DialogHelper.showYesNoDialog(NewMessageItemDetailActivity.this, R.string.confirm, R.string.confirm_delete, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            recordPlay.stopAndHideLayout();
                            message.setRecordedVoice(null);
                        }
                    });
                }
            });
            imagePreview.setImageFile(message.getCapturedImage());
            imagePreview.setAllowDelete(true);
            imagePreview.setOnClickDelete(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogHelper.showYesNoDialog(NewMessageItemDetailActivity.this, R.string.confirm, R.string.confirm_delete, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            message.setCapturedImage(null);
                            imagePreview.setImageFile(null);
                        }
                    });
                }
            });
        }

    }


    @Override
    public boolean isLoginActivity() {
        return true;
    }

    @Override
    public boolean isHomeActivity() {
        return false;
    }

}