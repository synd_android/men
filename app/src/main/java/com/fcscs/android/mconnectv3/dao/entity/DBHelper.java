package com.fcscs.android.mconnectv3.dao.entity;

import java.util.Collection;
import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.common.util.PrefsUtil;
import com.fcscs.android.mconnectv3.dao.entity.DaoMaster.DevOpenHelper;

import de.greenrobot.dao.QueryBuilder;

public class DBHelper {

    public static final String MCONNECT = "mconnectv3";

    public static void clearCache(Context context) {

        DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, MCONNECT, null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster.createAllTables(db, true);
        db.close();

        deleteAllBuildings(context);
        deleteAllFloors(context);
        deleteAllLocations(context);
        deleteAllDepartments(context);
        deleteAllRunners(context);
    }

    public static void deleteNotification(Context context, NotificationModel m) {

        DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, MCONNECT, null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        DaoSession daoSession = daoMaster.newSession();
        NotificationModelDao dao = daoSession.getNotificationModelDao();
        dao.delete(m);
        db.close();

    }


    public static void saveNotification(Context context, NotificationModel lo) {

        if (lo == null) {
            return;
        }

        DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, MCONNECT, null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        DaoSession daoSession = daoMaster.newSession();

        daoSession.getNotificationModelDao().insert(lo);

        db.close();

    }

    public static List<NotificationModel> queryNotifications(Context context, String userId, String product) {

        DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, MCONNECT, null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        DaoSession daoSession = daoMaster.newSession();
        NotificationModelDao dao = daoSession.getNotificationModelDao();
        QueryBuilder<NotificationModel> builder = dao.queryBuilder();
        builder = builder.where(NotificationModelDao.Properties.UserId.eq(userId));
        builder = builder.where(NotificationModelDao.Properties.Product.eq(product));
        List<NotificationModel> list = builder.list();
        db.close();
        return list;
    }

    public static void deleteAllBuildings(Context context) {

        DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, MCONNECT, null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        DaoSession daoSession = daoMaster.newSession();
        daoSession.getBuildingDao().deleteAll();

        db.close();

    }

    public static long getBuildingCount(Context context) {

        DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, MCONNECT, null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        DaoSession daoSession = daoMaster.newSession();
        long cc = daoSession.getBuildingDao().count();

        db.close();

        return cc;
    }

    public static void saveBuildings(Context context, List<Building> list) {

        if (list == null || list.size() == 0) {
            return;
        }

        DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, MCONNECT, null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        DaoSession daoSession = daoMaster.newSession();

        for (Building lo : list) {
            daoSession.getBuildingDao().insert(lo);
        }

        db.close();

    }

    public static List<Building> queryBuildings(Context context) {

        DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, MCONNECT, null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        DaoSession daoSession = daoMaster.newSession();

        List<Building> list = daoSession.getBuildingDao().queryBuilder().list();
        db.close();
        return list;
    }

    public static void deleteAllFloors(Context context) {

        DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, MCONNECT, null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        DaoSession daoSession = daoMaster.newSession();
        daoSession.getFloorDao().deleteAll();

        db.close();

    }

    public static long getFloorCount(Context context) {

        DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, MCONNECT, null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        DaoSession daoSession = daoMaster.newSession();
        long cc = daoSession.getFloorDao().count();

        db.close();

        return cc;
    }

    public static long getRunnerCount(Context context) {

        DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, MCONNECT, null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        DaoSession daoSession = daoMaster.newSession();
        long cc = daoSession.getRunnerDao().count();

        db.close();

        return cc;
    }

    public static void saveFloors(Context context, Collection<Floor> list) {

        if (list == null || list.size() == 0) {
            return;
        }

        DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, MCONNECT, null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        DaoSession daoSession = daoMaster.newSession();

        daoSession.getFloorDao().insertInTx(list);
        db.close();

    }

    public static List<Floor> queryFloors(Context context, String buildingId) {

        DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, MCONNECT, null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        DaoSession daoSession = daoMaster.newSession();
        FloorDao dao = daoSession.getFloorDao();
        QueryBuilder<Floor> builder = dao.queryBuilder();
        if (McUtils.isNullOrEmpty(buildingId) == false) {
            builder = builder.where(FloorDao.Properties.BuildingId.eq(buildingId));
        }

        List<Floor> list = builder.list();
        db.close();
        return list;
    }

    public static void deleteAllLocations(Context context) {

        DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, MCONNECT, null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        DaoSession daoSession = daoMaster.newSession();
        daoSession.getLocationDao().deleteAll();

        db.close();

    }

    public static void deleteAllDepartments(Context context) {

        DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, MCONNECT, null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        DaoSession daoSession = daoMaster.newSession();
        daoSession.getDepartmentDao().deleteAll();
        db.close();

    }

    public static void deleteAllRunners(Context context) {

        DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, MCONNECT, null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        DaoSession daoSession = daoMaster.newSession();
        daoSession.getRunnerDao().deleteAll();
        db.close();

    }

    public static long getLocationCount(Context context) {

        DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, MCONNECT, null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        DaoSession daoSession = daoMaster.newSession();
        long c = daoSession.getLocationDao().count();

        db.close();

        return c;
    }

    public static void saveLocations(Context context, List<Location> list) {

        if (list == null || list.size() == 0) {
            return;
        }

        DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, MCONNECT, null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        DaoSession daoSession = daoMaster.newSession();
        LocationDao dao = daoSession.getLocationDao();

        dao.insertInTx(list);
        db.close();

    }

    public static void saveDepartmentss(Context context, List<Department> list) {

        if (list == null || list.size() == 0) {
            return;
        }

        DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, MCONNECT, null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        DaoSession daoSession = daoMaster.newSession();

        for (Department lo : list) {
            daoSession.getDepartmentDao().insert(lo);
        }

        db.close();

    }

    public static void saveRunners(Context context, List<Runner> list) {

        if (list == null || list.size() == 0) {
            return;
        }

        DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, MCONNECT, null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        DaoSession daoSession = daoMaster.newSession();

        for (Runner lo : list) {
            daoSession.getRunnerDao().insert(lo);
        }

        db.close();

    }

    public static List<Department> queryDepartments(Context context) {

        DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, MCONNECT, null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        DaoSession daoSession = daoMaster.newSession();

        QueryBuilder<Department> builder = daoSession.getDepartmentDao().queryBuilder();
        List<Department> list = builder.list();

        db.close();
        return list;
    }

    public static List<Runner> queryRunners(Context context, String term, Long departmentId) {

        DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, MCONNECT, null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        DaoSession daoSession = daoMaster.newSession();

        QueryBuilder<Runner> builder = daoSession.getRunnerDao().queryBuilder();

        if (McUtils.isNullOrEmpty(departmentId) == false) {
            builder = builder.where(RunnerDao.Properties.DepartmentId.eq(departmentId));
        }

        if (McUtils.isNullOrEmpty(term) == false) {
            builder = builder.where(RunnerDao.Properties.Fullname.like("%" + term + "%"));
        }

        List<Runner> list = builder.list();

        db.close();
        return list;
    }

    //public static List<Location> queryLocations(Context context, int PageSize, int PageIndex, String buildingId, String floorId, String term) {
    public static List<Location> queryLocations(Context context, int PageIndex, String buildingId, String floorId, String term) {

        DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, MCONNECT, null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        DaoSession daoSession = daoMaster.newSession();
        LocationDao dao = daoSession.getLocationDao();

        QueryBuilder<Location> builder = dao.queryBuilder();
        if (McUtils.isNullOrEmpty(buildingId) == false) {
            builder = builder.where(LocationDao.Properties.BuildingId.eq(buildingId));
        }
        if (McUtils.isNullOrEmpty(floorId) == false) {
            builder = builder.where(LocationDao.Properties.FloorId.eq(floorId));
        }
        if (McUtils.isNullOrEmpty(term) == false) {
            builder = builder.whereOr(LocationDao.Properties.LocationCode.like("%" + term + "%"),
                    LocationDao.Properties.Name.like("%" + term + "%"));
        }

		/*
		String strPageSize = String.valueOf(PageSize);
		String strPageIndex = String.valueOf(PageIndex);
		int intTmpPageSize = 0;
		String strTmpPageSize = "";
		int intOffset = PageIndex;
		if(PageIndex == 0){
			intTmpPageSize = PageSize;
			strTmpPageSize = String.valueOf(intTmpPageSize);
		}else{
			if(PageIndex > 0){
				intTmpPageSize = (PageSize * (PageIndex + 1));
				strTmpPageSize = String.valueOf(intTmpPageSize);
			}
		}
		*/

        //..Modify By 	: Chua Kam Hoong
        //..Date 		: 20 May 2015
        //List<Location> list = builder.limit(200).list();
        //List<Location> list = builder.limit(10).list();
        List<Location> list = null;
		/*
		if ((PageSize == 0) && (PageIndex == 0)){
			list = builder.limit(200).list();
		}else{
			list = builder.offset(0).limit(intTmpPageSize).list();
		}
		*/
        list = builder.limit(200).list();
        db.close();
        return list;
    }

    public static DBSessionContext getSessionContext(Context context) {
        return PrefsUtil.getSessionPrefs(context);
    }

    public static void saveSessionContext(Context context, DBSessionContext cc) {
        PrefsUtil.saveSessionPrefs(context, cc);
    }

    public static DBConfigContext getConfigContext(Context context) {
        return PrefsUtil.getConfigPrefs(context);
    }

    public static void saveConfigContext(Context context, DBConfigContext cc) {
        PrefsUtil.saveConfigPrefs(context, cc);
    }

}
