package com.fcscs.android.mconnectv3.manager.model;

import com.fcscs.android.mconnectv3.common.util.SoapHelper;

import org.ksoap2.serialization.SoapObject;

import java.io.Serializable;

/**
 * Created by admin on 10/28/15.
 */
public class GroupLocationInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     *
     */
    private String groupID = "";
    private String Code = "";
    private String Name = "";
    private String HTID = "";

    public GroupLocationInfo() {
    }

    public GroupLocationInfo(SoapObject soap) {
        groupID = SoapHelper.getStringProperty(soap, "ID", "");
        Code = SoapHelper.getStringProperty(soap, "Code", "");
        HTID = SoapHelper.getStringProperty(soap, "HTID", "");
        Name = SoapHelper.getStringProperty(soap, "Name", "");
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getGroupID() {
        return groupID;
    }

    public void setGroupID(String groupID) {
        this.groupID = groupID;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getHTID() {
        return HTID;
    }

    public void setHTID(String HTID) {
        this.HTID = HTID;
    }

}
