package com.fcscs.android.mconnectv3.http;

public class HttpException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 3752033751838612858L;
    private int statusCode = -1;

    public HttpException(Throwable throwable, int statusCode) {
        super(throwable);
        this.statusCode = statusCode;
    }

    public HttpException() {
        super();
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

}
