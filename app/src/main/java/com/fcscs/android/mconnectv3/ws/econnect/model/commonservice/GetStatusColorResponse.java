package com.fcscs.android.mconnectv3.ws.econnect.model.commonservice;

import com.fcscs.android.mconnectv3.common.McEnums;
import com.fcscs.android.mconnectv3.common.util.SoapHelper;

import org.ksoap2.serialization.SoapObject;

import java.io.Serializable;
import java.util.HashMap;

public class GetStatusColorResponse implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4902163054265113607L;
    private HashMap<Integer, McEnums.StatusColor> statusColorMap;

    public GetStatusColorResponse(SoapObject soap) {
        SoapObject soapContent = (SoapObject) soap.getProperty("getStatusColor");
        statusColorMap = new HashMap<Integer, McEnums.StatusColor>();
        for (int i = 0; i < soapContent.getPropertyCount(); i++) {
            if ("SC".equalsIgnoreCase(SoapHelper.getPropertyName(soapContent, i))) {
                SoapObject s = (SoapObject) soapContent.getProperty(i);
                int statusCode = SoapHelper.getIntegerProperty(s, "StatusCode", 0);
                String color = SoapHelper.getStringProperty(s, "Color", "");

                McEnums.StatusColor statusColor = McEnums.StatusColor.BLACK;
                if (color.equalsIgnoreCase("Green")) statusColor = McEnums.StatusColor.GREEN;
                if (color.equalsIgnoreCase("Red")) statusColor = McEnums.StatusColor.RED;
                if (color.equalsIgnoreCase("Blue")) statusColor = McEnums.StatusColor.BLUE;
                if (color.equalsIgnoreCase("Purple")) statusColor = McEnums.StatusColor.PURPLE;
                if (color.equalsIgnoreCase("Brown")) statusColor = McEnums.StatusColor.BROWN;
                if (color.equalsIgnoreCase("DarkCyan")) statusColor = McEnums.StatusColor.DARKCYAN;
                if (color.equalsIgnoreCase("Orange")) statusColor = McEnums.StatusColor.ORANGE;
                if (color.equalsIgnoreCase("Limeade")) statusColor = McEnums.StatusColor.LIMEADE;

                statusColorMap.put(statusCode, statusColor);
            }
        }
    }

    public HashMap<Integer, McEnums.StatusColor> getMap() {
        return statusColorMap;
    }

}
