package com.fcscs.android.mconnectv3.manager.model;

import java.io.Serializable;

import com.fcscs.android.mconnectv3.common.McEnums.JobStatus;

public class StatusModel implements Serializable {

    private static final long serialVersionUID = 29412794977356826L;
    private int count;
    private JobStatus status;

    public StatusModel(int count, JobStatus status) {
        this.count = count;
        this.setStatus(status);
    }

    public StatusModel() {

    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public JobStatus getStatus() {
        return status;
    }

    public void setStatus(JobStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return status == null ? "" : status.toString();
    }

}
