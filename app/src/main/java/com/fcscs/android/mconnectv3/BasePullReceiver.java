package com.fcscs.android.mconnectv3;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.fcscs.android.mconnectv3.common.McConstants;
import com.fcscs.android.mconnectv3.common.McEnums;
import com.fcscs.android.mconnectv3.common.ui.McBackgroundAsyncTask;
import com.fcscs.android.mconnectv3.common.util.DateTimeHelper;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.manager.model.JobModel;
import com.fcscs.android.mconnectv3.manager.model.MessageModel;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.FindMsgResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.JobViewResponse;
import com.fcscs.android.mconnectv3.ws.econnect.ECService;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.GetPendingNotificationResponse;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.PullNotificationModel;

public abstract class BasePullReceiver extends BroadcastReceiver {

    public static final int TYPE_JOB = 1;
    public static final int TYPE_MSG = 2;
    public static final String TAG = BasePullReceiver.class.getSimpleName();
    public static final Integer NO_NETWORK = 3000;
    public static final int MSG_TYPE_JOB = 1;
    public static final int MSG_TYPE_MSG = 2;
    public static final int MSG_TYPE_OTHER_JOB = 3;
    public static final int MSG_TYPE_OTHER_MSG = 4;
    private static String textShowPush = "*";
    private static String paramJobTypePending = "1";
    private static String paramJobTypeAll = "";

    protected static class PullTask extends McBackgroundAsyncTask {

        private Context context;
        private boolean isNoNetwork = true;
        private int returnCode = -1;
        private int type;

        public PullTask(Context context, int type) {
            super(context);
            this.context = context;
            this.type = type;
        }

        @Override
        public void doInBackground() {
            if (context == null) {
                return;
            }

            isNoNetwork = McUtils.isNetworkOff(context);
            if (isNoNetwork) {
                McUtils.saveCrashReport(context, new Throwable("No Network."));
                return;
            } else {
                NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                manager.cancel(NO_NETWORK);
                GetPendingNotificationResponse resp = getPendingNotification(context);
                if (resp != null) {
                    returnCode = resp.getReturnCode();
                    if (returnCode == 0) {
                        List<PullNotificationModel> list = resp.getList();
                        List<PullNotificationModel> jobs = new ArrayList<PullNotificationModel>();
                        List<String> msgs = new ArrayList<String>();
                        List<PullNotificationModel> otherjobs = new ArrayList<PullNotificationModel>();
                        List<PullNotificationModel> vListNotif = null;
                        if (list != null && list.size() > 0) {
                            vListNotif = new ArrayList<PullNotificationModel>();
                            for (PullNotificationModel m : list) {
                                int intMessageType = m.getMessageType();
                                String notificationContent = m.getNotificationContent();
                                if (intMessageType == MSG_TYPE_JOB && this.type == TYPE_JOB) {
                                    jobs.add(m);
                                    vListNotif.add(new PullNotificationModel(m.getJobNo(), notificationContent));
                                } else if (intMessageType == MSG_TYPE_MSG && this.type == TYPE_MSG) {
                                    msgs.add(m.getJobNo());
                                    vListNotif.add(new PullNotificationModel(m.getJobNo(), notificationContent));
                                } else if (intMessageType == MSG_TYPE_OTHER_JOB && this.type == TYPE_JOB) {
                                    otherjobs.add(m);
                                    vListNotif.add(new PullNotificationModel(m.getJobNo(), notificationContent));
                                }
                            }
                        }

                        if (jobs.size() > 0) {
                            prShowJobNotifications(vListNotif);
                        }
                        if (msgs.size() > 0) {
                            prShowMessageNotifications(vListNotif);
                        }
                        if (otherjobs.size() > 0) {
                            prShowOtherJobNotifications(vListNotif);
                        }
                    }
                }
            }
        }

        @Override
        public void onPostExecute() {
            if (isNoNetwork && !McConstants.OFFLINE_MODE) {
                NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                GCMNotification.notifyNoNetwork(context, manager, NO_NETWORK);
                toastMsg(context.getString(R.string.network_connection_unavailable));
            }
        }

        private GetPendingNotificationResponse getPendingNotification(Context context) {
            GetPendingNotificationResponse resp = null;
            try {
                resp = ECService.getInstance().getPendingNotification(context);
            } catch (Throwable e) {
            }
            return resp;
        }


        private void prShowMessageNotifications(List<PullNotificationModel> ListNotif) {

            List<PullNotificationModel> vListNotif = ListNotif;
            FindMsgResponse list = ECService.getInstance().getAdhocLists(context, new Date(), 1);
            if (list != null && list.getMessageList() != null && list.getMessageList().size() > 0) {
                Map<Long, MessageModel> map = new HashMap<Long, MessageModel>();
                for (MessageModel m : list.getMessageList()) {
                    map.put(m.getMsgDoneId(), m);
                }
                NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                for (PullNotificationModel qMdl : vListNotif) {
                    try {
                        long mn = Long.parseLong(qMdl.getJobNo());
                        MessageModel model = map.get(mn);
                        if (model == null) {
                            continue;
                        }
                        model.setNotificationContent(qMdl.getNotificationContent());
                        GCMNotification.showECMsgNotification(context, manager, model, "*");
                    } catch (Exception e) {
                    }
                }
            }

        }

//        private void showLog(String function, List<PullNotificationModel> pullmodel) {
//            for (int i = 0; i < pullmodel.size(); i++) {
//                PullNotificationModel model = pullmodel.get(i);
//                Log.i("showLog", function + " getJobNo " + i + " : " + model.getJobNo());
//            }
//        }

        private void prShowJobNotifications(List<PullNotificationModel> pullmodel) {
            McUtils.saveLogToSDCard("###retrieve jobs...");
            JobViewResponse resp = ECService.getInstance().getJobs(context, "", new Date(), "", "", "", "", "", "", paramJobTypeAll, null);
            if (resp != null && resp.getJobViews() != null && resp.getJobViews().size() > 0) {
                McUtils.saveLogToSDCard("###retrieve jobs size: " + resp.getJobViews().size());
                Map<String, JobModel> map = new HashMap<String, JobModel>();
                for (JobModel j : resp.getJobViews()) {
                    McUtils.saveLogToSDCard("###anaylyze job: " + j.getJobNum() + ", is ack: " + j.getIsAcked());
                    if (j.getStatus().getStatusId() != McEnums.JobStatus.COMPLETED.getStatusId()
                            || j.getStatus().getStatusId() != McEnums.JobStatus.CANCELLED.getStatusId()) {
                        map.put(j.getJobNum(), j);
                    }
                }
                NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                for (PullNotificationModel pm : pullmodel) {
                    JobModel model = map.get(pm.getJobNo());
                    if (model == null) {
                        McUtils.saveLogToSDCard("###show job notification: can't get model from cache, JobNO: " + pm.getJobNo());
                        continue;
                    }
                    model.setNotificationType(pm.getMessageType());
                    model.setNotificationId(pm.getNotificationId());
                    model.setNotificationContent(pm.getNotificationContent());
                    model.setIsRead(false);
                    McUtils.saveLogToSDCard("###show job notification: " + model.getJobNum());

                    GCMNotification.showECJobNotification(context, manager, model, textShowPush, true);
                }
            } else {
                McUtils.saveLogToSDCard("###retrieve none jobs");
            }
        }

        private void prShowOtherJobNotifications(List<PullNotificationModel> pullmodel) {
            McUtils.saveLogToSDCard("###retrieve jobs...");
            JobViewResponse resp = ECService.getInstance().getJobs(context, "", new Date(), "", "", "", "", "", "", "2", null); //
            if (resp != null && resp.getJobViews() != null && resp.getJobViews().size() > 0) {
                McUtils.saveLogToSDCard("###retrieve jobs size: " + resp.getJobViews().size());
                Map<String, JobModel> map = new HashMap<String, JobModel>();
                for (JobModel j : resp.getJobViews()) {
                    McUtils.saveLogToSDCard("###anaylyze job: " + j.getJobNum() + ", is ack: " + j.getIsAcked());
                    map.put(j.getJobNum(), j);
                }
                NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                for (PullNotificationModel pm : pullmodel) {
                    JobModel model = map.get(pm.getJobNo());
                    if (model == null) {
                        McUtils.saveLogToSDCard("###show job notification: can't get model from cache, JobNO: " + pm.getJobNo());
                        continue;
                    }
                    model.setNotificationType(pm.getMessageType());
                    model.setNotificationId(pm.getNotificationId());
                    model.setNotificationContent(pm.getNotificationContent());
                    model.setIsRead(false);
                    McUtils.saveLogToSDCard("###show job notification: " + model.getJobNum());

//                    textShowPush = textShowPush + " " + model.getStatus().getEConnectName() + " ";
                    GCMNotification.showECJobNotification(context, manager, model, textShowPush, true);
                }
            } else {
                McUtils.saveLogToSDCard("###retrieve none jobs");
            }
        }

        private void toastMsg(final String s) {
            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable() {
                public void run() {
                    Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
                }
            });
        }

    }

}


