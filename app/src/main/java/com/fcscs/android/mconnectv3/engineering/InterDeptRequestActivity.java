package com.fcscs.android.mconnectv3.engineering;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.McOnEditorActionListener;
import com.fcscs.android.mconnectv3.common.QRCodeParserFactory;
import com.fcscs.android.mconnectv3.common.QRCodeParserFactory.InterDepartmentQRCodeParser;
import com.fcscs.android.mconnectv3.common.SessionContext;
import com.fcscs.android.mconnectv3.common.ui.HomeTopBar;
import com.fcscs.android.mconnectv3.common.util.DateTimeHelper;
import com.fcscs.android.mconnectv3.common.util.DialogHelper;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.common.util.PrefsUtil;
import com.fcscs.android.mconnectv3.main.NewRequestBaseActivity;
import com.fcscs.android.mconnectv3.zxing.CaptureActivity;

import java.util.Date;

public class InterDeptRequestActivity extends NewRequestBaseActivity {

    private HomeTopBar headBar;
    private int intPageSize = 10;
    private int intPageIndex = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        mContext = this;
        headBar = (HomeTopBar) findViewById(R.id.top_bar);
        headBar.getTitleTv().setText(R.string.inter_dept_request);
        llEquip.setVisibility(View.VISIBLE);

        where.setOnEditorActionListener(new McOnEditorActionListener() {

            @Override
            public boolean onEnter(TextView view, int actionId, KeyEvent event) {
                String searchTerm = where.getText().toString().trim();
                //..Modify By 	: Chua Kam Hoong
                //..Date 		: 20 May 2015
                //findLocations(searchTerm);
                intPageIndex = 1;
                //findLocations(intPageSize, intPageIndex, searchTerm);
                findLocations(intPageIndex, searchTerm);
                return true;
            }
        });

        qrButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(InterDeptRequestActivity.this, CaptureActivity.class);
                intent.putExtra("type", QRCodeParserFactory.INTERDEPT_REQUEST);
                startActivityForResult(intent, SCANNIN_LOCATION_CODE);

            }
        });
        qrButton.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                findLocations(0, where.getText().toString().trim());
                return true;
            }
        });


        boolean enable = PrefsUtil.getQRCodeNewRequest(this);
        if (enable == false) {
            qrButton.setVisibility(View.GONE);
        }

        findViewById(R.id.ll_who).setVisibility(View.GONE);

        if (PrefsUtil.getEnableConnectPlus(this)) {
            llEquip.setVisibility(View.GONE);
        }

        if (SessionContext.getInstance().getUserType() != null && SessionContext.getInstance().getUserType().equals("S")) {
            llAcknow.setVisibility(View.VISIBLE);
        } else {
            llAcknow.setVisibility(View.GONE);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK) {
            return;
        }

        if (requestCode == SCANNIN_LOCATION_CODE && data != null) {
            Bundle bundle = data.getExtras();
            String result = bundle.getString("result");
            InterDepartmentQRCodeParser parser = new InterDepartmentQRCodeParser();
            if (parser.isCorrectFormat(result)) {
                new NonGuestQRCodeTask(this, parser.room, "", "0", "").exec();
            } else {
                toast(R.string.qr_code_format_is_incorrect);
            }
        }
    }


    protected void onAddButtonClick() {
        if (McUtils.isNullOrEmpty(request.getLocationCode())) {
            toast(getString(R.string.please_select_location));
            where.requestFocus();
            return;
        } else {
            super.onAddButtonClick();
        }
    }

    public void onSumitButtonClick() {
        if (McUtils.isNullOrEmpty(request.getLocationCode())) {
            toast(getString(R.string.please_select_location));
            where.requestFocus();
            return;
        } else if (request.getItems().size() == 0) {
            toast(getString(R.string.please_select_serviceitem));
            return;
        } else {
            Date da = null;
            if (McUtils.isNullOrEmpty(date.getEditableText().toString()) == false) {
                if (DateTimeHelper.getDateByTimeZone(new Date()).after(dateAndTime.getTime())) {
                    toast(getString(R.string.please_select_future_time));
                    return;
                }
                da = dateAndTime.getTime();
            }
            request.setScheduledDate(da);
            submitCheckRequest(request.getGuestName(), request.getLocationCode(), false, INTER_DEPARTMENT_REQUEST_TYPE);
//			sumitInterDepartmentRequest();
        }
    }


    @Override
    public void find(String string) {
        //..Modify By 	: Chua Kam Hoong
        //..Date 		: 20 May 2015
        //findLocations(string);
        intPageIndex = 1;
        //findLocations(intPageSize, intPageIndex, string);
        findLocations(intPageIndex, string);
    }

	
	/*
    @Override
	public void find(int PageSize, int PageIndex, String string) {
		//..Modify By 	: Chua Kam Hoong
		//..Date 		: 20 May 2015
		//findLocations(string);
		findLocations(intPageSize, intPageIndex, string);
	}
	*/
}
