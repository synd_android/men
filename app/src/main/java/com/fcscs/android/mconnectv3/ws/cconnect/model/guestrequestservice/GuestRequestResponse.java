package com.fcscs.android.mconnectv3.ws.cconnect.model.guestrequestservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;

public class GuestRequestResponse implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7227932918742915939L;

    //cconnect
    private String result;
    private String jobNo;
    private List<String> jobNos;
    private String error;

    //econnect
    private String returnCode;
    private String errorMessage;

    private String returnCode1;
    private String errorMessage1;
    private String returnCode2;
    private String errorMessage2;
    private String returnCode3;
    private String errorMessage3;

    public String getReturnCode3() {
        return returnCode3;
    }

    public void setReturnCode3(String returnCode3) {
        this.returnCode3 = returnCode3;
    }

    public String getErrorMessage3() {
        return errorMessage3;
    }

    public void setErrorMessage3(String errorMessage3) {
        this.errorMessage3 = errorMessage3;
    }

    public GuestRequestResponse() {
    }

    public GuestRequestResponse(SoapObject soap) {
        if (soap.hasAttribute("result")) {
            this.result = soap.getAttribute("result").toString();
        }
        if (soap.hasProperty("jobNo") && (soap.getProperty("jobNo") instanceof SoapPrimitive)) {
            this.jobNo = ((SoapPrimitive) soap.getProperty("jobNo")).toString();
        }
        if (soap.hasProperty("error") && (soap.getProperty("error") instanceof SoapPrimitive)) {
            this.error = ((SoapPrimitive) soap.getProperty("error")).toString();
        }
        jobNos = new ArrayList<String>();
        if (soap.getPropertyCount() > 0) {
            for (int i = 0; i < soap.getPropertyCount(); i++) {
                if (soap.getProperty(i) instanceof SoapPrimitive) {
                    jobNos.add(((SoapPrimitive) soap.getProperty(i)).toString());
                }
            }
        }
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getJobNo() {
        return jobNo;
    }

    public void setJobNo(String jobNo) {
        this.jobNo = jobNo;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getReturnCode1() {
        return returnCode1;
    }

    public void setReturnCode1(String returnCode1) {
        this.returnCode1 = returnCode1;
    }

    public String getErrorMessage1() {
        return errorMessage1;
    }

    public void setErrorMessage1(String errorMessage1) {
        this.errorMessage1 = errorMessage1;
    }

    public String getReturnCode2() {
        return returnCode2;
    }

    public void setReturnCode2(String returnCode2) {
        this.returnCode2 = returnCode2;
    }

    public String getErrorMessage2() {
        return errorMessage2;
    }

    public void setErrorMessage2(String errorMessage2) {
        this.errorMessage2 = errorMessage2;
    }

    public List<String> getJobNos() {
        return jobNos;
    }

    public void setJobNos(List<String> jobNos) {
        this.jobNos = jobNos;
    }

}
