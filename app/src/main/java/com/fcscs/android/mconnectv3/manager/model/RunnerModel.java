package com.fcscs.android.mconnectv3.manager.model;

import java.io.Serializable;

public class RunnerModel implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -7676662958458532586L;
    private String name;
    private long id;
    private Long groupId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof RunnerModel) {
            if (((RunnerModel) o).id == id) {
                return true;
            }
        }
        return false;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }


}
