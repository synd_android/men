package com.fcscs.android.mconnectv3.ws.econnect.model.commonservice;

import com.google.gson.annotations.SerializedName;

/**
 * Created by FCS on 7/7/17.
 */

public class PMCondition {
    @SerializedName("ConditionID")
    private int conditionID;
    @SerializedName("ConditionName")
    private String conditionName;
    @SerializedName("ConditionLang")
    private String conditionLang;

    @SerializedName("EmailAlert")
    private String emailAlert;
    @SerializedName("EmailTemplate")
    private String emailTemplate;

    public int getConditionID() {
        return conditionID;
    }

    public void setConditionID(int conditionID) {
        this.conditionID = conditionID;
    }

    public String getConditionName() {
        return conditionName;
    }

    public void setConditionName(String conditionName) {
        this.conditionName = conditionName;
    }

    public String getConditionLang() {
        return conditionLang;
    }

    public void setConditionLang(String conditionLang) {
        this.conditionLang = conditionLang;
    }

    public String getEmailAlert() {
        return emailAlert;
    }

    public void setEmailAlert(String emailAlert) {
        this.emailAlert = emailAlert;
    }

    public String getEmailTemplate() {
        return emailTemplate;
    }

    public void setEmailTemplate(String emailTemplate) {
        this.emailTemplate = emailTemplate;
    }
}
