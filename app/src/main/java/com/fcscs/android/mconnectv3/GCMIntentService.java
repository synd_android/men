package com.fcscs.android.mconnectv3;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.fcscs.android.mconnectv3.common.McConstants;
import com.fcscs.android.mconnectv3.common.ServiceExceptionHandler;
import com.google.android.gcm.GCMBaseIntentService;
import com.google.android.gcm.GCMRegistrar;

public class GCMIntentService extends GCMBaseIntentService {
    private static final String TAG = GCMIntentService.class.getSimpleName();
    private static final String GCM_SENDER_ID = "702929492786";
    private static Context ctx = null;
    private static Handler mHandler;
    private static String currentRegId = null;

    public GCMIntentService() {
        super(GCM_SENDER_ID);
    }

    public static void registerGCM(Context ctx, Handler handler) {

        if (McConstants.OFFLINE_MODE) {
            return;
        }

        try {
            mHandler = handler;
            GCMIntentService.ctx = ctx;
            ServiceExceptionHandler.setHandler(ctx);
            GCMRegistrar.checkDevice(ctx);
            GCMRegistrar.checkManifest(ctx);

            currentRegId = GCMRegistrar.getRegistrationId(ctx);
            if (!TextUtils.isEmpty(currentRegId)) {
                unregisterGCM();
            } else {
                currentRegId = null;
                GCMRegistrar.register(ctx, GCM_SENDER_ID);
            }
        } catch (Exception e) {
            currentRegId = null;
            Log.w(TAG, "Fail to start gcm service");
        }
    }

    public static void unregisterGCM() {
        if (McConstants.OFFLINE_MODE) {
            return;
        }

        if (GCMIntentService.ctx != null) {
            try {
                GCMRegistrar.unregister(GCMIntentService.ctx);
                GCMRegistrar.onDestroy(GCMIntentService.ctx);
            } catch (Exception e) {
                Log.w(TAG, "Fail to destroy gcm receiver");
            }
        }
    }

    @Override
    protected void onRegistered(Context context, String registrationId) {
        Log.d(TAG, "registration id = " + registrationId);
        try {
            boolean result = GCMNotification.registerId(context, registrationId);
            currentRegId = result ? registrationId : null;
        } catch (Exception e) {
            e.printStackTrace();
            currentRegId = null;
        }
    }

    @Override
    protected void onUnregistered(Context arg0, String arg1) {
        Log.d(TAG, "unregistered = " + arg1);
        if (!TextUtils.isEmpty(currentRegId)) {
            GCMRegistrar.register(ctx, GCM_SENDER_ID);
        }
        currentRegId = null;
    }

    @Override
    protected void onError(Context context, String errorId) {
        Log.e(TAG, "errorId = " + errorId);
        currentRegId = null;
        new Thread(new Runnable() {

            @Override
            public void run() {
                Looper.prepare();
                Toast.makeText(GCMIntentService.this, R.string.gcm_unavailable, Toast.LENGTH_LONG).show();
                Looper.loop();
            }
        });
    }

    @Override
    protected boolean onRecoverableError(Context context, String errorId) {
        Log.e(TAG, "errorId = " + errorId);
        currentRegId = null;
        return super.onRecoverableError(context, errorId);
    }

    @Override
    protected void onMessage(Context arg0, Intent arg1) {
        Log.i(TAG, "Received message = " + arg1.getExtras().toString());

        //Retrieve message and extra
        Bundle bundle = arg1.getExtras();
        if (bundle != null) {
            if (mHandler != null) {
                Message message = mHandler.obtainMessage(); // get a message
                message.setData(bundle); // attach bundle
                mHandler.sendMessage(message);
            }
        }
    }

    public static void reset() {
        currentRegId = null;
    }

    public static String getCurrentRegId() {
        return currentRegId;
    }
}