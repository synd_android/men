package com.fcscs.android.mconnectv3.main;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.fcscs.android.mconnectv3.R;

import com.fcscs.android.mconnectv3.common.BaseActivity;
import com.fcscs.android.mconnectv3.common.McConstants;
import com.fcscs.android.mconnectv3.common.SessionContext;
import com.fcscs.android.mconnectv3.common.ui.FcsBaseAdapter;
import com.fcscs.android.mconnectv3.common.ui.HomeTopBar;
import com.fcscs.android.mconnectv3.common.util.PrefsUtil;
import com.fcscs.android.mconnectv3.engineering.InterDeptRequestActivity;
import com.fcscs.android.mconnectv3.manager.model.ServiceRequest;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.ModuleLicense;

public class NewRequestActivity extends BaseActivity {

    private List<String> reqList = null;
    private ListView requestLv;
    private HomeTopBar topBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.request_view);

        topBar = (HomeTopBar) findViewById(R.id.top_bar);
        topBar.getTitleTv().setText(R.string.new_request);

        prepareData();

        requestLv = (ListView) findViewById(R.id.listview);
        requestLv.setSelector(R.drawable.spacebar);
        requestLv.setClickable(true);
        requestLv.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                String selReq = reqList.get(pos);
                Log.i("JOBREQUEST", "selected item: " + selReq);
                Intent i = null;
                if (getString(R.string.guest_request).equals(selReq)) {
                    // i = new Intent(JobRequestActivity.this,
                    // GuestRequestActivity.class);
                    i = new Intent(NewRequestActivity.this, GuestRequestActivity2.class);
                } else if (getString(R.string.non_guest_request).equals(selReq)) {
                    // i = new Intent(JobRequestActivity.this,
                    // NonGuestRequestActivity.class);
                    i = new Intent(NewRequestActivity.this, NonGuestRequestActivity2.class);
                } else if (getString(R.string.inter_deparment_request).equals(selReq)) {
                    // i = new Intent(JobRequestActivity.this,
                    // InterDepartmentRequestActivity.class);
                    i = new Intent(NewRequestActivity.this, InterDepartmentRequestActivity2.class);
//                    if (McConstants.IS_MCONNECT) {
//                        i = new Intent(NewRequestActivity.this, InterDepartmentRequestActivity2.class);
//                    } else {
//                        i = new Intent(NewRequestActivity.this, InterDeptRequestActivity.class);
//                    }

                } else if (getString(R.string.inter_dept_request).equals(selReq)) {
                    i = new Intent(NewRequestActivity.this, InterDeptRequestActivity.class);
                } else if (getString(R.string.engineering_request).equals(selReq)) {
                    i = new Intent(NewRequestActivity.this, EngineeringRequestActivity.class);
                }
                if (i != null) {
                    startActivity(i);
                }
            }
        });
        FcsBaseAdapter adapter = new FcsBaseAdapter(this, reqList);
        requestLv.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ServiceRequest.destroyInstance();
    }

    public void prepareData() {
        reqList = new ArrayList<String>();

        SessionContext session = SessionContext.getInstance();
        ModuleLicense mod = session.getModule();

        if (mod.isGuestRequest()) {
            if (PrefsUtil.getEnableGuestRequestReq(getCurrentContext())) {
                reqList.add(getString(R.string.guest_request));
            }
        }

//		if (mod.isNonGuestRequest()) {
        if ((!McConstants.OFFLINE_MODE && PrefsUtil.getEnableNonGuestRequestReq(getCurrentContext()))) {
            reqList.add(getString(R.string.non_guest_request));
        }

        if (mod.isInterDeptRequest()) {
            if (PrefsUtil.getEnableEng(getCurrentContext()) == 0) {
                reqList.add(getString(R.string.inter_deparment_request));
            } else {

                reqList.add(getString(R.string.inter_dept_request));
            }
        }

        if (!McConstants.OFFLINE_MODE && PrefsUtil.getEnableEngReq(getCurrentContext()) > 0) {
            if (mod.isEngineeringRequest()) {
                reqList.add(getString(R.string.engineering_request));
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            setResult(RESULT_CANCELED);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }


    @Override
    public boolean isLoginActivity() {
        return true;
    }

    @Override
    public boolean isHomeActivity() {
        return false;
    }

}
