package com.fcscs.android.mconnectv3.ws.cconnect.model.guestrequestservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.ksoap2.serialization.SoapObject;

public class FindRoomGuestResponse implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -168683513667074636L;

    private List<Guest> guests = null;

    public FindRoomGuestResponse() {
        guests = new ArrayList<Guest>();
    }

    public FindRoomGuestResponse(SoapObject soap) {
        guests = new ArrayList<Guest>();
        if (soap.getPropertyCount() > 0) {
            for (int i = 0; i < soap.getPropertyCount(); i++) {
                guests.add(new Guest((SoapObject) soap.getProperty(i)));
            }
        }
    }

    public List<Guest> getGuests() {
        return guests;
    }

    public void setGuests(List<Guest> guests) {
        this.guests = guests;
    }

}
