package com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.ksoap2.serialization.SoapObject;

import com.fcscs.android.mconnectv3.manager.model.MessageModel;

public class FindMsgResponse implements Serializable {
    private static final long serialVersionUID = -7827845711759910110L;

    private static final String TOTAL = "total";

    private int total;
    private List<MessageModel> messageList;

    public FindMsgResponse() {
        this.total = 0;
        this.messageList = new ArrayList<MessageModel>();
    }

    public FindMsgResponse(SoapObject soap) {
        this.total = Integer.valueOf(soap.getAttribute(TOTAL).toString());
        this.messageList = new ArrayList<MessageModel>();
        if (soap.getPropertyCount() > 0) {
            for (int i = 0; i < soap.getPropertyCount(); i++) {
                messageList.add(new MessageModel((SoapObject) soap.getProperty(i)));
            }
        }
    }

    public int getTotal() {
        return total;
    }

    public List<MessageModel> getMessageList() {
        return messageList;
    }

    public void setMessageList(List<MessageModel> messageList) {
        this.messageList = messageList;
    }
}
