package com.fcscs.android.mconnectv3.ws.econnect.model.commonservice;

import java.util.Hashtable;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

public class NotificationStatusRequestJobDetails implements KvmSerializable {
    private String jobNo;
    private int jobType;

    public NotificationStatusRequestJobDetails() {
    }

    public NotificationStatusRequestJobDetails(String jobNo, int jobType) {
        this.jobNo = jobNo;
        this.jobType = jobType;
    }

    public void setJobNo(String jobNo) {
        this.jobNo = jobNo;
    }

    public void setJobType(int jobType) {
        this.jobType = jobType;
    }

    public String getJobNo() {
        return jobNo;
    }

    public int getJobType() {
        return jobType;
    }

    public Object getProperty(int arg0) {
        switch (arg0) {
            case 0:
                return jobNo;
            case 1:
                return jobType;
        }
        return null;
    }

    public int getPropertyCount() {
        return 2;
    }

    public void getPropertyInfo(int index, Hashtable arg1, PropertyInfo propertyInfo) {
        switch (index) {
            case 0:
                propertyInfo.name = "JobNo";
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                break;
            case 1:
                propertyInfo.name = "JobType";
                propertyInfo.type = PropertyInfo.INTEGER_CLASS;
                break;
            default:
                break;
        }
    }

    public void setProperty(int index, Object value) {
        switch (index) {
            case 0:
                this.jobNo = value.toString();
                break;
            case 1:
                this.jobType = Integer.parseInt(value.toString());
                break;
            default:
                break;
        }
    }
}
