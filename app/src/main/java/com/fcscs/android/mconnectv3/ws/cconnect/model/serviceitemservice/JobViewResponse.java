package com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.ksoap2.serialization.SoapObject;

import com.fcscs.android.mconnectv3.common.util.SoapConverter;
import com.fcscs.android.mconnectv3.manager.model.JobModel;

public class JobViewResponse implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7346682385149288200L;

    private int total;
    private List<JobModel> jobViews = null;
    private int code;

    public JobViewResponse() {
        jobViews = new ArrayList<JobModel>();
        total = 0;
    }

    public JobViewResponse(SoapObject soap) {
        jobViews = new ArrayList<JobModel>();
        this.total = Integer.valueOf(soap.getAttribute("total").toString());
        if (soap.getPropertyCount() > 0) {
            for (int i = 0; i < soap.getPropertyCount(); i++) {
                jobViews.add(SoapConverter.getCCJob(soap));
            }
        }
    }

    public int getTotal() {
        return total;
    }

    public List<JobModel> getJobViews() {
        return jobViews;
    }

    public void setJobViews(List<JobModel> jobViews) {
        this.jobViews = jobViews;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

}
