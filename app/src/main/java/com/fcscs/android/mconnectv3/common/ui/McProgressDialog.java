package com.fcscs.android.mconnectv3.common.ui;

import com.fcscs.android.mconnectv3.R;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.Window;

public class McProgressDialog extends ProgressDialog {

    public McProgressDialog(Context context) {
        super(context, R.style.NewDialog);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
    }

}
