package com.fcscs.android.mconnectv3.cache;

import android.content.Context;
import android.graphics.drawable.Drawable;

public class DrawableCache extends CacheBase<Integer, Drawable> {

    public DrawableCache(int maxSize) {
        super(maxSize);
    }

    public void put(Context context, Integer resId) {
        Drawable draw = context.getResources().getDrawable(resId);
        put(resId, draw);
    }

    public Drawable get(Context context, Integer resId) {
        if (!containsKey(resId)) {
            put(context, resId);
        }
        return get(resId);
    }

}
