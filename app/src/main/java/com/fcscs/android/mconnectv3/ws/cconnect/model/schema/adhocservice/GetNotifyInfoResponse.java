package com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.ksoap2.serialization.SoapObject;

public class GetNotifyInfoResponse implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 5280964970481631678L;
    private List<Notify> notifyList = new ArrayList<Notify>();

    public GetNotifyInfoResponse() {

    }

    public GetNotifyInfoResponse(SoapObject soap) {
        if (soap.getPropertyCount() > 0) {
            for (int i = 0; i < soap.getPropertyCount(); i++) {
                notifyList.add(new Notify((SoapObject) soap.getProperty(i)));
            }
        }
    }

    public List<Notify> getNotifyList() {
        return notifyList;
    }

    public void setNotifyList(List<Notify> notifyList) {
        this.notifyList = notifyList;
    }

    public class Notify implements Serializable {

        /**
         *
         */
        private static final long serialVersionUID = 2837015295568247158L;
        public String type;
        public Integer count;

        public Notify(SoapObject soap) {
            this.count = Integer.valueOf(soap.getAttribute("count").toString());
            this.type = soap.getAttributeAsString("type").toString();
        }

        public Notify() {

        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Integer getCount() {
            return count;
        }

        public void setCount(Integer count) {
            this.count = count;
        }

    }

}
