
package com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice;

import java.io.Serializable;

import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;

import com.j256.ormlite.field.DatabaseField;

public class ServiceItemViewModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7450612944838156532L;


    private long orgId;
    private Long propertyId;
    private Short duration;
    private Long calcDuration;
    private Long skillId;
    private long categoryId;
    private long departmentId;


    private String nameSecLang;

    private String durationUnit;
    private String category;
    private String skill;
    private String departmentName;
    private String displayName;

    @DatabaseField(id = true)
    private long serviceItemId;
    @DatabaseField
    private String name;
    @DatabaseField
    private String code;
    @DatabaseField
    private String serviceCodeCode;
    @DatabaseField
    private String serviceCodeName;
    @DatabaseField
    private int enabledQuantity;

    public ServiceItemViewModel() {
    }

    public ServiceItemViewModel(SoapObject soap) {
        this.serviceItemId = Long.valueOf(soap.getAttribute("serviceItemId").toString());
        this.orgId = Long.valueOf(soap.getAttribute("orgId").toString());
        if (soap.hasAttribute("propertyId")) {
            this.propertyId = Long.valueOf(soap.getAttribute("propertyId").toString());
        }
        if (soap.hasAttribute("duration")) {
            this.duration = Short.valueOf(soap.getAttribute("duration").toString());
        }
        if (soap.hasAttribute("calcDuration")) {
            this.calcDuration = Long.valueOf(soap.getAttribute("calcDuration").toString());
        }
        if (soap.hasAttribute("skillId")) {
            this.skillId = Long.valueOf(soap.getAttribute("skillId").toString());
        }
        this.categoryId = Long.valueOf(soap.getAttribute("categoryId").toString());
        this.departmentId = Long.valueOf(soap.getAttribute("departmentId").toString());

        if (soap.hasProperty("name") && (soap.getProperty("name") instanceof SoapPrimitive)) {
            this.name = ((SoapPrimitive) soap.getProperty("name")).toString();
        }
        if (soap.hasProperty("nameSecLang") && (soap.getProperty("nameSecLang") instanceof SoapPrimitive)) {
            this.nameSecLang = ((SoapPrimitive) soap.getProperty("nameSecLang")).toString();
        }
        if (soap.hasProperty("code") && (soap.getProperty("code") instanceof SoapPrimitive)) {
            this.code = ((SoapPrimitive) soap.getProperty("code")).toString();
        }
        if (soap.hasProperty("durationUnit") && (soap.getProperty("durationUnit") instanceof SoapPrimitive)) {
            this.durationUnit = ((SoapPrimitive) soap.getProperty("durationUnit")).toString();
        }
        if (soap.hasProperty("category") && (soap.getProperty("category") instanceof SoapPrimitive)) {
            this.category = ((SoapPrimitive) soap.getProperty("category")).toString();
        }
        if (soap.hasProperty("skill") && (soap.getProperty("skill") instanceof SoapPrimitive)) {
            this.skill = ((SoapPrimitive) soap.getProperty("skill")).toString();
        }
        if (soap.hasProperty("departmentName") && (soap.getProperty("departmentName") instanceof SoapPrimitive)) {
            this.departmentName = ((SoapPrimitive) soap.getProperty("departmentName")).toString();
        }
        this.displayName = name + "(" + departmentName + ", " + category + ")";
    }

    @Override
    public String toString() {
        return this.name;
    }

    public long getServiceItemId() {
        return serviceItemId;
    }

    public long getOrgId() {
        return orgId;
    }

    public Long getPropertyId() {
        return propertyId;
    }

    public Short getDuration() {
        return duration;
    }

    public Long getCalcDuration() {
        return calcDuration;
    }

    public Long getSkillId() {
        return skillId;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public long getDepartmentId() {
        return departmentId;
    }

    public String getName() {
        return name;
    }

    public String getNameSecLang() {
        return nameSecLang;
    }

    public String getCode() {
        return code;
    }

    public String getDurationUnit() {
        return durationUnit;
    }

    public String getCategory() {
        return category;
    }

    public String getSkill() {
        return skill;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setServiceItemId(long serviceItemId) {
        this.serviceItemId = serviceItemId;
    }

    public void setOrgId(long orgId) {
        this.orgId = orgId;
    }

    public void setPropertyId(Long propertyId) {
        this.propertyId = propertyId;
    }

    public void setDuration(Short duration) {
        this.duration = duration;
    }

    public void setCalcDuration(Long calcDuration) {
        this.calcDuration = calcDuration;
    }

    public void setSkillId(Long skillId) {
        this.skillId = skillId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public void setDepartmentId(long departmentId) {
        this.departmentId = departmentId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNameSecLang(String nameSecLang) {
        this.nameSecLang = nameSecLang;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setDurationUnit(String durationUnit) {
        this.durationUnit = durationUnit;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getServiceCodeCode() {
        return serviceCodeCode;
    }

    public void setServiceCodeCode(String serviceCodeCode) {
        this.serviceCodeCode = serviceCodeCode;
    }

    public String getServiceCodeName() {
        return serviceCodeName;
    }

    public void setServiceCodeName(String serviceCodeName) {
        this.serviceCodeName = serviceCodeName;
    }

    public int getEnabledQuantity() {
        return enabledQuantity;
    }

    public void setEnabledQuantity(int enabledQuantity) {
        this.enabledQuantity = enabledQuantity;
    }
}
