package com.fcscs.android.mconnectv3.ws.cconnect.model.securityservice;

import java.io.Serializable;

import org.ksoap2.serialization.SoapObject;

public class LoginResponse implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7388310827650667613L;

    private Long userId;
    private String name;
    private String sessionId;
    private Long orgId;
    private Long propId;
    private String userType;
    private int reAssignRunner;
    private String userDepartment;

    public String getUserDepartment() {
        return userDepartment;
    }

    public void setUserDepartment(String userDepartment) {
        this.userDepartment = userDepartment;
    }

    public int getReAssignRunner() {
        return reAssignRunner;
    }

    public void setReAssignRunner(int reAssignRunner) {
        this.reAssignRunner = reAssignRunner;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserTimeZone() {
        return userTimeZone;
    }

    public void setUserTimeZone(String userTimeZone) {
        this.userTimeZone = userTimeZone;
    }

    private String userTimeZone;

    // econnect: 0 success;1 fail
    private String returnCode;
    private String errorMsg;

    public LoginResponse() {
    }

    public LoginResponse(SoapObject soap) {
        this.userId = Long.valueOf(soap.getAttribute("fcsUserId").toString());
        this.sessionId = soap.getAttribute("wsSessionId").toString();
        this.orgId = Long.valueOf(soap.getAttribute("orgId").toString());
        if (soap.hasAttribute("propId")) {
            this.propId = Long.valueOf(soap.getAttribute("propId").toString());
        }
        name = "";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public void setPropId(Long propId) {
        this.propId = propId;
    }

    public Long getUserId() {
        return userId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public Long getOrgId() {
        return orgId;
    }

    public Long getPropId() {
        return propId;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public String getReturnCode() {
        return returnCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
