package com.fcscs.android.mconnectv3.main;

import java.util.Date;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.McConstants;
import com.fcscs.android.mconnectv3.common.McOnEditorActionListener;
import com.fcscs.android.mconnectv3.common.QRCodeParserFactory;
import com.fcscs.android.mconnectv3.common.QRCodeParserFactory.GuestQRCodeParser;
import com.fcscs.android.mconnectv3.common.ui.HomeTopBar;
import com.fcscs.android.mconnectv3.common.util.DateTimeHelper;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.common.util.PrefsUtil;
import com.fcscs.android.mconnectv3.manager.model.GuestInfo;
import com.fcscs.android.mconnectv3.zxing.CaptureActivity;

public class GuestRequestActivity2 extends NewRequestBaseActivity {

    private HomeTopBar headBar;
    private EditText who;
    private GuestInfo currentGuest;
    private ImageView info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        headBar = (HomeTopBar) findViewById(R.id.top_bar);
        headBar.getTitleTv().setText(R.string.guest_request);


        where.setHint(R.string.room_num);
        if (!McConstants.NON_HOTEL_PRODUCT) {
            where.setInputType(InputType.TYPE_CLASS_NUMBER);
        }
        where.setOnEditorActionListener(new McOnEditorActionListener() {

            @Override
            public boolean onEnter(TextView view, int actionId, KeyEvent event) {
                String term = where.getText().toString().trim();
                findGuests(where, term, false);
                return true;
            }
        });

        qrButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GuestRequestActivity2.this, CaptureActivity.class);
                intent.putExtra("type", QRCodeParserFactory.GUEST_REQUEST);
                startActivityForResult(intent, SCANNIN_GREQUEST_CODE);
            }
        });
        qrButton.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                findGuests(where, null, true);
                return true;
            }
        });

        boolean enable = PrefsUtil.getQRCodeNewRequest(this);
        if (enable == false) {
            qrButton.setVisibility(View.GONE);
        }

        who = (EditText) findViewById(R.id.commonWhoEtId);
        who.setHint(getString(R.string.guestname_vip_msg));
        who.setEnabled(false);

        info = (ImageView) findViewById(R.id.iv_info);
        info.setVisibility(View.VISIBLE);
        info.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (currentGuest == null) {
                    toast(getString(R.string.please_select_guest));
                } else {
                    Intent i = new Intent(GuestRequestActivity2.this, GuestDetails.class);
                    i.putExtra("guest", currentGuest);
                    startActivity(i);
                }
            }

        });

    }

    @Override
    protected void onGuestSelected(int pos, GuestInfo guest) {
        if (guest != null) {
            currentGuest = guest;
            request.setGuestName(guest.getGuestUsername());
            request.setRoomNo(guest.getRoomNum());
            request.setLocationCode("" + guest.getLocationId());
            where.setText(guest.getRoomNum());
            who.setText(guest.getGuestUsername() + "(" + guest.getVipMsg() + ")");
            who.requestFocus();
        } else {
            currentGuest = null;
            request.setGuestName(null);
            request.setRoomNo(null);
            where.setText("");
            who.setText("");
            request.setLocationCode("");
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK) {
            return;
        }

        if (requestCode == SCANNIN_GREQUEST_CODE && data != null) {
            GuestQRCodeParser parser = new GuestQRCodeParser();
            String qrcode = data.getStringExtra("result");
            if (parser.isCorrectFormat(qrcode)) {
                new GuestQRCodeTask(this, parser.room, "", "0", "").exec();
            } else {
                toast(R.string.qr_code_format_is_incorrect);
            }
        }
    }

    protected void onAddButtonClick() {
        if (McUtils.isNullOrEmpty(request.getLocationCode())) {
            toast(getString(R.string.please_input_roomnumber));
            where.requestFocus();
        } else {
            super.onAddButtonClick();
        }
    }

    public void onSumitButtonClick() {
        if (currentGuest == null) {
            toast(getString(R.string.please_select_guest));
            return;
        } else if (request.getItems().size() == 0) {
            toast(getString(R.string.please_select_serviceitem));
            return;
        } else {
            Date da = null;
            if (McUtils.isNullOrEmpty(date.getEditableText().toString()) == false) {
                if (DateTimeHelper.getDateByTimeZone(new Date()).after(dateAndTime.getTime())) {
                    toast(getString(R.string.please_select_future_time));
                    return;
                }
                da = dateAndTime.getTime();
            }
            String guestName = currentGuest.getGuestUsername();
            request.setScheduledDate(da);
            request.setGuestName(guestName);
            submitCheckRequest(request.getGuestName(), request.getRoomNo(), true, GUEST_REQUEST_TYPE);
//			sumitGuestRequest();
        }
    }

    @Override
    public void find(String string) {
        findGuests(where, string, false);
    }

}
