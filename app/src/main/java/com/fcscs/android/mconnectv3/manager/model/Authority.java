//package com.fcscs.android.mconnect.manager.model;
//
//import java.io.Serializable;
//
//import org.ksoap2.serialization.SoapObject;
//
//import com.fcscs.android.mconnectv3.common.util.McUtils;
//import com.fcscs.core.common.ProductEnum;
//import com.fcscs.core.common.authority.UserAuthority;
//
//public class Authority implements Serializable {
//
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = 6377378570167494089L;
//	private String type;
//	private String name;
//	private UserAuthority userAuthority;
//	private String product;
//	private ProductEnum productEnum;
//
//	public Authority() {
//	}
//
//	public Authority(SoapObject soap) {
//		this.type = soap.getAttribute("type").toString();
//		this.name = soap.getAttribute("name").toString();
//		this.product = soap.getAttribute("product").toString();
//
//		this.productEnum = ProductEnum.valueOf(this.product);
//		this.userAuthority = McUtils.getUserAuthority(name, type);
//	}
//
//	public String getType() {
//		return type;
//	}
//
//	public void setType(String type) {
//		this.type = type;
//	}
//
//	public String getFullname() {
//		return name;
//	}
//
//	public void setFullname(String name) {
//		this.name = name;
//	}
//
//	public String getProduct() {
//		return product;
//	}
//
//	public void setProduct(String product) {
//		this.product = product;
//	}
//
//	public UserAuthority getUserAuthority() {
//		return userAuthority;
//	}
//
//	public void setUserAuthority(UserAuthority userAuthority) {
//		this.userAuthority = userAuthority;
//	}
//
//	public ProductEnum getProductEnum() {
//		return productEnum;
//	}
//
//	public void setProductEnum(ProductEnum productEnum) {
//		this.productEnum = productEnum;
//	}
//
//}