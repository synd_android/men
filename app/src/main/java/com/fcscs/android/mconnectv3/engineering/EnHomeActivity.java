package com.fcscs.android.mconnectv3.engineering;

import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Messenger;
import android.telephony.TelephonyManager;
import android.util.AndroidException;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fcscs.android.mconnectv3.AlertPanicManager;
import com.fcscs.android.mconnectv3.CheckNetworkConnectivityReceiver;
import com.fcscs.android.mconnectv3.CheckNetworkConnectivityReceiver.CheckConnectivity;
import com.fcscs.android.mconnectv3.McApplication;
import com.fcscs.android.mconnectv3.PullNotificationService;
import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.BaseActivity;
import com.fcscs.android.mconnectv3.common.ConfigContext;
import com.fcscs.android.mconnectv3.common.McConstants;
import com.fcscs.android.mconnectv3.common.OnFlingGestureListeners;
import com.fcscs.android.mconnectv3.common.SessionContext;
import com.fcscs.android.mconnectv3.common.ui.HomeModuleListAdapter;
import com.fcscs.android.mconnectv3.common.ui.LogoutTopBar;
import com.fcscs.android.mconnectv3.common.ui.McBackgroundAsyncTask;
import com.fcscs.android.mconnectv3.common.ui.McProgressAsyncTask;
import com.fcscs.android.mconnectv3.common.util.DialogHelper;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.common.util.PrefsUtil;
import com.fcscs.android.mconnectv3.main.FavoritesActivity;
import com.fcscs.android.mconnectv3.main.GuestDetailActivity;
import com.fcscs.android.mconnectv3.main.JobSearchActivity;
import com.fcscs.android.mconnectv3.main.LoadingActivity;
import com.fcscs.android.mconnectv3.main.MinibarActivity;
import com.fcscs.android.mconnectv3.main.MyJobsActivity;
import com.fcscs.android.mconnectv3.main.MyMessageActivity;
import com.fcscs.android.mconnectv3.main.NewRequestActivity;
import com.fcscs.android.mconnectv3.main.PanicButtonActivity;
import com.fcscs.android.mconnectv3.main.RoomStatusActivity;
import com.fcscs.android.mconnectv3.main.SendMessageActivity;
import com.fcscs.android.mconnectv3.manager.model.HomeModuleTag;
import com.fcscs.android.mconnectv3.manager.model.HomeModuleTag.OnViewBindListener;
import com.fcscs.android.mconnectv3.smartpush.SmartPush;
import com.fcscs.android.mconnectv3.ws.cconnect.AdhocService;
import com.fcscs.android.mconnectv3.ws.cconnect.SecurityService;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.GetNotifyInfoResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.GetNotifyInfoResponse.Notify;
import com.fcscs.android.mconnectv3.ws.econnect.ECService;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.ModuleLicense;

import java.util.ArrayList;
import java.util.List;

//import com.android.volley.AuthFailureError;
//import com.android.volley.NetworkError;
//import com.android.volley.NetworkResponse;
//import com.android.volley.NoConnectionError;
//import com.android.volley.ParseError;
//import com.android.volley.RequestQueue;
//import com.android.volley.Response;
//import com.android.volley.ServerError;
//import com.android.volley.TimeoutError;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.Volley;
////import com.csipsimple.api.SipProfile;
////import com.csipsimple.service.SipService;
//import com.fcs.fcsptt.activity.FragmentContainerActivity;
//import com.fcs.fcsptt.activity.PushReceiver;
//import com.fcs.fcsptt.activity.PushToTalkActivity;
////import com.fcs.fcsptt.events.MQTTService;
//import com.fcs.fcsptt.events.FcsSipMqttService;
//import com.fcs.fcsptt.model.BaseModel;
//import com.fcs.fcsptt.model.ChannelModel;
//import com.fcs.fcsptt.model.CurrentUser;
//import com.fcs.fcsptt.util.PTTConstants;
//import com.fcs.fcsptt.util.PreferencesUtil;
//import com.fcs.fcsptt.util.RunningServiceChecker;
//import com.fcs.fcsptt.util.SecurityUtil;

//import static com.fcs.fcsptt.activity.FragmentContainerActivity.CheckLoginRole;

public class EnHomeActivity extends BaseActivity {

    protected static final String TAG = EnHomeActivity.class.getSimpleName();

    private BroadcastReceiver updateBadgeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (SmartPush.ACTION_UPDATE_COUNTER.equalsIgnoreCase(action)) {
                Log.d(TAG, "receive badge update message");

                if (task == null || AsyncTask.Status.RUNNING.equals(task.getStatus()) == false) {
                    task = new MessageCountLoader(context);
                    task.exec();
                }
            }
        }
    };

    private Button topLeftBtn, topRightBtn, logoutBtn;
    private LogoutTopBar logoutTopBar;
    private List<HomeModuleTag> tags;
    private TextView msgCountTextView;
    private GridView moduleGridView;

    private HomeModuleListAdapter adapter;
    protected MessageCountLoader task;

    private ImageView panic;
    HomeCallReciever mCallReceive;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);
        PullNotificationService.register(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(new Intent(this, PullNotificationService.class));
        } else {
            startService(new Intent(this, PullNotificationService.class));
        }


        moduleGridView = (GridView) findViewById(R.id.gridview);
        logoutTopBar = (LogoutTopBar) findViewById(R.id.ltb_logout);
        topLeftBtn = (Button) findViewById(R.id.btn_top_home);
        topRightBtn = (Button) findViewById(R.id.btn_top_menu);
        topLeftBtn.setVisibility(View.INVISIBLE);
        topRightBtn.setBackgroundResource(R.drawable.icon_connected);
        topRightBtn.setClickable(false);

        panic = (ImageView) findViewById(R.id.panic_button);
        panic.setOnTouchListener(new OnFlingGestureListeners() {

            @Override
            public void onRightToLeft() {
                if ("open".equals(panic.getTag())) {
                    panic.setImageResource(R.drawable.panic_button_close);
                    panic.setTag("close");
                    AlertPanicManager.getInstance().stopSoundAlert();
                    PrefsUtil.setClickPanic(EnHomeActivity.this, false);
                }
            }

            @Override
            public void onLeftToRight() {
                if ("open".equals(panic.getTag()) == false) {
                    panic.setImageResource(R.drawable.panic_button_open);
                    panic.setTag("open");
                    AlertPanicManager.getInstance().playSoundAlert();
                }
            }

            @Override
            public void onBottomToTop() {
            }

            @Override
            public void onTopToBottom() {
            }

            @Override
            public void onTouchDown() {
            }
        });

        panic.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if ("open".equals(panic.getTag())) {
                    PrefsUtil.setClickPanic(EnHomeActivity.this, true);
                    if (ConfigContext.getInstance().isEnablePanicCall()) {
                        if (ConfigContext.getInstance().isAutoAlert()) {
                            makeCall();
                        } else {
                            showPanicDialog();
                        }

                    } else {
                        Intent i = new Intent(EnHomeActivity.this, PanicButtonActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        EnHomeActivity.this.startActivity(i);
                    }
                }
                new PanicButtonSubmit(getCurrentContext()).exec();
            }
        });
//        panic.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				if ("open".equals(panic.getTag())) {
////                    if (ConfigContext.getInstance().isEnablePanicCall()) {
////                        showPanicDialog();
////                    } else {
//					Intent i = new Intent(HomeActivity.this, PanicButtonActivity.class);
//					i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//					HomeActivity.this.startActivity(i);
////                    }
//				}
//			}
//		});
        panic.setVisibility(View.GONE);
        new PanicButtonConfig(this).exec();
        new RoomStatusConfig(this).exec();

        logoutBtn = (Button) findViewById(R.id.bottom_logout_btn);
        logoutBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                logoutBtn.setClickable(false);

                DialogHelper.showYesNoDialog(getCurrentContext(), R.string.confirm_logout, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        logout();
                    }
                });

                logoutBtn.setClickable(true);

            }

        });

        logoutTopBar.getTitleTv().setText(SessionContext.getInstance().getUsername());
        prepareAdapter();
        preparePTT();
        ImageButton walkie_talkie = (ImageButton) findViewById(R.id.bottom_walkie_talkie_btn);
//		walkie_talkie.setOnClickListener(didClickPtt);
        walkie_talkie.setVisibility(McConstants.SHOW_PTT ? View.VISIBLE : View.GONE);

//		String roomStatusMsg = PrefsUtil.getRoomStatusMsg(this);

        mCallReceive = new HomeCallReciever();
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.intent.action.PHONE_STATE");
        registerReceiver(mCallReceive, filter);
//		McApplication.application.addObserver(this);
//		McApplication.application.checkUpdatesManually();
    }


    private CheckConnectivity checking = new CheckConnectivity() {
        @Override
        public void hasConenction(boolean hasConnection) {
            if (topRightBtn != null) {
                if (hasConnection) {
                    topRightBtn.setBackgroundResource(R.drawable.icon_connected);
                } else {
                    topRightBtn.setBackgroundResource(R.drawable.icon_disconnected);
                }
                isHasInternetConnection = hasConnection;
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        getApp().setLogin(true);
        CheckNetworkConnectivityReceiver.register(checking);

        if (task == null || AsyncTask.Status.RUNNING.equals(task.getStatus()) == false) {
            task = new MessageCountLoader(getCurrentContext());
            task.exec();
        }
        registerReceiver(updateBadgeReceiver, new IntentFilter(SmartPush.ACTION_UPDATE_COUNTER));
        // check for same sipservice running
//		registerReceiver(pushReceiver, intentFilter, null, null);
//		isSameSipServiceRunning = RunningServiceChecker.isSameSipServiceRunning(this);
//		isSipServiceRunning = RunningServiceChecker.isSipServiceRunning(this);
//		isMQTTServiceRunning = RunningServiceChecker.isMQTTServiceRunning(this);

    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(updateBadgeReceiver);
//		unregisterReceiver(pushReceiver);

    }


    private void prepareAdapter() {
        ;
        ModuleLicense mod = getSessionCtx().getModule();

        tags = new ArrayList<HomeModuleTag>();
        if (mod.isMyJob()) {
            final HomeModuleTag myJob = createModuleTag(R.drawable.m_myjobs, R.string.my_job, MyJobsActivity.class);
            myJob.setBindListener(new OnViewBindListener() {

                @Override
                public void onViewBind(int position, View convertView, ViewGroup parent) {
                    jobCountTextView = myJob.getMessageCount();
                    updateMessageCount();
                }
            });
            tags.add(myJob);
        }

        if (mod.isMyMessage()) {
            final HomeModuleTag myMessageModule = createModuleTag(R.drawable.m_mymsg, R.string.my_message, MyMessageActivity.class);
            myMessageModule.setBindListener(new OnViewBindListener() {

                @Override
                public void onViewBind(int position, View convertView, ViewGroup parent) {
                    msgCountTextView = myMessageModule.getMessageCount();
                    updateMessageCount();
                }
            });
            tags.add(myMessageModule);
        }
        if (mod.isJobSearch()) {
            tags.add(createModuleTag(R.drawable.m_jobsearch, R.string.job_search, JobSearchActivity.class));
        }
        if (mod.isJobRequest()) {
            tags.add(createModuleTag(R.drawable.m_newrequest, R.string.new_request, NewRequestActivity.class));
        }
        if (mod.isSendMessage()) {
            tags.add(createModuleTag(R.drawable.m_new_message, R.string.new_messages, SendMessageActivity.class));
        }
        //if (PrefsUtil.getEnableEng(this) == 0) {
            if (mod.isGuestDetail()) {
                tags.add(createModuleTag(R.drawable.guest_details, R.string.guestdetails, GuestDetailActivity.class));
            }

            if (mod.isMinibar()) {
                tags.add(createModuleTag(R.drawable.m_minibar, R.string.minibar, MinibarActivity.class));
            }

            if (mod.isRoomStatus()) {
                tags.add(createModuleTag(R.drawable.m_roomstatus, R.string.room_status, RoomStatusActivity.class));
            }
        //}

        if (mod.isFavorite()) {
            tags.add(createModuleTag(R.drawable.m_favorites, R.string.favorites, FavoritesActivity.class));
        }
        adapter = new HomeModuleListAdapter(this, tags);
        moduleGridView.setAdapter(adapter);

    }

    private HomeModuleTag createModuleTag(int iconResId, int textResId, final Class<?> activity) {

        HomeModuleTag myJobModule = new HomeModuleTag(iconResId, textResId);
        myJobModule.setClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                startActivity(new Intent(EnHomeActivity.this, activity));
            }
        });

        return myJobModule;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

    private void updateMessageCount() {

        if (msgCountTextView != null) {
            if (getSessionCtx().getMessageCount() > 0) {
                msgCountTextView.setVisibility(View.VISIBLE);
                msgCountTextView.setText("" + SessionContext.getInstance().getMessageCount());
            } else {
                msgCountTextView.setVisibility(View.GONE);
            }
        }

        if (jobCountTextView != null) {
            if (getSessionCtx().getJobCount() > 0) {
                jobCountTextView.setText("" + SessionContext.getInstance().getJobCount());
                jobCountTextView.setVisibility(View.VISIBLE);
            } else {
                jobCountTextView.setVisibility(View.INVISIBLE);
            }
        }
    }

    protected TextView jobCountTextView;

    private LogoutLoader task2;

    class LogoutLoader extends McProgressAsyncTask {

        public LogoutLoader(Context context) {
            super(context);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            SmartPush.getInstance().unRegisterPush(EnHomeActivity.this.getApplicationContext());
//			GCMIntentService.unregisterGCM();
        }

        @Override
        public void doInBackground() {
            SessionContext sessionCtx = SessionContext.getInstance();

            Context context = getCurrentContext();
            if (isCConnect()) {
                SecurityService.getInstance().logout(context, sessionCtx.getFcsUserId(), sessionCtx.getWsSessionId());
            } else {
                ECService.getInstance().updateToken(context, sessionCtx.getFcsUserId(), "");
                try {
                    Thread.sleep(1000 * 1);
                } catch (InterruptedException e) {
                }
                ECService.getInstance().getLogout(context, "" + sessionCtx.getFcsUserId());
                stopService(new Intent(EnHomeActivity.this, PullNotificationService.class));
            }

            sessionCtx.setLogin(false);
            sessionCtx.setWsSessionId("");
            sessionCtx.persist(getCurrentContext());

            McApplication app = (McApplication) context.getApplicationContext();
            app.getSessionCache().clear();
            app.setLogoutProcessed(false);

        }

        @Override
        public void onPostExecute() {

            PrefsUtil.setOfflineMode(getCurrentContext(), false);

            String ns = Context.NOTIFICATION_SERVICE;
            NotificationManager mManager = (NotificationManager) getCurrentContext().getSystemService(ns);
            mManager.cancelAll();

            Intent i = new Intent(getCurrentContext(), LoadingActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);

        }

    }

    @Override
    public boolean isLoginActivity() {
        return true;
    }

    @Override
    public boolean isHomeActivity() {
        return true;
    }

    class MessageCountLoader extends McBackgroundAsyncTask {

        private GetNotifyInfoResponse noteResp;

        public MessageCountLoader(Context context) {
            super(context);
        }

        @Override
        public void doInBackground() {
            if (isCConnect()) {
                noteResp = AdhocService.getInstance().getNotifyInfo(EnHomeActivity.this);
            } else {
                int bIncludeEngineeringJobs = PrefsUtil.getEnableEng(EnHomeActivity.this);
                noteResp = ECService.getInstance().getBadge(EnHomeActivity.this, getSessionCtx().getFcsUserId(), bIncludeEngineeringJobs);
            }
        }

        @Override
        public void onPostExecute() {

            if (noteResp != null) {
                for (Notify note : noteResp.getNotifyList()) {
                    if ("MSG_COUNT".equals(note.getType())) {
                        getSessionCtx().setMessageCount(note.getCount() == null ? 0 : note.getCount());
                        getSessionCtx().persist(getBaseContext());
                    }
                    if ("JOB_COUNT".equals(note.getType())) {
                        getSessionCtx().setJobCount(note.getCount() == null ? 0 : note.getCount());
                        getSessionCtx().persist(getBaseContext());
                    }
                }
            }
            adapter.notifyDataSetChanged();
//			updateMessageCount();
        }

    }

    public void onTouch(MotionEvent e) {
    }

    private void logout() {
//		if (PreferencesUtil.getAlreadyOpenPTT(this)) {
//			FragmentContainerActivity.requestLogout(HomeActivity.this, new Response.Listener<BaseModel>() {
//				@Override
//				public void onResponse(BaseModel response) {
//					logoutFunction();
//				}
//			}, new Response.ErrorListener() {
//				@Override
//				public void onErrorResponse(VolleyError error) {
//					logoutFunction();
//				}
//			});
//		}else {
        logoutFunction();
//		}

    }

    private void logoutFunction() {
//		PreferencesUtil.setIsmConnectLogoutPrefs(HomeActivity.this, true);
//		PreferencesUtil.setIsLogoutPrefs(HomeActivity.this, true);
//		sendLogoutBroadcast();
        if (task2 == null || AsyncTask.Status.RUNNING.equals(task2.getStatus()) == false) {
            task2 = new LogoutLoader(getCurrentContext());
            task2.exec();
        }
    }

    private class PanicButtonConfig extends McBackgroundAsyncTask {

        private ConfigContext conf;

        public PanicButtonConfig(Context context) {
            super(context);
        }

        @Override
        public void doInBackground() {
            conf = ECService.getInstance().getPanicButtonConfig(getCurrentContext());
        }

        @Override
        public void onPostExecute() {
            if (conf.isEnableOnMainPage()) {
                panic.setVisibility(View.VISIBLE);
            }
        }

    }

    private class RoomStatusConfig extends McBackgroundAsyncTask {

        private String result;

        public RoomStatusConfig(Context context) {
            super(context);
        }

        @Override
        public void doInBackground() {
            result = ECService.getInstance().getRoomStatus(getCurrentContext());
        }

        @Override
        public void onPostExecute() {
            PrefsUtil.setRoomStatusMsg(getCurrentContext(), result);
        }

    }

    private void makeCall() {
        String phoneNo = ConfigContext.getInstance().getPanicCallContactNumber();
        if (McUtils.isNullOrEmpty(phoneNo) == false) {
            try {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + phoneNo));
//				startActivity(callIntent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void showPanicDialog() {
        DialogHelper.showYesNoDialog(this, R.string.panic_alert, R.string.start_panic_call, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                makeCall();
            }
        });
    }

    /** mqtt code*/
    /**
     * @author khatran
     */

    public ProgressDialog progressBar;
    String[] groupSubcribe;
    boolean isRegister;
    private Messenger service = null;
    //    private final Messenger serviceHandler = new Messenger(new ServiceHandler());
    private IntentFilter intentFilter = null;
    //	private PushReceiver pushReceiver;
    private FCAReceiver logoutReceiver = new FCAReceiver();
    private LoginMConnectDuplicationReceiver loginmConnectDuplication = new LoginMConnectDuplicationReceiver();
    private DuplicateIDReceiver duplicateIDReceiver = new DuplicateIDReceiver();
    public boolean isHasInternetConnection = true;

//	public void onTouch(MotionEvent e) {
//	}

//	private void sendLogoutBroadcast() {
//		Intent intent = new Intent();
//		intent.setAction(PTTConstants.LOGOUT_MCONNECT);
//		sendBroadcast(intent);
//	}

//	private void stopSipAndMQTT() {
////		if (isSipServiceRunning) {
////			Intent intentSip = new Intent(HomeActivity.this, SipService.class);
////			stopService(intentSip);
////		}
////		SipdroidEngine.mSipdroidEngine.halt();
//		if (isMQTTServiceRunning) {
//			Intent intentMQTT = new Intent(HomeActivity.this, FcsSipMqttService.class);
//			stopService(intentMQTT);
//		}
//	}
//
////	private void stopSip() {
////		if (isSipServiceRunning) {
////			Intent intentSip = new Intent(HomeActivity.this, SipService.class);
////			stopService(intentSip);
////		}
////	}
//
//	RequestQueue mRequestQueue;//
//
//	private RequestQueue mQueue() {
//		if (mRequestQueue == null) {
//			mRequestQueue = Volley.newRequestQueue(HomeActivity.this);
//			return mRequestQueue;
//		} else {
//			return mRequestQueue;
//		}
//	}

    //	private void setRole(String roleValue) {
//		boolean isPlay = false;
//		boolean isEdit = false;
//		boolean isView = false;
//		boolean isNotPermision = false;
//		String[] parts = roleValue.split("\\|");
//		for (String retval : parts) {
//			if (retval.equals(PTTConstants.ROLE_MOBILE_PERMISSION_EDIT)) {
//				isEdit = true;
//			} else if (retval.equals(PTTConstants.ROLE_MOBILE_PERMISSION_PLAY)) {
//				isPlay = true;
//			} else if (retval.equals(PTTConstants.ROLE_MOBILE_PERMISSION_VIEW)) {
//				isView = true;
//			} else if (retval.equals(PTTConstants.ROLE_NO_ROLES)) {
//				isNotPermision = true;
//			}
//		}
//		if (isEdit) {
//			PreferencesUtil.setUserRole(HomeActivity.this, PTTConstants.PARAM_PER_ADMIN);
//		} else if (isView) {
//			PreferencesUtil.setUserRole(HomeActivity.this, PTTConstants.PARAM_PER_USER);
//		} else if (isNotPermision) {
//			PreferencesUtil.setUserRole(HomeActivity.this, PTTConstants.PARAM_NO_PERMISSION);
//		}
//		PreferencesUtil.setHistoryViewPrefs(HomeActivity.this, isPlay);
//	}
    private int deleteAllSipAccount() {
//		int rows = -1;
//		try {
//			rows = getContentResolver().delete(SipProfile.ACCOUNT_URI, null, null);
//			Log.d("Sip Account del", rows + "");
//		}
//		catch (IllegalArgumentException e) {
//
//		}
        return 0;
    }

    public class LoginMConnectDuplicationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent i) {
            String action = i.getAction();
            if ("LOGIN_MCONNECT_DUPLICATION".equalsIgnoreCase(action)) {
                deleteAllSipAccount();
//            stopSipAndMQTT();
//            PreferencesUtil.setIsmConnectLogoutPrefs(HomeActivity.this, true);
//            PreferencesUtil.setIsLogoutPrefs(HomeActivity.this, true);
//            sendLogoutBroadcast();
                stopService(new Intent(EnHomeActivity.this, PullNotificationService.class));
                SessionContext sessionCtx = SessionContext.getInstance();
                sessionCtx.setLogin(false);
                sessionCtx.setWsSessionId("");
                sessionCtx.persist(getCurrentContext());

                McApplication app = (McApplication) context.getApplicationContext();
                app.getSessionCache().clear();
                app.setLogoutProcessed(false);

                PrefsUtil.setOfflineMode(getCurrentContext(), false);

                String ns = Context.NOTIFICATION_SERVICE;
                NotificationManager mManager = (NotificationManager) getCurrentContext().getSystemService(ns);
                mManager.cancelAll();

                Intent newIntent = new Intent(getCurrentContext(), LoadingActivity.class);
                newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(newIntent);
            }
        }
    }

    boolean exitPtt = false;

    public class FCAReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent i) {
            String action = i.getAction();
            if ("NO_PERMISION".equalsIgnoreCase(action)) {
                if (i.getStringExtra("MSG") == null || i.getStringExtra("MSG").length() == 0) {
                    Toast.makeText(context, i.getStringExtra("MSG"), Toast.LENGTH_LONG).show();
                } else {
//				Toast.makeText(context, getResources().getString(com.fcs.fcsptt.R.string.deactive_notify), Toast.LENGTH_LONG).show();
                }

            }
        }
    }

    public class DuplicateIDReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent i) {
            String action = i.getAction();
            if ("DUPLICATE_RECEIVER".equalsIgnoreCase(action)) {
//			Toast.makeText(context, getResources().getString(com.fcs.fcsptt.R.string.duplicate_login), Toast.LENGTH_LONG).show();
            }
        }
    }

//	public void updateSubcribe() {
//		int UserID = PreferencesUtil.getUserIDPrefs(HomeActivity.this);
//		CheckLoginRole(UserID, mQueue(), HomeActivity.this, new Response.Listener<CurrentUser>() {
//			@Override
//			public void onResponse(CurrentUser response) {
//				if (response != null && response.code == com.fcs.fcsptt.datacenter.Parser.STATUS_CODE_OK) {
//					ArrayList<ChannelModel.ChannelItemModel> channelsItem = response.getCurrentUserItem().getCurrentUser().getChannelsItem();
//					groupSubcribe = new String[channelsItem.size()];
//					for (int i = 0; i < channelsItem.size(); i++) {
//						ChannelModel.ChannelItemModel objIndex = channelsItem.get(i);
//						if (objIndex != null)
//							groupSubcribe[i] = "g" + objIndex.getChannelid();
//					}
////                    sendUnTopics(PreferencesUtil.getGroupPrefs(HomeActivity.this));
//				}
//			}
//		}, new Response.ErrorListener() {
//			@Override
//			public void onErrorResponse(VolleyError error) {
//
//			}
//		});
//
//	}
//
//	public void checkLogin() {
//		String sessionID = SessionContext.getInstance().getWsSessionId();
//		Long userID = SessionContext.getInstance().getFcsUserId();
//		String username = SessionContext.getInstance().getUsername();
//		String name = SessionContext.getInstance().getName();
//		SharedPreferences prefs = getCurrentContext().getSharedPreferences(PreferencesUtil.PREFERENCE_DATA, Context.MODE_PRIVATE);
//		SharedPreferences.Editor editor = prefs.edit();
//		editor.putString(PreferencesUtil.SESSION_ID, sessionID);
//		editor.putInt(PreferencesUtil.USER_ID, userID.intValue());
//		editor.putString(PreferencesUtil.USER_NAME, username);
//		editor.putString(PreferencesUtil.NAME, name);
//		editor.commit();
//		//TODO: get role from server after that if valid will push PTT screen
////		CheckLoginRole(userID.intValue(), mQueue(), HomeActivity.this, listenerCheckUserSuccess, errorListener);
//	}
//
//	public Response.Listener<CurrentUser> listenerCheckUserSuccess = new Response.Listener<CurrentUser>() {
//		@Override
//		public void onResponse(CurrentUser response) {
//			if (response != null && response.code == com.fcs.fcsptt.datacenter.Parser.STATUS_CODE_OK && response.getCurrentUserItem() != null) {
//
//				SharedPreferences prefs = getCurrentContext().getSharedPreferences(PreferencesUtil.PREFERENCE_DATA, Context.MODE_PRIVATE);
//				SharedPreferences.Editor editor = prefs.edit();
//
//				editor.putInt(PreferencesUtil.USER_ID, response.getCurrentUserItem().getCurrentUser().getUserId());
//				setRole(response.getCurrentUserItem().getCurrentUser().getRole());
//				editor.putInt(PreferencesUtil.SPEAK_RELEASE_TIMEOUT, response.getCurrentUserItem().getSpeakerReleaseTimeout());
//				editor.putString(PreferencesUtil.USER_NAME, response.getCurrentUserItem().getCurrentUser().getNickName());
//				editor.putString(PreferencesUtil.NAME, response.getCurrentUserItem().getCurrentUser().getNickName());
//				editor.commit();
//				ArrayList<ChannelModel.ChannelItemModel> channelsItem = response.getCurrentUserItem().getCurrentUser().getChannelsItem();
//				if (channelsItem != null && channelsItem.size() > 0) {
//					if (channelsItem != null) {
//						groupSubcribe = new String[channelsItem.size()];
//						for (int i = 0; i < channelsItem.size(); i++) {
//							ChannelModel.ChannelItemModel objIndex = channelsItem.get(i);
//							if (objIndex != null)
//								groupSubcribe[i] = "g" + objIndex.getChannelid();
//						}
//						PreferencesUtil.saveGroupPrefs(HomeActivity.this, groupSubcribe);
//					}
//				}
//
//			} else {
//				SharedPreferences prefs = getCurrentContext().getSharedPreferences(PreferencesUtil.PREFERENCE_DATA, Context.MODE_PRIVATE);
//				SharedPreferences.Editor editor = prefs.edit();
//				editor.putString(PreferencesUtil.USER_ROLE, null);
//				editor.commit();
//			}
//		}
//	};
//
//	public Response.ErrorListener errorListener = new Response.ErrorListener() {
//		@Override
//		public void onErrorResponse(VolleyError error) {
//			SharedPreferences prefs = getCurrentContext().getSharedPreferences(PreferencesUtil.PREFERENCE_DATA, Context.MODE_PRIVATE);
//			SharedPreferences.Editor editor = prefs.edit();
//			editor.putString(PreferencesUtil.USER_ROLE, null);
//			editor.commit();
//		}
//	};


    //region PROGRESS BAR
    public void showLoading() {
        if (progressBar == null)
            progressBar = ProgressDialog.show(EnHomeActivity.this, "", "Please wait...");

        progressBar.show();
    }

    public void hideLoading() {
        if (progressBar != null && !this.isFinishing() && progressBar.isShowing()) {
            progressBar.dismiss();

        }
    }

    @Override
    protected void onStop() {
        super.onStop();
//
    }


//


    @Override
    protected void onDestroy() {
//		unregisterReceiver(mBatInfoReceiver);
//		Toast.makeText(getApplicationContext(), "Go To Destroy Home mConnect", Toast.LENGTH_LONG).show();
        /**
         * Logout: should stop all started services.
         */

        //just for sure
        deleteAllSipAccount();
        /**
         * as app only gets destroyed when memory is out or user kill from task manager
         */
//		stopSipAndMQTT();

//        sendLogoutBroadcast();

        unregisterReceiver(logoutReceiver);
        unregisterReceiver(loginmConnectDuplication);
        unregisterReceiver(duplicateIDReceiver);
//		mRequestQueue = null;
//		PreferencesUtil.setCurrentCallGroup(HomeActivity.this, "false");
//
//		PreferencesUtil.setActiveUserId(HomeActivity.this, "-1");
//		PreferencesUtil.setExistCallingUserToUserStatus(HomeActivity.this, false);

//		sendLogoutBroadcast();
        unregisterReceiver(mCallReceive);
//		mCallReceive = null;
        super.onDestroy();

    }


    /**
     * end mqtt
     */

    private boolean isSameSipServiceRunning;
    private boolean isSipServiceRunning;
    private boolean isMQTTServiceRunning;

    private void preparePTT() {
//		String role = HomeActivity.this.getSharedPreferences(PreferencesUtil.PREFERENCE_DATA, Context.MODE_PRIVATE).getString("USER_ROLE", null);
//		try {
//			SharedPreferences prefs = getCurrentContext().getSharedPreferences(PreferencesUtil.PREFERENCE_DATA, Context.MODE_PRIVATE);
//			SharedPreferences.Editor editor = prefs.edit();
//			Long userID = SessionContext.getInstance().getFcsUserId();
//			String keyStr = "";
//			if (getPackageName().equalsIgnoreCase(PTTConstants.PACKET_MHSKP))
//				keyStr = "des=^qry=^usr=^pwd=^dat=^dur=^platform=1^applicationId=" + PTTConstants.MHSKP + "^uid=" + userID;
//			else if (getPackageName().equalsIgnoreCase(PTTConstants.PACKET_MCONNECT))
//				keyStr = "des=^qry=^usr=^pwd=^dat=^dur=^platform=1^applicationId=" + PTTConstants.MCONNECT + "^uid=" + userID;
//			//
//			String encryptText = SecurityUtil.encrypt(keyStr);
//			editor.putString(PreferencesUtil.SECRETKEY, encryptText);
//			editor.putInt(PreferencesUtil.USER_ID, userID.intValue());
//			editor.commit();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		String serverURL = HomeActivity.this.getSharedPreferences(PreferencesUtil.PREFERENCE_DATA, Context.MODE_PRIVATE).getString("SERVER_URL_PTT", "");
////		if (serverURL.length() > 0) {
////			checkLogin();
////		}
//
//		pushReceiver = new PushReceiver();
//
//		intentFilter = new IntentFilter();
//
//		intentFilter.addAction(FcsSipMqttService.PACKAGE);
//
        registerReceiver(logoutReceiver, new IntentFilter("NO_PERMISION"), null, null);
        registerReceiver(loginmConnectDuplication, new IntentFilter("LOGIN_MCONNECT_DUPLICATION"), null, null);
        registerReceiver(duplicateIDReceiver, new IntentFilter("DUPLICATE_RECEIVER"), null, null);
//		PreferencesUtil.setAlreadyOpenPTT(this, false);
    }

//	OnClickListener didClickPtt = new OnClickListener() {
//		@Override
//		public void onClick(View v) {
//
//
//			if (exitPtt) {
//				exitPtt = false;
//			}
//			// sipservice already start, so dont go to ptt screen.
//			if (isSameSipServiceRunning) {
//				Toast.makeText(HomeActivity.this, getResources().getString(com.fcs.fcsptt.R.string.ptt_already_running), Toast.LENGTH_SHORT).show();
//				return;
//			}
//
//			Long userID = SessionContext.getInstance().getFcsUserId();
//
//			String serverURL = PreferencesUtil.getServerPTTURL(HomeActivity.this);//HomeActivity.this.getSharedPreferences(PreferencesUtil.PREFERENCE_DATA, Context.MODE_PRIVATE).getString("SERVER_URL_PTT", "");
//			if (serverURL.length() == 0) {
//				Toast.makeText(HomeActivity.this, getResources().getString(com.fcs.fcsptt.R.string.url_does_not_config), Toast.LENGTH_SHORT).show();
//				return;
//			}
//
//			//TODO: get role from server after that if valid will push PTT screen
//			showLoading();
//			CheckLoginRole(userID.intValue(), mQueue(), HomeActivity.this, new Response.Listener<CurrentUser>() {
//						@Override
//						public void onResponse(CurrentUser response) {
//							hideLoading();
//							if (response != null && response.code == com.fcs.fcsptt.datacenter.Parser.STATUS_CODE_OK && response.getCurrentUserItem() != null) {
//
//								SharedPreferences prefs = getCurrentContext().getSharedPreferences(PreferencesUtil.PREFERENCE_DATA, Context.MODE_PRIVATE);
//								SharedPreferences.Editor editor = prefs.edit();
//
//								editor.putInt(PreferencesUtil.USER_ID, response.getCurrentUserItem().getCurrentUser().getUserId());
//								editor.putInt(PreferencesUtil.SPEAK_RELEASE_TIMEOUT, response.getCurrentUserItem().getSpeakerReleaseTimeout());
//								editor.putString(PreferencesUtil.USER_NAME, response.getCurrentUserItem().getCurrentUser().getNickName());
//								editor.putString(PreferencesUtil.NAME, response.getCurrentUserItem().getCurrentUser().getUserName());
//								editor.putString(PreferencesUtil.TOKEN_KEY, response.getCurrentUserItem().getAccess_token());
//								editor.commit();
//								setRole(response.getCurrentUserItem().getCurrentUser().getRole());
//								String role = PreferencesUtil.getUserRole(HomeActivity.this);
//								if (role.equals(PTTConstants.PARAM_NO_PERMISSION)) {
//									Toast.makeText(HomeActivity.this, getResources().getString(com.fcs.fcsptt.R.string.have_not_permission), Toast.LENGTH_SHORT).show();
//									return;
//								}
//								PreferencesUtil.setGroupDefaultPrefs(HomeActivity.this, "");
//								if (response.getCurrentUserItem().getCurrentUser().getChannelsItem() != null && response.getCurrentUserItem().getCurrentUser().getChannelsItem().size() > 0) {
//									for (ChannelModel.ChannelItemModel IndexObj : response.getCurrentUserItem().getCurrentUser().getChannelsItem()) {
//										if (IndexObj.getIsDefault() == 1) {
////                                        if (PreferencesUtil.getGroupDefaultPrefs(HomeActivity.this).length() == 0)
//											PreferencesUtil.setGroupDefaultPrefs(HomeActivity.this, IndexObj.getChannelid() + "|" + IndexObj.getGroupName());
//											break;
//										}
//									}
//								}
//								ArrayList<ChannelModel.ChannelItemModel> channelsItem = response.getCurrentUserItem().getCurrentUser().getChannelsItem();
//								if (channelsItem != null && channelsItem.size() > 0) {
//									if (channelsItem != null) {
//										groupSubcribe = new String[channelsItem.size()];
//										for (int i = 0; i < channelsItem.size(); i++) {
//											ChannelModel.ChannelItemModel objIndex = channelsItem.get(i);
//											if (objIndex != null)
//												groupSubcribe[i] = "g" + objIndex.getChannelid();
//										}
//										PreferencesUtil.saveGroupPrefs(HomeActivity.this, groupSubcribe);
//									}
//								}
//								if (response.getCurrentUserItem().getDepartments() != null && response.getCurrentUserItem().getDepartments().size() > 0) {
//									Set<String> departmentArray = new HashSet<String>();
//
//									for (CurrentUser.CurrentUserModel.Department indexObj : response.getCurrentUserItem().getDepartments()) {
//										departmentArray.add(indexObj.getObjId() + "|" + indexObj.getDepartmentName());
//									}
//									PreferencesUtil.setDepartmentPrefs(HomeActivity.this, departmentArray);
//								}
//
//								if (role == null) {
//									Toast.makeText(HomeActivity.this, getResources().getString(com.fcs.fcsptt.R.string.have_not_permission), Toast.LENGTH_SHORT).show();
//								} else if (role.equals("Super Admin")) {
//									Toast.makeText(HomeActivity.this, role, Toast.LENGTH_SHORT).show();
//								} else {
//									PreferencesUtil.setAlreadyOpenPTT(HomeActivity.this, true);
//									PreferencesUtil.setIsLogoutPrefs(HomeActivity.this, false);
//									PreferencesUtil.setIsmConnectLogoutPrefs(HomeActivity.this, false);
//									Intent i = new Intent(HomeActivity.this, PushToTalkActivity.class);
//									i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//									startActivity(i);
//
//								}
//							} else {
//								Toast.makeText(HomeActivity.this, getResources().getString(com.fcs.fcsptt.R.string.does_not_exist), Toast.LENGTH_SHORT).show();
//							}
//						}
//					}, new Response.ErrorListener() {
//						@Override
//						public void onErrorResponse(VolleyError error) {
//							hideLoading();
//
//
//							NetworkResponse response = error.networkResponse;
//							if (response != null && response.data != null) {
//								switch (response.statusCode) {
//									case 500: {
//										Toast.makeText(HomeActivity.this,
//												getResources().getString(com.fcs.fcsptt.R.string.server_error),
//												Toast.LENGTH_LONG).show();
//										break;
//									}
//									case 401: {
//										Toast.makeText(HomeActivity.this, getResources().getString(com.fcs.fcsptt.R.string.does_not_exist), Toast.LENGTH_SHORT).show();
//										break;
//									}
//									case 403: {
//										Toast.makeText(HomeActivity.this, getResources().getString(com.fcs.fcsptt.R.string.have_not_permission), Toast.LENGTH_SHORT).show();
//										break;
//									}
//									case 404:{
//										Toast.makeText(HomeActivity.this, "Server URL not found", Toast.LENGTH_SHORT).show();
//										break;
//									}
//									default: {
//										Toast.makeText(HomeActivity.this, getResources().getString(com.fcs.fcsptt.R.string.disconnect_msg_ptt), Toast.LENGTH_SHORT).show();
//									}
//								}
//							} else {
//								if (error instanceof TimeoutError || error instanceof NoConnectionError) {
//
//									Toast.makeText(HomeActivity.this,
//											getResources().getString(com.fcs.fcsptt.R.string.network_error_message),
//											Toast.LENGTH_LONG).show();
//									return;
//								} else if (error instanceof AuthFailureError) {
//									//TODO
//									Toast.makeText(HomeActivity.this, getResources().getString(com.fcs.fcsptt.R.string.have_not_permission), Toast.LENGTH_SHORT).show();
//									return;
//								} else if ((error instanceof ParseError) || (error instanceof ServerError)
//										|| (error instanceof NetworkError)) {
//									//TODO
//									Toast.makeText(HomeActivity.this, getResources().getString(com.fcs.fcsptt.R.string.disconnect_msg_ptt), Toast.LENGTH_SHORT).show();
//									return;
//								}
//
//								Toast.makeText(HomeActivity.this, "Error: Unknown error code", Toast.LENGTH_SHORT).show();
//							}
//						}
//					}
//			);
//		}
//	};

    private static final String SECRET_KEY = "9jLjXmk4hkQ/DvHswmZggGQJGsgQdPAd";

    private class PanicButtonSubmit extends McProgressAsyncTask {

        private boolean result;

        public PanicButtonSubmit(Context context) {
            super(context);
        }

        @Override
        public void doInBackground() {
            result = ECService.getInstance().postPanicButtonAlert(getCurrentContext(), "", SECRET_KEY, "");
        }

        @Override
        public void onPostExecute() {
            if (result) {
                Toast.makeText(getCurrentContext(), getString(R.string.success_submit_panic_message), Toast.LENGTH_LONG).show();

            } else {
                Toast.makeText(getCurrentContext(), getString(R.string.failed_sumit_panic_message), Toast.LENGTH_LONG).show();
            }
        }
    }

    public static class HomeCallReciever extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if ("android.intent.action.PHONE_STATE".equalsIgnoreCase(action)) {
                if (PrefsUtil.isClickPanic(context)) {
                    if (intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(
                            TelephonyManager.EXTRA_STATE_RINGING)) {

                        // Phone number
                        String incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);

                        // Ringing state
                        // This code will execute when the phone has an incoming call
                    } else if (intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(
                            TelephonyManager.EXTRA_STATE_IDLE)) {

                        //AlertPanicManager.getInstance().playSoundAlert();

                    } else if (intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(
                            TelephonyManager.EXTRA_STATE_OFFHOOK)) {
                        String endCall = "end call2";
                        Log.d("beanbechoi", endCall);
                    }

                }
            }
        }
    }

}
