package com.fcscs.android.mconnectv3.manager.model;

import com.fcscs.android.mconnectv3.common.McEnums.JobStatus;

import java.io.Serializable;
import java.util.Date;

public class EquipmentModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String equipmentName;
    private String equipmentNumber;
    private String location;
    private int equipmentStatusID;
    private String equipmentStatus;
    private int jobStatusID;
    private String jobStatus;
    private String jobDetails;
    private String group;
    private String tag;
    private String manufacturer;
    private String model;
    private String serialNumber;
    private Date dateCommissioned;
    private int warrantyContractID;
    private Date warrantyExpiryDate;
    private String warrantyCompany;
    private String warrantyContactPhone;
    private String warrantyContactEmail;

    public String getEquipmentName() {
        return equipmentName;
    }

    public void setEquipmentName(String equipmentName) {
        this.equipmentName = equipmentName;
    }

    public String getEquipmentNumber() {
        return equipmentNumber;
    }

    public void setEquipmentNumber(String equipmentNumber) {
        this.equipmentNumber = equipmentNumber;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getEquipmentStatusID() {
        return equipmentStatusID;
    }

    public void setEquipmentStatusID(int equipmentStatusID) {
        this.equipmentStatusID = equipmentStatusID;
    }

    public String getEquipmentStatus() {
        return equipmentStatus;
    }

    public void setEquipmentStatus(String equipmentStatus) {
        this.equipmentStatus = equipmentStatus;
    }

    public int getJobStatusID() {
        return jobStatusID;
    }

    public void setJobStatusID(int jobStatusID) {
        this.jobStatusID = jobStatusID;
    }

    public String getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus;
    }

    public String getJobDetails() {
        return jobDetails;
    }

    public void setJobDetails(String jobDetails) {
        this.jobDetails = jobDetails;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Date getDateCommissioned() {
        return dateCommissioned;
    }

    public void setDateCommissioned(Date dateCommissioned) {
        this.dateCommissioned = dateCommissioned;
    }

    public int getWarrantyContractID() {
        return warrantyContractID;
    }

    public void setWarrantyContractID(int warrantyContractID) {
        this.warrantyContractID = warrantyContractID;
    }

    public Date getWarrantyExpiryDate() {
        return warrantyExpiryDate;
    }

    public void setWarrantyExpiryDate(Date warrantyExpiryDate) {
        this.warrantyExpiryDate = warrantyExpiryDate;
    }

    public String getWarrantyCompany() {
        return warrantyCompany;
    }

    public void setWarrantyCompany(String warrantyCompany) {
        this.warrantyCompany = warrantyCompany;
    }

    public String getWarrantyContactPhone() {
        return warrantyContactPhone;
    }

    public void setWarrantyContactPhone(String warrantyContactPhone) {
        this.warrantyContactPhone = warrantyContactPhone;
    }

    public String getWarrantyContactEmail() {
        return warrantyContactEmail;
    }

    public void setWarrantyContactEmail(String warrantyContactEmail) {
        this.warrantyContactEmail = warrantyContactEmail;
    }
}
