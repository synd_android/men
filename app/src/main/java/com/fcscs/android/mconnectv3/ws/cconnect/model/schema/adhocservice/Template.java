package com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice;

import java.io.Serializable;

import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;

public class Template implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8282087451820485285L;

    private long templateId;
    private String templateName;

    //econnect
    private String detail;

    public Template() {
    }

    public Template(SoapObject soap) {
        this.templateId = Long.valueOf(soap.getAttribute("templateId").toString());
        if (soap.hasProperty("templateName") && (soap.getProperty("templateName") instanceof SoapPrimitive)) {
            this.templateName = ((SoapPrimitive) soap.getProperty("templateName")).toString();
        }
    }

    public long getTemplateId() {
        return templateId;
    }

    public String getTemplateName() {
        return templateName;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public void setTemplateId(long templateId) {
        this.templateId = templateId;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    @Override
    public String toString() {
        return templateName == null ? super.toString() : templateName;
    }

}