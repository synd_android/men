package com.fcscs.android.mconnectv3.common;

import android.content.Context;

import com.fcscs.android.mconnectv3.common.util.McUtils;

public class McExceptionHandlerBase {

    protected Context context;

    public McExceptionHandlerBase(Context context) {
        super();
        this.context = context;
    }

    protected void saveThrowableToSDCard(Throwable e) {
        if (McUtils.isSDCardExist()) {
            McUtils.saveCrashReport(context, e);
        }
    }

}