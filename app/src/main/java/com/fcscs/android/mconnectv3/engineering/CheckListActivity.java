package com.fcscs.android.mconnectv3.engineering;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.BaseActivity;
import com.fcscs.android.mconnectv3.common.SessionContext;
import com.fcscs.android.mconnectv3.common.ui.HomeTopBar;
import com.fcscs.android.mconnectv3.common.ui.McProgressAsyncTask;
import com.fcscs.android.mconnectv3.common.util.DateTimeHelper;
import com.fcscs.android.mconnectv3.manager.model.JobModel;
import com.fcscs.android.mconnectv3.ws.econnect.ECService;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.EngineeringChecklistDetails;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.GetEngineeringChecklistResponse;

import java.util.List;

public class CheckListActivity extends BaseActivity {
    private JobModel jm;
    public static final String KEY_JOBMODEL = "JobModel";
    private TextView tvChecklist;
    private TextView tvEquip;
    private TextView tvEquipNo;
    private TextView tvLocation;
    private TextView tvJob;
    private TextView tvDate;
    private TextView tvAssignTo;
    private LinearLayout llJobDetail;
    private LinearLayout llInspect;
    private TextView tvCompletionDate;
    private TextView tvInspectedBy;
    private TextView tvInspectedDate;
    private LinearLayout llList;
    private List<EngineeringChecklistDetails> listItem;
    private TextView tvUpdate;
    private HomeTopBar topBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_list);
        Bundle bundle = getIntent().getExtras();
        if (bundle.get(KEY_JOBMODEL) != null) {
            jm = (JobModel) bundle.get(KEY_JOBMODEL);
        }
        topBar = (HomeTopBar) findViewById(R.id.top_bar);
        topBar.getTitleTv().setText(R.string.checklist);
        llJobDetail = (LinearLayout) findViewById(R.id.ll_jobdetail);
        llInspect = (LinearLayout) findViewById(R.id.ll_inspected);
        tvChecklist = (TextView) findViewById(R.id.tv_checklist_title);
        tvEquip = (TextView) findViewById(R.id.tv_equip_title);
        tvEquipNo = (TextView) findViewById(R.id.tv_equip_no_title);
        tvLocation = (TextView) findViewById(R.id.tv_location_title);
        tvJob = (TextView) findViewById(R.id.tv_job_title);
        tvDate = (TextView) findViewById(R.id.tv_date_title);
        tvAssignTo = (TextView) findViewById(R.id.tv_assign_title);
        tvCompletionDate = (TextView) findViewById(R.id.tv_conpletion_date);
        tvInspectedBy = (TextView) findViewById(R.id.tv_inspected_by);
        tvInspectedDate = (TextView) findViewById(R.id.tv_inspected_date);
        llList = (LinearLayout) findViewById(R.id.ll_list);
        tvUpdate = (TextView) findViewById(R.id.btn_update_msg);
        tvUpdate.setVisibility(View.VISIBLE);
        setDataForJobdetail(jm);
        new loadChecklist(this).execute();
        updateChecklist();
    }


    private void setDataForJobdetail(JobModel jobModel) {
        tvEquip.setText(jobModel.getEquipmentName());
        tvEquipNo.setText(jobModel.getEquipmentNo());
        tvLocation.setText(jobModel.getLocationDesc());
        tvJob.setText(jobModel.getJobNum());
        tvAssignTo.setText(jobModel.getAssignTo());
        String strCompletionDate = "";
        if (jobModel.getCompletedTime() != null) {
            strCompletionDate = jobModel.getCompletedTime() + "";
        }
        tvCompletionDate.setText(strCompletionDate);
        tvDate.setText(DateTimeHelper.getStrDateByTimeZone(jobModel.getDate()));
    }

    @Override
    public boolean isLoginActivity() {
        return false;
    }

    @Override
    public boolean isHomeActivity() {
        return false;
    }

    private class loadChecklist extends McProgressAsyncTask {
        Context mContext;
        GetEngineeringChecklistResponse res;

        public loadChecklist(Context context) {
            super(context);
            this.mContext = context;
        }

        @Override
        public void doInBackground() {
            res = ECService.getInstance().GetEngineeringChecklist(mContext, jm.getJobId() + "");
        }

        @Override
        public void onPostExecute() {
            if (res == null || res.getList() == null || res.getList().size() == 0) {
                toast(R.string.null_checklist);
                finish();
            } else {
                listItem = res.getList();
                setDataList(llList);
            }
        }
    }

    private void updateChecklist() {
        tvUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new updateCheckList(CheckListActivity.this).execute();
            }
        });
    }

    private class updateCheckList extends McProgressAsyncTask {
        boolean isResult = false;
        Context mContext;

        public updateCheckList(Context context) {
            super(context);
            mContext = context;
        }

        @Override
        public void doInBackground() {
            isResult = ECService.getInstance().updateChecklist(CheckListActivity.this, listItem, "" + jm.getJobId());
        }

        @Override
        public void onPostExecute() {
            if (isResult) {
                Toast.makeText(mContext, mContext.getString(R.string.update_successfully), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(mContext, mContext.getString(R.string.update_fail), Toast.LENGTH_SHORT).show();
            }
            finish();
        }
    }

    private class Viewholder {
        TextView tvContent;
        LinearLayout llRemark;
        LinearLayout llInput;
        //        TextView tvReamrkContent;
        EditText edRemarkContent;
        TextView tvCheckListRemark;
        TextView tvInputcontent;
        RadioGroup radioGroup;
        RadioButton raYes;
        RadioButton raNo;
        TextView tvInputRemark;
        EditText edInput;
    }

    private void setDataList(final LinearLayout list) {
        LayoutInflater inflater = this.getLayoutInflater();
        Viewholder viewholder;
        tvChecklist.setText(listItem.get(0).getChecklistName());
        for (int i = 0; i < listItem.size(); i++) {
            viewholder = new Viewholder();
            final EngineeringChecklistDetails checklist = listItem.get(i);
            final View vi = inflater.inflate(R.layout.checklist_item, null);
            viewholder.tvContent = (TextView) vi.findViewById(R.id.tv_checklist_content);
            viewholder.tvCheckListRemark = (TextView) vi.findViewById(R.id.tv_checklist_remark);
            viewholder.tvInputcontent = (TextView) vi.findViewById(R.id.tv_checklist_input_content);
            viewholder.tvInputRemark = (TextView) vi.findViewById(R.id.tv_checklist_input_remark);
            viewholder.llRemark = (LinearLayout) vi.findViewById(R.id.ll_checklist_remark);

            viewholder.radioGroup = (RadioGroup) vi.findViewById(R.id.radio_checklist_input);
            viewholder.raYes = (RadioButton) vi.findViewById(R.id.radio_checklist_yes);
            viewholder.raNo = (RadioButton) vi.findViewById(R.id.radio_checklist_no);

            viewholder.edInput = (EditText) vi.findViewById(R.id.ed_input);
            viewholder.edRemarkContent = (EditText) vi.findViewById(R.id.ed_checklist_remark);

            viewholder.llInput = (LinearLayout) vi.findViewById(R.id.ll_checklist_input);
            list.addView(vi);

            viewholder.edInput.setText(checklist.getInputAnswer());
            viewholder.edRemarkContent.setText(checklist.getRemarksAnswer());

            viewholder.tvInputRemark.setTag(viewholder);
            viewholder.tvInputRemark.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Viewholder viewholder1 = (Viewholder) view.getTag();
                    if (viewholder1.edRemarkContent.getVisibility() == View.GONE) {
                        viewholder1.llRemark.setVisibility(View.VISIBLE);
                        viewholder1.edRemarkContent.setVisibility(View.VISIBLE);
                    } else {
                        viewholder1.edRemarkContent.setVisibility(View.GONE);
                    }
                }
            });
            if (!TextUtils.isEmpty(checklist.getContent())) {
                viewholder.tvContent.setVisibility(View.VISIBLE);
                viewholder.tvContent.setText(checklist.getContent());
            } else {
                viewholder.tvContent.setVisibility(View.GONE);
            }
            if (checklist.getInput().equalsIgnoreCase("Text")
                    || TextUtils.isEmpty(checklist.getInput()) || checklist.getInput().equalsIgnoreCase("null")) {
                viewholder.llInput.setVisibility(View.VISIBLE);
                viewholder.radioGroup.setVisibility(View.GONE);
                if (!checklist.getLabel().equals("")) {
                    viewholder.tvInputcontent.setVisibility(View.VISIBLE);
                    viewholder.tvInputcontent.setText(checklist.getLabel());
                }
                viewholder.edInput.setVisibility(View.VISIBLE);
                if (checklist.isRemarks()) {
                    viewholder.tvInputRemark.setVisibility(View.VISIBLE);
                } else {
                    viewholder.tvInputRemark.setVisibility(View.GONE);
                }

            } else if (checklist.getInput().equalsIgnoreCase("Yes/No")) {
                viewholder.llInput.setVisibility(View.VISIBLE);
                viewholder.radioGroup.setVisibility(View.VISIBLE);
                viewholder.tvInputcontent.setVisibility(View.GONE);
                viewholder.raYes.setChecked(false);
                viewholder.raNo.setChecked(false);
                if (checklist.isRemarks()) {
                    viewholder.tvInputRemark.setVisibility(View.VISIBLE);
                } else {
                    viewholder.tvInputRemark.setVisibility(View.GONE);
                }
                if (!checklist.getAnswerID().equals("") && !checklist.getInputAnswer().equals("")) {
                    if (checklist.getInputAnswer().equalsIgnoreCase("YES")) {
                        viewholder.raYes.setChecked(true);
                    } else {
                        viewholder.raNo.setChecked(true);
                    }
                }
            }

            viewholder.edInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    checklist.setInputAnswer(charSequence.toString());
                }

                @Override
                public void afterTextChanged(Editable editable) {
                }
            });
            viewholder.edRemarkContent.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    checklist.setRemarksAnswer(charSequence.toString());
                }

                @Override
                public void afterTextChanged(Editable editable) {
                }
            });

            viewholder.radioGroup.setTag(viewholder);
            viewholder.radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                    Viewholder viewholder1 = (Viewholder) radioGroup.getTag();
                    if (i == viewholder1.raYes.getId()) {
                        checklist.setInputAnswer("YES");
                    } else {
                        checklist.setInputAnswer("NO");
                    }
                }
            });
        }
    }

}
