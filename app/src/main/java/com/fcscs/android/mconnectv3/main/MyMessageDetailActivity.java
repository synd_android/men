package com.fcscs.android.mconnectv3.main;

import java.util.Date;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import com.fcscs.android.mconnectv3.GCMNotification;
import com.fcscs.android.mconnectv3.McApplication;
import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.BaseActivity;
import com.fcscs.android.mconnectv3.common.ui.HomeTopBar;
import com.fcscs.android.mconnectv3.common.ui.ImagePreviewLayout;
import com.fcscs.android.mconnectv3.common.ui.McBackgroundAsyncTask;
import com.fcscs.android.mconnectv3.common.ui.RecordPlayLayout;
import com.fcscs.android.mconnectv3.common.util.DateTimeHelper;
import com.fcscs.android.mconnectv3.common.util.DialogHelper;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.common.util.PrefsUtil;
import com.fcscs.android.mconnectv3.common.util.VibrateUtils;
import com.fcscs.android.mconnectv3.manager.model.MessageModel;
import com.fcscs.android.mconnectv3.ws.cconnect.AdhocService;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.GetMsgResponse;
import com.fcscs.android.mconnectv3.ws.econnect.ECService;
import com.fcscs.android.mconnectv3.ws.econnect.ECService.MediaWrapper;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.AckAdHocResponse;

public class MyMessageDetailActivity extends BaseActivity {
    private static final int RC_MESSAGE = 1;
    private TextView sendUsernameText;
    private TextView ackStatusText;
    private TextView receiveDateText;
    private TextView messageText;
    private HomeTopBar homeTopBar;
    private GetMsgResponse myResponse;
    private MessageModel model;
    private TextView btn_bottom_left;
    private TextView btn_bottom_right;
    private AckMessageLoader task;
    protected DeleteMessageLoader task1;
    public ImagePreviewLayout llImagePreview;
    public RecordPlayLayout llRecordPlay;
    public MediaWrapper wrapper;

    public static final String KEY_RESULT_CC = "result_cc";
    public static final String KEY_RESULT_EC = "result_ec";

    public static Intent createCConnectIntent(Context ctx, GetMsgResponse response) {
        Intent intent = new Intent(ctx, MyMessageDetailActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(KEY_RESULT_CC, response);
        return intent;
    }

    public static Intent createEConnectIntent(Context ctx, MessageModel model) {
        Intent i = new Intent(ctx, MyMessageDetailActivity.class);
        i.putExtra(KEY_RESULT_EC, model);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_message_detail);

        if (!((McApplication) getApplication()).isLogin()) {
            toast(R.string.already_logout);
        }

        btn_bottom_left = (TextView) findViewById(R.id.btn_bottom_left1);
        btn_bottom_right = (TextView) findViewById(R.id.btn_bottom_right1);
        homeTopBar = (HomeTopBar) findViewById(R.id.top_bar);
        homeTopBar.getTitleTv().setText(getString(R.string.message_detail));

        Bundle bundle = getIntent().getExtras();
        if (isCConnect()) {
            myResponse = (GetMsgResponse) bundle.get(KEY_RESULT_CC);
            if (myResponse == null) {
                return;
            }
            sendUsernameText = (TextView) findViewById(R.id.my_message_detail_text2);
            sendUsernameText.setText(myResponse.getFromUserName());
            ackStatusText = (TextView) findViewById(R.id.my_message_detail_text4);
            ackStatusText.setText(myResponse.isAck() ? getString(R.string.acknowledge) : getString(R.string.not_acknowledge));
            receiveDateText = (TextView) findViewById(R.id.my_message_detail_text6);
            receiveDateText.setText(McUtils.formatDateTime(myResponse.getCreatedDate()));
            messageText = (TextView) findViewById(R.id.my_message_detail_text8);
            messageText.setText(myResponse.getDetail());
        } else {
            model = (MessageModel) bundle.get(KEY_RESULT_EC);
            if (model == null) {
                return;
            }
            sendUsernameText = (TextView) findViewById(R.id.my_message_detail_text2);
            sendUsernameText.setText(model.getFromUsername());
            ackStatusText = (TextView) findViewById(R.id.my_message_detail_text4);
            ackStatusText.setText(model.getAckDate() != null ? getString(R.string.acknowledge)
                    : getString(R.string.not_acknowledge));
            receiveDateText = (TextView) findViewById(R.id.my_message_detail_text6);
            receiveDateText.setText(McUtils.formatDateTime(model.getCreatedDate()));
            messageText = (TextView) findViewById(R.id.my_message_detail_text8);
            messageText.setText(model.getTitle());
            new GetMessageMedia(this, "" + model.getMsgDoneId()).exec();
        }

        if (isCConnect()) {
            if (myResponse.isAck()) {
                btn_bottom_left.setVisibility(View.INVISIBLE);
                btn_bottom_right.setVisibility(View.VISIBLE);
            } else {
                btn_bottom_left.setVisibility(View.VISIBLE);
                btn_bottom_right.setVisibility(View.INVISIBLE);
            }
        } else {
            boolean autoAckMsg = PrefsUtil.isAutoAckMsg(this);
            if (null != model.getAckDate() || autoAckMsg) {
                btn_bottom_left.setVisibility(View.INVISIBLE);
                btn_bottom_right.setVisibility(View.INVISIBLE);
            } else {
                btn_bottom_left.setVisibility(View.VISIBLE);
                btn_bottom_right.setVisibility(View.INVISIBLE);
            }
            if (autoAckMsg) {
                ackMsg(true);
            }
        }
        btn_bottom_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VibrateUtils.needVibrate(getCurrentContext());
                ackMsg(false);
            }
        });

        btn_bottom_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VibrateUtils.needVibrate(getCurrentContext());
                confirmDelete();
            }
        });

    }

    private void ackMsg(boolean isAutoAck) {
        long msgDoneId;
        if (isCConnect()) {
            msgDoneId = myResponse.getMsgDoneId();
        } else {
            msgDoneId = model.getMsgDoneId();
        }

        if (task == null || AsyncTask.Status.RUNNING.equals(task.getStatus()) == false) {
            task = new AckMessageLoader(getCurrentContext(), msgDoneId, isAutoAck);
            task.exec();
        }
    }

    class AckMessageLoader extends McBackgroundAsyncTask {

        public AckMessageLoader(Context context, long msgDoneId, boolean isAutoAck) {
            super(context);
            this.msgDoneId = msgDoneId;
            this.isAutoAck = isAutoAck;
        }

        private long msgDoneId;
        private Boolean response;
        private boolean isAutoAck;

        @Override
        public void doInBackground() {
            if (isCConnect()) {
                response = AdhocService.getInstance().updateMsgStatus(getCurrentContext(), msgDoneId);

            } else {
                AckAdHocResponse response = ECService.getInstance().ackAdHoc(getCurrentContext(), "" + msgDoneId);
                if (response != null) {
                    this.response = response.isSuccess();
                }
            }
        }

        @Override
        public void onPostExecute() {
            if (response != null) {
                if (!response) {
                    toast(getString(R.string.update_fail));
                } else if (response) {
                    model.setAckDate(DateTimeHelper.getDateByTimeZone(new Date()));
                    model.setAcknowledged(true);
                    btn_bottom_left.setVisibility(View.INVISIBLE);
                    ackStatusText.setText(getString(R.string.acknowledge));
                    GCMNotification.cancelECMsgNotification(getCurrentContext(), "" + model.getMsgDoneId());
                    if (!isAutoAck) {
                        toast(R.string.acknowledge_message_successfully);
                        goBack();
                    }
                }
            }
        }

    }

    class DeleteMessageLoader extends McBackgroundAsyncTask {

        private long msgDoneId;
        private Boolean response;

        public DeleteMessageLoader(Context context, long msgDoneId) {
            super(context);
            this.msgDoneId = msgDoneId;
        }

        @Override
        public void doInBackground() {
            if (isCConnect()) {
                response = AdhocService.getInstance().deleteMsg(getCurrentContext(), msgDoneId);

            }
        }

        @Override
        public void onPostExecute() {
            if (response != null) {
                if (!response) {
                    toast(getString(R.string.delete_fail));
                } else {
                    toast(getString(R.string.delete_successfully));
                    final Intent i = new Intent();
                    setResult(RESULT_OK, i);
                    i.putExtra(KEY_RESULT_CC, true);
                    finish();
                }
            }
        }

    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            goBack();
            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

    private void goBack() {
        Intent i = new Intent();
        i.putExtra("msg", model);
        setResult(RESULT_OK, i);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == RC_MESSAGE) {
            myResponse = (GetMsgResponse) data.getSerializableExtra(KEY_RESULT_CC);
        } else {
            setResult(RESULT_OK);
            finish();
        }
    }

    public boolean confirmDelete() {
        DialogHelper.showYesNoDialog(getCurrentContext(), R.string.confirm_delete, new OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (myResponse != null) {
                    ;

                    if (task1 == null || AsyncTask.Status.RUNNING.equals(task1.getStatus()) == false) {
                        task1 = new DeleteMessageLoader(getCurrentContext(), myResponse.getMsgDoneId());
                        task1.exec();
                    }
                }
            }
        });
        return true;
    }

    @Override
    public boolean isLoginActivity() {
        return true;
    }

    @Override
    public boolean isHomeActivity() {
        return false;
    }

    class GetMessageMedia extends McBackgroundAsyncTask {

        private String msgId;

        public GetMessageMedia(Context context, String msgId) {
            super(context);
            this.msgId = msgId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            llRecordPlay = (RecordPlayLayout) findViewById(R.id.ll_record_play);
            llImagePreview = (ImagePreviewLayout) findViewById(R.id.ll_image_preview);
        }

        @Override
        public void doInBackground() {
            wrapper = ECService.getInstance().getMediaLibraryContent(getCurrentContext(), msgId, 1);
        }

        @Override
        public void onPostExecute() {

            llImagePreview.setImageFile(wrapper.image);
            llImagePreview.setOnClickImage(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getCurrentContext(), ImagePreviewActivity.class);
                    i.putExtra("file", wrapper.image);
                    startActivity(i);
                }
            });

            llRecordPlay.setRecordFile(wrapper.voice);

        }

    }

}
