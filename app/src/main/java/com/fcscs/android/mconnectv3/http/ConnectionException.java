package com.fcscs.android.mconnectv3.http;

public class ConnectionException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 3467629738064948326L;

    public ConnectionException() {
        super();
    }

    public ConnectionException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

}
