package com.fcscs.android.mconnectv3.main;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

//import com.fcs.fcsptt.activity.SettingActivity;
import com.fcscs.android.mconnectv3.CheckNetworkConnectivityReceiver;
import com.fcscs.android.mconnectv3.CheckNetworkConnectivityReceiver.CheckConnectivity;
import com.fcscs.android.mconnectv3.McApplication;
import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.BaseActivity;
import com.fcscs.android.mconnectv3.common.ConfigContext;
import com.fcscs.android.mconnectv3.common.McConstants;
import com.fcscs.android.mconnectv3.common.McEnums;
import com.fcscs.android.mconnectv3.common.QRCodeParserFactory;
import com.fcscs.android.mconnectv3.common.SessionContext;
import com.fcscs.android.mconnectv3.common.ui.McProgressAsyncTask;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.common.util.PrefsUtil;
import com.fcscs.android.mconnectv3.common.util.VibrateUtils;
import com.fcscs.android.mconnectv3.manager.model.Language;
import com.fcscs.android.mconnectv3.smartpush.SmartPush;
import com.fcscs.android.mconnectv3.ws.cconnect.SecurityService;
import com.fcscs.android.mconnectv3.ws.econnect.ECService;
import com.fcscs.android.mconnectv3.zxing.CaptureActivity;

public class ConfigActivity extends BaseActivity {

    private static final int BUFFER_SIZE = 1024;

    private TextView btnSubmit;
    // ismartpush
    private RadioGroup rgPushService;
    private EditText etMQTTUrl;

    private Button email;
    private EditText contextUrlEt;
    private RadioGroup rgServiceType;
    public static McEnums.ServiceType DEFAULT_SERVICE_TYPE = McEnums.ServiceType.ECONNECT;
    private boolean useCconnect = false;
    private EditText etPropId;
    //add hotelID
    private EditText etHotelId;
    private ImageView qrButton;
    private final static int QR_CODE = 1001;

    private LinearLayout llProp;
    private CheckBox cbVibrate;
    private CheckBox cbDebugMode;
    private CheckBox cbEnableWifiCheck;
    private TextView btnCancel;
    private Button topRightBtn;
    private Button mPttSettingBtn;
    private Spinner mNotificationSpn;
    private ArrayList<String> mNotificationValues;
    private boolean mCheckFirstTime = false;
    LinearLayout hotelLayout;
    protected ConfigProgressTask task;

    protected SendEmailTask task1;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.config);

        ConfigContext ctx = ConfigContext.getInstance();
        ctx.populate(getCurrentContext());
        etMQTTUrl = (EditText) findViewById(R.id.et_mqtt_url);
        etMQTTUrl.setText(SmartPush.getConfigMQTTURLPrefs(this));
        topRightBtn = (Button) findViewById(R.id.btn_top_menu);
        topRightBtn.setClickable(false);

        mPttSettingBtn = (Button) findViewById(R.id.ptt_setting);
//        mPttSettingBtn.setOnClickListener(onPttSettingClickListener);
        mPttSettingBtn.setVisibility(McConstants.SHOW_PTT ? View.VISIBLE : View.GONE);
        setIcon(!McUtils.isNetworkOff(this));
        ((TextView) findViewById(R.id.tv_top_title)).setText(R.string.configuration);

        contextUrlEt = (EditText) findViewById(R.id.et_context_url);
        contextUrlEt.setText(ctx.getServiceUrl());

        llProp = (LinearLayout) findViewById(R.id.ll_prop);

        etPropId = (EditText) findViewById(R.id.et_prop_id);
        etPropId.setText(ctx.getPropertyId() > 0 ? "" + ctx.getPropertyId() : "");

        //add HotelID
        etHotelId = (EditText) findViewById(R.id.et_hotel_id);
        etHotelId.setText(ctx.gethotelId());

        qrButton = (ImageView) findViewById(R.id.qr_camera);
        qrButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConfigActivity.this, CaptureActivity.class);
                intent.putExtra("type", QRCodeParserFactory.CONFIG_URL_REQUEST);
                startActivityForResult(intent, QR_CODE);
            }
        });

        rgPushService = (RadioGroup) findViewById(R.id.rg_service_push);
        rgPushService.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.rb_gcm) {
                    etMQTTUrl.setVisibility(View.GONE);
                    rgPushService.check(R.id.rb_gcm);
                } else {
                    etMQTTUrl.setVisibility(View.VISIBLE);
                    rgPushService.check(R.id.rb_mqtt);
                }
            }
        });
        if (SmartPush.getConfigPushTypePrefs(this) == SmartPush.PUSH_TYPE_GCM) {
            rgPushService.check(R.id.rb_gcm);
            etMQTTUrl.setVisibility(View.GONE);
        } else {
            rgPushService.check(R.id.rb_mqtt);
            etMQTTUrl.setVisibility(View.VISIBLE);
        }
        hotelLayout = (LinearLayout) findViewById(R.id.ll_hotelID);
        rgServiceType = (RadioGroup) findViewById(R.id.rg_service_type);
        if (McConstants.COMPILATION_PRODUCT == McEnums.CompilationProduct.PERCIPIA) {
            ((RadioButton) rgServiceType.getChildAt(0)).setText(" ");
            ((RadioButton) rgServiceType.getChildAt(1)).setText("Percipia Server");
            rgPushService.setVisibility(View.GONE);
        }
        rgServiceType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rb_cconnect) {
                    llProp.setVisibility(View.VISIBLE);
                    PrefsUtil.setSettingServiceType(ConfigActivity.this, McEnums.ServiceType.CCONNECT);
                    hotelLayout.setVisibility(View.VISIBLE);
                    qrButton.setVisibility(View.VISIBLE);
                    useCconnect = true;
                } else {
                    llProp.setVisibility(View.VISIBLE);
                    PrefsUtil.setSettingServiceType(ConfigActivity.this, McEnums.ServiceType.ECONNECT);
                    hotelLayout.setVisibility(View.GONE);
                    qrButton.setVisibility(View.VISIBLE);
                    useCconnect = false;
                }
            }
        });

        if (PrefsUtil.getSettingServiceType(this) == McEnums.ServiceType.ECONNECT) {
            useCconnect = false;
            llProp.setVisibility(View.VISIBLE);
            rgServiceType.check(R.id.rb_econnect);
        } else {
            useCconnect = true;
            llProp.setVisibility(View.VISIBLE);
            rgServiceType.check(R.id.rb_cconnect);
        }

        btnSubmit = (TextView) findViewById(R.id.btn_send_msg);
        btnSubmit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (task == null || AsyncTask.Status.RUNNING.equals(task.getStatus()) == false) {
                    task = new ConfigProgressTask(getCurrentContext());
                    task.exec();
                }
            }
        });

        btnCancel = (TextView) findViewById(R.id.btn_cancel_msg);
        btnCancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });

        email = (Button) findViewById(R.id.email);
        email.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if (task1 == null || AsyncTask.Status.RUNNING.equals(task1.getStatus()) == false) {
                    task1 = new SendEmailTask(getCurrentContext());
                    task1.exec();
                }
            }
        });

        cbVibrate = (CheckBox) findViewById(R.id.vibrate);
        cbVibrate.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                if (isChecked) {
                    VibrateUtils.vibrate(getCurrentContext());
                }
            }
        });

        cbDebugMode = (CheckBox) findViewById(R.id.debug_mode);
        if (PrefsUtil.isDebugMode(this)) {
            cbDebugMode.setChecked(true);
        } else {
            cbDebugMode.setChecked(false);
        }

        String[] notification_array = getResources().getStringArray(R.array.notifycation_source);
        mNotificationValues = new ArrayList<String>(Arrays.asList(notification_array));

        mNotificationSpn = (Spinner) findViewById(R.id.notifySpn);
        ArrayAdapter<String> notifySpnAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, mNotificationValues);
        notifySpnAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        mNotificationSpn.setAdapter(notifySpnAdapter);
        int currentSound = PrefsUtil.getNotificationSound(this);
        mNotificationSpn.setSelection(currentSound);

        mNotificationSpn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//				String notifySound = mNotificationValues.get(position);
                PrefsUtil.setNotificationSound(ConfigActivity.this, position);
                if (mCheckFirstTime) {
                    if (position == 0) {
                        try {
                            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                            final MediaPlayer mpintro = MediaPlayer.create(ConfigActivity.this, notification);
                            mpintro.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                @Override
                                public void onCompletion(MediaPlayer mp) {
                                    mpintro.release();
                                }
                            });
                            mpintro.start();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Uri path = null;
                        if (position == McConstants.NOTIFICATION_SOUND_1)
                            path = Uri.parse("android.resource://" + getCurrentContext().getPackageName() + "/" + R.raw.notification1);
                        else if (position == McConstants.NOTIFICATION_SOUND_2)
                            path = Uri.parse("android.resource://" + getCurrentContext().getPackageName() + "/" + R.raw.notification2);
                        else if (position == McConstants.NOTIFICATION_SOUND_3)
                            path = Uri.parse("android.resource://" + getCurrentContext().getPackageName() + "/" + R.raw.notification3);
                        else if (position == McConstants.NOTIFICATION_SOUND_4)
                            path = Uri.parse("android.resource://" + getCurrentContext().getPackageName() + "/" + R.raw.notification4);

                        if (path != null) {
                            final MediaPlayer mpintro = MediaPlayer.create(ConfigActivity.this, path);
                            mpintro.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                @Override
                                public void onCompletion(MediaPlayer mp) {
                                    mpintro.release();
                                }
                            });
                            mpintro.start();
                        }
                    }
                }
                mCheckFirstTime = true;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        cbEnableWifiCheck = (CheckBox) findViewById(R.id.enable_sound_wifi);
        if (PrefsUtil.isEnableSoundWifiCheck(this)) {
            cbEnableWifiCheck.setChecked(true);
            mNotificationSpn.setEnabled(true);
        } else {
            cbEnableWifiCheck.setChecked(false);
            mNotificationSpn.setEnabled(false);
        }
        cbEnableWifiCheck.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                PrefsUtil.setEnableSoundWifiCheck(ConfigActivity.this, isChecked);
                mNotificationSpn.setEnabled(isChecked);
            }
        });

        initialDeviceInfo();

    }

//    OnClickListener onPttSettingClickListener = new OnClickListener() {
//        @Override
//        public void onClick(View v) {
//
//
////            Intent i = new Intent(ConfigActivity.this, SettingActivity.class);
////            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
////            startActivity(i);
//        }
//    };

    @Override
    protected void onResume() {
        super.onResume();
        CheckNetworkConnectivityReceiver.register(checking);
    }

    //add hotelId
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == QR_CODE) {
            QRCodeParserFactory.ConfigUrlQRCodeParser parser = new QRCodeParserFactory.ConfigUrlQRCodeParser();
            String qrcode = data.getStringExtra("result");
            if (parser.isCorrectFormat(qrcode)) {
                contextUrlEt.setText(parser.url);
            }
        }
    }

    private void setIcon(boolean hasConnection) {
        if (hasConnection) {
            topRightBtn.setBackgroundResource(R.drawable.icon_connected);
        } else {
            topRightBtn.setBackgroundResource(R.drawable.icon_disconnected);
        }
    }

    private CheckConnectivity checking = new CheckConnectivity() {
        @Override
        public void hasConenction(boolean hasConnection) {
            if (topRightBtn != null) {
                setIcon(hasConnection);
            }
        }
    };

    private void initialDeviceInfo() {
        TextView tv1 = (TextView) findViewById(R.id.osversionasw);
        String os = McUtils.getAndroidOSVersion();
        tv1.setText(os);

        TextView tv2 = (TextView) findViewById(R.id.rgaasw);
        String gcm = McUtils.getGoogleAccountUsername(getCurrentContext());
        tv2.setText(gcm);

        TextView tv3 = (TextView) findViewById(R.id.screenrasw);
        String res = "" + McUtils.getScreenResolutionHeight(this) + "x" + McUtils.getScreenResolutionWidth(this);
        tv3.setText(res);

        TextView tv4 = (TextView) findViewById(R.id.lastrasw);
        String user = SessionContext.getInstance().getUsername();
        tv4.setText(user == null ? "" : user);

//		String versionName = null;
//		try {
//			versionName = getPackageManager().getPackageInfo(getPackageName(), 0 ).versionName;
//		} catch (NameNotFoundException e) {
//		}

        TextView tv5 = (TextView) findViewById(R.id.verionasw);
//		tv5.setText("");
//		if (versionName != null) {
//			tv5.setText("V" + versionName);
//		}

        tv5.setText("V" + McConstants.APP_VERSION_NAME);
    }

    private Map<String, String> collectMconnectInfo(String wsurl, String wsversion) {

        Map<String, String> map = new TreeMap<String, String>();

        String os = McUtils.getAndroidOSVersion();
        String gcm = McUtils.getGoogleAccountUsername(getCurrentContext());
        String res = "" + McUtils.getScreenResolutionHeight(this) + "x" + McUtils.getScreenResolutionWidth(this);
        String user = SessionContext.getInstance().getUsername();
        String versionName = null;
//		try {
//			versionName = getPackageManager().getPackageInfo(getPackageName(), 0 ).versionName;
//			if (versionName != null) {
//				versionName = "V" + versionName;
//			}
//		} catch (NameNotFoundException e) {
//		}
        versionName = "V" + McConstants.APP_VERSION_NAME;
        map.put(getString(R.string.os_version), os);
        map.put(getString(R.string.google_account), gcm);
        map.put(getString(R.string.screen_resolution), res);
        map.put(getString(R.string.last_runner_login), user);
        map.put(getString(R.string.m_connect_version), versionName);
        map.put(getString(R.string.web_service_url), wsurl);
        map.put(getString(R.string.web_service_version), wsversion);

        return map;
    }

    private class ConfigProgressTask extends McProgressAsyncTask {

        private String lastServiceUrl;
        private boolean isValid = false;
        private String error = "";

        private ConfigProgressTask(Context context) {
            super(context);
        }

        @Override
        public void doInBackground() {

            if (McUtils.isNullOrEmpty(contextUrlEt.getText().toString())) {
                error = getString(R.string.msg_url_empty);
                contextUrlEt.requestFocus();
                isValid = false;
                return;
            }

            ConfigContext configCtx = ConfigContext.getInstance();
            lastServiceUrl = configCtx.getServiceUrl();

            configCtx.setServiceType(useCconnect ? McEnums.ServiceType.CCONNECT : McEnums.ServiceType.ECONNECT);
            String serviceUrl = contextUrlEt.getText().toString().trim();
            configCtx.setServiceUrl(serviceUrl);

            try {
                new URL(serviceUrl);
            } catch (MalformedURLException e) {
                error = getString(R.string.msg_url_format_wrong);
                isValid = false;
                return;
            }

            Long pid = -1L;
            String propId = etPropId.getText().toString();
            if ("".equals(propId.trim())) {
                error = getString(R.string.prop_id_required);
                isValid = false;
                return;
            }

            try {
                pid = Long.valueOf(propId.trim());
                if (pid <= 0) {
                    error = getString(R.string.prop_id_integer);
                    isValid = false;
                    return;
                }
            } catch (Exception e) {
                error = getString(R.string.prop_id_integer);
                isValid = false;
                return;
            }

            //add hotelId
            String hotelId = etHotelId.getText().toString();
            if (PrefsUtil.getSettingServiceType(McApplication.application) == McEnums.ServiceType.CCONNECT && "".equals(hotelId.trim())) {
                error = getString(R.string.hotel_id_required);
                isValid = false;
                return;
            }

            if (useCconnect) {
                // service url of c-Connect should be a directory
                if (serviceUrl.endsWith(".wsdl")) {
                    int in = serviceUrl.lastIndexOf("/");
                    serviceUrl = serviceUrl.substring(0, in);
                    in = serviceUrl.lastIndexOf("/");
                    serviceUrl = serviceUrl.substring(0, in);
                }

                if (serviceUrl.endsWith("/") == false) {
                    serviceUrl = serviceUrl + "/";
                }
                if (!SecurityService.getInstanceNonUI().connect(ConfigActivity.this, serviceUrl)) {
                    error = getString(R.string.url_not_be_connected);
                    isValid = false;
                    return;
                }
            } else {
//                if (serviceUrl.endsWith("asmx") == false) {
//                    error = getString(R.string.invalid_econnect_serviceurl);
//                    isValid = false;
//                    return;
//                }
                //if (!ECService.getInstance().connect(ConfigActivity.this, serviceUrl)) {
                if (ECService.getInstance().getWebserviceVersion(ConfigActivity.this).length() == 0) {
                    error = getString(R.string.url_not_be_connected);
                    isValid = false;
                    return;
                }
            }

            configCtx.setPropertyId(pid);
            //add hotelId
            configCtx.sethotelId(hotelId);
            configCtx.persist(ConfigActivity.this);

            PrefsUtil.setDebugMode(getCurrentContext(), cbDebugMode.isChecked());
            PrefsUtil.setEnableVibration(getCurrentContext(), cbVibrate.isChecked());


            getSessionCache().remove(LoadingActivity.PROPERTY_LANGUAGE);

            if (rgPushService.getCheckedRadioButtonId() == R.id.rb_gcm) {
                SmartPush.saveConfigPushPrefs(ConfigActivity.this, "", SmartPush.PUSH_TYPE_GCM);
            } else {
                SmartPush.saveConfigPushPrefs(ConfigActivity.this, etMQTTUrl.getText().toString(), SmartPush.PUSH_TYPE_MQTT);
            }

            isValid = true;
        }

        @Override
        public void onPostExecute() {
            if (isValid) {
                new LoadStatusColorTask(ConfigActivity.this).exec();
                new LoadLanguageTask(ConfigActivity.this).exec();
                final Intent i = new Intent(ConfigActivity.this, LoadingActivity.class);
                startActivity(i);
            } else {
                ConfigContext configCtx = ConfigContext.getInstance();
                configCtx.setServiceType(PrefsUtil.getSettingServiceType(ConfigActivity.this));
                configCtx.setServiceUrl(lastServiceUrl);
                configCtx.persist(ConfigActivity.this);
                toast(error);
            }
        }

    }

    @Override
    public boolean isLoginActivity() {
        return false;
    }

    @Override
    public boolean isHomeActivity() {
        return false;
    }

    private static File zip(File[] files, File output) {
        BufferedInputStream origin = null;
        ZipOutputStream out = null;
        try {
            out = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(output)));
            byte data[] = new byte[BUFFER_SIZE];
            for (int i = 0; i < files.length; i++) {
                FileInputStream fi = new FileInputStream(files[i]);
                origin = new BufferedInputStream(fi, BUFFER_SIZE);
                try {
                    String path = files[i].getAbsolutePath();
                    ZipEntry entry = new ZipEntry(path.substring(path.lastIndexOf("/") + 1));
                    out.putNextEntry(entry);
                    int count;
                    while ((count = origin.read(data, 0, BUFFER_SIZE)) != -1) {
                        out.write(data, 0, count);
                    }
                } finally {
                    origin.close();
                }
            }
            return output;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                }
            }
        }
    }

    private class SendEmailTask extends McProgressAsyncTask {

        private String error;
        private List<String> list;
        private File output;
        private String version;
        private String serviceUrl;

        public SendEmailTask(Context context) {
            super(context);
            this.error = null;
        }


        @Override
        public void doInBackground() {
            serviceUrl = contextUrlEt.getText().toString().trim();
            if (!ECService.getInstance().connect(ConfigActivity.this, serviceUrl)) {
                error = getString(R.string.url_not_be_connected);
                return;
            }
            list = ECService.getInstance().getSupportEmailList(getCurrentContext());
            version = ECService.getInstance().getWebserviceVersion(getCurrentContext());

            if (McUtils.isSDCardExist()) {
                File dir = new File(McConstants.LOG_OUTPUT_LOCATION);
                output = new File(dir.getParent(), "logfiles.zip");
                if (output.exists()) {
                    output.delete();
                }
                File file = null;
                for (File f : dir.listFiles()) {
                    if (file == null) {
                        file = f;
                    } else if (f.lastModified() > file.lastModified()) {
                        file = f;
                    }
                }
                if (file != null) {
                    output = zip(new File[]{file}, output);
                }
            }

        }

        @Override
        public void onPostExecute() {

            if (error != null) {
                toast(error);
                return;
            }

            final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND_MULTIPLE);
            emailIntent.setType("plain/text");

            ArrayList<Uri> uris = new ArrayList<Uri>();

            if (output != null) {
                String[] filePaths = new String[]{output.getAbsolutePath()};
                for (String file : filePaths) {
                    File fileIn = new File(file);
                    Uri u = Uri.fromFile(fileIn);
                    uris.add(u);
                }
            }

            if (list != null && list.size() > 0) {
                String[] arr = new String[list.size()];
                emailIntent.putExtra(Intent.EXTRA_EMAIL, (String[]) list.toArray(arr));
            }

            StringBuffer content = new StringBuffer();

            content.append("FCS Engineering Info\n");
            Map<String, String> info = collectMconnectInfo(serviceUrl, version);
            for (Entry<String, String> entry : info.entrySet()) {
                content.append(entry.getKey());
                content.append(" ");
                content.append(entry.getValue());
                content.append("\n");
            }

            content.append("\n");
            content.append("Device Info\n");
            info = McUtils.collectDeviceInfo(getCurrentContext());
            for (Entry<String, String> entry : info.entrySet()) {
                content.append(entry.getKey());
                content.append("=");
                content.append(entry.getValue());
                content.append("\n");
            }


            emailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.support_email_subject));
            emailIntent.putExtra(Intent.EXTRA_TEXT, content.toString());
            emailIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);

            startActivity(Intent.createChooser(emailIntent, getCurrentContext().getString(R.string.send_email)));


        }

    }

    private class LoadLanguageTask extends McProgressAsyncTask {

        public LoadLanguageTask(Context context) {
            super(context);
        }

        @Override
        public void doInBackground() {
            ECService.getInstance().getLanguageResourceFiles(getCurrentContext());
            McUtils.updateResourceStringMap(getCurrentContext());

            // Refresh app cache
            List<Language> languages = McUtils.getLanguageList(getCurrentContext());
            Locale curLocale = Locale.US;
            int curIndex = 0;
            Locale sessionLocale = PrefsUtil.getLocale();
            for (int i = 0; i < languages.size(); i++) {
                Language lan = languages.get(i);
                Locale locale = lan.getLocale();
                if (sessionLocale.equals(locale)) {
                    curLocale = locale;
                    curIndex = i;
                    break;
                }
            }
            String LANGUAGE_LOCALE = "language_locale";
            String LANGUAGE_INDEX = "language_index";
            String LANGUAGE_LIST = "language_list";
            getAppCache().put(LANGUAGE_INDEX, curIndex);
            getAppCache().put(LANGUAGE_LOCALE, curLocale);
            getAppCache().put(LANGUAGE_LIST, languages);
        }

        @Override
        public void onPostExecute() {

        }

    }

    private class LoadStatusColorTask extends McProgressAsyncTask {

        public LoadStatusColorTask(Context context) {
            super(context);
        }

        @Override
        public void doInBackground() {
            ECService.getInstance().getStatusColor(getCurrentContext());
        }

        @Override
        public void onPostExecute() {

        }

    }
}