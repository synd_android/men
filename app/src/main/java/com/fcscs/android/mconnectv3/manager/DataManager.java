package com.fcscs.android.mconnectv3.manager;

import java.util.List;

import android.content.Context;

import com.fcscs.android.mconnectv3.manager.model.Language;

public interface DataManager {

    List<Language> getLanguageList(Context context);

    boolean updateGCMToken(Context context, String token);

}
