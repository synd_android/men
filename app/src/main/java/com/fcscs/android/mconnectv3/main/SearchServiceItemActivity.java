package com.fcscs.android.mconnectv3.main;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.BaseActivity;
import com.fcscs.android.mconnectv3.common.ui.HomeTopBar;
import com.fcscs.android.mconnectv3.common.ui.McProgressAsyncTask;
import com.fcscs.android.mconnectv3.common.ui.RoundCornerListAdapter;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.manager.model.ServiceCodeModel;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.ServiceItemViewModel;
import com.fcscs.android.mconnectv3.ws.econnect.ECService;

public class SearchServiceItemActivity extends BaseActivity {

    private EditText searchField;
    private Spinner departmentSp;

    private ServiceCodeModel curServiceCode;
    private String serviceCode = null;

    private ListView listView;
    private String sTerm;
    private HomeTopBar headBar;
    private ArrayList<ServiceItemViewModel> aryList = null;
    private ItemListAdapter lvAdapter;
    private boolean startLoading;
    public List<ServiceCodeModel> serviceCodes;
    protected ItemAsyncTask task;
    private ServiceCodeAsync task1;

    private android.widget.Toast toast;
    private String strToastMsg = "";
    private int intPageSize = 10;
    private int intPageIndex = 0;
    private String strPassValuePageIndex;
    private boolean blnIsScrollAtTopMostPosition = false;
    //private boolean blnIsScrollUp = false;
    private boolean blnIsScrollDown = false;
    private boolean blnLoadingMore = false;
    private int intLastFirstVisibleItem;
    private int intCurrentVisibleItemCount;

    private int mLastFirstVisibleItem;
    private boolean blnIsScrollingUp;
    private List<ServiceItemViewModel> vListItems = null;

    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_service_item);

        //..Create By Chua...
        vListItems = new ArrayList<ServiceItemViewModel>();
        aryList = new ArrayList<ServiceItemViewModel>();

        headBar = (HomeTopBar) findViewById(R.id.top_bar);
        headBar.getTitleTv().setText(R.string.searchserviceitem);

        Bundle bundle = getIntent().getExtras();
        sTerm = bundle.getString("searchTerm");
        //..Created By : Chua Kam Hoong
        strPassValuePageIndex = bundle.getString("searchPageIndex");
        startLoading = true;

        searchField = (EditText) findViewById(R.id.searchFiled);
        searchField.setText(sTerm);
        searchField.setHint(R.string.service_item);
        searchField.setOnKeyListener(new OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:

                            if (task == null || AsyncTask.Status.RUNNING.equals(task.getStatus()) == false) {
                                //task = new ItemAsyncTask(getCurrentContext(), searchField.getText().toString(), serviceCode);
                                //..Reset to initial state...
                                intPageIndex = 1;
                                vListItems.clear();
                                String strTmpMsg = "Term : '" + searchField.getText().toString() + "' Service Code : '" + serviceCode + "'";
                                android.widget.Toast.makeText(getCurrentContext(), "" + strTmpMsg, android.widget.Toast.LENGTH_SHORT).show();
                                //task = new ItemAsyncTask(getCurrentContext(), vListItems, intPageSize, intPageIndex, searchField.getText().toString(), serviceCode);
                                task = new ItemAsyncTask(getCurrentContext(), vListItems, intPageIndex, searchField.getText().toString(), serviceCode);
                                task.exec();
                            }
                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        });

        departmentSp = (Spinner) findViewById(R.id.deptspinner);
        departmentSp.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                int index = pos - 1;
                if (serviceCodes != null && index > -1 && index < serviceCodes.size()) {
                    curServiceCode = serviceCodes.get(index);
                    serviceCode = curServiceCode.getCode();
                } else {
                    curServiceCode = null;
                    serviceCode = null;
                }
                if (!startLoading) {
                    if (task == null || AsyncTask.Status.RUNNING.equals(task.getStatus()) == false) {
                        intPageIndex = 1;
                        vListItems.clear();
                        if (curServiceCode != null) {
                            //String strTmpMsg = "Term : '" + searchField.getText().toString() + "' Service Code : '" + curServiceCode.getCode() + "'";
                            //android.widget.Toast.makeText(getCurrentContext(), "" + strTmpMsg, android.widget.Toast.LENGTH_SHORT).show();
                            //task = new ItemAsyncTask(getCurrentContext(), searchField.getText().toString(), curServiceCode.getCode());
                            //task = new ItemAsyncTask(getCurrentContext(), intPageSize, 0, searchField.getText().toString(), curServiceCode.getCode());
                            //task = new ItemAsyncTask(getCurrentContext(), vListItems, intPageSize, 1, searchField.getText().toString(), curServiceCode.getCode());
                            task = new ItemAsyncTask(getCurrentContext(), vListItems, intPageIndex, searchField.getText().toString(), curServiceCode.getCode());
                        } else {
                            //String strTmpMsg = "Term : '" + searchField.getText().toString() + "' Service Code : 'null'";
                            //android.widget.Toast.makeText(getCurrentContext(), "" + strTmpMsg, android.widget.Toast.LENGTH_SHORT).show();
                            //task = new ItemAsyncTask(getCurrentContext(), searchField.getText().toString(), null);
                            //task = new ItemAsyncTask(getCurrentContext(), intPageSize, 0, searchField.getText().toString(), null);
                            //task = new ItemAsyncTask(getCurrentContext(), vListItems, intPageSize, 1, searchField.getText().toString(), null);
                            task = new ItemAsyncTask(getCurrentContext(), vListItems, intPageIndex, searchField.getText().toString(), null);
                        }
                        task.exec();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });


        listView = (ListView) findViewById(R.id.listview);

        //..Get listview current position - used to maintain scroll position...
        int intCurrentPosition = listView.getFirstVisiblePosition();

        lvAdapter = new ItemListAdapter(getCurrentContext(), aryList, R.layout.search_serviceitem_list_item);
        listView.setAdapter(lvAdapter);
        listView.setSelectionFromTop(intCurrentPosition + 1, 0);

        //..Created By 	: Chua Kam Hoong
        //..Date 		: 20 May 2015
        listView.setOnScrollListener(new android.widget.AbsListView.OnScrollListener() {
            //@Override
            //public void onScrollStateChanged(android.widget.AbsListView view, int scrollState) {}

            @Override
            public void onScrollStateChanged(android.widget.AbsListView view, int scrollState) {
               /*
			   if(blnIsScrollDown == true){
				   blnIsScrollDown = false;
				   int intTotalCount = view.getCount()-1;
				   if(view.getLastVisiblePosition() == intTotalCount){
					    
					   	//strUrl = strUrlOrig;
						//common.JSonAsyncProfileList1 objRunAsync = new common.JSonAsyncProfileList1(mContex, listview, strUrl, intPageIndex);
						//objRunAsync.execute("Paging");
					   	if (!startLoading) {
							if (task == null || AsyncTask.Status.RUNNING.equals(task.getStatus()) == false) {
								
								intPageIndex = intPageIndex + 1;
							    strToastMsg = "Page Index :" + intPageIndex + " When scroll down.";
							    //toast = android.widget.Toast.makeText(getCurrentContext(), strToastMsg, android.widget.Toast.LENGTH_SHORT);
							   	//toast.show();
							    
								if (curServiceCode != null) {
									//task = new ItemAsyncTask(getCurrentContext(), searchField.getText().toString(), curServiceCode.getCode());
									//task = new ItemAsyncTask(getCurrentContext(), vListItems, intPageSize, intPageIndex, searchField.getText().toString(), curServiceCode.getCode());
									task = new ItemAsyncTask(getCurrentContext(), vListItems, intPageIndex, searchField.getText().toString(), curServiceCode.getCode());
								} else {
									//task = new ItemAsyncTask(getCurrentContext(), searchField.getText().toString(), null);
									//task = new ItemAsyncTask(getCurrentContext(), vListItems, intPageSize, intPageIndex, searchField.getText().toString(), null);
									task = new ItemAsyncTask(getCurrentContext(), vListItems, intPageIndex, searchField.getText().toString(), null);
								}
								task.exec();
							}
						}
				   }
			   }
			   */


                //--------------------------------------------------------------------------------------------------------------------------------------------------

                String strBreak = "";
                //..Detecting the scrolling direction in the adapter (up/down)
                //..URL : http://stackoverflow.com/questions/12114963/detecting-the-scrolling-direction-in-the-adapter-up-down
                if (view.getId() == listView.getId()) {
                    final int intTmpCurrentFirstVisibleItem = listView.getFirstVisiblePosition();

                    if (intTmpCurrentFirstVisibleItem > mLastFirstVisibleItem) {
                        blnIsScrollingUp = false;
                    } else if (intTmpCurrentFirstVisibleItem < mLastFirstVisibleItem) {
                        blnIsScrollingUp = true;
                    }

                    mLastFirstVisibleItem = intTmpCurrentFirstVisibleItem;

                    if (blnIsScrollingUp == false) {

                        //..Detected user scroll down...
                        int intTotalCount = view.getCount() - 1;

                        //if ((intCurrentVisibleItemCount > 0) && (scrollState == SCROLL_STATE_IDLE)) {
                        if ((view.getLastVisiblePosition() == intTotalCount) && (scrollState == SCROLL_STATE_IDLE)) {
                            //if ((intCurrentVisibleItemCount > 0) && (scrollState == SCROLL_STATE_IDLE)) {
                            //if ((view.getLastVisiblePosition() == (view.getCount()-1)) && (intCurrentVisibleItemCount > 0) && (scrollState == SCROLL_STATE_IDLE) && (!blnLoadingMore)) {
                            /*** In this way I detect if there's been a scroll which has completed ***/
                            /*** do the work! ***/
                            //strBreak = "03 Break here.";
                            if (!startLoading) {
                                if (task == null || AsyncTask.Status.RUNNING.equals(task.getStatus()) == false) {
                                    //strBreak = "02 Break here. Total Count : " + intTotalCount + " Scroll State : " + scrollState;
                                    //android.widget.Toast.makeText(getCurrentContext(), "" + strBreak, android.widget.Toast.LENGTH_SHORT).show();
                                    ////android.widget.Toast.makeText(getCurrentContext(), "Current Count : " + intCurrentVisibleItemCount + " State : " + scrollState + " Scrol Down : " + blnIsScrollDown, android.widget.Toast.LENGTH_SHORT).show();
                                    intPageIndex = intPageIndex + 1;

                                    if (curServiceCode != null) {
                                        //task = new ItemAsyncTask(getCurrentContext(), searchField.getText().toString(), curServiceCode.getCode());
                                        //task = new ItemAsyncTask(getCurrentContext(), vListItems, intPageSize, intPageIndex, searchField.getText().toString(), curServiceCode.getCode());
                                        task = new ItemAsyncTask(getCurrentContext(), vListItems, intPageIndex, searchField.getText().toString(), curServiceCode.getCode());
                                    } else {
                                        //task = new ItemAsyncTask(getCurrentContext(), searchField.getText().toString(), null);
                                        //task = new ItemAsyncTask(getCurrentContext(), vListItems, intPageSize, intPageIndex, searchField.getText().toString(), null);
                                        task = new ItemAsyncTask(getCurrentContext(), vListItems, intPageIndex, searchField.getText().toString(), null);
                                    }
                                    task.exec();
                                }
                            }
                        }

                    }

                }

            }

            @Override
            public void onScroll(android.widget.AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                intCurrentVisibleItemCount = visibleItemCount;
				/*
				if (listView.getLastVisiblePosition() == (totalItemCount - 1)) {

	               android.widget.Toast.makeText(getCurrentContext(), "Total Count : " + totalItemCount, android.widget.Toast.LENGTH_SHORT).show();

	                try {

	                } catch (Exception e) {
	                }

	            } */
			   /*
			   if(intLastFirstVisibleItem < firstVisibleItem)
	           {
				   //android.util.Log.i("SCROLLING DOWN","TRUE");
				   //toast = android.widget.Toast.makeText(mContex, "You are scrolling down", android.widget.Toast.LENGTH_SHORT);
			   	   //toast.show();
				   int intLastInScreen = firstVisibleItem + visibleItemCount;    
				   if((intLastInScreen == totalItemCount) && !(blnLoadingMore)){   
					    blnIsScrollDown = true;
					    //intPageIndex = intPageIndex + 1;
					    //android.widget.Toast.makeText(mContex, "Bottom has been reached, Go to Page Index : " + intPageIndex, android.widget.Toast.LENGTH_SHORT).show();
				   }
	           }
	           if( intLastFirstVisibleItem > firstVisibleItem)
	           {
	        	   //blnIsScrollUp = true;
	        	   //android.util.Log.i("SCROLLING UP","TRUE");
	        	   //toast = android.widget.Toast.makeText(mContex, "You are scrolling up", android.widget.Toast.LENGTH_SHORT);
			   	   //toast.show();
	        	   if (firstVisibleItem == 0) {
	        		   blnIsScrollAtTopMostPosition = true;
	               } 
	           }
	           intLastFirstVisibleItem = firstVisibleItem;
	           */
            }

        });


        if (task1 == null || AsyncTask.Status.RUNNING.equals(task1.getStatus()) == false) {
            task1 = new ServiceCodeAsync(getCurrentContext());
            task1.exec();
        }

        listView.requestFocus();
        McUtils.hideIME(this);

        if (task == null || AsyncTask.Status.RUNNING.equals(task.getStatus()) == false) {
            //..Create By Chua.
            //task = new ItemAsyncTask(this, sTerm, null);
            //task = new ItemAsyncTask(this, intPageSize, intPageIndex, sTerm, null);
            //task = new ItemAsyncTask(this, 0, 0, sTerm, null);
            //task = new ItemAsyncTask(this, vListItems, 0, 1, sTerm, null);
            //task = new ItemAsyncTask(this, vListItems, 1, sTerm, null);
            if (strPassValuePageIndex.length() > 0) {
                intPageIndex = Integer.parseInt(strPassValuePageIndex);
            }
            task = new ItemAsyncTask(this, vListItems, intPageIndex, sTerm, null);
            task.exec();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    class ItemAsyncTask extends McProgressAsyncTask {

        //private int intPageSize;
        private int intPageIndex;
        private String term;
        private String serviceCode;
        private List<ServiceItemViewModel> list;
        private List<ServiceItemViewModel> vListPar;

        //public ItemAsyncTask(Context context, List<ServiceItemViewModel> pList, int PageSize, int PageIndex, String term, String deptId) {
        public ItemAsyncTask(Context context, List<ServiceItemViewModel> pList, int PageIndex, String term, String deptId) {
            super(context);
            vListPar = pList;
            //intPageSize = PageSize;
            intPageIndex = PageIndex;
            this.term = term;
            this.serviceCode = deptId;
        }

        @Override
        public void doInBackground() {
            aryList.clear();
            //..Modify By 	: Chua Kam Hoong
            //..Date 		: 20 May 2015
            //list = ECService.getInstance().searchServiceItem(getCurrentContext(), term, serviceCode);
            //list = ECService.getInstance().searchServiceItem(getCurrentContext(), intPageSize, intPageIndex, term, serviceCode);
            //String strTmpPageIndex = "" + intPageIndex;
            ////list = ECService.getInstance().searchServiceItem(getCurrentContext(), vListPar, intPageSize, strTmpPageIndex, term, serviceCode);
            //list = ECService.getInstance().searchServiceItem(getCurrentContext(), vListPar, strTmpPageIndex, term, serviceCode);
            list = ECService.getInstance().searchServiceItem(getCurrentContext(), vListPar, intPageIndex, term, serviceCode);
        }

        @Override
        public void onPostExecute() {
            if (list == null || list.size() == 0) {
                toast(R.string.no_item_found);
            } else {
                aryList.addAll(list);
            }
            lvAdapter.notifyDataSetChanged();
            hideSoftInput(searchField);
            startLoading = false;
        }

    }

    class ItemListAdapter extends RoundCornerListAdapter<ServiceItemViewModel> {

        public ItemListAdapter(Context context, List<ServiceItemViewModel> list, int layout) {
            super(context, list, layout);
        }

        @Override
        public void onBindView(View view, Context context, int position, ServiceItemViewModel item) {
            final Tag tag = (Tag) view.getTag();
            final ServiceItemViewModel model = (ServiceItemViewModel) getItem(position);

            String name = "";
            if (isCConnect()) {
                name = model.getName() + "(" + model.getDepartmentName() + ", " + model.getCategory() + ")";
            } else {
                name = model.getName();
            }
            tag.name.setText(name);
            tag.code.setText(model.getCode());

            view.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent i = new Intent();
                    i.putExtra("ServiceItemViewModel", model);
                    setResult(RESULT_OK, i);
                    finish();
                }
            });
        }

        @Override
        public View newView(Context context, int position, ViewGroup parent) {
            View v = super.newView(context, position, parent);
            Tag tag = new Tag();

            tag.name = (TextView) v.findViewById(R.id.item_name);
            tag.code = (TextView) v.findViewById(R.id.item_code);

            v.setTag(tag);
            return v;
        }

        class Tag {
            TextView name;
            TextView code;
        }

    }

    class ServiceCodeAsync extends McProgressAsyncTask {

        private List<ServiceCodeModel> list;

        public ServiceCodeAsync(Context context) {
            super(context);
        }

        @Override
        public void doInBackground() {
            list = ECService.getInstance().getServiceCodes(getCurrentContext());
        }

        @Override
        public void onPostExecute() {

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getCurrentContext(), android.R.layout.simple_spinner_item);

            if (list == null || list.size() == 0) {
                adapter.add(getString(R.string.all_department));
            } else {
                serviceCodes = list;
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                adapter.add(getString(R.string.all_department));
                for (ServiceCodeModel m : list) {
                    adapter.add(m.getName());
                }
            }
            departmentSp.setAdapter(adapter);

        }

    }

    @Override
    public boolean isLoginActivity() {
        return true;
    }

    @Override
    public boolean isHomeActivity() {
        return false;
    }

}