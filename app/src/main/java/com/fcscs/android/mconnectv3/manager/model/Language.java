package com.fcscs.android.mconnectv3.manager.model;

import java.io.Serializable;
import java.util.Locale;

public class Language implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7036219820778194539L;
    private String name;
    private String code;
    private Locale locale;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    @Override
    public String toString() {
        return this.name;
    }

}
