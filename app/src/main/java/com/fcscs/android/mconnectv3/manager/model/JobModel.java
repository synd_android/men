package com.fcscs.android.mconnectv3.manager.model;

import java.io.Serializable;
import java.util.Date;

import com.fcscs.android.mconnectv3.common.McEnums.JobStatus;

public class JobModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String serviceItemName;
    private String roomNum;
    private String jobNum;
    private JobStatus status;
    private String priorityName;
    private Date deadline;
    private Date date;
    private String detail;
    private long jobId;
    private int quantity;
    private Boolean isAcked;
    private Boolean isRead;
    private String locationDesc = "";
    private String vip;
    private String reportedBy = "";
    private String guestName;
    private String requestorName;
    private String remarks;
    private int escalatedLevel = 0;
    private int ccjob = 0;
    private int notificationType = 0;
    private int notificationId = 0;
    private String notificationContent = "";
    private String equipmentName;
    private String equipmentNo;
    private String manHours;
    private String cost;
    private String assignTo;
    private int serviceTypeID;
    private String actionTaken;
    private int isInspected;
    private String serviceItemCode;
    private String locationCode;
    private int requiredPicture;
    private int requiredESignature;
    private String AcknowledgedBy;
    private String ClosedByName;

    public int getRequiredPicture() {
        return requiredPicture;
    }

    public void setRequiredPicture(int requiredPicture) {
        this.requiredPicture = requiredPicture;
    }

    public int getRequiredESignature() {
        return requiredESignature;
    }

    public void setRequiredESignature(int requiredESignature) {
        this.requiredESignature = requiredESignature;
    }

    public String getServiceItemCode() {
        return serviceItemCode;
    }

    public void setServiceItemCode(String serviceItemCode) {
        this.serviceItemCode = serviceItemCode;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public int getIsInspected() {
        return isInspected;
    }

    public void setIsInspected(int isInspected) {
        this.isInspected = isInspected;
    }

    public String getActionTaken() {
        return actionTaken;
    }

    public void setActionTaken(String actionTaken) {
        this.actionTaken = actionTaken;
    }

    public int getServiceTypeID() {
        return serviceTypeID;
    }

    public void setServiceTypeID(int serviceTypeID) {
        this.serviceTypeID = serviceTypeID;
    }

    public String getAssignTo() {
        return assignTo;
    }

    public void setAssignTo(String assignTo) {
        this.assignTo = assignTo;
    }

    public String getManHours() {
        return manHours;
    }

    public void setManHours(String manHours) {
        this.manHours = manHours;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    private Date requestedDate;
    private Date completedTime;

    public String getEquipmentName() {
        return equipmentName;
    }

    public void setEquipmentName(String equipmentName) {
        this.equipmentName = equipmentName;
    }

    public String getEquipmentNo() {
        return equipmentNo;
    }

    public void setEquipmentNo(String equipmentNo) {
        this.equipmentNo = equipmentNo;
    }

    public String getServiceItemName() {
        return serviceItemName;
    }

    public void setServiceItemName(String serviceItemName) {
        this.serviceItemName = serviceItemName;
    }

    public String getRoomNum() {
        return roomNum;
    }

    public void setRoomNum(String roomNum) {
        this.roomNum = roomNum;
    }

    public String getJobNum() {
        return jobNum;
    }

    public void setJobNum(String jobNum) {
        this.jobNum = jobNum;
    }

    public JobStatus getStatus() {
        return status;
    }

    public void setStatus(JobStatus status) {
        this.status = status;
    }

    public String getPriorityName() {
        return priorityName;
    }

    public void setPriorityName(String priorityName) {
        this.priorityName = priorityName;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public long getJobId() {
        return jobId;
    }

    public void setJobId(long jobId) {
        this.jobId = jobId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Boolean getIsRead() {
        return isRead;
    }

    public void setIsRead(Boolean isRead) {
        this.isRead = isRead;
    }

    public Boolean getIsAcked() {
        return isAcked;
    }

    public void setIsAcked(Boolean isAcked) {
        this.isAcked = isAcked;
    }

    public String getLocationDesc() {
        return locationDesc;
    }

    public void setLocationDesc(String locationDesc) {
        this.locationDesc = locationDesc;
    }

    public String getVip() {
        return vip;
    }

    public void setVip(String vip) {
        this.vip = vip;
    }

    public String getRequestorName() {
        return requestorName;
    }

    public void setRequestorName(String requestorName) {
        this.requestorName = requestorName;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getGuestName() {
        return guestName;
    }

    public void setGuestName(String guestName) {
        this.guestName = guestName;
    }

    public String getReportedBy() {
        return reportedBy;
    }

    public void setReportedBy(String reportedBy) {
        this.reportedBy = reportedBy;
    }

    public Date getRequestedDate() {
        return requestedDate;
    }

    public void setRequestedDate(Date requestedDate) {
        this.requestedDate = requestedDate;
    }

    public Date getCompletedTime() {
        return completedTime;
    }

    public void setCompletedTime(Date completedTime) {
        this.completedTime = completedTime;
    }

    public int getEscalatedLevel() {
        return escalatedLevel;
    }

    public void setEscalatedLevel(int escalatedLevel) {
        this.escalatedLevel = escalatedLevel;
    }

    public int getCcjob() {
        return ccjob;
    }

    public void setCcjob(int ccjob) {
        this.ccjob = ccjob;
    }

    public int getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(int notificationType) {
        this.notificationType = notificationType;
    }

    public int getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(int notificationId) {
        this.notificationId = notificationId;
    }

    public String getNotificationContent() {
        return notificationContent;
    }

    public void setNotificationContent(String notificationContent) {
        this.notificationContent = notificationContent;
    }

    public void setAcknowledgedBy(String acknowledgedBy) {
        AcknowledgedBy = acknowledgedBy;
    }

    public String getAcknowledgedBy() {
        return AcknowledgedBy;
    }

    public void setClosedByName(String closedByName) {
        ClosedByName = closedByName;
    }

    public String getClosedByName() {
        return ClosedByName;
    }
}
