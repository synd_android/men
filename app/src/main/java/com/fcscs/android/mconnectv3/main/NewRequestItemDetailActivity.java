package com.fcscs.android.mconnectv3.main;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.BaseActivity;
import com.fcscs.android.mconnectv3.common.ui.HomeTopBar;
import com.fcscs.android.mconnectv3.common.ui.ImagePreviewLayout;
import com.fcscs.android.mconnectv3.common.ui.RecordPlayLayout;
import com.fcscs.android.mconnectv3.common.util.DialogHelper;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.manager.model.ServiceRequest;
import com.fcscs.android.mconnectv3.manager.model.ServiceRequestItem;

public class NewRequestItemDetailActivity extends BaseActivity {


    private HomeTopBar homeTopBar;
    private TextView where;
    private TextView who;
    private TextView what;
    private TextView when;
    private TextView qty;
    private EditText remark;
    private RecordPlayLayout recordPlay;
    private int index;
    private ServiceRequest request;
    private ServiceRequestItem item;
    private ImagePreviewLayout imagePreview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.common_service_item_detail);

        homeTopBar = (HomeTopBar) findViewById(R.id.top_bar);
        homeTopBar.getTitleTv().setText(R.string.guestdetails);
        homeTopBar.getLeftBtn().setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });
        where = (TextView) findViewById(R.id.where);
        who = (TextView) findViewById(R.id.who);
        what = (TextView) findViewById(R.id.what);
        when = (TextView) findViewById(R.id.when);
        qty = (TextView) findViewById(R.id.qty);
        remark = (EditText) findViewById(R.id.remark);

        recordPlay = (RecordPlayLayout) findViewById(R.id.ll_record_play);
        imagePreview = (ImagePreviewLayout) findViewById(R.id.ll_image_preview);

        index = getIntent().getIntExtra("index", -1);
        request = ServiceRequest.getInstance();
        if (index < 0 || request.getItems() == null || index >= request.getItems().size()) {
            finish();
        } else {
            item = request.getItems().get(index);
            recordPlay.setRecordFile(item.getRecordedVoice());
            where.setText(request.getRoomNo());
            who.setText(request.getGuestName());
            what.setText(item.getName());
            qty.setText("" + item.getQuantity());
            when.setText(McUtils.formatDateTime(request.getScheduledDate()));
            remark.setText(item.getRemarks());
            remark.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    item.setRemarks(s.toString().trim());
                }
            });

            recordPlay.setAllowDelete(true);
            recordPlay.setListener(new RecordPlayLayout.OnClickRecordPlay() {
                @Override
                public void clickPlay(RecordPlayLayout play) {

                }

                @Override
                public void clickPause(RecordPlayLayout play) {

                }

                @Override
                public void clickDelete(RecordPlayLayout play) {
                    DialogHelper.showYesNoDialog(NewRequestItemDetailActivity.this, R.string.confirm, R.string.confirm_delete, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            recordPlay.stopAndHideLayout();
                            item.setRecordedVoice(null);
                        }
                    });
                }
            });
            imagePreview.setImageFile(item.getCapturedImage());
            imagePreview.setAllowDelete(true);
            imagePreview.setOnClickDelete(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogHelper.showYesNoDialog(NewRequestItemDetailActivity.this, R.string.confirm, R.string.confirm_delete, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            item.setCapturedImage(null);
                            imagePreview.setImageFile(null);
                        }
                    });
                }
            });
        }

    }


    @Override
    public boolean isLoginActivity() {
        return true;
    }

    @Override
    public boolean isHomeActivity() {
        return false;
    }

}