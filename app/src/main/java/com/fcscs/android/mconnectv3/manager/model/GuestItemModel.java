package com.fcscs.android.mconnectv3.manager.model;

import java.io.Serializable;

public class GuestItemModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7769514172353102589L;

    private long serviceItemId;
    private long serviceItemCategoryId;
    private int quantity;
    private long deliverToLocationId;
    private long runnerId;
    private String remarks;


    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public GuestItemModel() {
    }

    public long getRunnerId() {
        return runnerId;
    }

    public void setRunnerId(long runnerId) {
        this.runnerId = runnerId;
    }

    public long getServiceItemId() {
        return serviceItemId;
    }

    public void setServiceItemId(long serviceItemId) {
        this.serviceItemId = serviceItemId;
    }

    public long getServiceItemCategoryId() {
        return serviceItemCategoryId;
    }

    public void setServiceItemCategoryId(long serviceItemCategoryId) {
        this.serviceItemCategoryId = serviceItemCategoryId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public long getDeliverToLocationId() {
        return deliverToLocationId;
    }

    public void setDeliverToLocationId(long deliverToLocationId) {
        this.deliverToLocationId = deliverToLocationId;
    }

}
