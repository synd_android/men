package com.fcscs.android.mconnectv3.main;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.BaseActivity;
import com.fcscs.android.mconnectv3.common.McEnums.FavoritesEnum;
import com.fcscs.android.mconnectv3.common.ui.HomeTopBar;
import com.fcscs.android.mconnectv3.common.ui.McProgressAsyncTask;
import com.fcscs.android.mconnectv3.common.ui.RoundCornerListAdapter;
import com.fcscs.android.mconnectv3.manager.model.FavouriteLink;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.GetFavouriteLinkResponse;
import com.fcscs.android.mconnectv3.ws.econnect.ECService;

public class FavoritesActivity extends BaseActivity {
    private HomeTopBar homeTopBar;
    private Spinner typeSp;
    private FavoritesEnum currFilterType;
    private GetFavouriteLinkResponse response;
    private ListView favoritLinkLv;
    private FavoriteLinksItemListAdapter adapter;
    private List<FavouriteLink> links = new ArrayList<FavouriteLink>();
    protected FavoriteLoader task;

    //	[6:05:34 PM] Christopher Felix: 0 = internal, 1 = external, 2 = guest
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.favorites);


        homeTopBar = (HomeTopBar) findViewById(R.id.top_bar);
        homeTopBar.getTitleTv().setText(R.string.favorites);

        typeSp = (Spinner) findViewById(R.id.favorites_favoritesType);
        final ArrayAdapter<String> typeAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item);
        typeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        typeAdapter.add(getString(R.string.favorite_type_internal));
        typeAdapter.add(getString(R.string.favorite_type_external));
        typeAdapter.add(getString(R.string.favorite_type_guest_stuff));

//		for (McEnums.FavoritesEnum type : McEnums.FavoritesEnum.values()) {
//			typeAdapter.add(type.geteConnectName());
//		}

        typeSp.setAdapter(typeAdapter);
        typeSp.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                if (pos == 0) {
                    currFilterType = FavoritesEnum.INTERNAL;
                } else if (pos == 1) {
                    currFilterType = FavoritesEnum.EXTERNAL;
                } else if (pos == 2) {
                    currFilterType = FavoritesEnum.GUEST_STUFF;
                }

                if (task == null || AsyncTask.Status.RUNNING.equals(task.getStatus()) == false) {
                    task = new FavoriteLoader(getCurrentContext(), currFilterType);
                    task.exec();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });

        favoritLinkLv = (ListView) findViewById(R.id.favorites_listview);
        favoritLinkLv.setSelector(R.drawable.spacebar);
        favoritLinkLv.setClickable(true);
        favoritLinkLv.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                FavouriteLink linkItem = links.get(pos);
                Uri uri = Uri.parse(linkItem.getUrl());
                Intent it = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(it);
            }
        });
        adapter = new FavoriteLinksItemListAdapter(FavoritesActivity.this, links, R.layout.jobstatus_list_item);
        favoritLinkLv.setAdapter(adapter);

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            setResult(RESULT_OK);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    private class FavoriteLinksItemListAdapter extends RoundCornerListAdapter<FavouriteLink> {

        public FavoriteLinksItemListAdapter(Context context, List<FavouriteLink> list, int layout) {
            super(context, list, layout);
        }

        @Override
        public void beforeBindView(View view, Context context, int position, FavouriteLink item) {
            Tag tag = new Tag();
            tag.title = (TextView) view.findViewById(R.id.jobstatus);
            view.setTag(tag);
        }

        @Override
        public void onBindView(View view, Context context, int position, FavouriteLink item) {
            final Tag tag = (Tag) view.getTag();
            tag.title.setText(item.getTitle());
        }

        class Tag {
            TextView title;
        }

    }

    private class FavoriteLoader extends McProgressAsyncTask {

        private FavoritesEnum type;

        private FavoriteLoader(Context context, FavoritesEnum type) {
            super(context);
            this.type = type;
        }

        @Override
        public void doInBackground() {
            response = ECService.getInstance().getFavouriteLink(FavoritesActivity.this, type);
            links = response.getFavouriteLinks();

        }

        @Override
        public void onPostExecute() {
            adapter = new FavoriteLinksItemListAdapter(FavoritesActivity.this, links, R.layout.jobstatus_list_item);
            favoritLinkLv.setAdapter(adapter);
        }

    }

    @Override
    public boolean isLoginActivity() {
        return true;
    }

    @Override
    public boolean isHomeActivity() {
        return false;
    }

}
