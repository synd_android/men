package com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.ksoap2.serialization.SoapObject;

import com.fcscs.android.mconnectv3.dao.entity.Location;


public class GetLocationsResponse implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1873098794125529586L;
    private int total = 0;


    private List<Location> locationList;

    public GetLocationsResponse() {
    }

    public GetLocationsResponse(SoapObject soap) {
        locationList = new ArrayList<Location>();
        this.total = Integer.valueOf(soap.getAttribute("total").toString());
        for (int i = 0; i < soap.getPropertyCount(); i++) {
            locationList.add(new Location((SoapObject) soap.getProperty(i)));
        }
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<Location> getLocationList() {
        return locationList;
    }

    public void setLocationList(List<Location> locationList) {
        this.locationList = locationList;
    }


}
