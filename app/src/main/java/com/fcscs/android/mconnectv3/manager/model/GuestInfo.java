package com.fcscs.android.mconnectv3.manager.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.ksoap2.serialization.SoapObject;

import android.content.Context;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.util.DateTimeHelper;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.common.util.SoapHelper;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.ProfileNote;

public class GuestInfo implements Serializable {

    private String roomNum = "";
    private Boolean isvip;
    private String vipMsg = "";
    private String language = "";
    private String guestUsername = "";
    private String checkinDate = "";
    private String checkoutDate = "";
    private String etaDate = "";
    private String etdDate = "";
    private Long roomLinkId = 0L;
    private Long locationId = 0L;
    private Long pmsGuestId = 0L;
    private Long extNo = 0L;
    private String comments = "";


    private String dnd = "";
    private String textMsg = "";
    private String voiceMsg = "";
    private String wakeupCall = "";

    private boolean dndOn;
    private boolean voiceOn;
    private boolean textOn;
    private boolean wakeupCallOn;

    //eCN getGuestListing API
    private String profileID = "";
    private String firstName = "";
    private String lastName = "";
    private String membershipNo = "";
    private String membershipType = "";
    private String birthdate = "";
    private String company = "";
    private String blackList = "";    //1=blacklist
    private String guestType = "";
    private String vipCode = "";
    private String awu = "";
    private String preference = "";
    private List<ProfileNote> noteListing = new ArrayList<ProfileNote>();
    ;

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public GuestInfo() {
    }

    public GuestInfo(SoapObject soap) {

        roomLinkId = SoapHelper.getLongAttribute(soap, "guestRoomLinkId", 0L);
        roomNum = SoapHelper.getStringAttribute(soap, "roomNo", "");
        isvip = SoapHelper.getBooleanAttribute(soap, "vip", false);
        locationId = SoapHelper.getLongAttribute(soap, "locationId", 0L);
        pmsGuestId = SoapHelper.getLongAttribute(soap, "pmsGuestId", 0L);
        checkinDate = SoapHelper.getStringAttribute(soap, "checkinDate", "");
        guestUsername = SoapHelper.getStringProperty(soap, "guestName", "");
        extNo = SoapHelper.getLongProperty(soap, "extNo", 0L);
        comments = SoapHelper.getStringProperty(soap, "comments", "");
        dndOn = SoapHelper.getBooleanProperty(soap, "dndOn", false);
        voiceOn = SoapHelper.getBooleanProperty(soap, "voiceOn", false);
        textOn = SoapHelper.getBooleanProperty(soap, "textOn", false);
    }

    public GuestInfo(Context context, SoapObject soap, boolean getGuestListing) {
        profileID = SoapHelper.getStringProperty(soap, "ProfileID", "");
        firstName = SoapHelper.getStringProperty(soap, "FirstName", "");
        lastName = SoapHelper.getStringProperty(soap, "Lastname", "");
        membershipNo = SoapHelper.getStringProperty(soap, "MembershipNo", "");
        membershipType = SoapHelper.getStringProperty(soap, "MembershipType", "");
        birthdate = SoapHelper.getStringProperty(soap, "Birthdate", "");
        company = SoapHelper.getStringProperty(soap, "Company", "");
        blackList = SoapHelper.getStringProperty(soap, "BlackList", "");
        guestType = SoapHelper.getStringProperty(soap, "Type", "");


        checkinDate = SoapHelper.getStringProperty(soap, "CheckIn", "");
        checkoutDate = SoapHelper.getStringProperty(soap, "CheckOut", "");

        Date CiDate = SoapHelper.getDateProperty(soap, "CheckIn", DateTimeHelper.DEFAULT_FORMATTER_DATETIME_24H_T, null);
        checkinDate = McUtils.formatDate(CiDate);

        Date CoDate = SoapHelper.getDateProperty(soap, "CheckOut", DateTimeHelper.DEFAULT_FORMATTER_DATETIME_24H_T, null);
        checkoutDate = McUtils.formatDate(CoDate);

        etaDate = SoapHelper.getStringProperty(soap, "ETA", "");
        etdDate = SoapHelper.getStringProperty(soap, "ETD", "");

        Date myEtaDate = SoapHelper.getDateProperty(soap, "ETA", DateTimeHelper.DEFAULT_FORMATTER_DATETIME_24H_T, null);
        etaDate = McUtils.formatDate(myEtaDate);
        Date myEtdDate = SoapHelper.getDateProperty(soap, "ETD", DateTimeHelper.DEFAULT_FORMATTER_DATETIME_24H_T, null);
        etdDate = McUtils.formatDate(myEtdDate);

        roomNum = SoapHelper.getStringProperty(soap, "RoomNo", "");
        vipMsg = SoapHelper.getStringProperty(soap, "VipCode", "");
        language = SoapHelper.getStringProperty(soap, "Language", "");

        String yes = context.getString(R.string.yes);
        String no = context.getString(R.string.no);

        String var = SoapHelper.getStringProperty(soap, "TextMsg", "");
        textMsg = "1".equalsIgnoreCase(var) ? yes : no;        //1: YES 0:NO
        var = SoapHelper.getStringProperty(soap, "DND", "");
        dnd = "1".equalsIgnoreCase(var) ? yes : no;        //1: YES 0:NO
        var = SoapHelper.getStringProperty(soap, "VoiceMsg", "");
        voiceMsg = "1".equalsIgnoreCase(var) ? yes : no;        //1: YES 0:NO

        var = SoapHelper.getStringProperty(soap, "AWU", "");
        awu = "1".equalsIgnoreCase(var) ? yes : no;        //1: YES 0:NO

        preference = SoapHelper.getStringProperty(soap, "PreferenceCodes", "");

        guestUsername = firstName + " " + lastName;
        SoapObject obj = (SoapObject) soap.getProperty("NoteListing");
        if (obj != null) {
            noteListing = new ArrayList<ProfileNote>();
            for (int i = 0; i < obj.getPropertyCount(); i++) {
                if ("NoteDetails".equalsIgnoreCase(SoapHelper.getPropertyName(obj, i))) {
                    SoapObject note = (SoapObject) obj.getProperty(i);
                    String typeCode = SoapHelper.getStringProperty(note, "TypeCode", "").trim();
                    String typeDesc = SoapHelper.getStringProperty(note, "TypeDescription", "").trim();
                    String noteDesc = SoapHelper.getStringProperty(note, "NoteDescription", "").trim();
                    if (!"".equalsIgnoreCase(typeCode) && !"".equalsIgnoreCase(typeDesc) && !"".equalsIgnoreCase(noteDesc)) {
                        ProfileNote n = new ProfileNote();
                        n.setTypeCode(typeCode);
                        n.setTypeDesc(typeDesc);
                        n.setNoteDesc(noteDesc);
                        noteListing.add(n);
                    }
                }
            }
        }

    }

    public String getVipMsg() {
        return vipMsg;
    }

    public void setVipMsg(String vipMsg) {
        this.vipMsg = vipMsg;
    }

    public String getDnd() {
        return dnd;
    }

    public void setDnd(String dnd) {
        this.dnd = dnd;
    }

    public String getTextMsg() {
        return textMsg;
    }

    public void setTextMsg(String textMsg) {
        this.textMsg = textMsg;
    }

    public String getVoiceMsg() {
        return voiceMsg;
    }

    public void setVoiceMsg(String voiceMsg) {
        this.voiceMsg = voiceMsg;
    }

    public Long getPmsGuestId() {
        return pmsGuestId;
    }

    public void setPmsGuestId(Long pmsGuestId) {
        this.pmsGuestId = pmsGuestId;
    }

    public Long getExtNo() {
        return extNo;
    }

    public void setExtNo(Long extNo) {
        this.extNo = extNo;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Long getLocationId() {
        return locationId;
    }

    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    public Long getRoomLinkId() {
        return roomLinkId;
    }

    public void setRoomLinkId(Long roomLinkId) {
        this.roomLinkId = roomLinkId;
    }

    public String getRoomNum() {
        return roomNum;
    }

    public void setRoomNum(String folio) {
        this.roomNum = folio;
    }

    public Boolean getIsvip() {
        return isvip;
    }

    public void setIsvip(Boolean isvip) {
        this.isvip = isvip;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getGuestUsername() {
        return guestUsername;
    }

    public void setGuestUsername(String guestUsername) {
        this.guestUsername = guestUsername;
    }

    public String getCheckinDate() {
        return checkinDate;
    }

    public void setCheckinDate(String checkinDate) {
        this.checkinDate = checkinDate;
    }

    public String getCheckoutDate() {
        return checkoutDate;
    }

    public void setCheckoutDate(String checkoutDate) {
        this.checkoutDate = checkoutDate;
    }

    public String getETADate() {
        return etaDate;
    }

    public void setETADate(String etaDate) {
        this.etaDate = etaDate;
    }

    public String getETDDate() {
        return etdDate;
    }

    public void setETDDate(String etdDate) {
        this.etdDate = etdDate;
    }

    public boolean isDndOn() {
        return dndOn;
    }

    public void setDndOn(boolean dndOn) {
        this.dndOn = dndOn;
    }

    public boolean isVoiceOn() {
        return voiceOn;
    }

    public void setVoiceOn(boolean voiceOn) {
        this.voiceOn = voiceOn;
    }

    public boolean isTextOn() {
        return textOn;
    }

    public void setTextOn(boolean textOn) {
        this.textOn = textOn;
    }

    public boolean isWakeupCallOn() {
        return wakeupCallOn;
    }

    public void setWakeupCallOn(boolean callOn) {
        this.wakeupCallOn = callOn;
    }

    public String getWakeupCall() {
        return wakeupCall;
    }

    public void setWakeupCall(String wakeupCall) {
        this.wakeupCall = wakeupCall;
    }

    public String getProfileID() {
        return profileID;
    }

    public void setProfileID(String profileID) {
        this.profileID = profileID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMembershipNo() {
        return membershipNo;
    }

    public void setMembershipNo(String membershipNo) {
        this.membershipNo = membershipNo;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getBlackList() {
        return blackList;
    }

    public void setBlackList(String blackList) {
        this.blackList = blackList;
    }

    public String getGuestType() {
        return guestType;
    }

    public void setGuestType(String guestType) {
        this.guestType = guestType;
    }

    public String getVipCode() {
        return vipCode;
    }

    public void setVipCode(String vipCode) {
        this.vipCode = vipCode;
    }

    public String getAwu() {
        return awu;
    }

    public void setAwu(String awu) {
        this.awu = awu;
    }

    public List<ProfileNote> getNoteListing() {
        return noteListing;
    }

    public void setNoteListing(List<ProfileNote> noteListing) {
        this.noteListing = noteListing;
    }

    public String getNotesListString() {
        String ret = "";
        if (comments != null) {
            ret = comments;
        }
        if (noteListing != null && noteListing.size() > 0) {
            for (ProfileNote s : noteListing) {
                String typeCode = s.getTypeCode();
                String typeDesc = s.getTypeDesc();
                String noteDesc = s.getNoteDesc();
                ret = ret + "Type Code: " + typeCode + " Type Description: " + typeDesc + "\nNote Description: " + noteDesc + "\n";
                //ret = ret + "\n" + s;
            }
        }
        return ret.trim();
    }

    public String getPreference() {
        return preference;
    }

    public void setPreference(String preference) {
        this.preference = preference;
    }

    public String getMembershipType() {
        return membershipType;
    }

    public void setMembershipType(String membershipType) {
        this.membershipType = membershipType;
    }
}
