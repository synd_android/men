package com.fcscs.android.mconnectv3.ws.cconnect;

import java.util.Date;

import org.ksoap2.serialization.SoapObject;

import android.content.Context;

import com.fcscs.android.mconnectv3.common.util.DateTimeHelper;
import com.fcscs.android.mconnectv3.http.WebServiceBase;
import com.fcscs.android.mconnectv3.ws.cconnect.model.securityservice.LoginResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.securityservice.LogoutResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.securityservice.UpdateSessionResponse;

public class SecurityService extends WebServiceBase {

    private static final String CONNECT_RESPONSE = "ConnectResponse";
    private static final String CONNECT_REQUEST = "ConnectRequest";

    private static final String WEBSERVICE_URL_SUFFIX = "ws/securityService";
    private static final String NAMESPACE = "http://fcscs.com/ws/schemas/securityservice";
    private static final String LOGIN_REQ_NAME = "LoginRequest";
    private static final String LOGIN_RESP_NAME = "LoginResponse";
    private static final String LOGOUT_REQ_NAME = "LogoutRequest";
    private static final String LOGOUT_RESP_NAME = "LogoutResponse";
    private static final String UPDATE_SESSION_REQ_NAME = "UpdateSessionRequest";
    private static final String UPDATE_SESSION_RESP_NAME = "UpdateSessionResponse";

    private static SecurityService instance;
    private static SecurityService instanceNonUI;

    private SecurityService() {
    }

    public static SecurityService getInstance() {
        if (instance == null) {
            instance = new SecurityService();
        }
        instance.setEnableUI(true);
        return instance;
    }

    public static SecurityService getInstanceNonUI() {
        if (instanceNonUI == null) {
            instanceNonUI = new SecurityService();
        }
        instanceNonUI.setEnableUI(false);
        return instanceNonUI;
    }

    public LoginResponse login(Context ctx, String username, String password, String ip, String deviceId, long propId) {

        SoapObject request = new SoapObject(NAMESPACE, LOGIN_REQ_NAME);
        request.addAttribute("username", username);
        request.addAttribute("password", password);
        request.addAttribute("propId", propId);
        if (ip != null && !"".equals(ip)) {
            request.addProperty("ip", ip);
            if (deviceId != null && !"".equals(deviceId)) {
                request.addProperty("deviceId", deviceId);
            }
            request.addProperty("deviceType", "ANDROID");
//			request.addProperty("source", "MCONNECT");
//			request.addProperty("languageCode", "");
        }

        SoapObject soap = callWebService(ctx, request, WEBSERVICE_URL_SUFFIX, NAMESPACE, LOGIN_RESP_NAME);

        if (soap != null) {
            return new LoginResponse(soap);
        } else {
            return null;
        }
    }

    public LogoutResponse logout(Context ctx, long fcsUserId, String wsSessionId) {

        SoapObject request = new SoapObject(NAMESPACE, LOGOUT_REQ_NAME);
        request.addAttribute("fcsUserId", fcsUserId);
        request.addAttribute("wsSessionId", wsSessionId);

        SoapObject soap = callWebService(ctx, request, WEBSERVICE_URL_SUFFIX, NAMESPACE, LOGOUT_RESP_NAME);

        if (soap != null) {
            return new LogoutResponse(soap);
        } else {
            return null;
        }

    }

    public UpdateSessionResponse updateSession(Context ctx, long fcsUserId, String wsSessionId, String ip, String c2dmId) {

        SoapObject request = new SoapObject(NAMESPACE, UPDATE_SESSION_REQ_NAME);
        request.addAttribute("fcsUserId", fcsUserId);
        request.addAttribute("wsSessionId", wsSessionId);
        if (ip != null) {
            request.addProperty("ip", ip.trim());
        }
        if (c2dmId != null) {
            request.addProperty("c2dmId", c2dmId.trim());
        }

        SoapObject soap = callWebService(ctx, request, WEBSERVICE_URL_SUFFIX, NAMESPACE, UPDATE_SESSION_RESP_NAME);

        if (soap != null) {
            return new UpdateSessionResponse(soap);
        } else {
            return null;
        }

    }

    public boolean connect(Context ctx, String serviceUrl) {

        SoapObject request = new SoapObject(NAMESPACE, CONNECT_REQUEST);
        request.addAttribute("timestamp", DateTimeHelper.getDateByTimeZone(new Date()).getTime());

        SoapObject soap = callWebService(request, serviceUrl, WEBSERVICE_URL_SUFFIX, NAMESPACE, CONNECT_RESPONSE);

        if (soap != null) {
            return Boolean.valueOf(soap.getAttribute("result").toString());
        } else {
            return false;
        }

    }


}
