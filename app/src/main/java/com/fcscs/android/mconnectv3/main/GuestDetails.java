package com.fcscs.android.mconnectv3.main;

import android.os.Bundle;
import android.view.View;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.manager.model.GuestInfo;

public class GuestDetails extends GuestDetailCommonActivity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getIntent().getExtras();
        View view = findViewById(R.id.ll_search);
        view.setVisibility(View.GONE);
        if (bundle.get("guest") != null) {
            GuestInfo guest = (GuestInfo) bundle.get("guest");
            populatityUserInfo(guest);
        } else {
            finish();
        }
    }
}
