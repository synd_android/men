package com.fcscs.android.mconnectv3.common.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

public class LogoutTopBar extends TopBar {

    public LogoutTopBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            leftBtn.setVisibility(View.INVISIBLE);
        }

    }

    public void onClickLeftBtn(View view) {
    }

}
