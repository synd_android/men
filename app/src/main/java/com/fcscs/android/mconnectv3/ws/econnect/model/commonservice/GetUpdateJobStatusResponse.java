package com.fcscs.android.mconnectv3.ws.econnect.model.commonservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.ksoap2.serialization.SoapObject;

import com.fcscs.android.mconnectv3.common.util.SoapHelper;

public class GetUpdateJobStatusResponse implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8196112344337633758L;

    private int returnCode;
    private String errorMsg;
    private List<UpdateableJobModel> list;

    public GetUpdateJobStatusResponse() {

    }

    public GetUpdateJobStatusResponse(SoapObject soap) {
        SoapObject status = (SoapObject) soap.getProperty("ResponseStatus");
        returnCode = SoapHelper.getIntegerProperty(status, "ReturnCode", 1);
        errorMsg = SoapHelper.getStringProperty(status, "ErrorMsg", "");

        list = new ArrayList<UpdateableJobModel>();
        //SoapObject listing = (SoapObject)soap.getProperty("NotificationsListing");
        for (int i = 0; i < soap.getPropertyCount(); i++) {
            if ("UpdateJobStatusListing".equalsIgnoreCase(SoapHelper.getPropertyName(soap, i))) {
                SoapObject s = (SoapObject) soap.getProperty(i);
                for (int j = 0; j < s.getPropertyCount(); j++) {
                    if ("UpdateJobStatusDetails".equalsIgnoreCase(SoapHelper.getPropertyName(s, j))) {
                        SoapObject l = (SoapObject) s.getProperty(j);
                        String JobNo = SoapHelper.getStringProperty(l, "JobNo", "");
                        int type = SoapHelper.getIntegerProperty(l, "Updateable", UpdateableJobModel.IS_NOT_RUNNER_JOB);

                        UpdateableJobModel m = new UpdateableJobModel();
                        m.setJobNumber(JobNo);
                        m.setUpdateable(type);
                        list.add(m);
                    }
                }
            }
        }
    }

    public int getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(int returnCode) {
        this.returnCode = returnCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public List<UpdateableJobModel> getList() {
        return list;
    }

    public void setList(List<UpdateableJobModel> list) {
        this.list = list;
    }


}
