package com.fcscs.android.mconnectv3.main;

import android.content.Context;
import android.support.v4.view.GestureDetectorCompat;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.Button;

/**
 * Created by NguyenMinhTuan on 1/11/16.
 */
public class VoiceGestureButton extends Button implements GestureDetector.OnDoubleTapListener, GestureDetector.OnGestureListener {
    private static final String TAG = "GestureButton";
    private GestureDetectorCompat mDetector;
    private OnDoubleTapListener mDoubleTab;

    public VoiceGestureButton(Context context) {
        super(context);
        init(context);
    }

    public VoiceGestureButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public VoiceGestureButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        mDetector = new GestureDetectorCompat(context, this);
        mDetector.setOnDoubleTapListener(this);
    }

    public void setDoubleTabListener(OnDoubleTapListener listener) {
        mDoubleTab = listener;
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onDoubleTap(MotionEvent e) {

        if (mDoubleTab != null) {
            mDoubleTab.onDouleTap();
        }
        return false;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        this.mDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        return false;
    }

    public interface OnDoubleTapListener {
        void onDouleTap();
    }
}
