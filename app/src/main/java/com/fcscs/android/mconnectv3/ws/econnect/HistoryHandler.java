package com.fcscs.android.mconnectv3.ws.econnect;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.fcscs.android.mconnectv3.manager.model.GuestHistoryConfig;

public class HistoryHandler extends DefaultHandler {

    private boolean currentElement;
    private String currentValue;
    private GuestHistoryConfig history;

    public HistoryHandler() {
        this.setHistory(new GuestHistoryConfig());
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        super.startElement(uri, localName, qName, attributes);
        currentElement = true;
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        super.endElement(uri, localName, qName);
        currentElement = false;
        if ("D1".equalsIgnoreCase(localName)) {
            getHistory().setDay1("YES".equalsIgnoreCase(currentValue));
        } else if ("W1".equalsIgnoreCase(localName)) {
            getHistory().setWeek1("YES".equalsIgnoreCase(currentValue));
        } else if ("M1".equalsIgnoreCase(localName)) {
            getHistory().setMonth1("YES".equalsIgnoreCase(currentValue));
        } else if ("M3".equalsIgnoreCase(localName)) {
            getHistory().setMonth3("YES".equalsIgnoreCase(currentValue));
        } else if ("M6".equalsIgnoreCase(localName)) {
            getHistory().setMonth6("YES".equalsIgnoreCase(currentValue));
        } else if ("Y1".equalsIgnoreCase(localName)) {
            getHistory().setYear1("YES".equalsIgnoreCase(currentValue));
        }

    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        super.characters(ch, start, length);
        if (currentElement) {
            currentValue = new String(ch, start, length).trim();
            currentElement = false;
        }
    }

    public GuestHistoryConfig getHistory() {
        return history;
    }

    public void setHistory(GuestHistoryConfig history) {
        this.history = history;
    }

}
