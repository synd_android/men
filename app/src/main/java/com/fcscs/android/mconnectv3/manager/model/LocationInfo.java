package com.fcscs.android.mconnectv3.manager.model;

import com.fcscs.android.mconnectv3.common.util.SoapHelper;

import org.ksoap2.serialization.SoapObject;

import java.io.Serializable;

/**
 * Created by NguyenMinhTuan on 11/4/15.
 */
public class LocationInfo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String GroupID;
    private String GroupCode;
    private String GroupName;
    private String GroupHTID;
    private String LocationCodeID;
    private String LocationCodeCode;
    private String LocationCodeName;
    private String LocationCodeQrCode;

    public LocationInfo() {
    }

    public LocationInfo(SoapObject soap) {
        GroupID = SoapHelper.getStringProperty(soap, "GroupID", "");
        GroupCode = SoapHelper.getStringProperty(soap, "GroupCode", "");
        GroupName = SoapHelper.getStringProperty(soap, "GroupName", "");
        GroupHTID = SoapHelper.getStringProperty(soap, "GroupHTID", "");
        LocationCodeID = SoapHelper.getStringProperty(soap, "LocationCodeID", "");
        LocationCodeCode = SoapHelper.getStringProperty(soap, "LocationCodeCode", "");
        LocationCodeName = SoapHelper.getStringProperty(soap, "LocationCodeName", "");
        LocationCodeQrCode = SoapHelper.getStringProperty(soap, "LocationCodeQrCode", "");
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getLocationCodeQrCode() {
        return LocationCodeQrCode;
    }

    public void setLocationCodeQrCode(String locationCodeQrCode) {
        LocationCodeQrCode = locationCodeQrCode;
    }

    public String getLocationCodeName() {
        return LocationCodeName;
    }

    public void setLocationCodeName(String locationCodeName) {
        LocationCodeName = locationCodeName;
    }

    public String getGroupID() {
        return GroupID;
    }

    public void setGroupID(String groupID) {
        GroupID = groupID;
    }

    public String getGroupCode() {
        return GroupCode;
    }

    public void setGroupCode(String groupCode) {
        GroupCode = groupCode;
    }

    public String getGroupName() {
        return GroupName;
    }

    public void setGroupName(String groupName) {
        GroupName = groupName;
    }

    public String getGroupHTID() {
        return GroupHTID;
    }

    public void setGroupHTID(String groupHTID) {
        GroupHTID = groupHTID;
    }

    public String getLocationCodeID() {
        return LocationCodeID;
    }

    public void setLocationCodeID(String locationCodeID) {
        LocationCodeID = locationCodeID;
    }

    public String getLocationCodeCode() {
        return LocationCodeCode;
    }

    public void setLocationCodeCode(String locationCodeCode) {
        LocationCodeCode = locationCodeCode;
    }

}
