package com.fcscs.android.mconnectv3.main;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils.TruncateAt;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.ui.McProgressAsyncTask;
import com.fcscs.android.mconnectv3.common.ui.RoundCornerListAdapter;
import com.fcscs.android.mconnectv3.common.util.DialogHelper;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.dao.entity.Runner;
import com.fcscs.android.mconnectv3.manager.model.RunnerModel;
import com.fcscs.android.mconnectv3.ws.cconnect.AdhocService;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.FindRunnerResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.GetTemplateResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.Template;
import com.fcscs.android.mconnectv3.ws.econnect.ECService;

public class IndividualMessageActivity extends NewMessageBaseActivity {

    private static final int RC_RUNNER_SEARCH = 1;

    //	private List<RunnerModel> adapterRunners = new ArrayList<RunnerModel>();
    private GetTemplateResponse curTemp;
    //	private TreeSet<Long> addedRunners = new TreeSet<Long>();
    private RunnerListAdapter adapter;
    private boolean isReLoad;
    //	private ArrayList<Runner> myRunners;
    protected IndividualMessageSender task;
    private FindRunnerLoader task1;
    private TemplateLoader task2;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        homeTopBar.getTitleTv().setText(getString(R.string.individual_message));

        adapter = new RunnerListAdapter(this, R.layout.common_message_adapter_item);
        runnerListView.setAdapter(adapter);
        runnerListView.setEmptyView(findViewById(R.id.tv_empty));


        etRunner.setHint(R.string.input_recipient_name);
        etRunner.setOnKeyListener(new OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:
                            findRunners();
                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        });


        btnAdd.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if (!validateMessage()) {
                    return;
                }

                if (message.getRunners().size() == 0) {
                    toast(R.string.no_recipient);
                    return;
                }

                if (task == null || AsyncTask.Status.RUNNING.equals(task.getStatus()) == false) {
                    task = new IndividualMessageSender(getCurrentContext());
                    task.exec();
                }

            }
        });

        btnCancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                confirmCancel();
            }
        });
    }


    @SuppressWarnings("unchecked")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == RC_RUNNER_SEARCH) {
            ArrayList<Runner> myRunners = (ArrayList<Runner>) data.getSerializableExtra(SearchRunnerActivity.KEY_RESULT);
            if (myRunners != null) {
                addRunnersToList(myRunners);
            }
        }
    }

    public void addRunner(long id, String name) {

        RunnerModel item = new RunnerModel();
        item.setId(id);
        item.setName(name);

        if (!isReLoad && message.getRunners().contains(item)) {
            return;
        }

        TextView tv = new TextView(IndividualMessageActivity.this);
        tv.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
        tv.setText(name);
        tv.setTextColor(android.graphics.Color.GRAY);
        tv.setSingleLine(true);
        tv.setEllipsize(TruncateAt.MIDDLE);
        tv.setPadding(0, 5, 2, 0);
        etRunner.setText("");

        message.getRunners().add(item);
    }

    protected void findRunners() {

        String str = etRunner.getText().toString().trim();

        if (task1 == null || AsyncTask.Status.RUNNING.equals(task1.getStatus()) == false) {
            task1 = new FindRunnerLoader(getCurrentContext(), str, null);
            task1.exec();
        }

    }

    class FindRunnerLoader extends McProgressAsyncTask {

        public FindRunnerLoader(Context context, String term, Long deptId) {
            super(context);
            this.term = term;
            this.deptId = deptId;
        }

        private String term;
        private Long deptId;
        private FindRunnerResponse res;

        @Override
        public void doInBackground() {
            if (isCConnect()) {
                res = AdhocService.getInstanceNonUI().findRunners(IndividualMessageActivity.this, term, deptId, 0, 300);
            } else {
                res = ECService.getInstance().getAdHocUserList(IndividualMessageActivity.this, term, null, null);
            }
        }

        @Override
        public void onPostExecute() {
            if (res != null) {
                List<Runner> runners = res.getRunners();
                if (runners == null || runners.size() == 0) {
                    toast(getString(R.string.no_runner_found));
                } else if (runners.size() == 1) {
                    addRunnersToList(runners);
                } else {
                    Intent i = new Intent(getCurrentContext(), SearchRunnerActivity.class);
                    i.putExtra(SearchRunnerActivity.KEY_RUNNER, res);
                    i.putExtra(SearchRunnerActivity.KEY_TERM, term);
                    i.putExtra(SearchRunnerActivity.KEY_DEP_ID, deptId);
                    startActivityForResult(i, RC_RUNNER_SEARCH);
                }
            }
        }

    }

    public boolean validateMessage() {
        String msg = etMessage.getEditableText().toString().trim();
        if (McUtils.getMessageCount(msg) == 0) {
            toast(getString(R.string.message_is_empty));
            return false;
        } else {
            return true;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return confirmCancel();
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean confirmCancel() {
        if (McUtils.isNullOrEmpty(etMessage.getText().toString()) == false) {

            DialogHelper.showYesNoDialog(getCurrentContext(), R.string.confirm_cancel,
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            setResult(RESULT_OK);
                            finish();
                        }
                    });

        } else {
            setResult(RESULT_CANCELED);
            finish();
        }
        hideSoftInput(etMessage);
        return true;
    }

    class RunnerListAdapter extends RoundCornerListAdapter<RunnerModel> {

        public RunnerListAdapter(Context context, int layout) {
            super(context, new ArrayList<RunnerModel>(), layout);
        }

        public void refresh() {
            mList = message.getRunners();
            notifyDataSetChanged();
        }

        @Override
        public void onBindView(View view, Context context, int position, final RunnerModel item) {

            TextView tv = (TextView) view.findViewById(R.id.individualRunner);
            ImageView iv = (ImageView) view.findViewById(R.id.individualRunner_delete);
            tv.setText(item.getName());
            iv.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    message.getRunners().remove(item);
                    refresh();
                }
            });

            view.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(IndividualMessageActivity.this, NewMessageItemDetailActivity.class);
                    i.putExtra("index", message.getRunners().indexOf(item));
                    IndividualMessageActivity.this.startActivity(i);
                }
            });
        }

    }

    class TemplateLoader extends McProgressAsyncTask {

        private long templateId;

        public TemplateLoader(Context context, long templateId) {
            super(context);
            this.templateId = templateId;
        }

        @Override
        public void doInBackground() {
            isReLoad = true;
            curTemp = AdhocService.getInstance().getMsgTemplate(getCurrentContext(), templateId);
        }

        @Override
        public void onPostExecute() {
            if (curTemp != null) {
                etMessage.setText(curTemp.getDetail());
                message.getRunners().clear();
                for (GetTemplateResponse.Runner r : curTemp.getRunnerList()) {
                    addRunner(r.getRunnerId(), r.getUsername());
                }
                adapter.refresh();
                isReLoad = false;
            }
        }

    }

    @Override
    protected void onTemplateSelected(AdapterView<?> parent, View view, int position, long id, Template temp) {
        if (temp != null) {
            if (isCConnect()) {
                if (task2 == null || AsyncTask.Status.RUNNING.equals(task2.getStatus()) == false) {
                    task2 = new TemplateLoader(getCurrentContext(), temp.getTemplateId());
                    task2.exec();
                }
            } else {
                etMessage.setText(temp.getDetail());
            }
        } else {
            if (curTemp != null) {
                etMessage.setText("");
            }
            curTemp = null;
        }

    }

    private void addRunnersToList(List<Runner> runners) {
        for (Runner r : runners) {
            addRunner(r.getRunnerId(), r.getFullname());
        }
        adapter.refresh();
        McUtils.adjustListViewHeight(runnerListView, adapter);
    }

}
