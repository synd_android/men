package com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice;

import java.io.Serializable;
import java.util.List;

import com.fcscs.android.mconnectv3.manager.model.FavouriteLink;

public class GetFavouriteLinkResponse implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private List<FavouriteLink> favouriteLinks = null;

    public GetFavouriteLinkResponse() {
//		minibarItems = new ArrayList<MinibarItem>();
    }

    public void setFavouriteLinks(List<FavouriteLink> favouriteLinks) {
        this.favouriteLinks = favouriteLinks;
    }

    public List<FavouriteLink> getFavouriteLinks() {
        return favouriteLinks;
    }

}
