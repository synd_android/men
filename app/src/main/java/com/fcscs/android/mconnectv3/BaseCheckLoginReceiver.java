package com.fcscs.android.mconnectv3;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import com.fcscs.android.mconnectv3.common.ui.McBackgroundAsyncTask;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.ws.econnect.ECService;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.GetCheckLoginResponse;

public abstract class BaseCheckLoginReceiver extends BroadcastReceiver {

    public static final String TAG = BaseCheckLoginReceiver.class.getSimpleName();
    public static final Integer NO_NETWORK = 3000;

    protected static class CheckLoginTask extends McBackgroundAsyncTask {

        private Context context;
        private boolean isNoNetwork = true;
        private int returnCode = -1;
        private int type;

        public CheckLoginTask(Context context, int type) {
            super(context);
            this.context = context;
            this.type = type;
        }

        @Override
        public void doInBackground() {
            if (context == null) {
                return;
            }

            isNoNetwork = McUtils.isNetworkOff(context);
            if (isNoNetwork) {
                McUtils.saveCrashReport(context, new Throwable("No Network."));
                return;
            } else {
                NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                manager.cancel(NO_NETWORK);
                GetCheckLoginResponse resp = getCheckLogin(context);
                if (resp != null && resp.getReturnCode() == 1) {
                    //TODO: already login duplication show logout screen
                    toastMsg(resp.getErrorMsg());
                    Intent intent = new Intent();
                    intent.setAction("LOGIN_MCONNECT_DUPLICATION");
                    context.sendBroadcast(intent);
                }
            }
        }

        @Override
        public void onPostExecute() {
        }

        private GetCheckLoginResponse getCheckLogin(Context context) {
            GetCheckLoginResponse resp = null;
            try {
                resp = ECService.getInstance().getCheckLogin(context);
            } catch (Throwable e) {
            }
            return resp;
        }

        private void toastMsg(final String s) {
            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable() {
                public void run() {
                    Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

}


