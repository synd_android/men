package com.fcscs.android.mconnectv3.ws.cconnect.model.guestrequestservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.ksoap2.serialization.SoapObject;

import android.content.Context;

import com.fcscs.android.mconnectv3.manager.model.GuestInfo;

public class GuestInfoResponse implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private List<GuestInfo> guestList;


    public List<GuestInfo> getGuestList() {
        return guestList;
    }


    public void setGuestList(List<GuestInfo> guestList) {
        this.guestList = guestList;
    }

    public GuestInfoResponse() {
        guestList = new ArrayList<GuestInfo>();
    }

    public GuestInfoResponse(SoapObject soap) {
        guestList = new ArrayList<GuestInfo>();
        if (soap.getPropertyCount() > 0) {
            for (int i = 0; i < soap.getPropertyCount(); i++) {
                guestList.add(new GuestInfo((SoapObject) soap.getProperty(i)));
            }
        }
    }

    public GuestInfoResponse(Context context, SoapObject soap, boolean getGuestListing) {
        guestList = new ArrayList<GuestInfo>();
        if (soap.getPropertyCount() > 0) {
            for (int i = 0; i < soap.getPropertyCount(); i++) {
                guestList.add(new GuestInfo(context, (SoapObject) soap.getProperty(i), getGuestListing));
            }
        }
    }

}
