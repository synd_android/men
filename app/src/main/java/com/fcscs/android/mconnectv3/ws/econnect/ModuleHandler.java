package com.fcscs.android.mconnectv3.ws.econnect;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.ModuleLicense;

public class ModuleHandler extends DefaultHandler {

    private boolean currentElement;
    private String currentValue;
    private ModuleLicense licence;

    public ModuleHandler() {
        this.licence = new ModuleLicense();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        super.startElement(uri, localName, qName, attributes);
        currentElement = true;
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        super.endElement(uri, localName, qName);

        currentElement = false;
        if ("MyJob".equalsIgnoreCase(localName)) {
            if ("YES".equals(currentValue)) {
                this.getLicence().setMyJob(true);
            }
        } else if ("AdHoc".equalsIgnoreCase(localName)) {
            if ("YES".equals(currentValue)) {
                this.getLicence().setMyMessage(true);
            }
        } else if ("ViewJob".equalsIgnoreCase(localName)) {
            if ("YES".equals(currentValue)) {
                this.getLicence().setJobSearch(true);
            }
        } else if ("SendMessage".equalsIgnoreCase(localName)) {
            if ("YES".equals(currentValue)) {
                this.getLicence().setSendMessage(true);
            }
        } else if ("JobRequest".equalsIgnoreCase(localName)) {
            if ("YES".equals(currentValue)) {
                this.getLicence().setJobRequest(true);
            }
        } else if ("GuestDetail".equalsIgnoreCase(localName)) {
            if ("YES".equals(currentValue)) {
                this.getLicence().setGuestDetail(true);
            }
        } else if ("Minibar".equalsIgnoreCase(localName)) {
            if ("YES".equals(currentValue)) {
                this.getLicence().setMinibar(true);
            }
        } else if ("RoomStatus".equalsIgnoreCase(localName)) {
            if ("YES".equals(currentValue)) {
                this.getLicence().setRoomStatus(true);
            }
        } else if ("Favourite".equalsIgnoreCase(localName)) {
            if ("YES".equals(currentValue)) {
                this.getLicence().setFavorite(true);
            }
        }

        if (getLicence().isJobRequest()) {
            getLicence().setGuestRequest(true);
            getLicence().setInterDeptRequest(true);
            getLicence().setNonGuestRequest(true);
        }

    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        super.characters(ch, start, length);
        if (currentElement) {
            currentValue = new String(ch, start, length).trim();
            currentElement = false;
        }
    }

    public ModuleLicense getLicence() {
        return licence;
    }

}
