package com.fcscs.android.mconnectv3.ws.cconnect.model.guestrequestservice;

import com.fcscs.android.mconnectv3.manager.model.LocationInfo;

import org.ksoap2.serialization.SoapObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by NguyenMinhTuan on 11/4/15.
 */
public class FindLocationResponse implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private List<LocationInfo> locationInfoList;

    public FindLocationResponse() {
        locationInfoList = new ArrayList<LocationInfo>();
    }

    public FindLocationResponse(SoapObject soap) {
        locationInfoList = new ArrayList<LocationInfo>();
        if (soap.getPropertyCount() > 0) {
            for (int i = 0; i < soap.getPropertyCount(); i++) {
                locationInfoList.add(new LocationInfo((SoapObject) soap.getProperty(i)));
            }
        }
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public List<LocationInfo> getLocationInfoList() {
        return locationInfoList;
    }

    public void setLocationInfoList(List<LocationInfo> locationInfoList) {
        this.locationInfoList = locationInfoList;
    }


}

