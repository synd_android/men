package com.fcscs.android.mconnectv3.common.ui;

import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.content.res.Configuration;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.util.PrefsUtil;
import com.fcscs.android.mconnectv3.manager.model.HomeModuleTag;
import com.fcscs.android.mconnectv3.manager.model.HomeModuleTag.OnViewBindListener;


public class HomeModuleListAdapter extends BaseAdapter {

    private Context context;
    private final List<HomeModuleTag> tags;
    private LayoutInflater inflater;
    private Locale mLocale;

    public HomeModuleListAdapter(Context context, final List<HomeModuleTag> tags) {
        super();
        this.context = context;
        this.tags = tags;
        this.inflater = LayoutInflater.from(this.context);
        mLocale = PrefsUtil.getLocale();
    }

    @Override
    public int getCount() {
        return tags.size();
    }

    @Override
    public Object getItem(int position) {
        return tags.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Configuration config = this.context.getResources().getConfiguration();
        if (config.locale == null || config.locale.equals(mLocale) == false) {
            config.locale = mLocale;
            this.context.getResources().updateConfiguration(config, this.context.getResources().getDisplayMetrics());
        }

        final HomeModuleTag tag = (HomeModuleTag) getItem(position);
        convertView = inflater.inflate(R.layout.night_item, parent, false);
        TextView text = (TextView) convertView.findViewById(R.id.tv_item_text);
        tag.setText(text);
        tag.getText().setText(context.getText(tag.getTextResId()));
        ImageView icon = (ImageView) convertView.findViewById(R.id.iv_item_image);
        tag.setIcon(icon);
        tag.getIcon().setImageResource(tag.getIconResId());
        TextView messageCount = (TextView) convertView.findViewById(R.id.message_account);
        tag.setMessageCount(messageCount);
        tag.getMessageCount().setVisibility(View.INVISIBLE);
        if (tag.getClickListener() != null) {
            convertView.setOnClickListener(tag.getClickListener());
        }

        OnViewBindListener bind = tag.getBindListener();
        if (bind != null) {
            bind.onViewBind(position, convertView, parent);
        }

        return convertView;
    }

}
