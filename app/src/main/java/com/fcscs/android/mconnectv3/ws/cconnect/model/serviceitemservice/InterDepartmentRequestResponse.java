package com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;

import android.util.Log;

public class InterDepartmentRequestResponse implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4304894090217721712L;

    private int result;
    private String errorMessage;
    private List<String> jobNumList;

    //econnect
    private String returnCode;

    private String returnCode1;
    private String errorMessage1;

    private String returnCode2;
    private String errorMessage2;

    private String returnCode3;
    private String errorMessage3;

    public String getReturnCode3() {
        return returnCode3;
    }

    public void setReturnCode3(String returnCode3) {
        this.returnCode3 = returnCode3;
    }

    public String getErrorMessage3() {
        return errorMessage3;
    }

    public void setErrorMessage3(String errorMessage3) {
        this.errorMessage3 = errorMessage3;
    }

    public InterDepartmentRequestResponse() {
        jobNumList = new ArrayList<String>();
    }

    public InterDepartmentRequestResponse(SoapObject response) {
        jobNumList = new ArrayList<String>();
        result = Integer.valueOf(response.getAttributeAsString("result"));
        if (result == 1) {
            for (int i = 0; i < response.getPropertyCount(); i++) {
                if (response.hasProperty("jobNo") && (response.getProperty("jobNo") instanceof SoapPrimitive)) {
                    //TODO:
                    Log.i("InterDep", response.getProperty(i) + "---InterDepartmentReqest--55-----------");
                    jobNumList.add(response.getPropertyAsString(i));
                }
            }
        } else {
            //			errorMessage = response.getProperty("jobNo").toString();
        }
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<String> getJobNumList() {
        return jobNumList;
    }

    public void setJobNumList(List<String> jobNumList) {
        this.jobNumList = jobNumList;
    }

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public String getReturnCode1() {
        return returnCode1;
    }

    public void setReturnCode1(String returnCode1) {
        this.returnCode1 = returnCode1;
    }

    public String getErrorMessage1() {
        return errorMessage1;
    }

    public void setErrorMessage1(String errorMessage1) {
        this.errorMessage1 = errorMessage1;
    }

    public String getReturnCode2() {
        return returnCode2;
    }

    public void setReturnCode2(String returnCode2) {
        this.returnCode2 = returnCode2;
    }

    public String getErrorMessage2() {
        return errorMessage2;
    }

    public void setErrorMessage2(String errorMessage2) {
        this.errorMessage2 = errorMessage2;
    }

}
