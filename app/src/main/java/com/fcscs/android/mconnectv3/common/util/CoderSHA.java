package com.fcscs.android.mconnectv3.common.util;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class CoderSHA {

    private static final String SHA_TRANSFORMATION = "HmacSHA256";//SHA-256
    private static final String MD5_TRANSFORMATION = "MD5";
    private static final String CHARSET = "UTF-8";

    public static String coderMD5(String pass) {

        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest.getInstance(MD5_TRANSFORMATION);
            digest.update(pass.getBytes());
            byte messageDigest[] = digest.digest();
            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++)
                hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
            // Log.d("MD5", "Pass: " + pass + ", MD5 Hash: " + hexString.toString());
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            // e.printStackTrace();
        }
        return "";
    }

    public static String saltText(String in) {

        String salt = "%@&8AY-}@^*)$h/#Z";
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < in.length(); i++) {
            result.append(in.charAt(i));
            result.append(salt.charAt(i % salt.length()));
        }
        return result.toString();
    }

    public static String hmacSHA256Digest(String msg, String keyString) {
        String digest = null;
        try {
            SecretKeySpec key = new SecretKeySpec((keyString).getBytes(CHARSET), SHA_TRANSFORMATION);
            Mac mac = Mac.getInstance(SHA_TRANSFORMATION);
            mac.init(key);

            byte[] bytes = mac.doFinal(msg.getBytes("ASCII"));

            StringBuffer hash = new StringBuffer();
            for (int i = 0; i < bytes.length; i++) {
                String hex = Integer.toHexString(0xFF & bytes[i]);
                if (hex.length() == 1) {
                    hash.append('0');
                }
                hash.append(hex);
            }
            digest = hash.toString();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return digest;
    }

}
