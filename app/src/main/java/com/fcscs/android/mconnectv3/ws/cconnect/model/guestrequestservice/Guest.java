package com.fcscs.android.mconnectv3.ws.cconnect.model.guestrequestservice;

import java.io.Serializable;
import java.util.Date;

import org.ksoap2.serialization.SoapObject;

import com.fcscs.android.mconnectv3.common.util.DateTimeHelper;
import com.fcscs.android.mconnectv3.common.util.SoapHelper;

public class Guest implements Serializable {

    public static final String PATTERN = DateTimeHelper.DEFAULT_FORMATTER_DATETIME_24H;

    /**
     */
    private static final long serialVersionUID = -168683513667074636L;

    //Attribute
    private Date checkinDate;
    private Date etaDate;
    private long guestRoomLinkId;
    private long locationId;
    private long pmsGuestId;
    private boolean vip;
    private String vipMsg;
    private String language;
    private String roomNum;
    private String checkin;
    private String checkout;

    //elements
    private String guestName;
    private String extNo;
    private String comments;
    private Date checkoutDate;
    private Date etdDate;

    private String dnd;
    private String textMsg;
    private String voiceMsg;
    private String preference;

    public Guest() {
    }

    public Guest(SoapObject soap) {

        guestRoomLinkId = SoapHelper.getLongAttribute(soap, "guestRoomLinkId", 0L);
        locationId = SoapHelper.getLongAttribute(soap, "locationId", 0L);
        pmsGuestId = SoapHelper.getLongAttribute(soap, "pmsGuestId", 0L);
        vip = SoapHelper.getBooleanAttribute(soap, "vip", false);
        roomNum = SoapHelper.getStringAttribute(soap, "roomNo", "");

        guestName = SoapHelper.getStringProperty(soap, "guestName", "");
        extNo = SoapHelper.getStringProperty(soap, "extNo", "");
        comments = SoapHelper.getStringProperty(soap, "comments", "");

        String checkoutDateStr = SoapHelper.getStringAttribute(soap, "checkoutDate", null);
        if (checkoutDateStr != null) {
            checkoutDate = DateTimeHelper.parseDateStr(checkoutDateStr, PATTERN, null);
        }

        String checkinDateStr = SoapHelper.getStringAttribute(soap, "checkinDate", null);
        if (checkoutDateStr != null) {
            checkinDate = DateTimeHelper.parseDateStr(checkinDateStr, PATTERN, null);
        }

        String strEtaDateStr = SoapHelper.getStringAttribute(soap, "ETA", null);
        if (strEtaDateStr != null) {
            etaDate = DateTimeHelper.parseDateStr(strEtaDateStr, PATTERN, null);
        }

        String strEtdDateStr = SoapHelper.getStringAttribute(soap, "ETD", null);
        if (strEtaDateStr != null) {
            etdDate = DateTimeHelper.parseDateStr(strEtdDateStr, PATTERN, null);
        }

        dnd = "Off";
        textMsg = "No";
        voiceMsg = "No";
    }

    public String getCheckin() {
        return checkin;
    }

    public void setCheckin(String checkin) {
        this.checkin = checkin;
    }

    public String getCheckout() {
        return checkout;
    }

    public void setCheckout(String checkout) {
        this.checkout = checkout;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getVipMsg() {
        return vipMsg;
    }

    public void setVipMsg(String vipMsg) {
        this.vipMsg = vipMsg;
    }

    public String getDnd() {
        return dnd;
    }

    public void setDnd(String dnd) {
        this.dnd = dnd;
    }

    public String getTextMsg() {
        return textMsg;
    }

    public void setTextMsg(String textMsg) {
        this.textMsg = textMsg;
    }

    public String getVoiceMsg() {
        return voiceMsg;
    }

    public void setVoiceMsg(String voiceMsg) {
        this.voiceMsg = voiceMsg;
    }

    public long getPmsGuestId() {
        return pmsGuestId;
    }

    public void setPmsGuestId(long pmsGuestId) {
        this.pmsGuestId = pmsGuestId;
    }

    public String getRoomNum() {
        return roomNum;
    }

    public void setRoomNum(String roomNum) {
        this.roomNum = roomNum;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getExtNo() {
        return extNo;
    }

    public long getLocationId() {
        return locationId;
    }

    public boolean isVip() {
        return vip;
    }

    public long getGuestRoomLinkId() {
        return guestRoomLinkId;
    }

    public Date getCheckinDate() {
        return checkinDate;
    }

    public Date getCheckoutDate() {
        return checkoutDate;
    }

    public Date getETADate() {
        return etaDate;
    }

    public Date getETDDate() {
        return etdDate;
    }

    public String getGuestName() {
        return guestName;
    }

    public void setCheckinDate(Date checkinDate) {
        this.checkinDate = checkinDate;
    }

    public void setGuestRoomLinkId(long guestRoomLinkId) {
        this.guestRoomLinkId = guestRoomLinkId;
    }

    public void setLocationId(long locationId) {
        this.locationId = locationId;
    }

    public void setVip(boolean vip) {
        this.vip = vip;
    }

    public void setGuestName(String guestName) {
        this.guestName = guestName;
    }

    public void setExtNo(String extNo) {
        this.extNo = extNo;
    }

    public void setCheckoutDate(Date checkoutDate) {
        this.checkoutDate = checkoutDate;
    }

    public String getPreference() {
        return preference;
    }

    public void setPreference(String preference) {
        this.preference = preference;
    }

}