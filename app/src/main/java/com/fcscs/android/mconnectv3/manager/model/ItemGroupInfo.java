package com.fcscs.android.mconnectv3.manager.model;

import com.fcscs.android.mconnectv3.common.util.SoapHelper;

import org.ksoap2.serialization.SoapObject;

import java.io.Serializable;

/**
 * Created by NguyenMinhTuan on 11/4/15.
 */
public class ItemGroupInfo implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String ID = "";
    private String Code = "";
    private String Name = "";

    public ItemGroupInfo() {
    }

    public ItemGroupInfo(SoapObject soap) {
        ID = SoapHelper.getStringProperty(soap, "ID", "");
        Code = SoapHelper.getStringProperty(soap, "Code", "");
        Name = SoapHelper.getStringProperty(soap, "Name", "");
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}