package com.fcscs.android.mconnectv3.common.ui;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.util.McUtils;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

public class RecordPlayLayout extends LinearLayout {


    public static interface OnClickRecordPlay {
        void clickPlay(RecordPlayLayout play);

        void clickPause(RecordPlayLayout play);

        void clickDelete(RecordPlayLayout play);
    }

    private static final String TAG = RecordPlayLayout.class.getSimpleName();
    private ImageView ivDelete;

    private ImageView ivPlay;
    private TextView tvTime;
    private View voicell;
    private Context context;
    private int counter;
    private MediaPlayer mediaPlayer;
    private int playCounter;
    private Timer playTimer;
    private OnClickRecordPlay listener;
    private View view;

    public RecordPlayLayout(Context context) {
        this(context, null);
    }

    public RecordPlayLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.common_record_play_layout, this);

        if (!isInEditMode()) {
            voicell = findViewById(R.id.playback);
            tvTime = (TextView) findViewById(R.id.record_time);
            ivPlay = (ImageView) findViewById(R.id.record_play);
            ivDelete = (ImageView) findViewById(R.id.delete);
            voicell.setVisibility(View.GONE);
            ivDelete.setVisibility(View.GONE);
        }
    }

    public void setListener(OnClickRecordPlay listener) {
        this.listener = listener;
    }

    public void stopRecordPlay() {
        if ("play".equals(ivPlay.getTag())) {
            stopMediaPlayer();
        }
    }

    public void stopAndHideLayout() {
        stopRecordPlay();
        view.setVisibility(View.GONE);
    }

    public void setRecordFile(final File voice) {
        if (voice != null && voice.exists() && voice.length() > 0) {
            Log.d(TAG, String.format("record file=%s, size=%dk", voice.getAbsolutePath(), ((int) voice.length() / 1024)));
            MediaPlayer mp = MediaPlayer.create(context, Uri.fromFile(voice));
            counter = mp.getDuration() / 1000;
            if (counter > 0) {
                voicell.setVisibility(View.VISIBLE);
                tvTime.setText(McUtils.formatCounter(counter));
                ivPlay.setClickable(true);
                ivPlay.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if ("play".equals(ivPlay.getTag())) {
                            if (listener != null) {
                                listener.clickPause(RecordPlayLayout.this);
                            }
                            stopMediaPlayer();
                        } else {
                            if (listener != null) {
                                listener.clickPlay(RecordPlayLayout.this);
                            }
                            playMedia(voice);
                        }
                    }
                });
            } else {
                view.setVisibility(View.GONE);
            }
        } else {
            view.setVisibility(View.GONE);
        }
    }

    public void setDemoRecordFile() {
        MediaPlayer mp = MediaPlayer.create(context, R.raw.demo_audio);
        counter = mp.getDuration() / 1000;
        if (counter > 0) {
            voicell.setVisibility(View.VISIBLE);
            tvTime.setText(McUtils.formatCounter(counter));
            ivPlay.setClickable(true);
            ivPlay.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if ("play".equals(ivPlay.getTag())) {
                        if (listener != null) {
                            listener.clickPause(RecordPlayLayout.this);
                        }
                        stopMediaPlayer();
                    } else {
                        if (listener != null) {
                            listener.clickPlay(RecordPlayLayout.this);
                        }
                        playMediaDemo();
                    }
                }
            });
        }
    }


    public void setAllowDelete(boolean allow) {
        if (allow) {
            ivDelete.setVisibility(View.VISIBLE);
            ivDelete.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.clickDelete(RecordPlayLayout.this);
                    }
                }
            });
        } else {
            ivDelete.setVisibility(View.GONE);
        }
    }

    private void playMediaDemo() {

        try {
            mediaPlayer = MediaPlayer.create(context, R.raw.demo_audio);
            mediaPlayer.start();

            ivPlay.setTag("play");
            ivPlay.setImageResource(R.drawable.record_stop);

            playCounter = counter;
            playTimer = new Timer();
            playTimer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    mPlayHandler.obtainMessage(1).sendToTarget();
                }
            }, 0, 1000);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void playMedia(File file) {

        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setDataSource(file.getAbsolutePath());
            mediaPlayer.prepare();
            mediaPlayer.start();

            ivPlay.setTag("play");
            ivPlay.setImageResource(R.drawable.record_stop);

            playCounter = counter;
            playTimer = new Timer();
            playTimer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    mPlayHandler.obtainMessage(1).sendToTarget();
                }
            }, 0, 1000);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void stopMediaPlayer() {
        try {

            if (playTimer != null) {
                playTimer.cancel();
            }

            if (mediaPlayer != null) {
                mediaPlayer.stop();
                mediaPlayer.reset();
                mediaPlayer.release();
                mediaPlayer = null;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        ivPlay.setTag("");
        ivPlay.setImageResource(R.drawable.record_play);

        int second = counter % 60;
        int minute = counter / 60;
        tvTime.setText(String.format("%02d:%02d", minute, second));

    }

    public Handler mPlayHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (playCounter >= 0) {
                playCounter--;
                if(playCounter != -1){
                    tvTime.setText(String.format("-%s", McUtils.formatCounter(playCounter)));
                }
            } else if (playCounter == -1) {
                stopMediaPlayer();
                playCounter--;
            }
        }
    };

}
