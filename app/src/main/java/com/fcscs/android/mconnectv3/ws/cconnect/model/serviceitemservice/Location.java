//package com.fcscs.android.mconnect.ws.cconnect.model.serviceitemservice;
//
//import java.io.Serializable;
//
//import org.ksoap2.serialization.SoapObject;
//import org.ksoap2.serialization.SoapPrimitive;
//
//public class Location implements Serializable{
//
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = -3670212901736961959L;
//	private Long locationId;
//	private String locationCode;
//	private String name;
//	private String floorId;
//	private String buildingId;
//	
//	public Location(){
//	}
//	public Location(SoapObject soap){
//		locationId = Long.valueOf(soap.getAttributeAsString("locationId"));
//		locationCode = soap.getAttributeAsString("code");
//		
//		if (soap.hasProperty("description") && (soap.getProperty("description") instanceof SoapPrimitive)) {
//			name = ((SoapPrimitive) soap.getProperty("description")).toString();
//		}
//		
//		if (soap.hasProperty("roomNum") && (soap.getProperty("roomNum") instanceof SoapPrimitive)) {
//			name = ((SoapPrimitive) soap.getProperty("roomNum")).toString();
//		}
//	}
//	
//	public Long getLocationId() {
//		return locationId;
//	}
//	public void setLocationId(Long locationId) {
//		this.locationId = locationId;
//	}
//	public String getLocationCode() {
//		return locationCode;
//	}
//	public void setLocationCode(String locationCode) {
//		this.locationCode = locationCode;
//	}
//	public String getFullname() {
//		return name;
//	}
//	public void setFullname(String description) {
//		this.name = description;
//	}
//
//	public String getFloorId() {
//		return floorId;
//	}
//
//	public void setFloorId(String floor) {
//		this.floorId = floor;
//	}
//	public String getBuildingId() {
//		return buildingId;
//	}
//	public void setBuildingId(String buildingId) {
//		this.buildingId = buildingId;
//	}
//	
//	
//}
