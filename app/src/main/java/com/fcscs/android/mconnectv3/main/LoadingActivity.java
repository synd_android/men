package com.fcscs.android.mconnectv3.main;

import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.BaseActivity;
import com.fcscs.android.mconnectv3.common.McConstants;
import com.fcscs.android.mconnectv3.common.McEnums;
import com.fcscs.android.mconnectv3.common.SessionContext;
import com.fcscs.android.mconnectv3.common.ui.McBackgroundAsyncTask;
import com.fcscs.android.mconnectv3.common.util.CoderSHA;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.common.util.PrefsUtil;
import com.fcscs.android.mconnectv3.engineering.EnHomeActivity;
import com.fcscs.android.mconnectv3.manager.model.Language;
import com.fcscs.android.mconnectv3.smartpush.SmartPush;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.ModuleLicense;

public class LoadingActivity extends BaseActivity {

    private static final String TAG = LoadingActivity.class.getSimpleName();

    static final String PROPERTY_LANGUAGE = "property_language";
    private static final String LANGUAGE_LOCALE = "language_locale";
    static final String LANGUAGE_INDEX = "language_index";
    static final String LANGUAGE_LIST = "language_list";
    protected static final String RESET_LOCALE = "RESET_LOCALE";
    private boolean firstLaunch;

    private ECLoginLoader task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//		McApplication.application.removeObserver(this);
        setContentView(R.layout.loading);

        if (getIntent().getBooleanExtra(RESET_LOCALE, false)) {
            firstLaunch = false;
        } else {
            firstLaunch = true;
        }

    }


    @SuppressWarnings("unused")
    @Override
    protected void onStart() {
        super.onStart();

        getSessionCtx().populate(getCurrentContext());
        getConfigCtx().populate(getCurrentContext());

        if (McUtils.isNullOrEmpty(getConfigCtx().getServiceUrl())) {
            Intent i = new Intent(this, ConfigActivity.class);
            startActivity(i);
            return;
        }

        if (McConstants.SHOW_CONFIG_PAGE && PrefsUtil.isFirstLaunchAfterInstall(this)) {
            PrefsUtil.setFirstLaunchAfterInstall(this, false);
            Intent i = new Intent(this, ConfigActivity.class);
            startActivity(i);
            return;
        }

        if (firstLaunch && getSessionCtx().isLogin() && isEconnect()) {
            String demoUsr = "demo";
            String demoPwd = getResources().getString(R.string.fcs_demo_pwd);
            if (McConstants.COMPILATION_PRODUCT == McEnums.CompilationProduct.PERCIPIA) {
                demoPwd = getResources().getString(R.string.per_demo_pwd);
            }
            String username = getSessionCtx().getUsername();
            String password = getSessionCtx().getPassword();

            //SHA256
            String key = getResources().getString(R.string.secure_pref_key);
            if (demoUsr.equals(username) && demoPwd.equals(CoderSHA.hmacSHA256Digest(password, key))) {
                PrefsUtil.setOfflineMode(this, true);
            } else {
                PrefsUtil.setOfflineMode(this, false);
            }

            if (task == null || AsyncTask.Status.RUNNING.equals(task.getStatus()) == false) {
                task = new ECLoginLoader(this);
                task.exec();
            }
            return;
        }

        gotoLoginPage();

    }


    private void gotoLoginPage() {
        List<Language> languages = McUtils.getLanguageList(this);

        Locale curLocale = Locale.US;
        int curIndex = 0;

        Locale sessionLocale = PrefsUtil.getLocale();
        Log.i(TAG, "locale: " + sessionLocale);

        for (int i = 0; i < languages.size(); i++) {
            Language lan = languages.get(i);
            Locale locale = lan.getLocale();
            if (sessionLocale.equals(locale)) {
                curLocale = locale;
                curIndex = i;
                break;
            }
        }

        getAppCache().put(LANGUAGE_INDEX, curIndex);
        getAppCache().put(LANGUAGE_LOCALE, curLocale);
        getAppCache().put(LANGUAGE_LIST, languages);

        Configuration config = getResources().getConfiguration();
        config.locale = curLocale;
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());

        Intent intent = new Intent(LoadingActivity.this, LoginActivity.class);
        startActivity(intent);


    }

    private class ECLoginLoader extends McBackgroundAsyncTask {

        private ModuleLicense mod;
        private boolean success;

        public ECLoginLoader(Context context) {
            super(context);
        }

        private void pause() {
            try {
                Thread.sleep(800);
            } catch (InterruptedException e) {
            }
        }

        @Override
        public void doInBackground() {
//			String type = "2";
//			LoginResponse resp = ECService.getInstance().getLogin(getCurrentContext(), getSessionCtx().getUsername(), getSessionCtx().getPassword(), "", type);
//			if (resp != null && "0".equals(resp.getReturnCode())) {
//				success = true;			
//				
//				pause();
//				ECService.getInstance().postUpdateLoginLanguage(getCurrentContext());
//				pause();
//				ECService.getInstance().getCommonConfigurations(getCurrentContext());	
//				pause();
//				boolean is2_1 = true;
//				mod = ECService.getInstance().getModuleLicense(getCurrentContext(), getSessionCtx().getFcsUserId(), is2_1);
//				if (mod != null) {
//					getSessionCtx().setModule(mod);
//					getSessionCtx().persist(getCurrentContext());
//				} else {
//					success = false;
//				}
//			}	
//			success = true;
        }

        @Override
        public void onPostExecute() {
//			if (success) {
//				GCMIntentService.registerGCM(getCurrentContext());
            SessionContext.getInstance().setUserTimeZone(PrefsUtil.getTimeZone(LoadingActivity.this));
            SessionContext.getInstance().setUserDepartment(PrefsUtil.getUserDepartMent(LoadingActivity.this));
            SessionContext.getInstance().setUserType(PrefsUtil.getUserType(LoadingActivity.this));
            SessionContext.getInstance().setReAssignRunner(PrefsUtil.getmEnReAssignRunner(LoadingActivity.this));
            SmartPush.getInstance().registerPush(LoadingActivity.this.getApplicationContext());
            if (PrefsUtil.getEnableEng(getCurrentContext()) == 0) {
                Intent i = new Intent(getCurrentContext(), HomeActivity.class);
                i.putExtra(McConstants.KEY_USERNAME, getSessionCtx().getUsername());
                startActivity(i);
            } else {
                Intent i = new Intent(getCurrentContext(), EnHomeActivity.class);
                i.putExtra(McConstants.KEY_USERNAME, getSessionCtx().getUsername());
                startActivity(i);
            }


//			} else {
//				if (mod == null) {
//					toast(R.string.fail_user_license);
//				}
//				gotoLoginPage();
//			}			
        }

    }

    @Override
    public boolean isLoginActivity() {
        return false;
    }

    @Override
    public boolean isHomeActivity() {
        return false;
    }

}
