package com.fcscs.android.mconnectv3;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.fcscs.android.mconnectv3.common.McConstants;
import com.fcscs.android.mconnectv3.common.ServiceExceptionHandler;
import com.fcscs.android.mconnectv3.common.util.PrefsUtil;

public class PullMsgReceiver extends BasePullReceiver {

    private Context ctx;
    private PullTask task;


    public void setAlarm(Context context) {

        if (context == null) {
            return;
        }

        int interval = PrefsUtil.getMsgPullingInterval(context);
        if (interval <= 0) {
            return;
        } else if (interval <= 20) {
            interval = 20;
        }

        Log.d(TAG, "msg timer interval=" + interval);
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, PullMsgReceiver.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, TYPE_MSG, i, 0);
        //am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 1000 * interval, pi);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            AlarmManager.AlarmClockInfo alarmClockInfo = new AlarmManager.AlarmClockInfo(System.currentTimeMillis() + 1000 * interval, pi);
            am.setAlarmClock(alarmClockInfo, pi);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            am.setExact(android.app.AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 1000 * interval, pi);
        } else {
            am.set(android.app.AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 1000 * interval, pi);
        }
    }

    void cancelAlarm(Context context) {

        if (context == null) {
            return;
        }

        int interval = PrefsUtil.getMsgPullingInterval(context);
        if (interval <= 0) {
            return;
        }

        Intent intent = new Intent(context, PullMsgReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, TYPE_MSG, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (McConstants.OFFLINE_MODE) {
            return;
        }

        ctx = context;
        ServiceExceptionHandler.setHandler(ctx);

        Log.d(TAG, "repeat msg timer");
        if (task == null || AsyncTask.Status.RUNNING.equals(task.getStatus()) == false) {
            task = new PullTask(context, TYPE_MSG);
            task.exec();
            setAlarm(context);
        }
    }
}
