package com.fcscs.android.mconnectv3.ws.econnect.model.commonservice;

import java.io.Serializable;

public class ProfileNote implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -6729275403830557172L;
    private String typeCode;
    private String typeDesc;
    private String noteDesc;

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getTypeDesc() {
        return typeDesc;
    }

    public void setTypeDesc(String typeDesc) {
        this.typeDesc = typeDesc;
    }

    public String getNoteDesc() {
        return noteDesc;
    }

    public void setNoteDesc(String noteDesc) {
        this.noteDesc = noteDesc;
    }
}
