package com.fcscs.android.mconnectv3.main;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.BaseActivity;
import com.fcscs.android.mconnectv3.common.ConfigContext;
import com.fcscs.android.mconnectv3.common.ui.HomeTopBar;
import com.fcscs.android.mconnectv3.common.ui.McProgressAsyncTask;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.common.util.PrefsUtil;
import com.fcscs.android.mconnectv3.dao.entity.DBSessionContext;
import com.fcscs.android.mconnectv3.ws.econnect.ECService;

/**
 * Created by FCS on 2014/5/14.
 */
public class PanicButtonActivity extends BaseActivity {

    private static final String SECRET_KEY = "9jLjXmk4hkQ/DvHswmZggGQJGsgQdPAd";
    private String line1No;
    private EditText etLocation;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.panic_button_layout);

        HomeTopBar topbar = (HomeTopBar) findViewById(R.id.top_bar);
        topbar.getTitleTv().setText(R.string.panic_button);

        etLocation = (EditText) findViewById(R.id.location);

        TextView tel = (TextView) findViewById(R.id.tel_no);
        TelephonyManager telManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        line1No = telManager.getLine1Number();
        if (McUtils.isNullOrEmpty(line1No) == false) {
            tel.setText(String.format("%s (%s)", line1No, getString(R.string.auto_detect)));
        } else {
            tel.setText(getString(R.string.no_phone_no_detected));
        }

        TextView runner = (TextView) findViewById(R.id.runner);

        DBSessionContext prefs = PrefsUtil.getSessionPrefs(this);
        if (prefs.getLogin() == false) {
            topbar.getRightBtn().setVisibility(View.GONE);
            runner.setText(getString(R.string.no_runner_logged_in));
        } else {
            String username = prefs.getUsername();
            String fullname = prefs.getFullname();
            if (McUtils.isNullOrEmpty(fullname) && McUtils.isNullOrEmpty(username) == false) {
                runner.setText(username);
            } else if (McUtils.isNullOrEmpty(fullname) == false && McUtils.isNullOrEmpty(username) == false) {
                runner.setText(String.format("%s (%s)", fullname, username));
            }
        }

        Button call = (Button) findViewById(R.id.call);
        if (ConfigContext.getInstance().isEnablePanicCall()) {
            call.setVisibility(View.VISIBLE);
        } else {
            call.setVisibility(View.INVISIBLE);
        }
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                call();
            }
        });

        Button cancel = (Button) findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Button submit = (Button) findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new PanicButtonSubmit(getCurrentContext()).exec();
            }
        });
    }

    @Override
    public boolean isLoginActivity() {
        return false;
    }

    @Override
    public boolean isHomeActivity() {
        return false;
    }

    private class PanicButtonSubmit extends McProgressAsyncTask {

        private boolean result;

        public PanicButtonSubmit(Context context) {
            super(context);
        }

        @Override
        public void doInBackground() {
            result = ECService.getInstance().postPanicButtonAlert(getCurrentContext(), line1No, SECRET_KEY, etLocation.getText().toString());
        }

        @Override
        public void onPostExecute() {
            if (result) {
                Toast.makeText(getCurrentContext(), getString(R.string.success_submit_panic_message), Toast.LENGTH_LONG).show();
                if (ConfigContext.getInstance().isEnablePanicCall()) {
                    call();
                }
            } else {
                Toast.makeText(getCurrentContext(), getString(R.string.failed_sumit_panic_message), Toast.LENGTH_LONG).show();
            }
        }
    }

    private void call() {
        String phoneNo = ConfigContext.getInstance().getPanicCallContactNumber();
        if (McUtils.isNullOrEmpty(phoneNo) == false) {
            try {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + phoneNo));
                startActivity(callIntent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}