package com.fcscs.android.mconnectv3.main;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.fcscs.android.mconnectv3.R;

import com.fcscs.android.mconnectv3.common.BaseActivity;
import com.fcscs.android.mconnectv3.common.ui.FcsAdapterForAdHocMessage;
import com.fcscs.android.mconnectv3.common.ui.HomeTopBar;

public class SendMessageActivity extends BaseActivity {

    private ListView adhocLv;
    private List<String> adhocList;
    private HomeTopBar homeTopBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.send_message);

        homeTopBar = (HomeTopBar) findViewById(R.id.top_bar);
        homeTopBar.getTitleTv().setText(R.string.new_messages);

        prepareData();

        adhocLv = (ListView) findViewById(R.id.ad_hoc_msg_list_view);
        adhocLv.setSelector(R.drawable.spacebar);
        adhocLv.setClickable(true);
        adhocLv.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                String selReq = adhocList.get(pos);
                Intent i = null;
                if (getString(R.string.individual_message).equals(selReq)) {
                    i = new Intent(SendMessageActivity.this, IndividualMessageActivity.class);
                } else if (getString(R.string.group_message).equals(selReq)) {
                    i = new Intent(SendMessageActivity.this, GroupMessageActivity.class);
                }
                if (i != null) {
                    startActivity(i);
                }
            }
        });
        FcsAdapterForAdHocMessage adapter = new FcsAdapterForAdHocMessage(this, adhocList);
        adhocLv.setAdapter(adapter);

    }

    public void prepareData() {
        adhocList = new ArrayList<String>();
        adhocList.add(getString(R.string.individual_message));
        adhocList.add(getString(R.string.group_message));
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            setResult(RESULT_OK);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean isLoginActivity() {
        return true;
    }

    @Override
    public boolean isHomeActivity() {
        return false;
    }
}
