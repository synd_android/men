package com.fcscs.android.mconnectv3.manager.model;

import java.io.Serializable;

import com.j256.ormlite.field.DatabaseField;

public class LanguageResource implements Serializable {

    @DatabaseField(generatedId = true)
    private int id = 0;

    @DatabaseField
    private String name = "";
    @DatabaseField
    private String code = "";
    @DatabaseField
    private String value = "";

    @DatabaseField
    private Integer propertyId = null;

    private static final long serialVersionUID = 1L;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(Integer propertyId) {
        this.propertyId = propertyId;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof LanguageResource) {
            LanguageResource p = (LanguageResource) o;
            if (name == null || code == null) {
                return false;
            }
            if (name.equals(p.name) && code.equals(p.code)) {
                return true;
            }
        }
        return false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


}
