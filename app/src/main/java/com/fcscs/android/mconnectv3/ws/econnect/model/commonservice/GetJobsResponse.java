package com.fcscs.android.mconnectv3.ws.econnect.model.commonservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.ksoap2.serialization.SoapObject;

import com.fcscs.android.mconnectv3.common.util.SoapConverter;
import com.fcscs.android.mconnectv3.common.util.SoapHelper;
import com.fcscs.android.mconnectv3.manager.model.JobModel;

public class GetJobsResponse implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7783347883143157205L;

    private int returnCode;
    private String errorMsg;
    private List<JobModel> list;

    public GetJobsResponse() {

    }

    public GetJobsResponse(SoapObject soap) {
        SoapObject respStatus = (SoapObject) soap.getProperty("ResponseStatus");
        returnCode = SoapHelper.getIntegerProperty(respStatus, "ReturnCode", 1);
        errorMsg = SoapHelper.getStringProperty(respStatus, "ErrorMsg", "");
        list = new ArrayList<JobModel>();
        for (int i = 0; i < soap.getPropertyCount(); i++) {
            if ("JobListing".equalsIgnoreCase(SoapHelper.getPropertyName(soap, i))) {
                SoapObject detail = (SoapObject) soap.getProperty(i);
                if (!detail.hasProperty("JobDetails")) {
                    continue;
                }
                SoapObject l = (SoapObject) detail.getProperty("JobDetails");
                JobModel jm = SoapConverter.getECJob(l);
                list.add(jm);

            }
        }
    }

    public int getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(int returnCode) {
        this.returnCode = returnCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public List<JobModel> getList() {
        return list;
    }

    public void setList(List<JobModel> list) {
        this.list = list;
    }

}
