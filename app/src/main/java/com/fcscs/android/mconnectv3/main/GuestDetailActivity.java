package com.fcscs.android.mconnectv3.main;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.ImageView;
import android.widget.ListView;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.McConstants;
import com.fcscs.android.mconnectv3.common.QRCodeParserFactory;
import com.fcscs.android.mconnectv3.common.QRCodeParserFactory.LocationQRCodeParser;
import com.fcscs.android.mconnectv3.common.ui.McProgressAsyncTask;
import com.fcscs.android.mconnectv3.common.util.PrefsUtil;
import com.fcscs.android.mconnectv3.manager.model.GuestInfo;
import com.fcscs.android.mconnectv3.ws.cconnect.GuestRequestService;
import com.fcscs.android.mconnectv3.ws.cconnect.model.guestrequestservice.GuestInfoResponse;
import com.fcscs.android.mconnectv3.ws.econnect.ECService;
import com.fcscs.android.mconnectv3.zxing.CaptureActivity;

import java.util.ArrayList;
import java.util.List;

public class GuestDetailActivity extends GuestDetailCommonActivity {

    private GuestLoader task;
    private GuestListLoader task1;
    ImageView qrButton;
    final static int SCANNIN_LOCATION_CODE = 1001;

    private int mLastFirstVisibleItem;
    private boolean blnIsScrollingUp;
    private List<GuestInfo> vListTmpGuestInfo = new ArrayList<GuestInfo>();
    private AlertDialog.Builder builder = null;
    private AlertDialog _popWin = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        searchField.setVisibility(View.VISIBLE);
        if (McConstants.NON_HOTEL_PRODUCT) {
            searchField.setHint(R.string.room_num);
        }
        searchField.setOnKeyListener(new OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:
                            //findGuestByName();
                            findGuestByName(1);
                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        });
        qrButton = (ImageView) findViewById(R.id.qr_camera);
        qrButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(GuestDetailActivity.this, CaptureActivity.class);
                intent.putExtra("type", QRCodeParserFactory.LOCATION_REQUEST);
                startActivityForResult(intent, SCANNIN_LOCATION_CODE);
            }
        });
        qrButton.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                findGuestByName(true);
                return true;
            }
        });

        boolean enable = PrefsUtil.getQRCodeGuestDetail(this);
        if (enable == false) {
            qrButton.setVisibility(View.GONE);
        }
    }

    final static int QR_CODE = 1001;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == QR_CODE) {
                Bundle bundle = data.getExtras();
                String result = bundle.getString("result");
                result = result.replace("location|LOC", "");
                searchField.setText(result);
                findGuestByName(false);
            }

            if (requestCode == SCANNIN_LOCATION_CODE) {
                LocationQRCodeParser parser = new LocationQRCodeParser();
                String qrcode = data.getStringExtra("result");
                if (parser.isCorrectFormat(qrcode)) {
                    searchField.setText(parser.room);
                    findGuestByName(false);
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void findGuestByName(int PageIndex) {

        int intPageIndex = PageIndex;
        String strSearchGuestName = searchField.getText().toString().trim();
        if (strSearchGuestName.length() == 0) {
            searchField.requestFocus();
            toast(getString(R.string.please_enter_room_or_guest_name));
            return;
        }

        if (task1 == null || AsyncTask.Status.RUNNING.equals(task1.getStatus()) == false) {
            //task1 = new GuestListLoader(getCurrentContext(), strSearchGuestName);
            task1 = new GuestListLoader(getCurrentContext(), intPageIndex, strSearchGuestName);
            task1.exec();
        }
    }


    //AlertDialog.Builder builder
    private void findGuestByName(AlertDialog.Builder builder, ListView listView, int PageIndex) {
        int intPageIndex = PageIndex;
        String strSearchGuestName = searchField.getText().toString().trim();
        if (strSearchGuestName.length() == 0) {
            searchField.requestFocus();
            toast(getString(R.string.please_enter_room_or_guest_name));
            return;
        }
        if (task1 == null || AsyncTask.Status.RUNNING.equals(task1.getStatus()) == false) {
            //task1 = new GuestListLoader(getCurrentContext(), strSearchGuestName);
            //task1 = new GuestListLoader(getCurrentContext(), listView, intPageIndex, strSearchGuestName);
            task1 = new GuestListLoader(getCurrentContext(), builder, listView, intPageIndex, strSearchGuestName);
            task1.exec();
        }
    }


    //..Chua...
    private void findGuestByName(ListView listView, int PageIndex) {
        int intPageIndex = PageIndex;
        String strSearchGuestName = searchField.getText().toString().trim();
        if (strSearchGuestName.length() == 0) {
            searchField.requestFocus();
            toast(getString(R.string.please_enter_room_or_guest_name));
            return;
        }
        if (task1 == null || AsyncTask.Status.RUNNING.equals(task1.getStatus()) == false) {
            //task1 = new GuestListLoader(getCurrentContext(), strSearchGuestName);
            task1 = new GuestListLoader(getCurrentContext(), listView, intPageIndex, strSearchGuestName);
            task1.exec();
        }
    }

    private void findGuestByName(boolean searchAll) {

        String strSearchGuestName = searchField.getText().toString().trim();
        if (!searchAll) {
            if (strSearchGuestName.length() == 0) {
                searchField.requestFocus();
                toast(getString(R.string.please_enter_room_or_guest_name));
                return;
            }
        } else {
            _popWin = null;
        }

        if (task1 == null || AsyncTask.Status.RUNNING.equals(task1.getStatus()) == false) {
            task1 = new GuestListLoader(getCurrentContext(), strSearchGuestName);
            task1.exec();
        }
    }

    private void processUi(GuestInfoResponse res, int PageIndex) {
        if (res != null) {
            final int intPageIndex = PageIndex;
            //final List<GuestInfo> vListDataGuest = res.getGuestList();
            final List<GuestInfo> vListDataGuest;
            if (intPageIndex == 1) {
                vListTmpGuestInfo.clear();
                vListTmpGuestInfo.addAll(res.getGuestList());
            } else {
                vListTmpGuestInfo.addAll(res.getGuestList());
            }
            vListDataGuest = vListTmpGuestInfo;

            if (vListDataGuest.size() > 1) {
                List<String> list = new ArrayList<String>();
                for (GuestInfo guest : vListDataGuest) {
                    String display = "";
                    if (guest.getRoomNum() != null && !"".equalsIgnoreCase(guest.getRoomNum())) {
                        display = display + "(" + guest.getRoomNum() + ")";
                    }
                    list.add(display + guest.getGuestUsername());
                }
                String[] items = new String[list.size()];
                items = (String[]) list.toArray(items);

                //--------------------------------------------------------------------------------------------------------------------

                if (intPageIndex == 1) {
                    builder = new AlertDialog.Builder(this);
                }
                if (builder == null) {
                    builder = new AlertDialog.Builder(this);
                }

                //builder = new AlertDialog.Builder(this);
                //..Chua
                //String strDebugTitle = getString(R.string.label_select_guest);
                //toast(strDebugTitle, R.string.request_create_error);
                //toast(getString(R.string.label_select_guest));
                builder.setTitle(getString(R.string.label_select_guest));
                //builder.setTitle("Test Paging");
                //"Four","Five","Six","Seven","Eight","Nine","Ten"

                //builder.setItems(new String[] {"one","two","three","Four","Five","Six","Seven","Eight","Nine","Ten"}, new DialogInterface.OnClickListener() {
                builder.setItems(items, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which > -1 && which < vListDataGuest.size()) {
                            if (isCConnect()) {
                                populatityUserInfo(GuestRequestService.getInstance()
                                        .getGuestDetail(GuestDetailActivity.this, vListDataGuest.get(which).getRoomLinkId())
                                        .getGuestList().get(0));
                            } else {
                                populatityUserInfo(vListDataGuest.get(which));
                            }
                            dialog.cancel();
                        }
                    }    //..End of Click...
                });


                //AlertDialog.Builder builder = new AlertDialog.Builder(this);
                //builder.setTitle("Select Color Mode");
                //..MEthod 2: using listview...
                //final ListView _listView = new ListView(this);
                //int intDrawable = getResources().getIdentifier("subtle_white_gradient", "drawable", "com.my.package");
                //_listView.setBackgroundColor(intDrawable);
                //_listView.setBackgroundColor(android.graphics.Color.WHITE);

                //String[] strAryData = new String[] { "Bright Mode", "Normal Mode", "three","Four","Five","Six","Seven","Eight","Nine","Ten" };
                //android.widget.ArrayAdapter<String> _Adapter = new android.widget.ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, strAryData);
                //android.widget.ArrayAdapter<String> _Adapter = new android.widget.ArrayAdapter<String>(this, android.R.layout.simple_list_item_single_choice, strAryData);
                //android.widget.ArrayAdapter<String> _Adapter = new android.widget.ArrayAdapter<String>(getCurrentContext(), android.R.layout.simple_list_item_1, items);
                //_listView.setAdapter(_Adapter);
                //builder.setView(_listView);
                //final android.app.Dialog _dialog = builder.create();
                //_dialog.show();


                if (intPageIndex == 1) {
                    _popWin = builder.create();
                }
                if (_popWin == null) {
                    _popWin = builder.create();
                }
                _popWin.show();

                //---------------------------------     Scroll Code 	---------------------------------\\

                if (intPageIndex == 1) {
                    final ListView _listView = _popWin.getListView();
                    _listView.setOnScrollListener(new android.widget.AbsListView.OnScrollListener() {

                        @Override
                        public void onScrollStateChanged(android.widget.AbsListView view, int scrollState) {
                            if (view.getId() == _listView.getId()) {
                                final int intTmpCurrentFirstVisibleItem = _listView.getFirstVisiblePosition();
                                //final int intTmpCurrentFirstVisibleItem = view.getFirstVisiblePosition();

                                if (intTmpCurrentFirstVisibleItem > mLastFirstVisibleItem) {
                                    blnIsScrollingUp = false;
                                } else if (intTmpCurrentFirstVisibleItem < mLastFirstVisibleItem) {
                                    blnIsScrollingUp = true;
                                }

                                mLastFirstVisibleItem = intTmpCurrentFirstVisibleItem;

                                if (blnIsScrollingUp == false) {
                                    //..Detected user scroll down...
                                    int intTotalCount = view.getCount() - 1;

                                    if ((view.getLastVisiblePosition() == intTotalCount) && (scrollState == SCROLL_STATE_IDLE)) {
                                        //_popWin.dismiss();
                                        //android.widget.Toast.makeText(getCurrentContext(), "End of Scroll. Index : " + (intPageIndex + 1), android.widget.Toast.LENGTH_SHORT).show();
                                        //findGuestByName(intPageIndex + 1);
                                        //findGuestByName(listView, intPageIndex + 1);
                                        findGuestByName(builder, _listView, intPageIndex + 1);
                                    }     //..End of if...

                                }    //..End of if...

                            }    //..End of if...
                        }

                        @Override
                        public void onScroll(android.widget.AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                        }

                    });


                }    //..End of If...

                //--------------------------------- End Of Scroll Code  ---------------------------------//

            } else if (vListDataGuest.size() == 1) {

                if (task == null || AsyncTask.Status.RUNNING.equals(task.getStatus()) == false) {
                    task = new GuestLoader(getCurrentContext(), vListDataGuest.get(0));
                    task.exec();
                }

            } else {
                toast(getString(R.string.msg_no_guest_found));
                populatityUserInfo(null);
            }

        }//..End of res...
    }

    private void processUi_orig(GuestInfoResponse res, int PageIndex) {
        if (res != null) {
            final int intPageIndex = PageIndex;
            //final List<GuestInfo> vListDataGuest = res.getGuestList();
            final List<GuestInfo> vListDataGuest;
            if (intPageIndex == 1) {
                vListTmpGuestInfo.clear();
                vListTmpGuestInfo.addAll(res.getGuestList());
            } else {
                vListTmpGuestInfo.addAll(res.getGuestList());
            }
            vListDataGuest = vListTmpGuestInfo;

            if (vListDataGuest.size() > 1) {
                List<String> list = new ArrayList<String>();
                for (GuestInfo guest : vListDataGuest) {
                    String display = "";
                    if (guest.getRoomNum() != null && !"".equalsIgnoreCase(guest.getRoomNum())) {
                        display = display + "(" + guest.getRoomNum() + ")";
                    }
                    list.add(display + guest.getGuestUsername());
                }
                String[] items = new String[list.size()];
                items = (String[]) list.toArray(items);

                //AlertDialog.Builder builder = new AlertDialog.Builder(this);
                if (builder == null) {
                    builder = new AlertDialog.Builder(this);
                }
                //builder = new AlertDialog.Builder(this);
                //..Chua
                //String strDebugTitle = getString(R.string.label_select_guest);
                //toast(strDebugTitle, R.string.request_create_error);
                //toast(getString(R.string.label_select_guest));
                //builder.setTitle(getString(R.string.label_select_guest));
                builder.setTitle("Test Paging");
                /*
				builder.setItems(items, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (which > -1 && which < vListDataGuest.size()) {
							if (isCConnect()) {
								populatityUserInfo(GuestRequestService.getInstance()
										.getGuestDetail(GuestDetailActivity.this, vListDataGuest.get(which).getRoomLinkId())
										.getGuestList().get(0));
							} else {
								populatityUserInfo(vListDataGuest.get(which));
							}
							dialog.cancel();
						}
					}
				});
				builder.setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (which > -1 && which < vListDataGuest.size()) {
							if (isCConnect()) {
								populatityUserInfo(GuestRequestService.getInstance()
										.getGuestDetail(GuestDetailActivity.this, vListDataGuest.get(which).getRoomLinkId())
										.getGuestList().get(0));
							} else {
								populatityUserInfo(vListDataGuest.get(which));
							}
							dialog.cancel();
						}
					}
				});
				*/


                builder.setItems(items, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which > -1 && which < vListDataGuest.size()) {
                            if (isCConnect()) {
                                populatityUserInfo(GuestRequestService.getInstance()
                                        .getGuestDetail(GuestDetailActivity.this, vListDataGuest.get(which).getRoomLinkId())
                                        .getGuestList().get(0));
                            } else {
                                populatityUserInfo(vListDataGuest.get(which));
                            }
                            dialog.cancel();
                        }
                    }
                });
				
	
				
				
				
				/*
				builder.setNegativeButton("Cancel",
	                    new DialogInterface.OnClickListener() {

	                        @Override
	                        public void onClick(DialogInterface dialog, int which) {
	                            dialog.dismiss();
	                        }
	                    });
	            */


                //..Date Added : 03 June 2015
                //..Chua
                //AlertDialog alert = builder.create();
                //builder.getListView().setFastScrollEnabled(true);
                //AlertDialog _popWin = builder.create();
                if (intPageIndex == 1) {
                    //_popWin = builder.create();
                }
                _popWin = builder.create();


                if (intPageIndex == 1) {
                    final ListView listView = _popWin.getListView();
                    //ListAdapter lvAdapter = _popWin.getListView().getAdapter();

                    //..Get listview current position - used to maintain scroll position...
                    //int intCurrentPosition = listView.getFirstVisiblePosition();
                    //listView.setSelectionFromTop(intCurrentPosition + 1, 0);
                    //listView.setOnScrollListener(new android.widget.AbsListView.OnScrollListener(){

                    //}
                    //_popWin.getListView().setOnScrollListener(new android.widget.AbsListView.OnScrollListener(){
                    listView.setOnScrollListener(new android.widget.AbsListView.OnScrollListener() {

                        @Override
                        public void onScrollStateChanged(android.widget.AbsListView view, int scrollState) {

                            if (view.getId() == listView.getId()) {
                                final int intTmpCurrentFirstVisibleItem = listView.getFirstVisiblePosition();
                                //final int intTmpCurrentFirstVisibleItem = view.getFirstVisiblePosition();


                                if (intTmpCurrentFirstVisibleItem > mLastFirstVisibleItem) {
                                    blnIsScrollingUp = false;
                                } else if (intTmpCurrentFirstVisibleItem < mLastFirstVisibleItem) {
                                    blnIsScrollingUp = true;
                                }

                                mLastFirstVisibleItem = intTmpCurrentFirstVisibleItem;

                                if (blnIsScrollingUp == false) {
                                    //..Detected user scroll down...
                                    int intTotalCount = view.getCount() - 1;

                                    if ((view.getLastVisiblePosition() == intTotalCount) && (scrollState == SCROLL_STATE_IDLE)) {
                                        /*** In this way I detect if there's been a scroll which has completed ***/
                                        /*** do the work! ***/
                                        //_popWin.dismiss();
                                        //android.widget.Toast.makeText(getCurrentContext(), "End of Scroll. Index : " + (intPageIndex + 1), android.widget.Toast.LENGTH_SHORT).show();
                                        //findGuestByName(intPageIndex + 1);
                                        //findGuestByName(listView, intPageIndex + 1);
                                        findGuestByName(builder, listView, intPageIndex + 1);
                                    }     //..End of if...

                                }    //..End of if...

                            }    //..End of if...

                        }

                        @Override
                        public void onScroll(android.widget.AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                        }

                    });
                }


                //if(intPageIndex == 1){
                _popWin.show();
                //}


            } else if (vListDataGuest.size() == 1) {

                if (task == null || AsyncTask.Status.RUNNING.equals(task.getStatus()) == false) {
                    task = new GuestLoader(getCurrentContext(), vListDataGuest.get(0));
                    task.exec();
                }

            } else {
                toast(getString(R.string.msg_no_guest_found));
                populatityUserInfo(null);
            }
        }    //..End of res...
    }

    private void processUi(GuestInfoResponse res, AlertDialog.Builder builder, ListView listView, int PageIndex) {
        if (res != null) {
            final int intPageIndex = PageIndex;
            //final List<GuestInfo> vListDataGuest = res.getGuestList();
            final List<GuestInfo> vListDataGuest;
            if (intPageIndex == 1) {
                vListTmpGuestInfo.clear();
                vListTmpGuestInfo.addAll(res.getGuestList());
            } else {
                vListTmpGuestInfo.addAll(res.getGuestList());
            }
            vListDataGuest = vListTmpGuestInfo;

            if (vListDataGuest.size() > 1) {
                List<String> list = new ArrayList<String>();
                for (GuestInfo guest : vListDataGuest) {
                    String display = "";
                    if (guest.getRoomNum() != null && !"".equalsIgnoreCase(guest.getRoomNum())) {
                        display = display + "(" + guest.getRoomNum() + ")";
                    }
                    list.add(display + guest.getGuestUsername());
                }
                String[] items = new String[list.size()];
                items = (String[]) list.toArray(items);

                //android.widget.ArrayAdapter myAdap = new android.widget.ArrayAdapter();

                //AlertDialog.Builder builder = new AlertDialog.Builder(this);
				
				/*
				 
				 android.widget.ArrayAdapter<String> mAdapter = new android.widget.SimpleCursorAdapter(getActivity(),
                        android.R.layout.simple_list_item_single_choice,
                        null,
                        new String[] { "title" },
                        new int[] { android.R.id.text1 },
                        0);
                        
                 android.widget.ArrayAdapter<String> arrayAdapter = new android.widget.ArrayAdapter<String>(
		                 getCurrentContext(), 
		                 android.R.layout.simple_list_item_single_choice,
		                 new String[] {"one","two","three"});
				 
				 android.widget.ArrayAdapter<String> arrayAdapter = new android.widget.ArrayAdapter<String>(
		                 getCurrentContext(), 
		                 android.R.layout.simple_list_item_1,
		                 items);
		                 
		                 
				 */


                //..Set position when scroll...
                int intCurrentPosition = listView.getFirstVisiblePosition();

                //lvAdapter = new ItemListAdapter(getCurrentContext(), aryList, R.layout.search_serviceitem_list_item);
                //listView.setAdapter(lvAdapter);
				
				
				/*
				android.R.layout.simple_list_item_single_choice
				"one 1","two 2","three 3","Four 4","Five 5","Six 6","Seven 7","Eight 8","Nine 9","Ten 10"
				android.widget.ArrayAdapter<String> arrayAdapter = new android.widget.ArrayAdapter<String>(
						getCurrentContext(), 
		                 android.R.layout.simple_list_item_single_choice,
		                 items);
		        
		        android.widget.ArrayAdapter<String> arrayAdapter = new android.widget.ArrayAdapter<String>(
						 getCurrentContext(), 
		                 android.R.layout.simple_list_item_1,
		                 new String[] { "one 1","two 2","three 3","Four 4" });
		        
		        android.widget.ArrayAdapter<String> arrayAdapter = new android.widget.ArrayAdapter<String>(
						 getCurrentContext(), 
		                 android.R.layout.simple_list_item_1,
		                 items);
		                 
		        */

                android.widget.ArrayAdapter<String> arrayAdapter = new android.widget.ArrayAdapter<String>(
                        getCurrentContext(),
                        android.R.layout.simple_list_item_1,
                        items) {

                    @Override
                    public View getView(int position, android.view.View convertView, android.view.ViewGroup parent) {
                        View view = super.getView(position, convertView, parent);

                        android.widget.TextView _textView = (android.widget.TextView) view.findViewById(android.R.id.text1);

			            /*YOUR CHOICE OF COLOR*/
                        _textView.setTextColor(android.graphics.Color.BLACK);

                        return view;
                    }

                };

                //new String[] {"one","two","three","Four","Five","Six","Seven","Eight","Nine","Ten"}


                //ListAdapter lvAdapter = listView.getAdapter();
                //listView.setAdapter(null);
                listView.setAdapter(arrayAdapter);
                //listView.setBackgroundColor(android.graphics.Color.BLACK);
                listView.setBackgroundColor(android.graphics.Color.WHITE);
                //listView.getAdapter().notify();
                listView.setSelectionFromTop(intCurrentPosition + 1, 0);


                //listView.getAdapter().notify();
                //builder.setView(listView);
                //builder.notify();


                //builder.setTitle("Test Paging 888");

                //final ListView listView = _popWin.getListView();
                //ItemListAdapter lvAdapter = new ItemListAdapter(getCurrentContext(), items, R.layout.search_serviceitem_list_item);
                //ListAdapter lvAdapter = listView.getAdapter();
                //ListAdapter lvAdapter = listView.setAdapter(adapter);
                //listView.setAdapter(null);
                //listView.setAdapter(lvAdapter);

                //listView.setAdapter(null);
                //builder.setTitle("Test Paging");
                //builder.setItems(items, null);
                //listView.getAdapter().notify();
                //builder.notify();
                //builder.show();
				
				/*
				builder.setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (which > -1 && which < vListDataGuest.size()) {
							if (isCConnect()) {
								populatityUserInfo(GuestRequestService.getInstance()
										.getGuestDetail(GuestDetailActivity.this, vListDataGuest.get(which).getRoomLinkId())
										.getGuestList().get(0));
							} else {
								populatityUserInfo(vListDataGuest.get(which));
							}
							dialog.cancel();
						}
					}
				});
				
				listView.getAdapter().notify();
				builder.notify();
				*/
				
				/*
				ListAdapter lvAdapter = listView.getAdapter();
				lvAdapter.notify();		
				builder.notify();
				builder.show();
				*/


            }    //..End of If...

        }    //..End of res...
    }

    class GuestLoader extends McProgressAsyncTask {

        private List<GuestInfo> list;
        private GuestInfo info;

        public GuestLoader(Context context, GuestInfo info) {
            super(context);
            this.info = info;
        }

        @Override
        public void doInBackground() {
            if (isCConnect() && info.getRoomLinkId() != null) {
                long id = info.getRoomLinkId();
                list = GuestRequestService.getInstance().getGuestDetail(getCurrentContext(), id).getGuestList();
            }
        }

        @Override
        public void onPostExecute() {
            if (isCConnect()) {
                if (list != null && list.size() > 0) {
                    populatityUserInfo(list.get(0));
                }
            } else {
                populatityUserInfo(info);
            }
        }

    }

    class GuestListLoader extends McProgressAsyncTask {

        private AlertDialog.Builder builder;
        private ListView listView;
        private int intPageIndex;

        public GuestListLoader(Context context, String term) {
            super(context);
            this.term = term;
        }

        public GuestListLoader(Context context, int PageIndex, String term) {
            super(context);
            intPageIndex = PageIndex;
            this.term = term;
        }

        public GuestListLoader(Context context, ListView listView, int PageIndex, String term) {
            super(context);
            this.listView = listView;
            intPageIndex = PageIndex;
            this.term = term;
        }

        public GuestListLoader(Context context, AlertDialog.Builder builder, ListView listView, int PageIndex, String term) {
            super(context);
            this.builder = builder;
            this.listView = listView;
            intPageIndex = PageIndex;
            this.term = term;
        }

        private String term;
        private GuestInfoResponse res;

        @Override
        public void doInBackground() {
            if (isCConnect()) {
                res = GuestRequestService.getInstance().findGuestByName(getCurrentContext(), term);
            } else {
                if (!McConstants.NON_HOTEL_PRODUCT) {
                    if (term.matches("[0-9]+")) {
                        ////res = ECService.getInstanceNonUI().getGuestList(getCurrentContext(), "", "", term, "");
                        //res = ECService.getInstance().getGuestListing(getCurrentContext(), term, "", "", "", "");
                        res = ECService.getInstance().getGuestListing(getCurrentContext(), intPageIndex, term, "", "", "", "");
                    } else {
                        //Happen here...
                        ////res = ECService.getInstanceNonUI().getGuestList(getCurrentContext(), term, "", "", "");
                        //res = ECService.getInstance().getGuestListing(getCurrentContext(), "", term, "", "", "");
                        res = ECService.getInstance().getGuestListing(getCurrentContext(), intPageIndex, "", term, "", "", "");
                    }
                } else {
                    res = ECService.getInstance().getGuestListing(getCurrentContext(), intPageIndex, term, "", "", "", "");
                }
            }
        }

        @Override
        public void onPostExecute() {
            if (res != null) {
                //processUi(res, intPageIndex);
                if (this.builder == null) {
                    processUi(res, intPageIndex);
                } else {
                    processUi(res, this.builder, this.listView, intPageIndex);
                }
            }
        }
    }
}