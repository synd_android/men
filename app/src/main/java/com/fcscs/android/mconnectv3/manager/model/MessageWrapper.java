package com.fcscs.android.mconnectv3.manager.model;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by FCS on 2014/6/19.
 */
public class MessageWrapper implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2213712998460984839L;

    private static MessageWrapper instance = null;

    private String name;
    private String content;

    private File capturedImage;
    private MediaLibraryConfig.Tag imageTag;

    private File recordedVoice;
    private MediaLibraryConfig.Tag voiceTag;

    private List<RunnerModel> runners = new ArrayList<RunnerModel>();

    private MessageWrapper() {
    }

    public synchronized static MessageWrapper getInstance() {
        if (instance == null) {
            instance = new MessageWrapper();
        }
        return instance;
    }

    public static boolean isAvailable() {
        if (instance == null) {
            return false;
        }
        return true;
    }

    public static void destroyInstance() {
        if (instance == null) {
            return;
        }
        if (instance.getCapturedImage() != null) {
            instance.getCapturedImage().delete();
        }
        if (instance.getRecordedVoice() != null) {
            instance.getRecordedVoice().delete();
        }
        instance = null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public File getCapturedImage() {
        return capturedImage;
    }

    public void setCapturedImage(File capturedImage) {
        this.capturedImage = capturedImage;
    }

    public MediaLibraryConfig.Tag getImageTag() {
        return imageTag;
    }

    public void setImageTag(MediaLibraryConfig.Tag imageTag) {
        this.imageTag = imageTag;
    }

    public File getRecordedVoice() {
        return recordedVoice;
    }

    public void setRecordedVoice(File recordedVoice) {
        this.recordedVoice = recordedVoice;
    }

    public MediaLibraryConfig.Tag getVoiceTag() {
        return voiceTag;
    }

    public void setVoiceTag(MediaLibraryConfig.Tag voiceTag) {
        this.voiceTag = voiceTag;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<RunnerModel> getRunners() {
        return runners;
    }

    public void setRunners(List<RunnerModel> runners) {
        this.runners = runners;
    }

}
