package com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice;

import java.io.Serializable;
import java.util.List;

import com.fcscs.android.mconnectv3.manager.model.MinibarItem;

public class GetMinbarItemsResponse implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private List<MinibarItem> minibarItems = null;

    public GetMinbarItemsResponse() {
    }

    public void setMinibarItems(List<MinibarItem> minibarItems) {
        this.minibarItems = minibarItems;
    }

    public List<MinibarItem> getMinibarItems() {
        return minibarItems;
    }

}
