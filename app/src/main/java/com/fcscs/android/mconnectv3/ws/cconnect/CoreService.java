package com.fcscs.android.mconnectv3.ws.cconnect;

import java.util.ArrayList;
import java.util.List;

import org.ksoap2.serialization.SoapObject;

import android.content.Context;

import com.fcscs.android.mconnectv3.common.SessionContext;
import com.fcscs.android.mconnectv3.dao.entity.Department;
import com.fcscs.android.mconnectv3.http.WebServiceBase;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.ModuleLicense;

public class CoreService extends WebServiceBase {

    private static final String GET_AUTHORITY_RESPONSE = "GetAuthorityResponse";
    private static final String GET_AUTHORITY_REQUEST = "GetAuthorityRequest";
    private static final String WEBSERVICE_URL_SUFFIX = "ws/coreWSService";
    private static final String NAMESPACE = "http://fcscs.com/ws/schemas/coreservice";

    private static final String GET_DEPARTMENT_REQ_NAME = "GetDepartmentRequest";
    private static final String GET_DEPARTMENT_RESP_NAME = "GetDepartmentResponse";

    private static CoreService instance;
    private static CoreService instanceNonUI;

    private CoreService() {
    }

    public static CoreService getInstance() {
        if (instance == null) {
            instance = new CoreService();
        }
        instance.setEnableUI(true);
        return instance;
    }

    public static CoreService getInstanceNonUI() {
        if (instanceNonUI == null) {
            instanceNonUI = new CoreService();
        }
        instanceNonUI.setEnableUI(false);
        return instanceNonUI;
    }

    public List<Department> getDepartments(Context ctx) {

        SoapObject request = new SoapObject(NAMESPACE, GET_DEPARTMENT_REQ_NAME);
        request.addAttribute("fcsUserId", SessionContext.getInstance().getFcsUserId());
        request.addAttribute("wsSessionId", SessionContext.getInstance().getWsSessionId());

        SoapObject soap = callWebService(ctx, request, WEBSERVICE_URL_SUFFIX, NAMESPACE, GET_DEPARTMENT_RESP_NAME);

        List<Department> departments = new ArrayList<Department>();
        if (soap != null) {
            if (soap.getPropertyCount() > 0) {
                for (int i = 0; i < soap.getPropertyCount(); i++) {
                    departments.add(new Department((SoapObject) soap.getProperty(i)));
                }
            }
        }

        return departments;
    }

    public ModuleLicense getAuthorities(Context ctx) {

        SoapObject request = new SoapObject(NAMESPACE, GET_AUTHORITY_REQUEST);
        request.addAttribute("fcsUserId", SessionContext.getInstance().getFcsUserId());
        request.addAttribute("wsSessionId", SessionContext.getInstance().getWsSessionId());

        SoapObject soap = callWebService(ctx, request, WEBSERVICE_URL_SUFFIX, NAMESPACE, GET_AUTHORITY_RESPONSE);

        if (soap != null) {

//			List<Authority> authorities = new ArrayList<Authority>();
//			if (soap.getPropertyCount() > 0) {
//				for (int i = 0; i < soap.getPropertyCount(); i++) {
//					Authority authority = null;
//					try {
//						authority = new Authority((SoapObject) soap.getProperty(i));
//					} catch (Exception e) {
//					}
//					if (authority != null) {
//						authorities.add(authority);
//					}
//				}
//			}
//
//			ModuleLicense mod = new ModuleLicense();
//
//			List<UserAuthority> userAuthorities = new ArrayList<UserAuthority>();
//			for (Authority auth : authorities) {
//				Log.d(getClass().getSimpleName(), "name=" + auth.getFullname() + ", product=" + auth.getProduct()
//						+ ", type=" + auth.getType());
//				userAuthorities.add(auth.getUserAuthority());
//			}
//
//			HashSet<ProductEnum> productEnums = new HashSet<ProductEnum>();
//			HashSet<UserAuthority> authorityEnums = new HashSet<UserAuthority>();
//			for (UserAuthority auth : userAuthorities) {
//				ProductEnum product = auth.getProduct();
//				productEnums.add(product);
//				if (product.getParent() != null) {
//					productEnums.add(product.getParent());
//				}
//				authorityEnums.add(auth);
//			}
//
//			mod.setGuestRequest(authorityEnums.contains(EconnectAuthority.eCONNECT_JOB_CREATE));
//			mod.setNonGuestRequest(authorityEnums.contains(EconnectAuthority.eCONNECT_NON_IH));
//			mod.setInterDeptRequest(authorityEnums.contains(EconnectAuthority.eCONNECT_INTER_DEPT_CREATE));
//			if (mod.isGuestRequest() || mod.isNonGuestRequest() || mod.isInterDeptRequest()) {
//				mod.setJobRequest(true);
//			}
//			mod.setSendMessage(authorityEnums.contains(AdHocMessageAuthority.AD_HOC_ADD));
//			mod.setGuestDetail(authorityEnums.contains(EconnectAuthority.eCONNECT_CONFIG_GR_LINK));
//			mod.setMyJob(true);
//			mod.setAdhoc(true);
//			mod.setViewJob(true);

            ModuleLicense mod = new ModuleLicense();
            mod.setGuestRequest(true);
            mod.setNonGuestRequest(true);
            mod.setInterDeptRequest(true);
            mod.setJobRequest(true);
            mod.setSendMessage(true);
            mod.setGuestDetail(true);
            mod.setMyJob(true);
            mod.setMyMessage(true);
            mod.setJobSearch(true);

            return mod;
        } else {
            return null;
        }

    }
}
