package com.fcscs.android.mconnectv3.common.ui;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.ConfigContext;
import com.fcscs.android.mconnectv3.common.McConstants;
import com.fcscs.android.mconnectv3.common.McEnums.ServiceType;
import com.fcscs.android.mconnectv3.common.SessionContext;
import com.fcscs.android.mconnectv3.common.util.McUtils;

public abstract class McBackgroundAsyncTask extends AsyncTask<Void, Void, Void> {

    private static final String TAG = McBackgroundAsyncTask.class.getSimpleName();

    private Context context;

    public McBackgroundAsyncTask(Context context) {
        this.context = context;
    }

    @Override
    protected Void doInBackground(Void... params) {
        try {
            doInBackground();
        } catch (Throwable e) {
            Log.w(TAG, "doInBackground failed", e);
        }
        return null;
    }

    public abstract void doInBackground();

    protected void toast(CharSequence info) {
        Toast.makeText(context, info, Toast.LENGTH_LONG).show();
    }

    protected void toast(int resId) {
        Toast.makeText(context, resId, Toast.LENGTH_LONG).show();
    }

    public boolean isEconnect() {
        return ServiceType.ECONNECT.equals(ConfigContext.getInstance().getServiceType());
    }

    public boolean isCConnect() {
        return ServiceType.CCONNECT.equals(ConfigContext.getInstance().getServiceType());
    }

    public SessionContext getSessionCtx() {
        return SessionContext.getInstance();
    }

    public ConfigContext getConfigCtx() {
        return ConfigContext.getInstance();
    }

    public Context getCurrentContext() {
        return this.context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        boolean allDisconnected = McUtils.isNetworkOff(context);
        if (allDisconnected && !McConstants.OFFLINE_MODE) {
            toast(R.string.network_connection_unavailable);
        }

    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        try {
            onPostExecute();
        } catch (Throwable e) {
            Log.w(TAG, "onPostExecute failed", e);
        }
    }

    public abstract void onPostExecute();

    public void exec() {
        execute();
    }

}
