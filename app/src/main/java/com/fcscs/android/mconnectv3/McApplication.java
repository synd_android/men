package com.fcscs.android.mconnectv3;

import android.app.Activity;
import android.app.Application;
import android.graphics.Bitmap;
import android.os.Environment;

//import com.fcs.demo.autogate.AutoUpdateApk;
//import com.fcs.fcsptt.util.PreferencesUtil;
import com.fcscs.android.mconnectv3.cache.DrawableCache;
import com.fcscs.android.mconnectv3.cache.ObjectCache;
import com.fcscs.android.mconnectv3.common.McConstants;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.common.util.PrefsUtil;

import java.io.File;
import java.util.Observer;

public class McApplication extends Application {

    public static String PACKAGE_NAME;
    public static File DATA_DIRECTORY;

    private boolean isLogin = false;
    private DrawableCache drawableCache = null;
    private ObjectCache sessionCache = null;
    private ObjectCache appCache = null;
    private boolean logoutProcessed = true;
    private boolean redirect = false;

    public static McApplication application = null;
    private static Bitmap cachedPicture = null;


    @Override
    public void onCreate() {
        super.onCreate();
        application = this;

        PACKAGE_NAME = getApplicationContext().getPackageName();
        DATA_DIRECTORY = new File(Environment.getExternalStorageDirectory().getAbsolutePath().concat("/Android/data/" + PACKAGE_NAME));

        drawableCache = new DrawableCache(50);
        sessionCache = new ObjectCache(50);
        appCache = new ObjectCache(20);

        if (PrefsUtil.isDebugMode(this)) {
            McConstants.ENABLE_DEBUG_MODE = true;
        } else {
            McConstants.ENABLE_DEBUG_MODE = false;
        }

        if (PrefsUtil.isOfflineMode(this)) {
            McConstants.OFFLINE_MODE = true;
        } else {
            McConstants.OFFLINE_MODE = false;
        }
//        String urlUpdate = PreferencesUtil.getServerUpdateFullURL(this);
//        aua = new AutoUpdateApk(getApplicationContext(), urlUpdate);
        McUtils.deleteLog();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        drawableCache.clear();
        if (!isLogin) {
            sessionCache.clear();
        }
    }

    @Override
    public void onTerminate() {
        drawableCache.clear();
        sessionCache.clear();
        super.onTerminate();
        application = null;
    }

    public ObjectCache getSessionCache() {
        return sessionCache;
    }

    public DrawableCache getDrawableCache() {
        return drawableCache;
    }

    public boolean isLogin() {
        return isLogin;
    }

    public void setLogin(boolean isLogin) {
        this.isLogin = isLogin;
    }

    public ObjectCache getAppCache() {
        return appCache;
    }

    public boolean isLogoutProcessed() {
        return logoutProcessed;
    }

    public void setLogoutProcessed(boolean logoutProcessed) {
        this.logoutProcessed = logoutProcessed;
    }

    public boolean isRedirect() {
        return redirect;
    }

    public void setRedirect(boolean redirect) {
        this.redirect = redirect;
    }

    public static Bitmap getCachedPicture() {
        return cachedPicture;
    }

    public static void setCachedPicture(Bitmap cachedPicture) {
        if (McApplication.cachedPicture != null) {
            McApplication.cachedPicture.recycle();
        }
        McApplication.cachedPicture = cachedPicture;
    }

//    private AutoUpdateApk aua;
//
//    @Override
//    public void addObserver(Observer observer) {
//        aua.addObserver(observer);
//    }
//
//    @Override
//    public void removeObserver(Observer observer) {
//        aua.deleteObserver(observer);
//    }
//
//    @Override
//    public void installApplication() {
//        aua.InstallApplication();
//    }
//
//    @Override
//    public void checkUpdatesManually() {
//        aua.checkUpdatesManually(PreferencesUtil.getServerUpdateTime(this));
//    }
//
//    @Override
//    public void showDialog(Activity activity) {
//        aua.showDialog(activity);
//    }
}
