package com.fcscs.android.mconnectv3.common.util;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.lang.Thread.UncaughtExceptionHandler;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.kxml2.io.KXmlSerializer;
import org.xmlpull.v1.XmlSerializer;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.net.Uri;
import android.os.Build;
import android.os.Debug;
import android.os.Environment;
import android.provider.MediaStore.Images.Media;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Adapter;
import android.widget.EditText;
import android.widget.ListView;

import com.fcscs.android.mconnectv3.McApplication;
import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.DatabaseHelper;
import com.fcscs.android.mconnectv3.common.McBase64;
import com.fcscs.android.mconnectv3.common.McConstants;
import com.fcscs.android.mconnectv3.common.McEnums.JobStatus;
import com.fcscs.android.mconnectv3.common.ui.McBackgroundAsyncTask;
import com.fcscs.android.mconnectv3.manager.model.Language;
import com.fcscs.android.mconnectv3.manager.model.LanguageResource;
import com.fcscs.android.mconnectv3.manager.model.LanguageDetail;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.GetJobSummaryResponse;
import com.fcscs.android.mconnectv3.ws.econnect.ECService;
import com.j256.ormlite.dao.RuntimeExceptionDao;

public class McUtils {

    private static final String TAG = McUtils.class.getSimpleName();
    public static Map<String, String> resourceStringMap = null;

    public static void updateResourceStringMap(Context ctx) {
        String code = PrefsUtil.getLanguageCode(ctx);
        resourceStringMap = new HashMap<String, String>();
        DatabaseHelper helper = DatabaseHelper.getInstance(ctx);
        RuntimeExceptionDao<LanguageResource, String> dao = helper.getLanguageResourceDao();
        List<LanguageResource> list = dao.queryForEq("code", code);
        for (LanguageResource r : list) {
            resourceStringMap.put(r.getName(), r.getValue());
        }
    }

    public static String getResourceString(Context ctx, String code, String name, int resourceId) {

        if (resourceStringMap == null || resourceStringMap.isEmpty()) {
            resourceStringMap = new HashMap<String, String>();
            DatabaseHelper helper = DatabaseHelper.getInstance(ctx);
            RuntimeExceptionDao<LanguageResource, String> dao = helper.getLanguageResourceDao();
            List<LanguageResource> list = dao.queryForEq("code", code);
            for (LanguageResource r : list) {
                resourceStringMap.put(r.getName(), r.getValue());
            }
        }

        String value = resourceStringMap.get(name);
        if (value == null || value.trim().isEmpty()) {
            value = ctx.getResources().getString(resourceId);
        }

        return value;
    }

    public static String formatShortDateTime(Date date) {
        if (date == null) {
            return "";
        }
        Locale locale = PrefsUtil.getLocale();
        String fmt = "dd-MMM HH:mm";
        if (McApplication.application != null) {
            fmt = PrefsUtil.getShortDateTimeFormat(McApplication.application);
        }
        SimpleDateFormat sd = new SimpleDateFormat(fmt, locale);
        return sd.format(date);
    }

    public static String formatDateTime(Date date) {
        if (date == null) {
            return "";
        }
        Locale locale = PrefsUtil.getLocale();
        String fmt = DateTimeHelper.FORMAT_DATETIME_FULL_DASH;
        if (McApplication.application != null) {
            fmt = PrefsUtil.getDateTimeFormat(McApplication.application);
        }
        SimpleDateFormat sd = new SimpleDateFormat(fmt, locale);
        return sd.format(date);
    }

    public static boolean isEnterAction(int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_NULL || actionId == EditorInfo.IME_ACTION_DONE
                || actionId == EditorInfo.IME_ACTION_GO || actionId == EditorInfo.IME_ACTION_NEXT
                || actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_SEND) {
            if (event == null || event.getAction() == KeyEvent.ACTION_DOWN) {
                return true;
            }
        }
        return false;
    }

    public static boolean equalStr(boolean caseSensitive, String one, String two) {
        if (one == two) {
            return true;
        } else if (one == null || two == null) {
            return false;
        }
        if (caseSensitive) {
            return one.equals(two);
        } else {
            return one.equalsIgnoreCase(two);
        }
    }

//	public static String formatShortDateTime(Date date) {
//		if (date == null) {
//			return "";
//		}
//		Locale locale = PrefsUtil.getLocale();
//		SimpleDateFormat sd = new SimpleDateFormat("dd MMM HH:mm", locale);
//		return sd.format(date);
//	}

    public static String formatShortDate(Date date) {
        if (date == null) {
            return "";
        }
        Locale locale = PrefsUtil.getLocale();
        String fmt = "dd-MMM";
        if (McApplication.application != null) {
            fmt = PrefsUtil.getShortDateFormat(McApplication.application);
        }
        SimpleDateFormat sd = new SimpleDateFormat(fmt, locale);
        return sd.format(date);
    }

    public static String formatDate(Date date) {
        if (date == null) {
            return "";
        }
        Locale locale = PrefsUtil.getLocale();
        String fmt = DateTimeHelper.FORMAT_DATE_DASH;
        if (McApplication.application != null) {
            fmt = PrefsUtil.getDateFormat(McApplication.application);
        }
        SimpleDateFormat sd = new SimpleDateFormat(fmt, locale);
        return sd.format(date);
    }

    public static String convertToString(Object obj) {
        if (obj == null) {
            return null;
        }
        return obj.toString();
    }

    public static int getScreenResolutionHeight(Activity act) {
        DisplayMetrics dm = new DisplayMetrics();
        act.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.heightPixels;
    }

    public static int getScreenResolutionWidth(Activity act) {
        DisplayMetrics dm = new DisplayMetrics();
        act.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.widthPixels;
    }

    public static String getAndroidOSVersion() {
        //		return "" + android.os.Build.VERSION.SDK_INT;
        return android.os.Build.VERSION.RELEASE;
    }

    public static String getGoogleAccountUsername(Context ctx) {
        AccountManager manager = AccountManager.get(ctx);
        Account[] accounts = manager.getAccountsByType("com.google");
        List<String> possibleEmails = new LinkedList<String>();

        for (Account account : accounts) {
            possibleEmails.add(account.name);
        }

        if (!possibleEmails.isEmpty() && possibleEmails.get(0) != null) {
            String email = possibleEmails.get(0);
            String[] parts = email.split("@");
            if (parts.length > 0 && parts[0] != null) {
                return email;
            }
        }
        return "";
    }

    private static final InputFilter normalInputFilter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            return null;
        }
    };

    private static final InputFilter disabledInputFilter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence src, int start, int end, Spanned dst, int dstart, int dend) {
            return src.length() < 1 ? dst.subSequence(dstart, dend) : "";
        }
    };

    public static void enableEditText(EditText et, boolean enable) {
        if (enable) {
            et.setFilters(new InputFilter[]{normalInputFilter});
            et.setEnabled(true);
        } else {
            et.setFilters(new InputFilter[]{disabledInputFilter});
            et.setEnabled(false);
        }
    }

    public static boolean isNullOrEmpty(Object str) {
        if (str == null) {
            return true;
        }
        if (str instanceof String && str.toString().trim().equals("")) {
            return true;
        }
        return false;
    }

    public static Integer getStatusID(GetJobSummaryResponse resp, JobStatus status) {

        if (resp == null || resp.getStatusIdMap() == null || status == null) {
            return null;
        }

        return resp.getStatusIdMap().get(status);
    }

    public static String getLocalIpAddress(boolean preferIpv4) {
        try {
            Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
            while (en.hasMoreElements()) {
                NetworkInterface i = (NetworkInterface) en.nextElement();
                for (Enumeration<InetAddress> en2 = i.getInetAddresses(); en2.hasMoreElements(); ) {
                    InetAddress addr = (InetAddress) en2.nextElement();
                    if (!addr.isLoopbackAddress()) {
                        if (addr instanceof Inet4Address) {
                            if (!preferIpv4) {
                                continue;
                            }
                            return addr.getHostAddress().toString();
                        }
                        if (addr instanceof Inet6Address) {
                            if (preferIpv4) {
                                continue;
                            }
                            return addr.getHostAddress().toString();
                        }
                    }
                }
            }
            return null;
        } catch (SocketException ex) {
            Log.e("getLocalIpAddress", ex.toString());
        }
        return null;
    }

    public static boolean expectedSoapObject(Object object, String namespace, String name) {

        if (object == null || !(object instanceof SoapObject)) {
            return false;
        }

        SoapObject soap = (SoapObject) object;
        if (soap.getNamespace().equals(namespace) && soap.getName().equals(name)) {
            return true;
        }
        return false;
    }

    public static int getMessageCount(String detail) {
        int charCount = 0;
        String[] strs = detail.split("\\t|\r|\n");
        for (String str : strs) {
            charCount += str.length();
        }
        return charCount;
    }

    public static void hideIME(Activity activity) {
        View focus = activity.getCurrentFocus();
        if (focus != null) {
            InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(focus.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

//	public static UserAuthority getUserAuthority(String name, String type) {
//		try {
//			Class<?> cls = Class.forName("com.fcscs.core.common.authority." + type);
//			Method method = cls.getMethod("values");
//			UserAuthority[] userAuthos = (UserAuthority[]) method.invoke(null);
//			for (UserAuthority userAuth : userAuthos) {
//				if (userAuth.getAuthority().equals(name)) {
//					return userAuth;
//				}
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return null;
//	}

    public static String getECServiceLang() {
        Locale locale = PrefsUtil.getLocale();
        if (Locale.TAIWAN.equals(locale)) {
            return "cht";
        } else if (Locale.US.equals(locale)) {
            return "eng";
        } else if (Locale.CHINA.equals(locale)) {
            return "chs";
        }
        return "eng"; //Default
    }

    public static boolean isNetworkOff(Context ctx) {

        ConnectivityManager mgr = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        State connected = NetworkInfo.State.CONNECTED;
        State connecting = NetworkInfo.State.CONNECTING;
        boolean allDisconnected = true;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Network[] networks = mgr.getAllNetworks();
            NetworkInfo ni;
            for (Network mNetwork : networks) {
                ni = mgr.getNetworkInfo(mNetwork);
                if (ni == null) {
                    continue;
                }
                if (ni.isConnectedOrConnecting() == true) {
                    allDisconnected = false;
                }
            }
        } else {
            NetworkInfo[] infos = mgr.getAllNetworkInfo();
            if (infos != null) {
                for (NetworkInfo ni : infos) {
                    if (ni == null) {
                        continue;
                    }
                    if (ni.isConnectedOrConnecting() == true) {
                        allDisconnected = false;
                    }
                }
            }
        }
        return allDisconnected;
    }

//	public static boolean isNetworkOff(Context ctx) {
//
//		ConnectivityManager mgr = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
//		State connected = NetworkInfo.State.CONNECTED;
//		State connecting = NetworkInfo.State.CONNECTING;
//
//		NetworkInfo[] infos = mgr.getAllNetworkInfo();
//
//		boolean allDisconnected = true;
//		if (infos != null) {
//			for (NetworkInfo ni : infos) {
//				if (ni == null) {
//					continue;
//				}
//				if (ni.getState() == connected || ni.getState() == connecting) {
//					allDisconnected = false;
//				}
//			}
//		}
//		return allDisconnected;
//	}

    public static boolean isSDCardExist() {
        return android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
    }

    public static Map<String, String> collectDeviceInfo(Context ctx) {

        Map<String, String> infos = new TreeMap<String, String>();
//		try {
//			PackageManager pm = ctx.getPackageManager();
//			PackageInfo pi = pm.getPackageInfo(ctx.getPackageName(), PackageManager.GET_ACTIVITIES);
//			if (pi != null) {
//				String versionName = pi.versionName == null ? "null" : pi.versionName;
//				String versionCode = pi.versionCode + "";
//
//				infos.put("versionName", versionName);
//				infos.put("versionCode", versionCode);
//			}
//		} catch (NameNotFoundException e) {
//			Log.e(TAG, "an error occured when collect package info", e);
//		}

        infos.put("versionName", McConstants.APP_VERSION_NAME);
        infos.put("versionCode", "");
        Field[] fields = Build.class.getDeclaredFields();
        for (Field field : fields) {
            try {
                field.setAccessible(true);
                infos.put(field.getName(), field.get(null).toString());
                Log.d(TAG, field.getName() + " : " + field.get(null));
            } catch (Exception e) {
                Log.e(TAG, "an error occured when collect crash info", e);
            }
        }
        return infos;
    }

    public static String saveCrashReport(Context context, Throwable e) {

        BufferedWriter writer = null;
        try {
            if (!isSDCardExist()) {
                return null;
            }

            // === print stack trace
            String stacktrace = getStacktrace(e);

            // === collect device info
            StringBuffer sb = new StringBuffer();
            Map<String, String> infos = collectDeviceInfo(context);
            for (Map.Entry<String, String> entry : infos.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                sb.append(key + "=" + value + "\n");
            }

            String log = "\n" + getTimestamp() + "\n";
            log += stacktrace + "\n";
            log += sb.toString() + "\n";

            File file = getLogFile();
            writer = new BufferedWriter(new FileWriter(file, true));
            writer.write(log);

            return log;

        } catch (Exception e2) {
            e2.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (Exception e3) {
                }
            }
        }

        return null;
    }

    public static void saveUncaughtException(Context context, UncaughtExceptionHandler handler, Thread t, Throwable e) {
        String log = saveCrashReport(context, e);
        if (log != null) {
            new SendCrashReport(context, log, handler, t, e).exec();
        }
    }

    public static class SendCrashReport extends McBackgroundAsyncTask {

        private String message;
        private Context context;
        private Thread thread;
        private UncaughtExceptionHandler handler;
        private Throwable ex;

        public SendCrashReport(Context context, String message, UncaughtExceptionHandler handler, Thread thread, Throwable ex) {
            super(context);
            this.message = message;
            this.context = context;
            this.thread = thread;
            this.handler = handler;
            this.ex = ex;
        }

        @Override
        public void doInBackground() {
            Boolean result = ECService.getInstance().postWSLog(context, message, 2);
            Log.i(TAG, "sent crash report. result=" + result);
            if (handler != null) {
                handler.uncaughtException(thread, ex);
            }
        }

        @Override
        public void onPostExecute() {
        }

    }

    public static void saveLogToSDCard(String content) {


    }

    public static void saveSoapToSDCard(String url, SoapObject request, String response, Throwable e) {
        try {

            if (!isSDCardExist()) {
                return;
            }

            File file = getLogFile();
            BufferedWriter writer = new BufferedWriter(new FileWriter(file, true));
            writer.write("\n" + getTimestamp() + "\n");
            writer.write(url);
            writer.write("\n");

            String req = null;
            if (request != null) {
                req = new String(createRequestData(request));
            }
            if (req != null) {
                writer.write(req);
                writer.write("\n");
            }

            if (response != null) {
                writer.write(getTimestamp() + "\n");
                writer.write(response);
                writer.write("\n");
            }

            String error = getStacktrace(e);
            if (error != null) {
                writer.write(error);
                writer.write("\n");
            }
            writer.close();

        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static void deleteLog() {
        try {
            if (!isSDCardExist()) {
                return;
            }
            File file = getLogFile();
            File dir = file.getParentFile();

            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DAY_OF_MONTH, -7);

            for (File f : dir.listFiles()) {
                if (f.lastModified() < cal.getTimeInMillis()) {
                    f.delete();
                }
            }

            Log.d(TAG, "remove old files");

        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private static byte[] createRequestData(SoapObject request) throws IOException {

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);
        envelope.setAddAdornments(false);

        ByteArrayOutputStream bos = new ByteArrayOutputStream(262144);
        byte[] result = null;
        XmlSerializer xw = new KXmlSerializer();
        xw.setOutput(bos, "UTF-8");
        envelope.write(xw);
        xw.flush();
        bos.write(13);
        bos.write(10);
        bos.flush();
        result = bos.toByteArray();
        xw = null;
        bos = null;
        return result;
    }

    private static String getStacktrace(Throwable e) {

        if (e == null) {
            return null;
        }

        final Writer result = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(result);
        e.printStackTrace(printWriter);
        Throwable ca = e.getCause();
        while (ca != null) {
            ca.printStackTrace(printWriter);
            ca = ca.getCause();
        }
        String stacktrace = result.toString();
        printWriter.close();
        return stacktrace;
    }

    private static File getLogFile() {

        SimpleDateFormat df = new SimpleDateFormat(DateTimeHelper.FORMAT_DATEYEAR_NON);
        String timestamp = df.format(DateTimeHelper.getDateByTimeZone(new Date()));

        String path = McConstants.LOG_OUTPUT_LOCATION + File.separator + timestamp + ".log";

        File file = new File(path);
        if (file.getParentFile().exists() == false) {
            file.getParentFile().mkdirs();
        }

        return file;
    }

    private static String getTimestamp() {

        SimpleDateFormat df = new SimpleDateFormat(DateTimeHelper.FORMAT_DATETIME_DASH);
        String timestamp = df.format(DateTimeHelper.getDateByTimeZone(new Date()));

        return timestamp;
    }

//	public static Bitmap decodeBitmap(Context mContext, File file, int reqWidth, int reqHeight) {
//
//		Bitmap resizedBitmap = null;
//
//		try {
//
//			final BitmapFactory.Options options = new BitmapFactory.Options();
//			options.inJustDecodeBounds = true;
//			BitmapFactory.decodeFile(file.getAbsolutePath(), options);
//
//			final int height = options.outHeight;
//			final int width = options.outWidth;
//			int inSampleSize = 1;
//			while ((height / inSampleSize) > reqHeight || (width / inSampleSize) > reqWidth) {
//				inSampleSize *= 2;
//			}
//
//			options.inSampleSize = inSampleSize;
//			options.inJustDecodeBounds = false;
//			Bitmap decodeFile = BitmapFactory.decodeFile(file.getAbsolutePath(), options);
//			System.gc();
//			logHeap();
//
//			return decodeFile;
//
//		} catch (Exception e) {
//			Log.e(TAG, "getResizedBitmap", e);
//			saveCrashReport(mContext, e);
//		} catch (OutOfMemoryError e) {
//			Log.e(TAG, "getResizedBitmap", e);
//			saveCrashReport(mContext, e);
//			System.gc();
//		}
//		return resizedBitmap;
//	}

    public static Bitmap decodeBitmap(Context mContext, Uri uri, int reqWidth, int reqHeight) {

        Bitmap resizedBitmap = null;

        try {

            InputStream is = mContext.getContentResolver().openInputStream(uri);
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(is, null, options);
            is.close();

            final int height = options.outHeight;
            final int width = options.outWidth;
            int inSampleSize = 1;
            while ((height / inSampleSize) > reqHeight || (width / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }

            options.inSampleSize = inSampleSize;
            options.inJustDecodeBounds = false;

            is = mContext.getContentResolver().openInputStream(uri);
            Bitmap decodeFile = BitmapFactory.decodeStream(is, null, options);
            is.close();

            System.gc();
            logHeap();

            return decodeFile;

        } catch (Exception e) {
            Log.e(TAG, "getResizedBitmap", e);
            saveCrashReport(mContext, e);
        } catch (OutOfMemoryError e) {
            Log.e(TAG, "getResizedBitmap", e);
            saveCrashReport(mContext, e);
            System.gc();
        }
        return resizedBitmap;
    }


    @SuppressLint("UseValueOf")
    public static void logHeap() {

        Double allocated = new Double(Debug.getNativeHeapAllocatedSize()) / new Double((1048576));
        Double available = new Double(Debug.getNativeHeapSize()) / 1048576.0;
        Double free = new Double(Debug.getNativeHeapFreeSize()) / 1048576.0;
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(3);
        df.setMinimumFractionDigits(3);

        Log.d(TAG, "Native heap: allocated=" + df.format(allocated) + "MB");
        Log.d(TAG, "Native heap: free=" + df.format(free) + "MB");
        Log.d(TAG, "Native heap: available=" + df.format(available) + "MB");

        String total = df.format(new Double(Runtime.getRuntime().totalMemory() / 1048576));
        String max = df.format(new Double(Runtime.getRuntime().maxMemory() / 1048576));
        String freem = df.format(new Double(Runtime.getRuntime().freeMemory() / 1048576));

        Log.d(TAG, "Memory: total=" + total + "MB");
        Log.d(TAG, "Memory: max=" + max + "MB");
        Log.d(TAG, "Memory: free=" + freem + "MB");
    }

//	private Bitmap getBitmap(Context mContext, File file) {
//
//		Uri uri = Uri.fromFile(file);
//		InputStream in = null;
//		try {
//			
//			final int IMAGE_MAX_SIZE;			
//			if (VERSION.SDK_INT < 14) {
//				IMAGE_MAX_SIZE = 800000;
//			} else {
//				IMAGE_MAX_SIZE = 1200000;
//			}
//			
//			in = mContext.getContentResolver().openInputStream(uri);
//
//			// Decode image size
//			BitmapFactory.Options o = new BitmapFactory.Options();
//			o.inJustDecodeBounds = true;
//			BitmapFactory.decodeStream(in, null, o);
//			in.close();
//
//			int scale = 1;
//			while ((o.outWidth * o.outHeight) * (1 / Math.pow(scale, 2)) > IMAGE_MAX_SIZE) {
//				scale++;
//			}
//			Log.d(TAG, "scale = " + scale + ", orig-width: " + o.outWidth + ", orig-height: " + o.outHeight);
//
//			Bitmap b = null;
//			in = mContext.getContentResolver().openInputStream(uri);
//			if (scale > 1) {
//				scale--;
//				// scale to max possible inSampleSize that still yields an image
//				// larger than target
//				o = new BitmapFactory.Options();
//				o.inSampleSize = scale;
//				b = BitmapFactory.decodeStream(in, null, o);
//
//				// resize to desired dimensions
//				int height = b.getHeight();
//				int width = b.getWidth();
//				
//				Log.d(TAG, "1th scale operation dimenions - width: " + width + ", height: " + height);
//
//				double y = Math.sqrt(IMAGE_MAX_SIZE / (((double) width) / height));
//				double x = (y / height) * width;
//
//				Bitmap scaledBitmap = Bitmap.createScaledBitmap(b, (int) x, (int) y, true);
//				b.recycle();
//				b = scaledBitmap;
//
//				
//			} else {
//				b = BitmapFactory.decodeStream(in);
//			}
//			in.close();
//
//			System.gc();
//			Log.d(TAG, "bitmap size - width: " + b.getWidth() + ", height: " + b.getHeight());
//			return b;
//		} catch (IOException e) {
//			Log.e(TAG, e.getMessage(), e);
//			return null;
//		}
//	}

//	public static Bitmap resizeBitmap(Context mContext, File file, int maxWidth, int maxHeight) {
//
//		Bitmap resizedBitmap = null;
//
//		try {
//
//			FileInputStream in = new FileInputStream(file);
//			BitmapFactory.Options o = new BitmapFactory.Options();
//			o.inSampleSize = 1;
//			Bitmap bm = BitmapFactory.decodeStream(in, null, o);
//			in.close();
//
//			if (maxWidth == 0 && maxHeight == 0) {
//				return bm;
//			}
//
//			int width = bm.getWidth();
//			int height = bm.getHeight();
//
//			int scalew = width;
//			int scaleh = height;
//
//			if (maxWidth > 0 && width > maxWidth) {
//				scalew = maxWidth;
//				scaleh = (maxWidth * height) / width;
//			}
//
//			if (maxHeight > 0 && scaleh > maxHeight) {
//				scaleh = maxHeight;
//				scalew = (maxHeight * width) / height;
//			}
//
//			float scaleWidth = ((float) scalew) / width;
//			float scaleHeight = ((float) scaleh) / height;
//
//			Matrix matrix = new Matrix();
//			matrix.postScale(scaleWidth, scaleHeight);
//			resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
//			bm.recycle();
//
//			Log.d(TAG, "resize bitmap=" + resizedBitmap.getWidth() + "x" + resizedBitmap.getHeight());
//
//		} catch (Exception e) {
//			Log.e(TAG, "getResizedBitmap", e);
//			saveCrashReport(mContext, e);
//		} catch (OutOfMemoryError e) {
//			Log.e(TAG, "getResizedBitmap", e);
//			saveCrashReport(mContext, e);
//			System.gc();
//		}
//		return resizedBitmap;
//	}

//	public static Bitmap resizeBitmap(Context mContext, Bitmap bm, int maxWidth, int maxHeight) {
//
//		Bitmap resizedBitmap = null;
//
//		try {
//
//			if (maxWidth == 0 && maxHeight == 0) {
//				return bm;
//			}
//
//			int width = bm.getWidth();
//			int height = bm.getHeight();
//
//			int scalew = width;
//			int scaleh = height;
//
//			if (maxWidth > 0 && width > maxWidth) {
//				scalew = maxWidth;
//				scaleh = (maxWidth * height) / width;
//			}
//
//			if (maxHeight > 0 && scaleh > maxHeight) {
//				scaleh = maxHeight;
//				scalew = (maxHeight * width) / height;
//			}
//
//			float scaleWidth = ((float) scalew) / width;
//			float scaleHeight = ((float) scaleh) / height;
//
//			Matrix matrix = new Matrix();
//			matrix.postScale(scaleWidth, scaleHeight);
//			resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
//			Log.d(TAG, "resize bitmap=" + resizedBitmap.getWidth() + "x" + resizedBitmap.getHeight());
//
//		} catch (Exception e) {
//			Log.e(TAG, "getResizedBitmap", e);
//			saveCrashReport(mContext, e);
//		} catch (OutOfMemoryError e) {
//			Log.e(TAG, "getResizedBitmap", e);
//			saveCrashReport(mContext, e);
//			System.gc();
//		}
//		return resizedBitmap;
//	}

    public static String encodeTobase64(Context mContext, Bitmap image) {
        try {

            Log.d(TAG, "Before encoding. Width=" + image.getWidth() + ", height=" + image.getHeight());

            Bitmap immagex = image;
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] b = baos.toByteArray();
            String imageEncoded = McBase64.encodeBytes(b);

            return imageEncoded;
        } catch (Exception e) {
            Log.e(TAG, "encodeTobase64", e);
            saveCrashReport(mContext, e);
        } catch (OutOfMemoryError e) {
            Log.e(TAG, "encodeTobase64", e);
            saveCrashReport(mContext, e);
            System.gc();
        }
        return null;
    }

    public static File decodeToFile(Context mContext, String base64, String ext) {
        if (isNullOrEmpty(base64)) {
            return null;
        }
        try {
            File dir = mContext.getExternalCacheDir();
            SimpleDateFormat df = new SimpleDateFormat(DateTimeHelper.FORMAT_DATETIME_NON);
            if (ext == null) {
                ext = "tmp";
            }
            File file = new File(dir, String.format("%s.%s", df.format(new Date()), ext));
            byte[] bytes = Base64.decode(base64, Base64.DEFAULT);
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(bytes);
            fos.close();
            return file;
        } catch (Exception e) {
            Log.e(TAG, "encodeTobase64", e);
            saveCrashReport(mContext, e);
        } catch (OutOfMemoryError e) {
            Log.e(TAG, "encodeTobase64", e);
            saveCrashReport(mContext, e);
            System.gc();
        }
        return null;
    }

    public static String encodeFileToBase64(File file) {

        if (file == null || file.exists() == false || file.length() <= 0) {
            return "";
        }
        Log.d(TAG, "Start encoding file=" + file.getAbsolutePath());

        InputStream inputStream = null;//You can get an inputStream using any IO API
        byte[] bytes;
        byte[] buffer = new byte[8192];
        try {
            inputStream = new FileInputStream(file);
            int bytesRead;
            long start = System.currentTimeMillis();
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            try {
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    output.write(buffer, 0, bytesRead);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            bytes = output.toByteArray();
            String encodedString = Base64.encodeToString(bytes, Base64.DEFAULT);
            Log.d(TAG, String.format("encoding time:%sms", (System.currentTimeMillis() - start)));
            return encodedString;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getExtension(File file) {
        String ext = null;
        String s = file.getName();
        int i = s.lastIndexOf('.');
        if (i > 0 && i < s.length() - 1) {
            ext = s.substring(i + 1).toLowerCase();
        }
        return ext;
    }

    public static boolean storeImage(Context mContext, Uri src, File dest) {
        Bitmap bm = null;
        try {
            bm = Media.getBitmap(mContext.getContentResolver(), src);
            int r = bm.getWidth() * bm.getHeight();
            int quality = 100;
            if (r > 1280 * 1024) {
                quality = 85;
            }
            FileOutputStream out = new FileOutputStream(dest);
            bm.compress(Bitmap.CompressFormat.JPEG, quality, out);
            bm.recycle();
            System.gc();

            return true;
        } catch (Exception e) {
            Log.e(TAG, "storeImage", e);
            saveCrashReport(mContext, e);
        } catch (OutOfMemoryError e) {
            Log.e(TAG, "storeImage", e);
            saveCrashReport(mContext, e);
            System.gc();
        }
        return false;
    }

    public static void adjustListViewHeight(ListView listView, Adapter adapter) {
        int totalHeight = 0;
        for (int i = 0, len = adapter.getCount(); i < len; i++) {
            View listItem = adapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (adapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    public static List<Language> getLanguageList(Context context) {
        List<Language> languages = new ArrayList<Language>();

        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        RuntimeExceptionDao<LanguageDetail, String> dao = helper.getLanguageModelDao();
        List<LanguageDetail> languageDetails = dao.queryForAll();
        for (LanguageDetail d : languageDetails) {
            Language propLang = new Language();
            if (d.getCode().equalsIgnoreCase("ENG")) {
                propLang.setName("English");
                propLang.setCode("en_US");
                propLang.setLocale(Locale.US);
                languages.add(propLang);
            } else if (d.getCode().equalsIgnoreCase("CHS")) {
                propLang.setName(context.getString(R.string.language_zh_cn));
                propLang.setCode("zh_CN");
                propLang.setLocale(Locale.CHINA);
                languages.add(propLang);
            } else if (d.getCode().equalsIgnoreCase("CHT")) {
                propLang.setName(context.getString(R.string.language_zh_tw));
                propLang.setCode("zh_TW");
                propLang.setLocale(Locale.TAIWAN);
                languages.add(propLang);
            } else if (d.getCode().equalsIgnoreCase("JPN")) {
                Locale locale = Locale.JAPAN;
                propLang.setName(context.getString(R.string.language_ja_jp));
                propLang.setCode("ja_JP");
                propLang.setLocale(locale);
                languages.add(propLang);
            } else if (d.getCode().equalsIgnoreCase("KOR")) {
                Locale locale = new Locale("ko", "KR");
                propLang.setName(context.getString(R.string.language_ko_kr));
                propLang.setCode("ko_KR");
                propLang.setLocale(locale);
                languages.add(propLang);
            } else if (d.getCode().equalsIgnoreCase("THA")) {
                Locale locale = new Locale("th", "TH");
                propLang.setName(context.getString(R.string.language_th_th));
                propLang.setCode("th_TH");
                propLang.setLocale(locale);
                languages.add(propLang);
            } else if (d.getCode().equalsIgnoreCase("FRA")) {
                Locale locale = new Locale("fr", "FR");
                propLang.setName(context.getString(R.string.language_fr_fr));
                propLang.setCode("fr_FR");
                propLang.setLocale(locale);
                languages.add(propLang);
            } else if (d.getCode().equalsIgnoreCase("SPA")) {
                Locale locale = new Locale("es", "ES");
                propLang.setName(context.getString(R.string.language_es_es));
                propLang.setCode("es_ES");
                propLang.setLocale(locale);
                languages.add(propLang);
            }
        }

        if (languages.isEmpty()) {
            languages = getDefaultLanguageList(context);
        }

        return languages;
    }

    public static List<Language> getDefaultLanguageList(Context context) {
        List<Language> languages = new ArrayList<Language>();

        Language propLang = new Language();
        propLang.setName("English");
        propLang.setCode("en_US");
        propLang.setLocale(Locale.US);

        languages.add(propLang);

        propLang = new Language();
        propLang.setName(context.getString(R.string.language_zh_cn));
        propLang.setCode("zh_CN");
        propLang.setLocale(Locale.CHINA);
        languages.add(propLang);

        propLang = new Language();
        propLang.setName(context.getString(R.string.language_zh_tw));
        propLang.setCode("zh_TW");
        propLang.setLocale(Locale.TAIWAN);
        languages.add(propLang);

        propLang = new Language();
        Locale locale = Locale.JAPAN;
        propLang.setName(context.getString(R.string.language_ja_jp));
        propLang.setCode("ja_JP");
        propLang.setLocale(locale);
        languages.add(propLang);

        propLang = new Language();
        locale = new Locale("ko", "KR");
        propLang.setName(context.getString(R.string.language_ko_kr));
        propLang.setCode("ko_KR");
        propLang.setLocale(locale);
        languages.add(propLang);

        propLang = new Language();
        locale = new Locale("th", "TH");
        propLang.setName(context.getString(R.string.language_th_th));
        propLang.setCode("th_TH");
        propLang.setLocale(locale);
        languages.add(propLang);

        propLang = new Language();
        locale = new Locale("fr", "FR");
        propLang.setName(context.getString(R.string.language_fr_fr));
        propLang.setCode("fr_FR");
        propLang.setLocale(locale);
        languages.add(propLang);

        propLang = new Language();
        locale = new Locale("es", "ES");
        propLang.setName(context.getString(R.string.language_es_es));
        propLang.setCode("es_ES");
        propLang.setLocale(locale);
        languages.add(propLang);

        return languages;
    }

    public static void copy(File src, File dst) {
        InputStream in = null;
        OutputStream out = null;
        try {
            in = new FileInputStream(src);
            out = new FileOutputStream(dst);

            // Transfer bytes from in to out
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                }
            }
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                }
            }
        }
    }

    public static Bitmap decodeSampledBitmapFromResource(File image, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
//        BitmapFactory.decodeResource(res, resId, options);
        BitmapFactory.decodeFile(image.getAbsolutePath(), options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(image.getAbsolutePath(), options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static String formatCounter(int counter) {
        int second = counter % 60;
        int minute = counter / 60;
        return String.format("%02d:%02d", minute, second);
    }

    public static String getMacAddr() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    String zeroPad = "00";
                    String bstr1 = Integer.toHexString(b & 0xFF);
                    String bstr2 = zeroPad.substring(bstr1.length()) + bstr1.toUpperCase();
                    res1.append(bstr2 + ":");
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
            //handle exception
        }
        return "1";
    }

    public static String getPathDownload(Context context, String url) {
        String[] separated = url.split("/");
        String result = separated[separated.length - 1];
        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        File folder = new File(extStorageDirectory, "pdf");
        folder.mkdir();
        String pathFile = folder + "/" + result;
        return pathFile;
    }

    public static File getPDFFileByUrl(Context context, String url) {
        String[] separated = url.split("/");
        String result = separated[separated.length - 1];
        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        File folder = new File(extStorageDirectory, "pdf");
        folder.mkdir();
        String file = folder + "/" + result;
        File filePDF = new File(file);
        try {
            filePDF.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return filePDF;
    }

    public static boolean fileExists(Context context, String fileUrl) {
        String[] separated = fileUrl.split("/");
        String result = separated[separated.length - 1];

        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        File folder = new File(extStorageDirectory, "pdf");
        folder.mkdir();
        String file = folder + "/" + result;
        File f = new File(file);
        Log.i("filePath", f.getPath());
        if (f.exists())
            return true;
        return false;
    }

    public static void downloadFile(String fileUrl, File directory) {
        try {
            final int MEGABYTE = 1024 * 1024;
            URL url = new URL(fileUrl);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            //urlConnection.setRequestMethod("GET");
            //urlConnection.setDoOutput(true);
            urlConnection.connect();

            InputStream inputStream = urlConnection.getInputStream();
            FileOutputStream fileOutputStream = new FileOutputStream(directory);
            int totalSize = urlConnection.getContentLength();

            byte[] buffer = new byte[MEGABYTE];
            int bufferLength = 0;
            while ((bufferLength = inputStream.read(buffer)) > 0) {
                fileOutputStream.write(buffer, 0, bufferLength);
            }
            fileOutputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Bitmap rotateImage(Bitmap bitmap, int degree) {
        Matrix matrix = new Matrix();
        Bitmap rotated = null;
        if (bitmap != null) {
            matrix.postRotate(degree);
            rotated = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(),
                    matrix, true);
        }
        return rotated;
    }

}
