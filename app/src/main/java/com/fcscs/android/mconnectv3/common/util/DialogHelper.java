package com.fcscs.android.mconnectv3.common.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.fcscs.android.mconnectv3.R;

public class DialogHelper {

    public static void showYesNoDialog(Context mContext, int messageResId, DialogInterface.OnClickListener yesListener) {
        new AlertDialog.Builder(mContext).setIcon(android.R.drawable.ic_dialog_alert).setTitle(R.string.confirm).setMessage(messageResId)
                .setPositiveButton(R.string.yes, yesListener).setNegativeButton(R.string.no, null).show();
    }

    public static void showYesNoDialog(Context mContext, int messageResId, DialogInterface.OnClickListener yesListener, DialogInterface.OnClickListener noListener) {
        new AlertDialog.Builder(mContext).setIcon(android.R.drawable.ic_dialog_alert).setTitle(R.string.confirm).setMessage(messageResId)
                .setPositiveButton(R.string.yes, yesListener).setNegativeButton(R.string.no, noListener).show();
    }

    public static void showYesNoDialog(Context mContext, int titleResId, int messageResId, DialogInterface.OnClickListener yesListener) {
        new AlertDialog.Builder(mContext).setIcon(android.R.drawable.ic_dialog_alert).setTitle(titleResId).setMessage(messageResId)
                .setPositiveButton(R.string.yes, yesListener).setNegativeButton(R.string.no, null).show();
    }

    public static void showAlertDialog(Context mContext, int messageResId, DialogInterface.OnClickListener yesListener) {
        new AlertDialog.Builder(mContext).setIcon(android.R.drawable.ic_dialog_alert).setTitle(R.string.confirm).setMessage(messageResId)
                .setPositiveButton(R.string.yes, yesListener).show();
    }

    public static void showAlertComletion(Context mContext, String title, DialogInterface.OnClickListener yesListener) {
        new AlertDialog.Builder(mContext).setIcon(android.R.drawable.ic_dialog_alert).setTitle(R.string.completion_alert).setMessage(title)
                .setPositiveButton(R.string.yes, yesListener).show();
    }

    public static void showYesNoDialog(Context mContext, int titleResId, int messageResId, DialogInterface.OnClickListener yesListener, DialogInterface.OnClickListener noListener) {
        new AlertDialog.Builder(mContext).setIcon(android.R.drawable.ic_dialog_alert).setTitle(titleResId).setMessage(messageResId)
                .setPositiveButton(R.string.yes, yesListener).setNegativeButton(R.string.no, noListener).show();
    }

}
