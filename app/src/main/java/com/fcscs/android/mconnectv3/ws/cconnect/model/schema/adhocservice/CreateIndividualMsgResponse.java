package com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice;

import java.io.Serializable;
import java.util.Date;

import org.ksoap2.serialization.SoapObject;

import com.fcscs.android.mconnectv3.common.util.DateTimeHelper;
import com.fcscs.android.mconnectv3.common.util.SoapHelper;

public class CreateIndividualMsgResponse implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2347067879577070834L;
    private String adHocMsgId;
    private String error;
    private Date created;

    //econnect
    private boolean success;
    private String returnCode = "";

    public CreateIndividualMsgResponse() {
    }

    public CreateIndividualMsgResponse(SoapObject soap) {


        Long id = SoapHelper.getLongAttribute(soap, "adHocMsgId", 0L);
        this.adHocMsgId = SoapHelper.getStringAttribute(soap, "adHocMsgId", "");
        this.success = id > 0;
        this.error = SoapHelper.getStringProperty(soap, "error", "");
        this.created = SoapHelper.getDateProperty(soap, "createdTime", DateTimeHelper.DEFAULT_FORMATTER_DATETIME_24H, DateTimeHelper.getDateByTimeZone(new Date()));

    }

    public String getAdHocMsgId() {
        return adHocMsgId;
    }

    public String getError() {
        return error;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public void setAdHocMsgId(String adHocMsgId) {
        this.adHocMsgId = adHocMsgId;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

}
