package com.fcscs.android.mconnectv3.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.BaseActivity;
import com.fcscs.android.mconnectv3.common.McConstants;
import com.fcscs.android.mconnectv3.common.ui.BottomBar;
import com.fcscs.android.mconnectv3.common.ui.McProgressAsyncTask;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.common.util.PrefsUtil;
import com.fcscs.android.mconnectv3.manager.model.LanguageDetail;
import com.fcscs.android.mconnectv3.manager.model.ServiceRequest;
import com.fcscs.android.mconnectv3.ws.econnect.ECService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Locale;

/**
 * Modified by Kha Tran
 * on : 2015/07/10  --> Fix #858
 */
public class TranslateActivity extends BaseActivity {

    public static final String EXTRA_TRANSLATED_TEXT = "extra_translated_text";
    public static final String EXTRA_TRANSLATED_TEXT_IS_MERGE = "extra_translated_text_is_merge";

    protected EditText etFrom;
    private Spinner spFrom;
    private Spinner spTo;
    private EditText etTo;
    private BottomBar footBar;
    private ImageButton infoMsgTo ,infoMsgFrom ;

    protected LanguageDetail selectedFrom;
    protected LanguageDetail selectedTo;
    private Locale currentLocale;
    private JSONObject mCurrentTranslate;
    private static final String languageName = "languageName";
    private static final String languageCode = "languageCode";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.translation_layout);
        currentLocale = PrefsUtil.getLocale();
        try {
            mCurrentTranslate = PrefsUtil.getCurrentTranslate(this);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (mCurrentTranslate == null) {
            mCurrentTranslate = new JSONObject();
            try {
                mCurrentTranslate.put(languageCode, currentLocale.getLanguage());
                mCurrentTranslate.put(languageName, currentLocale.getDisplayLanguage());
                PrefsUtil.setCurrentTranslate(this, mCurrentTranslate);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        etFrom = (EditText) findViewById(R.id.et_from);
        etFrom.setClickable(true);

        spFrom = (Spinner) findViewById(R.id.sp_from);
        spTo = (Spinner) findViewById(R.id.sp_to);
        infoMsgFrom = (ImageButton) findViewById(R.id.info_msg_from);
        infoMsgTo = (ImageButton) findViewById(R.id.info_msg_to);

        infoMsgFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toast(R.string.info_message);
            }
        });
        infoMsgTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toast(R.string.info_message);
            }
        });


        etTo = (EditText) findViewById(R.id.et_to);
        etTo.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        footBar = (BottomBar) findViewById(R.id.footBarView);
        footBar.getLeftTv().setText(R.string.translate);
        footBar.getLeftTv().setClickable(true);
        footBar.getLeftTv().setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (McConstants.OFFLINE_MODE) {
                    offlineTranslate(true);
                    return;
                }
                new GetTranslatedTextTask(getCurrentContext(), true, true).exec();
            }
        });

        footBar.getMiddleTv().setText(R.string.confirm);
        footBar.getMiddleTv().setClickable(true);
        footBar.getMiddleTv().setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                /**
                 * Bug #858
                 * Handle confirm Button
                 */
                if (McConstants.OFFLINE_MODE) {
                    offlineTranslate(false);
                    return;
                }
                new GetTranslatedTextTask(getCurrentContext(), false, true).exec();


            }
        });

        footBar.getRightTv().setText(R.string.cancel);
        footBar.getRightTv().setClickable(true);
        footBar.getRightTv().setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        new GetLanguageListTask(this).exec();
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey("TEXT_TRANSLATE")) {
            etFrom.setText(extras.getString("TEXT_TRANSLATE"));
        }
        if (McConstants.OFFLINE_MODE) {
            initOfflineMode();
        }
    }

    private void offlineTranslate(boolean isTranslate) {
        if (selectedTo.getCode().equalsIgnoreCase("cht")) {
            etTo.setText("測試");
        } else if (selectedTo.getCode().equalsIgnoreCase("chs")) {
            etTo.setText("测试");
        } else if (selectedTo.getCode().equalsIgnoreCase("eng")) {
            etTo.setText("Test");
        }

        String translatedText = etTo.getText().toString();
        Intent data = new Intent();
        data.putExtra(EXTRA_TRANSLATED_TEXT, translatedText);
        data.putExtra(EXTRA_TRANSLATED_TEXT_IS_MERGE, isTranslate);
        setResult(RESULT_OK, data);
        finish();
    }

    private void initOfflineMode() {

        final ArrayAdapter<LanguageDetail> adapterFrom = new ArrayAdapter<LanguageDetail>(getCurrentContext(), android.R.layout.simple_spinner_item);
        adapterFrom.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        LanguageDetail m1 = new LanguageDetail();
        m1.setDescription("English");
        m1.setCode("eng");
        adapterFrom.add(m1);

        LanguageDetail m2 = new LanguageDetail();
        m2.setDescription("繁体中文 (Traditional Chinese)");
        m2.setCode("cht");
        adapterFrom.add(m2);

        LanguageDetail m3 = new LanguageDetail();
        m3.setDescription("简体中文 (Simplified Chinese)");
        m3.setCode("chs");
        adapterFrom.add(m3);

        spFrom.setAdapter(adapterFrom);
        spFrom.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                if (isLoadDataFrom) {
                    isLoadDataFrom = false;
                    return;
                }
                LanguageDetail item = adapterFrom.getItem(pos);
                selectedFrom = item;
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        spTo.setAdapter(adapterFrom);
        spTo.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                if (isLoadDataTo) {
                    isLoadDataTo = false;
                    return;
                }
                LanguageDetail item = adapterFrom.getItem(pos);
                selectedTo = item;
                try {
                    if (!mCurrentTranslate.get(languageCode).equals(item.getCode())) {
                        mCurrentTranslate = new JSONObject();
                        mCurrentTranslate.put(languageCode, selectedTo.getCode());
                        mCurrentTranslate.put(languageName, selectedTo.getDescription());
                        PrefsUtil.setCurrentTranslate(TranslateActivity.this, mCurrentTranslate);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

    }

    @Override
    protected void onDestroy() {
        ServiceRequest.destroyInstance();
        super.onDestroy();
    }

    protected class GetLanguageListTask extends McProgressAsyncTask { //McBackgroundAsyncTask { // comment this for a loading dialog

        private ArrayAdapter<LanguageDetail> adapterFrom;
        private List<LanguageDetail> languageList;
        private ArrayAdapter<LanguageDetail> adapterTo;


        public GetLanguageListTask(Context context) {
            super(context);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            adapterFrom = new ArrayAdapter<LanguageDetail>(getCurrentContext(), android.R.layout.simple_spinner_item);
            adapterFrom.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            LanguageDetail m1 = new LanguageDetail();
            m1.setDescription(currentLocale.getDisplayLanguage());
            m1.setCode(currentLocale.getLanguage());
            adapterFrom.add(m1);

            spFrom.setAdapter(adapterFrom);
            spFrom.setOnItemSelectedListener(new OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                    if (isLoadDataFrom) {
                        isLoadDataFrom = false;
                        return;
                    }
                    LanguageDetail item = adapterFrom.getItem(pos);
                    selectedFrom = item;
                }

                @Override
                public void onNothingSelected(AdapterView<?> arg0) {
                }
            });


            adapterTo = new ArrayAdapter<LanguageDetail>(getCurrentContext(), android.R.layout.simple_spinner_item);
            adapterTo.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            LanguageDetail m2 = new LanguageDetail();
            try {
                m2.setDescription(mCurrentTranslate.getString(languageName));
                m2.setCode(mCurrentTranslate.getString(languageCode));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            adapterTo.add(m2);

            spTo.setAdapter(adapterTo);
            spTo.setOnItemSelectedListener(new OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                    if (isLoadDataTo) {
                        isLoadDataTo = false;
                        return;
                    }
                    LanguageDetail item = adapterTo.getItem(pos);
                    selectedTo = item;
                    try {
                        if (!mCurrentTranslate.get(languageCode).equals(item.getCode())) {
                            mCurrentTranslate = new JSONObject();
                            mCurrentTranslate.put(languageCode, selectedTo.getCode());
                            mCurrentTranslate.put(languageName, selectedTo.getDescription());
                            PrefsUtil.setCurrentTranslate(TranslateActivity.this, mCurrentTranslate);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> arg0) {
                }
            });

        }

        @Override
        public void doInBackground() {
            languageList = ECService.getInstance().getLanguageList(getCurrentContext());
            ECService.getInstance().getLanguageResourceFiles(getCurrentContext());
        }

        @Override
        public void onPostExecute() {

            if (languageList != null && languageList.size() > 0) {

                adapterFrom.clear();
                adapterTo.clear();
                int idxselectedFrom = -1;
                int idxselectedTo = -1;
                for (int i = 0; i < languageList.size(); i++) {
                    LanguageDetail item = languageList.get(i);
                    adapterFrom.add(item);
                    if (item.getCode().equalsIgnoreCase(currentLocale.getLanguage())) {
                        idxselectedFrom = i;
                    }
                }
                adapterFrom.notifyDataSetChanged();
                if (idxselectedFrom != -1) {
                    isLoadDataFrom = true;
                    spFrom.setSelection(idxselectedFrom);
                    selectedFrom = languageList.get(idxselectedFrom);
                }
                String languageCurrentCode = "";
                try {
                    languageCurrentCode = mCurrentTranslate.getString(languageCode);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                for (int i = 0; i < languageList.size(); i++) {
                    LanguageDetail item = languageList.get(i);
                    adapterTo.add(item);
                    if (item.getCode().equalsIgnoreCase(languageCurrentCode)) {
                        idxselectedTo = i;
                    }
                }
                adapterTo.notifyDataSetChanged();
                if (idxselectedTo != -1) {
                    isLoadDataTo = true;
                    spTo.setSelection(idxselectedTo);
                    selectedTo = languageList.get(idxselectedTo);
                }
                if (idxselectedFrom != -1 && idxselectedTo != -1 && idxselectedFrom != idxselectedTo) {
                    new GetTranslatedTextTask(getCurrentContext(), false, false).exec();
                }
            }

        }

    }

    boolean isLoadDataFrom = false;
    boolean isLoadDataTo = false;

    protected class GetTranslatedTextTask extends McProgressAsyncTask { // McBackgroundAsyncTask

        private boolean cancel;
        private boolean isTranslate;
        private boolean isFinish;
        private String text;

        public GetTranslatedTextTask(Context context, boolean isTranslate, boolean isFinish) {
            super(context);
            this.isTranslate = isTranslate;
            this.isFinish = isFinish;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if (selectedFrom == null || selectedTo == null) {
                cancel = true;
                return;
            }

            if (McUtils.isNullOrEmpty(selectedFrom.getCode()) || McUtils.isNullOrEmpty(selectedTo.getCode())) {
                cancel = true;
                return;
            }


        }

        @Override
        public void doInBackground() {
            if (!cancel) {
                text = ECService.getInstance().getTranslatedText(getCurrentContext(), selectedFrom.getCode(), selectedTo.getCode(), etFrom.getText().toString());
            }
        }

        @Override
        public void onPostExecute() {
            if (McUtils.isNullOrEmpty(text) == false) {
                if (isFinish) {
                    etTo.setText(text);
                    String translatedText = etTo.getText().toString();
                    Intent data = new Intent();
                    data.putExtra(EXTRA_TRANSLATED_TEXT, translatedText);
                    data.putExtra(EXTRA_TRANSLATED_TEXT_IS_MERGE, isTranslate);
                    setResult(RESULT_OK, data);
                    finish();
                } else {
                    String remarkText = etTo.getText().toString();
                    if (text.length() > 0) {
                        if (isTranslate) {
                            remarkText = remarkText + "\n" + text;
                        } else {
                            remarkText = text;
                        }
                    }
                    etTo.setText(remarkText);
                }
            } else {
                etTo.setText("");
            }

        }

    }

    @Override
    public boolean isLoginActivity() {
        return true;
    }

    @Override
    public boolean isHomeActivity() {
        return false;
    }

}
