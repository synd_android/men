package com.fcscs.android.mconnectv3.common.util;

import java.util.Locale;

import android.content.Context;
import android.content.SharedPreferences;

import com.fcscs.android.mconnectv3.McApplication;
import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.McConstants;
import com.fcscs.android.mconnectv3.common.McEnums.ServiceType;
import com.fcscs.android.mconnectv3.dao.entity.DBConfigContext;
import com.fcscs.android.mconnectv3.dao.entity.DBSessionContext;
import com.fcscs.android.mconnectv3.main.ConfigActivity;
import com.fcscs.android.mconnectv3.manager.model.GuestHistoryConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PrefsUtil {

    private static final String M_CONNECT_ENABLE_ENGINEERING_REQUEST = "mCONNECT_ENABLE_ENGINEERINGREQUEST";
    private static final String M_CONNECT_ENABLE_ENGINEERING = "mCONNECT_ENABLE_MENGINEERING";
    private static final String M_CONNECT_ENABLE_PTT = "mCONNECT_ENABLE_PTT";
    private static final String M_CONNECT_MINIBAR_POSTING_TYPE = "mCONNECT_MINIBAR_POSTING_TYPE";
    private static final String SHORT_DATE_TIME_FORMAT = "SHORT_DATE_TIME_FORMAT";
    private static final String SHORT_DATE_FORMAT = "SHORT_DATE_FORMAT";
    private static final String DATE_TIME_FORMAT = "DATE_TIME_FORMAT";
    private static final String TIME_ZONE_FORMAT = "TIME_ZONE_FORMAT";
    private static final String DATE_FORMAT = "DATE_FORMAT";
    private static final String SESSION_VIEW_MESSAGE = "session_view_message";
    private static final String SESSION_VIEW_JOB = "session_view_job";
    private static final String SESSION_NAME = "session_name";
    private static final String SESSION_USER_NAME = "session_user_name";
    private static final String SESSION_USER_ID = "session_user_id";
    private static final String SESSION_TOKEN = "session_token";
    private static final String SESSION_ROOM_STATUS = "session_room_status";
    private static final String SESSION_REMEMBER = "session_remember";
    private static final String SESSION_PASSWORD = "session_password";
    private static final String SESSION_NON_GUEST_REQUEST = "session_non_guest_request";
    private static final String SESSION_NON_GUEST_REQUEST_API = "SESSION_NON_GUEST_REQUEST_API";
    private static final String SESSION_GUEST_REQUEST_API = "SESSION_GUEST_REQUEST_API";
    private static final String SESSION_NEW_REQUEST = "session_new_request";
    private static final String SESSION_NEW_MESSAGE = "session_new_message";
    private static final String SESSION_MINIBAR = "session_minibar";
    private static final String SESSION_MSG_COUNT = "session_msg_count";
    private static final String SESSION_LOGIN = "session_login";
    private static final String SESSION_LOCALE_LANGUAGE = "session_locale_language";
    private static final String SESSION_LOCALE_COUNTRY = "session_locale_country";
    private static final String SESSION_JOB_SEARCH = "session_job_search";
    private static final String SESSION_INTER_DEPARTMENT = "session_inter_department";
    private static final String SESSION_GUEST_REQUEST = "session_guest_request";
    private static final String SESSION_GUEST_DETAIL = "session_guest_detail";
    private static final String SESSION_FAVORITE = "session_favorite";
    private static final String CONFIG_SERVICE_URL = "config_service_url";
    private static final String CONFIG_SERVICE_TYPE = "config_service_type";
    private static final String SETTING_SERVICE_TYPE = "setting_service_type";
    private static final String CONFIG_HOTEL_ID = "config_hotel_id";
    private static final String CONFIG_PROP_ID = "config_prop_id";
    private static final String CONFIG_PULL_PERIOD = "config_pull_period";
    private static final String CONFIG_VIBRATE = "config_vibrate";
    private static final String CONFIG_AUTOACK = "config_autoack";
    private static final String ENABLE_DEBUG_MODE = "ENABLE_DEBUG_MODE";
    private static final String ENABLE_SOUND_WIFI_CHECK = "ENABLE_SOUND_WIFI_CHECK";
    private static final String NOTIFICATION_SOUND = "NOTIFICATION_SOUND";
    private static final String ENABLE_AUTO_ACK_MSG = "ENABLE_AUTO_ACK_MSG";
    private static final String ENABLE_VIBRATION = "ENABLE_VIBRATION";
    private static final String ENABLE_AUTO_ACK_MY_JOB = "ENABLE_AUTO_ACK_MY_JOB";
    private static final String GUEST_HISTORY_D1 = "GUEST_HISTORY_D1";
    private static final String GUEST_HISTORY_W1 = "GUEST_HISTORY_W1";
    private static final String GUEST_HISTORY_M1 = "GUEST_HISTORY_M1";
    private static final String GUEST_HISTORY_M3 = "GUEST_HISTORY_M3";
    private static final String GUEST_HISTORY_M6 = "GUEST_HISTORY_M6";
    private static final String GUEST_HISTORY_Y1 = "GUEST_HISTORY_Y1";
    private static final String IS_OFFLINE_MODE = "IS_OFFLINE_MODE";
    private static final String IS_CLICK_PANIC = "IS_CLICK_PANIC";
    private static final String JOB_PULLING_INTERVAL = "JOB_PULLING_INTERVAL";
    private static final String MSG_PULLING_INTERVAL = "MSG_PULLING_INTERVAL";
    private static final String FIRST_LAUNCHA_AFTER_INSTALL = "FIRST_LAUNCHA_AFTER_INSTALL";
    private static final String M_CONNECT_QRCODE = "M_CONNECT_QRCODE";
    private static final String MCONNECT_JOB_STATUS = "mCONNECT_JOB_STATUS";
    private static final String ALLOW_JOB_EDIT_AFTER_COMPLETED = "ALLOW_JOB_EDIT_AFTER_COMPLETED";
    private static final String ENABLE_CONNECT_PLUS = "ENABLE_CONNECT_PLUS";

    private static final String M_EN_RE_ASSIGN_RUNNER = "M_EN_RE_ASSIGN_RUNNER";
    private static final String M_EN_USER_TYPE = "M_EN_USER_TYPE";
    private static final String M_EN_USER_DEPARTMENT = "M_EN_USER_DEPARTMENT";
    private static final String CONDITION_LIST = "CONDITION_LIST";
    private static Locale locale = null;
    private static SecurePreferences sSecuredPref;
//    sessionCtx.setReAssignRunner(loginResp.getReAssignRunner());
//                sessionCtx.setUserType(loginResp.getUserType());
//                sessionCtx.setUserDepartment(loginResp.getUserDepartment());

    private static SharedPreferences getPrefs(Context context) {
        return context.getSharedPreferences("preference.data", Context.MODE_PRIVATE);
    }

    private static SecurePreferences getSecurePrefs(Context context) {
        if (sSecuredPref == null) {
            sSecuredPref = new SecurePreferences(context, "spref", context.getResources().getString(R.string.secure_pref_key), false);
        }
        return sSecuredPref;
    }

    public static void saveConfigPrefs(Context context, DBConfigContext config) {

        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();

        editor.putString(CONFIG_PROP_ID, config.getPropertyId());
        editor.putString(CONFIG_SERVICE_TYPE, config.getServiceType());
        editor.putString(CONFIG_SERVICE_URL, config.getServiceUrl());
        editor.putString(CONFIG_PULL_PERIOD, config.getPullPeriod());
        editor.putString(CONFIG_VIBRATE, config.getVibrate());
        editor.putString(CONFIG_AUTOACK, config.getAutoAck());

        editor.putString("PanicCallContactNumber", config.getPanicCallContactNumber());
        editor.putBoolean("EnableOnLoginPage", config.isEnableOnLoginPage());
        editor.putBoolean("EnableOnMainPage", config.isEnableOnMainPage());
        editor.putBoolean("EnablePanicCall", config.isEnablePanicCall());
        editor.putBoolean("EnablePanicJob", config.isEnablePanicJob());

        editor.commit();

    }

    public static DBConfigContext getConfigPrefs(Context context) {

        SharedPreferences prefs = getPrefs(context);

        DBConfigContext config = new DBConfigContext();
        config.setPropertyId(prefs.getString(CONFIG_PROP_ID, "-1"));
        config.setServiceType(prefs.getString(CONFIG_SERVICE_TYPE, ServiceType.ECONNECT.toString()));
        config.setServiceUrl(prefs.getString(CONFIG_SERVICE_URL, McConstants.WEBSERVICE_APP_URL));
        config.setPullPeriod(prefs.getString(CONFIG_PULL_PERIOD, "" + McConstants.PULL_PERIOD));
        config.setVibrate(prefs.getString(CONFIG_VIBRATE, "True"));
        config.setAutoAck(prefs.getString(CONFIG_AUTOACK, "True"));
        config.setPanicCallContactNumber(prefs.getString("PanicCallContactNumber", ""));
        config.setEnablePanicJob(prefs.getBoolean("EnablePanicJob", false));
        config.setEnablePanicCall(prefs.getBoolean("EnablePanicCall", false));
        config.setEnableOnMainPage(prefs.getBoolean("EnableOnMainPage", false));
        config.setEnableOnLoginPage(prefs.getBoolean("EnableOnLoginPage", false));
        return config;

    }

    public static void saveLocale(Locale locale) {

        PrefsUtil.locale = locale;

        SharedPreferences prefs = getPrefs(McApplication.application);
        SharedPreferences.Editor editor = prefs.edit();

        editor.putString(SESSION_LOCALE_COUNTRY, locale.getCountry());
        editor.putString(SESSION_LOCALE_LANGUAGE, locale.getLanguage());

        editor.commit();

    }

    public static Locale getLocale() {

        if (PrefsUtil.locale != null) {
            return PrefsUtil.locale;
        }

        SharedPreferences prefs = getPrefs(McApplication.application);

        String country = prefs.getString(SESSION_LOCALE_COUNTRY, null);
        String language = prefs.getString(SESSION_LOCALE_LANGUAGE, null);

        Locale locale = null;
        if (language == null) {
            locale = Locale.US;
        } else {
            locale = new Locale(language, country);
        }
        PrefsUtil.locale = locale;
        return locale;

    }


    public static void saveSessionPrefs(Context context, DBSessionContext session) {

        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();

        SecurePreferences securePrefs = getSecurePrefs(context);

        editor.putBoolean(SESSION_FAVORITE, session.getFavorite());
        editor.putBoolean(SESSION_GUEST_DETAIL, session.getGuestDetail());
        editor.putBoolean(SESSION_GUEST_REQUEST, session.getGuestReuqest());
        editor.putBoolean(SESSION_INTER_DEPARTMENT, session.getInterDepartmentRequest());
        editor.putBoolean(SESSION_JOB_SEARCH, session.getJobSearch());
        editor.putBoolean(SESSION_LOGIN, session.getLogin());
        editor.putInt(SESSION_MSG_COUNT, session.getMessageCount());
        editor.putBoolean(SESSION_MINIBAR, session.getMinibar());
        editor.putBoolean(SESSION_NEW_MESSAGE, session.getNewMessage());
        editor.putBoolean(SESSION_NEW_REQUEST, session.getNewRequest());
        editor.putBoolean(SESSION_NON_GUEST_REQUEST, session.getNonGuestRequest());
        editor.putBoolean(SESSION_REMEMBER, session.getRemember());
        editor.putBoolean(SESSION_ROOM_STATUS, session.getRoomStatus());
        editor.putBoolean(SESSION_VIEW_JOB, session.getMyJob());
        editor.putBoolean(SESSION_VIEW_MESSAGE, session.getViewMessage());

//        editor.putBoolean(SESSION_VIEW_MESSAGE, session.getViewMessage());
        editor.commit();

        securePrefs.put(SESSION_PASSWORD, session.getPassword());
        securePrefs.put(SESSION_TOKEN, session.getSessionToken());
        securePrefs.put(SESSION_USER_ID, session.getUserId());
        securePrefs.put(SESSION_USER_NAME, session.getUsername());
        securePrefs.put(SESSION_NAME, session.getFullname());

    }

    public static DBSessionContext getSessionPrefs(Context context) {

        SharedPreferences prefs = getPrefs(context);

        DBSessionContext session = new DBSessionContext();

        SecurePreferences securePrefs = getSecurePrefs(context);

        session.setFavorite(prefs.getBoolean(SESSION_FAVORITE, session.getFavorite()));
        session.setGuestDetail(prefs.getBoolean(SESSION_GUEST_DETAIL, session.getGuestDetail()));
        session.setGuestReuqest(prefs.getBoolean(SESSION_GUEST_REQUEST, session.getGuestReuqest()));
        session.setInterDepartmentRequest(prefs.getBoolean(SESSION_INTER_DEPARTMENT, session.getInterDepartmentRequest()));
        session.setJobSearch(prefs.getBoolean(SESSION_JOB_SEARCH, session.getJobSearch()));
        session.setLogin(prefs.getBoolean(SESSION_LOGIN, session.getLogin()));
        session.setMessageCount(prefs.getInt(SESSION_MSG_COUNT, session.getMessageCount()));
        session.setMinibar(prefs.getBoolean(SESSION_MINIBAR, session.getMinibar()));
        session.setNewMessage(prefs.getBoolean(SESSION_NEW_MESSAGE, session.getNewMessage()));
        session.setNewRequest(prefs.getBoolean(SESSION_NEW_REQUEST, session.getNewRequest()));
        session.setNonGuestRequest(prefs.getBoolean(SESSION_NON_GUEST_REQUEST, session.getNonGuestRequest()));
        session.setRemember(prefs.getBoolean(SESSION_REMEMBER, session.getRemember()));
        session.setRoomStatus(prefs.getBoolean(SESSION_ROOM_STATUS, session.getRoomStatus()));
        session.setMyJob(prefs.getBoolean(SESSION_VIEW_JOB, session.getMyJob()));
        session.setMyMessage(prefs.getBoolean(SESSION_VIEW_MESSAGE, session.getViewMessage()));

        session.setPassword(securePrefs.getString(SESSION_PASSWORD));
        session.setSessionToken(securePrefs.getString(SESSION_TOKEN));
        session.setUserId(securePrefs.getString(SESSION_USER_ID));
        session.setUsername(securePrefs.getString(SESSION_USER_NAME));
        session.setFullname(securePrefs.getString(SESSION_NAME));
        return session;

    }

    public static void setDebugMode(Context context, boolean isDebugMode) {

        McConstants.ENABLE_DEBUG_MODE = isDebugMode;
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(ENABLE_DEBUG_MODE, isDebugMode);
        editor.commit();

    }

    public static boolean isDebugMode(Context context) {
        SharedPreferences prefs = getPrefs(context);
        return prefs.getBoolean(ENABLE_DEBUG_MODE, McConstants.DEFAULT_DEBUG_MODE);
    }

    //add hotelId
    public static void setHotelId(Context context, String hotelId) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(CONFIG_HOTEL_ID, hotelId);
        editor.commit();

    }

    public static String getHotelId(Context context) {
        SharedPreferences prefs = getPrefs(context);
        String value = prefs.getString(CONFIG_HOTEL_ID, "1");
        return value;
    }

    public static void setReAssignRunner(Context context, int reAssignRunner) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(M_EN_RE_ASSIGN_RUNNER, reAssignRunner);
        editor.commit();

    }

    public static int getmEnReAssignRunner(Context context) {
        SharedPreferences prefs = getPrefs(context);
        int value = prefs.getInt(M_EN_RE_ASSIGN_RUNNER, 0);
        return value;
    }

    public static void setConditionList(Context context, String listCondition) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(CONDITION_LIST, listCondition);
        editor.commit();

    }

    public static String getConditionList(Context context) {
        SharedPreferences prefs = getPrefs(context);
        String value = prefs.getString(CONDITION_LIST, "");
        return value;
    }

    public static void setUserType(Context context, String userType) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(M_EN_USER_TYPE, userType);
        editor.commit();

    }

    public static String getUserType(Context context) {
        SharedPreferences prefs = getPrefs(context);
        String value = prefs.getString(M_EN_USER_TYPE, "");
        return value;
    }


    public static void setUserDepartMent(Context context, String userDepartment) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(M_EN_USER_DEPARTMENT, userDepartment);
        editor.commit();

    }

    public static String getUserDepartMent(Context context) {
        SharedPreferences prefs = getPrefs(context);
        String value = prefs.getString(M_EN_USER_DEPARTMENT, "");
        return value;
    }


    public static void setEnableSoundWifiCheck(Context context, boolean isEnable) {

        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(ENABLE_SOUND_WIFI_CHECK, isEnable);
        editor.commit();

    }

    public static boolean isEnableSoundWifiCheck(Context context) {
        SharedPreferences prefs = getPrefs(context);
        return prefs.getBoolean(ENABLE_SOUND_WIFI_CHECK, McConstants.DEFAULT_ENABLE_SOUND_WIFI_CHECK);
    }

    public static void setNotificationSound(Context context, int soundName) {

        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(NOTIFICATION_SOUND, soundName);
        editor.commit();

    }

    public static int getNotificationSound(Context context) {
        SharedPreferences prefs = getPrefs(context);
        return prefs.getInt(NOTIFICATION_SOUND, McConstants.DEFAULT_NOTIFICATION_SOUND_TYPE);
    }

    public static void setEnableVibration(Context context, boolean enable) {

        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(ENABLE_VIBRATION, enable);
        editor.commit();

    }

    public static boolean isOfflineMode(Context context) {
        SharedPreferences prefs = getPrefs(context);
        return prefs.getBoolean(IS_OFFLINE_MODE, false);
    }

    public static void setOfflineMode(Context context, boolean enable) {

        McConstants.OFFLINE_MODE = enable;

        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(IS_OFFLINE_MODE, enable);
        editor.commit();

    }

    public static boolean isClickPanic(Context context) {
        SharedPreferences prefs = getPrefs(context);
        return prefs.getBoolean(IS_CLICK_PANIC, false);
    }

    public static void setClickPanic(Context context, boolean enable) {

        //McConstants.OFFLINE_MODE = enable;

        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(IS_CLICK_PANIC, enable);
        editor.commit();

    }


    public static boolean isEnableVibration(Context context) {
        SharedPreferences prefs = getPrefs(context);
        return prefs.getBoolean(ENABLE_VIBRATION, McConstants.DEFAULT_ENABLE_VIBRATION);

    }

    public static void setAutoAckMsg(Context context, boolean isAutoAck) {

        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(ENABLE_AUTO_ACK_MSG, isAutoAck);
        editor.commit();

    }

    public static boolean isAutoAckMsg(Context context) {
        SharedPreferences prefs = getPrefs(context);
        return prefs.getBoolean(ENABLE_AUTO_ACK_MSG, false);

    }

    public static void setAutoAckMyJob(Context context, boolean isAutoAck) {

        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(ENABLE_AUTO_ACK_MY_JOB, isAutoAck);
        editor.commit();

    }

    public static boolean isAutoAckMyJob(Context context) {
        SharedPreferences prefs = getPrefs(context);
        return prefs.getBoolean(ENABLE_AUTO_ACK_MY_JOB, false);
//		return true;
    }

    public static void setGuestHistoryConfig(Context context, GuestHistoryConfig config) {

        if (config == null) {
            config = new GuestHistoryConfig();
        }

        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(GUEST_HISTORY_D1, config.isDay1());
        editor.putBoolean(GUEST_HISTORY_W1, config.isWeek1());
        editor.putBoolean(GUEST_HISTORY_M1, config.isMonth1());
        editor.putBoolean(GUEST_HISTORY_M3, config.isMonth3());
        editor.putBoolean(GUEST_HISTORY_M6, config.isMonth6());
        editor.putBoolean(GUEST_HISTORY_Y1, config.isYear1());
        editor.commit();

    }

    public static GuestHistoryConfig getGuestHistoryConfig(Context context) {
        SharedPreferences prefs = getPrefs(context);
        GuestHistoryConfig config = new GuestHistoryConfig();
        config.setDay1(prefs.getBoolean(GUEST_HISTORY_D1, false));
        config.setWeek1(prefs.getBoolean(GUEST_HISTORY_W1, false));
        config.setMonth1(prefs.getBoolean(GUEST_HISTORY_M1, false));
        config.setMonth3(prefs.getBoolean(GUEST_HISTORY_M3, false));
        config.setMonth6(prefs.getBoolean(GUEST_HISTORY_M6, false));
        config.setYear1(prefs.getBoolean(GUEST_HISTORY_Y1, false));
        return config;
    }

    public static void setJobPullingInterval(Context context, int value) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(JOB_PULLING_INTERVAL, value);
        editor.commit();
    }

    public static int getJobPullingInterval(Context context) {
        SharedPreferences prefs = getPrefs(context);
        int value = prefs.getInt(JOB_PULLING_INTERVAL, 60);
        return value;
    }

    public static void setMsgPullingInterval(Context context, int value) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(MSG_PULLING_INTERVAL, value);
        editor.commit();
    }

    public static int getMsgPullingInterval(Context context) {
        SharedPreferences prefs = getPrefs(context);
        int value = prefs.getInt(MSG_PULLING_INTERVAL, 120);
        return value;
    }

    public static void setFirstLaunchAfterInstall(Context context, boolean first) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(FIRST_LAUNCHA_AFTER_INSTALL, first);
        editor.commit();
    }

    public static boolean isFirstLaunchAfterInstall(Context context) {
        SharedPreferences prefs = getPrefs(context);
        boolean value = prefs.getBoolean(FIRST_LAUNCHA_AFTER_INSTALL, true);
        return value;
    }

    public static void setShortDateFormat(Context context, String fmt) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(SHORT_DATE_FORMAT, fmt);
        editor.commit();
    }

    public static String getShortDateFormat(Context context) {
        SharedPreferences prefs = getPrefs(context);
        String value = prefs.getString(SHORT_DATE_FORMAT, "dd-MMM");
        return value;
    }

    public static void setShortDateTimeFormat(Context context, String fmt) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(SHORT_DATE_TIME_FORMAT, fmt);
        editor.commit();
    }

    public static String getShortDateTimeFormat(Context context) {
        SharedPreferences prefs = getPrefs(context);
        String value = prefs.getString(SHORT_DATE_TIME_FORMAT, "dd-MMM HH:mm");
        return value;
    }

    public static void setDateFormat(Context context, String fmt) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(DATE_FORMAT, fmt);
        editor.commit();
    }

    public static String getDateFormat(Context context) {
        SharedPreferences prefs = getPrefs(context);
        String value = prefs.getString(DATE_FORMAT, DateTimeHelper.FORMAT_DATE_DASH);
        return value;
    }

    public static void setSettingServiceType(Context context, ServiceType type) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(SETTING_SERVICE_TYPE, type.toString());
        editor.commit();
    }

    public static ServiceType getSettingServiceType(Context context) {
        SharedPreferences prefs = getPrefs(context);
        String value = prefs.getString(SETTING_SERVICE_TYPE, ConfigActivity.DEFAULT_SERVICE_TYPE.toString());
        return ServiceType.valueOf(value);
    }


    public static void setDateTimeFormat(Context context, String fmt) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(DATE_TIME_FORMAT, fmt);
        editor.commit();
    }

    public static String getDateTimeFormat(Context context) {
        SharedPreferences prefs = getPrefs(context);
        String value = prefs.getString(DATE_TIME_FORMAT, DateTimeHelper.DEFAULT_FORMATTER_DATETIME_24H);
        return value;
    }


    public static void setTimeZone(Context context, String fmt) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(TIME_ZONE_FORMAT, fmt);
        editor.commit();
    }

    public static String getTimeZone(Context context) {
        SharedPreferences prefs = getPrefs(context);
        String value = prefs.getString(TIME_ZONE_FORMAT, DateTimeHelper.DEFAULT_TIME_ZONE);
        return value;
    }


    public static void setTimeout(Context context, int timeout) {

        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();

        editor.putInt("TIMEOUT", timeout);
        editor.commit();

    }

    public static int getTimeout(Context context) {

        if (context == null) {
            return 15;
        }

        SharedPreferences prefs = getPrefs(context);
        return prefs.getInt("TIMEOUT", 15);

    }

    public static void setMinibarType(Context context, int value) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(M_CONNECT_MINIBAR_POSTING_TYPE, value);
        editor.commit();
    }

    public static void setEnablePTT(Context context, int value) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(M_CONNECT_ENABLE_PTT, value);
        editor.commit();
    }

    public static void setEnableEngReq(Context context, int value) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(M_CONNECT_ENABLE_ENGINEERING_REQUEST, value);
        editor.commit();
    }

    public static void setEnableEng(Context context, int value) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(M_CONNECT_ENABLE_ENGINEERING, value);
        editor.commit();
    }

    /**
     * now, based on the INI Minibar_Posting_Type,
     * 0 = show total minibar
     * 1 = show itemized minibar
     * 2 = show both
     *
     * @param context
     * @return
     */
    public static int getMinibarType(Context context) {
        SharedPreferences prefs = getPrefs(context);
        int value = prefs.getInt(M_CONNECT_MINIBAR_POSTING_TYPE, 0);
        return value;
    }

    public static int getEnablePTT(Context context) {
        SharedPreferences prefs = getPrefs(context);
        int value = prefs.getInt(M_CONNECT_ENABLE_PTT, 0);
        return value;
    }

    public static int getEnableEngReq(Context context) {
        SharedPreferences prefs = getPrefs(context);
        int value = prefs.getInt(M_CONNECT_ENABLE_ENGINEERING_REQUEST, 0);
        return value;
    }

    public static int getEnableEng(Context context) {
        SharedPreferences prefs = getPrefs(context);
        int value = prefs.getInt(M_CONNECT_ENABLE_ENGINEERING, 0);
        return value;
    }

    public static int getAllowJobEditAfterCompleted(Context context) {
        SharedPreferences prefs = getPrefs(context);
        int value = prefs.getInt(ALLOW_JOB_EDIT_AFTER_COMPLETED, 0);
        return value;
    }

    public static void setAllowJobEditAfterCompleted(Context context, int value) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(ALLOW_JOB_EDIT_AFTER_COMPLETED, value);
        editor.commit();
    }

    public static void setEnableGuestRequestReq(Context context, int value) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(SESSION_GUEST_REQUEST_API, value == 0 ? false : true);
        editor.commit();
    }

    public static void setEnableNonGuestRequestReq(Context context, int value) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(SESSION_NON_GUEST_REQUEST_API, value == 0 ? false : true);
        editor.commit();
    }

    public static void setEnableConnectPlus(Context context, int value) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(ENABLE_CONNECT_PLUS, value == 0 ? false : true);
        editor.commit();
    }

    public static boolean getEnableGuestRequestReq(Context context) {
        SharedPreferences prefs = getPrefs(context);
        boolean value = prefs.getBoolean(SESSION_GUEST_REQUEST_API, true);
        return value;
    }

    public static boolean getEnableNonGuestRequestReq(Context context) {
        SharedPreferences prefs = getPrefs(context);
        boolean value = prefs.getBoolean(SESSION_NON_GUEST_REQUEST_API, false);
        return value;

//        return true;
    }

    public static boolean getEnableConnectPlus(Context context) {
        SharedPreferences prefs = getPrefs(context);
        boolean value = prefs.getBoolean(ENABLE_CONNECT_PLUS, false);
        return value;
    }

    public static void setQRCodeLogin(Context context, boolean enable) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("mCONNECT_QR_CODE_LOGIN", enable);
        editor.commit();
    }

    public static boolean getQRCodeLogin(Context context) {
        SharedPreferences prefs = getPrefs(context);
        boolean value = prefs.getBoolean("mCONNECT_QR_CODE_LOGIN", false);
        return value;
    }

    public static void setQRCodeLogout(Context context, boolean enable) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("mCONNECT_QR_CODE_LOGOUT", enable);
        editor.commit();
    }

    public static boolean getQRCodeLogout(Context context) {
        SharedPreferences prefs = getPrefs(context);
        boolean value = prefs.getBoolean("mCONNECT_QR_CODE_LOGOUT", false);
        return value;
    }

    public static void setQRCodeLicenseRegistration(Context context, boolean enable) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("mCONNECT_QR_CODE_LICENSE_REGISTRATION", enable);
        editor.commit();
    }

    public static boolean getQRCodeLicenseRegistration(Context context) {
        SharedPreferences prefs = getPrefs(context);
        boolean value = prefs.getBoolean("mCONNECT_QR_CODE_LICENSE_REGISTRATION", false);
        return value;
    }

    public static void setQRCodeSearchJob(Context context, boolean enable) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("mCONNECT_QR_CODE_SEARCH_JOB", enable);
        editor.commit();
    }

    public static boolean getQRCodeSearchJob(Context context) {
        SharedPreferences prefs = getPrefs(context);
        boolean value = prefs.getBoolean("mCONNECT_QR_CODE_SEARCH_JOB", false);
        return value;
    }

    public static void setQRCodeGuestDetail(Context context, boolean enable) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("mCONNECT_QR_CODE_GUEST_DETAIL", enable);
        editor.commit();
    }

    public static boolean getQRCodeGuestDetail(Context context) {
        SharedPreferences prefs = getPrefs(context);
        boolean value = prefs.getBoolean("mCONNECT_QR_CODE_GUEST_DETAIL", false);
        return value;
    }

    public static void setQRCodeNewRequest(Context context, boolean enable) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("mCONNECT_QR_CODE_NEW_REQUEST", enable);
        editor.commit();
    }

    public static boolean getQRCodeNewRequest(Context context) {
        SharedPreferences prefs = getPrefs(context);
        boolean value = prefs.getBoolean("mCONNECT_QR_CODE_NEW_REQUEST", false);
        return value;
    }

    public static void setQRCodeRoomStatus(Context context, boolean enable) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("mCONNECT_QR_CODE_ROOM_STATUS", enable);
        editor.commit();
    }

    public static boolean getQRCodeRoomStatus(Context context) {
        SharedPreferences prefs = getPrefs(context);
        boolean value = prefs.getBoolean("mCONNECT_QR_CODE_ROOM_STATUS", false);
        return value;
    }


    public static void setQRCodeMinibar(Context context, boolean enable) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("mCONNECT_QR_CODE_MINIBAR", enable);
        editor.commit();
    }

    public static void setConnectJobStatus(Context context, boolean enable) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(MCONNECT_JOB_STATUS, enable);
        editor.commit();
    }

    public static boolean getQRCodeMinibar(Context context) {
        SharedPreferences prefs = getPrefs(context);
        boolean value = prefs.getBoolean("mCONNECT_QR_CODE_MINIBAR", false);
        return value;
    }

    public static boolean getConnectJobStatus(Context context) {
        SharedPreferences prefs = getPrefs(context);
        boolean value = prefs.getBoolean(MCONNECT_JOB_STATUS, false);
        return value;
    }

    public static void setLanguageCode(Context context, String value) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("MC_LANGUAGE_CODE", value);
        editor.commit();
    }

    public static int getEnDerpId(Context context) {
        SharedPreferences prefs = getPrefs(context);
        int value = prefs.getInt("ENGINEERING_DEPARTMENT_ID", 0);
        return value;
    }

    public static void setEnDerpId(Context context, int value) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("ENGINEERING_DEPARTMENT_ID", value);
        editor.commit();
    }

    public static String getLanguageCode(Context context) {
        SharedPreferences prefs = getPrefs(context);
        String value = prefs.getString("MC_LANGUAGE_CODE", "en_US");
        return value;
    }


    public static JSONArray getRoomStatus(Context context) throws JSONException {
        SharedPreferences prefs = getPrefs(context);
        JSONArray result = null;
        String strJson = prefs.getString("MC_ROOM_STATUS", "0");
        if (strJson != null)
            result = new JSONArray(strJson);
        return result;
    }

    public static void setRoomStatus(Context context, JSONArray jsonArray) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("MC_ROOM_STATUS", jsonArray.toString());
        editor.commit();
    }

    public static void setRoomStatusMsg(Context context, String msg) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("MC_ROOM_STATUS_MSG", msg);
        editor.commit();
    }

    public static String getRoomStatusMsg(Context context) {
        SharedPreferences prefs = getPrefs(context);
        String msg = prefs.getString("MC_ROOM_STATUS_MSG", "");
        return msg;
    }

    public static JSONObject getCurrentTranslate(Context context) throws JSONException {
        SharedPreferences prefs = getPrefs(context);
        JSONObject result = null;
        String strJson = prefs.getString("MC_CURRENT_TRANSLATE", "0");
        if (strJson != null)
            result = new JSONObject(strJson);
        return result;

    }

    public static void setCurrentTranslate(Context context, JSONObject jsonObject) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("MC_CURRENT_TRANSLATE", jsonObject.toString());
        editor.commit();
    }
}
