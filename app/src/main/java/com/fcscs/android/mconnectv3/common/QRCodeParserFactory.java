package com.fcscs.android.mconnectv3.common;

import android.webkit.URLUtil;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fcscs.android.mconnectv3.common.util.McUtils;

public class QRCodeParserFactory {

    public static final int GUEST_REQUEST = 1;
    public static final int INTERDEPT_REQUEST = 2;
    public static final int LOCATION_REQUEST = 3;
    public static final int ROOM_STATUS_REQUEST = 4;
    public static final int MINIBAR_REQUEST = 5;
    public static final int GROUP_LOCATION_REQUEST = 6;
    //add hotelId
    public static final int CONFIG_URL_REQUEST = 7;
    //
    public static final int TYPE_NORMAL = 8;

    public static QRCodeParser getParser(int type) {
        switch (type) {
            case GUEST_REQUEST:
                return new GuestQRCodeParser();
            case INTERDEPT_REQUEST:
                return new InterDepartmentQRCodeParser();
            case LOCATION_REQUEST:
                return new LocationQRCodeParser();
            case ROOM_STATUS_REQUEST:
                return new RoomStatusQRCodeParser();
            case MINIBAR_REQUEST:
                return new MinibarQRCodeParser();
            case GROUP_LOCATION_REQUEST:
                return new GroupLocationQRCodeParser();
            case CONFIG_URL_REQUEST:
                return new ConfigUrlQRCodeParser();
            case TYPE_NORMAL:
                return new NormaQRCodeParser();
        }

        return null;
    }

    public static class GuestQRCodeParser implements QRCodeParser {

        public String room;
        public String code;
        public String qty;
        public String auto;

        @Override
        public boolean isCorrectFormat(String qrcode) {

            Pattern pattern = Pattern.compile("guestrequest|LOC([0-9a-zA-Z\\-.]+)|ITC([0-9a-zA-Z\\-]+)|QTY([0-9]+)|AUT([0-9]+)");

            try {
                Matcher matcher = pattern.matcher(qrcode);
                if (matcher.groupCount() >= 2) {
                    matcher.find();
                    matcher.find();
                    room = matcher.group(1);
//					matcher.find();
//					code = matcher.group(2);
//					matcher.find();
//					qty = matcher.group(3);
//					matcher.find();
//					auto = matcher.group(4);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (McUtils.isNullOrEmpty(room)) {
                return false;
            }
            return true;
        }

    }

    public static class InterDepartmentQRCodeParser implements QRCodeParser {

        public String room;
        public String code;
        public String qty;
        public String auto;

        @Override
        public boolean isCorrectFormat(String result) {

            Pattern pattern = Pattern.compile("nonguestrequest|LOC([0-9a-zA-Z\\-.]+)|ITC([0-9a-zA-Z\\-]+)|QTY([0-9]+)|AUT([0-9]+)");

            try {
                Matcher matcher = pattern.matcher(result);
                if (matcher.groupCount() >= 2) {
                    matcher.find();
                    matcher.find();
                    room = matcher.group(1);
//					matcher.find();
//					code = matcher.group(2);
//					matcher.find();
//					qty = matcher.group(3);
//					matcher.find();
//					auto = matcher.group(4);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (McUtils.isNullOrEmpty(room)) {// || McUtils.isNullOrEmpty(code) || McUtils.isNullOrEmpty(qty) || McUtils.isNullOrEmpty(auto)) {
                return false;
            } else {
                return true;
            }
        }

    }

    public static class NormaQRCodeParser implements QRCodeParser {

//        public String room;

        @Override
        public boolean isCorrectFormat(String result) {
            return true;
//			Pattern pattern = Pattern.compile("location|LOC([0-9a-zA-Z\\-]+)");
//
//			try {
//				Matcher matcher = pattern.matcher(result);
//				if (matcher.groupCount() == 1) {
//					matcher.find();
//					matcher.find();
//					room = matcher.group(1);
//				}
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//
//			if (McUtils.isNullOrEmpty(room)) {
//				return false;
//			} else {
//				return true;
//			}
        }

    }


    public static class LocationQRCodeParser implements QRCodeParser {

        public String room;

        @Override
        public boolean isCorrectFormat(String result) {

            Pattern pattern = Pattern.compile("location|LOC([0-9a-zA-Z\\-]+)");

            try {
                Matcher matcher = pattern.matcher(result);
                if (matcher.groupCount() == 1) {
                    matcher.find();
                    matcher.find();
                    room = matcher.group(1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (McUtils.isNullOrEmpty(room)) {
                return false;
            } else {
                return true;
            }
        }

    }

    public static class RoomStatusQRCodeParser implements QRCodeParser {

        public String room;

        @Override
        public boolean isCorrectFormat(String result) {

            Pattern pattern = Pattern.compile("roomstatus|LOC([0-9a-zA-Z\\-]+)");

            try {
                Matcher matcher = pattern.matcher(result);
                if (matcher.groupCount() == 1) {
                    matcher.find();
                    matcher.find();
                    room = matcher.group(1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (McUtils.isNullOrEmpty(room)) {
                return false;
            } else {
                return true;
            }
        }

    }

    public static class MinibarQRCodeParser implements QRCodeParser {

        public String room;

        @Override
        public boolean isCorrectFormat(String result) {

            Pattern pattern = Pattern.compile("minibar|LOC([0-9a-zA-Z\\-]+)");

            try {
                Matcher matcher = pattern.matcher(result);
                if (matcher.groupCount() == 1) {
                    matcher.find();
                    matcher.find();
                    room = matcher.group(1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (McUtils.isNullOrEmpty(room)) {
                return false;
            } else {
                return true;
            }
        }

    }

    //add hotelId
    public static class ConfigUrlQRCodeParser implements QRCodeParser {

        public String url;

        @Override
        public boolean isCorrectFormat(String result) {

            if (McUtils.isNullOrEmpty(result)) {
                return false;
            } else {
                if (URLUtil.isHttpUrl(result) || URLUtil.isHttpsUrl(result)) {
                    url = result;
                    return true;
                }
                return false;
            }
        }

    }

    public static class GroupLocationQRCodeParser implements QRCodeParser {

        public String room;

        @Override
        public boolean isCorrectFormat(String result) {

            return true;
//			Pattern pattern = Pattern.compile("minibar|LOC([0-9a-zA-Z\\-]+)");
//
//			try {
//				Matcher matcher = pattern.matcher(result);
//				if (matcher.groupCount() == 1) {
//					matcher.find();
//					matcher.find();
//					room = matcher.group(1);
//				}
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//
//			if (McUtils.isNullOrEmpty(room)) {
//				return false;
//			} else {
//				return true;
//			}
        }

    }

}
