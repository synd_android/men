package com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice;

import java.io.Serializable;

import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;

public class Group implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -5893637949020931521L;
    private String groupName;
    private Long groupId;

    public Group() {
    }

    public Group(SoapObject soap) {
        this.groupId = Long.valueOf(soap.getAttribute("groupId").toString());
        if (soap.hasProperty("groupName") && (soap.getProperty("groupName") instanceof SoapPrimitive)) {
            this.groupName = ((SoapPrimitive) soap.getProperty("groupName")).toString();
        }
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

}