package com.fcscs.android.mconnectv3.common.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.util.McUtils;

import java.io.File;

public class ImagePreviewLayout extends LinearLayout {

    private static final String TAG = ImagePreviewLayout.class.getSimpleName();

    private ImageView ivDelete;
    private ImageView ivImage;
    private Context context;
    private View view;
    private int screenWidth;
    private int screenHeight;
    private OnClickListener deleteListener;
    private OnClickListener imageListener;

    public ImagePreviewLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.common_image_preview_layout, this);

        if (!isInEditMode()) {
            ivImage = (ImageView) findViewById(R.id.image);
            ivDelete = (ImageView) findViewById(R.id.delete);
            ivDelete.setVisibility(View.GONE);
            view.setVisibility(View.GONE);
            ivImage.setImageBitmap(null);
        }
    }


    public void setImageFile(File file) {
        if (file != null && file.exists() && file.length() > 0) {
            Bitmap bitmap = McUtils.decodeSampledBitmapFromResource(file, 1000, 1000);
            if (bitmap != null) {
                ivImage.setImageBitmap(bitmap);
                view.setVisibility(View.VISIBLE);
            } else {
                view.setVisibility(View.GONE);
            }
        } else {
            view.setVisibility(View.GONE);
        }
    }

    public void setDemoImage() {
        ivImage.setImageResource(R.drawable.captureimagedemo);
        view.setVisibility(View.VISIBLE);
    }

    public void setAllowDelete(boolean allow) {
        if (allow) {
            ivDelete.setVisibility(View.VISIBLE);
        } else {
            ivDelete.setVisibility(View.GONE);
        }
    }

    public void setOnClickDelete(OnClickListener l) {
        this.deleteListener = l;
        ivDelete.setOnClickListener(deleteListener);
    }

    public void setOnClickImage(OnClickListener l) {
        this.imageListener = l;
        ivImage.setOnClickListener(imageListener);
    }
}
