package com.fcscs.android.mconnectv3.ws.cconnect;

import java.util.List;

import org.ksoap2.serialization.SoapObject;

import android.content.Context;

import com.fcscs.android.mconnectv3.common.SessionContext;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.dao.entity.DBHelper;
import com.fcscs.android.mconnectv3.dao.entity.Location;
import com.fcscs.android.mconnectv3.http.WebServiceBase;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.GetBuildingsResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.GetFloorsResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.GetLocationsResponse;

public class CommonRequestService extends WebServiceBase {

    private static final String WS_SUFFIX = "ws/coreWSService";
    private static final String GENERALNAMESPACE = "http://fcscs.com/ws/schemas/generalservice";

    private static final String GETBUILDINGREQUESTNAME = "GetBuildingsRequest";
    private static final String GETBUILDINGRESPONSENAME = "GetBuildingsResponse";

    private static final String GETFLOORREQUESTNAME = "GetFloorsRequest";
    private static final String GETFLOORRESPONSENAME = "GetFloorsResponse";

    private static final String GETLOCATIONREQUESTNAME = "GetLocationsRequest";
    private static final String GETLOCATIONRESPONSENAME = "GetLocationsResponse";

    private static CommonRequestService instance;

    private CommonRequestService() {
    }

    public static CommonRequestService getInstance() {
        if (instance == null) {
            instance = new CommonRequestService();
        }
        instance.setEnableUI(true);
        return instance;
    }

    public GetBuildingsResponse findBuildings(Context ctx) {

        if (DBHelper.getBuildingCount(ctx) > 0L) {
            GetBuildingsResponse resp = new GetBuildingsResponse();
            resp.setBuildingList(DBHelper.queryBuildings(ctx));
            return resp;
        }

        SoapObject soap = callWebService(ctx,
                commonMethod(GENERALNAMESPACE, GETBUILDINGREQUESTNAME), WS_SUFFIX, GENERALNAMESPACE, GETBUILDINGRESPONSENAME);
        if (soap != null) {
            GetBuildingsResponse resp = new GetBuildingsResponse(soap);
            DBHelper.saveBuildings(ctx, resp.getBuildingList());
            return resp;
        } else {
            return null;
        }
    }

    public GetFloorsResponse findFloors(Context ctx) {

        if (DBHelper.getFloorCount(ctx) > 0L) {
            GetFloorsResponse resp = new GetFloorsResponse();
            resp.setFloorList(DBHelper.queryFloors(ctx, null));
            return resp;
        }

        SoapObject soap = callWebService(ctx,
                commonMethod(GENERALNAMESPACE, GETFLOORREQUESTNAME), WS_SUFFIX, GENERALNAMESPACE, GETFLOORRESPONSENAME);
        if (soap != null) {
            GetFloorsResponse resp = new GetFloorsResponse(soap);
            DBHelper.saveFloors(ctx, resp.getFloorList());
            return resp;
        } else {
            return null;
        }

    }

    public GetLocationsResponse findLocations(Context ctx, int PageIndex, String term, Long buildingId, Long floorId) {

        //if (DBHelper.getLocationCount(ctx) == 0L) {

        int intPageIndex = PageIndex;
        long lngDebug1 = SessionContext.getInstance().getFcsUserId();
        String strDebug2 = SessionContext.getInstance().getWsSessionId();
        String strDebug3 = GENERALNAMESPACE;
        String strDebug4 = GETLOCATIONREQUESTNAME;

        SoapObject request = new SoapObject(GENERALNAMESPACE, GETLOCATIONREQUESTNAME);

        request.addAttribute("fcsUserId", SessionContext.getInstance().getFcsUserId());
        request.addAttribute("wsSessionId", SessionContext.getInstance().getWsSessionId());
        //request.addAttribute("start", 0);
        request.addAttribute("start", intPageIndex);
        request.addAttribute("numResults", 10000);

        SoapObject soap = callWebService(ctx, request, WS_SUFFIX, GENERALNAMESPACE, GETLOCATIONRESPONSENAME);
        if (soap != null) {
            GetLocationsResponse resp = new GetLocationsResponse(soap);
            DBHelper.saveLocations(ctx, resp.getLocationList());
        }

        //}

        //..Modify By 	: Chua Kam Hoong
        //..Date 		: 20 May 2015
        //List<Location> list = DBHelper.queryLocations(ctx, McUtils.convertToString(buildingId), McUtils.convertToString(floorId), term);
        //List<Location> list = DBHelper.queryLocations(ctx, 0, 0, McUtils.convertToString(buildingId), McUtils.convertToString(floorId), term);
        List<Location> list = DBHelper.queryLocations(ctx, PageIndex, McUtils.convertToString(buildingId), McUtils.convertToString(floorId), term);

        GetLocationsResponse resp = new GetLocationsResponse();
        resp.setLocationList(list);
        resp.setTotal(list.size());

        return resp;

    }

    private SoapObject commonMethod(String nameSpace, String requestName) {

        SoapObject request = new SoapObject(nameSpace, requestName);
        request.addAttribute("fcsUserId", SessionContext.getInstance().getFcsUserId());
        request.addAttribute("wsSessionId", SessionContext.getInstance().getWsSessionId());

        return request;
    }
}
