package com.fcscs.android.mconnectv3.common.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.util.McUtils;

import java.io.File;

public class ImagePagerLayout extends LinearLayout {

    private static final String TAG = ImagePagerLayout.class.getSimpleName();

    private Context context;
    private View view;
    private ViewPager viewPager;

    public interface OnClickImageListener {
        void onClick(View v, String imageFile);
    }

    public static OnClickImageListener onClickListener;

    public ImagePagerLayout(final Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.common_image_pager_layout, this);

        if (!isInEditMode()) {
            view.setVisibility(View.GONE);
            viewPager = (ViewPager) findViewById(R.id.viewPager);
            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    int count = viewPager.getChildCount();

                    if (position == 0 && count == 1) {
                        enableArrow(false, false);
                    } else if (position == 0 && count > 1) {
                        enableArrow(false, true);
                    } else if (position > 0 && position < count - 1) {
                        enableArrow(true, true);
                    } else {
                        enableArrow(true, false);
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });

            view.findViewById(R.id.arrowLeft).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    viewPager.setCurrentItem(viewPager.getCurrentItem() - 1, true);
                }
            });

            view.findViewById(R.id.arrowRight).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);
                }
            });
        }
    }

    public void setDemoImage(FragmentManager fragmentManager) {
        view.setVisibility(View.VISIBLE);
        enableArrow(false, false);
        viewPager.setAdapter(new ImageFragmentPagerAdapter(fragmentManager));
    }

    public void setImageFiles(FragmentManager fragmentManager, String[] files) {
        if (files != null && files.length > 0) {
            view.setVisibility(View.VISIBLE);
            if (files.length == 1) {
                enableArrow(false, false);
            } else {
                enableArrow(false, true);
            }
            viewPager.setAdapter(new ImageFragmentPagerAdapter(fragmentManager, files));
        } else {
            view.setVisibility(View.GONE);
        }
    }

    private void enableArrow(boolean enableLeft, boolean enableRight) {
        view.findViewById(R.id.arrowLeft).setEnabled(enableLeft);
        view.findViewById(R.id.arrowLeft).setVisibility(enableLeft ? VISIBLE : INVISIBLE);

        view.findViewById(R.id.arrowRight).setEnabled(enableRight);
        view.findViewById(R.id.arrowRight).setVisibility(enableRight ? VISIBLE : INVISIBLE);
    }

    private class ImageFragmentPagerAdapter extends FragmentPagerAdapter {

        private String[] files;
        private boolean isDemo;

        public ImageFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
            this.isDemo = true;
        }

        public ImageFragmentPagerAdapter(FragmentManager fm, String[] files) {
            super(fm);
            this.files = files;
            this.isDemo = false;
        }

        @Override
        public int getCount() {
            if (isDemo) {
                return 1;
            }
            return files != null ? files.length : 0;
        }

        @Override
        public Fragment getItem(int position) {
            if (isDemo) {
                return SwipeFragment.newInstance();
            }
            return SwipeFragment.newInstance(files, position);
        }
    }

    public static class SwipeFragment extends Fragment {

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View swipeView = inflater.inflate(R.layout.common_image_simple_layout, container, false);
            ImageView imageView = (ImageView) swipeView.findViewById(R.id.image);
            try {
                Bundle bundle = getArguments();
                if (bundle.containsKey("POSITION") && bundle.containsKey("FILES")) {
                    final int position = bundle.getInt("POSITION");
                    final String file = bundle.getStringArray("FILES")[position];
                    Bitmap bitmap = McUtils.decodeSampledBitmapFromResource(new File(file), 1000, 1000);
                    imageView.setImageBitmap(McUtils.rotateImage(bitmap,90));

                    imageView.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (onClickListener != null) {
                                onClickListener.onClick(view, file);
                            }
                        }
                    });
                } else {
                    imageView.setImageResource(R.drawable.captureimagedemo);

                    imageView.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (onClickListener != null) {
                                onClickListener.onClick(view, null);
                            }
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return swipeView;
        }

        static SwipeFragment newInstance() {
            SwipeFragment swipeFragment = new SwipeFragment();
            Bundle bundle = new Bundle();
            swipeFragment.setArguments(bundle);
            return swipeFragment;
        }

        static SwipeFragment newInstance(String[] files, int position) {
            SwipeFragment swipeFragment = new SwipeFragment();
            Bundle bundle = new Bundle();
            bundle.putStringArray("FILES", files);
            bundle.putInt("POSITION", position);
            swipeFragment.setArguments(bundle);
            return swipeFragment;
        }
    }

    public void setOnClickImage(OnClickImageListener listener) {
        onClickListener = listener;
    }
}
