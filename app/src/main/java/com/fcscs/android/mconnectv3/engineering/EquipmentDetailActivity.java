package com.fcscs.android.mconnectv3.engineering;

import android.content.Context;
import android.os.Bundle;
import android.app.Activity;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.BaseActivity;
import com.fcscs.android.mconnectv3.common.ui.HomeTopBar;
import com.fcscs.android.mconnectv3.common.ui.McBackgroundAsyncTask;
import com.fcscs.android.mconnectv3.manager.model.EquipmentModel;
import com.fcscs.android.mconnectv3.manager.model.JobModel;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.GetEquipmentResponse;
import com.fcscs.android.mconnectv3.ws.econnect.ECService;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.UpdateableJobModel;

import java.util.List;

public class EquipmentDetailActivity extends BaseActivity {
    @Override
    public boolean isLoginActivity() {
        return false;
    }

    @Override
    public boolean isHomeActivity() {
        return false;
    }

    public static final String KEY_JOBMODEL = "JobModel";
    private JobModel jm;

    private EditText edEquip;
    private EditText edEquipNo;
    private EditText edLocation;
    private EditText edStatus;
    private EditText edPm;
    private EditText edRemarks;
    private EditText edGroup;
    private EditText edTag;
    private EditText edManufacture;
    private EditText edModel;
    private EditText edSerialNo;
    private HomeTopBar topBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_equipment_detail);
        topBar = (HomeTopBar) findViewById(R.id.top_bar);
        topBar.getTitleTv().setText(R.string.asset_Detail);

        edEquip = (EditText) findViewById(R.id.ed_equip_detail_equip);
        edEquipNo = (EditText) findViewById(R.id.ed_equip_detail_equipno);
        edLocation = (EditText) findViewById(R.id.ed_equip_detail_location);
        edPm = (EditText) findViewById(R.id.ed_equip_detail_pm);
        edStatus = (EditText) findViewById(R.id.ed_equip_detail_status);
        edRemarks = (EditText) findViewById(R.id.ed_equip_detail_remark);
        edGroup = (EditText) findViewById(R.id.ed_equip_detail_group);
        edTag = (EditText) findViewById(R.id.ed_equip_detail_tag);
        edManufacture = (EditText) findViewById(R.id.ed_equip_detail_manufacture);
        edModel = (EditText) findViewById(R.id.ed_equip_detail_model);
        edSerialNo = (EditText) findViewById(R.id.ed_equip_detail_serialno);

        Bundle bundle = getIntent().getExtras();
        if (bundle.get(KEY_JOBMODEL) != null) {
            jm = (JobModel) bundle.get(KEY_JOBMODEL);
            new loadEquipDetail(this).execute();
        }

    }

    private class loadEquipDetail extends McBackgroundAsyncTask {
        Context mContext;
        private int returnCode = -1;

        public loadEquipDetail(Context context) {
            super(context);
            mContext = context;
        }

        GetEquipmentResponse mRes;
        EquipmentModel equipmentModel;

        @Override
        public void doInBackground() {
            mRes = ECService.getInstance().getEquipmentDetail(mContext, jm.getJobId() + "");
        }

        @Override
        public void onPostExecute() {
            if (mRes != null) {
                returnCode = mRes.getCode();
                if (returnCode != 1) {
                    equipmentModel = mRes.getEquipmentModel();
                    setDataToView(equipmentModel);
                }

            }
        }
    }

    private void setDataToView(EquipmentModel equipmentModel) {
        edEquip.setText(equipmentModel.getEquipmentName());
        edEquipNo.setText(equipmentModel.getEquipmentNumber());
        edLocation.setText(equipmentModel.getLocation());
        edStatus.setText(equipmentModel.getEquipmentStatus());
        edPm.setText(equipmentModel.getJobStatus());
        edRemarks.setText(equipmentModel.getJobDetails());
        edGroup.setText(equipmentModel.getGroup());
        edTag.setText(equipmentModel.getTag());
        edManufacture.setText(equipmentModel.getManufacturer());
        edModel.setText(equipmentModel.getModel());
        edModel.setText(equipmentModel.getSerialNumber());
    }


}
