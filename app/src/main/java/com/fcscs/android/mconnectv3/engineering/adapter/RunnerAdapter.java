package com.fcscs.android.mconnectv3.engineering.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.TextView;

import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.EngineeringRunnersDetails;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by FCS on 5/4/17.
 */

public class RunnerAdapter extends BaseAdapter {
    List<EngineeringRunnersDetails> mList;
    Context mContext;

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public List<EngineeringRunnersDetails> getList() {
        return mList;
    }

    public RunnerAdapter(List<EngineeringRunnersDetails> mList, Context mContext) {
        this.mList = mList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(android.R.layout.simple_spinner_item, parent, false);
            viewHolder.tvTitle = (TextView) convertView.findViewById(android.R.id.text1);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.tvTitle.setText(mList.get(position).getRunnerUserFullName());
        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        final ViewHolderDropDown viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolderDropDown();
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
            viewHolder.checkedTextView = (CheckedTextView) convertView.findViewById(android.R.id.text1);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolderDropDown) convertView.getTag();
        }
        viewHolder.checkedTextView.setText(mList.get(position).getRunnerUserFullName());
        return convertView;
    }

    private class ViewHolderDropDown {
        CheckedTextView checkedTextView;
    }

    private class ViewHolder {
        TextView tvTitle;
    }
}

