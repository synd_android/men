package com.fcscs.android.mconnectv3.common;

import java.util.Locale;
import java.util.Observable;
import java.util.Observer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

//import com.fcs.demo.autogate.AutoUpdateApk;
import com.fcscs.android.mconnectv3.McApplication;
import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.cache.ObjectCache;
import com.fcscs.android.mconnectv3.common.McEnums.ServiceType;
import com.fcscs.android.mconnectv3.common.ui.HomeTopBar;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.common.util.PrefsUtil;

public abstract class BaseActivity extends FragmentActivity {
//		implements Observer {

    private static final String TAG = BaseActivity.class.getSimpleName();
    protected Locale mLocale;
    private String languageCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new McExceptionHandler(this));

        mLocale = PrefsUtil.getLocale();
        languageCode = PrefsUtil.getLanguageCode(this);
        Configuration config = getResources().getConfiguration();
        config.locale = mLocale;
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
//		McApplication.application.addObserver(this);

    }

    protected void setTextViewString(TextView v, String resourceName, int resourceId) {
        String text = McUtils.getResourceString(this, languageCode, resourceName, resourceId);
        v.setText(text);
    }

    protected void setTextViewString(int textViewId, String resourceName, int resourceId) {
        TextView v = (TextView) findViewById(textViewId);
        if (v != null) {
            setTextViewString(v, resourceName, resourceId);
        }
    }

    protected void toast(String resourceName, int resourceId) {
        String text = McUtils.getResourceString(this, languageCode, resourceName, resourceId);
        toast(text);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (getApp().isRedirect() && isLoginActivity() && !isHomeActivity()) {
            finish();
        }
        if (isLoginActivity() && isHomeActivity()) {
            getApp().setRedirect(false);
        }

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        View tb = findViewById(R.id.top_bar);
        if (tb != null && tb instanceof HomeTopBar) {
            Button btn = (Button) findViewById(R.id.btn_top_home);
            btn.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    hideSoftInput(v);
                    onBackPressed();
                }
            });
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (getApp().isRedirect() && isLoginActivity() && !isHomeActivity()) {
            finish();
        }
        if (isLoginActivity() && isHomeActivity()) {
            getApp().setRedirect(false);
        }
    }

    @Override
    protected void onDestroy() {
        Log.i(TAG, "destroying " + getClass().getCanonicalName());
        unbindViews();
        super.onDestroy();
    }

    protected void unbindViews() {
        View view = findViewById(R.id.layout_root);
        if (view != null) {
            Log.i(TAG, "recycling bitmap " + getClass().getCanonicalName());
            unbindDrawables(view);
        }
    }

    private void unbindDrawables(View view) {
        if (view != null) {
            if (view.getBackground() != null) {
                view.getBackground().setCallback(null);
            }
            if (view instanceof AdapterView) {
                for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                    View child = ((ViewGroup) view).getChildAt(i);
                    unbindDrawables(child);
                    if (child instanceof ImageView == false) {
                        continue;
                    }
                    try {
                        ImageView tmp = (ImageView) child;
                        tmp.getDrawingCache();
                        tmp.getDrawingCache().recycle();
                        tmp.destroyDrawingCache();
                        tmp.setImageBitmap(null);
                        tmp.setImageDrawable(null);
                    } catch (Exception e) {
                    }
                }

            } else if (view instanceof ViewGroup) {
                for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                    View child = ((ViewGroup) view).getChildAt(i);
                    unbindDrawables(child);
                    if (child instanceof ImageView == false) {
                        continue;
                    }
                    try {
                        ImageView tmp = (ImageView) child;
                        tmp.getDrawingCache().recycle();
                        tmp.destroyDrawingCache();
                        tmp.setImageBitmap(null);
                        tmp.setImageDrawable(null);
                    } catch (Exception e) {

                    }
                }
            }

        }

    }

    @Override
    protected void onPause() {
        Log.i(TAG, "pausing " + getClass().getCanonicalName());
        super.onPause();
    }

    public abstract boolean isLoginActivity();

    public abstract boolean isHomeActivity();

    protected void hideSoftInput(View v) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    protected void showSoftInput(View v) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT);
    }

    protected void focusEditText(EditText et) {
        et.requestFocus();
        showSoftInput(et);
    }

    protected void toast(CharSequence info) {
        Toast.makeText(this, info, Toast.LENGTH_LONG).show();
    }

    protected void toast(int resId) {
        Toast.makeText(this, resId, Toast.LENGTH_LONG).show();
    }

    public boolean isEconnect() {
        return ServiceType.ECONNECT.equals(ConfigContext.getInstance().getServiceType());
    }

    public boolean isCConnect() {
        return ServiceType.CCONNECT.equals(ConfigContext.getInstance().getServiceType());
    }

    public McApplication getApp() {
        return (McApplication) getApplication();
    }

    public SessionContext getSessionCtx() {
        return SessionContext.getInstance();
    }

    public ConfigContext getConfigCtx() {
        return ConfigContext.getInstance();
    }

    public ObjectCache getSessionCache() {
        return getApp().getSessionCache();
    }

    public ObjectCache getAppCache() {
        return getApp().getAppCache();
    }

    public Context getCurrentContext() {
        return this;
    }
//	@Override
//	public void update(Observable observable, Object data) {
//		if( ((String)data).equalsIgnoreCase(AutoUpdateApk.AUTOUPDATE_GOT_UPDATE) ) {
//			android.util.Log.i("AutoUpdateApkActivity", "Have just received update!");
//			if (!isFinishing()) {
//				McApplication.application.showDialog(BaseActivity.this);
//			}
//		}
//		if( ((String)data).equalsIgnoreCase(AutoUpdateApk.AUTOUPDATE_HAVE_UPDATE) ) {
//			android.util.Log.i("AutoUpdateApkActivity", "There's an update available!");
//		}
//	}

}
