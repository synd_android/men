package com.fcscs.android.mconnectv3.common.ui;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.fcscs.android.mconnectv3.McApplication;
import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.cache.DrawableCache;

public abstract class RoundCornerListAdapter<E> extends BaseAdapter {

    protected List<E> mList;
    private int mLayout;
    private Context mContext;
    private LayoutInflater mInflater;
    private Drawable mRoundCornerTop = null;
    private Drawable mRoundCornerMiddle = null;
    private Drawable mRoundCornerBottom = null;
    private Drawable mRoundBackground = null;

    public RoundCornerListAdapter(Context context, List<E> list, int layout) {
        super();
        this.mContext = context;
        if (list == null) {
            list = new ArrayList<E>();
        }
        this.mList = list;
        this.mLayout = layout;
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mRoundCornerTop = getDrawableCache().get(context, R.drawable.list_top_selector);
        mRoundCornerMiddle = getDrawableCache().get(context, R.drawable.list_middle_selector);
        mRoundCornerBottom = getDrawableCache().get(context, R.drawable.list_bottom_selector);
        mRoundBackground = getDrawableCache().get(context, R.drawable.circle_listbg);

    }

    public McApplication getApp() {
        return (McApplication) (mContext == null ? null : mContext.getApplicationContext());
    }

    public DrawableCache getDrawableCache() {
        return getApp() == null ? null : getApp().getDrawableCache();
    }

    public List<E> getItemList() {
        return mList;
    }

    public void setItemList(List<E> list) {
        this.mList = list;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public E getItem(int position) {
        if (position >= mList.size()) {
            return null;
        }
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        if (position >= mList.size()) {
            return -1;
        }
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v;
        if (convertView == null) {
            v = newView(mContext, position, parent);
        } else {
            v = convertView;
        }
        setBackground(v, mContext, position);
        onBindView(v, mContext, position, getItem(position));
        return v;
    }

    public View newView(Context context, int position, ViewGroup parent) {
        View v = mInflater.inflate(mLayout, parent, false);
        beforeBindView(v, context, position, getItem(position));
        return v;
    }

    public void beforeBindView(View view, Context context, int position, E item) {

    }

    private void setBackground(View view, Context context, int position) {
        int count = getCount();
        if (count == 1) {
            view.setBackgroundDrawable(mRoundBackground);
        } else if (count == 2) {
            if (position == 0) {
                view.setBackgroundDrawable(mRoundCornerTop);
            } else {
                view.setBackgroundDrawable(mRoundCornerBottom);
            }
        } else if (count > 2) {
            if (position == 0) {
                view.setBackgroundDrawable(mRoundCornerTop);
            } else if (position == (count - 1)) {
                view.setBackgroundDrawable(mRoundCornerBottom);
            } else {
                view.setBackgroundResource(R.drawable.list_middle_selector);
            }
        }
    }


    public abstract void onBindView(View view, Context context, int position, final E item);


}
