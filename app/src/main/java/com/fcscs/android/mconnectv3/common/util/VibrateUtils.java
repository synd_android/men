package com.fcscs.android.mconnectv3.common.util;

import android.content.Context;
import android.os.Vibrator;

public class VibrateUtils {
    public static void vibrate(Context ctx) {
        if (PrefsUtil.isEnableVibration(ctx)) {
            Vibrator v = (Vibrator) ctx.getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(100);
        }
    }

    public static void needVibrate(Context ctx) {
        vibrate(ctx);
    }
}
