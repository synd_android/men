package com.fcscs.android.mconnectv3.main;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.fcscs.android.mconnectv3.McApplication;
import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.BaseActivity;
import com.fcscs.android.mconnectv3.common.McConstants;
import com.fcscs.android.mconnectv3.common.McEnums.JobStatus;
import com.fcscs.android.mconnectv3.common.ui.HomeTopBar;
import com.fcscs.android.mconnectv3.common.ui.McDatePickerDialog;
import com.fcscs.android.mconnectv3.common.ui.McProgressAsyncTask;
import com.fcscs.android.mconnectv3.common.ui.RoundCornerListAdapter;
import com.fcscs.android.mconnectv3.common.util.DateTimeHelper;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.common.util.PrefsUtil;
import com.fcscs.android.mconnectv3.manager.model.JobModel;
import com.fcscs.android.mconnectv3.manager.model.StatusModel;
import com.fcscs.android.mconnectv3.ws.cconnect.GuestRequestService;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.GetJobSummaryResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.JobViewResponse;
import com.fcscs.android.mconnectv3.ws.econnect.ECService;

public class MyJobsActivity extends BaseActivity {

    private EditText dateEt;
    private ListView statusLv;
    private static final int REQ_CODE_SEARCH_ITEMS = 2;
    private List<StatusModel> statusModels = new ArrayList<StatusModel>();
    private JobStatusListAdapter adapter;
    private HomeTopBar topBar;
    private JobStatus status;
    private TextView pending_all;
    public final static String COMPLETED_JOB = "COMPLETED_JOB";
//	boolean isCompletedJob = true;


    private Calendar dateAndTime = Calendar.getInstance(Locale.US);
    private DatePickerDialog.OnDateSetListener d = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            dateAndTime.set(Calendar.YEAR, year);
            dateAndTime.set(Calendar.MONTH, monthOfYear);
            dateAndTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            dateEt.setText(DateTimeHelper.formatDateToStr(dateAndTime.getTime(), DateTimeHelper.YYYY_MM_DD, ""));
            getJobSummary();
        }

    };

    private GetJobSummaryResponse resp;
    private Button lastDay;
    private Button nextDay;
    private boolean autoAckMyJob = false;
    private McDatePickerDialog datePickerDialog;
    protected FindJobAsyncTask task;
    private SummaryAsyncTask task2;
    private Spinner spSearch;
    int[] searctTime;
    int timeSelect = 0;
    private boolean isEngineering = false;
    LinearLayout llSpinnerTime;
    private Spinner filterTimeSp;

    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_jobs);
        isEngineering = PrefsUtil.getEnableEng(this) == 1 ? true : false;

        if (!((McApplication) getApplication()).isLogin()) {
            toast(R.string.already_logout);
        }
        Resources r = getResources();
        searctTime = r.getIntArray(R.array.array_search_date_id);

        autoAckMyJob = PrefsUtil.isAutoAckMyJob(getCurrentContext());

        topBar = (HomeTopBar) findViewById(R.id.top_bar);
        topBar.getTitleTv().setText(R.string.my_job);
        dateEt = (EditText) findViewById(R.id.et_date);
        String nowDate = DateTimeHelper.FORMATTER_DATE.format(new Date());
        dateEt.setText(nowDate);
        dateEt.setClickable(true);
        dateEt.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker();
            }
        });

        lastDay = (Button) findViewById(R.id.pre_day);
        lastDay.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dateAndTime.add(Calendar.DAY_OF_YEAR, -1);
                dateEt.setText(DateTimeHelper.formatDateToStr(dateAndTime.getTime(), DateTimeHelper.YYYY_MM_DD, ""));
                getJobSummary();
            }
        });

        nextDay = (Button) findViewById(R.id.nxt_day);
        nextDay.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dateAndTime.add(Calendar.DAY_OF_YEAR, 1);
                dateEt.setText(DateTimeHelper.formatDateToStr(dateAndTime.getTime(), DateTimeHelper.YYYY_MM_DD, ""));
                getJobSummary();
            }
        });

        statusLv = (ListView) findViewById(R.id.myjob_statusgridview);
        statusLv.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long arg3) {

                status = statusModels.get(pos).getStatus();
                int total = statusModels.get(pos).getCount();
//				check status completed.
//						JobStatus js = jm.getStatus();
//				if (JobStatus.PENDING.equals(js)) {
//					pending++;
//				} else if (JobStatus.COMPLETED.equals(js)) {

                Date date = null;
                if (McUtils.isNullOrEmpty(dateEt.getText().toString()) == false) {
                    date = DateTimeHelper.parseDateStr(dateEt.getText().toString(), DateTimeHelper.YYYY_MM_DD, null);
                }

                if (task == null || AsyncTask.Status.RUNNING.equals(task.getStatus()) == false) {
                    task = new FindJobAsyncTask(getCurrentContext(), status, date, total);
                    task.exec();
                }
            }
        });
        adapter = new JobStatusListAdapter(MyJobsActivity.this, statusModels, R.layout.jobstatus_list_item);
        statusLv.setAdapter(adapter);

        TextView searchBtn = (TextView) findViewById(R.id.my_jobs_searchBtn);
        searchBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                getJobSummary();
            }
        });
        llSpinnerTime = (LinearLayout) findViewById(R.id.job_search_ll_spinnertime);
        if (isEngineering) {
            llSpinnerTime.setVisibility(View.VISIBLE);
        }
        spSearch = (Spinner) findViewById(R.id.jobSearch_searchType_time_Spinner);
        spSearch.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                timeSelect = searctTime.length > i ? searctTime[i] : 0;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        if (isEconnect()) {
            int bIncludeEngineeringJobs = PrefsUtil.getEnableEng(MyJobsActivity.this);
            pending_all = (TextView) findViewById(R.id.pending_btn);
            if (bIncludeEngineeringJobs != 1) {
                pending_all.setVisibility(View.VISIBLE);
                pending_all.setText(R.string.pending);
                pending_all.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (task == null || AsyncTask.Status.RUNNING.equals(task.getStatus()) == false) {
                            task = new FindJobAsyncTask(getCurrentContext(), JobStatus.PENDING, null, 0);
                            task.exec();
                        }
                    }
                });
            } else {
                pending_all.setVisibility(View.GONE);
            }
        }

        getJobSummary();

    }

    private class JobStatusListAdapter extends RoundCornerListAdapter<StatusModel> {

        public JobStatusListAdapter(Context context, List<StatusModel> list, int layout) {
            super(context, list, layout);
        }

        @Override
        public void onBindView(View v, Context context, int position, StatusModel item) {

            TextView statusTv = (TextView) v.findViewById(R.id.jobstatus);
            TextView countTv = (TextView) v.findViewById(R.id.jobstatusCount);
            if (item.getStatus() != null) {
                statusTv.setText(item.getStatus().getResourceId());
            }

            StringBuffer sb = new StringBuffer();
            sb.append("(").append(item.getCount()).append(")");
            countTv.setText(sb.toString());
        }

    }

    @Override
    protected void onResume() {
        if (adapter != null && statusLv != null) {
            statusLv.setAdapter(adapter);
        }
        getJobSummary();
        super.onResume();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            setResult(RESULT_OK);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    private class FindJobAsyncTask extends McProgressAsyncTask {

        private JobStatus status;
        private Date date;
        private JobViewResponse res;

        public FindJobAsyncTask(Context context, JobStatus status, Date date, int total) {
            super(context);
            this.status = status;
            this.date = date;
        }

        @Override
        public void doInBackground() {
            if (isCConnect()) {
                res = GuestRequestService.getInstance().findJobView(getCurrentContext(), date, null, null, 0, 2000);
            } else {
                if (isEngineering) {
                    res = ECService.getInstance().getMyJobsList(getCurrentContext(), date, McUtils.getStatusID(resp, status), timeSelect + "");
                } else {
                    res = ECService.getInstance().getMyJobsList(getCurrentContext(), date, McUtils.getStatusID(resp, status), "");
                }
            }
        }

        @Override
        public void onPostExecute() {

            if (res == null || res.getJobViews().size() == 0) {
                toast(getString(R.string.no_job_found));
            } else {

                if (isCConnect()) {
//					JobStatus js = jm.getStatus();
//					if (JobStatus.PENDING.equals(js)) {
//						pending++;
//					} else if (JobStatus.COMPLETED.equals(js)) {
//
//					}

                    final Intent si = new Intent(getCurrentContext(), JobSummaryActivity.class);
                    si.putExtra(JobSummaryActivity.STATUS, status);
                    si.putExtra(McConstants.KEY_AUTO_ACK_JOB, autoAckMyJob);
                    si.putExtra(McConstants.KEY_RESULT, (ArrayList<JobModel>) res.getJobViews());
                    startActivityForResult(si, REQ_CODE_SEARCH_ITEMS);
                } else if (isEconnect()) {
                    final Intent si = new Intent(getCurrentContext(), JobSummaryActivity.class);
                    si.putExtra(JobSummaryActivity.STATUS, status);
                    si.putExtra(JobSummaryActivity.DATE, date);
                    si.putExtra(JobSummaryActivity.SEARCHDAY, timeSelect);
                    ArrayList<JobModel> jobViewModels = new ArrayList<JobModel>();
                    for (JobModel jobView : res.getJobViews()) {
                        if (!status.equals(JobStatus.ALL) && !status.equals(jobView.getStatus())) {
                            continue;
                        }
                        if ((JobStatus.PENDING.equals(jobView.getStatus()) || JobStatus.COMPLETED.equals(jobView.getStatus())
                                || JobStatus.TIMEOUT.equals(jobView.getStatus()) || JobStatus.CANCELLED.equals(jobView.getStatus()))
                                || JobStatus.DELAYED.equals(jobView.getStatus())) {
                            jobViewModels.add(jobView);
                        }
                    }

                    if (jobViewModels.size() == 0) {
                        toast(getString(R.string.no_job_found));
                        return;
                    }

                    si.putExtra(McConstants.KEY_AUTO_ACK_JOB, autoAckMyJob);
                    si.putExtra(McConstants.KEY_RESULT, jobViewModels);
                    startActivityForResult(si, REQ_CODE_SEARCH_ITEMS);
                }
            }

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == REQ_CODE_SEARCH_ITEMS) {
            boolean change = data.getBooleanExtra("change", false);
            if (change) {
                getJobSummary();
            }
        }
    }

    private class SummaryAsyncTask extends McProgressAsyncTask {

        private Date date;

        public SummaryAsyncTask(Context context) {
            super(context);

            if (McUtils.isNullOrEmpty(dateEt.getText().toString()) == false) {
                date = DateTimeHelper.parseDateStr(dateEt.getText().toString(), DateTimeHelper.YYYY_MM_DD, null);

            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            for (StatusModel m : statusModels) {
                m.setCount(0);
            }
            adapter.notifyDataSetChanged();
        }

        @Override
        public void doInBackground() {
            if (date != null) {
                if (isCConnect()) {
                    resp = GuestRequestService.getInstance().GetJobSummaryStatusCount(getCurrentContext(), date);
                } else {
//					date = DateTimeHelper.getDateByTimeZone(date);
                    if (!isEngineering) {
                        resp = ECService.getInstance().getJobSummary(getCurrentContext(), date, "");
                    } else {
                        resp = ECService.getInstance().getJobSummary(getCurrentContext(), date, timeSelect + "");
                    }
                }
            }
        }

        @Override
        public void onPostExecute() {
            if (date != null) {
                if (resp != null && resp.getStatusModels() != null) {
                    statusModels.clear();
                    statusModels.addAll(resp.getStatusModels());
                }
                adapter.notifyDataSetChanged();
            }
        }

    }

    @Override
    public boolean isLoginActivity() {
        return true;
    }

    @Override
    public boolean isHomeActivity() {
        return false;
    }

    private synchronized void showDatePicker() {

        if (datePickerDialog != null && datePickerDialog.isShowing()) {
            return;
        }
        Date currentTime = DateTimeHelper.getDateByTimeZone(dateAndTime.getTime());
        datePickerDialog = new McDatePickerDialog(MyJobsActivity.this, dateAndTime.getTime());
        datePickerDialog.setPermanentTitle(getString(R.string.set_date));
        datePickerDialog.setOnClickDone(new OnClickListener() {

            @Override
            public void onClick(View v) {
                DatePicker datePicker = datePickerDialog.getDatePicker();
                dateAndTime.set(Calendar.YEAR, datePicker.getYear());
                dateAndTime.set(Calendar.MONTH, datePicker.getMonth());
                dateAndTime.set(Calendar.DAY_OF_MONTH, datePicker.getDayOfMonth());
                dateEt.setText(DateTimeHelper.formatDateToStr(dateAndTime.getTime(), DateTimeHelper.YYYY_MM_DD, ""));
                datePickerDialog.dismiss();
                getJobSummary();
            }
        });
        datePickerDialog.show();
    }

    private void getJobSummary() {
        if (task2 == null || AsyncTask.Status.RUNNING.equals(task2.getStatus()) == false) {
            task2 = new SummaryAsyncTask(getCurrentContext());
            task2.exec();
        }
    }

}
