package com.fcscs.android.mconnectv3.manager.model;

import java.io.Serializable;

public class NonGuestItemModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1642855632464275284L;

    private String guestName;
    private long locationId;
    private long serviceItemId;
    private long serviceItemCategoryId;
    private long runnerId;
    private int qty;
    private String remarks;


    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getGuestName() {
        return guestName;
    }

    public void setGuestName(String guestName) {
        this.guestName = guestName;
    }

    public long getLocationId() {
        return locationId;
    }

    public void setLocationId(long locationId) {
        this.locationId = locationId;
    }

    public long getServiceItemId() {
        return serviceItemId;
    }

    public void setServiceItemId(long serviceItemId) {
        this.serviceItemId = serviceItemId;
    }

    public long getServiceItemCategoryId() {
        return serviceItemCategoryId;
    }

    public void setServiceItemCategoryId(long serviceItemCategoryId) {
        this.serviceItemCategoryId = serviceItemCategoryId;
    }

    public long getRunnerId() {
        return runnerId;
    }

    public void setRunnerId(long runnerId) {
        this.runnerId = runnerId;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }
}
