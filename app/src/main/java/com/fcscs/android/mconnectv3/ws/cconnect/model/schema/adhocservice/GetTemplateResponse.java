package com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;

public class GetTemplateResponse implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4156046736214983385L;
    private long templateId;
    private String templateName;
    private String detail;
    private Long groupId;
    private String groupName;
    private List<Runner> runnerList = new ArrayList<GetTemplateResponse.Runner>();

    public GetTemplateResponse() {
    }

    public GetTemplateResponse(SoapObject soap) {
        this.templateId = Long.valueOf(soap.getAttribute("templateId").toString());
        if (soap.hasProperty("templateName") && (soap.getProperty("templateName") instanceof SoapPrimitive)) {
            this.templateName = ((SoapPrimitive) soap.getProperty("templateName")).toString();
        }
        if (soap.hasProperty("detail") && (soap.getProperty("detail") instanceof SoapPrimitive)) {
            this.detail = ((SoapPrimitive) soap.getProperty("detail")).toString();
        }
        if (soap.hasProperty("groupId") && (soap.getProperty("groupId") instanceof SoapPrimitive)) {
            this.groupId = Long.valueOf(((SoapPrimitive) soap.getProperty("groupId")).toString());
        }
        if (soap.hasProperty("groupName") && (soap.getProperty("groupName") instanceof SoapPrimitive)) {
            this.groupName = ((SoapPrimitive) soap.getProperty("groupName")).toString();
        }
        if (soap.hasProperty("groupRunner")) {
            for (int i = 0; i < soap.getPropertyCount(); i++) {
                Object obj = soap.getProperty(i);
                if ((obj instanceof SoapObject)) {
                    SoapObject s = (SoapObject) obj;
                    if (s.hasAttribute("runnerId")) {
                        runnerList.add(new Runner(s));
                    }
                }
            }
        }

    }

    public long getTemplateId() {
        return templateId;
    }

    public String getTemplateName() {
        return templateName;
    }

    public String getDetail() {
        return detail;
    }

    public Long getGroupId() {
        return groupId;
    }

    public String getGroupName() {
        return groupName;
    }


    public List<Runner> getRunnerList() {
        return runnerList;
    }


    public class Runner implements Serializable {

        /**
         *
         */
        private static final long serialVersionUID = 5146481696474964713L;
        private long runnerId;
        private String firstName;
        private String lastName;
        private String username;

        public Runner(SoapObject soap) {
            this.runnerId = Long.valueOf(soap.getAttribute("runnerId").toString());
            if (soap.hasProperty("firstName") && (soap.getProperty("firstName") instanceof SoapPrimitive)) {
                this.firstName = ((SoapPrimitive) soap.getProperty("firstName")).toString();
            }
            if (soap.hasProperty("lastName") && (soap.getProperty("lastName") instanceof SoapPrimitive)) {
                this.lastName = ((SoapPrimitive) soap.getProperty("lastName")).toString();
            }
            if (soap.hasProperty("username") && (soap.getProperty("username") instanceof SoapPrimitive)) {
                this.username = ((SoapPrimitive) soap.getProperty("username")).toString();
            }
        }

        public long getRunnerId() {
            return runnerId;
        }

        public String getFirstName() {
            return firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public String getUsername() {
            return username;
        }

    }

}
