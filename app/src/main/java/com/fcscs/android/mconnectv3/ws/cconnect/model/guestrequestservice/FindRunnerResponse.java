package com.fcscs.android.mconnectv3.ws.cconnect.model.guestrequestservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;

public class FindRunnerResponse implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8220410289182942415L;

    private List<Runner> runners = null;

    public FindRunnerResponse() {
        runners = new ArrayList<Runner>();
    }

    public FindRunnerResponse(SoapObject soap) {
        runners = new ArrayList<Runner>();
        if (soap.getPropertyCount() > 0) {
            for (int i = 0; i < soap.getPropertyCount(); i++) {
                runners.add(new Runner((SoapObject) soap.getProperty(i)));
            }
        }
    }

    public List<Runner> getRunners() {
        return runners;
    }


    public class Runner implements Serializable {

        /**
         *
         */
        private static final long serialVersionUID = 8368277249476775045L;

        private long runnerId;
        private String fullname;
        private String departmentName;
        private String departmentNameSec;

        public Runner() {
        }

        public Runner(SoapObject soap) {
            if (soap.hasAttribute("runnerId")) {
                this.runnerId = Long.valueOf(soap.getAttribute("runnerId").toString());
            }
            if (soap.hasProperty("fullname") && (soap.getProperty("fullname") instanceof SoapPrimitive)) {
                this.fullname = ((SoapPrimitive) soap.getProperty("fullname")).toString();
            }
            if (soap.hasProperty("departmentName") && (soap.getProperty("departmentName") instanceof SoapPrimitive)) {
                this.departmentName = ((SoapPrimitive) soap.getProperty("departmentName")).toString();
            }
            if (soap.hasProperty("departmentNameSec") && (soap.getProperty("departmentNameSec") instanceof SoapPrimitive)) {
                this.departmentNameSec = ((SoapPrimitive) soap.getProperty("departmentNameSec")).toString();
            }

        }

        public long getRunnerId() {
            return runnerId;
        }

        public String getFullname() {
            return fullname;
        }

        public String getDepartmentName() {
            return departmentName;
        }

        public String getDepartmentNameSec() {
            return departmentNameSec;
        }


    }


}
