package com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.ksoap2.serialization.SoapObject;


public class FindRunnerGroupResponse implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -9051252018551677554L;
    private List<Group> groupList = new ArrayList<Group>();

    public FindRunnerGroupResponse() {
    }

    public FindRunnerGroupResponse(SoapObject soap) {
        if (soap.getPropertyCount() > 0) {
            for (int i = 0; i < soap.getPropertyCount(); i++) {
                groupList.add(new Group((SoapObject) soap.getProperty(i)));
            }
        }
    }

    public List<Group> getGroupList() {
        return groupList;
    }

    public void setGroupList(List<Group> groupList) {
        this.groupList = groupList;
    }


}
