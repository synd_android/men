package com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice;

import com.fcscs.android.mconnectv3.common.util.SoapConverter;
import com.fcscs.android.mconnectv3.manager.model.EquipmentModel;
import com.fcscs.android.mconnectv3.manager.model.JobModel;

import org.ksoap2.serialization.SoapObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GetEquipmentResponse implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7346682385149288200L;

    EquipmentModel equipmentModel;
    private int code;

    public GetEquipmentResponse() {
        equipmentModel = new EquipmentModel();
    }

    public GetEquipmentResponse(SoapObject soap) {
        equipmentModel = new EquipmentModel();
    }

    public EquipmentModel getEquipmentModel() {
        return equipmentModel;
    }

    public void setEquipmentModel(EquipmentModel equipmentModel) {
        this.equipmentModel = equipmentModel;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
