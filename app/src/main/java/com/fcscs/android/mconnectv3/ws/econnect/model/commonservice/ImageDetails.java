package com.fcscs.android.mconnectv3.ws.econnect.model.commonservice;

public class ImageDetails {
    private String jobNo;
    private String image;

    public String getJobNo() {
        return jobNo;
    }

    public void setJobNo(String jobNo) {
        this.jobNo = jobNo;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
