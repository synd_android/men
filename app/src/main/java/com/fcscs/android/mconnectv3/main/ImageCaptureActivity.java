package com.fcscs.android.mconnectv3.main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Intent;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.hardware.Camera;
import android.hardware.Camera.ErrorCallback;
import android.os.Bundle;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.fcscs.android.mconnectv3.McApplication;
import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.McConstants;
import com.fcscs.android.mconnectv3.common.util.DateTimeHelper;
import com.fcscs.android.mconnectv3.common.util.McUtils;

/**
 * Created by FCS on 2014/5/23.
 */
public class ImageCaptureActivity extends Activity implements SurfaceHolder.Callback, View.OnClickListener, Camera.PictureCallback {

    private static final String TAG = ImageCaptureActivity.class.getSimpleName();

    private SurfaceView mSurface;
    private SurfaceHolder mHolder;
    private TextView retake;
    private TextView usePhoto;
    private Camera mCamera;
    private boolean mPreviewRunning;
    private File takePicture;
    private int windowHeight;
    private int windowWidth;
    private Timer timer;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_capture_layout);

        getWindowInfo();

        mSurface = (SurfaceView) findViewById(R.id.camera);
        mSurface.setOnClickListener(this);

        mHolder = mSurface.getHolder();
        mHolder.addCallback(this);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        retake = (TextView) findViewById(R.id.retake);
        retake.setOnClickListener(this);
        retake.setText(getString(R.string.take));
        usePhoto = (TextView) findViewById(R.id.use_photo);
        usePhoto.setOnClickListener(this);
        usePhoto.setVisibility(View.INVISIBLE);

        timer = new Timer();
        timer.scheduleAtFixedRate(
                new TimerTask() {
                    @Override
                    public void run() {
                        try {
                            mCamera.autoFocus(null);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                0,
                5000);

    }

    protected void getWindowInfo() {
        View v = findViewById(R.id.root);
        if (v != null) {
            v.post(new Runnable() {
                @Override
                public void run() {
                    calculateSize();
                }
            });
        }
    }

    protected void calculateSize() {
        Rect rect = new Rect();
        Window window = getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(rect);

        windowHeight = rect.height();
        windowWidth = rect.width();
    }

    protected Camera openFrontFacingCamera() {
        int cameraCount = 0;
        Camera cam = null;
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        cameraCount = Camera.getNumberOfCameras();
        for (int camIdx = 0; camIdx < cameraCount; camIdx++) {
            Camera.getCameraInfo(camIdx, cameraInfo);
            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                try {
                    cam = Camera.open(camIdx);
                } catch (RuntimeException e) {
                    Log.d(TAG, "Camera failed to open: " + e.getLocalizedMessage());
                }
            }
        }

        return cam;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        mCamera = Camera.open();
        if (mCamera == null) {
            mCamera = openFrontFacingCamera();
        }
        mPreviewRunning = false;
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        if (mPreviewRunning) {
            mCamera.stopPreview();
        }

        int cameraCount = 0;
        Camera cam = null;
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        cameraCount = Camera.getNumberOfCameras();
        boolean isFoundFront = false;
        for (int camIdx = 0; camIdx < cameraCount; camIdx++) {
            Camera.getCameraInfo(camIdx, cameraInfo);
            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                isFoundFront = true;
            }
        }

        if (isFoundFront && cameraCount == 1) {
            mCamera.setDisplayOrientation(270);
        } else {
            mCamera.setDisplayOrientation(90);
        }

        Camera.Parameters p = mCamera.getParameters();
        setRotation(p);

        List<Integer> formats = p.getSupportedPictureFormats();
        if (formats != null && formats.contains(ImageFormat.JPEG)) {
            p.setPictureFormat(ImageFormat.JPEG);
            Log.d(TAG, "JPEG picture format");
        }

        List<?> focus = p.getSupportedFocusModes();
        if (focus != null && focus.contains(android.hardware.Camera.Parameters.FOCUS_MODE_AUTO)) {
            p.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
            Log.d(TAG, "focus mode auto");
        } else if (focus != null && focus.contains(android.hardware.Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
            p.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
            Log.d(TAG, "focus mode auto");
        }

        List<?> whiteMode = p.getSupportedWhiteBalance();
        if (whiteMode != null && whiteMode.contains(android.hardware.Camera.Parameters.WHITE_BALANCE_AUTO)) {
            p.setWhiteBalance(Camera.Parameters.WHITE_BALANCE_AUTO);
            Log.d(TAG, "white balance auto");
        }

        List<String> flashModes = p.getSupportedFlashModes();
        if (flashModes != null && flashModes.contains(android.hardware.Camera.Parameters.FLASH_MODE_AUTO)) {
            p.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
            Log.d(TAG, "flash mode auto");
        }

        List<Camera.Size> picSizes = p.getSupportedPictureSizes();
        if (picSizes != null) {

            Collections.sort(picSizes, new Comparator<Camera.Size>() {
                @Override
                public int compare(Camera.Size lhs, Camera.Size rhs) {
                    return rhs.width * rhs.width - lhs.width * lhs.height;
                }
            });

            Camera.Size tsize = null;
            for (Camera.Size size : picSizes) {
                if (size.width * size.height <= 1280 * 960) {
                    tsize = size;
                    break;
                }
            }

            if (tsize != null) {
                Log.d(TAG, String.format("set picture size: width=%d, height=%d", tsize.width, tsize.height));
                p.setPictureSize(tsize.width, tsize.height);
            }
        }

        mCamera.setParameters(p);

        try {
            mCamera.setPreviewDisplay(holder);
        } catch (Exception e) {
            e.printStackTrace();
            if (McConstants.ENABLE_DEBUG_MODE) {
                McUtils.saveCrashReport(this, e);
            }
        }

        try {
            mCamera.startPreview();
            mCamera.autoFocus(null);
        } catch (Exception e) {
            e.printStackTrace();
            if (McConstants.ENABLE_DEBUG_MODE) {
                McUtils.saveCrashReport(this, e);
            }
        }

        mCamera.setErrorCallback(new ErrorCallback() {

            @Override
            public void onError(int paramInt, Camera paramCamera) {
                if (McConstants.ENABLE_DEBUG_MODE) {
                    McUtils.saveLogToSDCard("Camera Error Code: " + paramInt);
                }
            }
        });
        mPreviewRunning = true;
    }

    private void setRotation(Camera.Parameters p) {
        //STEP #1: Get rotation degrees
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(Camera.CameraInfo.CAMERA_FACING_BACK, info);
        int rotation = getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break; //Natural orientation
            case Surface.ROTATION_90:
                degrees = 90;
                break; //Landscape left
            case Surface.ROTATION_180:
                degrees = 180;
                break;//Upside down
            case Surface.ROTATION_270:
                degrees = 270;
                break;//Landscape right
        }
        int rotate = (info.orientation - degrees + 360) % 360;

        //STEP #2: Set the 'rotation' parameter
        p.setRotation(rotate);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.retake:
                if (takePicture == null) {
                    try {
                        mCamera.takePicture(null, null, this);
                        mPreviewRunning = false;
                        retake.setText(R.string.retake);
                        usePhoto.setVisibility(View.VISIBLE);
                    } catch (Exception e) {
                        e.printStackTrace();
                        if (McConstants.ENABLE_DEBUG_MODE) {
                            McUtils.saveCrashReport(this, e);
                        }
                    }
                } else {
                    try {
                        takePicture = null;
                        mCamera.startPreview();
                        mPreviewRunning = true;
                        retake.setText(R.string.take);
                        usePhoto.setVisibility(View.INVISIBLE);
                    } catch (Exception e) {
                        e.printStackTrace();
                        if (McConstants.ENABLE_DEBUG_MODE) {
                            McUtils.saveCrashReport(this, e);
                        }
                    }
                }
                break;
            case R.id.use_photo:
                if (takePicture != null) {
                    Intent i = new Intent(this, ImageTagActivity.class);
                    i.putExtra("image", takePicture);
                    i.putExtra("width", windowWidth);
                    i.putExtra("height", windowHeight);
                    i.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
                    this.startActivity(i);
                    finish();
                }
                break;
            case R.id.camera:
                try {
                    mCamera.autoFocus(null);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        File file = getOutputFile();
        if (file == null) {
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(data);
            fos.close();

            Log.d(TAG, "write to file=" + file.getAbsolutePath());
            takePicture = file;
            mCamera.stopPreview();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static File getOutputFile() {
        File dir = new File(McApplication.DATA_DIRECTORY, "camera");
        if (!dir.exists()) {
            if (!dir.mkdirs()) {
                Log.w(TAG, String.format("no such dir=%s", dir.getAbsolutePath()));
                return null;
            }
        }

        SimpleDateFormat sdf = new SimpleDateFormat(DateTimeHelper.FORMAT_DATETIME_NON_S);
        File file = new File(dir, String.format("%s.jpeg", sdf.format(DateTimeHelper.getDateByTimeZone(new Date()))));
        return file;
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        mCamera.stopPreview();
        mPreviewRunning = false;
        mCamera.release();
        if (timer != null) {
            timer.cancel();
        }
    }


}