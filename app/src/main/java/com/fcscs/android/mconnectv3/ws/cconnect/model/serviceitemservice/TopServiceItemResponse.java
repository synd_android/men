package com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.ksoap2.serialization.SoapObject;


public class TopServiceItemResponse implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5947969111567017053L;

    private List<ServiceItemViewModel> serviceItems = null;

    public TopServiceItemResponse() {
        serviceItems = new ArrayList<ServiceItemViewModel>();
    }

    public TopServiceItemResponse(SoapObject soap) {
        serviceItems = new ArrayList<ServiceItemViewModel>();
        if (soap.getPropertyCount() > 0) {
            for (int i = 0; i < soap.getPropertyCount(); i++) {
                serviceItems.add(new ServiceItemViewModel((SoapObject) soap.getProperty(i)));
            }
        }
    }

    public List<ServiceItemViewModel> getServiceItems() {
        return serviceItems;
    }

    public void setServiceItems(List<ServiceItemViewModel> serviceItems) {
        this.serviceItems = serviceItems;
    }

}
