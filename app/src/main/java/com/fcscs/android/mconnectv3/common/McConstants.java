package com.fcscs.android.mconnectv3.common;

import android.os.Environment;

import com.fcscs.android.mconnectv3.zxing.camera.FrontLightMode;

import java.io.File;

public final class McConstants {

    public final static long PULL_PERIOD = 60L;
    public final static String APP_VERSION_NAME = "4.1.19";
    public final static String KEY_USERNAME = "username";
//	public final static boolean IS_MCONNECT= false;

//	public static final String GCM_SENDER_ID = "702929492786";

    public static final int WS_CONNECT_TIMEOUT = 15000; // millisecond
    public static final int WS_READ_TIMEOUT = 15000; // millisecond

    public static final boolean SAVE_CRASH_REPORT = true;
    public static final boolean SHOW_PTT = false;
//	public static final boolean ENABLE_AUTOACK = false;

    public static final String APP_DIRECTORY = Environment.getExternalStorageDirectory().getPath() + File.separator + "fcsengineeringv4";
    public static final String CAMERA_OUTPUT_LOCATION = APP_DIRECTORY + File.separator + "camera_compressed.tmp";
    public static final String CAMERA_TEMP_LOCATION = APP_DIRECTORY + File.separator + "camera_capture.tmp";
    public static final String LOG_OUTPUT_LOCATION = APP_DIRECTORY + File.separator + "log";

//	public final static String WEBSERVICE_APP_URL = "https://fcs.wynnpalace.com/ws/econnect.asmx"; //WynnPalace server


    //  public final static String WEBSERVICE_APP_URL = "http://10.8.2.158:81/ws/econnect.asmx"; // FCS QA Server
//	public final static String WEBSERVICE_APP_URL = "http://203.125.35.139/WS/eConnect.asmx";	//Sheraton Towel
//	public final static String WEBSERVICE_APP_URL = "https://210.177.86.156/ws/econnect.asmx";	//Island Shangri La
//	public final static String WEBSERVICE_APP_URL = "http://59.188.66.67:81/WS/eConnect.asmx";	//Penta Hotel HK
//	public final static String WEBSERVICE_APP_URL = "http://192.168.4.18:81/ws/econnect.asmx";	//Thailand Client internal ip
//	public final static String WEBSERVICE_APP_URL = "https://54.251.105.155/WSBeta/eConnect.asmx";	//KL AWS
//	public final static String WEBSERVICE_APP_URL = "https://210.184.6.233/WSBeta/eConnect.asmx";	//KL AWS
//	public final static String WEBSERVICE_APP_URL = "http://podiumaster.fcshosted.com/ws/econnect.asmx";	//KL AWS
//  public final static String WEBSERVICE_APP_URL = "http://210.184.6.233/WSBeta/eConnect.asmx"; //HK Temporary server
//	public final static String WEBSERVICE_APP_URL = "https://csolution.fcshosted.com/WS_cEN/WS.asmx?action=pFLEdmFbvSU7jjxwJHj0YE/fXv6UD3aB"; //local Phua server
//	public final static String WEBSERVICE_APP_URL = "https://54.255.40.150/WS_cEN/WS.asmx?action=pFLEdmFbvSU7jjxwJHj0YJVLJFgJtiMQ9Csdfwx5%2bOw%3d";
//	public final static String WEBSERVICE_APP_URL = " http://localhost:81/WS/eConnect.asmx";
//	public final static String WEBSERVICE_APP_URL = "https://122.248.193.145/WS/eConnect.asmx"; //Le Global penetration test
//  public final static String WEBSERVICE_APP_URL = "http://podiumaster.fcshosted.com/ws/econnect.asmx"; //HK Temporary server
//	public final static String WEBSERVICE_APP_URL = "http://10.146.8.223:81/WS/eConnect.asmx"; // Hilton Garden Inn Bali
//	public final static String WEBSERVICE_APP_URL = "http://211.25.48.86:81/WS/eConnect.asmx"; //One World Hotel
//    public final static String WEBSERVICE_APP_URL = "http://fcsqacengineering.fcshosted.com/WS_cEN/WS.asmx?action=pFLEdmFbvSU7jjxwJHj0YE/fXv6UD3aB"; // QA
//    public final static String WEBSERVICE_APP_URL = "http://fcsen.fcsqahosted.com/cSolution/Engineering/WS/WS.asmx?action=pFLEdmFbvSU7jjxwJHj0YJVLJFgJtiMQ9Csdfwx5%2bOw%3d"; // QA IUAT Server
    public final static String WEBSERVICE_APP_URL = "https://pmgcsolution.fcshosted.com/WS_Dev/WS.asmx?action=pFLEdmFbvSU7jjxwJHj0YJVLJFgJtiMQ9Csdfwx5%2bOw%3d"; // QA IUAT Server
//    public final static String WEBSERVICE_APP_URL = "http://fcsen.fcsqahosted.com/cSolution/Engineering/WS/WS.asmx?action=pFLEdmFbvSU7jjxwJHj0YJVLJFgJtiMQ9Csdfwx5%2bOw%3d"; // QA IUAT Server
//	public final static String WEBSERVICE_APP_URL = "https://christopher.gomez.fcshosted.com/WS/eConnect.asmx";

//  public final static String WEBSERVICE_APP_URL = "http://christopher.gomez.fcshosted.com/ws/econnect.asmx"; //HK Temporary server
//  public final static String WEBSERVICE_APP_URL = "http://christopher.gomez.fcssolution.com/ws/econnect.asmx"; //HK Temporary server
//	public final static String WEBSERVICE_APP_URL = "https://fcstest.wynnmacau.com/ws_wp/econnect.asmx"; //Wynn Palace QA Server
//	public final static String WEBSERVICE_APP_URL = "https://125.31.59.30/ws/econnect.asmx"; //Wynn Palace Production Server
//	public final static String WEBSERVICE_APP_URL = "https://fcs.wynnpalace.com/ws/econnect.asmx"; //Wynn Palace DNS Production Server
//	public final static String WEBSERVICE_APP_URL = "http://10.30.1.15:81/WS/eConnect.asmx"; //Hotel Jen Tanglin Singapore
//	public final static String WEBSERVICE_APP_URL = "";	//Nothing
//	public final static String WEBSERVICE_APP_URL = "http://46.137.211.142:81/eConnect.asmx"; // Great World Serviced Apts Singapore
//	public final static String WEBSERVICE_APP_URL = "http://star.percipia.net/star/Link/ws/ws.asmx"; //Percipia
//  public final static String WEBSERVICE_APP_URL = "https://fcsv4.app.fcscs.com/cSolution/Engineering/WS/WS.asmx?action=pFLEdmFbvSU7jjxwJHj0YFRRtt1Rjte7qWx%2fESI3WkE%3d"; // The Tanglin Club SG
//  public final static String WEBSERVICE_APP_URL = "https://fcsv4.app.fcscs.com/cSolution/Engineering/WS/WS.asmx?action=pFLEdmFbvSU5LOpILHqM%2b8Rj%2fOFLEuy2EHSRNE8O%2ffM%3d"; // Tune Hotel KLIA 2
//  public final static String WEBSERVICE_APP_URL = "http://10.168.47.8/cSolution/Engineering/WS/WS.asmx?action=pFLEdmFbvSU5LOpILHqM%2b719BKF8Gw7Bpm7fGPYWv4I%3d"; // Langham London

    // true = 22 Jan 2016 WS and above = single term ECService.getInstance().getRoomList(getCurrentContext(), SessionContext.getInstance().getFcsUserId(), term, "");
    // false = both term ECService.getInstance().getRoomList(getCurrentContext(), SessionContext.getIimport React, { Component } from 'react';
    //import { View, Text, TouchableOpacity, TextInput } from 'react-native';
    //import EStyleSheet from 'react-native-extended-stylesheet';
    //import Modal from 'react-native-modal';
    //
    //const styles = EStyleSheet.create({
    //  container: {
    //    alignSelf: 'center',
    //    justifyContent: 'center',
    //    alignItems: 'center',
    //    backgroundColor: '$lightGray',
    //    width: '$width/3.6',
    //    borderRadius: '$rem',
    //  },
    //  title: {
    //    color: '$black',
    //    fontSize: '$fontSizeNormal',
    //    paddingBottom: '$rem/2',
    //    paddingTop: '$rem',
    //    fontWeight: 'bold',
    //  },
    //  desc: {
    //    color: '$black',
    //    fontSize: '$fontSizeSmall',
    //    paddingBottom: '$rem/2',
    //  },
    //  textInput: {
    //    backgroundColor: '$white',
    //    paddingHorizontal: '$rem/2',
    //    width: '90%',
    //    height: '$rem*2',
    //    textAlign: 'left',
    //    marginVertical: '$rem/2',
    //    fontSize: '$fontSizeSemiSmall',
    //    borderWidth: 1,
    //    borderRadius: '$rem/4',
    //    borderColor: 'rgba(0,0,0,0.1)',
    //  },
    //  buttonsContainer: {
    //    flexDirection: 'row',
    //    borderTopWidth: '$borderHeight',
    //    borderColor: 'rgba(0,0,0,0.1)',
    //  },
    //  buttonContainer: {
    //    flex: 1,
    //    alignItems: 'center',
    //    paddingVertical: '$rem/1.5',
    //  },
    //  button: {
    //    color: '$lightBlue',
    //    fontSize: '$fontSizeNormal',
    //    fontFamily: '$fontSemiBold',
    //  },
    //});
    //
    //export default class VoidModal extends Component {
    //  constructor(props) {
    //    super(props);
    //
    //    this.state = { reason: '' };
    //  }
    //
    //  render() {
    //    // FIXME: find a solution to avoid rendering twice the list , loading part has problem
    //    console.log(this.props, 'voidModal');
    //    return (
    //      <Modal
    //        isVisible={this.props.modalVoidVisible}
    //        onBackdropPress={() => this.props.onClose()}
    //        animationIn="fadeIn"
    //        animationOut="fadeOut"
    //        backdropOpacity={0.4}
    //        avoidKeyboard
    //      >
    //        <View style={styles.container}>
    //          <Text style={[styles.title]}>Void Cahiering Charges</Text>
    //          <Text style={[styles.desc]}>Please state a reason for voiding</Text>
    //          <TextInput
    //            style={styles.textInput}
    //            placeholder="Reason"
    //            onChangeText={reason => this.setState({ reason })}
    //            value={this.state.reason}
    //          />
    //          <View style={styles.buttonsContainer}>
    //            <TouchableOpacity
    //              style={[
    //                styles.buttonContainer,
    //                { borderEndWidth: 1, borderColor: 'rgba(0,0,0,0.1)' },
    //              ]}
    //              onPress={() => {
    //                this.props.onClose();
    //              }}
    //            >
    //              <Text style={styles.button}>Cancel</Text>
    //            </TouchableOpacity>
    //            <TouchableOpacity
    //              style={[styles.buttonContainer]}
    //              onPress={() => {
    //                this.props.onClickOk(this.props.item, this.state.reason);
    //              }}
    //            >
    //              <Text style={styles.button}>OK</Text>
    //            </TouchableOpacity>
    //          </View>
    //        </View>
    //      </Modal>
    //    );
    //  }
    //}nstance().getFcsUserId(), term, term);
    public static final boolean UNIQUE_GETROOMSLISTING = true;

    public static final boolean DEFAULT_DEBUG_MODE = false;
    public static final boolean DEFAULT_ENABLE_SOUND_WIFI_CHECK = true;
    // 0: default is system sound | 1: notification1 | 2: notification2 ...
    public static final int DEFAULT_NOTIFICATION_SOUND_TYPE = 0;
    public static final int NOTIFICATION_SOUND_1 = 1;
    public static final int NOTIFICATION_SOUND_2 = 2;
    public static final int NOTIFICATION_SOUND_3 = 3;
    public static final int NOTIFICATION_SOUND_4 = 4;

    public static boolean ENABLE_DEBUG_MODE = false;

    public static final boolean SHOW_CONFIG_PAGE = false;

    public static boolean OFFLINE_MODE = false;

    public static final boolean DEFAULT_ENABLE_VIBRATION = true;

    public static final String KEY_AUTO_ACK_JOB = "KEY_AUTO_ACK_JOB";

    public static final String KEY_RESULT = "result";
    public static final String KEY_TOTAL = "total";
    public static final String KEY_TITLE = "title";
    public static final String KEY_TYPE = "type";
    public static final String KEY_TERM = "term";
    public static final String KEY_DEP_ID = "deptId";
    public static final int TYPE_SEARCH_ITEMS = 1;
    public static final String KEY_GUEST_INFO = "KEY_GUEST_INFO";


    //ZXing Camera Preference
    public static final boolean USE_AUTO_FOCUS = true;
    public static final boolean KEY_DISABLE_CONTINUOUS_FOCUS = true;
    public static final boolean KEY_INVERT_SCAN = false;
    public static final boolean KEY_DISABLE_BARCODE_SCENE_MODE = true;
    public static final boolean KEY_DISABLE_METERING = true;
    public static final boolean KEY_DISABLE_EXPOSURE = true;
    public static final String KEY_FRONT_LIGHT_MODE = FrontLightMode.OFF.toString();

    //ZXing decode preference
    public static final boolean KEY_DECODE_1D_PRODUCT = true;
    public static final boolean KEY_DECODE_1D_INDUSTRIAL = true;
    public static final boolean KEY_DECODE_QR = true;
    public static final boolean KEY_DECODE_DATA_MATRIX = true;
    public static final boolean KEY_DECODE_AZTEC = false;
    public static final boolean KEY_DECODE_PDF417 = false;

    //ZXing action handler
    public static final String KEY_CUSTOM_PRODUCT_SEARCH = null;

    //ZXing beep & vibration
    public static final boolean KEY_VIBRATE = false;
    public static final boolean KEY_PLAY_BEEP = true;

    //ZXing capture
    public static final boolean KEY_DISABLE_AUTO_ORIENTATION = true;
    public static final boolean KEY_COPY_TO_CLIPBOARD = true;
    public static final boolean KEY_BULK_MODE = false;
    public static final boolean KEY_AUTO_OPEN_WEB = false;
    public static final boolean KEY_SUPPLEMENTAL = true;

    public static final String LANGUAGE_DIR = APP_DIRECTORY + File.separator + "language";

    public static final McEnums.CompilationProduct COMPILATION_PRODUCT = McEnums.CompilationProduct.MENGINEERING;
    public static final boolean NON_HOTEL_PRODUCT = true;

    public static boolean IS_FIRST_ROOTED = true;

}
