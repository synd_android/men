package com.fcscs.android.mconnectv3.main;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils.TruncateAt;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.BaseActivity;
import com.fcscs.android.mconnectv3.common.ui.HomeTopBar;
import com.fcscs.android.mconnectv3.common.ui.McDatePickerDialog;
import com.fcscs.android.mconnectv3.common.ui.McProgressAsyncTask;
import com.fcscs.android.mconnectv3.common.ui.RoundCornerListAdapter;
import com.fcscs.android.mconnectv3.common.util.DateTimeHelper;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.manager.model.MessageModel;
import com.fcscs.android.mconnectv3.ws.cconnect.AdhocService;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.FindMsgResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.GetMsgResponse;
import com.fcscs.android.mconnectv3.ws.econnect.ECService;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.AckAdHocResponse;

public class MyMessageActivity extends BaseActivity {
    private static final int RC_MESSAGE = 1;
    private EditText dateEditText;
    private HomeTopBar homeTopBar;
    private ListView lv;
    private List<MessageModel> models = new ArrayList<MessageModel>();
    private MyMessageListAdapter adapter;

    private Calendar dateAndTime = Calendar.getInstance(Locale.US);
    private Button lastDay;
    private Button nextDay;
    private TextView pending_all;
    private McDatePickerDialog datePickerDialog;
    protected PendingMessageLoader task;
    private MessageListLoader task1;
    private AckAllMessageTask ackAllMessageTask;
    private TextView ackAllBtn;

    private void doSearch() {
        dateEditText.setText(DateTimeHelper.DATE_FORMATTER4.format(dateAndTime.getTime()));

        if (task1 == null || AsyncTask.Status.RUNNING.equals(task1.getStatus()) == false) {
            task1 = new MessageListLoader(getCurrentContext(), dateAndTime.getTime());
            task1.exec();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_messages);
        homeTopBar = (HomeTopBar) findViewById(R.id.top_bar);
        homeTopBar.getTitleTv().setText(getString(R.string.my_message));

        dateEditText = (EditText) findViewById(R.id.et_date);
        dateEditText.setClickable(true);
        dateEditText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                popupDatePickDialog();
            }
        });

        lastDay = (Button) findViewById(R.id.pre_day);
        lastDay.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dateAndTime.add(Calendar.DAY_OF_YEAR, -1);
                dateEditText.setText(DateTimeHelper.formatDateToStr(dateAndTime.getTime(), DateTimeHelper.YYYY_MM_DD, ""));
                doSearch();
            }
        });

        nextDay = (Button) findViewById(R.id.nxt_day);
        nextDay.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dateAndTime.add(Calendar.DAY_OF_YEAR, 1);
                dateEditText.setText(DateTimeHelper.formatDateToStr(dateAndTime.getTime(), DateTimeHelper.YYYY_MM_DD, ""));
                doSearch();
            }
        });

        lv = (ListView) findViewById(R.id.my_message_summ_gridview);
        adapter = new MyMessageListAdapter(this, models, R.layout.my_message_summary_list_item);
        lv.setAdapter(adapter);
        lv.setEmptyView(findViewById(R.id.tv_empty));

        TextView searchBtn = (TextView) findViewById(R.id.my_message_searchBtn);
        searchBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                doSearch();
            }
        });

        ackAllBtn = (TextView) findViewById(R.id.ack_all);
        ackAllBtn.setText(R.string.ack_all);
        ackAllBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (ackAllMessageTask == null || AsyncTask.Status.RUNNING.equals(ackAllMessageTask.getStatus()) == false) {
                    ackAllMessageTask = new AckAllMessageTask(getCurrentContext());
                    ackAllMessageTask.exec();
                }
            }
        });

        if (isEconnect()) {
            pending_all = (TextView) findViewById(R.id.pending_btn);
            pending_all.setVisibility(View.VISIBLE);
            pending_all.setText(R.string.pending);
            pending_all.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (task == null || AsyncTask.Status.RUNNING.equals(task.getStatus()) == false) {
                        task = new PendingMessageLoader(getCurrentContext(), 1);
                        task.exec();
                    }
                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        doSearch();
    }

    private synchronized void popupDatePickDialog() {

        if (datePickerDialog != null && datePickerDialog.isShowing()) {
            return;
        }
        Date currentTime = DateTimeHelper.getDateByTimeZone(dateAndTime.getTime());
        datePickerDialog = new McDatePickerDialog(this, dateAndTime.getTime());
        datePickerDialog.setPermanentTitle(getString(R.string.set_date));
        datePickerDialog.setOnClickDone(new OnClickListener() {

            @Override
            public void onClick(View v) {
                DatePicker datePicker = datePickerDialog.getDatePicker();
                dateAndTime.set(Calendar.YEAR, datePicker.getYear());
                dateAndTime.set(Calendar.MONTH, datePicker.getMonth());
                dateAndTime.set(Calendar.DAY_OF_MONTH, datePicker.getDayOfMonth());
                datePickerDialog.dismiss();
                doSearch();

            }
        });
        datePickerDialog.show();
    }

    private class MessageListLoader extends McProgressAsyncTask {

        private Date createdDate;
        private FindMsgResponse response;

        private MessageListLoader(Context context, Date createdDate) {
            super(context);
            this.createdDate = createdDate;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            models.clear();
            adapter.notifyDataSetChanged();
        }

        @Override
        public void doInBackground() {
            if (isCConnect()) {
                response = AdhocService.getInstance().findMsg(getCurrentContext(), createdDate, 0, 200);
            } else {
                response = ECService.getInstance().getAcHocList(getCurrentContext(), createdDate);
            }
        }

        @Override
        public void onPostExecute() {
            if (response != null) {
                models.addAll(response.getMessageList());
                adapter.notifyDataSetChanged();
            }
            setBottomVisibility();
        }

    }

    private class PendingMessageLoader extends McProgressAsyncTask {

        private int allNak;
        private FindMsgResponse response;

        private PendingMessageLoader(Context context, int allNak) {
            super(context);
            this.allNak = allNak;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            models.clear();
            adapter.notifyDataSetChanged();
        }

        @Override
        public void doInBackground() {
            if (isCConnect()) {

            } else {
                response = ECService.getInstance().getAdhocLists(getCurrentContext(), null, allNak);
            }
        }

        @Override
        public void onPostExecute() {
            if (response != null) {
                models.addAll(response.getMessageList());
                adapter.notifyDataSetChanged();
            }
            setBottomVisibility();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            setResult(RESULT_CANCELED);
            finish();
            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

    class MyMessageListAdapter extends RoundCornerListAdapter<MessageModel> {

        protected MessageLoader task;

        public MyMessageListAdapter(Context context, List<MessageModel> list, int layout) {
            super(context, list, layout);
        }

        @Override
        public void onBindView(View v, Context context, int position, final MessageModel item) {

            TextView sendUsername = (TextView) v.findViewById(R.id.my_message_send_username);
            TextView receiveDate = (TextView) v.findViewById(R.id.my_message_receive_date);
            TextView datail = (TextView) v.findViewById(R.id.my_message_detail);

            sendUsername.setEllipsize(TruncateAt.MIDDLE);

            String name = item.getFromUsername();
            if (item.getAckDate() != null) {
                name = name + "(" + getCurrentContext().getString(R.string.ack) + ")";
            }

//			sendUsername.setText(item.getFromUsername());
            sendUsername.setText(name);

            datail.setEllipsize(TruncateAt.MIDDLE);
            datail.setText(item.getTitle());

            receiveDate.setText(McUtils.formatDateTime(item.getCreatedDate()));

            v.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (task == null || AsyncTask.Status.RUNNING.equals(task.getStatus()) == false) {
                        task = new MessageLoader(getCurrentContext(), item);
                        task.exec();
                    }

                }
            });
        }

    }

    class MessageLoader extends McProgressAsyncTask {

        private GetMsgResponse response;
        private MessageModel item;

        public MessageLoader(Context context, MessageModel item) {
            super(context);
            this.item = item;
        }

        @Override
        public void doInBackground() {
            if (isCConnect()) {
                response = AdhocService.getInstance().getMsg(getCurrentContext(), item.getMsgDoneId());
            }
        }

        @Override
        public void onPostExecute() {
            if (isCConnect()) {
                if (response != null) {
                    toast(getString(R.string.find_message_successfully));
                    Intent i = MyMessageDetailActivity.createCConnectIntent(getCurrentContext(), response);
                    startActivityForResult(i, RC_MESSAGE);
                }
            } else {
                Intent i = MyMessageDetailActivity.createEConnectIntent(getCurrentContext(), item);
                startActivityForResult(i, RC_MESSAGE);
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == RC_MESSAGE && data != null && data.hasExtra("msg")) {
            MessageModel m = (MessageModel) data.getSerializableExtra("msg");
            for (MessageModel mm : models) {
                if (mm.getMsgDoneId() == m.getMsgDoneId()) {
                    int i = models.indexOf(mm);
                    models.remove(i);
                    models.add(i, m);
                    break;
                }
            }
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean isLoginActivity() {
        return true;
    }

    @Override
    public boolean isHomeActivity() {
        return false;
    }

    private void setBottomVisibility() {
        boolean show = false;
        if (models != null && models.size() > 0) {
            for (MessageModel j : models) {
                if (j.isAcknowledged() == false) {
                    show = true;
                    break;
                }
            }
        }
        if (show) {
            ackAllBtn.setEnabled(true);
        } else {
            ackAllBtn.setEnabled(false);
        }
    }

    class AckAllMessageTask extends McProgressAsyncTask {

        private AckAdHocResponse result;
        private int numAck = 0;

        public AckAllMessageTask(Context context) {
            super(context);
        }


        @Override
        public void doInBackground() {
            if (models != null) {
                for (MessageModel j : models) {
                    if (j.isAcknowledged()) {
                        continue;
                    }
                    result = ECService.getInstance().ackAdHoc(getCurrentContext(), String.valueOf(j.getMsgDoneId()));
                    if (result != null && result.isSuccess()) {
                        numAck += 1;
                    }
                }
            }
        }

        @Override
        public void onPostExecute() {
            if (numAck > 0) {
                toast(R.string.acknowledge_message_successfully);
                finish();
            } else {
                toast(R.string.fail_to_acknowledge_message);
            }
            setBottomVisibility();
        }
    }


}
