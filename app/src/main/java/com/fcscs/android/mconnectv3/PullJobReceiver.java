package com.fcscs.android.mconnectv3;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.fcscs.android.mconnectv3.common.McConstants;
import com.fcscs.android.mconnectv3.common.ServiceExceptionHandler;
import com.fcscs.android.mconnectv3.common.util.PrefsUtil;

public class PullJobReceiver extends BasePullReceiver {

    private Context ctx;
    private PullTask task;

    private BaseCheckLoginReceiver.CheckLoginTask taskCheckLogin;

    public void setAlarm(Context context) {

        if (context == null) {
            return;
        }

        int interval = PrefsUtil.getJobPullingInterval(context);
        if (interval <= 0) {
            return;
        } else if (interval <= 20) {
            interval = 20;
        }
        Log.d(TAG, "job timer interval=" + interval);
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, PullJobReceiver.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, TYPE_JOB, i, 0);
        //am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 1000 * interval, pi);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            AlarmManager.AlarmClockInfo alarmClockInfo = new AlarmManager.AlarmClockInfo(System.currentTimeMillis() + 1000 * interval, pi);
            am.setAlarmClock(alarmClockInfo, pi);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            am.setExact(android.app.AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 1000 * interval, pi);
        } else {
            am.set(android.app.AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 1000 * interval, pi);
        }
    }

    void cancelAlarm(Context context) {

        if (context == null) {
            return;
        }

        int interval = PrefsUtil.getJobPullingInterval(context);
        if (interval <= 0) {
            return;
        }

        Intent intent = new Intent(context, PullJobReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, TYPE_JOB, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (McConstants.OFFLINE_MODE) {
            return;
        }

        ctx = context;
        ServiceExceptionHandler.setHandler(ctx);

        Log.d(TAG, "repeat job timer");
        if (task == null || AsyncTask.Status.RUNNING.equals(task.getStatus()) == false) {
            task = new PullTask(context, TYPE_JOB);
            task.exec();
            taskCheckLogin = new BaseCheckLoginReceiver.CheckLoginTask(context, 3);
            taskCheckLogin.exec();
            setAlarm(context);
        }
    }
}
