package com.fcscs.android.mconnectv3.ws.cconnect.model.securityservice;

import java.io.Serializable;

import org.ksoap2.serialization.SoapObject;

public class UpdateSessionResponse implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 5764995023755995714L;
    private boolean result;

    public UpdateSessionResponse() {
    }

    public UpdateSessionResponse(SoapObject soap) {
        this.result = Boolean.valueOf(soap.getAttribute("result").toString());
    }

    public boolean isResult() {
        return result;
    }

}
