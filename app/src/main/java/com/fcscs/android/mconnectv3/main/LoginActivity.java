package com.fcscs.android.mconnectv3.main;

import java.util.List;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Space;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

//import com.fcs.fcsptt.util.PreferencesUtil;
import com.fcscs.android.mconnectv3.AlertPanicManager;
import com.fcscs.android.mconnectv3.CheckNetworkConnectivityReceiver;
import com.fcscs.android.mconnectv3.CheckNetworkConnectivityReceiver.CheckConnectivity;
import com.fcscs.android.mconnectv3.PullNotificationService;
import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.cache.ObjectCache;
import com.fcscs.android.mconnectv3.common.BaseActivity;
import com.fcscs.android.mconnectv3.common.ConfigContext;
import com.fcscs.android.mconnectv3.common.McConstants;
import com.fcscs.android.mconnectv3.common.McEnums;
import com.fcscs.android.mconnectv3.common.OnFlingGestureListeners;
import com.fcscs.android.mconnectv3.common.SessionContext;
import com.fcscs.android.mconnectv3.common.ui.LogoutTopBar;
import com.fcscs.android.mconnectv3.common.ui.McBackgroundAsyncTask;
import com.fcscs.android.mconnectv3.common.ui.McProgressAsyncTask;
import com.fcscs.android.mconnectv3.common.util.CoderSHA;
import com.fcscs.android.mconnectv3.common.util.DialogHelper;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.common.util.PrefsUtil;
import com.fcscs.android.mconnectv3.common.util.RootUtils;
import com.fcscs.android.mconnectv3.engineering.EnHomeActivity;
import com.fcscs.android.mconnectv3.manager.model.Language;
import com.fcscs.android.mconnectv3.manager.model.ServiceRequest;
import com.fcscs.android.mconnectv3.manager.model.ServiceRequestItem;
import com.fcscs.android.mconnectv3.smartpush.SmartPush;
import com.fcscs.android.mconnectv3.ws.cconnect.CoreService;
import com.fcscs.android.mconnectv3.ws.cconnect.SecurityService;
import com.fcscs.android.mconnectv3.ws.cconnect.model.securityservice.LoginResponse;
import com.fcscs.android.mconnectv3.ws.econnect.ECService;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.GetCheckLoginResponse;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.ModuleLicense;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class LoginActivity extends BaseActivity {

    public static final String LOGIN_CACHE_PASSWORD = "login_cache_password";
    public static final String LOGIN_CACHE_USER = "login_cache_user";
    private static final String LOGIN_IS_REMEMBER = "LOGIN_IS_REMEMBER";
    protected static final String RESET_LOCALE = "RESET_LOCALE";

    private Button btnLogin;
    private EditText etUsername;
    private EditText etPassword;
    private Spinner spLanguage;
    private CheckBox cbRememberMe;
    private Button topRightBtn;
    private ArrayAdapter<Language> adapter;
    protected ECLoginLoader task;
    private ImageView panic;
    private Space spaLoginTop;
    private LogoutTopBar loginTopBar;
    CallReciever mCallReceive;

    @NeedsPermission({
            Manifest.permission.GET_ACCOUNTS
            , Manifest.permission.ACCESS_WIFI_STATE
            , Manifest.permission.ACCESS_NETWORK_STATE
            , Manifest.permission.VIBRATE
            , Manifest.permission.WRITE_EXTERNAL_STORAGE
            , Manifest.permission.READ_PHONE_STATE
            , Manifest.permission.RECORD_AUDIO
            , Manifest.permission.CALL_PHONE
            , Manifest.permission.CAMERA
    })
    void askForPermission() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//		McApplication.application.removeObserver(this);
        setContentView(R.layout.login);

        stopService(new Intent(LoginActivity.this, PullNotificationService.class));

//		String versionName = null;
//		try {
//			versionName = getPackageManager().getPackageInfo(getPackageName(), 0 ).versionName;
//		} catch (NameNotFoundException e) {
//		}

        TextView tvVersion = (TextView) findViewById(R.id.version_id);
//		if (versionName != null) {
//			tvVersion.setVisibility(View.VISIBLE);
//			tvVersion.setText("V" + versionName);
//		} else {
//			tvVersion.setVisibility(View.INVISIBLE);
//		}
        mCallReceive = new CallReciever();
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.intent.action.PHONE_STATE");
        registerReceiver(mCallReceive, filter);
        tvVersion.setVisibility(View.VISIBLE);
        tvVersion.setText("V" + McConstants.APP_VERSION_NAME);
        etUsername = (EditText) findViewById(R.id.login_username);
        etPassword = (EditText) findViewById(R.id.login_password);
        cbRememberMe = (CheckBox) findViewById(R.id.cb_remember_me);
        btnLogin = (Button) findViewById(R.id.login_btn);
        spLanguage = (Spinner) findViewById(R.id.sp_language);
        topRightBtn = (Button) findViewById(R.id.btn_top_menu);
        topRightBtn.setClickable(false);

        setTextViewString(R.id.lb_userid, "userid", R.string.userid);
        setTextViewString(R.id.paswd, "paswd", R.string.paswd);
        setTextViewString(R.id.lb_language, "language", R.string.language);
        setTextViewString(R.id.login_btn, "login", R.string.login);

        setIcon(!McUtils.isNetworkOff(this));
        initialLanguageSpinner();
        initialFieldsFromSessionContext();
        restoreCurrentState();

        if (McConstants.COMPILATION_PRODUCT == McEnums.CompilationProduct.PERCIPIA) {
            RelativeLayout layout = (RelativeLayout) findViewById(R.id.layout_root);
            layout.setBackgroundResource(R.drawable.bg_percipia);

            LogoutTopBar logoutTopBar = (LogoutTopBar) findViewById(R.id.login_topbar);
            logoutTopBar.setBackgroundColor(Color.rgb(20, 100, 50));
        } else if (McConstants.COMPILATION_PRODUCT == McEnums.CompilationProduct.MENGINEERING) {
            RelativeLayout layout = (RelativeLayout) findViewById(R.id.layout_root);
            layout.setBackgroundResource(R.drawable.bg_en);

            LogoutTopBar logoutTopBar = (LogoutTopBar) findViewById(R.id.login_topbar);
            logoutTopBar.setBackgroundColor(Color.rgb(20, 100, 50));
        }
        RelativeLayout layout = (RelativeLayout) findViewById(R.id.layout_root);
        if (McConstants.SHOW_PTT) {
            layout.setBackgroundResource(R.drawable.bg_ptt);
        }

        spaLoginTop = (Space) findViewById(R.id.space_logintop);
        ViewGroup.LayoutParams spaceparams = spaLoginTop.getLayoutParams();
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        if (metrics.densityDpi <= DisplayMetrics.DENSITY_LOW) { // ldpi
            spaceparams.height = 0;
        } else if (metrics.densityDpi > DisplayMetrics.DENSITY_LOW && metrics.densityDpi <= DisplayMetrics.DENSITY_MEDIUM) { //mdpi
            spaceparams.height = 0;
        } else { //hdpi

        }
        spaLoginTop.setLayoutParams(spaceparams);

        loginTopBar = (LogoutTopBar) findViewById(R.id.login_topbar);
        if (PrefsUtil.getEnableEng(this) == 0) {
            loginTopBar.getTitleTv().setText("FCS m-Connect");
        } else {
            loginTopBar.getTitleTv().setText("FCS Engineering");
        }
        if (McConstants.COMPILATION_PRODUCT == McEnums.CompilationProduct.PERCIPIA) {
            loginTopBar.getTitleTv().setText("Percipia iLink");
        } else if (McConstants.COMPILATION_PRODUCT == McEnums.CompilationProduct.MENGINEERING) {
            loginTopBar.getTitleTv().setText("FCS Engineering");
        }

        panic = (ImageView) findViewById(R.id.panic_button);
        panic.setOnTouchListener(new OnFlingGestureListeners() {

            @Override
            public void onRightToLeft() {
                if ("open".equals(panic.getTag())) {
                    panic.setImageResource(R.drawable.panic_button_close);
                    panic.setTag("close");
                    AlertPanicManager.getInstance().stopSoundAlert();
                }
            }

            @Override
            public void onLeftToRight() {
                if ("open".equals(panic.getTag()) == false) {
                    panic.setImageResource(R.drawable.panic_button_open);
                    panic.setTag("open");
                    AlertPanicManager.getInstance().playSoundAlert();
                }
            }

            @Override
            public void onBottomToTop() {
            }

            @Override
            public void onTopToBottom() {
            }

            @Override
            public void onTouchDown() {
            }
        });
        panic.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if ("open".equals(panic.getTag())) {

                    if (ConfigContext.getInstance().isEnablePanicCall()) {
                        if (ConfigContext.getInstance().isAutoAlert()) {
                            makeCall();
                        } else {
                            showPanicDialog();
                        }

                    } else {
                        Intent i = new Intent(LoginActivity.this, PanicButtonActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        LoginActivity.this.startActivity(i);
                    }
                }
                new PanicButtonSubmit(getCurrentContext()).exec();
            }
        });
        panic.setVisibility(View.INVISIBLE);


        btnLogin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                String username = etUsername.getText().toString().trim();
                String password = etPassword.getText().toString().trim();
                String configUsr = "config";
                String configPwd = getResources().getString(R.string.fcs_config_pwd);
                if (McConstants.COMPILATION_PRODUCT == McEnums.CompilationProduct.PERCIPIA) {
                    configPwd = getResources().getString(R.string.per_config_pwd);
                }
                if ("".equals(username)) {
                    toast(getString(R.string.msg_username_empty));
                    return;
                }

                if ("".equals(password)) {
                    toast(getString(R.string.msg_password_empty));
                    return;
                }
                //SHA256
                String key = getResources().getString(R.string.secure_pref_key);
                if ((configUsr.equals(username) && (configPwd.equals(CoderSHA.hmacSHA256Digest(password, key))) || "mnb".equals(username))) {
                    Intent i = new Intent(LoginActivity.this, ConfigActivity.class);
                    startActivity(i);
                    return;
                }

                if (isCConnect()) {
                    new CCLoginLoader(getCurrentContext(), username, password).exec();
                } else {
//					if (task == null || AsyncTask.Status.RUNNING.equals(task.getStatus()) == false) {
//						task = new ECLoginLoader(getCurrentContext(), username, password, cbRememberMe.isChecked());
//						task.exec();
//					}
//					TODO: check login duplicate when login
                    if (!username.equalsIgnoreCase(mUserName)) isCheckedLogin = false;
                    if (!isCheckedLogin
//							&& !username.equalsIgnoreCase(PreferencesUtil.getLastUserNameLoginPrefs(LoginActivity.this))
                            ) {
                        mUserName = username;
                        mPassword = password;
                        new CheckLoginTask(LoginActivity.this, username).exec();
                    } else {
                        if (task == null || AsyncTask.Status.RUNNING.equals(task.getStatus()) == false) {
                            task = new ECLoginLoader(getCurrentContext(), username, password, cbRememberMe.isChecked());
                            task.exec();
                        }
                    }
                }
            }
        });
//        LoginActivityPermissionsDispatcher.askForPermissionWithCheck(this);

        // detect rooted
        detectRooted();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        LoginActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (getSessionCtx().isLogin()) {
            getSessionCtx().setLogin(false);
            getSessionCtx().persist(this);
        }

        if (McUtils.isNullOrEmpty(getConfigCtx().getServiceUrl())) {
            Intent i = new Intent(LoginActivity.this, LoadingActivity.class);
            startActivity(i);
            return;
        }
    }


    private void initialFieldsFromSessionContext() {

        final SessionContext sessionCtx = SessionContext.getInstance();
        sessionCtx.populate(this);

        // initial fields with DB data
        if (sessionCtx.isLogin() || sessionCtx.isRemember()) {
            if (sessionCtx.getUsername() != null) {
                etUsername.setText(sessionCtx.getUsername());
            }
            if (sessionCtx.getPassword() != null) {
                etPassword.setText(sessionCtx.getPassword());
            }
            cbRememberMe.setChecked(true);
        } else {
            etUsername.setText("");
            etPassword.setText("");
            cbRememberMe.setChecked(false);
            etUsername.requestFocus();
        }
    }

    @SuppressWarnings("unchecked")
    private void initialLanguageSpinner() {

        final List<Language> languages = (List<Language>) getAppCache().get(LoadingActivity.LANGUAGE_LIST);
        Object idx = getAppCache().get(LoadingActivity.LANGUAGE_INDEX);
        if (languages == null || idx == null) {
            Intent i = new Intent(this, LoadingActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            return;
        }
        final int curIndex = (Integer) idx;

        adapter = new ArrayAdapter<Language>(this,
                android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        for (Language lang : languages) {
            adapter.add(lang);
        }
        spLanguage.setAdapter(adapter);
        spLanguage.setSelection(curIndex);

        spLanguage.setOnItemSelectedListener(new OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                Language lan = languages.get(position);
                if (position != curIndex && lan != null) {

                    PrefsUtil.saveLocale(lan.getLocale());

                    saveCurrentState();

                    Intent intent = new Intent(LoginActivity.this, LoadingActivity.class);
                    intent.putExtra(LoadingActivity.RESET_LOCALE, true);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }

    private void saveCurrentState() {
        ObjectCache cache = getAppCache();
        cache.put(LOGIN_CACHE_USER, etUsername.getText().toString());
        cache.put(LOGIN_CACHE_PASSWORD, etPassword.getText().toString());
        cache.put(LOGIN_IS_REMEMBER, cbRememberMe.isChecked());
    }

    private void restoreCurrentState() {
        ObjectCache cache = getAppCache();
        if (cache.containsKey(LOGIN_CACHE_USER) == false) {
            return;
        }
        etUsername.setText((CharSequence) cache.get(LOGIN_CACHE_USER));
        etPassword.setText((CharSequence) cache.get(LOGIN_CACHE_PASSWORD));
        cache.remove(LOGIN_CACHE_USER);
        cache.remove(LOGIN_CACHE_PASSWORD);
        if (cache.containsKey(LOGIN_IS_REMEMBER)) {
            cbRememberMe.setChecked((Boolean) cache.get(LOGIN_IS_REMEMBER));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getApp().setLogin(false);
        CheckNetworkConnectivityReceiver.register(checking);
        LoginActivityPermissionsDispatcher.askForPermissionWithCheck(this);
        new PanicButtonConfig(this).exec();
    }

    // detect rooted
    private void detectRooted() {
        if (RootUtils.isRooted() && McConstants.IS_FIRST_ROOTED) {
            String messages;

            messages = getResources().getString(R.string.message_rooted_device_detected);

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LoginActivity.this);
            alertDialogBuilder.setMessage(messages);

            alertDialogBuilder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int arg1) {
                    // Do nothing
                    dialog.dismiss();
                }
            });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
            McConstants.IS_FIRST_ROOTED = false;
        }
    }

    private void setIcon(boolean hasConnection) {
        if (hasConnection) {
            topRightBtn.setBackgroundResource(R.drawable.icon_connected);
        } else {
            topRightBtn.setBackgroundResource(R.drawable.icon_disconnected);
        }
    }

    private CheckConnectivity checking = new CheckConnectivity() {
        @Override
        public void hasConenction(boolean hasConnection) {
            if (topRightBtn != null) {
                setIcon(hasConnection);
            }
        }
    };

    @Override
    protected void onDestroy() {
        if (isCConnect()) {
            if (SessionContext.getInstance().getFcsUserId() > 0) {
                SecurityService.getInstance().logout(LoginActivity.this, SessionContext.getInstance().getFcsUserId(),
                        SessionContext.getInstance().getWsSessionId());
                SessionContext.getInstance().resetSession();
            }

        } else {
            if (SessionContext.getInstance().getFcsUserId() > 0) {
                SessionContext.getInstance().resetSession();
            }
        }
        unregisterReceiver(mCallReceive);
        super.onDestroy();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

    public static class CallReciever extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if ("android.intent.action.PHONE_STATE".equalsIgnoreCase(action)) {
                if (intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(
                        TelephonyManager.EXTRA_STATE_RINGING)) {

                    // Phone number
                    String incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);

                    // Ringing state
                    // This code will execute when the phone has an incoming call
                } else if (intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(
                        TelephonyManager.EXTRA_STATE_IDLE)) {

                    //AlertPanicManager.getInstance().playSoundAlert();

                } else if (intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(
                        TelephonyManager.EXTRA_STATE_OFFHOOK)) {
                    String endCall = "end call2";
                    Log.d("beanbechoi", endCall);
                }

            }
        }
    }

    static MediaPlayer mp = null;

    protected static void managerOfSound() {

    }

    public class ECLoginLoader extends McProgressAsyncTask {

        private LoginResponse loginResp;
        private String username;
        private String password;
        private ECService ecInst;
        private ModuleLicense mod;
        private Context context;
        private boolean remember;

        public ECLoginLoader(Context context, String username, String password, boolean remember) {
            super(context);
            this.username = username;
            this.password = password;
            this.context = context;
            this.remember = remember;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            btnLogin.setClickable(false);
        }

        @Override
        public void doInBackground() {

            ecInst = ECService.getInstance(15000, 15000);
            loginResp = ecInst.getCompositeLogin(LoginActivity.this, username, password);

            if (loginResp != null && "0".equals(loginResp.getReturnCode())) {

                final SessionContext sessionCtx = SessionContext.getInstance();
                mod = sessionCtx.getModule();

                String address = McUtils.getMacAddr();
                SmartPush.saveMQTTTopicPrefs(LoginActivity.this.getApplicationContext(), address);

                sessionCtx.setFcsUserId(loginResp.getUserId());
                sessionCtx.setOrgId(loginResp.getOrgId());
                sessionCtx.setPropId(loginResp.getPropId());
                sessionCtx.setWsSessionId(loginResp.getSessionId());
                sessionCtx.setUsername(username);
                sessionCtx.setName(loginResp.getName());
                sessionCtx.setPassword(password);
                sessionCtx.setRemember(cbRememberMe.isChecked());
                sessionCtx.setLogin(true);
                sessionCtx.setUserTimeZone(loginResp.getUserTimeZone());

                sessionCtx.setReAssignRunner(loginResp.getReAssignRunner());
                sessionCtx.setUserType(loginResp.getUserType());
                sessionCtx.setUserDepartment(loginResp.getUserDepartment());
                PrefsUtil.setReAssignRunner(LoginActivity.this, loginResp.getReAssignRunner());
                PrefsUtil.setUserType(LoginActivity.this, loginResp.getUserType());
                PrefsUtil.setUserDepartMent(LoginActivity.this, loginResp.getUserDepartment());
                PrefsUtil.setTimeZone(LoginActivity.this, loginResp.getUserTimeZone());

            }


//			ecInst = ECService.getInstance();
//
//			String token = "";
//			String type = "2";
//			loginResp = ecInst.getLogin(context, username, password, token, type);
//
//			if (loginResp != null && "0".equals(loginResp.getReturnCode())) {
//
//				final SessionContext sessionCtx = SessionContext.getInstance();
//				sessionCtx.setFcsUserId(loginResp.getUserId());
//				sessionCtx.setOrgId(loginResp.getOrgId());
//				sessionCtx.setPropId(loginResp.getPropId());
//				sessionCtx.setWsSessionId(loginResp.getSessionId());
//				sessionCtx.setUsername(username);
//				sessionCtx.setName(loginResp.getName());
//				sessionCtx.setPassword(password);
//				sessionCtx.setRemember(remember);
//				sessionCtx.setLogin(true);
//				
//				pause();
//				ecInst.postUpdateLoginLanguage(getCurrentContext());
//				pause();
//				ecInst.getCommonConfigurations(getCurrentContext());
//				pause();				
//				boolean is2_1 = true;	//mC v2.1 by Gosh
//				mod = ecInst.getModuleLicense(context, sessionCtx.getFcsUserId(), is2_1);
//				if (mod != null) {
//					sessionCtx.setModule(mod);					
//				}
//				sessionCtx.persist(getCurrentContext());
//				
//			}

        }

        @Override
        public void onPostExecute() {

            btnLogin.setClickable(true);

            if (loginResp == null) {
            } else if ("1".equals(loginResp.getReturnCode())) {
                toast(R.string.login_wrong_credential);
            } else if (mod == null) {
                toast(R.string.fail_user_license);
            } else {
//				PreferencesUtil.setIsmConnectLogoutPrefs(LoginActivity.this, false);
                final SessionContext sessionCtx = SessionContext.getInstance();
                // initialize the message count
                sessionCtx.setMessageCount(0);
                sessionCtx.persist(context);

                // login successfully
                toast(R.string.login_welcome, sessionCtx.getName());
                SmartPush.getInstance().reset();
                SmartPush.getInstance().registerPush(LoginActivity.this.getApplicationContext());
//				GCMIntentService.registerGCM(context);
//				PreferencesUtil.setLastUserNameLoginPrefs(context, username);
                if (PrefsUtil.getEnableEng(context) == 0) {
                    Intent i = new Intent(getCurrentContext(), HomeActivity.class);
                    i.putExtra(McConstants.KEY_USERNAME, username);
                    context.startActivity(i);
                } else {
                    Intent i = new Intent(getCurrentContext(), EnHomeActivity.class);
                    i.putExtra(McConstants.KEY_USERNAME, username);
                    context.startActivity(i);
                }
            }

        }

    }

    private class CCLoginLoader extends McProgressAsyncTask {

        private String username;
        private String password;

        private LoginResponse loginResp;
        private SecurityService securityInst;
        private CoreService coreInst;
        private ModuleLicense mod;

        public CCLoginLoader(Context context, String username, String password) {
            super(context);
            this.username = username;
            this.password = password;
        }

        @Override
        public void doInBackground() {
            WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();

            securityInst = SecurityService.getInstanceNonUI();
            loginResp = securityInst.login(LoginActivity.this, username, password, McUtils
                    .getLocalIpAddress(true), wifiInfo.getMacAddress(), ConfigContext.getInstance().getPropertyId());

            if (loginResp != null && loginResp.getUserId() > 0L) {

                final SessionContext sessionCtx = SessionContext.getInstance();
                SmartPush.saveMQTTTopicPrefs(LoginActivity.this.getApplicationContext(), String.valueOf(loginResp.getUserId()));
                sessionCtx.setFcsUserId(loginResp.getUserId());
                sessionCtx.setOrgId(loginResp.getOrgId());
                sessionCtx.setPropId(loginResp.getPropId());
                sessionCtx.setWsSessionId(loginResp.getSessionId());
                sessionCtx.setUsername(username);
                sessionCtx.setPassword(password);
                sessionCtx.setRemember(cbRememberMe.isChecked());
                sessionCtx.setLogin(true);
                sessionCtx.setUserTimeZone(loginResp.getUserTimeZone());
                sessionCtx.setReAssignRunner(loginResp.getReAssignRunner());
                sessionCtx.setUserType(loginResp.getUserType());
                sessionCtx.setUserDepartment(loginResp.getUserDepartment());

                PrefsUtil.setReAssignRunner(LoginActivity.this, loginResp.getReAssignRunner());
                PrefsUtil.setUserType(LoginActivity.this, loginResp.getUserType());
                PrefsUtil.setUserDepartMent(LoginActivity.this, loginResp.getUserDepartment());
                PrefsUtil.setTimeZone(LoginActivity.this, loginResp.getUserTimeZone());
                coreInst = CoreService.getInstanceNonUI();

                mod = coreInst.getAuthorities(LoginActivity.this);
                sessionCtx.setModule(mod);

            }
        }

        @Override
        public void onPostExecute() {
            if (loginResp == null) {
            } else if (loginResp.getUserId() <= 0L) {
                toast(R.string.login_wrong_credential);
            } else if (mod == null) {
                toast(R.string.fail_user_license);
            } else {

                final SessionContext sessionCtx = SessionContext.getInstance();
                // initialize the message count
                sessionCtx.setMessageCount(0);
                sessionCtx.persist(LoginActivity.this);

                // login successfully
                toast(R.string.login_welcome, username);

//				GCMIntentService.registerGCM(LoginActivity.this);
                SmartPush.getInstance().registerPush(LoginActivity.this.getApplicationContext());
                Intent i = new Intent(LoginActivity.this, EnHomeActivity.class);
                i.putExtra(McConstants.KEY_USERNAME, username);
                startActivity(i);

            }
        }

    }

    @Override
    public boolean isLoginActivity() {
        return false;
    }

    @Override
    public boolean isHomeActivity() {
        return false;
    }

    private class PanicButtonConfig extends McBackgroundAsyncTask {

        private ConfigContext conf;

        public PanicButtonConfig(Context context) {
            super(context);
        }

        @Override
        public void doInBackground() {
            conf = ECService.getInstance().getPanicButtonConfig(getCurrentContext());
        }

        @Override
        public void onPostExecute() {
            if (conf.isEnableOnLoginPage()) {
                panic.setVisibility(View.VISIBLE);
            }
        }

    }

    private void makeCall() {
        String phoneNo = ConfigContext.getInstance().getPanicCallContactNumber();
        if (McUtils.isNullOrEmpty(phoneNo) == false) {
            try {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + phoneNo));
//				startActivity(callIntent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void showPanicDialog() {
        DialogHelper.showYesNoDialog(this, R.string.panic_alert, R.string.start_panic_call, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                makeCall();
            }
        });
    }

    String mUserName, mPassword;

    public void checkLogin() {
        if (task == null || AsyncTask.Status.RUNNING.equals(task.getStatus()) == false) {
            task = new ECLoginLoader(getCurrentContext(), mUserName, mPassword, cbRememberMe.isChecked());
            task.exec();
        }
    }

    private static final String SECRET_KEY = "9jLjXmk4hkQ/DvHswmZggGQJGsgQdPAd";

    private class PanicButtonSubmit extends McProgressAsyncTask {

        private boolean result;

        public PanicButtonSubmit(Context context) {
            super(context);
        }

        @Override
        public void doInBackground() {
            result = ECService.getInstance().postPanicButtonAlert(getCurrentContext(), "", SECRET_KEY, "");
        }

        @Override
        public void onPostExecute() {
            if (result) {
                Toast.makeText(getCurrentContext(), getString(R.string.success_submit_panic_message), Toast.LENGTH_LONG).show();

            } else {
                Toast.makeText(getCurrentContext(), getString(R.string.failed_sumit_panic_message), Toast.LENGTH_LONG).show();
            }
        }
    }

    public static final Integer NO_NETWORK = 3000;
    private boolean isCheckedLogin = false;

    protected static class CheckLoginTask extends McProgressAsyncTask {

        private LoginActivity context;
        private boolean isNoNetwork = true;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        public CheckLoginTask(LoginActivity context, String userID) {
            super(context);
            this.context = context;
            this.userID = userID;
        }

        String userID;
        private int returnCode = 0;

        @Override
        public void doInBackground() {

            if (context == null) {
                return;
            }

            isNoNetwork = McUtils.isNetworkOff(context);
            if (isNoNetwork) {
                McUtils.saveCrashReport(context, new Throwable("No Network."));
                return;
            } else {
                NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                manager.cancel(NO_NETWORK);
                GetCheckLoginResponse resp = getCheckLogin(context, userID);
                if (resp != null && resp.getReturnCode() == 1) {
                    returnCode = 1;
                }
            }
        }

        @Override
        public void onPostExecute() {
            if (returnCode == 1) {
                //TODO: already login duplication show logout screen
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setMessage(String.format(context.getString(R.string.login_loggedinprompt), userID));

                alertDialogBuilder.setPositiveButton(context.getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        context.isCheckedLogin = true;
                    }
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            } else {
                context.checkLogin();
            }
        }

        private GetCheckLoginResponse getCheckLogin(Context context, String userName) {
            GetCheckLoginResponse resp = null;
            try {
                resp = ECService.getInstance().getCheckLogin(context, userName);
            } catch (Throwable e) {
            }
            return resp;
        }
    }
}
