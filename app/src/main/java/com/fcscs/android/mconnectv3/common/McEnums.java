package com.fcscs.android.mconnectv3.common;

import android.graphics.Color;

import java.io.Serializable;

import com.fcscs.android.mconnectv3.R;

public interface McEnums {

    public enum CompilationProduct {
        MCONNECT,
        MENGINEERING,
        PERCIPIA
    }

    public enum StatusColor implements Serializable {
        GREEN(Color.GREEN),
        RED(Color.RED),
        BLUE(Color.BLUE),
        PURPLE(0xFF6600CC),
        BROWN(0xFF994C00),
        DARKCYAN(0xFF000099),
        ORANGE(0xFFFF8000),
        LIMEADE(0xFF006600),
        BLACK(Color.BLACK);

        private int color;

        StatusColor(int clr) {
            this.color = clr;
        }

        public int getColor() {
            return this.color;
        }
    }

    public enum ServiceType {
        CCONNECT, ECONNECT
    }

    public enum FilterType {
        ROOMNO(R.string.dropdown_roomno),
        GUESTNAME(R.string.dropdown_guestname),
        ASSIGNEDBY(R.string.dropdown_assignby),
        ASSIGNEDTO(R.string.dropdown_assignto),
        JOBNO(R.string.jobno),

        LOCATION(R.string.dropdown_location),
        EQUIPMENTNO(R.string.equipment_no);

        private int dispStr;

        private FilterType(int dn) {
            dispStr = dn;
        }

        public int getDispStr() {
            return dispStr;
        }
    }

    public enum JobStatus implements Serializable {

        OPEN("Open", "OPEN", R.string.open, 6),
        PENDING("Pending", "PENDING", R.string.pending, 1),
        COMPLETED("Completed", "COMPLETED", R.string.completed, 3),
        CANCELLED("Cancelled", "CANCELLED", R.string.cancelled, 2),
        TIMEOUT("Timeout", "TIME_OUT", R.string.time_out, 5),
        SCHEDULED("Scheduled", "SCHEDULED", R.string.scheduled, 7),
        DELAYED("DELAYED", "DELAYED", R.string.delayed, 10),
        ALL("", "", R.string.all, 0);

        private String eConnectName;
        private String cConnectName;
        private int resourceId;
        private int statusId;

        JobStatus(String eConnectName, String cConnectName, int resourceId, int statusId) {
            this.eConnectName = eConnectName;
            this.cConnectName = cConnectName;
            this.setResourceId(resourceId);
            this.statusId = statusId;
        }

        public String getEConnectName() {
            return eConnectName;
        }

        public String getCConnectName() {
            return cConnectName;
        }

        public static String[] getMyJobAllStatuses() {
            return new String[]{PENDING.getCConnectName(), COMPLETED.getCConnectName(),
                    TIMEOUT.getCConnectName(), CANCELLED.getCConnectName()};
        }

        public static JobStatus convertFromCCStatus(String status) {
            for (JobStatus j : values()) {
                if (j.getCConnectName().equalsIgnoreCase(status)) {
                    return j;
                }
            }
            return null;
        }

        public static JobStatus convertFromECStatus(String status) {
            for (JobStatus j : values()) {
                if (j.getEConnectName().equalsIgnoreCase(status)) {
                    return j;
                }
            }
            return null;
        }

        public static JobStatus getJobStatusFromStatusId(int statusId) {
            for (JobStatus j : values()) {
                if (j.getStatusId() == statusId) {
                    return j;
                }
            }
            return null;
        }

        public int getResourceId() {
            return resourceId;
        }

        public void setResourceId(int resourceId) {
            this.resourceId = resourceId;
        }

        public int getStatusId() {
            return statusId;
        }

    }

    public enum FavoritesEnum {
        //		[6:05:34 PM] Christopher Felix: 0 = internal, 1 = external, 2 = guest
        INTERNAL("Internal", 0),
        EXTERNAL("External", 1),
        GUEST_STUFF("Guest Stuff", 2);

        private int value;

        FavoritesEnum(String eConnectName, int value) {
            this.value = value;
        }

        public int geteValue() {
            return value;
        }
    }

    enum RoomStatus {
//	Vacant Clean => S1
//	Vacant Dirty => S2
//	Occupied Clean => S3
//	Occupied Dirty => S4
//	Vacant Inspected => S5
//	Occupied Inspected => S6
//	Pick-up Room => S7
//	Out-Of-Order => S8
//	[4:45:21 PM] Phua Kim Leng: for example, the status "Vacant Clean" has code "S1"

        VACANT_CLEAN(R.string.vacant_clean, "S1"),
        VACANT_DIRTY(R.string.vacant_dirty, "S2"),
        OCCUPIED_CLEAN(R.string.occupied_clean, "S3"),
        OCCUPIED_DIRTY(R.string.occupied_dirty, "S4"),
        VACANT_INSPECTED(R.string.vacant_inspected, "S5"),
        OCCUPIED_INSPECTED(R.string.occupied_inspected, "S6"),
        PICK_UP_ROOM(R.string.pick_up_room, "S7"),
        OUT_OF_ORDER(R.string.out_of_order, "S8");

        private int dispStr;
        private String code;

        private RoomStatus(int dn, String code) {
            this.dispStr = dn;
            this.code = code;
        }

        public int getDispStr() {
            return dispStr;
        }

        public String getCode() {
            return code;
        }
    }

}
