package com.fcscs.android.mconnectv3.manager.model;

public class ServiceItemIdModel {

    private long serviceItemId;
    private long categoryId;

    public ServiceItemIdModel() {
    }

    public ServiceItemIdModel(long serviceItemId, long categoryId) {
        super();
        this.serviceItemId = serviceItemId;
        this.categoryId = categoryId;
    }

    public long getServiceItemId() {
        return serviceItemId;
    }

    public void setServiceItemId(long serviceItemId) {
        this.serviceItemId = serviceItemId;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || o instanceof ServiceItemIdModel == false) {
            return false;
        }
        ServiceItemIdModel model = (ServiceItemIdModel) o;
        if (model.getCategoryId() == categoryId && model.getServiceItemId() == serviceItemId) {
            return true;
        }
        return false;
    }

}
