package com.fcscs.android.mconnectv3.ws.cconnect.model.guestrequestservice;

import com.fcscs.android.mconnectv3.manager.model.GroupLocationInfo;

import org.ksoap2.serialization.SoapObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 10/28/15.
 */
public class FindGroupLocationResponse implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private List<GroupLocationInfo> groupLocationInfoList;

    public FindGroupLocationResponse() {
        groupLocationInfoList = new ArrayList<GroupLocationInfo>();
    }

    public FindGroupLocationResponse(SoapObject soap) {
        groupLocationInfoList = new ArrayList<GroupLocationInfo>();
        if (soap.getPropertyCount() > 0) {
            for (int i = 0; i < soap.getPropertyCount(); i++) {
                groupLocationInfoList.add(new GroupLocationInfo((SoapObject) soap.getProperty(i)));
            }
        }
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public List<GroupLocationInfo> getGroupLocationInfoList() {
        return groupLocationInfoList;
    }

    public void setGroupLocationInfoList(List<GroupLocationInfo> groupLocationInfoList) {
        this.groupLocationInfoList = groupLocationInfoList;
    }

}
