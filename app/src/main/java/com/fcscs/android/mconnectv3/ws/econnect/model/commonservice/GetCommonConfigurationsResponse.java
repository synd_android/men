package com.fcscs.android.mconnectv3.ws.econnect.model.commonservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.ksoap2.serialization.SoapObject;

import com.fcscs.android.mconnectv3.common.util.SoapHelper;

public class GetCommonConfigurationsResponse implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3462243046338355652L;
    private int returnCode;
    private String errorMsg;
    private List<CommonConfigurationsDetails> list;


    public GetCommonConfigurationsResponse() {

    }

    public GetCommonConfigurationsResponse(SoapObject soap) {
        SoapObject status = (SoapObject) soap.getProperty("ResponseStatus");
        returnCode = SoapHelper.getIntegerProperty(status, "ReturnCode", 1);
        errorMsg = SoapHelper.getStringProperty(status, "ErrorMsg", "");

        list = new ArrayList<CommonConfigurationsDetails>();
        for (int i = 0; i < soap.getPropertyCount(); i++) {
            if ("CommonConfigurationsListing".equalsIgnoreCase(SoapHelper.getPropertyName(soap, i))) {
                SoapObject s = (SoapObject) soap.getProperty(i);
                for (int j = 0; j < s.getPropertyCount(); j++) {
                    if ("CommonConfigurationsDetails".equalsIgnoreCase(SoapHelper.getPropertyName(s, j))) {
                        SoapObject l = (SoapObject) s.getProperty(j);
                        String module = SoapHelper.getStringProperty(l, "Module", "");
                        String value = SoapHelper.getStringProperty(l, "Value", "");

                        CommonConfigurationsDetails m = new CommonConfigurationsDetails();
                        m.setModule(module);
                        m.setValue(value);
                        list.add(m);
                    }
                }
            }
        }
    }

    public int getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(int returnCode) {
        this.returnCode = returnCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public List<CommonConfigurationsDetails> getList() {
        return list;
    }

    public void setList(List<CommonConfigurationsDetails> list) {
        this.list = list;
    }
}
