//package com.fcscs.android.mconnect.manager.model;
//
//import java.io.File;
//import java.io.Serializable;
//
///**
// * Created by FCS on 2014/6/19.
// */
//public class MessageItem implements Serializable {
//
//    private String name;
//    private String runnerId;
//    private String groupId;
//
//    private File capturedImage;
//    private MediaLibraryConfig.Tag imageTag;
//
//    private File recordedVoice;
//    private MediaLibraryConfig.Tag voiceTag;
//
//    public File getCapturedImage() {
//        return capturedImage;
//    }
//
//    public void setCapturedImage(File capturedImage) {
//        this.capturedImage = capturedImage;
//    }
//
//    public File getRecordedVoice() {
//        return recordedVoice;
//    }
//
//    public void setRecordedVoice(File recordedVoice) {
//        this.recordedVoice = recordedVoice;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    @Override
//    public boolean equals(Object o) {
//        if (o != null && o instanceof MessageItem && ((MessageItem) o).getRunnerId() != null) {
//            if (((MessageItem) o).getRunnerId().equals(runnerId)) {
//                return true;
//            }
//        }
//        return false;
//    }
//
//    public MediaLibraryConfig.Tag getImageTag() {
//        return imageTag;
//    }
//
//    public void setImageTag(MediaLibraryConfig.Tag imageTag) {
//        this.imageTag = imageTag;
//    }
//
//    public MediaLibraryConfig.Tag getVoiceTag() {
//        return voiceTag;
//    }
//
//    public void setVoiceTag(MediaLibraryConfig.Tag voiceTag) {
//        this.voiceTag = voiceTag;
//    }
//
//
//    public String getRunnerId() {
//        return runnerId;
//    }
//
//    public void setRunnerId(String runnerId) {
//        this.runnerId = runnerId;
//    }
//
//    public String getGroupId() {
//        return groupId;
//    }
//
//    public void setGroupId(String groupId) {
//        this.groupId = groupId;
//    }
//}
