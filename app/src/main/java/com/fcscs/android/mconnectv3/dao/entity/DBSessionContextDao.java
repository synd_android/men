package com.fcscs.android.mconnectv3.dao.entity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.DaoConfig;
import de.greenrobot.dao.Property;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.

/**
 * DAO for table tb_session.
 */
public class DBSessionContextDao extends AbstractDao<DBSessionContext, Long> {

    public static final String TABLENAME = "tb_session";

    /**
     * Properties of entity DBSessionContext.<br/>
     * Can be used for QueryBuilder and for referencing column names.
     */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property UserId = new Property(1, String.class, "userId", false, "USER_ID");
        public final static Property SessionToken = new Property(2, String.class, "sessionToken", false, "SESSION_TOKEN");
        public final static Property Username = new Property(3, String.class, "username", false, "USERNAME");
        public final static Property Password = new Property(4, String.class, "password", false, "PASSWORD");
        public final static Property LocaleLanguage = new Property(5, String.class, "localeLanguage", false, "LOCALE_LANGUAGE");
        public final static Property LocaleCountry = new Property(6, String.class, "localeCountry", false, "LOCALE_COUNTRY");
        public final static Property Remember = new Property(7, boolean.class, "remember", false, "REMEMBER");
        public final static Property Login = new Property(8, boolean.class, "login", false, "LOGIN");
        public final static Property MessageCount = new Property(9, int.class, "messageCount", false, "MESSAGE_COUNT");
        public final static Property ViewJob = new Property(10, boolean.class, "viewJob", false, "VIEW_JOB");
        public final static Property ViewMessage = new Property(11, boolean.class, "viewMessage", false, "VIEW_MESSAGE");
        public final static Property JobSearch = new Property(12, boolean.class, "jobSearch", false, "JOB_SEARCH");
        public final static Property NewRequest = new Property(13, boolean.class, "newRequest", false, "NEW_REQUEST");
        public final static Property GuestReuqest = new Property(14, boolean.class, "guestReuqest", false, "GUEST_REUQEST");
        public final static Property NonGuestRequest = new Property(15, boolean.class, "nonGuestRequest", false, "NON_GUEST_REQUEST");
        public final static Property InterDepartmentRequest = new Property(16, boolean.class, "interDepartmentRequest", false, "INTER_DEPARTMENT_REQUEST");
        public final static Property NewMessage = new Property(17, boolean.class, "newMessage", false, "NEW_MESSAGE");
        public final static Property Minibar = new Property(18, boolean.class, "minibar", false, "MINIBAR");
        public final static Property Favorite = new Property(19, boolean.class, "favorite", false, "FAVORITE");
        public final static Property GuestDetail = new Property(20, boolean.class, "guestDetail", false, "GUEST_DETAIL");
        public final static Property RoomStatus = new Property(21, boolean.class, "roomStatus", false, "ROOM_STATUS");
    }

    ;


    public DBSessionContextDao(DaoConfig config) {
        super(config);
    }

    public DBSessionContextDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /**
     * Creates the underlying database table.
     */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists ? "IF NOT EXISTS " : "";
        db.execSQL("CREATE TABLE " + constraint + "'tb_session' (" + //
                "'_id' INTEGER PRIMARY KEY ," + // 0: id
                "'USER_ID' TEXT NOT NULL ," + // 1: userId
                "'SESSION_TOKEN' TEXT," + // 2: sessionToken
                "'USERNAME' TEXT NOT NULL ," + // 3: username
                "'PASSWORD' TEXT NOT NULL ," + // 4: password
                "'LOCALE_LANGUAGE' TEXT NOT NULL ," + // 5: localeLanguage
                "'LOCALE_COUNTRY' TEXT," + // 6: localeCountry
                "'REMEMBER' INTEGER NOT NULL ," + // 7: remember
                "'LOGIN' INTEGER NOT NULL ," + // 8: login
                "'MESSAGE_COUNT' INTEGER NOT NULL ," + // 9: messageCount
                "'VIEW_JOB' INTEGER NOT NULL ," + // 10: viewJob
                "'VIEW_MESSAGE' INTEGER NOT NULL ," + // 11: viewMessage
                "'JOB_SEARCH' INTEGER NOT NULL ," + // 12: jobSearch
                "'NEW_REQUEST' INTEGER NOT NULL ," + // 13: newRequest
                "'GUEST_REUQEST' INTEGER NOT NULL ," + // 14: guestReuqest
                "'NON_GUEST_REQUEST' INTEGER NOT NULL ," + // 15: nonGuestRequest
                "'INTER_DEPARTMENT_REQUEST' INTEGER NOT NULL ," + // 16: interDepartmentRequest
                "'NEW_MESSAGE' INTEGER NOT NULL ," + // 17: newMessage
                "'MINIBAR' INTEGER NOT NULL ," + // 18: minibar
                "'FAVORITE' INTEGER NOT NULL ," + // 19: favorite
                "'GUEST_DETAIL' INTEGER NOT NULL ," + // 20: guestDetail
                "'ROOM_STATUS' INTEGER NOT NULL );"); // 21: roomStatus
    }

    /**
     * Drops the underlying database table.
     */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "'tb_session'";
        db.execSQL(sql);
    }

    /**
     * @inheritdoc
     */
    @Override
    protected void bindValues(SQLiteStatement stmt, DBSessionContext entity) {
        stmt.clearBindings();

        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
        stmt.bindString(2, entity.getUserId());

        String sessionToken = entity.getSessionToken();
        if (sessionToken != null) {
            stmt.bindString(3, sessionToken);
        }
        stmt.bindString(4, entity.getUsername());
        stmt.bindString(5, entity.getPassword());
        stmt.bindLong(8, entity.getRemember() ? 1l : 0l);
        stmt.bindLong(9, entity.getLogin() ? 1l : 0l);
        stmt.bindLong(10, entity.getMessageCount());
        stmt.bindLong(11, entity.getMyJob() ? 1l : 0l);
        stmt.bindLong(12, entity.getViewMessage() ? 1l : 0l);
        stmt.bindLong(13, entity.getJobSearch() ? 1l : 0l);
        stmt.bindLong(14, entity.getNewRequest() ? 1l : 0l);
        stmt.bindLong(15, entity.getGuestReuqest() ? 1l : 0l);
        stmt.bindLong(16, entity.getNonGuestRequest() ? 1l : 0l);
        stmt.bindLong(17, entity.getInterDepartmentRequest() ? 1l : 0l);
        stmt.bindLong(18, entity.getNewMessage() ? 1l : 0l);
        stmt.bindLong(19, entity.getMinibar() ? 1l : 0l);
        stmt.bindLong(20, entity.getFavorite() ? 1l : 0l);
        stmt.bindLong(21, entity.getGuestDetail() ? 1l : 0l);
        stmt.bindLong(22, entity.getRoomStatus() ? 1l : 0l);
    }

    /**
     * @inheritdoc
     */
    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }

    /**
     * @inheritdoc
     */
    @Override
    public DBSessionContext readEntity(Cursor cursor, int offset) {
        DBSessionContext entity = new DBSessionContext( //
                cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
                cursor.getString(offset + 1), // userId
                cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2), // sessionToken
                cursor.getString(offset + 3), // username
                cursor.getString(offset + 4), // password
                cursor.getString(offset + 5), // localeLanguage
                cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6), // localeCountry
                cursor.getShort(offset + 7) != 0, // remember
                cursor.getShort(offset + 8) != 0, // login
                cursor.getInt(offset + 9), // messageCount
                cursor.getShort(offset + 10) != 0, // viewJob
                cursor.getShort(offset + 11) != 0, // viewMessage
                cursor.getShort(offset + 12) != 0, // jobSearch
                cursor.getShort(offset + 13) != 0, // newRequest
                cursor.getShort(offset + 14) != 0, // guestReuqest
                cursor.getShort(offset + 15) != 0, // nonGuestRequest
                cursor.getShort(offset + 16) != 0, // interDepartmentRequest
                cursor.getShort(offset + 17) != 0, // newMessage
                cursor.getShort(offset + 18) != 0, // minibar
                cursor.getShort(offset + 19) != 0, // favorite
                cursor.getShort(offset + 20) != 0, // guestDetail
                cursor.getShort(offset + 21) != 0 // roomStatus
        );
        return entity;
    }

    /**
     * @inheritdoc
     */
    @Override
    public void readEntity(Cursor cursor, DBSessionContext entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setUserId(cursor.getString(offset + 1));
        entity.setSessionToken(cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
        entity.setUsername(cursor.getString(offset + 3));
        entity.setPassword(cursor.getString(offset + 4));
        entity.setRemember(cursor.getShort(offset + 7) != 0);
        entity.setLogin(cursor.getShort(offset + 8) != 0);
        entity.setMessageCount(cursor.getInt(offset + 9));
        entity.setMyJob(cursor.getShort(offset + 10) != 0);
        entity.setMyMessage(cursor.getShort(offset + 11) != 0);
        entity.setJobSearch(cursor.getShort(offset + 12) != 0);
        entity.setNewRequest(cursor.getShort(offset + 13) != 0);
        entity.setGuestReuqest(cursor.getShort(offset + 14) != 0);
        entity.setNonGuestRequest(cursor.getShort(offset + 15) != 0);
        entity.setInterDepartmentRequest(cursor.getShort(offset + 16) != 0);
        entity.setNewMessage(cursor.getShort(offset + 17) != 0);
        entity.setMinibar(cursor.getShort(offset + 18) != 0);
        entity.setFavorite(cursor.getShort(offset + 19) != 0);
        entity.setGuestDetail(cursor.getShort(offset + 20) != 0);
        entity.setRoomStatus(cursor.getShort(offset + 21) != 0);
    }

    /**
     * @inheritdoc
     */
    @Override
    protected Long updateKeyAfterInsert(DBSessionContext entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }

    /**
     * @inheritdoc
     */
    @Override
    public Long getKey(DBSessionContext entity) {
        if (entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    /**
     * @inheritdoc
     */
    @Override
    protected boolean isEntityUpdateable() {
        return true;
    }

}
