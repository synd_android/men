package com.fcscs.android.mconnectv3.http;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.URISyntaxException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.Transport;
import org.xmlpull.v1.XmlPullParserException;

import android.content.Context;
import android.util.Log;
import android.webkit.URLUtil;

import com.fcscs.android.mconnectv3.common.ConfigContext;
import com.fcscs.android.mconnectv3.common.McConstants;
import com.fcscs.android.mconnectv3.common.McEnums.ServiceType;
import com.fcscs.android.mconnectv3.common.util.McUtils;

public class WebServiceBase {


    private static final String TAG = WebServiceBase.class.getSimpleName();

    private String requestBody;
    private String responseBody;
    private Throwable throwable;
    private boolean enableUI;

    private int connectTimeout = McConstants.WS_CONNECT_TIMEOUT;
    private int readTimeout = McConstants.WS_READ_TIMEOUT;

    public void setEnableUI(boolean enableUI) {
        this.enableUI = enableUI;
    }

    public boolean isEnableUI() {
        return enableUI;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public String getResponseBody() {
        return responseBody;
    }

    public Throwable getThrowable() {
        return throwable;
    }

    public boolean isEconnect() {
        return ServiceType.ECONNECT.equals(ConfigContext.getInstance().getServiceType());
    }

    public boolean isCConnect() {
        return ServiceType.CCONNECT.equals(ConfigContext.getInstance().getServiceType());
    }

    protected SoapObject callWebService(SoapObject request, String serviceUrl, String wsUrlSuffix, String namespace,
                                        String responseName) {
        return callWebService(request, serviceUrl, wsUrlSuffix, "", namespace, responseName, false);
    }

    protected SoapObject callWebService(SoapObject request, String serviceUrl, String wsUrlSuffix, String soapAction,
                                        String namespace, String responseName) {
        return callWebService(request, serviceUrl, wsUrlSuffix, soapAction, namespace, responseName, false);
    }

    protected SoapObject callWebService(SoapObject request, String serviceUrl, String wsUrlSuffix, String soapAction,
                                        String namespace, String responseName, boolean base64Serialize) {

        Object response = null;

        StringBuffer sb = new StringBuffer(serviceUrl);
        if (wsUrlSuffix != null) {
            sb.append(wsUrlSuffix);
        }
        final String url = sb.toString();

        requestBody = null;
        responseBody = null;
        throwable = null;

        if (McConstants.ENABLE_DEBUG_MODE) {
            McUtils.saveSoapToSDCard(url, request, null, null);
        }

        try {
            response = connect(url, request, soapAction, base64Serialize);
        } catch (Throwable ex) {
            throwable = ex;
            Log.e(TAG, "failed to call web service", ex);
        }

        if (McConstants.ENABLE_DEBUG_MODE || throwable != null) {
            McUtils.saveSoapToSDCard(url, null, responseBody, throwable);
        }

        if (response != null && (response instanceof SoapObject)) {

            if (McUtils.expectedSoapObject(response, namespace, responseName)) {
                return (SoapObject) response;
            } else {
                Log.w(TAG, "Response Soap: " + ((SoapObject) response).toString());
            }
        }

        return null;
    }

    protected SoapObject callWebService(Context ctx, SoapObject request, String wsUrlSuffix, String namespace,
                                        String responseName) {
        return callWebService(request, ConfigContext.getInstance().getServiceUrl(), wsUrlSuffix, namespace, responseName);
    }

    protected Object connect(String wsUrl, SoapObject request, String soapAction, boolean base64Serialize) throws XmlPullParserException,
            IOException, URISyntaxException, HttpException, ConnectionException {

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
//		if (base64Serialize) {
//			McMarshalBase64 marshal = new McMarshalBase64();
//		     marshal.register(envelope);
//		}
        envelope.setOutputSoapObject(request);
        envelope.setAddAdornments(false);

        Transport transport = null;
        try {
            if (URLUtil.isHttpsUrl(wsUrl) || URLUtil.isHttpUrl(wsUrl)) {
                if (wsUrl.contains("\n") || wsUrl.contains("\r")) {
                    wsUrl = wsUrl.replace("\n", "").replace("\r", "");
                }
                Log.i(TAG, "URL: " + wsUrl);
            }
            Log.i(TAG, "Method: " + request.getName());
            transport = getHttpTransportSE(wsUrl);
            transport.debug = true;
            transport.call(soapAction, envelope);
            requestBody = transport.requestDump;
            responseBody = transport.responseDump;
        } catch (SocketTimeoutException e) {
            throw new ConnectionException("Socket timeout!", e);
        } catch (IOException e) {
            if (transport instanceof ResponseInfo) {
                ResponseInfo info = (ResponseInfo) transport;
                HttpException httpEx = new HttpException(e, info.getResponseCode());
                throw httpEx;
            } else {
                throw e;
            }
        }
        return envelope.bodyIn;
    }

    protected Transport getHttpTransportSE(String url) throws URISyntaxException {
        return new FcsHttpTransportSE(url, getConnectTimeout(), getReadTimeout());
    }

    public int getConnectTimeout() {
        return connectTimeout;
    }

    public void setConnectTimeout(int connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public int getReadTimeout() {
        return readTimeout;
    }

    public void setReadTimeout(int readTimeout) {
        this.readTimeout = readTimeout;
    }

}
