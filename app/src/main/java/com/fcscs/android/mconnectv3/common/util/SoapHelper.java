package com.fcscs.android.mconnectv3.common.util;

import java.util.Calendar;
import java.util.Date;

import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;

import android.util.Log;

public class SoapHelper {


    private static final String TAG = SoapHelper.class.getSimpleName();

    /**
     * example:
     * getInnerSoapObject(soap, 0, 0, "guest");
     *
     * @param soap
     * @param params
     * @return
     */
    public static SoapObject getInnerSoapObject(SoapObject soap, Object... params) {

        if (params == null || params.length == 0) {
            throw new NullPointerException("Invalid parameters");
        }

        SoapObject r = soap;
        for (int i = 0; i < params.length; i++) {
            if (r == null) {
                return null;
            }
            if (params[i] instanceof Number) {
                int num = ((Number) params[i]).intValue();
                if (num >= 0 && num < r.getPropertyCount() && r.getProperty(num) instanceof SoapObject) {
                    r = (SoapObject) r.getProperty(num);
                } else {
                    return null;
                }
            } else if (params[i] instanceof String) {
                String str = params[i].toString();
                if (r.hasProperty(str) && r.getProperty(str) instanceof SoapObject) {
                    r = (SoapObject) r.getProperty(str);
                } else {
                    return null;
                }
            } else {
                return null;
            }
        }

        return r;
    }

    public static Date getDateProperty(SoapObject soap, String propertyName, String pattern, Date defValue) {
        if (soap.hasProperty(propertyName)) {
            Object property = soap.getProperty(propertyName);
            if (property instanceof SoapPrimitive) {
                Date date = DateTimeHelper.parseDateStr(property.toString(), pattern, defValue);
                if (date != null && isDefaultDate(date)) {
                    return defValue;
                }
                return date;
            }
        }
        return defValue;
    }

    public static boolean isDefaultDate(Date date) {
        if (date == null) {
            return true;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        if (cal.get(Calendar.YEAR) == 1900
                && cal.get(Calendar.MONTH) == 0
                && cal.get(Calendar.DAY_OF_MONTH) == 1
                && cal.get(Calendar.HOUR_OF_DAY) == 0
                && cal.get(Calendar.MINUTE) == 0
                && cal.get(Calendar.SECOND) == 0) {
            return true;
        }
        return false;
    }

    public static Date getDefaultDate() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1900);
        cal.set(Calendar.MONTH, 0);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static String getStringProperty(SoapObject soap, String propertyName, String defValue) {
        if (soap.hasProperty(propertyName)) {
            Object property = soap.getProperty(propertyName);
            if (property instanceof SoapPrimitive) {
                return property.toString();
            }
        }
        return defValue;
    }

    public static String getStringAttribute(SoapObject soap, String attributeName, String defValue) {
        if (soap.hasAttribute(attributeName)) {
            String property = (String) soap.getAttribute(attributeName);
            if (property != null) {
                return property;
            }
        }
        return defValue;
    }

    public static Boolean getBooleanProperty(SoapObject soap, String propertyName, Boolean defValue) {
        if (soap.hasProperty(propertyName)) {
            Object property = soap.getProperty(propertyName);
            if (property instanceof SoapPrimitive) {
                try {
                    return Boolean.valueOf(property.toString());
                } catch (Exception e) {
                    Log.w(TAG, "getPrimitivePropertyAsBoolean:" + propertyName + "=" + property);
                }
            }
        }
        return defValue;
    }

    public static Boolean getBooleanAttribute(SoapObject soap, String attributeName, Boolean defValue) {
        if (soap.hasAttribute(attributeName)) {
            String property = (String) soap.getAttribute(attributeName);
            if (property != null) {
                try {
                    boolean r = Boolean.valueOf(property);
                    return r;
                } catch (Exception e) {
                    Log.w(TAG, "getAttributeAsBoolean:" + attributeName + "=" + property);
                }
            }
        }
        return defValue;
    }

    public static Integer getIntegerProperty(SoapObject soap, String propertyName, Integer defValue) {
        if (soap.hasProperty(propertyName)) {
            Object property = soap.getProperty(propertyName);
            if (property instanceof SoapPrimitive) {
                try {
                    return Integer.valueOf(property.toString());
                } catch (Exception e) {
                    Log.w(TAG, "getPrimitivePropertyAsInteger:" + propertyName + "=" + property);
                }
            }
        }
        return defValue;
    }

    public static Integer getIntegerAttribute(SoapObject soap, String attributeName, Integer defValue) {
        if (soap.hasAttribute(attributeName)) {
            String property = (String) soap.getAttribute(attributeName);
            if (property != null) {
                try {
                    int r = Integer.valueOf(property);
                    return r;
                } catch (Exception e) {
                    Log.w(TAG, "getAttributeAsInteger:" + attributeName + "=" + property);
                }
            }
        }
        return defValue;
    }

    public static Long getLongProperty(SoapObject soap, String propertyName, Long defValue) {
        if (soap.hasProperty(propertyName)) {
            Object property = soap.getProperty(propertyName);
            if (property instanceof SoapPrimitive) {
                try {
                    return Long.valueOf(property.toString());
                } catch (Exception e) {
                    Log.w(TAG, "getPrimitivePropertyAsLong:" + propertyName + "=" + property);
                }
            }
        }
        return defValue;
    }

    public static Long getLongAttribute(SoapObject soap, String attributeName, Long defValue) {
        if (soap.hasAttribute(attributeName)) {
            String property = (String) soap.getAttribute(attributeName);
            if (property != null) {
                try {
                    long r = Long.valueOf(property);
                    return r;
                } catch (Exception e) {
                    Log.w(TAG, "getAttributeAsLong:" + attributeName + "=" + property);
                }
            }
        }
        return defValue;
    }

    public static String getPropertyName(SoapObject so, int i) {
        PropertyInfo pi = new PropertyInfo();
        so.getPropertyInfo(i, pi);
        return pi.name;
    }


}
