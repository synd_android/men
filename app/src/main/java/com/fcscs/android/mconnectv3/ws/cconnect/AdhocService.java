package com.fcscs.android.mconnectv3.ws.cconnect;

import java.util.Date;
import java.util.List;

import org.ksoap2.serialization.SoapObject;

import android.content.Context;

import com.fcscs.android.mconnectv3.common.SessionContext;
import com.fcscs.android.mconnectv3.common.util.DateTimeHelper;
import com.fcscs.android.mconnectv3.http.WebServiceBase;
import com.fcscs.android.mconnectv3.manager.model.GroupMessageIdModel;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.CreateGroupMsgResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.CreateIndividualMsgResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.FindMsgResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.FindRunnerGroupResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.FindRunnerResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.GetGroupRunnerListResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.GetMsgResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.GetNotifyInfoResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.GetTemplateListResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.GetTemplateResponse;

public class AdhocService extends WebServiceBase {

    private static final String GET_NOTIFY_INFO_RESPONSE = "GetNotifyInfoResponse";
    private static final String GET_NOTIFY_INFO_REQUEST = "GetNotifyInfoRequest";
    private static final String CREATE_GROUP_MSG_RESPONSE = "CreateGroupMsgResponse";
    private static final String CREATE_GROUP_MSG_REQUEST = "CreateGroupMsgRequest";
    private static final String GET_GROUP_RUNNER_LIST_RESPONSE = "GetGroupRunnerListResponse";
    private static final String GET_GROUP_RUNNER_LIST_REQUEST = "GetGroupRunnerListRequest";
    private static final String FIND_RUNNER_GROUP_RESPONSE = "FindRunnerGroupResponse";
    private static final String FIND_RUNNER_GROUP_REQUEST = "FindRunnerGroupRequest";
    private static final String WEBSERVICE_URL_SUFFIX = "ws/adhocService";
    private static final String NAMESPACE = "http://fcscs.com/ws/schemas/adhocservice";

    private static final String CREATE_INDIVIDUAL_MSG_RESPONSE = "CreateIndividualMsgResponse";
    private static final String CREATE_INDIVIDUAL_MSG_REQUEST = "CreateIndividualMsgRequest";
    private static final String FIND_RUNNER_RESPONSE = "FindRunnerResponse";
    private static final String FIND_RUNNER_REQUEST = "FindRunnerRequest";
    private static final String GET_TEMPLATE_RESPONSE = "GetTemplateResponse";
    private static final String GET_TEMPLATE_REQUEST = "GetTemplateRequest";
    private static final String GET_TEMPLATE_LIST_RESPONSE = "GetTemplateListResponse";
    private static final String GET_TEMPLATE_LIST_REQUEST = "GetTemplateListRequest";

    private static final String FIND_MSG_REQUEST = "FindMsgRequest";
    private static final String FIND_MSG_RESPONSE = "FindMsgResponse";

    private static final String GET_MSG_REQUEST = "GetMsgRequest";
    private static final String GET_MSG_RESPONSE = "GetMsgResponse";

    private static final String UPDATE_MSG_STATUS_REQUEST = "UpdateMsgStatusRequest";
    private static final String UPDATE_MSG_STATUS_RESPONSE = "UpdateMsgStatusResponse";

    private static final String DELETE_MSG_REQUEST = "DeleteMsgRequest";
    private static final String DELETE_MSG_RESPONSE = "DeleteMsgResponse";

    private static AdhocService instance;
    private static AdhocService instanceNonUI;

    private AdhocService() {
    }

    public static AdhocService getInstance() {
        if (instance == null) {
            instance = new AdhocService();
        }
        instance.setEnableUI(true);
        return instance;
    }

    public static AdhocService getInstanceNonUI() {
        if (instanceNonUI == null) {
            instanceNonUI = new AdhocService();
        }
        instanceNonUI.setEnableUI(false);
        return instanceNonUI;
    }

    public GetTemplateListResponse getMsgTemplates(Context ctx) {

        SoapObject request = new SoapObject(NAMESPACE, GET_TEMPLATE_LIST_REQUEST);
        request.addAttribute("fcsUserId", SessionContext.getInstance().getFcsUserId());
        request.addAttribute("wsSessionId", SessionContext.getInstance().getWsSessionId());

        SoapObject response = callWebService(ctx, request, WEBSERVICE_URL_SUFFIX, NAMESPACE, GET_TEMPLATE_LIST_RESPONSE);
        if (response != null) {
            return new GetTemplateListResponse(response);
        } else {
            return null;
        }

    }

    public GetTemplateResponse getMsgTemplate(Context ctx, long templateId) {

        SoapObject request = new SoapObject(NAMESPACE, GET_TEMPLATE_REQUEST);
        request.addAttribute("fcsUserId", SessionContext.getInstance().getFcsUserId());
        request.addAttribute("wsSessionId", SessionContext.getInstance().getWsSessionId());
        request.addAttribute("templateId", templateId);

        SoapObject response = callWebService(ctx, request, WEBSERVICE_URL_SUFFIX, NAMESPACE, GET_TEMPLATE_RESPONSE);
        if (response != null) {
            return new GetTemplateResponse(response);
        } else {
            return null;
        }

    }

    public FindRunnerResponse findRunners(Context ctx, String term, Long deptId, int start, int numResult) {

        SoapObject request = new SoapObject(NAMESPACE, FIND_RUNNER_REQUEST);
        request.addAttribute("fcsUserId", SessionContext.getInstance().getFcsUserId());
        request.addAttribute("wsSessionId", SessionContext.getInstance().getWsSessionId());
        request.addAttribute("start", start);
        request.addAttribute("numResults", numResult);
        if (term != null) {
            request.addProperty("term", term);
        }
        if (deptId != null) {
            request.addProperty("departmentId", deptId);
        }

        SoapObject response = callWebService(ctx, request, WEBSERVICE_URL_SUFFIX, NAMESPACE, FIND_RUNNER_RESPONSE);
        if (response != null) {
            return new FindRunnerResponse(response);
        } else {
            return null;
        }
    }

    public CreateIndividualMsgResponse createIndividualMsg(Context ctx, Long templateId, String message, List<Long> runnerIds) {

        SoapObject request = new SoapObject(NAMESPACE, CREATE_INDIVIDUAL_MSG_REQUEST);
        request.addAttribute("fcsUserId", SessionContext.getInstance().getFcsUserId());
        request.addAttribute("wsSessionId", SessionContext.getInstance().getWsSessionId());
        request.addAttribute("message", message);
        if (templateId != null) {
            request.addProperty("templateId", templateId);
        }
        for (Long id : runnerIds) {
            request.addProperty("runnerId", id);
        }

        SoapObject response = callWebService(ctx, request, WEBSERVICE_URL_SUFFIX, NAMESPACE, CREATE_INDIVIDUAL_MSG_RESPONSE);
        if (response != null) {
            return new CreateIndividualMsgResponse(response);
        } else {
            return null;
        }

    }

    public FindRunnerGroupResponse findRunnerGroups(Context ctx, String term) {

        SoapObject request = new SoapObject(NAMESPACE, FIND_RUNNER_GROUP_REQUEST);
        request.addAttribute("fcsUserId", SessionContext.getInstance().getFcsUserId());
        request.addAttribute("wsSessionId", SessionContext.getInstance().getWsSessionId());
        if (term != null) {
            request.addProperty("term", term);
        }

        SoapObject response = callWebService(ctx, request, WEBSERVICE_URL_SUFFIX, NAMESPACE, FIND_RUNNER_GROUP_RESPONSE);
        if (response != null) {
            return new FindRunnerGroupResponse(response);
        } else {
            return null;
        }

    }

    public GetGroupRunnerListResponse getGroupRunners(Context ctx, long groupId) {

        SoapObject request = new SoapObject(NAMESPACE, GET_GROUP_RUNNER_LIST_REQUEST);
        request.addAttribute("fcsUserId", SessionContext.getInstance().getFcsUserId());
        request.addAttribute("wsSessionId", SessionContext.getInstance().getWsSessionId());
        request.addAttribute("groupId", groupId);

        SoapObject response = callWebService(ctx, request, WEBSERVICE_URL_SUFFIX, NAMESPACE, GET_GROUP_RUNNER_LIST_RESPONSE);
        if (response != null) {
            return new GetGroupRunnerListResponse(response);
        } else {
            return null;
        }

    }

    public CreateGroupMsgResponse createGroupMessage(Context ctx, String message, Long templateId, List<GroupMessageIdModel> models) {

        SoapObject request = new SoapObject(NAMESPACE, CREATE_GROUP_MSG_REQUEST);

        request.addAttribute("fcsUserId", SessionContext.getInstance().getFcsUserId());
        request.addAttribute("wsSessionId", SessionContext.getInstance().getWsSessionId());
        request.addAttribute("message", message);
        if (templateId != null) {
            request.addAttribute("templateId", templateId);
        }

        String itemName = "runner";
        for (GroupMessageIdModel model : models) {
            SoapObject item = new SoapObject(NAMESPACE, itemName);
            item.addAttribute("runnerId", model.getRunnerId());
            if (model.getGroupId() != null) {
                item.addProperty("groupId", model.getGroupId());
            }
            request.addSoapObject(item);
        }

        SoapObject response = callWebService(ctx, request, WEBSERVICE_URL_SUFFIX, NAMESPACE, CREATE_GROUP_MSG_RESPONSE);
        if (response != null) {
            return new CreateGroupMsgResponse(response);
        } else {
            return null;
        }

    }

    public FindMsgResponse findMsg(Context ctx, Date createdDate, int start, int numResults) {
        SoapObject request = new SoapObject(NAMESPACE, FIND_MSG_REQUEST);
        request.addAttribute("fcsUserId", SessionContext.getInstance().getFcsUserId());
        request.addAttribute("wsSessionId", SessionContext.getInstance().getWsSessionId());
        request.addAttribute("start", start);
        request.addAttribute("numResults", numResults);
        request.addAttribute("createdDate", DateTimeHelper.FORMATTER_DATETIME_24H.format(createdDate));

        SoapObject response = callWebService(ctx, request, WEBSERVICE_URL_SUFFIX, NAMESPACE, FIND_MSG_RESPONSE);
        if (response != null) {
            return new FindMsgResponse(response);
        } else {
            return null;
        }
    }

    public GetMsgResponse getMsg(Context ctx, long msgDoneId) {

        SoapObject request = new SoapObject(NAMESPACE, GET_MSG_REQUEST);
        request.addAttribute("fcsUserId", SessionContext.getInstance().getFcsUserId());
        request.addAttribute("wsSessionId", SessionContext.getInstance().getWsSessionId());
        request.addAttribute("msgDoneId", msgDoneId);

        SoapObject response = callWebService(ctx, request, WEBSERVICE_URL_SUFFIX, NAMESPACE, GET_MSG_RESPONSE);
        if (response != null) {
            return new GetMsgResponse(response);
        } else {
            return null;
        }
    }

    public Boolean updateMsgStatus(Context ctx, long msgDoneId) {

        SoapObject request = new SoapObject(NAMESPACE, UPDATE_MSG_STATUS_REQUEST);
        request.addAttribute("fcsUserId", SessionContext.getInstance().getFcsUserId());
        request.addAttribute("wsSessionId", SessionContext.getInstance().getWsSessionId());
        request.addAttribute("msgDoneId", msgDoneId);
        request.addAttribute("ack", true);

        SoapObject response = callWebService(ctx, request, WEBSERVICE_URL_SUFFIX, NAMESPACE, UPDATE_MSG_STATUS_RESPONSE);
        if (response != null) {
            return Boolean.valueOf(response.getAttributeAsString("result"));
        } else {
            return null;
        }
    }

    public Boolean deleteMsg(Context ctx, long msgDoneId) {

        SoapObject request = new SoapObject(NAMESPACE, DELETE_MSG_REQUEST);
        request.addAttribute("fcsUserId", SessionContext.getInstance().getFcsUserId());
        request.addAttribute("wsSessionId", SessionContext.getInstance().getWsSessionId());
        request.addAttribute("msgDoneId", msgDoneId);

        SoapObject response = callWebService(ctx, request, WEBSERVICE_URL_SUFFIX, NAMESPACE, DELETE_MSG_RESPONSE);
        if (response != null) {
            return Boolean.valueOf(response.getAttributeAsString("result"));
        } else {
            return null;
        }
    }

    public GetNotifyInfoResponse getNotifyInfo(Context ctx) {

        SoapObject request = new SoapObject(NAMESPACE, GET_NOTIFY_INFO_REQUEST);
        request.addAttribute("fcsUserId", SessionContext.getInstance().getFcsUserId());
        request.addAttribute("wsSessionId", SessionContext.getInstance().getWsSessionId());

        SoapObject response = callWebService(ctx, request, WEBSERVICE_URL_SUFFIX, NAMESPACE, GET_NOTIFY_INFO_RESPONSE);
        if (response != null) {
            return new GetNotifyInfoResponse(response);
        } else {
            return null;
        }
    }

}
