package com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.ksoap2.serialization.SoapObject;

import com.fcscs.android.mconnectv3.dao.entity.Runner;

public class GetGroupRunnerListResponse implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 354923322939317677L;

    private List<Runner> runnerList = new ArrayList<Runner>();

    public GetGroupRunnerListResponse() {
    }

    public GetGroupRunnerListResponse(SoapObject soap) {
        if (soap.getPropertyCount() > 0) {
            for (int i = 0; i < soap.getPropertyCount(); i++) {
                runnerList.add(new Runner((SoapObject) soap.getProperty(i)));
            }
        }
    }

    public List<Runner> getRunnerList() {
        return runnerList;
    }

    public void setRunnerList(List<Runner> runnerList) {
        this.runnerList = runnerList;
    }

}
