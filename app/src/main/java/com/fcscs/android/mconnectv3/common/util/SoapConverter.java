package com.fcscs.android.mconnectv3.common.util;

import java.util.Date;

import org.ksoap2.serialization.SoapObject;

import com.fcscs.android.mconnectv3.common.McEnums.JobStatus;
import com.fcscs.android.mconnectv3.manager.model.EquipmentModel;
import com.fcscs.android.mconnectv3.manager.model.JobModel;

public class SoapConverter {

    public static JobModel getECJob(SoapObject node) {
        JobModel jm = new JobModel();
        jm.setDate(SoapHelper.getDateProperty(node, "RequestedDate", DateTimeHelper.DEFAULT_FORMATTER_DATETIME_24H_T, null));
        jm.setRequestedDate(SoapHelper.getDateProperty(node, "RequestedDate", DateTimeHelper.DEFAULT_FORMATTER_DATETIME_24H_T, null));
        jm.setDeadline(SoapHelper.getDateProperty(node, "Deadline", DateTimeHelper.DEFAULT_FORMATTER_DATETIME_24H_T, null));
        jm.setCompletedTime(SoapHelper.getDateProperty(node, "ClosedDate", DateTimeHelper.DEFAULT_FORMATTER_DATETIME_24H_T, null));
        jm.setStatus(JobStatus.getJobStatusFromStatusId(SoapHelper.getIntegerProperty(node, "StatusID", 1)));
        jm.setIsAcked(!"Not Ack".equalsIgnoreCase(SoapHelper.getStringProperty(node, "AckStatus", "")));
        jm.setJobId(SoapHelper.getLongProperty(node, "ID", -1L));
        jm.setJobNum(SoapHelper.getStringProperty(node, "WorkOrder", ""));
        jm.setServiceItemName(SoapHelper.getStringProperty(node, "Item", ""));
        jm.setQuantity(SoapHelper.getIntegerProperty(node, "ItemQty", 1));
        jm.setRequestorName(SoapHelper.getStringProperty(node, "AssignBy", ""));
        jm.setGuestName(SoapHelper.getStringProperty(node, "GuestName", ""));
        jm.setPriorityName(SoapHelper.getStringProperty(node, "Priority", ""));
        jm.setReportedBy(SoapHelper.getStringProperty(node, "ReportedByName", ""));
        jm.setRoomNum(SoapHelper.getStringProperty(node, "RoomNo", ""));
        jm.setVip(SoapHelper.getStringProperty(node, "VIP", ""));
        jm.setRemarks(SoapHelper.getStringProperty(node, "Description", ""));
        jm.setEscalatedLevel(SoapHelper.getIntegerProperty(node, "EscalatedLevel", 0));
        jm.setCcjob(SoapHelper.getIntegerProperty(node, "CCJob", 0));
        jm.setEquipmentName(SoapHelper.getStringProperty(node, "EquipmentName", ""));
        jm.setEquipmentNo(SoapHelper.getStringProperty(node, "EquipmentNo", ""));
        jm.setManHours(SoapHelper.getStringProperty(node, "ManHours", ""));
        jm.setCost(SoapHelper.getStringProperty(node, "Cost", ""));
        jm.setAssignTo(SoapHelper.getStringProperty(node, "AssignTo", ""));
        jm.setServiceTypeID(SoapHelper.getIntegerProperty(node, "ServiceTypeID", 0));
        jm.setActionTaken(SoapHelper.getStringProperty(node, "ActionTaken", ""));
        jm.setIsInspected(SoapHelper.getIntegerProperty(node, "IsInspected", 0));

        jm.setServiceItemCode(SoapHelper.getStringProperty(node, "ServiceItemCode", ""));
        jm.setLocationCode(SoapHelper.getStringProperty(node, "LocationCode", ""));

        jm.setRequiredPicture(SoapHelper.getIntegerProperty(node, "RequiredPicture", 0));
        jm.setRequiredESignature(SoapHelper.getIntegerProperty(node, "RequiredESignature", 0));

        jm.setAcknowledgedBy(SoapHelper.getStringProperty(node, "AcknowledgedBy", ""));
        jm.setClosedByName(SoapHelper.getStringProperty(node, "ClosedByName", ""));
        return jm;
    }

    public static JobModel getCCJob(SoapObject response) {
        JobModel jm = new JobModel();
        jm.setRequestorName(SoapHelper.getStringAttribute(response, "requestorName", ""));
        jm.setRemarks(SoapHelper.getStringProperty(response, "remarks", ""));
        SoapObject soap = (SoapObject) response.getProperty(0);
        if (soap != null) {
            jm.setJobId(SoapHelper.getLongAttribute(soap, "jobId", -1L));
            jm.setJobNum(SoapHelper.getStringAttribute(soap, "jobCode", ""));
            jm.setRoomNum(SoapHelper.getStringProperty(soap, "roomNum", ""));
            jm.setStatus(JobStatus.convertFromCCStatus(SoapHelper.getStringAttribute(soap, "status", "")));
            jm.setDeadline(SoapHelper.getDateProperty(soap, "deadline", DateTimeHelper.DEFAULT_FORMATTER_DATETIME_24H, DateTimeHelper.getDateByTimeZone(new Date())));
            jm.setDate(SoapHelper.getDateProperty(soap, "created", DateTimeHelper.DEFAULT_FORMATTER_DATETIME_24H, DateTimeHelper.getDateByTimeZone(new Date())));
            jm.setQuantity(SoapHelper.getIntegerAttribute(soap, "quantity", 0));
            jm.setServiceItemName(SoapHelper.getStringAttribute(soap, "serviceItemName", ""));
            jm.setPriorityName(SoapHelper.getStringProperty(soap, "priorityName", ""));
            jm.setIsAcked(SoapHelper.getBooleanAttribute(soap, "acknowledge", false));
            jm.setLocationDesc(SoapHelper.getStringAttribute(soap, "locationDesc", ""));
            jm.setCcjob(SoapHelper.getIntegerProperty(soap, "CCJob", 0));
        }

        return jm;
    }

    public static EquipmentModel getEquipmentDetail(SoapObject response) {
        EquipmentModel em = new EquipmentModel();
        em.setEquipmentName(SoapHelper.getStringProperty(response, "EquipmentName", ""));
        em.setEquipmentNumber(SoapHelper.getStringProperty(response, "EquipmentNumber", ""));
        em.setLocation(SoapHelper.getStringProperty(response, "Location", ""));
        em.setEquipmentStatusID(SoapHelper.getIntegerProperty(response, "EquipmentStatusID", 0));
        em.setEquipmentStatus(SoapHelper.getStringProperty(response, "EquipmentStatus", ""));
        em.setJobStatusID(SoapHelper.getIntegerProperty(response, "JobStatusID", 0));
        em.setJobStatus(SoapHelper.getStringProperty(response, "JobStatus", ""));
        em.setJobDetails(SoapHelper.getStringProperty(response, "JobDetails", ""));
        em.setGroup(SoapHelper.getStringProperty(response, "Group", ""));
        em.setTag(SoapHelper.getStringProperty(response, "Tag", ""));
        em.setManufacturer(SoapHelper.getStringProperty(response, "Manufacturer", ""));
        em.setModel(SoapHelper.getStringProperty(response, "Model", ""));
        em.setSerialNumber(SoapHelper.getStringProperty(response, "SerialNumber", ""));
        em.setDateCommissioned(SoapHelper.getDateProperty(response, "DateCommissioned", DateTimeHelper.DEFAULT_FORMATTER_DATETIME_24H, DateTimeHelper.getDateByTimeZone(new Date())));
        em.setWarrantyContractID(SoapHelper.getIntegerProperty(response, "WarrantyContractID", 0));
        em.setWarrantyExpiryDate(SoapHelper.getDateProperty(response, "warrantyExpiryDate", DateTimeHelper.DEFAULT_FORMATTER_DATETIME_24H, DateTimeHelper.getDateByTimeZone(new Date())));
        em.setWarrantyCompany(SoapHelper.getStringProperty(response, "WarrantyCompany", ""));
        em.setWarrantyContactPhone(SoapHelper.getStringProperty(response, "WarrantyContactPhone", ""));
        em.setWarrantyContactEmail(SoapHelper.getStringProperty(response, "WarrantyContactEmail", ""));

        return em;

    }

}
