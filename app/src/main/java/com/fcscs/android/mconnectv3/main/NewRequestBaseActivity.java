package com.fcscs.android.mconnectv3.main;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.ToggleButton;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.BaseActivity;
import com.fcscs.android.mconnectv3.common.McConstants;
import com.fcscs.android.mconnectv3.common.McOnEditorActionListener;
import com.fcscs.android.mconnectv3.common.ui.BottomBar;
import com.fcscs.android.mconnectv3.common.ui.McBackgroundAsyncTask;
import com.fcscs.android.mconnectv3.common.ui.McDatePickerDialog;
import com.fcscs.android.mconnectv3.common.ui.McProgressAsyncTask;
import com.fcscs.android.mconnectv3.common.ui.McTimePickerDialog;
import com.fcscs.android.mconnectv3.common.ui.RoundCornerListAdapter;
import com.fcscs.android.mconnectv3.common.util.DateTimeHelper;
import com.fcscs.android.mconnectv3.common.util.DialogHelper;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.dao.entity.Location;
import com.fcscs.android.mconnectv3.engineering.EquipmentListActivity;
import com.fcscs.android.mconnectv3.manager.model.EquipmentDetails;
import com.fcscs.android.mconnectv3.manager.model.GuestInfo;
import com.fcscs.android.mconnectv3.manager.model.MediaLibraryConfig;
import com.fcscs.android.mconnectv3.manager.model.ServiceRequest;
import com.fcscs.android.mconnectv3.manager.model.ServiceRequestItem;
import com.fcscs.android.mconnectv3.ws.cconnect.GuestRequestService;
import com.fcscs.android.mconnectv3.ws.cconnect.ServiceItemService;
import com.fcscs.android.mconnectv3.ws.cconnect.model.guestrequestservice.EngineeringRequestResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.guestrequestservice.GuestInfoResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.guestrequestservice.GuestRequestResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.GetLocationsResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.InterDepartmentRequestResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.ServiceItemViewModel;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.TopServiceItemResponse;
import com.fcscs.android.mconnectv3.ws.econnect.ECService;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class NewRequestBaseActivity extends BaseActivity {

    private static final int RQ_SEARCH_ITEMS = 1000;
    private static final int RQ_SEARCH_LOCATIONS = 1001;
    protected static final int RQ_SEARCH_RUNNER = 1004;
    private static final int RQ_RECORD_VOICE = 1005;
    private static final int RQ_CAPTURE_IMAGE = 1006;

    public final static int SCANNIN_GREQUEST_CODE = 1007;
    public final static int SCANNIN_LOCATION_CODE = 1008;
    protected static final int RQ_TRANSLATE = 1009;
    private static final int RQ_EQUIPMENT = 1010;

    public final static int GUEST_REQUEST_TYPE = 0;
    public final static int NON_GUEST_REQUEST_TYPE = 1;
    public final static int INTER_DEPARTMENT_REQUEST_TYPE = 3;
    protected EditText where;
    protected EditText serviceItemTv;
    private Spinner top10ServiceItem;
    private Spinner quantity;
    private EditText remarks;
    private BottomBar footBar;
    private ImageButton infoMsgRemark;
    // private String qty;

    private ImageView dateClear;
    private final static int DELAY = 0; // Delay n mins;


    private String TAG = getClass().getSimpleName();
    protected EditText date;
    private EditText time;
    private TimePickerDialog timePickDialog;
    protected Calendar dateAndTime = null;

    private McDatePickerDialog datePickerDialog;
    private McTimePickerDialog timePickerDialog;
    private FindTopServiceItemsTask task;
    private InterDepartmentRequestSender task1;
    private GuestRequestSender task2;
    private FindServiceItemTask task3;
    private FindLocationTask task4;
    private EngineeringRequestLoader task6;
    private ImageView ivMic;
    private ImageView ivCam;
    protected ImageView qrButton;

    protected ServiceRequest request = null;
    private ListView serviceItemLv;
    private ItemRoundAdapter serviceItemAdapter;
    private GuestListLoader task5;
    private ImageView btnTranslate;

    protected LinearLayout llEquip;
    protected Button btnEquip;
    protected ImageView ivEquip;
    protected Context mContext;

    protected LinearLayout llAcknow;
    protected ToggleButton tbPicture;
    protected ToggleButton tbSign;
    protected ArrayList<EquipmentDetails> mListEquipmentDetail = new ArrayList<>();
    protected EquipmentDetails mEquipmentSelected = null;

    //private int intPageSize = 10;
    //private int intPageIndex = 0;
    private List<ServiceItemViewModel> vListItems = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.common_new_request);
        mContext = this;

        llEquip = (LinearLayout) findViewById(R.id.ll_equip);
        btnEquip = (Button) findViewById(R.id.spin_equip);
        ivEquip = (ImageView) findViewById(R.id.iv_equip_clear);
        llEquip.setVisibility(View.GONE);
        tbPicture = (ToggleButton) findViewById(R.id.tb_picture);
        tbSign = (ToggleButton) findViewById(R.id.tb_sign);
        llAcknow = (LinearLayout) findViewById(R.id.ll_acknow);
        llAcknow.setVisibility(View.GONE);
        infoMsgRemark = (ImageButton) findViewById(R.id.info_msg_remark);


        //..Create By Chua...
        vListItems = new ArrayList<ServiceItemViewModel>();

        request = ServiceRequest.getInstance();
        request.setItem(new ServiceRequestItem());

        where = (EditText) findViewById(R.id.commonWhereEtId);
        where.setHint(getString(R.string.location));
        where.setClickable(true);

        qrButton = (ImageView) findViewById(R.id.qr_camera);

        dateClear = (ImageView) findViewById(R.id.commonDateClear);
        dateClear.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dateAndTime = null;
                date.setText("");
                time.setText("");
            }
        });

        TextView W = (TextView) findViewById(R.id.textView7);
        date = (EditText) findViewById(R.id.commonWhenDate);
        time = (EditText) findViewById(R.id.commonWhenTime);
        date.setHint(getString(R.string.date));
        time.setHint(getString(R.string.time));
        if (isCConnect()) {
            W.setVisibility(View.GONE);
            date.setVisibility(View.GONE);
            time.setVisibility(View.GONE);
            dateClear.setVisibility(View.GONE);
        } else {
            date.setHint(getString(R.string.please_select));
            date.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    popupDatePickDialog();
                }
            });
            time.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    popupTimePickDialog();
                }
            });
        }

        // spinner top 10 service item
        top10ServiceItem = (Spinner) findViewById(R.id.commonTopServiceSpId);
        initializeTopServiceItemSpinner(top10ServiceItem);

        // editText searchServiceItem
        serviceItemTv = (EditText) (EditText) findViewById(R.id.commonSearchServiceEtId);
        serviceItemTv.setHint(R.string.service_item);
        serviceItemTv.setOnEditorActionListener(new McOnEditorActionListener() {

            @Override
            public boolean onEnter(TextView view, int actionId, KeyEvent event) {
                if (McUtils.isNullOrEmpty(request.getLocationCode())) {
                    toast(getString(R.string.please_select_location));
                } else {
                    top10ServiceItem.setSelection(0);
                    //findServiceItems(serviceItemTv.getText().toString());
                    findServiceItems(1, serviceItemTv.getText().toString());
                    hideSoftInput(serviceItemTv);
                }
                return true;
            }
        });

        quantity = (Spinner) findViewById(R.id.commonQuantitySpId);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(R.layout.spinnerstyle);
        for (int i = 1; i < 100; i++) {
            adapter.add(i + "");
        }
        quantity.setAdapter(adapter);
        quantity.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                String qty = adapter.getItem(pos);
                request.getItem().setQuantity(Integer.parseInt(qty));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        remarks = (EditText) findViewById(R.id.commonRemarkEtId);
        if (McConstants.OFFLINE_MODE) {
            remarks.setText("Test");
        }
        remarks.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                validaterLength(remarks, 255);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        infoMsgRemark.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                toast(R.string.info_message);
            }
        });

        btnTranslate = (ImageView) findViewById(R.id.iv_translate);
        btnTranslate.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(NewRequestBaseActivity.this, TranslateActivity.class);
                if (remarks != null && remarks.getText().toString().length() > 0) {
                    i.putExtra("TEXT_TRANSLATE", remarks.getText().toString().trim());
                }
                NewRequestBaseActivity.this.startActivityForResult(i, RQ_TRANSLATE);
            }
        });

        serviceItemLv = (ListView) findViewById(R.id.service_item_list);
        serviceItemAdapter = new ItemRoundAdapter(this);
        serviceItemLv.setAdapter(serviceItemAdapter);

        // button bar
            footBar = (BottomBar) findViewById(R.id.footBarView);
        footBar.getLeftTv().setClickable(true);
        footBar.getLeftTv().setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onAddButtonClick();
            }
        });

        footBar.getMiddleTv().setClickable(true);
        footBar.getMiddleTv().setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onSumitButtonClick();
            }
        });

        footBar.getRightTv().setClickable(true);
        footBar.getRightTv().setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onCancelButtonClick();
            }
        });

        ivMic = (ImageView) findViewById(R.id.btn_mic);
        ivMic.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (request.isItemSelected() == false) {
                    toast(getString(R.string.please_select_serviceitem));
                    serviceItemTv.requestFocus();
                } else {
                    Intent i = new Intent(NewRequestBaseActivity.this, VoiceTagActivy.class);
                    NewRequestBaseActivity.this.startActivityForResult(i, RQ_RECORD_VOICE);
                }
            }
        });
        ivMic.setVisibility(View.INVISIBLE);

        ivCam = (ImageView) findViewById(R.id.camera);
        ivCam.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (request.isItemSelected() == false) {
                    toast(getString(R.string.please_select_serviceitem));
                    serviceItemTv.requestFocus();
                } else {
                    Intent i = new Intent(NewRequestBaseActivity.this, ImageCaptureActivity.class);
                    NewRequestBaseActivity.this.startActivityForResult(i, RQ_CAPTURE_IMAGE);
                }
            }
        });
        ivCam.setVisibility(View.INVISIBLE);

        ivEquip.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                btnEquip.setText(getString(R.string.title_equipment));
                mEquipmentSelected = null;
            }
        });
        btnEquip.setText(getString(R.string.title_equipment));
        btnEquip.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (McUtils.isNullOrEmpty(request.getLocationCode())) {
                    toast(getString(R.string.please_select_location));
                } else {
                    final Intent si = new Intent(getCurrentContext(), EquipmentListActivity.class);
                    si.putExtra(EquipmentListActivity.KEY_LOCATION, request.getLocationCode());
                    startActivityForResult(si, RQ_EQUIPMENT);
                }

            }
        });

        new GetMediaLibraryConfig(this).exec();
    }

    @Override
    protected void onDestroy() {
        ServiceRequest.destroyInstance();
        super.onDestroy();
    }

    private synchronized void popupDatePickDialog() {

        if (datePickerDialog != null && datePickerDialog.isShowing()) {
            return;
        }

        if (dateAndTime == null) {
            initDateAndTime();
        }
        Date currentTime = DateTimeHelper.getDateByTimeZone(dateAndTime.getTime());
        datePickerDialog = new McDatePickerDialog(this, dateAndTime.getTime());
        datePickerDialog.setPermanentTitle(getString(R.string.set_date));
        datePickerDialog.setOnClickDone(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Date dateNow = DateTimeHelper.getDateByTimeZone(new Date());
                DatePicker datePicker = datePickerDialog.getDatePicker();
                dateAndTime.set(Calendar.YEAR, datePicker.getYear());
                dateAndTime.set(Calendar.MONTH, datePicker.getMonth());
                dateAndTime.set(Calendar.DAY_OF_MONTH, datePicker.getDayOfMonth());
                dateAndTime.set(Calendar.HOUR_OF_DAY, dateNow.getHours());
                dateAndTime.set(Calendar.MINUTE, dateNow.getMinutes() + DELAY);
                datePickerDialog.dismiss();
                updateLabel();
            }
        });
        datePickerDialog.show();
    }

    private void initDateAndTime() {
        dateAndTime = Calendar.getInstance(Locale.getDefault());
    }

    private synchronized void popupTimePickDialog() {

        if (timePickerDialog != null && timePickerDialog.isShowing()) {
            return;
        }

        Date currentTime = null;
        if (dateAndTime == null) {
            currentTime = DateTimeHelper.getDateByTimeZone(new Date());
        } else {
            currentTime = dateAndTime.getTime();
        }
        timePickerDialog = new McTimePickerDialog(this, currentTime, true);
        timePickerDialog.setOnClickDone(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (dateAndTime == null) {
                    initDateAndTime();
                }
                TimePicker picker = timePickerDialog.getTimePicker();
                picker.clearFocus();
//				picker.setCurrentMinute(picker.getCurrentMinute());
//				picker.setCurrentHour(picker.getCurrentHour());
                dateAndTime.set(Calendar.HOUR_OF_DAY, picker.getCurrentHour());
                dateAndTime.set(Calendar.MINUTE, picker.getCurrentMinute() + DELAY);
                timePickerDialog.dismiss();
                updateLabel();
            }
        });
        timePickerDialog.show();
    }

    private void updateLabel() {
        date.setText(DateTimeHelper.DATE_FORMATTER4.format(dateAndTime.getTime()));
        time.setText(DateTimeHelper.TIME_FORMATTER.format(dateAndTime.getTime()));
    }

    protected void onAddButtonClick() {
        int intQty = request.getItem().getQuantity();

        if (request.isItemSelected() == false) {
            toast(getString(R.string.please_select_serviceitem));
            serviceItemTv.requestFocus();
        } else if (request.getItems().size() == 3) {
            toast(getString(R.string.max_3_items));
            //..No need by default it is 1...
            //} else if (request.getItem().getQuantity() <= 0) {
            //toast(getString(R.string.please_select_quantity));
        } else {
            String strRem = remarks.getText().toString().trim();
            addServiceItem(strRem);
        }

    }

    private void onCancelButtonClick() {

        if (request.getItems().size() > 0) {

            DialogHelper.showYesNoDialog(getCurrentContext(), R.string.confirm_cancel, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    setResult(Activity.RESULT_OK);
                    finish();
                }
            });

        } else {
            setResult(Activity.RESULT_OK);
            finish();
        }

        hideSoftInput(remarks);

    }

    protected void onSumitButtonClick() {
        toast("Hallo on Submit");
    }

    private void addServiceItem(String remarks) {

        boolean isEx = false;
        ServiceRequestItem mo = request.getItem();
        for (ServiceRequestItem sm : request.getItems()) {
            if (McUtils.isNullOrEmpty(sm.getItemId()) == false && sm.getItemId().equals(mo.getItemId())) {
                isEx = true;
                break;
            }
        }

        if (!isEx) {
            //..Modify By Chua :
            if (mo.getQuantity() == 0) {
                mo.setQuantity(1);
            }
            mo.setRemarks(remarks);
            request.getItems().add(mo);
            serviceItemAdapter.setItemList(request.getItems());
            serviceItemAdapter.notifyDataSetChanged();
            /**
             * Kha fix bug: Add New job  overlap
             */
            // A work around to enlarge the listview in scrollview
            // enlarge
            setListViewHeightBasedOnChildren(serviceItemLv);
            // end bug fix
            clearStatus();
            toast(R.string.add_item_successfully);
        } else {
            toast(R.string.item_has_added);
            request.setItem(new ServiceRequestItem());
            serviceItemTv.setText("");
        }
    }


    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount() && i < 5; i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    private void clearStatus() {
        request.setItem(new ServiceRequestItem());
        top10ServiceItem.setSelection(0);
        serviceItemTv.setText("");
        quantity.setSelection(0);
        remarks.setText("");
        serviceItemTv.requestFocus();
    }

    public void submitCheckRequest(String requestor, String locationCode, boolean guestRequest, int type) {
        if (task6 == null || AsyncTask.Status.RUNNING.equals(task6.getStatus()) == false) {
            task6 = new EngineeringRequestLoader(getCurrentContext(), requestor, locationCode, guestRequest, type);
            task6.exec();
        }
    }

    protected class EngineeringRequestLoader extends McProgressAsyncTask {

        private EngineeringRequestResponse res;
        private String mRequestor, mCurrecntLocationCode;
        private boolean mRBGuestRequest;
        private int mType;

        public EngineeringRequestLoader(Context context, String requestor, String currentLocationCode, boolean guestRequest, int type) {
            super(context);
            this.mRequestor = requestor;
            this.mCurrecntLocationCode = currentLocationCode;
            this.mRBGuestRequest = guestRequest;
            this.mType = type;
        }

        @Override
        public void doInBackground() {
            res = ECService.getInstance().createEngineeringRequest(getCurrentContext(), ServiceRequest.getInstance().getItems(), ((dateAndTime != null && dateAndTime.getTime() != null) ? dateAndTime.getTime() : null), mRequestor, mCurrecntLocationCode, /*mRBGuestRequest == true ? "1" : "2"*/ String.valueOf(mType));
        }

        private void submit() {
            switch (mType) {
                case GUEST_REQUEST_TYPE:
                    sumitGuestRequest();
                    break;
                case NON_GUEST_REQUEST_TYPE:
                    sumitInterDepartmentRequest();
                    break;
                case INTER_DEPARTMENT_REQUEST_TYPE:
                    sumitInterDepartmentRequest();
                    break;
            }
        }

        @Override
        public void onPostExecute() {
            if (res != null) {
                if (res.getReturnCode().equalsIgnoreCase("0")) {
                    final String returnCode1 = res.getReturnCode1();
                    final String returnCode2 = res.getReturnCode2();
                    final String returnCode3 = res.getReturnCode3();
                    if (returnCode1.equalsIgnoreCase("1")
                            || returnCode2.equalsIgnoreCase("1")
                            || returnCode3.equalsIgnoreCase("1")) {
                        StringBuffer errMsg = new StringBuffer();
                        if (returnCode1.equalsIgnoreCase("1")) {
                            ServiceRequestItem item = ServiceRequest.getInstance().getItems().get(0);
                            errMsg.append(", " + item.getName());
                        }
                        if (returnCode2.equalsIgnoreCase("1")) {
                            ServiceRequestItem item = ServiceRequest.getInstance().getItems().get(1);
                            errMsg.append(", " + item.getName());
                        }
                        if (returnCode3.equalsIgnoreCase("1")) {
                            ServiceRequestItem item = ServiceRequest.getInstance().getItems().get(2);
                            errMsg.append(", " + item.getName());
                        }
                        toast(getString(R.string.engineering_error_1) + errMsg.toString());
                    } else if (returnCode1.equalsIgnoreCase("4")
                            || returnCode2.equalsIgnoreCase("4")
                            || returnCode3.equalsIgnoreCase("4")) {
                        StringBuffer errMsg = new StringBuffer();
                        if (returnCode1.equalsIgnoreCase("4")) {
                            ServiceRequestItem item = ServiceRequest.getInstance().getItems().get(0);
                            errMsg.append(", " + item.getName());
                        }
                        if (returnCode2.equalsIgnoreCase("4")) {
                            ServiceRequestItem item = ServiceRequest.getInstance().getItems().get(1);
                            errMsg.append(", " + item.getName());
                        }
                        if (returnCode3.equalsIgnoreCase("4")) {
                            ServiceRequestItem item = ServiceRequest.getInstance().getItems().get(2);
                            errMsg.append(", " + item.getName());
                        }
                        toast(getString(R.string.engineering_error_4) + errMsg.toString());
                    } else if (returnCode1.equalsIgnoreCase("3") || returnCode2.equalsIgnoreCase("3") || returnCode3.equalsIgnoreCase("3")) {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NewRequestBaseActivity.this);
                        alertDialogBuilder.setMessage(getString(R.string.engineering_request_duplicate));

                        alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                ArrayList<ServiceRequestItem> listItemSubmit = getListItemCorrect(returnCode1, returnCode2, returnCode3, true);
                                ServiceRequest.getInstance().setItems(listItemSubmit);
                                sumitGuestRequest();
                            }
                        });
                        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // Do nothing
                                ArrayList<ServiceRequestItem> listItemSubmit = getListItemCorrect(returnCode1, returnCode2, returnCode3, false);
                                if (listItemSubmit.size() > 0) {
                                    ServiceRequest.getInstance().setItems(listItemSubmit);
                                    submit();
                                }
                            }
                        });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    } else {
                        ArrayList<ServiceRequestItem> listItemSubmit = getListItemCorrect(returnCode1, returnCode2, returnCode3, false);

                        ServiceRequest.getInstance().setItems(listItemSubmit);
                        submit();

                    }

                } else if (res.getReturnCode().equalsIgnoreCase("3")) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NewRequestBaseActivity.this);
                    alertDialogBuilder.setMessage(getString(R.string.engineering_request_duplicate));

                    alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            sumitGuestRequest();
                        }
                    });
                    alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Do nothing
                        }
                    });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();

                } else {
                    if (res.getErrorMessage() != null && res.getErrorMessage().length() > 0)
                        toast(res.getErrorMessage());
                    else if (res.getReturnCode().equalsIgnoreCase("1")) {
                        toast(getString(R.string.engineering_error_1));
                    } else if (res.getReturnCode().equalsIgnoreCase("2")) {
                        toast(getString(R.string.engineering_error_2));
                    } else if (res.getReturnCode().equalsIgnoreCase("4")) {
                        toast(getString(R.string.engineering_error_4));
                    } else {
                        toast(getString(R.string.engineering_error_all));
                    }
                }

            }

        }
    }

    protected ArrayList<ServiceRequestItem> getListItemCorrect(String returnCode1, String returnCode2, String returnCode3, boolean isGetDuclicate) {
        ArrayList<ServiceRequestItem> listItemSubmit = new ArrayList<ServiceRequestItem>();
        ArrayList<ServiceRequestItem> listItemCheck = ServiceRequest.getInstance().getItems();
        if (listItemCheck.size() > 0 && (returnCode1.equalsIgnoreCase("0") || (returnCode1.equalsIgnoreCase("3") && isGetDuclicate))) {
            listItemSubmit.add(listItemCheck.get(0));
        }
        if (listItemCheck.size() > 1 && (returnCode2.equalsIgnoreCase("0") || (returnCode2.equalsIgnoreCase("3") && isGetDuclicate))) {
            listItemSubmit.add(listItemCheck.get(1));
        }
        if (listItemCheck.size() > 2 && (returnCode3.equalsIgnoreCase("0") || (returnCode3.equalsIgnoreCase("3") && isGetDuclicate))) {
            listItemSubmit.add(listItemCheck.get(2));
        }
        return listItemSubmit;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }
        if (requestCode == RQ_SEARCH_ITEMS) {
            ServiceItemViewModel item = (ServiceItemViewModel) data.getSerializableExtra("ServiceItemViewModel");
            if (item != null) {
                onServiceItemSelected(item);
            }
        }

        if (requestCode == RQ_SEARCH_LOCATIONS) {
            Location loc = (Location) data.getSerializableExtra("Location");
            if (loc != null) {
                onLocationSelected(loc);
            }
        }

        if (requestCode == RQ_RECORD_VOICE) {
            File recordFile = (File) data.getSerializableExtra("record");
            request.getItem().setRecordedVoice(recordFile);
        }

        if (requestCode == RQ_CAPTURE_IMAGE) {
            File capturedImageFile = (File) data.getSerializableExtra("image");
            request.getItem().setCapturedImage(capturedImageFile);
        }

        /**
         * Fix #858 by Kha Tran
         * on 2015/07/10
         */
        if (requestCode == RQ_TRANSLATE) {
            String remarkText = remarks.getText().toString();
            String translatedText = data.getStringExtra(TranslateActivity.EXTRA_TRANSLATED_TEXT);
            if (translatedText.length() > 0) {
                boolean isMerge = data.getBooleanExtra(TranslateActivity.EXTRA_TRANSLATED_TEXT_IS_MERGE, true);
                if (isMerge) {
                    remarkText = remarkText + "\n" + translatedText;
                } else {
                    remarkText = translatedText;
                }
            }
            remarks.setText(remarkText);
        }
        if (requestCode == RQ_EQUIPMENT) {
            mEquipmentSelected = (EquipmentDetails) data.getSerializableExtra(EquipmentListActivity.KEY_EQUIPMENT);
            btnEquip.setText(mEquipmentSelected.getEquipmentName());
        }

    }

    protected void initializeTopServiceItemSpinner(Spinner sp) {

        if (task == null || AsyncTask.Status.RUNNING.equals(task.getStatus()) == false) {
            task = new FindTopServiceItemsTask(getCurrentContext(), sp);
            task.exec();
        }
    }

    protected class FindTopServiceItemsTask extends McBackgroundAsyncTask {

        private TopServiceItemResponse response;
        private Spinner sp;
        private ArrayAdapter<ServiceItemViewModel> adapter;

        public FindTopServiceItemsTask(Context context, Spinner sp) {
            super(context);
            this.sp = sp;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            adapter = new ArrayAdapter<ServiceItemViewModel>(getCurrentContext(), android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            ServiceItemViewModel m = new ServiceItemViewModel();
            m.setDisplayName(getString(R.string.top_item));
            m.setName(getString(R.string.top_item));
            adapter.add(m);
            sp.setAdapter(adapter);

            sp.setOnItemSelectedListener(new OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                    ServiceItemViewModel item = adapter.getItem(pos);
                    if (pos == 0) {
                        item = null;
                    }
                    onTopServiceItemSelected(pos, item);
                }

                @Override
                public void onNothingSelected(AdapterView<?> arg0) {
                }
            });

        }

        @Override
        public void doInBackground() {
            if (isCConnect()) {
                response = ServiceItemService.getInstance().getTopServiceItems(getCurrentContext(), 10);
            } else {
                response = ECService.getInstance().getTopServiceItems(getCurrentContext());
            }
        }

        @Override
        public void onPostExecute() {

            if (response != null && response.getServiceItems().size() > 0) {
                List<ServiceItemViewModel> top10List = (List<ServiceItemViewModel>) response.getServiceItems();
                for (ServiceItemViewModel item : top10List) {
                    adapter.add(item);
                }
                adapter.notifyDataSetChanged();
            }

        }

    }

    protected void onTopServiceItemSelected(int pos, ServiceItemViewModel item) {
        if (item == null) {
            return;
        }
        request.getItem().setItemId("" + item.getServiceItemId());
        request.getItem().setName(item.getName());
        if (item != null) {
            serviceItemTv.setText(item.getName());
        }
    }

    //..Modify By 	: Chua Kam Hoong
    //..Date 		: 20 May 2015
    //public void find(String string) {}
    //public void find(int PageSize, int PageIndex, String string) {}
    public void find(String string) {
    }

    protected void findGuests(EditText where, String term, boolean searchAll) {
        if (!searchAll) {
            if (term == null || term.trim().length() < 1) {
                return;
            }
        }

        if (task5 == null || AsyncTask.Status.RUNNING.equals(task5.getStatus()) == false) {
            task5 = new GuestListLoader(this, term);
            task5.exec();
        }
    }

    protected class GuestLoader extends McProgressAsyncTask {

        private List<GuestInfo> list;
        private GuestInfo info;

        public GuestLoader(Context context, GuestInfo info) {
            super(context);
            this.info = info;
        }

        @Override
        public void doInBackground() {
            if (isCConnect() && info.getRoomLinkId() != null) {
                long id = info.getRoomLinkId();
                list = GuestRequestService.getInstance().getGuestDetail(getCurrentContext(), id).getGuestList();
            }
        }

        @Override
        public void onPostExecute() {
        }

    }

    protected class GuestListLoader extends McProgressAsyncTask {

        public GuestListLoader(Context context, String term) {
            super(context);
            this.term = term;
        }

        public GuestListLoader(Context context, int PageIndex, String term) {
            super(context);
            intPageIndex = PageIndex;
            this.term = term;
        }

        private int intPageIndex;
        private String term;
        private GuestInfoResponse res;

        @Override
        public void doInBackground() {
//			if (term.matches("[0-9]+")) {
            // search by room
            //res = ECService.getInstance().getGuestListing(getCurrentContext(), term, "", "", "", "");
            res = ECService.getInstance().getGuestListing(getCurrentContext(), intPageIndex, term, "", "", "", "");
//			} else {
//				res = ECService.getInstance().getGuestListing(getCurrentContext(), "", term, "", "", "");
//			}
        }

        @Override
        public void onPostExecute() {
            if (res != null) {
                final List<GuestInfo> guests = res.getGuestList();
                if (guests.size() > 1) {
                    List<String> list = new ArrayList<String>();
                    for (GuestInfo guest : guests) {
                        String name = guest.getGuestUsername();
                        if (name != null && !name.equalsIgnoreCase("")) {
                            list.add("(" + guest.getRoomNum() + ")\n" + name);
                        } else {
                            list.add("(" + guest.getRoomNum() + ")");
                        }
                    }
                    String[] items = new String[list.size()];
                    items = (String[]) list.toArray(items);

                    AlertDialog.Builder builder = new AlertDialog.Builder(getCurrentContext());
                    builder.setTitle(getString(R.string.label_select_guest));
                    builder.setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (which > -1 && which < guests.size()) {
                                onGuestSelected(which, guests.get(which));
                                dialog.cancel();
                                hideSoftInput(where);
                            }
                        }
                    });
                    builder.show();
                } else if (guests.size() == 1) {
                    onGuestSelected(0, guests.get(0));
                    hideSoftInput(where);
                } else if (guests.size() == 0) {
                    toast(R.string.please_input_correctnum);
                    onEmptyGuests(term);
                }
            }

        }
    }

//	protected class FindGuestsTask extends McProgressAsyncTask {
//
//		private EditText where;
//		private String term;
//		private FindRoomGuestResponse res;
//
//		public FindGuestsTask(Context context, EditText where, String term) {
//			super(context);
//			this.where = where;
//			this.term = term;
//		}
//
//		@Override
//		public void doInBackground() {
//			res = ECService.getInstance().getRoomList(getCurrentContext(), SessionContext.getInstance().getFcsUserId(), term, term);
//		}
//
//		@Override
//		public void onPostExecute() {
//
//			if (res != null) {
//				final List<Guest> guests = res.getGuests();
//				if (guests.size() > 1) {
//					List<String> list = new ArrayList<String>();
//					for (Guest guest : guests) {
//						String name = guest.getGuestName().trim();
//						if (name != null && !name.equalsIgnoreCase("")) {
//							list.add("(" + guest.getRoomNum() + ")\n" + name);
//						} else {
//							list.add("(" + guest.getRoomNum() + ")");
//						}
//					}
//					String[] items = new String[list.size()];
//					items = (String[]) list.toArray(items);
//
//					AlertDialog.Builder builder = new AlertDialog.Builder(getCurrentContext());
//					builder.setTitle(getString(R.string.label_select_guest));
//					builder.setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {
//
//						@Override
//						public void onClick(DialogInterface dialog, int which) {
//							if (which > -1 && which < guests.size()) {
//								onGuestSelected(which, guests.get(which));
//								dialog.cancel();
//								hideSoftInput(where);
//							}
//						}
//					});
//					builder.show();
//				} else if (guests.size() == 1) {
//					onGuestSelected(0, guests.get(0));
//					hideSoftInput(where);
//				} else if (guests.size() == 0) {
//					toast(R.string.please_input_correctnum);
//					onEmptyGuests(term);
//				}
//			}
//
//		}
//
//	}

    protected void onGuestSelected(int pos, GuestInfo guest) {
    }

    protected void onEmptyGuests(String term) {
    }

    protected void findServiceItems(String term) {
        request.setItem(new ServiceRequestItem());
        if (task3 == null || AsyncTask.Status.RUNNING.equals(task3.getStatus()) == false) {
            task3 = new FindServiceItemTask(getCurrentContext(), term);
            task3.exec();
        }
    }

    //..Create By : Chua Kam Hoong
    protected void findServiceItems(int PageIndex, String term) {
        int intPageIndex = PageIndex;
        request.setItem(new ServiceRequestItem());
        if (task3 == null || AsyncTask.Status.RUNNING.equals(task3.getStatus()) == false) {
            task3 = new FindServiceItemTask(getCurrentContext(), intPageIndex, term);
            task3.exec();
        }
    }

    protected class FindServiceItemTask extends McProgressAsyncTask {

        private int intPageIndex = 0;
        private String term;
        private List<ServiceItemViewModel> list;

        public FindServiceItemTask(Context context, String term) {
            super(context);
            this.term = term;
        }

        public FindServiceItemTask(Context context, int PageIndex, String term) {
            super(context);
            intPageIndex = PageIndex;
            this.term = term;
        }

        @Override
        public void doInBackground() {
            if (isEconnect()) {
                //..Modify By 	: Chua Kam Hoong
                //..Date 		: 20 May 2015
                //list = ECService.getInstance().searchServiceItem(getCurrentContext(), term, "");
                //list = ECService.getInstance().searchServiceItem(getCurrentContext(), 10, 0, term, "");
                //list = ECService.getInstance().searchServiceItem(getCurrentContext(), vListItems, 10, "1", term, "");
                list = ECService.getInstance().searchServiceItem(getCurrentContext(), vListItems, intPageIndex, term, "");
            }
        }

        @Override
        public void onPostExecute() {

            if (list == null || list.size() == 0) {
                toast(R.string.no_item_found);
                onEmptyServiceItems(term);
            } else if (list.size() == 1) {
                onServiceItemSelected(list.get(0));
            } else {
                final Intent si = new Intent(getCurrentContext(), SearchServiceItemActivity.class);
                String strDebug1 = String.valueOf(intPageIndex);
                si.putExtra("searchTerm", term);
                si.putExtra("searchPageIndex", String.valueOf(intPageIndex));
                startActivityForResult(si, RQ_SEARCH_ITEMS);
            }

        }

    }

    protected void onServiceItemSelected(ServiceItemViewModel item) {
//		request.getItem().setItemId(item.getCode());
        request.getItem().setItemId("" + item.getServiceItemId());
        request.getItem().setName(item.getName());
        if (item != null) {
            serviceItemTv.setText(item.getName());
            if (item.getEnabledQuantity() == 0) {
                quantity.setEnabled(false);
            } else {
                quantity.setEnabled(true);
            }
        }
    }

    protected void onEmptyServiceItems(String term) {
    }

    protected void sumitGuestRequest() {

        if (task2 == null || AsyncTask.Status.RUNNING.equals(task2.getStatus()) == false) {
            task2 = new GuestRequestSender(getCurrentContext());
            task2.exec();
        }

    }

    protected class GuestRequestSender extends McProgressAsyncTask {

        private GuestRequestResponse res;

        public GuestRequestSender(Context context) {
            super(context);
        }

        @Override
        public void doInBackground() {
            String strRoomNo = request.getRoomNo();
            List<ServiceRequestItem> vListItems = request.getItems();
            Date dtDate = request.getScheduledDate();
            String strGuestName = request.getGuestName();

            res = ECService.getInstance().createGuestRequest(getCurrentContext(), request.getRoomNo(), request.getItems(), request.getScheduledDate(), request.getGuestName());

            if (res != null && res.getReturnCode().equals("0")) {
                int count = request.getItems().size();
                ServiceRequestItem item = null;
                String jobNo = null;
                if (count >= 1 && res.getReturnCode1().equalsIgnoreCase("0")) {
                    item = request.getItems().get(0);
                    jobNo = res.getErrorMessage1();
                    ECService.getInstance().postMediaLibraryContent(getCurrentContext(), item.getImageTag(), item.getCapturedImage(), 2, 1,
                            jobNo);
                    ECService.getInstance().postMediaLibraryContent(getCurrentContext(), item.getVoiceTag(), item.getRecordedVoice(), 2, 2,
                            jobNo);
                }
                if (count >= 2 && res.getReturnCode2().equalsIgnoreCase("0")) {
                    item = request.getItems().get(1);
                    jobNo = res.getErrorMessage2();
                    ECService.getInstance().postMediaLibraryContent(getCurrentContext(), item.getImageTag(), item.getCapturedImage(), 2, 1,
                            jobNo);
                    ECService.getInstance().postMediaLibraryContent(getCurrentContext(), item.getVoiceTag(), item.getRecordedVoice(), 2, 2,
                            jobNo);
                }
                if (count >= 3 && res.getReturnCode3().equalsIgnoreCase("0")) {
                    item = request.getItems().get(2);
                    jobNo = res.getErrorMessage3();
                    ECService.getInstance().postMediaLibraryContent(getCurrentContext(), item.getImageTag(), item.getCapturedImage(), 2, 1,
                            jobNo);
                    ECService.getInstance().postMediaLibraryContent(getCurrentContext(), item.getVoiceTag(), item.getRecordedVoice(), 2, 2,
                            jobNo);
                }
            }

        }

        @Override
        public void onPostExecute() {
            if (res != null) {
                if (isCConnect()) {
                    if (res.getJobNos() != null) {
                        if (res.getJobNos().size() > 0) {
                            StringBuilder sb1 = new StringBuilder();
                            for (int i = 0; i < res.getJobNos().size(); i++) {
                                sb1.append(res.getJobNos().get(i));
                                if (i != (res.getJobNos().size() - 1)) {
                                    sb1.append(" " + ",");
                                }
                            }
                            toast(getSuccessmessage(sb1.toString()));
                            setResult(Activity.RESULT_OK);
                            finish();
                        }
                    } else {
                        toast(R.string.request_create_error);
                    }
                } else {
                    if (res.getReturnCode().equals("0")) {

                        StringBuffer msg = new StringBuffer();
                        StringBuffer errMsg = new StringBuffer();

                        if (!res.getErrorMessage3().equals("") && res.getReturnCode3().equals("0")) {
                            msg.append(res.getErrorMessage3());
                            request.removeItemAtIndex(2);
                        } else if (request.getItems().size() >= 3 && !res.getErrorMessage3().equals("") && !res.getReturnCode3().equals("0")) {
                            ServiceRequestItem item = request.getItems().get(1);
                            errMsg.append(res.getErrorMessage3() + " - " + item.getName());
                        }
                        if (!res.getErrorMessage2().equals("") && res.getReturnCode2().equals("0")) {
                            msg.append(msg.toString().length() > 0 ? ", " + res.getErrorMessage2() : "" + res.getErrorMessage2());
                            request.removeItemAtIndex(1);
                        } else if (request.getItems().size() >= 2 && !res.getErrorMessage2().equals("") && !res.getReturnCode2().equals("0")) {
                            ServiceRequestItem item = request.getItems().get(1);
                            errMsg.append(errMsg.toString().length() > 0 ? " \n" : "" + res.getErrorMessage2() + " - " + item.getName());
                        }
                        if (!res.getErrorMessage1().equals("") && res.getReturnCode1().equals("0")) {
                            msg.append(msg.toString().length() > 0 ? ", " + res.getErrorMessage1() : "" + res.getErrorMessage1());
                            request.removeItemAtIndex(0);
                        } else if (request.getItems().size() >= 1 && !res.getErrorMessage1().equals("") && !res.getReturnCode1().equals("0")) {
                            ServiceRequestItem item = request.getItems().get(0);
                            errMsg.append(errMsg.toString().length() > 0 ? " \n" : "" + res.getErrorMessage1() + " - " + item.getName());
                        }
                        int count = request.getItems().size();
                        if (count == 0) {
                            toast(getSuccessmessage(msg.toString()));
                            setResult(Activity.RESULT_OK);
                            finish();
                        } else {
                            toast(getSuccessmessage(msg.toString(), errMsg.toString()));
                            serviceItemAdapter.setItemList(request.getItems());
                            serviceItemAdapter.notifyDataSetChanged();
                        }
                    } else {
                        StringBuffer msg = new StringBuffer();
                        if (!res.getErrorMessage1().equals("")) {
                            msg.append(res.getErrorMessage1());
                        }
                        if (!res.getErrorMessage2().equals("")) {
                            msg.append("," + res.getErrorMessage2());
                        }
                        if (!res.getErrorMessage3().equals("")) {
                            msg.append("," + res.getErrorMessage3());
                        }
                        toast(R.string.request_create_error);
                    }
                }
            }
        }

    }

    protected String getSuccessmessage(String info) {
        String dt = McUtils.formatDateTime(DateTimeHelper.getDateByTimeZone(new Date()));
        String msg = getString(R.string.job) + " " + info + " " + getString(R.string.created) + " \n" + dt;
        return msg;
    }

    protected String getSuccessmessage(String info, String error) {
        String dt = McUtils.formatDateTime(DateTimeHelper.getDateByTimeZone(new Date()));
        String msg = (info != null && info.length() > 0) ? getString(R.string.job) + " " + info + " " + getString(R.string.created) + " \n" + dt : "";
        return msg + " \n" + error;
    }


    protected void validaterLength(final EditText qtyEt, int length) {
        Editable editable = qtyEt.getText();
        int len = editable.length();
        if (len > length) {
            int selEndIndex = Selection.getSelectionEnd(editable);
            String str = editable.toString().trim();
            String newStr = str.substring(0, length);
            qtyEt.setText(newStr);
            editable = qtyEt.getText();
            int newLen = editable.length();
            if (selEndIndex > newLen) {
                selEndIndex = editable.length();
            }
            Selection.setSelection(editable, selEndIndex);
        }
    }

    //protected void findLocations(String term) {
    //protected void findLocations(int PageSize, int PageIndex, String term) {
    protected void findLocations(int PageIndex, String term) {
        if (task4 == null || AsyncTask.Status.RUNNING.equals(task4.getStatus()) == false) {
            //..Modify By 	: Chua Kam Hoong
            //..Date 		: 20 May 2015
            //task4 = new FindLocationTask(getCurrentContext(), term);
            //task4 = new FindLocationTask(getCurrentContext(), PageSize, PageIndex, term);
            task4 = new FindLocationTask(getCurrentContext(), PageIndex, term);
            task4.exec();
        }
    }

    protected class FindLocationTask extends McProgressAsyncTask {

        //private int intPageSize;
        private int intPageIndex;
        private String term;
        private GetLocationsResponse res;

        //public FindLocationTask(Context context, int PageSize, int PageIndex, String term) {
        public FindLocationTask(Context context, int PageIndex, String term) {
            super(context);
            //intPageSize = PageSize;
            intPageIndex = PageIndex;
            this.term = term;
        }

        @Override
        public void doInBackground() {
            /*
            if (isCConnect()) {
				res = CommonRequestService.getInstance().findLocations(getCurrentContext(), term, null, null);
			} else {
				//..Modify By 	: Chua Kam Hoong
				//..Date 		: 20 May 2015
				//res = ECService.getInstance().findLocations(getCurrentContext(), term, null);
				res = ECService.getInstance().findLocations(getCurrentContext(), intPageSize, intPageSize, term, null);
			}
			*/
            //..Note the following method : GetLocationsRequest not exist...
            //res = CommonRequestService.getInstance().findLocations(getCurrentContext(), intPageIndex, term, null, null);

            res = ECService.getInstance().findLocations(getCurrentContext(), intPageIndex, term, null);
        }

        @Override
        public void onPostExecute() {
            if (res != null) {
                if (res.getLocationList().size() == 0) {
                    toast(R.string.no_location_found);
                    onEmptyLocations(term);
                } else if (res.getLocationList().size() == 1) {
                    onLocationSelected(res.getLocationList().get(0));
                } else {
                    final Intent si = new Intent(getCurrentContext(), SearchLocationActivity.class);
                    si.putExtra("searchTerm", term);
                    si.putExtra("ResultList", (ArrayList<Location>) res.getLocationList());
                    si.putExtra("sum", res.getLocationList().size());
                    startActivityForResult(si, RQ_SEARCH_LOCATIONS);
                }
            }
        }

    }

    protected void onLocationSelected(Location loc) {
        if (loc != null) {
            request.setLocationCode(loc.getLocationCode());
            where.setText(loc.getName());
        } else {
            where.setText("");
            request.setLocationCode(null);
        }
    }

    protected void onEmptyLocations(String term) {

    }

    protected void sumitInterDepartmentRequest() {

        if (task1 == null || AsyncTask.Status.RUNNING.equals(task1.getStatus()) == false) {
            task1 = new InterDepartmentRequestSender(getCurrentContext());
            task1.exec();
        }

    }

    protected class InterDepartmentRequestSender extends McProgressAsyncTask {

        private InterDepartmentRequestResponse res;

        public InterDepartmentRequestSender(Context context) {
            super(context);
        }

        @Override
        public void doInBackground() {
            boolean isAckKnow = true;
            int intPic = 0;
            int intSign = 0;
            String equipmentId = "";
            if (mEquipmentSelected != null) {
                equipmentId = mEquipmentSelected.getEquipmentID() + "";
            }
            if (!tbPicture.isChecked() && !tbSign.isChecked()) {
                isAckKnow = false;
            } else {
                intPic = tbPicture.isChecked() ? 1 : 0;
                intSign = tbSign.isChecked() ? 1 : 0;
            }
            res = ECService.getInstance().createInterDepartmentRequest(getCurrentContext(), request.getLocationCode(), request.getItems(),
                    request.getScheduledDate(), request.getGuestName(), isAckKnow, intPic, intSign, equipmentId);
            if (res != null && res.getReturnCode().equals("0")) {
                int count = request.getItems().size();
                ServiceRequestItem item = null;
                String jobNo = null;
                if (count >= 1) {
                    item = request.getItems().get(0);
                    jobNo = res.getErrorMessage1();
                    ECService.getInstance().postMediaLibraryContent(getCurrentContext(), item.getImageTag(), item.getCapturedImage(), 2, 1,
                            jobNo);
                    ECService.getInstance().postMediaLibraryContent(getCurrentContext(), item.getVoiceTag(), item.getRecordedVoice(), 2, 2,
                            jobNo);
                }
                if (count >= 2) {
                    item = request.getItems().get(1);
                    jobNo = res.getErrorMessage2();
                    ECService.getInstance().postMediaLibraryContent(getCurrentContext(), item.getImageTag(), item.getCapturedImage(), 2, 1,
                            jobNo);
                    ECService.getInstance().postMediaLibraryContent(getCurrentContext(), item.getVoiceTag(), item.getRecordedVoice(), 2, 2,
                            jobNo);
                }
                if (count >= 3) {
                    item = request.getItems().get(2);
                    jobNo = res.getErrorMessage3();
                    ECService.getInstance().postMediaLibraryContent(getCurrentContext(), item.getImageTag(), item.getCapturedImage(), 2, 1,
                            jobNo);
                    ECService.getInstance().postMediaLibraryContent(getCurrentContext(), item.getVoiceTag(), item.getRecordedVoice(), 2, 2,
                            jobNo);
                }
            }

        }

        @Override
        public void onPostExecute() {
            if (res != null) {
                if (isCConnect()) {
                    if (res.getResult() == 1) {
                        StringBuffer jobs = new StringBuffer();
                        for (String s : res.getJobNumList()) {
                            jobs.append(s + ",");
                        }
                        String msg = jobs.toString();
                        toast(getSuccessmessage(msg.substring(0, msg.length() - 1)));
                        setResult(Activity.RESULT_OK);
                        finish();
                    } else {
                        toast(R.string.request_create_error);
                    }
                } else {
                    if (res.getReturnCode().equals("0")) {
                        StringBuffer msg = new StringBuffer();
                        StringBuffer errMsg = new StringBuffer();

                        if (!res.getErrorMessage3().equals("") && res.getReturnCode3().equals("0")) {
                            msg.append(res.getErrorMessage3());
                            request.removeItemAtIndex(2);
                        } else if (request.getItems().size() >= 3 && !res.getErrorMessage3().equals("") && !res.getReturnCode3().equals("0")) {
                            ServiceRequestItem item = request.getItems().get(1);
                            errMsg.append(res.getErrorMessage3() + " - " + item.getName());
                        }
                        if (!res.getErrorMessage2().equals("") && res.getReturnCode2().equals("0")) {
                            msg.append(msg.toString().length() > 0 ? ", " + res.getErrorMessage2() : "" + res.getErrorMessage2());
                            request.removeItemAtIndex(1);
                        } else if (request.getItems().size() >= 2 && !res.getErrorMessage2().equals("") && !res.getReturnCode2().equals("0")) {
                            ServiceRequestItem item = request.getItems().get(1);
                            errMsg.append(errMsg.toString().length() > 0 ? " \n" : "" + res.getErrorMessage2() + " - " + item.getName());
                        }
                        if (!res.getErrorMessage1().equals("") && res.getReturnCode1().equals("0")) {
                            msg.append(msg.toString().length() > 0 ? ", " + res.getErrorMessage1() : "" + res.getErrorMessage1());
                            request.removeItemAtIndex(0);
                        } else if (request.getItems().size() >= 1 && !res.getErrorMessage1().equals("") && !res.getReturnCode1().equals("0")) {
                            ServiceRequestItem item = request.getItems().get(0);
                            errMsg.append(errMsg.toString().length() > 0 ? " \n" : "" + res.getErrorMessage1() + " - " + item.getName());
                        }
                        int count = request.getItems().size();
                        if (count == 0) {
                            toast(getSuccessmessage(msg.toString()));
                            setResult(Activity.RESULT_OK);
                            finish();
                        } else {
                            toast(getSuccessmessage(msg.toString(), errMsg.toString()));
                            serviceItemAdapter.setItemList(request.getItems());
                            serviceItemAdapter.notifyDataSetChanged();
                        }
                    } else {
                        toast(R.string.request_create_error);
                    }
                }
            }
        }

    }

    @Override
    public boolean isLoginActivity() {
        return true;
    }

    @Override
    public boolean isHomeActivity() {
        return false;
    }

    private class GetMediaLibraryConfig extends McBackgroundAsyncTask {

        public GetMediaLibraryConfig(Context context) {
            super(context);
        }

        @Override
        public void doInBackground() {
            MediaLibraryConfig.config = ECService.getInstance().getMediaLibraryConfig(getCurrentContext());
        }

        @Override
        public void onPostExecute() {
            if (MediaLibraryConfig.config != null) {
                if (MediaLibraryConfig.config.isEnableCaptureImage()) {
                    ivCam.setVisibility(View.VISIBLE);

                } else {
                    ivCam.setVisibility(View.INVISIBLE);
                }
                if (MediaLibraryConfig.config.isEnableVoiceImage()) {
                    ivMic.setVisibility(View.VISIBLE);
                } else {
                    ivMic.setVisibility(View.INVISIBLE);
                }
            }
        }
    }

    public static class ItemRoundAdapter extends RoundCornerListAdapter<ServiceRequestItem> {

        private Activity activity;

        public ItemRoundAdapter(Activity activity) {
            super(activity, new ArrayList<ServiceRequestItem>(), R.layout.common_service_item);
            this.activity = activity;
        }

        @Override
        public void onBindView(View view, final Context context, int position, final ServiceRequestItem item) {

            final Tag tag = (Tag) view.getTag();
            final ServiceRequestItem model = (ServiceRequestItem) getItem(position);

            String status = String.format("%s X %s", model.getName(), model.getQuantity());
            tag.name.setText(status);

            tag.delete.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    ServiceRequest.getInstance().getItems().remove(item);
                    mList = ServiceRequest.getInstance().getItems();
                    notifyDataSetChanged();
                }
            });

            view.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(activity, NewRequestItemDetailActivity.class);
                    i.putExtra("index", ServiceRequest.getInstance().getItems().indexOf(model));
                    activity.startActivity(i);
                }
            });

        }

        @Override
        public View newView(Context context, int position, ViewGroup parent) {
            View v = super.newView(context, position, parent);
            Tag tag = new Tag();

            tag.name = (TextView) v.findViewById(R.id.name);
            tag.delete = (ImageView) v.findViewById(R.id.delete);

            v.setTag(tag);
            return v;
        }

        class Tag {
            TextView name;
            ImageView delete;
        }

    }

    protected class GuestQRCodeTask extends McProgressAsyncTask {

        private int intPageIndex;
        private String room;
        private String serviceItemCode;
        private List<ServiceItemViewModel> list;
        private GuestInfoResponse res;
        private int qty;
        private String auto;

        public GuestQRCodeTask(Context context, String room, String serviceItemCode, String qty, String auto) {
            super(context);
            this.room = room;
            this.serviceItemCode = serviceItemCode;
            this.qty = Integer.valueOf(qty);
            this.auto = auto;
        }

        public GuestQRCodeTask(Context context, int PageIndex, String room, String serviceItemCode, String qty, String auto) {
            super(context);
            intPageIndex = PageIndex;
            this.room = room;
            this.serviceItemCode = serviceItemCode;
            this.qty = Integer.valueOf(qty);
            this.auto = auto;
        }

        @Override
        public void doInBackground() {
            //res = ECService.getInstance().getRoomList(getCurrentContext(), SessionContext.getInstance().getFcsUserId(), room, room);
            res = ECService.getInstance().getGuestListing(getCurrentContext(), intPageIndex, room, "", "", "", "");
            //..Modify By 	: Chua Kam Hoong
            //..Date 		: 20 May 2015
            //list = ECService.getInstance().searchServiceItem(getCurrentContext(), serviceItemCode, null);
            //list = ECService.getInstance().searchServiceItem(getCurrentContext(), 0, 0, serviceItemCode, null);
            //list = ECService.getInstance().searchServiceItem(getCurrentContext(), vListItems, 0, "0", serviceItemCode, null);
            //list = ECService.getInstance().searchServiceItem(getCurrentContext(), vListItems, "1", serviceItemCode, null);
            list = ECService.getInstance().searchServiceItem(getCurrentContext(), vListItems, intPageIndex, serviceItemCode, null);
        }

        @Override
        public void onPostExecute() {

            if (res == null || res.getGuestList() == null || res.getGuestList().size() == 0) {
                toast(R.string.please_input_correctnum);
                return;
            }

            if (list == null || list.size() == 0) {
                toast(R.string.no_item_found);
                return;
            }

            GuestInfo guest = res.getGuestList().get(0);
            onGuestSelected(0, guest);

//			ServiceItemViewModel item = null;
//			for (ServiceItemViewModel model : list) {
//				if (model.getCode() != null && model.getCode().equals(serviceItemCode)) {
//					item = model;
//					break;
//				}
//			}
//			if (item == null) {
//				toast(R.string.no_item_found);
//				return;
//			}

//			onServiceItemSelected(item);
//			if (qty > 0 && qty < 100) {
//				quantity.setSelection(qty - 1);
//			}
//			if ("1".equals(auto)) {
//				onAddButtonClick();
//			}
        }

    }

    protected class NonGuestQRCodeTask extends McProgressAsyncTask {

        private int intPageIndex = 0;
        private String room;
        private String serviceItemCode;
        private List<ServiceItemViewModel> list;
        private String auto;
        private GetLocationsResponse res;
        private int qty;

        public NonGuestQRCodeTask(Context context, String room, String serviceItemCode, String qty, String auto) {
            super(context);
            this.room = room;
            this.serviceItemCode = serviceItemCode;
            this.auto = auto;
            this.qty = Integer.valueOf(qty);
        }

        public NonGuestQRCodeTask(Context context, int PageIndex, String room, String serviceItemCode, String qty, String auto) {
            super(context);
            intPageIndex = PageIndex;
            this.room = room;
            this.serviceItemCode = serviceItemCode;
            this.auto = auto;
            this.qty = Integer.valueOf(qty);
        }

        @Override
        public void doInBackground() {
            //res = ECService.getInstance().findLocations(getCurrentContext(), room, null);
            //list = ECService.getInstance().searchServiceItem(getCurrentContext(), serviceItemCode, null);
            //..Created By 	: Chua Kam Hoong
            //..Date 		: 20 May 2015
            //res = ECService.getInstance().findLocations(getCurrentContext(), 0, 0, room, null);
            res = ECService.getInstance().findLocations(getCurrentContext(), 1, room, null);
            //list = ECService.getInstance().searchServiceItem(getCurrentContext(), 0, 0, serviceItemCode, null);
            //list = ECService.getInstance().searchServiceItem(getCurrentContext(), vListItems, 0, "1", serviceItemCode, null);
            //list = ECService.getInstance().searchServiceItem(getCurrentContext(), vListItems, "1", serviceItemCode, null);
            list = ECService.getInstance().searchServiceItem(getCurrentContext(), vListItems, intPageIndex, serviceItemCode, null);
        }

        @Override
        public void onPostExecute() {

            if (res == null || res.getLocationList() == null || res.getLocationList().size() == 0) {
                toast(R.string.no_location_found);
                return;
            }

            if (list == null || list.size() == 0) {
                toast(R.string.no_item_found);
                return;
            }

            Location location = null;
            for (Location model : res.getLocationList()) {
                if (model.getLocationCode() != null && model.getLocationCode().equals(room)) {
                    location = model;
                    break;
                }
            }
            if (location == null) {
                toast(R.string.no_location_found);
                return;
            }
            onLocationSelected(location);

//			ServiceItemViewModel item = null;
//			for (ServiceItemViewModel model : list) {
//				if (model.getCode() != null && model.getCode().equals(serviceItemCode)) {
//					item = model;
//					break;
//				}
//			}
//			if (item == null) {
//				toast(R.string.no_item_found);
//				return;
//			}
//
//			onServiceItemSelected(item);
//			if (qty > 0 && qty < 100) {
//				quantity.setSelection(qty - 1);
//			}
//
//			if ("1".equals(auto)) {
//				onAddButtonClick();
//			}
        }

    }


}
