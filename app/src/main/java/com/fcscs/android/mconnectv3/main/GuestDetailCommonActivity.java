package com.fcscs.android.mconnectv3.main;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.BaseActivity;
import com.fcscs.android.mconnectv3.common.McConstants;
import com.fcscs.android.mconnectv3.common.ui.HomeTopBar;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.manager.model.GuestInfo;
import com.fcscs.android.mconnectv3.ws.cconnect.model.guestrequestservice.Guest;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.ProfileNote;

public class GuestDetailCommonActivity extends BaseActivity {

    protected EditText searchField;
    private TextView vip;
    private TextView language;
    private TextView guestName;
    private TextView arrivalDate;
    private TextView departureDate;
    private TextView tvETA;
    private TextView tvETD;
    private TextView roomNum;
    private TextView dndText;
    private TextView textMsg;
    private TextView voiceMsg;
    private TextView pref;
    private TextView notes;
    private LinearLayout notelayout;

    private HomeTopBar homeTopBar;
    private TextView wakeupCall;
    private TextView membership;
    private TextView tvHistory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.guest_details);

        homeTopBar = (HomeTopBar) findViewById(R.id.top_bar);
        homeTopBar.getTitleTv().setText(R.string.guestdetails);
        homeTopBar.getLeftBtn().setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });
        searchField = (EditText) findViewById(R.id.searchet);
        vip = (TextView) findViewById(R.id.viptv);
        roomNum = (TextView) findViewById(R.id.roomnumtv);
        language = (TextView) findViewById(R.id.languagetv);
        guestName = (TextView) findViewById(R.id.guestnametv);
        arrivalDate = (TextView) findViewById(R.id.checkindatetv);
        departureDate = (TextView) findViewById(R.id.checkoutdatetv);
        tvETA = (TextView) findViewById(R.id.etatv);
        tvETD = (TextView) findViewById(R.id.etdtv);

        dndText = (TextView) findViewById(R.id.dndtv);
        wakeupCall = (TextView) findViewById(R.id.calltv);
        textMsg = (TextView) findViewById(R.id.textmsgtv);
        voiceMsg = (TextView) findViewById(R.id.voicetv);
        membership = (TextView) findViewById(R.id.membershiptv);
        pref = (TextView) findViewById(R.id.preftv);
        notes = (TextView) findViewById(R.id.notestv);
        notelayout = (LinearLayout) findViewById(R.id.guest_detail_note_ll);
        tvHistory = (TextView) findViewById(R.id.btn_history);

        tvHistory.setEnabled(false);

    }


    protected void populatityUserInfo(final GuestInfo guestinfo) {
        if (guestinfo == null) {
            vip.setText("");
            roomNum.setText("");
            language.setText("");
            guestName.setText("");
            arrivalDate.setText("");
            departureDate.setText("");
            tvETA.setText("");
            tvETD.setText("");
            dndText.setText("");
            textMsg.setText("");
            voiceMsg.setText("");
            membership.setText("");
            wakeupCall.setText("");
            pref.setText("");
            notes.setText("");
            tvHistory.setEnabled(false);

        } else {
            searchField.setText("");
            roomNum.setText(guestinfo.getRoomNum());
            guestName.setText(guestinfo.getGuestUsername());
            if (isCConnect()) {
                language.setText("English");
                if (guestinfo.getIsvip()) {
                    vip.setText(R.string.yes);
                } else {
                    vip.setText(R.string.no);
                }
            } else {
                language.setText(guestinfo.getLanguage());
                vip.setText(guestinfo.getVipMsg());
            }
            arrivalDate.setText(guestinfo.getCheckinDate() == null ? "" : guestinfo.getCheckinDate());
            departureDate.setText(guestinfo.getCheckoutDate() == null ? "" : guestinfo.getCheckoutDate());
            tvETA.setText(guestinfo.getETADate() == null ? "" : guestinfo.getETADate());
            tvETD.setText(guestinfo.getETDDate() == null ? "" : guestinfo.getETDDate());
            dndText.setText(guestinfo.getDnd());
            textMsg.setText(guestinfo.getTextMsg());
            voiceMsg.setText(guestinfo.getVoiceMsg());
            membership.setText(guestinfo.getMembershipType());
            wakeupCall.setText(guestinfo.getAwu());
            pref.setText(guestinfo.getPreference());

            if (guestinfo.getNoteListing().size() > 0) {
                notes.setText("(" + guestinfo.getNoteListing().size() + ")");
                notelayout.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        goToNotes(guestinfo.getNoteListing());
                    }
                });
            } else {
                notes.setText("");
                notelayout.setOnClickListener(null);
            }
            tvHistory.setEnabled(true);
            tvHistory.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getCurrentContext(), GuestHistoryActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra(McConstants.KEY_GUEST_INFO, guestinfo);
                    startActivity(intent);
                }
            });
        }
    }

    protected void populatityGuestInfo(Guest guest) {

        roomNum.setText(guest.getRoomNum());
        guestName.setText(guest.getGuestName());
        if (isCConnect()) {
            arrivalDate.setText(guest.getCheckin());
            vip.setText(guest.isVip() ? R.string.yes : R.string.no);
            language.setText("English");
        } else {
            vip.setText(guest.getVipMsg());
            language.setText(guest.getLanguage());

            arrivalDate.setText(guest.getCheckinDate() == null ? "" : McUtils.formatDate(guest.getCheckinDate()));
            departureDate.setText(guest.getCheckoutDate() == null ? "" : McUtils.formatDate(guest.getCheckoutDate()));
            tvETA.setText(guest.getETADate() == null ? "" : McUtils.formatDate(guest.getETADate()));
            tvETD.setText(guest.getETDDate() == null ? "" : McUtils.formatDate(guest.getETDDate()));
        }
        dndText.setText(guest.getDnd());
        textMsg.setText(guest.getTextMsg());
        voiceMsg.setText(guest.getVoiceMsg());
        pref.setText("");
        notes.setText(guest.getComments());
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            setResult(RESULT_OK);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    protected void toast(String info) {
        Toast.makeText(this, info, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean isLoginActivity() {
        return true;
    }

    @Override
    public boolean isHomeActivity() {
        return false;
    }


    private void goToNotes(List<ProfileNote> list) {
        Intent i = NotesActivity.createIntent(getCurrentContext(), (ArrayList<ProfileNote>) list);
        startActivity(i);
    }
}
