package com.fcscs.android.mconnectv3.ws.cconnect;

import org.ksoap2.serialization.SoapObject;

import android.content.Context;

import com.fcscs.android.mconnectv3.common.SessionContext;
import com.fcscs.android.mconnectv3.http.WebServiceBase;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.SearchServiceItemResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.TopServiceItemResponse;

public class ServiceItemService extends WebServiceBase {

    private static ServiceItemService instance;
    private static ServiceItemService instanceNonUI;

    private ServiceItemService() {
    }

    public static ServiceItemService getInstance() {
        if (instance == null) {
            instance = new ServiceItemService();
        }
        instance.setEnableUI(true);
        return instance;
    }

    public static ServiceItemService getInstanceNonUI() {
        if (instanceNonUI == null) {
            instanceNonUI = new ServiceItemService();
        }
        instanceNonUI.setEnableUI(false);
        return instanceNonUI;
    }

    private static final String WEBSERVICE_URL_SUFFIX = "ws/serviceItemService";
    private static final String NAMESPACE = "http://fcscs.com/ws/schemas/serviceitemservice";

    private static final String SEARCH_ITEM_REQ_NAME = "SearchServiceItemRequest";
    private static final String SEARCH_ITEM_RESP_NAME = "SearchServiceItemResponse";
    private static final String TOP_ITEM_REQ_NAME = "TopServiceItemRequest";
    private static final String TOP_ITEM_RESP_NAME = "TopServiceItemResponse";

    public TopServiceItemResponse getTopServiceItems(Context ctx, int numResults) {

        SoapObject request = new SoapObject(NAMESPACE, TOP_ITEM_REQ_NAME);
        request.addAttribute("fcsUserId", SessionContext.getInstance().getFcsUserId());
        request.addAttribute("wsSessionId", SessionContext.getInstance().getWsSessionId());
        request.addAttribute("numResults", numResults);

        SoapObject soap = callWebService(ctx, request, WEBSERVICE_URL_SUFFIX, NAMESPACE, TOP_ITEM_RESP_NAME);

        if (soap != null) {
            return new TopServiceItemResponse(soap);
        } else {
            return null;
        }

    }

    public SearchServiceItemResponse findServiceItems(Context ctx, String term, Long departmentId, int start, int numResults) {

        SoapObject request = new SoapObject(NAMESPACE, SEARCH_ITEM_REQ_NAME);
        request.addAttribute("fcsUserId", SessionContext.getInstance().getFcsUserId());
        request.addAttribute("wsSessionId", SessionContext.getInstance().getWsSessionId());
        request.addAttribute("start", start);
        request.addAttribute("numResults", numResults);
        if (departmentId != null) {
            request.addAttribute("departmentId", departmentId);
        }
        if (term != null) {
            request.addProperty("term", term.trim());
        }

        SoapObject soap = callWebService(ctx, request, WEBSERVICE_URL_SUFFIX, NAMESPACE, SEARCH_ITEM_RESP_NAME);

        if (soap != null) {
            return new SearchServiceItemResponse(soap);
        } else {
            return null;
        }

    }

}
