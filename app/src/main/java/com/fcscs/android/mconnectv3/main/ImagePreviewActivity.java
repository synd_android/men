package com.fcscs.android.mconnectv3.main;

import java.io.File;

import uk.co.senab.photoview.PhotoViewAttacher;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.ImageView;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.BaseActivity;
import com.fcscs.android.mconnectv3.common.ui.HomeTopBar;
import com.fcscs.android.mconnectv3.common.util.McUtils;

public class ImagePreviewActivity extends BaseActivity {

    private ImageView image;
    private PhotoViewAttacher mAttacher;
    private Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_preview);

        HomeTopBar headBar = (HomeTopBar) findViewById(R.id.top_bar);
        headBar.getTitleTv().setText(R.string.image_preview);

        image = (ImageView) findViewById(R.id.iv_preview);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey("file")) {
                File file = (File) getIntent().getSerializableExtra("file");
                if (file != null) {
                    bitmap = McUtils.decodeSampledBitmapFromResource(file, 1000, 1000);
                    image.setImageBitmap(bitmap);
                    mAttacher = new PhotoViewAttacher(image);
                } else {
                    finish();
                }
            } else if (extras.containsKey("isDemo")) {
                image.setImageResource(R.drawable.captureimagedemo);
                mAttacher = new PhotoViewAttacher(image);
            }
        }
    }

    @Override
    protected void unbindViews() {
        //do nothing
    }

    @Override
    public boolean isLoginActivity() {
        return false;
    }

    @Override
    public boolean isHomeActivity() {
        return true;
    }

}
