package com.fcscs.android.mconnectv3.main;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.fcscs.android.mconnectv3.McApplication;
import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.BaseActivity;
import com.fcscs.android.mconnectv3.common.McConstants;
import com.fcscs.android.mconnectv3.common.McEnums;
import com.fcscs.android.mconnectv3.common.util.DateTimeHelper;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.manager.model.MediaLibraryConfig;
import com.fcscs.android.mconnectv3.manager.model.MessageWrapper;
import com.fcscs.android.mconnectv3.manager.model.ServiceRequest;

/**
 * Created by FCS on 2014/5/16.
 */
public class VoiceTagActivy extends BaseActivity implements View.OnClickListener {

    private static final String TAG = VoiceTagActivy.class.getSimpleName();
    private static final int NOTIFICATION_ID = 200;
    private Spinner spinner;
    private VoiceGestureButton ivStart;
    private ImageView ivPlay;
    private TextView tvTip;
    private TextView tvTime;
    private MediaRecorder recorder;
    private String packageName;
    private File recordFile;
    private Timer counterTimer;
    private int counter = 0;
    private int playCounter = 0;
    private MediaPlayer mediaPlayer;
    private Timer playTimer;
    private TextView tvUse;
    private TextView tvCancel;
    private Runnable checkLimitRunnable;
    private PowerManager.WakeLock mWakeLock;
    private ArrayAdapter<MediaLibraryConfig.Tag> adapter;
    private MediaLibraryConfig.Tag selectedTag;
    private int statusOfClick = 1;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.voice_tag_layout);

        spinner = (Spinner) findViewById(R.id.spinner);
        adapter = new ArrayAdapter<MediaLibraryConfig.Tag>(this, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        MediaLibraryConfig.Tag m = new MediaLibraryConfig.Tag();
        m.setId(-1L);
        m.setDescription(getString(R.string.attachment_tag));
        adapter.add(m);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                MediaLibraryConfig.Tag item = adapter.getItem(pos);
                if (pos == 0) {
                    item = null;
                }
                VoiceTagActivy.this.onItemSelected(pos, item);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
        if (MediaLibraryConfig.config != null) {
            for (MediaLibraryConfig.Tag tag : MediaLibraryConfig.config.getVoiceTagList()) {
                adapter.add(tag);
            }
            adapter.notifyDataSetChanged();
        }

        ivStart = (VoiceGestureButton) findViewById(R.id.record_start);
        tvTip = (TextView) findViewById(R.id.record_tip);
        ivPlay = (ImageView) findViewById(R.id.record_play);
        tvTime = (TextView) findViewById(R.id.record_time);
        tvUse = (TextView) findViewById(R.id.btn_use);
        tvCancel = (TextView) findViewById(R.id.btn_cancel);

//        ivStart.setOnClickListener(this);
        ivStart.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                statusOfClick = event.getAction();
//                Log.d("Tuan-Nguyen", event.getAction() + "");
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    (new Handler()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (statusOfClick == MotionEvent.ACTION_DOWN || statusOfClick == MotionEvent.ACTION_MOVE) {
                                startRecording(recordFile);
                            }
                        }
                    }, 0);

                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    stopRecording();
                }
                return false;
            }
        });
        ivPlay.setOnClickListener(this);
        ivPlay.setClickable(false);
        tvUse.setOnClickListener(this);
        tvCancel.setOnClickListener(this);

        packageName = getApplicationContext().getPackageName();
        SimpleDateFormat sd = new SimpleDateFormat(DateTimeHelper.FORMAT_DATETIME_NON_S);
        String date = sd.format(DateTimeHelper.getDateByTimeZone(new Date()));
        String str = McApplication.DATA_DIRECTORY.getAbsolutePath().concat("/record/record_%s.mp3");
        recordFile = new File(String.format(str, date));
        if (recordFile.getParentFile().exists() == false) {
            recordFile.getParentFile().mkdirs();
        }

        File file = null;
        if (ServiceRequest.isAvailable()) {
            file = ServiceRequest.getInstance().getItem().getRecordedVoice();
        } else if (MessageWrapper.isAvailable()) {
            file = MessageWrapper.getInstance().getRecordedVoice();
        }
        if (file != null && file.exists()) {
            Log.d(TAG, String.format("record file=%s, size=%dk", file.getAbsolutePath(), ((int) file.length() / 1024)));
            McUtils.copy(file, recordFile);
            MediaPlayer mp = MediaPlayer.create(this, Uri.fromFile(recordFile));
            counter = mp.getDuration() / 1000;
            if (counter > 0) {
                tvTime.setText(McUtils.formatCounter(counter));
                ivPlay.setClickable(true);
                tvTip.setText(R.string.press_to_rerecord_message);
            }
        }
    }

    @Override
    protected void onDestroy() {
        if (mediaPlayer != null) {
            mediaPlayer.reset();
            mediaPlayer.release();
        }
        if (recorder != null) {
            recorder.reset();
            recorder.release();
        }
        super.onDestroy();
    }

    @Override
    public boolean isLoginActivity() {
        return true;
    }

    @Override
    public boolean isHomeActivity() {
        return false;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.record_start:
                if ("start".equals(ivStart.getTag())) {
                    stopRecording();
                } else {
                    startRecording(recordFile);
                }
                break;
            case R.id.record_play:
                if ("play".equals(ivPlay.getTag())) {
                    stopMediaPlayer();
                } else {
                    playMedia(recordFile);
                }
                break;
            case R.id.btn_use:
                Intent data = new Intent();
                data.putExtra("record", recordFile);
                if (ServiceRequest.isAvailable()) {
                    ServiceRequest.getInstance().getItem().setRecordedVoice(recordFile);
                } else if (MessageWrapper.isAvailable()) {
                    MessageWrapper.getInstance().setRecordedVoice(recordFile);
                }
                setResult(RESULT_OK, data);
                finish();
                break;
            case R.id.btn_cancel:
                recordFile.delete();
                setResult(RESULT_CANCELED);
                finish();
                break;
        }
    }

    private void playMedia(File file) {

        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setDataSource(file.getAbsolutePath());
            mediaPlayer.setOnErrorListener(new OnErrorListener() {

                @Override
                public boolean onError(MediaPlayer paramMediaPlayer, int paramInt1, int paramInt2) {
                    if (McConstants.ENABLE_DEBUG_MODE) {
                        McUtils.saveLogToSDCard("MediaPlayer Error Code: " + paramInt1 + "," + paramInt2);
                    }
                    return false;
                }
            });
            mediaPlayer.prepare();
            mediaPlayer.start();

            ivPlay.setTag("play");
            ivPlay.setImageResource(R.drawable.record_stop);
            ivStart.setClickable(false);

            playCounter = counter;
            playTimer = new Timer();
            playTimer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    mPlayHandler.obtainMessage(1).sendToTarget();
                }
            }, 0, 1000);


        } catch (Exception e) {
            e.printStackTrace();
            if (McConstants.ENABLE_DEBUG_MODE) {
                McUtils.saveSoapToSDCard(null, null, null, e);
            }
        }

    }

    private void stopMediaPlayer() {
        try {

            if (playTimer != null) {
                playTimer.cancel();
            }

            if (mediaPlayer != null) {
                mediaPlayer.stop();
                mediaPlayer.reset();
                mediaPlayer.release();
                mediaPlayer = null;
            }

        } catch (Exception e) {
            e.printStackTrace();
            if (McConstants.ENABLE_DEBUG_MODE) {
                McUtils.saveCrashReport(this, e);
            }
        }
        ivPlay.setTag("");
        ivPlay.setImageResource(R.drawable.record_play);
        ivStart.setClickable(true);

        int second = counter % 60;
        int minute = counter / 60;
        tvTime.setText(String.format("%02d:%02d", minute, second));

    }

    private void stopRecording() {
        if (recorder != null) {
            try {
                recorder.stop();
                recorder.reset();
                recorder.release();
                recorder = null;

                if (counterTimer != null) {
                    counterTimer.cancel();
                }
                counterTimer = null;

                //disable runnable to check limit
                mCheckLimitHandler.removeCallbacks(checkLimitRunnable);
                checkLimitRunnable = null;

                // disable screen on
                mWakeLock.release();
                mWakeLock = null;

                ivStart.setBackgroundResource(R.drawable.record_start);
                ivStart.setTag("");
                tvTip.setText(getString(R.string.press_to_rerecord_message));
                ivPlay.setClickable(true);

                NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                nm.cancel(NOTIFICATION_ID);

                Log.d(TAG, String.format("record file=%s, size=%dk", recordFile.getAbsolutePath(), ((int) recordFile.length() / 1024)));
            } catch (Exception ex) {
                ex.printStackTrace();
                if (McConstants.ENABLE_DEBUG_MODE) {
                    McUtils.saveCrashReport(this, ex);
                }
                Toast.makeText(this, getString(R.string.fail_to_stop_recording), Toast.LENGTH_LONG).show();
            }
        }
    }


    private void startRecording(File file) {
        ivStart.setClickable(false);
        try {
            if (recorder != null) {
                recorder.release();
            }
            if (counterTimer != null) {
                counterTimer.cancel();
            }
            recorder = new MediaRecorder();
            recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            recorder.setOutputFile(file.getAbsolutePath());
            recorder.prepare();
            recorder.start();

            tvTip.setText(getString(R.string.recording));

            ivStart.setBackgroundResource(R.drawable.record_stop);
            ivStart.setTag("start");
            ivPlay.setClickable(false);

            counter = 0;
            tvTime.setText("00:00");

            counterTimer = new Timer();
            counterTimer.scheduleAtFixedRate(new TimerTask() {
                public void run() {
                    mCounterTimerHandler.obtainMessage(1).sendToTarget();
                }
            }, 0, 1000);

            checkLimitRunnable = new Runnable() {
                @Override
                public void run() {
                    //exceed time limit (10 minutes)
                    stopRecording();
                }
            };

            mCheckLimitHandler.postDelayed(checkLimitRunnable, 3 * 60 * 1000);

            // keep screen on
            PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK, "record");
            mWakeLock.acquire();

            int drawableIcon = R.drawable.pro_icon;
            if (McConstants.COMPILATION_PRODUCT == McEnums.CompilationProduct.PERCIPIA) {
                drawableIcon = R.drawable.pro_icon_green_percipia;
            } else if (McConstants.COMPILATION_PRODUCT == McEnums.CompilationProduct.MENGINEERING) {
                drawableIcon = R.drawable.pro_icon_en;
            }

            Intent notificationIntent = new Intent(this, VoiceTagActivy.class);
            PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
//            Notification notification = new Notification();
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
            mBuilder.setContentIntent(contentIntent)
                    .setContentTitle(getString(R.string.new_messages))
                    .setContentText(getString(R.string.recording))
                    .setSmallIcon(drawableIcon);
//            notification.icon = R.drawable.pro_icon;
//            notification.setLatestEventInfo(getApplicationContext(), getString(R.string.new_messages), getString(R.string.recording), contentIntent);
            NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            nm.notify(NOTIFICATION_ID, mBuilder.build());


        } catch (Exception e) {
            e.printStackTrace();
            if (McConstants.ENABLE_DEBUG_MODE) {
                McUtils.saveCrashReport(this, e);
            }
            Toast.makeText(this, getString(R.string.fail_to_start_recording), Toast.LENGTH_LONG).show();
        } finally {
            ivStart.setClickable(true);
        }
    }

    public Handler mCounterTimerHandler = new Handler() {
        public void handleMessage(Message msg) {
            tvTime.setText(McUtils.formatCounter(counter));
            counter++;
        }
    };

    public Handler mPlayHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (playCounter >= 0) {
                playCounter--;
                if(playCounter != -1){
                    tvTime.setText(String.format("-%s", McUtils.formatCounter(playCounter)));
                }
            } else if (playCounter == -1) {
                stopMediaPlayer();
                playCounter--;
            }
        }
    };

    public Handler mCheckLimitHandler = new Handler();

    private void onItemSelected(int pos, MediaLibraryConfig.Tag item) {
        selectedTag = item;

        if (ServiceRequest.isAvailable()) {
            ServiceRequest.getInstance().getItem().setVoiceTag(item);
        } else if (MessageWrapper.isAvailable()) {
            MessageWrapper.getInstance().setVoiceTag(item);
        }
    }

}