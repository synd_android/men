package com.fcscs.android.mconnectv3.engineering;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.BaseActivity;
import com.fcscs.android.mconnectv3.common.ui.HomeTopBar;
import com.fcscs.android.mconnectv3.common.ui.McProgressAsyncTask;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.github.gcacace.signaturepad.views.SignaturePad;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import uk.co.senab.photoview.PhotoViewAttacher;

public class SignatureActivity extends BaseActivity {

    //	private ImageView image;
//	private PhotoViewAttacher mAttacher;
    private Bitmap bitmap;
    SignaturePad signaturePad;
    Button btClear;
    Button btDone;
    public static final String BITMAP_IMAGE = "BITMAP_IMAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signature_layout);
        btClear = (Button) findViewById(R.id.sign_bt_clear);
        btDone = (Button) findViewById(R.id.sign_bt_done);

        HomeTopBar headBar = (HomeTopBar) findViewById(R.id.top_bar);
        headBar.getTitleTv().setText(R.string.e_signature);
        headBar.getRightBtn().setVisibility(View.GONE);

        signaturePad = (SignaturePad) findViewById(R.id.signature_pad);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey(BITMAP_IMAGE)) {
                File file = (File) getIntent().getSerializableExtra(BITMAP_IMAGE);
                if (file != null) {
                    bitmap = McUtils.decodeSampledBitmapFromResource(file, 1000, 1000);
                    signaturePad.setSignatureBitmap(bitmap);
                } else {
                    finish();
                }
            } else if (extras.containsKey("isDemo")) {
//				image.setImageResource(R.drawable.captureimagedemo);
//				mAttacher = new PhotoViewAttacher(image);
            }
        }
        btClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signaturePad.clear();
            }
        });
        btDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                SignatureActivity.this.finish();
                new getBitmap(SignatureActivity.this
                ).execute();
//                bitmap = signaturePad.getSignatureBitmap();
//				Intent intent = new Intent();
//				intent.putExtra(BITMAP_IMAGE, bitmap);
//				setResult(RESULT_OK, intent);
//				SignatureActivity.this.finish();
            }
        });
    }

    public boolean addJpgSignatureToGallery(Bitmap signature) {
        boolean result = false;
        try {
            File photo = new File(getAlbumStorageDir("SignaturePad"), String.format("Signature_%d.jpg", System.currentTimeMillis()));
            saveBitmapToJPG(signature, photo);
            scanMediaFile(photo);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public File getAlbumStorageDir(String albumName) {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), albumName);
        if (!file.mkdirs()) {
            Log.e("SignaturePad", "Directory not created");
        }
        return file;
    }

    public void saveBitmapToJPG(Bitmap bitmap, File photo) throws IOException {
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(newBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(bitmap, 0, 0, null);
        OutputStream stream = new FileOutputStream(photo);
        newBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
        stream.close();
    }

    private void scanMediaFile(File photo) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(photo);
        mediaScanIntent.setData(contentUri);
        SignatureActivity.this.sendBroadcast(mediaScanIntent);
    }

    private class getBitmap extends McProgressAsyncTask {
        byte[] byteArray;

        public getBitmap(Context context) {
            super(context);
        }

        @Override
        public void doInBackground() {
            bitmap = signaturePad.getSignatureBitmap();
            addJpgSignatureToGallery(bitmap);

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byteArray = stream.toByteArray();


        }

        @Override
        public void onPostExecute() {
//            bitmap=null;
            Intent intent = new Intent();
            if (byteArray != null) {
                intent.putExtra(BITMAP_IMAGE, byteArray);
            }
            setResult(RESULT_OK, intent);
            SignatureActivity.this.finish();
        }
    }

    @Override
    protected void unbindViews() {
        //do nothing
    }

    @Override
    public boolean isLoginActivity() {
        return false;
    }

    @Override
    public boolean isHomeActivity() {
        return true;
    }

}
