package com.fcscs.android.mconnectv3.main;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.BaseActivity;
import com.fcscs.android.mconnectv3.common.McConstants;
import com.fcscs.android.mconnectv3.common.QRCodeParserFactory;
import com.fcscs.android.mconnectv3.common.SessionContext;
import com.fcscs.android.mconnectv3.common.ui.HomeTopBar;
import com.fcscs.android.mconnectv3.common.ui.McProgressAsyncTask;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.common.util.PrefsUtil;
import com.fcscs.android.mconnectv3.ws.cconnect.GuestRequestService;
import com.fcscs.android.mconnectv3.ws.cconnect.model.guestrequestservice.FindRoomGuestResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.guestrequestservice.Guest;
import com.fcscs.android.mconnectv3.ws.econnect.ECService;
import com.fcscs.android.mconnectv3.zxing.CaptureActivity;

import java.util.ArrayList;
import java.util.List;

public class TotalMinbarActivity extends BaseActivity {
    private HomeTopBar homeTopBar;
    private EditText where;
    private EditText who;
    private EditText costTv;
    private TextView submit;
    private Guest currentGuest;
    protected MinibarPostLoader task;
    private FindGuestLoader task1;
    ImageView qrButton;

    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.total_minibar);
        homeTopBar = (HomeTopBar) findViewById(R.id.top_bar);
        homeTopBar.getTitleTv().setText(R.string.total_minibar);

        where = (EditText) findViewById(R.id.total_minibar_roomnum);
        where.setClickable(true);
        where.setHint(R.string.room_num);
        where.setInputType(InputType.TYPE_CLASS_NUMBER);
        where.setOnKeyListener(new OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                String term = where.getText().toString().trim();
                String name = who.getText().toString().trim();
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:
                            findGuestByRoomnum(term, name, false);
                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        });
        qrButton = (ImageView) findViewById(R.id.qr_camera);
        qrButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(TotalMinbarActivity.this, CaptureActivity.class);
                i.putExtra("type", QRCodeParserFactory.MINIBAR_REQUEST);
                startActivityForResult(i, QR_CODE);
            }
        });
        qrButton.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                boolean searchAll = where.getText().toString().length() == 0;
                findGuestByRoomnum(where.getText().toString(), "", searchAll);
                return true;
            }
        });

        boolean enable = PrefsUtil.getQRCodeMinibar(this);
        if (enable == false) {
            qrButton.setVisibility(View.GONE);
        }

        who = (EditText) findViewById(R.id.total_minibar_guestName);
        who.setHint(getString(R.string.guestname_isvip));
        who.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (currentGuest == null) {
                    toast(getString(R.string.please_select_guest));
                } else {
                    Intent i = new Intent(getCurrentContext(), GuestDetails.class);
                    i.putExtra("guest", currentGuest);
                    startActivity(i);
                }
            }
        });
        who.setEnabled(false);

        costTv = (EditText) findViewById(R.id.total_minibar_cost);

        submit = (TextView) findViewById(R.id.total_minibar_submit);
        submit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (currentGuest == null) {
                    where.requestFocus();
                    toast(R.string.please_choose_a_valid_guest);
                    return;
                }

                if (McUtils.isNullOrEmpty(costTv.getEditableText())) {
                    costTv.requestFocus();
                    toast(getString(R.string.please_input_cost));
                    return;
                }

                String cost = costTv.getEditableText().toString().trim();
                Double value = 0D;
                try {
                    value = Double.parseDouble(cost);
                    if (value < 0 || value > 100000) {
                        costTv.requestFocus();
                        toast(R.string.please_input_a_decimal_between_0_and_100_000);
                        return;
                    }
                } catch (Exception e) {
                    costTv.requestFocus();
                    toast(R.string.please_input_a_decimal_between_0_and_100_000);
                    return;
                }

                if (isEconnect()) {
                    if (task == null || AsyncTask.Status.RUNNING.equals(task.getStatus()) == false) {
                        task = new MinibarPostLoader(getCurrentContext(), currentGuest.getRoomNum(), "" + value);
                        task.exec();
                    }
                }
            }
        });
    }

    final static int QR_CODE = 1001;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == QR_CODE && resultCode == RESULT_OK) {
            Bundle bundle = data.getExtras();
            String result = bundle.getString("result");
            result = result.replace("minibar|LOC", "");
            String name = who.getText().toString().trim();
            findGuestByRoomnum(result, name, false);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void findGuestByRoomnum(String room, String guest, boolean searchAll) {

        if (!searchAll) {
            if (room == null || room.trim().length() < 1) {
                populateGuestInfo(null);
                return;
            }
        }

        if (task1 == null || AsyncTask.Status.RUNNING.equals(task1.getStatus()) == false) {
            task1 = new FindGuestLoader(getCurrentContext(), room);
            task1.exec();
        }

    }

    private void populateGuestInfo(Guest guest) {
        if (guest != null) {
            currentGuest = guest;
            if (isCConnect()) {
                where.setText(guest.getExtNo());
            } else {
                where.setText(guest.getRoomNum());
            }
            who.setText(guest.getGuestName() + "(" + guest.getVipMsg() + ")");
            costTv.requestFocus();
        } else {
            who.setText("");
            where.requestFocus();
            currentGuest = null;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            setResult(RESULT_OK);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    class FindGuestLoader extends McProgressAsyncTask {

        private String room;
        private FindRoomGuestResponse res;

        public FindGuestLoader(Context context, String room) {
            super(context);
            this.room = room;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            currentGuest = null;
            who.setText("");
        }

        @Override
        public void doInBackground() {

            if (isCConnect()) {
                res = GuestRequestService.getInstance().findRoomGuest(getCurrentContext(), room);
            } else {
//				res = ECService.getInstance().getRoomList(getCurrentContext(), room, true);
                if (McConstants.UNIQUE_GETROOMSLISTING) {
                    if (room != null && room.matches("[0-9]+")) {
                        res = ECService.getInstance().getRoomList(getCurrentContext(), SessionContext.getInstance().getFcsUserId(), room, "");
                    } else {
                        res = ECService.getInstance().getRoomList(getCurrentContext(), SessionContext.getInstance().getFcsUserId(), "", room);
                    }
                } else {
                    res = ECService.getInstance().getRoomList(getCurrentContext(), SessionContext.getInstance().getFcsUserId(), room, room); //According to Phua, both RoomNo and GuestName should pass RoomNo
                }
            }

        }

        @Override
        public void onPostExecute() {
            if (res != null) {
                final List<Guest> guests = res.getGuests();
                if (guests.size() > 1) {
                    List<String> list = new ArrayList<String>();
                    for (Guest guest : guests) {
                        String guestName = guest.getGuestName().trim();
                        if (guestName != null && !guestName.equalsIgnoreCase("")) {
                            list.add("(" + guest.getRoomNum() + ")\n" + guestName);
                        } else {
                            list.add("(" + guest.getRoomNum() + ")");
                        }
                    }
                    String[] items = new String[list.size()];
                    items = (String[]) list.toArray(items);

                    AlertDialog.Builder builder = new AlertDialog.Builder(getCurrentContext());
                    builder.setTitle(getString(R.string.label_select_guest));
                    builder.setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (which > -1 && which < guests.size()) {
                                populateGuestInfo(guests.get(which));
                                dialog.cancel();
                                hideSoftInput(where);
                            }
                        }
                    });
                    builder.show();
                } else if (guests.size() == 1) {
                    populateGuestInfo(guests.get(0));
                    hideSoftInput(where);
                } else if (guests.size() == 0) {
                    populateGuestInfo(null);
                    Toast.makeText(getCurrentContext(), R.string.please_input_correctnum, Toast.LENGTH_LONG).show();
                }
            }
        }

    }

    class MinibarPostLoader extends McProgressAsyncTask {

        private String roomNo;
        private String cost;
        private Boolean result;

        public MinibarPostLoader(Context context, String roomNo, String cost) {
            super(context);
            this.roomNo = roomNo;
            this.cost = cost;
        }

        @Override
        public void doInBackground() {
            result = false;
            if (isEconnect()) {
                result = ECService.getInstance().saveTotalPosting(getCurrentContext(), roomNo, cost);
            }
        }

        @Override
        public void onPostExecute() {
            if (result == true) {
                toast(getString(R.string.update_successfully));
                finish();
            } else {
                toast(getString(R.string.update_fail));
            }
        }

    }

    @Override
    public boolean isLoginActivity() {
        return true;
    }

    @Override
    public boolean isHomeActivity() {
        return false;
    }

}
