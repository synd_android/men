package com.fcscs.android.mconnectv3.main;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.BaseActivity;
import com.fcscs.android.mconnectv3.common.ui.HomeTopBar;
import com.fcscs.android.mconnectv3.common.ui.McBackgroundAsyncTask;
import com.fcscs.android.mconnectv3.common.ui.McProgressAsyncTask;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.manager.model.MediaLibraryConfig;
import com.fcscs.android.mconnectv3.manager.model.MessageWrapper;
import com.fcscs.android.mconnectv3.ws.cconnect.AdhocService;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.CreateIndividualMsgResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.GetTemplateListResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.Template;
import com.fcscs.android.mconnectv3.ws.econnect.ECService;

public class NewMessageBaseActivity extends BaseActivity {

    private static final int RQ_RECORD_VOICE = 1005;
    private static final int RQ_CAPTURE_IMAGE = 1006;

    private TemplateListLoader task;

    protected MessageWrapper message = null;
    private ImageView ivMic;
    private ImageView ivCam;

    protected HomeTopBar homeTopBar;
    protected EditText etMessage;
    protected Spinner spTemp;
    protected TextView btnAdd;
    protected TextView btnCancel;
    protected EditText etRunner;
    protected ListView runnerListView;
    protected ImageButton infoMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.common_message);

        message = MessageWrapper.getInstance();
        homeTopBar = (HomeTopBar) findViewById(R.id.top_bar);
        infoMsg = (ImageButton) findViewById(R.id.info_msg);


        EditText etCreate = (EditText) findViewById(R.id.et_createdby);
        etCreate.setText(getSessionCtx().getName());
        etCreate.setEnabled(false);

        etMessage = (EditText) findViewById(R.id.et_message);
        etMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                message.setContent(s.toString().trim());
            }
        });

        spTemp = (Spinner) findViewById(R.id.sp_templates);
        initTemplateSpiner(spTemp);

        etRunner = (EditText) findViewById(R.id.et_recipient);
        runnerListView = (ListView) findViewById(R.id.lv_recipients);

        btnAdd = (TextView) findViewById(R.id.btn_send_msg);
        btnAdd.setClickable(true);

        btnCancel = (TextView) findViewById(R.id.btn_cancel_msg);

        ivMic = (ImageView) findViewById(R.id.btn_mic);
        ivMic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(NewMessageBaseActivity.this, VoiceTagActivy.class);
                NewMessageBaseActivity.this.startActivityForResult(i, RQ_RECORD_VOICE);
            }
        });
        ivMic.setVisibility(View.INVISIBLE);

        ivCam = (ImageView) findViewById(R.id.camera);
        ivCam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(NewMessageBaseActivity.this, ImageCaptureActivity.class);
                NewMessageBaseActivity.this.startActivityForResult(i, RQ_CAPTURE_IMAGE);
            }
        });
        ivCam.setVisibility(View.INVISIBLE);

        infoMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toast(R.string.info_message);
            }
        });

        new GetMediaLibraryConfig(this).exec();

    }

    @Override
    protected void onDestroy() {
        MessageWrapper.destroyInstance();
        super.onDestroy();
    }

    public boolean validateMessage() {
        String msg = etMessage.getEditableText().toString().trim();
        if (McUtils.getMessageCount(msg) == 0) {
            toast(getString(R.string.message_is_empty));
            return false;
        } else {
            return true;
        }
    }

    protected void initTemplateSpiner(Spinner spTemplate) {
        if (task == null || AsyncTask.Status.RUNNING.equals(task.getStatus()) == false) {
            task = new TemplateListLoader(getCurrentContext(), spTemplate);
            task.exec();
        }
    }

    private class GetMediaLibraryConfig extends McBackgroundAsyncTask {

        public GetMediaLibraryConfig(Context context) {
            super(context);
        }

        @Override
        public void doInBackground() {
            MediaLibraryConfig.config = ECService.getInstance().getMediaLibraryConfig(getCurrentContext());
        }

        @Override
        public void onPostExecute() {
            if (MediaLibraryConfig.config != null) {
                if (MediaLibraryConfig.config.isEnableCaptureImage()) {
                    ivCam.setVisibility(View.VISIBLE);
                } else {
                    ivCam.setVisibility(View.INVISIBLE);
                }
                if (MediaLibraryConfig.config.isEnableVoiceImage()) {
                    ivMic.setVisibility(View.VISIBLE);
                } else {
                    ivMic.setVisibility(View.INVISIBLE);
                }
            }
        }
    }

    class TemplateListLoader extends McBackgroundAsyncTask {

        public TemplateListLoader(Context context, Spinner spTemplate) {
            super(context);
            this.spTemplate = spTemplate;
        }

        protected GetTemplateListResponse res;
        protected ArrayAdapter<Template> adapter;
        protected Spinner spTemplate;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            adapter = new ArrayAdapter<Template>(getCurrentContext(), android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            Template t = new Template();
            t.setTemplateName(getString(R.string.message_template));
            adapter.add(t);
            spTemplate.setAdapter(adapter);
        }

        @Override
        public void doInBackground() {
            if (isCConnect()) {
                res = AdhocService.getInstance().getMsgTemplates(getCurrentContext());
            } else {
                res = ECService.getInstance().getMessageTemplates(getCurrentContext());
            }
        }

        @Override
        public void onPostExecute() {
            if (res != null) {
                final List<Template> temps = res.getTemplates();
                if (temps.size() > 0) {
                    for (Template temp : temps) {
                        adapter.add(temp);
                    }
                    adapter.notifyDataSetChanged();
                    spTemplate.setOnItemSelectedListener(new TemplateListListener());
                }
            }
        }
    }

    protected final class TemplateListListener implements OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> arg0, View arg1, int pos, long arg3) {

            int index = pos - 1;
            Template temp = null;
            if (arg0.getAdapter() != null && index > -1 && index < arg0.getAdapter().getCount()) {
                temp = (Template) arg0.getAdapter().getItem(pos);
            }
            onTemplateSelected(arg0, arg1, pos, arg3, temp);

        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {

        }
    }

    protected void onTemplateSelected(AdapterView<?> parent, View view, int position, long id, Template temp) {
    }

    protected class IndividualMessageSender extends McProgressAsyncTask {

        private CreateIndividualMsgResponse res;

        public IndividualMessageSender(Context context) {
            super(context);
        }

        @Override
        public void doInBackground() {
            res = ECService.getInstance().sendAdHocMsg(getCurrentContext());
            if (res != null && res.getReturnCode().equals("0") && message != null) {
                String messageId = null;
                messageId = res.getAdHocMsgId();
                ECService.getInstance().postMediaLibraryContent(getCurrentContext(), message.getImageTag(),
                        message.getCapturedImage(), 1, 1, messageId);
                ECService.getInstance().postMediaLibraryContent(getCurrentContext(), message.getVoiceTag(),
                        message.getRecordedVoice(), 1, 2, messageId);
            }
        }

        @Override
        public void onPostExecute() {
            if (res != null && res.isSuccess()) {
                StringBuffer sb = new StringBuffer();
                sb.append(getString(R.string.message_created_confirm, res.getAdHocMsgId()));
                sb.append("\n");
                sb.append(McUtils.formatDateTime(res.getCreated()));
                toast(sb.toString());
                setResult(RESULT_OK);
                finish();
            } else {
                toast(getString(R.string.send_msg_fail));
            }
        }

    }

    protected class GroupMesageSender extends McProgressAsyncTask {

        private CreateIndividualMsgResponse response;

        public GroupMesageSender(Context context) {
            super(context);
        }

        @Override
        public void doInBackground() {
            response = ECService.getInstance().sendAdHocMsg(getCurrentContext());
            if (response != null && response.getReturnCode().equals("0") && message != null) {
                String messageId = null;
                messageId = response.getAdHocMsgId();
                ECService.getInstance().postMediaLibraryContent(getCurrentContext(), message.getImageTag(),
                        message.getCapturedImage(), 1, 1, messageId);
                ECService.getInstance().postMediaLibraryContent(getCurrentContext(), message.getVoiceTag(),
                        message.getRecordedVoice(), 1, 2, messageId);
            }
        }

        @Override
        public void onPostExecute() {
            if (response != null) {
                if (response.isSuccess()) {
                    StringBuffer sb = new StringBuffer();
                    sb.append(getString(R.string.message_created_confirm, response.getAdHocMsgId()));
                    sb.append("\n");
                    sb.append(McUtils.formatDateTime(response.getCreated()));

                    toast(sb.toString());
                    setResult(RESULT_OK);
                    finish();
                } else {
                    toast(getString(R.string.create_message_error));
                }
            }
        }

    }

    @Override
    public boolean isLoginActivity() {
        return true;
    }

    @Override
    public boolean isHomeActivity() {
        return false;
    }

}
