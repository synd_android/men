package com.fcscs.android.mconnectv3.ws.econnect.model.commonservice;

import com.fcscs.android.mconnectv3.common.util.SoapHelper;

import org.ksoap2.serialization.SoapObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GetEngineeringChecklistResponse implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2846090543654867439L;

    private int returnCode;
    private String errorMsg;
    private List<EngineeringChecklistDetails> list;


    public GetEngineeringChecklistResponse() {

    }

    public GetEngineeringChecklistResponse(SoapObject soap) {
        SoapObject status = (SoapObject) soap.getProperty("ResponseStatus");
        returnCode = SoapHelper.getIntegerProperty(status, "ReturnCode", 1);
        errorMsg = SoapHelper.getStringProperty(status, "ErrorMsg", "");

        list = new ArrayList<EngineeringChecklistDetails>();
        for (int i = 0; i < soap.getPropertyCount(); i++) {
            if ("EngineeringChecklistDetailsListing".equalsIgnoreCase(SoapHelper.getPropertyName(soap, i))) {
                SoapObject s = (SoapObject) soap.getProperty(i);
                for (int j = 0; j < s.getPropertyCount(); j++) {
                    if ("EngineeringChecklistDetails".equalsIgnoreCase(SoapHelper.getPropertyName(s, j))) {
                        SoapObject l = (SoapObject) s.getProperty(j);
                        int contentID = SoapHelper.getIntegerProperty(l, "ContentID", 0);
                        String content = SoapHelper.getStringProperty(l, "Content", "");
                        String input = SoapHelper.getStringProperty(l, "Input", "");
                        boolean Remarks = SoapHelper.getBooleanProperty(l, "Remarks", false);
                        String answerID = SoapHelper.getStringProperty(l, "AnswerID", "");
                        String inputAnswer = SoapHelper.getStringProperty(l, "InputAnswer", "");
                        String remarksAnswer = SoapHelper.getStringProperty(l, "RemarksAnswer", "");
                        String label = SoapHelper.getStringProperty(l, "Label", "");
                        String ChecklistName = SoapHelper.getStringProperty(l, "ChecklistName", "");
//						!"".equalsIgnoreCase(content) &&
                        if (contentID != 0) {
                            EngineeringChecklistDetails m = new EngineeringChecklistDetails();
                            m.setContentId(contentID);
                            m.setContent(content);
                            m.setInput(input);
                            m.setRemarks(Remarks);
                            m.setAnswerID(answerID);
                            m.setInputAnswer(inputAnswer);
                            m.setRemarksAnswer(remarksAnswer);
                            m.setLabel(label);
                            m.setChecklistName(ChecklistName);
                            list.add(m);
                        }
                    }
                }
            }
        }
    }

    public int getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(int returnCode) {
        this.returnCode = returnCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public List<EngineeringChecklistDetails> getList() {
        return list;
    }

    public void setList(List<EngineeringChecklistDetails> list) {
        this.list = list;
    }
}
