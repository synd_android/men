package com.fcscs.android.mconnectv3.manager.model;

import java.io.Serializable;
import java.util.Date;

public class EquipmentDetails implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private int EquipmentID;
    private String EquipmentNumber;
    private String EquipmentName;
    private String EquipmentLang;
    private String EquipmentGroup;
    private String EquipmentGroupCode;

    public EquipmentDetails() {
    }

    public String getEquipmentLang() {
        return EquipmentLang;
    }

    public void setEquipmentLang(String equipmentLang) {
        EquipmentLang = equipmentLang;
    }

    public String getEquipmentGroup() {
        return EquipmentGroup;
    }

    public void setEquipmentGroup(String equipmentGroup) {
        EquipmentGroup = equipmentGroup;
    }

    public String getEquipmentGroupCode() {
        return EquipmentGroupCode;
    }

    public void setEquipmentGroupCode(String equipmentGroupCode) {
        EquipmentGroupCode = equipmentGroupCode;
    }

    public EquipmentDetails(int equipmentID, String equipmentNumber, String equipmentName) {
        EquipmentID = equipmentID;
        EquipmentNumber = equipmentNumber;
        EquipmentName = equipmentName;
    }

    public int getEquipmentID() {
        return EquipmentID;
    }

    public void setEquipmentID(int equipmentID) {
        EquipmentID = equipmentID;
    }

    public String getEquipmentNumber() {
        return EquipmentNumber;
    }

    public void setEquipmentNumber(String equipmentNumber) {
        EquipmentNumber = equipmentNumber;
    }

    public String getEquipmentName() {
        return EquipmentName;
    }

    public void setEquipmentName(String equipmentName) {
        EquipmentName = equipmentName;
    }
}
