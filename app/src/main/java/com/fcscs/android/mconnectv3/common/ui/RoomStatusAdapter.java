package com.fcscs.android.mconnectv3.common.ui;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.main.RoomStatusActivity;
//import com.fcscs.android.mconnectv3.common.McEnums.RoomStatus;


public class RoomStatusAdapter extends BaseAdapter {

    private Context context;
    private final List<RoomStatusActivity.RoomStatus> tags;
    private LayoutInflater inflater;
    private RoomStatusActivity.RoomStatus selected;
    protected View previous;

    public RoomStatusAdapter(Context context, final List<RoomStatusActivity.RoomStatus> tags) {
        super();
        this.context = context;
        this.tags = tags;
        this.inflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return tags.size();
    }

    @Override
    public Object getItem(int position) {
        return tags.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final RoomStatusActivity.RoomStatus tag = (RoomStatusActivity.RoomStatus) getItem(position);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.room_status_item, parent, false);
            TextView text = (TextView) convertView.findViewById(R.id.tv_item_text);
            text.setText(tag.name);
        }
        final View view = convertView;

        if (getSelected() != null && getSelected().equals(tag)) {
            convertView.setBackgroundResource(R.drawable.dark_gray);
        } else {
            convertView.setBackgroundResource(R.drawable.light_white);
        }

        convertView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                selected = tag;
                if (previous != null) {
                    previous.setBackgroundResource(R.drawable.light_white);
                }
                view.setBackgroundResource(R.drawable.dark_gray);
                previous = view;
            }
        });

        return convertView;
    }

    public RoomStatusActivity.RoomStatus getSelected() {
        return selected;
    }

}
