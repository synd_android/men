package com.fcscs.android.mconnectv3.ws.econnect.model.commonservice;

import com.fcscs.android.mconnectv3.common.util.SoapHelper;
import com.fcscs.android.mconnectv3.manager.model.EquipmentDetails;
import com.fcscs.android.mconnectv3.manager.model.JobModel;

import org.ksoap2.serialization.SoapObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GetEquipmentsResponse implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 3462243046338355651L;

    private int returnCode;
    private String errorMsg;
    private List<EquipmentDetails> list;

    public GetEquipmentsResponse() {

    }

    public GetEquipmentsResponse(SoapObject soap) {
        SoapObject status = (SoapObject) soap.getProperty("ResponseStatus");
        returnCode = SoapHelper.getIntegerProperty(status, "ReturnCode", 1);
        errorMsg = SoapHelper.getStringProperty(status, "ErrorMsg", "");

        list = new ArrayList<EquipmentDetails>();
        for (int i = 0; i < soap.getPropertyCount(); i++) {
            if ("EquipmentDetailsListing".equalsIgnoreCase(SoapHelper.getPropertyName(soap, i))) {
                SoapObject s = (SoapObject) soap.getProperty(i);
                for (int j = 0; j < s.getPropertyCount(); j++) {
                    if ("EquipmentDetails".equalsIgnoreCase(SoapHelper.getPropertyName(s, j))) {
                        SoapObject l = (SoapObject) s.getProperty(j);
                        int id = SoapHelper.getIntegerProperty(l, "EquipmentID", -1);
                        String numnber = SoapHelper.getStringProperty(l, "EquipmentNumber", "");
                        String name = SoapHelper.getStringProperty(l, "EquipmentName", "");
                        String equipmentLand = SoapHelper.getStringProperty(l, "EquipmentLang", "");
                        String equipmentGroup = SoapHelper.getStringProperty(l, "EquipmentGroup", "");
                        String equipmentGroupCode = SoapHelper.getStringProperty(l, "EquipmentGroupCode", "");

                        EquipmentDetails m = new EquipmentDetails(id, numnber, name);
                        m.setEquipmentLang(equipmentLand);
                        m.setEquipmentGroup(equipmentGroup);
                        m.setEquipmentGroupCode(equipmentGroupCode);
                        list.add(m);
                    }
                }
            }
        }
    }

    public int getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(int returnCode) {
        this.returnCode = returnCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public List<EquipmentDetails> getList() {
        return list;
    }

    public void setList(List<EquipmentDetails> list) {
        this.list = list;
    }

}
