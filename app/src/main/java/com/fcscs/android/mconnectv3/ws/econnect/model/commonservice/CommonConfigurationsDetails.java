package com.fcscs.android.mconnectv3.ws.econnect.model.commonservice;

public class CommonConfigurationsDetails {

    public static final String EC_DATE_FORMAT = "mCONNECT_DATE_TIME_FORMAT";
    public static final String EC_AUTO_ACK_MSG = "mCONNECT_ADHOC_MSG_AUTO_ACK";
    public static final String EC_AUTO_ACK_JOB = "mCONNECT_JOBQ_AUTO_ACK";
    public static final String EC_JOB_PULLING_DURATION = "mCONNECT_NOTIFICATION_PULL_JOBQ_DURATION";
    public static final String EC_MSG_PULLING_DURATION = "mCONNECT_NOTIFICATION_PULL_AHMSG_DURATION";

    private String module;
    private String value;

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
