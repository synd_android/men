package com.fcscs.android.mconnectv3.main;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.BaseActivity;
import com.fcscs.android.mconnectv3.common.ui.HomeTopBar;
import com.fcscs.android.mconnectv3.common.ui.McLongUIOperationAyncTask;
import com.fcscs.android.mconnectv3.common.ui.McLongUIOperationAyncTask.McUIUpdatable;
import com.fcscs.android.mconnectv3.common.ui.McProgressAsyncTask;
import com.fcscs.android.mconnectv3.common.ui.RoundCornerListAdapter;
import com.fcscs.android.mconnectv3.ws.cconnect.AdhocService;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.FindRunnerGroupResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.Group;
import com.fcscs.android.mconnectv3.ws.econnect.ECService;

public class SearchRunnerGroupActivity extends BaseActivity {

    public static final String KEY_GROUP_LIST = "group";
    public static final String KEY_TERM = "term";
    public static final String KEY_RESULT = "KEY_RESULT";

    private HomeTopBar homeTopBar;
    private EditText etTerm;

    private List<Group> groups = null;
    private GroupListAdapter adapter;
    private ListView lvMain;
    private String term;
    protected GroupLoader task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_runner_group);

        Bundle bundle = getIntent().getExtras();
        final FindRunnerGroupResponse response = (FindRunnerGroupResponse) bundle.getSerializable(KEY_GROUP_LIST);
        term = bundle.getString(KEY_TERM);

        groups = new ArrayList<Group>();
        adapter = new GroupListAdapter(this, groups, R.layout.group_search_list_item);
        lvMain = (ListView) findViewById(R.id.lv_group);
        lvMain.setAdapter(adapter);
        lvMain.setEmptyView(findViewById(R.id.tvrs_empty));
        new McLongUIOperationAyncTask(new McUIUpdatable() {

            @Override
            public void update() {
                addGroups(response);
            }
        }).exec();


        homeTopBar = (HomeTopBar) findViewById(R.id.top_bar);
        homeTopBar.getTitleTv().setText(R.string.group_search);

        etTerm = (EditText) findViewById(R.id.et_term);
        etTerm.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:

                            String term = etTerm.getText().toString();
                            if (task == null || AsyncTask.Status.RUNNING.equals(task.getStatus()) == false) {
                                task = new GroupLoader(getCurrentContext(), term);
                                task.exec();
                            }
                            return true;

                        default:
                            break;
                    }
                }
                return false;
            }
        });

        if (term != null) {
            etTerm.setText(term);
        }

    }

    public void addGroups(FindRunnerGroupResponse response) {
        groups.clear();
        if (response != null) {
            if (response.getGroupList().size() != 0) {
                for (final Group r : response.getGroupList()) {
                    groups.add(r);
                }
            }
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            setResult(RESULT_CANCELED);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    class GroupLoader extends McProgressAsyncTask {

        public GroupLoader(Context context, String term) {
            super(context);
            this.term = term;
        }

        private String term;
        private FindRunnerGroupResponse response;


        @Override
        public void doInBackground() {
            if (isCConnect()) {
                response = AdhocService.getInstance().findRunnerGroups(SearchRunnerGroupActivity.this, term);
            } else {
                response = ECService.getInstance().getAdHocGroupList(SearchRunnerGroupActivity.this, term);
            }
        }

        @Override
        public void onPostExecute() {
            addGroups(response);
        }

    }

    class GroupListAdapter extends RoundCornerListAdapter<Group> {

        public GroupListAdapter(Context context, List<Group> list, int layout) {
            super(context, list, layout);
        }

        @Override
        public void onBindView(View v, Context context, int position, final Group item) {

            TextView tv = (TextView) v.findViewById(R.id.name);
            tv.setText(item.getGroupName());

            v.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent i = new Intent();
                    i.putExtra(KEY_RESULT, item);
                    setResult(RESULT_OK, i);
                    finish();
                }
            });

        }

    }

    @Override
    public boolean isLoginActivity() {
        return true;
    }

    @Override
    public boolean isHomeActivity() {
        return false;
    }

}
