package com.fcscs.android.mconnectv3.manager.model;

import java.io.Serializable;

import com.fcscs.android.mconnectv3.ws.cconnect.model.guestrequestservice.FindRunnerResponse.Runner;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.ServiceItemViewModel;

public class ServiceItemModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 29412794977356826L;

    private ServiceItemViewModel serviceItem;
    private int qty;
    private Runner runner;
    private long locationId;
    private String remarks;


    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public long getLocationId() {
        return locationId;
    }

    public void setLocationId(long locationId) {
        this.locationId = locationId;
    }

    public ServiceItemModel() {
    }

    public ServiceItemModel(ServiceItemViewModel serviceItem, int qty) {
        super();
        this.serviceItem = serviceItem;
        this.qty = qty;
    }

    public Runner getRunner() {
        return runner;
    }

    public void setRunner(Runner runner) {
        this.runner = runner;
    }

    public ServiceItemViewModel getServiceItem() {
        return serviceItem;
    }

    public void setServiceItem(ServiceItemViewModel serviceItem) {
        this.serviceItem = serviceItem;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

//	public Date getScheduledDate() {
//		return scheduledDate;
//	}
//
//	public void setScheduledDate(Date scheduledDate) {
//		this.scheduledDate = scheduledDate;
//	}

}
