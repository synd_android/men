package com.fcscs.android.mconnectv3.main;

import java.io.File;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.manager.model.MediaLibraryConfig;
import com.fcscs.android.mconnectv3.manager.model.MessageWrapper;
import com.fcscs.android.mconnectv3.manager.model.ServiceRequest;

/**
 * Created by FCS on 2014/5/23.
 */
public class ImageTagActivity extends Activity {

    private static final String TAG = ImageTagActivity.class.getSimpleName();
    private File file;
    private int width;
    private int height;
    private Spinner spinner;
    private ArrayAdapter<MediaLibraryConfig.Tag> adapter;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_tag_layout);

        ImageView image = (ImageView) findViewById(R.id.image);

        spinner = (Spinner) findViewById(R.id.spinner);
        adapter = new ArrayAdapter<MediaLibraryConfig.Tag>(this, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        MediaLibraryConfig.Tag m = new MediaLibraryConfig.Tag();
        m.setId(-1L);
        m.setDescription(getString(R.string.attachment_tag));
        adapter.add(m);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                MediaLibraryConfig.Tag item = adapter.getItem(pos);
                if (pos == 0) {
                    item = null;
                }
                ImageTagActivity.this.onItemSelected(pos, item);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
        if (MediaLibraryConfig.config != null) {
            for (MediaLibraryConfig.Tag tag : MediaLibraryConfig.config.getImageTagList()) {
                adapter.add(tag);
            }
            adapter.notifyDataSetChanged();
        }

        file = (File) getIntent().getSerializableExtra("image");
        width = (int) getIntent().getIntExtra("width", 800);
        height = (int) getIntent().getIntExtra("height", 800);
        if (file != null && file.exists()) {

            File[] files = file.getParentFile().listFiles();
            for (File f : files) {
                if (f.equals(file) == false) {
                    f.delete();
                }
            }

            Log.d(TAG, String.format("file path=%s size=%dk", file.getAbsolutePath(), file.length() / 1024));
            Bitmap bitmap = McUtils.decodeSampledBitmapFromResource(file, width, width);
            if (bitmap != null) {
                image.setImageBitmap(McUtils.rotateImage(bitmap,90));

                if (ServiceRequest.isAvailable()) {
                    ServiceRequest.getInstance().getItem().setCapturedImage(file);
                } else if (MessageWrapper.isAvailable()) {
                    MessageWrapper.getInstance().setCapturedImage(file);
                }
            }
        }

        TextView use = (TextView) findViewById(R.id.btn_use);
        use.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (file != null) {
                    Intent data = new Intent();
                    data.putExtra("image", file);
                    setResult(RESULT_OK, data);
                    finish();
                }
            }
        });

        TextView cancel = (TextView) findViewById(R.id.btn_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    private void onItemSelected(int pos, MediaLibraryConfig.Tag item) {
        if (ServiceRequest.isAvailable()) {
            ServiceRequest.getInstance().getItem().setImageTag(item);
        } else if (MessageWrapper.isAvailable()) {
            MessageWrapper.getInstance().setImageTag(item);
        }
    }
}