package com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.ksoap2.serialization.SoapObject;

import com.fcscs.android.mconnectv3.dao.entity.Runner;

public class FindRunnerResponse implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5337862351203201087L;

    private int total;
    private List<Runner> runners = new ArrayList<Runner>();

    public FindRunnerResponse() {
    }

    public FindRunnerResponse(SoapObject soap) {
        this.total = Integer.valueOf(soap.getAttribute("total").toString());
        if (soap.getPropertyCount() > 0) {
            for (int i = 0; i < soap.getPropertyCount(); i++) {
                runners.add(new Runner((SoapObject) soap.getProperty(i)));
            }
        }
    }

    public int getTotal() {
        return total;
    }

    public List<Runner> getRunners() {
        return runners;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public void setRunners(List<Runner> runners) {
        this.runners = runners;
    }

}
