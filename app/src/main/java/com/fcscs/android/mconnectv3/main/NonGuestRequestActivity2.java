package com.fcscs.android.mconnectv3.main;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.McOnEditorActionListener;
import com.fcscs.android.mconnectv3.common.QRCodeParserFactory;
import com.fcscs.android.mconnectv3.common.QRCodeParserFactory.InterDepartmentQRCodeParser;
import com.fcscs.android.mconnectv3.common.SessionContext;
import com.fcscs.android.mconnectv3.common.ui.HomeTopBar;
import com.fcscs.android.mconnectv3.common.ui.McProgressAsyncTask;
import com.fcscs.android.mconnectv3.common.util.DateTimeHelper;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.common.util.PrefsUtil;
import com.fcscs.android.mconnectv3.dao.entity.Runner;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.FindRunnerResponse;
import com.fcscs.android.mconnectv3.ws.econnect.ECService;
import com.fcscs.android.mconnectv3.zxing.CaptureActivity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class NonGuestRequestActivity2 extends NewRequestBaseActivity {

    private HomeTopBar headBar;
    private EditText who;
    private FindRunnerLoader task1;

    //	private EditText reportby;
    private int intPageSize = 10;
    private int intPageIndex = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        headBar = (HomeTopBar) findViewById(R.id.top_bar);
        headBar.getTitleTv().setText(R.string.non_guest_request);

        where.setOnEditorActionListener(new McOnEditorActionListener() {

            @Override
            public boolean onEnter(TextView view, int actionId, KeyEvent event) {
                String strSearchTerm = where.getText().toString().trim();
                //..Modify By 	: Chua Kam Hoong
                //..Date 		: 20 May 2015
                //findLocations(searchTerm);
                //findLocations(intPageSize, intPageIndex, searchTerm);
                intPageIndex = 1;
                findLocations(intPageIndex, strSearchTerm);
                return true;
            }
        });

        who = (EditText) findViewById(R.id.commonWhoEtId);
        who.setHint(R.string.guestname_isvip);
        who.setText(SessionContext.getInstance().getName());
        who.setEnabled(true);
        who.setOnEditorActionListener(new McOnEditorActionListener() {

            @Override
            public boolean onEnter(TextView view, int actionId, KeyEvent event) {
                String runner = who.getText().toString().trim();
                findRunners(runner);
                return true;
            }
        });

        qrButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NonGuestRequestActivity2.this, CaptureActivity.class);
                intent.putExtra("type", QRCodeParserFactory.INTERDEPT_REQUEST);
                startActivityForResult(intent, SCANNIN_LOCATION_CODE);
            }
        });
        qrButton.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                findLocations(0, where.getText().toString().trim());
                return true;
            }
        });

        boolean enable = PrefsUtil.getQRCodeNewRequest(this);
        if (enable == false) {
            qrButton.setVisibility(View.GONE);
        }

    }

    private void findRunners(String runner) {

        if (task1 == null || AsyncTask.Status.RUNNING.equals(task1.getStatus()) == false) {
            task1 = new FindRunnerLoader(this, runner);
            task1.exec();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }
        if (requestCode == RQ_SEARCH_RUNNER) {
            @SuppressWarnings("unchecked")
            ArrayList<Runner> myRunners = (ArrayList<Runner>) data.getSerializableExtra(SearchRunnerActivity.KEY_RESULT);
            if (myRunners != null && myRunners.size() > 0) {
                who.setText(myRunners.get(0).getFullname());
            }
        }
        if (requestCode == SCANNIN_LOCATION_CODE && data != null) {
            Bundle bundle = data.getExtras();
            String result = bundle.getString("result");
            //String result = "nonguestrequest|LOCLobby-101|ITC31022|QTY2|AUT0";

            InterDepartmentQRCodeParser parser = new InterDepartmentQRCodeParser();
            if (parser.isCorrectFormat(result)) {
                new NonGuestQRCodeTask(this, parser.room, "", "0", "").exec();
            } else {
                toast(R.string.qr_code_format_is_incorrect);
            }
        }
    }

    protected void onAddButtonClick() {
        String nonguest = who.getText().toString().trim();
        if (request.isLocationSelected()) {
            toast(getString(R.string.please_select_location));
            where.requestFocus();
            return;
        } else if ("".equals(nonguest)) {
            toast(getString(R.string.please_input_reportname));
            who.requestFocus();
            return;
        } else {
            super.onAddButtonClick();
        }
    }

    public void onSumitButtonClick() {
        String reportedByName = who.getText().toString().trim();
        if (request.isLocationSelected()) {
            toast(getString(R.string.please_select_location));
            where.requestFocus();
            return;
        } else if ("".equals(reportedByName)) {
            toast(getString(R.string.please_input_reportname));
            who.requestFocus();
            return;
        } else if (request.getItems().size() == 0) {
            toast(getString(R.string.please_select_serviceitem));
            serviceItemTv.requestFocus();
            return;
        } else {
            Date da = null;
            if (McUtils.isNullOrEmpty(date.getEditableText().toString()) == false) {
                if (DateTimeHelper.getDateByTimeZone(new Date()).after(dateAndTime.getTime())) {
                    toast(getString(R.string.please_select_future_time));
                    return;
                }
                da = dateAndTime.getTime();
            }
            request.setScheduledDate(da);
            request.setGuestName(reportedByName);
            submitCheckRequest(request.getGuestName(), request.getLocationCode(), true, NON_GUEST_REQUEST_TYPE);
//			sumitInterDepartmentRequest();
        }
    }

    class FindRunnerLoader extends McProgressAsyncTask {

        public FindRunnerLoader(Context context, String term) {
            super(context);
            this.term = term;
        }

        private String term;
        private FindRunnerResponse res;

        @Override
        public void doInBackground() {
            res = ECService.getInstance().getAdHocUserList(getCurrentContext(), term, null, null);
        }

        @Override
        public void onPostExecute() {
            if (res != null) {
                List<Runner> runners = res.getRunners();
                if (runners == null || runners.size() == 0) {
                    toast(getString(R.string.no_runner_found));
                } else if (runners.size() == 1) {
                    who.setText(runners.get(0).getFullname());
                } else {
                    Intent i = new Intent(getCurrentContext(), SearchRunnerActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.putExtra(SearchRunnerActivity.KEY_RUNNER, res);
                    i.putExtra(SearchRunnerActivity.KEY_TERM, term);
                    i.putExtra("isSingleChoice", true);
                    startActivityForResult(i, RQ_SEARCH_RUNNER);
                }
            }
        }

    }


    @Override
    public void find(String string) {
        //..Modify By 	: Chua Kam Hoong
        //..Date 		: 20 May 2015
        //findLocations(string);
        //findLocations(intPageSize, intPageIndex, string);
        findLocations(intPageIndex, string);
    }

    //public void find(String string) {
    /*
	@Override
	public void find(String string) {
		//..Modify By 	: Chua Kam Hoong
		//..Date 		: 20 May 2015
		//findLocations(string);
		findLocations(intPageSize, intPageIndex, string);
	}
	*/
}
