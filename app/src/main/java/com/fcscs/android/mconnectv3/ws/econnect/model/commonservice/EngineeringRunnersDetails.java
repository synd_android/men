package com.fcscs.android.mconnectv3.ws.econnect.model.commonservice;

/**
 * Created by FCS on 4/25/17.
 */

public class EngineeringRunnersDetails {
    private long runnerUserID = -1L;
    private String runnerUserName;
    private String runnerUserFullName;
    private String runnerDepartment;

    public EngineeringRunnersDetails() {
    }

    public String getRunnerDepartment() {
        return runnerDepartment;
    }

    public void setRunnerDepartment(String runnerDepartment) {
        this.runnerDepartment = runnerDepartment;
    }

    public long getRunnerUserID() {
        return runnerUserID;
    }

    public void setRunnerUserID(long runnerUserID) {
        this.runnerUserID = runnerUserID;
    }

    public String getRunnerUserName() {
        return runnerUserName;
    }

    public void setRunnerUserName(String runnerUserName) {
        this.runnerUserName = runnerUserName;
    }

    public String getRunnerUserFullName() {
        return runnerUserFullName;
    }

    public void setRunnerUserFullName(String runnerUserFullName) {
        this.runnerUserFullName = runnerUserFullName;
    }
}
