package com.fcscs.android.mconnectv3.ws.cconnect.model.securityservice;

import java.io.Serializable;

import org.ksoap2.serialization.SoapObject;

public class LogoutResponse implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6445280950775229978L;

    private boolean result;

    public LogoutResponse() {
    }

    public LogoutResponse(SoapObject soap) {
        this.result = Boolean.valueOf(soap.getAttribute("result").toString());
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

}
