package com.fcscs.android.mconnectv3.common;

import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

/**
 * OnEditorActionListener的实现类，方便监听回车事件
 *
 * @author Peter
 */
public abstract class McOnEditorActionListener implements OnEditorActionListener {

    @Override
    public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {

        if (event == null) {
            //单行EditText
            if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_NEXT
                    || actionId == EditorInfo.IME_ACTION_GO || actionId == EditorInfo.IME_ACTION_SEARCH
                    || actionId == EditorInfo.IME_ACTION_SEND) {
                return onEnter(view, actionId, event);
            } else {
                // 系统处理其他事件
                return false;
            }
        } else if (actionId == EditorInfo.IME_NULL) {
            // 多行EditText回车
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                return onEnter(view, actionId, event);
            } else {
                return true;
            }
        }
        return false;
    }

    /**
     * @param view
     * @param actionId
     * @param event
     * @return 当处理完事件时，应该返回true，否则系统可能反复调用监听器
     */
    public abstract boolean onEnter(TextView view, int actionId, KeyEvent event);

}
