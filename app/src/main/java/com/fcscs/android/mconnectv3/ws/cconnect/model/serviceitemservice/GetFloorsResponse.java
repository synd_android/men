package com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.ksoap2.serialization.SoapObject;

import com.fcscs.android.mconnectv3.dao.entity.Floor;


public class GetFloorsResponse implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1057530211892854776L;
    private List<Floor> floorList;

    public GetFloorsResponse() {
        floorList = new ArrayList<Floor>();
    }

    public GetFloorsResponse(SoapObject soap) {
        floorList = new ArrayList<Floor>();
        for (int i = 0; i < soap.getPropertyCount(); i++) {
            floorList.add(new Floor((SoapObject) soap.getProperty(i)));
        }
    }


    public List<Floor> getFloorList() {
        return floorList;
    }

    public void setFloorList(List<Floor> floorList) {
        this.floorList = floorList;
    }

}
