package com.fcscs.android.mconnectv3.ws.econnect.model.commonservice;

import com.fcscs.android.mconnectv3.common.util.SoapHelper;
import com.fcscs.android.mconnectv3.manager.model.JobModel;

import org.ksoap2.serialization.SoapObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GetNotificationStatusResponse implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 3462243046338355651L;

    private int returnCode;
    private String errorMsg;
    private List<JobModel> list;

    public GetNotificationStatusResponse() {

    }

    public GetNotificationStatusResponse(SoapObject soap) {
        SoapObject status = (SoapObject) soap.getProperty("ResponseStatus");
        returnCode = SoapHelper.getIntegerProperty(status, "ReturnCode", 1);
        errorMsg = SoapHelper.getStringProperty(status, "ErrorMsg", "");

        list = new ArrayList<JobModel>();
        for (int i = 0; i < soap.getPropertyCount(); i++) {
            if ("NotificationStatusResponseJobDetailsListing".equalsIgnoreCase(SoapHelper.getPropertyName(soap, i))) {
                SoapObject s = (SoapObject) soap.getProperty(i);
                for (int j = 0; j < s.getPropertyCount(); j++) {
                    if ("NotificationStatusResponseJobDetails".equalsIgnoreCase(SoapHelper.getPropertyName(s, j))) {
                        SoapObject l = (SoapObject) s.getProperty(j);
                        String jobNo = SoapHelper.getStringProperty(l, "JobNo", "");
                        int jobType = SoapHelper.getIntegerProperty(l, "JobType", 1);
                        int statusID = SoapHelper.getIntegerProperty(l, "StatusID", 0);

                        JobModel m = new JobModel();
                        m.setJobNum(jobNo);
                        m.setNotificationType(jobType);
                        m.setIsRead(statusID == 1 ? true : false);
                        list.add(m);
                    }
                }
            }
        }
    }

    public int getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(int returnCode) {
        this.returnCode = returnCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public List<JobModel> getList() {
        return list;
    }

    public void setList(List<JobModel> list) {
        this.list = list;
    }

}
