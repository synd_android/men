package com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.ksoap2.serialization.SoapObject;

public class GetTemplateListResponse implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2267191909484985919L;

    private List<Template> templates = new ArrayList<Template>();

    public GetTemplateListResponse() {
    }

    public GetTemplateListResponse(SoapObject soap) {
        if (soap.getPropertyCount() > 0) {
            for (int i = 0; i < soap.getPropertyCount(); i++) {
                templates.add(new Template((SoapObject) soap.getProperty(i)));
            }
        }
    }

    public List<Template> getTemplates() {
        return templates;
    }

    public void setTemplates(List<Template> templates) {
        this.templates = templates;
    }

}
