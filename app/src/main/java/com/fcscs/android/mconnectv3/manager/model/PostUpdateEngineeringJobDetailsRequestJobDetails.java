package com.fcscs.android.mconnectv3.manager.model;

import com.fcscs.android.mconnectv3.common.McEnums.JobStatus;

import java.io.Serializable;
import java.util.Date;

public class PostUpdateEngineeringJobDetailsRequestJobDetails implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String userId;
    private String jobNo;
    private String actionTaken;
    private String equipmentID;
    private String pmConditionID;
    private String manHours;
    private String cost;
    private Date deadline;

    public String getEquipmentID() {
        return equipmentID;
    }

    public void setEquipmentID(String equipmentID) {
        this.equipmentID = equipmentID;
    }

    public String getPmConditionID() {
        return pmConditionID;
    }

    public void setPmConditionID(String pmConditionID) {
        this.pmConditionID = pmConditionID;
    }

    public String getManHours() {
        return manHours;
    }

    public void setManHours(String manHours) {
        this.manHours = manHours;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getJobNo() {
        return jobNo;
    }

    public void setJobNo(String jobNo) {
        this.jobNo = jobNo;
    }

    public String getActionTaken() {
        return actionTaken;
    }

    public void setActionTaken(String actionTaken) {
        this.actionTaken = actionTaken;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public Date getDeadline() {
        return deadline;
    }
}
