package com.fcscs.android.mconnectv3.smartpush;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.fcscs.android.mconnectv3.GCMIntentService;
import com.google.android.gcm.GCMBaseIntentService;

/**
 * Created by FCS on 6/22/16.
 */
public class SmartPushGCMBroadcastReceiver extends BroadcastReceiver {
    private static final String TAG = "SmartPushGCMBroadcastReceiver";

    public SmartPushGCMBroadcastReceiver() {
    }

    public final void onReceive(Context context, Intent intent) {
        try {
            String action = intent.getAction();
            if ("com.google.android.c2dm.intent.RECEIVE".equalsIgnoreCase(action) ||
                    "com.google.android.c2dm.intent.REGISTRATION".equalsIgnoreCase(action))
                Log.v("SmartpushReceiver", "onReceive: " + intent.getAction());
            String className = this.getGCMIntentServiceClassName(context);
            Log.v("SmartpushReceiver", "GCM IntentService class: " + className);
            GCMBaseIntentService.runIntentInService(context, intent, className);
            this.setResult(-1, (String) null, (Bundle) null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected String getGCMIntentServiceClassName(Context context) {
        return GCMIntentService.class.getName();
    }
}
