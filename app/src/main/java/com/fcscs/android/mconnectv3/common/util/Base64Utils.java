package com.fcscs.android.mconnectv3.common.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

public class Base64Utils {

    public static byte[] getByteArrayFromString(String s) {
        byte[] b = Base64.decode(s, Base64.DEFAULT);
        return b;
    }

    public static Bitmap getImageFromBase64String(String s) {
        byte[] b = getByteArrayFromString(s);
        if (b != null && b.length > 0) {
            try {
                Bitmap image = BitmapFactory.decodeByteArray(b, 0, b.length);
                return image;
            } catch (Exception e) {
                e.printStackTrace();
            } catch (OutOfMemoryError e) {
                e.printStackTrace();
                System.gc();
            }
        }
        return null;
    }
}
