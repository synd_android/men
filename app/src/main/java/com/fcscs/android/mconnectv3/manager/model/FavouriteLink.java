package com.fcscs.android.mconnectv3.manager.model;

import java.io.Serializable;

import com.fcscs.android.mconnectv3.common.McEnums;

public class FavouriteLink implements Serializable {
//	<ID>1</ID>
//	<Type>1</Type>
//	<Title>Google</Title>
//	<URL>http://www.google.com</URL>

    private static final long serialVersionUID = -4745865410748683800L;
    private String id;
    private McEnums.FavoritesEnum type;
    private String title;
    private String url;

    public FavouriteLink() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setType(McEnums.FavoritesEnum type) {
        this.type = type;
    }

    public McEnums.FavoritesEnum getType() {
        return type;
    }

}
