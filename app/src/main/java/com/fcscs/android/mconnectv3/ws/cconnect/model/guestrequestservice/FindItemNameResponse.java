package com.fcscs.android.mconnectv3.ws.cconnect.model.guestrequestservice;

import com.fcscs.android.mconnectv3.manager.model.ItemNameInfo;

import org.ksoap2.serialization.SoapObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by NguyenMinhTuan on 11/4/15.
 */
public class FindItemNameResponse implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    ArrayList<ItemNameInfo> itemNameInfos;

    public FindItemNameResponse() {
        itemNameInfos = new ArrayList<ItemNameInfo>();
    }

    public FindItemNameResponse(SoapObject soap) {
        itemNameInfos = new ArrayList<ItemNameInfo>();
        if (soap.getPropertyCount() > 0) {

            for (int i = 0; i < soap.getPropertyCount(); i++) {
                itemNameInfos.add(new ItemNameInfo((SoapObject) soap.getProperty(i)));
            }
        }
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public ArrayList<ItemNameInfo> getItemNameInfos() {
        return itemNameInfos;
    }

    public void setItemNameInfos(ArrayList<ItemNameInfo> itemNameInfos) {
        this.itemNameInfos = itemNameInfos;
    }

}
