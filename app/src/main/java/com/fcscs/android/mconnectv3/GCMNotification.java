package com.fcscs.android.mconnectv3;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.fcscs.android.mconnectv3.common.ConfigContext;
import com.fcscs.android.mconnectv3.common.McConstants;
import com.fcscs.android.mconnectv3.common.McEnums;
import com.fcscs.android.mconnectv3.common.SessionContext;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.common.util.PrefsUtil;
import com.fcscs.android.mconnectv3.dao.entity.DBHelper;
import com.fcscs.android.mconnectv3.dao.entity.NotificationModel;
import com.fcscs.android.mconnectv3.main.JobDetailActivity;
import com.fcscs.android.mconnectv3.main.MyJobsActivity;
import com.fcscs.android.mconnectv3.main.MyMessageDetailActivity;
import com.fcscs.android.mconnectv3.manager.model.JobModel;
import com.fcscs.android.mconnectv3.manager.model.MessageModel;
import com.fcscs.android.mconnectv3.ws.cconnect.SecurityService;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.JobViewResponse;
import com.fcscs.android.mconnectv3.ws.econnect.ECService;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.PullNotificationModel;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GCMNotification {
    private static final String TAG = GCMNotification.class.getSimpleName();
    static int noteId = 0;

    public static boolean isCConnect() {
        if (McEnums.ServiceType.CCONNECT.equals(ConfigContext.getInstance().getServiceType())) {
            return true;
        }
        return false;
    }

    public static boolean registerId(Context context, String regId) {

        SessionContext ctx = SessionContext.getInstance();
        if (ctx.getFcsUserId() > 0) {
            if (isCConnect()) {
                return SecurityService.getInstanceNonUI()
                        .updateSession(context, ctx.getFcsUserId(), ctx.getWsSessionId(), null, regId) != null;
            } else {
                return ECService.getInstance().updateToken(context, ctx.getFcsUserId(), regId);
            }
        }
        return false;
    }

    public static int getECMsgNotificationId(String msgId) {
        try {
            return 10000000 + Integer.valueOf(msgId);
        } catch (Exception e) {
        }
        return 10000000;
    }

    public static long getECJobNotificationId(long jobId) {
        return 20000000 + jobId;
    }

    public static void cancelECMsgNotification(Context context, String id) {
        int nid = getECMsgNotificationId(id);
        cancelNotification(context, nid);
    }

    public static void cancelECJobNotification(Context context, String id) {
        try {
            long lid = Long.valueOf(id);
            long nid = getECJobNotificationId(lid);
            cancelNotification(context, (int) nid);
        } catch (Exception e) {
        }
    }

    public static void showECMsgNotification(Context context, NotificationManager mManager, MessageModel model, String prefix) {
        Intent msgIntent = MyMessageDetailActivity.createEConnectIntent(context, model);
        msgIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        int nid = getECMsgNotificationId("" + model.getMsgDoneId());
        String strTitle = prefix + model.getTitle();
        //..Create By : Chua Kam Hoong
        String strTitle2 = model.getNotificationContent();
        if (strTitle2.length() == 0) {
            strTitle2 = model.getTitle();
        }
        notify(context, mManager, strTitle, strTitle2, msgIntent, nid);
    }

    public static void showPushECMsgNotification(Context context, NotificationManager mManager, MessageModel model, String prefix) {
        Intent msgIntent = MyMessageDetailActivity.createEConnectIntent(context, model);
        msgIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        int nid = getECMsgNotificationId("" + model.getMsgDoneId());
        String strTitle = model.getNotificationContent();
        //..Create By : Chua Kam Hoong
        String strTitle2 = model.getNotificationContent();
        if (strTitle2.length() == 0) {
            strTitle2 = model.getTitle();
        }
        notify(context, mManager, strTitle, strTitle2, msgIntent, nid);
    }

    //has star
    public static void showECJobNotification(Context context, NotificationManager mManager, JobModel model, String prefix, Boolean showContentFromWS) {

        boolean isEngineering = PrefsUtil.getEnableEng(context) == 1 ? true : false;
        Intent fwdIntent;
        if (isEngineering) {
            fwdIntent = new Intent(context, JobDetailActivity.class);
            fwdIntent.putExtra(JobDetailActivity.KEY_PUSH_GCM, JobDetailActivity.KEY_PUSH_GCM);
            fwdIntent.putExtra("JobModel", model);
        }
        else {
            fwdIntent = new Intent(context, MyJobsActivity.class);
        }

        fwdIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        long nid = getECJobNotificationId(model.getJobId());

        String strTitle = generateTitle(model, prefix);
        //..Create By : Chua Kam Hoong
        //String strContent = generateBody(model);
        String strContent = model.getNotificationContent();
        if (strContent != null && strContent.length() == 0) {
            strContent = generateBody(model);
        }

        notify(context, mManager, strTitle, strContent, fwdIntent, (int) nid);

    }

    //no star
    public static void showPushECJobNotification(final Context context, final NotificationManager mManager, final JobModel model, final String prefix, Boolean showContentFromWS) {

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                getJobForPushECJobNotification(context , mManager, model, prefix);
            }
        });



    }

    private static String generateBody(JobModel model) {
        StringBuffer sb = new StringBuffer();
        if (model.getGuestName() != null && model.getGuestName().trim().length() > 0) {
            sb.append("(" + model.getGuestName() + ")");
        } else if (model.getReportedBy() != null && model.getReportedBy().trim().length() > 0) {
            sb.append("(" + model.getReportedBy() + ")");
        }
        sb.append("(" + model.getRoomNum() + ")");
        sb.append(model.getServiceItemName() + "*" + model.getQuantity());
        return sb.toString();
    }

    private static String generateTitle(JobModel model, String prefix) {
        String title = prefix + "(" + model.getJobId() + ")" + model.getServiceItemName() + "*" + model.getQuantity();
        return title;
    }

    public static void notify(Context context, NotificationManager mManager, String title, String content, Intent redirect) {
        int id = noteId++;
        notify(context, mManager, title, content, redirect, id);
    }

    public static void notify(Context context, NotificationManager mManager, String title, String content, Intent redirect, int id) {

        int drawableIcon = R.drawable.pro_icon;
        if (McConstants.COMPILATION_PRODUCT == McEnums.CompilationProduct.PERCIPIA) {
            drawableIcon = R.drawable.pro_icon_green_percipia;
        } else if (McConstants.COMPILATION_PRODUCT == McEnums.CompilationProduct.MENGINEERING) {
            drawableIcon = R.drawable.pro_icon_en;
        }

        PendingIntent contentIntent = PendingIntent.getActivity(context, id, redirect, PendingIntent.FLAG_CANCEL_CURRENT);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(drawableIcon)
                .setContentTitle(title)
                .setContentText(content)
                .setContentIntent(contentIntent);
        Notification notification = mBuilder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        setNotificationSoundVibrate(context, notification);
        mManager.notify(id, notification);
    }

    public static void notifyNoNetwork(Context context, NotificationManager mManager, int id) {
        int drawableIcon = R.drawable.pro_icon_red;
        if (McConstants.COMPILATION_PRODUCT == McEnums.CompilationProduct.PERCIPIA) {
            drawableIcon = R.drawable.pro_icon_grey_percipia;
        } else if (McConstants.COMPILATION_PRODUCT == McEnums.CompilationProduct.MENGINEERING) {
            drawableIcon = R.drawable.pro_icon_en;
        }
        String noNetwork = context.getString(R.string.network_connection_unavailable);
        PendingIntent intent = PendingIntent.getActivity(context, id, new Intent(Settings.ACTION_WIRELESS_SETTINGS),
                PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(drawableIcon)
                .setContentTitle(noNetwork)
                .setContentText(noNetwork)
                .setContentIntent(intent);
        Notification notification = mBuilder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notification.sound = null;
        notification.defaults = 0;
        setNotificationSoundVibrate(context, notification);
        mManager.notify(id, notification);
    }

    private static void cancelNotification(Context ctx, int notifyId) {
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager nMgr = (NotificationManager) ctx.getSystemService(ns);
        nMgr.cancel(notifyId);
    }

    private static void setNotificationSoundVibrate(Context context, Notification notification) {
        AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        boolean sound = false;
        boolean vibrate = false;

        switch (am.getRingerMode()) {
            case AudioManager.RINGER_MODE_SILENT:
                sound = false;
                vibrate = false;
                break;
            case AudioManager.RINGER_MODE_VIBRATE:
                sound = false;
                vibrate = true;
                break;
            case AudioManager.RINGER_MODE_NORMAL:
                sound = true;
                vibrate = true;
                break;
        }

        if (sound && PrefsUtil.isEnableSoundWifiCheck(context)) {
//			notification.defaults |= Notification.DEFAULT_SOUND;
            int notifySound = PrefsUtil.getNotificationSound(context);
            if (notifySound == McConstants.DEFAULT_NOTIFICATION_SOUND_TYPE) {
                notification.defaults |= Notification.DEFAULT_SOUND;
            } else {
                Uri path = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.notification1);
                if (notifySound == McConstants.NOTIFICATION_SOUND_1)
                    path = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.notification1);
                else if (notifySound == McConstants.NOTIFICATION_SOUND_2)
                    path = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.notification2);
                else if (notifySound == McConstants.NOTIFICATION_SOUND_3)
                    path = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.notification3);
                else if (notifySound == McConstants.NOTIFICATION_SOUND_4)
                    path = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.notification4);
                notification.sound = path;
            }
        }
        if (vibrate) {
            notification.defaults |= Notification.DEFAULT_VIBRATE;
            long[] vibr = {0, 100, 200, 400};
            notification.vibrate = vibr;
        }
    }

    public static void showNotifications(Context context, String prefix) {

        McApplication app = (McApplication) context.getApplicationContext();
        if (app.isLogin() == false) {
            return;
        }

        String userId = "" + SessionContext.getInstance().getFcsUserId();
        String product = ConfigContext.getInstance().getServiceType().toString();
        List<NotificationModel> list = DBHelper.queryNotifications(context, userId, product);

        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager mManager = (NotificationManager) context.getSystemService(ns);

        for (NotificationModel n : list) {
            try {
                String dataType = n.getDataType();
                String id = n.getDataId();
                String title = n.getNotificationTitle();
                String content = n.getNotificationContent();
                if ("JOB".equalsIgnoreCase(dataType)) {
                    showECJobNotification(context, mManager, id, prefix, title, content);
                } else if ("MSG".equalsIgnoreCase(dataType)) {
                    showECMsgNotification(context, mManager, id, prefix, title, content);
                }
                DBHelper.deleteNotification(context, n);
            } catch (Exception e) {
                Log.e(TAG, "Failed to show notification", e);
            }
        }

    }

    public static void showECJobNotification(Context context, NotificationManager mManager, String id, String prefix, String title,
                                             String content) {
        Intent fwdIntent = new Intent(context, MyJobsActivity.class);
        fwdIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        long nid = getECJobNotificationId(Long.valueOf(id));
        notify(context, mManager, prefix + title, content, fwdIntent, (int) nid);
    }

    public static void showECMsgNotification(Context context, NotificationManager mManager, String id, String prefix, String title,
                                             String content) {
        MessageModel message = ECService.getInstance().getAcHocDetail(context,
                SessionContext.getInstance().getFcsUserId(), id);
        if (message != null) {
            Intent msgIntent = MyMessageDetailActivity.createEConnectIntent(context, message);
            msgIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            int nid = getECMsgNotificationId(id);
            notify(context, mManager, prefix + title, content, msgIntent, nid);
        }
    }

    private static void getJobForPushECJobNotification(Context context , NotificationManager mManager, JobModel model, String prefix) {
        McUtils.saveLogToSDCard("###retrieve jobs...");
        JobViewResponse resp = ECService.getInstance().getJobs(context, "", new Date(), "", "", "", "", "", "", "", null);
        if (resp != null && resp.getJobViews() != null && resp.getJobViews().size() > 0) {
            McUtils.saveLogToSDCard("###retrieve jobs size: " + resp.getJobViews().size());
            Map<String, JobModel> map = new HashMap<String, JobModel>();
            for (JobModel j : resp.getJobViews()) {
                McUtils.saveLogToSDCard("###anaylyze job: " + j.getJobNum() + ", is ack: " + j.getIsAcked());
                if (j.getStatus().getStatusId() != McEnums.JobStatus.COMPLETED.getStatusId()
                        || j.getStatus().getStatusId() != McEnums.JobStatus.CANCELLED.getStatusId()) {
                    if(j.getJobNum().equals(model.getJobNum())){
                        map.put(j.getJobNum(), j);

                        boolean isEngineering = PrefsUtil.getEnableEng(context) == 1 ? true : false;

                        Intent fwdIntent;
                        if (isEngineering) {
                            fwdIntent = new Intent(context, JobDetailActivity.class);
                            fwdIntent.putExtra(JobDetailActivity.KEY_PUSH_GCM, JobDetailActivity.KEY_PUSH_GCM);
                            fwdIntent.putExtra("JobModel", j);
                        } else {
                            fwdIntent = new Intent(context, MyJobsActivity.class);
                        }
                        fwdIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        long nid = getECJobNotificationId(model.getJobId());
                        String strTitle = model.getNotificationContent();
                        // on mobile notification, if supervisor receive notification start with OK, then the 1st line become "Completion Alert" - Phua 20171219
                        if (SessionContext.getInstance().getUserType().equals("S")
                                && strTitle != null && strTitle.startsWith("OK")) {
                            strTitle = context.getString(R.string.completion_alert);
                        } else if (model.getNotificationId() != 0) {
                            strTitle = generateTitle(model, prefix);
                        }
                        String strContent = model.getNotificationContent();
                        if (strContent != null && strContent.length() == 0) {
                            strContent = generateBody(model);
                        }
                        notify(context, mManager, strTitle, strContent, fwdIntent, (int) nid);
                    }
                }
            }
            } else {
            McUtils.saveLogToSDCard("###retrieve none jobs");
        }
    }
}
