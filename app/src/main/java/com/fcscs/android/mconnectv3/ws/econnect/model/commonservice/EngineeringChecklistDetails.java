package com.fcscs.android.mconnectv3.ws.econnect.model.commonservice;

/**
 * Created by FCS on 4/25/17.
 */

public class EngineeringChecklistDetails {
    private int contentId;
    private String content;
    private String input;
    private String label;
    private boolean remarks;
    private String answerID;
    private String inputAnswer;
    private String remarksAnswer;
    private String checklistName;

    public String getChecklistName() {
        return checklistName;
    }

    public void setChecklistName(String checklistName) {
        this.checklistName = checklistName;
    }

    public EngineeringChecklistDetails() {
    }

    public int getContentId() {
        return contentId;
    }

    public void setContentId(int contentId) {
        this.contentId = contentId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isRemarks() {
        return remarks;
    }

    public void setRemarks(boolean remarks) {
        this.remarks = remarks;
    }

    public String getAnswerID() {
        return answerID;
    }

    public void setAnswerID(String answerID) {
        this.answerID = answerID;
    }

    public String getInputAnswer() {
        return inputAnswer;
    }

    public void setInputAnswer(String inputAnswer) {
        this.inputAnswer = inputAnswer;
    }

    public String getRemarksAnswer() {
        return remarksAnswer;
    }

    public void setRemarksAnswer(String remarksAnswer) {
        this.remarksAnswer = remarksAnswer;
    }
}
