package com.fcscs.android.mconnectv3.ws.cconnect.model.guestrequestservice;

import com.fcscs.android.mconnectv3.manager.model.ItemGroupInfo;
import com.fcscs.android.mconnectv3.manager.model.ItemTopInfo;

import org.ksoap2.serialization.SoapObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by NguyenMinhTuan on 11/4/15.
 */
public class FindItemResponse implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private List<ItemTopInfo> itemTopInfoList;
    private List<ItemGroupInfo> itemGroupInfoList;

    public String getRequestor() {
        return requestor;
    }

    public void setRequestor(String requestor) {
        this.requestor = requestor;
    }

    private String requestor = "";

    public FindItemResponse() {
        itemTopInfoList = new ArrayList<ItemTopInfo>();
        itemGroupInfoList = new ArrayList<ItemGroupInfo>();
    }

    public FindItemResponse(SoapObject soapTop, SoapObject soapGroup, String requestor) {
        itemTopInfoList = new ArrayList<ItemTopInfo>();
        itemGroupInfoList = new ArrayList<ItemGroupInfo>();
        if (soapTop.getPropertyCount() > 0) {
            for (int i = 0; i < soapTop.getPropertyCount(); i++) {
                itemTopInfoList.add(new ItemTopInfo((SoapObject) soapTop.getProperty(i)));
            }
        }
        if (soapGroup.getPropertyCount() > 0) {
            for (int i = 0; i < soapGroup.getPropertyCount(); i++) {
                itemGroupInfoList.add(new ItemGroupInfo((SoapObject) soapGroup.getProperty(i)));
            }
        }
        this.requestor = requestor;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public List<ItemGroupInfo> getItemGroupInfoList() {
        return itemGroupInfoList;
    }

    public void setItemGroupInfoList(List<ItemGroupInfo> itemGroupInfoList) {
        this.itemGroupInfoList = itemGroupInfoList;
    }

    public List<ItemTopInfo> getItemTopInfoList() {
        return itemTopInfoList;
    }

    public void setItemTopInfoList(List<ItemTopInfo> itemTopInfoList) {
        this.itemTopInfoList = itemTopInfoList;
    }
}