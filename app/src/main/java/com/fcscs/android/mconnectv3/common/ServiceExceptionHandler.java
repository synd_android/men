package com.fcscs.android.mconnectv3.common;

import java.lang.Thread.UncaughtExceptionHandler;

import android.content.Context;

import com.fcscs.android.mconnectv3.common.util.McUtils;

public class ServiceExceptionHandler extends McExceptionHandlerBase implements UncaughtExceptionHandler {

    public static void setHandler(Context ctx) {
        Thread.setDefaultUncaughtExceptionHandler(new ServiceExceptionHandler(ctx));
    }

    private UncaughtExceptionHandler defaultHandler;
    private Context mContext;

    public ServiceExceptionHandler(Context ctx) {
        super(ctx);
        this.mContext = ctx;
        if (defaultHandler == null) {
            defaultHandler = Thread.getDefaultUncaughtExceptionHandler();
        }
    }

    public void uncaughtException(Thread t, Throwable e) {
        if (McConstants.SAVE_CRASH_REPORT) {
            McUtils.saveUncaughtException(mContext, defaultHandler, t, e);
        } else {
            defaultHandler.uncaughtException(t, e);
        }
    }

}