package com.fcscs.android.mconnectv3.manager.model;

import java.io.Serializable;

public class ServiceCodeModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8759808709247125513L;

    private String code;
    private String name;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
