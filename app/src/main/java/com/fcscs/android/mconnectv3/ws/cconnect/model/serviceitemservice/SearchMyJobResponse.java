package com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fcscs.android.mconnectv3.manager.model.JobModel;
import com.fcscs.android.mconnectv3.manager.model.StatusModel;

public class SearchMyJobResponse implements Serializable {

    private static final long serialVersionUID = 1L;
    private List<StatusModel> statusModels = null;
    private int total;
    private List<JobModel> jobViews = null;

    public SearchMyJobResponse() {
        statusModels = new ArrayList<StatusModel>();
        jobViews = new ArrayList<JobModel>();
    }

    public int getTotal() {
        return total;
    }

    public List<JobModel> getJobViews() {
        return jobViews;
    }

    public void setJobViews(List<JobModel> jobViews) {
        this.jobViews = jobViews;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public void setStatusModels(List<StatusModel> statusModels) {
        this.statusModels = statusModels;
    }

    public List<StatusModel> getStatusModels() {
        return statusModels;
    }

}
