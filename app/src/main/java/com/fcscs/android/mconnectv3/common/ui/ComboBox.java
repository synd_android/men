package com.fcscs.android.mconnectv3.common.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fcscs.android.mconnectv3.R;

/**
 * Created by Tuan Nguyen on 9/23/15.
 */
public class ComboBox extends RelativeLayout {

    public AutoCompleteTextView _text;
    private ImageButton _button, _search;
    ArrayAdapter<String> array;
    boolean isShowDropDown;
    Context mContext;

    public ComboBox(Context context) {
        super(context);
        this.mContext = context;
        init();
    }

    public ComboBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        init();
    }

    private void init() {
        View v = inflate(getContext(), R.layout.combobox_item, this);
        _text = (AutoCompleteTextView) v.findViewById(R.id.text);
        _text.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mOnItemClickListener.selectedItem(ComboBox.this, i);
            }
        });
        _text.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                //mOnItemClickListener.clickDropdown(ComboBox.this);
                //_text.showDropDown();
            }
        });
        _text.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                mOnItemClickListener.beginEdit(ComboBox.this);
                return false;
            }
        });
        _text.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
        _search = (ImageButton) v.findViewById(R.id.search);
        _search.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                isShowDropDown = true;
                mOnItemClickListener.searchButton(ComboBox.this);
                if (array.getCount() == 1) {
                    _text.setSelection(0);
                } else {
                    _text.showDropDown();
                }
            }
        });
        _button = (ImageButton) v.findViewById(R.id.dropdownbtn);
        _button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                isShowDropDown = true;
                mOnItemClickListener.clickDropdown(ComboBox.this);
                if (array.getCount() == 1) {
                    _text.setSelection(0);
                } else {
                    _text.showDropDown();
                }
            }
        });
    }


    //    String[]  mSource;
    ArrayAdapter<String> adapter;

    /**
     * Sets the source for DDLB suggestions.
     * Cursor MUST be managed by supplier!!
     *
     * @param source Source of suggestions.
     */
    public void setSuggestionSource(final String[] source) {

        array = new ArrayAdapter<String>(mContext,
                R.layout.combobox_list_item, source);
        _text.setAdapter(array);
        array.notifyDataSetChanged();
        _text.post(new Runnable() {
            @Override
            public void run() {
                if (source.length == 1) {
                    _text.setSelection(0);
                    _text.setText(source[0]);
                    _text.dismissDropDown();
                } else if (isShowDropDown) {
                    _text.requestFocus();
                    _text.showDropDown();
                }
            }
        });
        isShowDropDown = false;

    }

    public void setSelection(int selection) {

        _text.post(new Runnable() {
            @Override
            public void run() {
                _text.setSelection(1);
            }
        });
    }

    /**
     * Gets the text in the combo box.
     *
     * @return Text.
     */
    public String getText() {
//        for (int i = 0; i< array.getCount();i++){
//            if(array.getItem(i).equalsIgnoreCase(_text.getText().toString())){
//                return _text.getText().toString();
//            }
//        }
//        _text.setText("");
        return _text.getText().toString();
    }

    /**
     * Sets the text in combo box.
     */
    public void setText(String text) {
        _text.setText(text);
    }

    public void setError(String text) {
        _text.setError(text);
    }

    public void setHint(String text) {
        _text.setHint(text);
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener listener) {
        _text.setOnItemClickListener(listener);
    }

    OnItemComboboxClickListener mOnItemClickListener;

    /**
     * Register a callback to be invoked when an item in this AdapterView has
     * been clicked.
     *
     * @param listener The callback that will be invoked.
     */
    public void setOnItemComboboxClickListener(OnItemComboboxClickListener listener) {
        mOnItemClickListener = listener;
    }

    public interface OnItemComboboxClickListener {
        void beginEdit(ComboBox cbx);

        void clickDropdown(ComboBox cbx);

        void selectedItem(ComboBox cbx, int pos);

        void searchButton(ComboBox cbx);
    }

    public void showDropdow() {
        _text.post(new Runnable() {
            @Override
            public void run() {
                _text.requestFocus();

                if (array.getCount() == 1) {
                    _text.setSelection(0);
                } else {
                    _text.showDropDown();
                }
            }
        });
    }

    public void showToast(String toastString) {
        Toast.makeText(mContext, toastString, Toast.LENGTH_SHORT).show();
    }

    public void focus() {
        _text.requestFocus();
    }

    public void setSelectedIndex(int index) {

        _text.setListSelection(index);
//        _text.post(new Runnable() {
//            @Override
//            public void run() {
//                _text.setListSelection(index);
//            }
//        });

    }
//    public void reset(){
//        _text.requestFocus();
//    }
//    public void reload(){
//        _text.requestFocus();
//    }
}
