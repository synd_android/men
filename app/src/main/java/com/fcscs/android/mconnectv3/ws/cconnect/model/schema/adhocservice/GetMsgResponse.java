package com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice;

import com.fcscs.android.mconnectv3.common.util.DateTimeHelper;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.ksoap2.serialization.SoapObject;

public class GetMsgResponse implements Serializable {

    //	<xs:sequence>
//	<xs:element type="xs:string" name="FromFirstName" minOccurs="0" />
//	<xs:element type="xs:string" name="FromLastName" minOccurs="0" />
//	<xs:element type="xs:string" name="FromUserName" minOccurs="0" />
//	<xs:element type="xs:dateTime" name="createdDate" minOccurs="0" />
//	<xs:element type="xs:string" name="detail" minOccurs="0" />
//</xs:sequence>
//<xs:attribute name="delete" type="xs:boolean" use="required" />
//<xs:attribute name="ack" type="xs:boolean" use="required" />
//<xs:attribute name="msgDoneId" type="xs:long" use="required" />

    private static final long serialVersionUID = -3399691663197978812L;

    private String fromUserName;
    private boolean ack;
    private Date createdDate;
    private String detail;
    private boolean delete;
    private long msgDoneId;

    public GetMsgResponse() {
    }

    public GetMsgResponse(SoapObject soap) {
        this.fromUserName = soap.getProperty("FromUserName").toString();
        this.ack = Boolean.valueOf(soap.getAttribute("ack").toString());

        SimpleDateFormat sdf = new SimpleDateFormat(DateTimeHelper.DEFAULT_FORMATTER_DATETIME_24H);
        try {
            this.createdDate = sdf.parse(soap.getProperty("createdDate").toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        this.detail = soap.getProperty("detail").toString();

        this.delete = Boolean.valueOf(soap.getAttribute("delete").toString());

        this.msgDoneId = Long.valueOf(soap.getAttribute("msgDoneId").toString());
    }

    public String getFromUserName() {
        return fromUserName;
    }

    public boolean isAck() {
        return ack;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public String getDetail() {
        return detail;
    }

    public boolean isDelete() {
        return delete;
    }

    public long getMsgDoneId() {
        return msgDoneId;
    }

    public void setFromUserName(String fromUserName) {
        this.fromUserName = fromUserName;
    }

    public void setAck(boolean ack) {
        this.ack = ack;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }

    public void setMsgDoneId(long msgDoneId) {
        this.msgDoneId = msgDoneId;
    }


}
