package com.fcscs.android.mconnectv3.manager.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by FCS on 2014/6/18.
 */
public class MediaLibraryConfig implements Serializable {

    public static MediaLibraryConfig config = null;

    private boolean enableCaptureImage;
    private boolean enableVoiceImage;
    private ArrayList<Tag> imageTagList;
    private ArrayList<Tag> voiceTagList;

    public boolean isEnableCaptureImage() {
        return enableCaptureImage;
    }

    public void setEnableCaptureImage(boolean enableCaptureImage) {
        this.enableCaptureImage = enableCaptureImage;
    }

    public boolean isEnableVoiceImage() {
        return enableVoiceImage;
    }

    public void setEnableVoiceImage(boolean enableVoiceImage) {
        this.enableVoiceImage = enableVoiceImage;
    }

    public ArrayList<Tag> getImageTagList() {
        return imageTagList;
    }

    public void setImageTagList(ArrayList<Tag> imageTagList) {
        this.imageTagList = imageTagList;
    }

    public ArrayList<Tag> getVoiceTagList() {
        return voiceTagList;
    }

    public void setVoiceTagList(ArrayList<Tag> voiceTagList) {
        this.voiceTagList = voiceTagList;
    }


    public static class Tag implements Serializable {
        private String description;
        private long id;

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        @Override
        public String toString() {
            return this.description != null ? this.description : "";
        }
    }
}
