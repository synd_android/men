package com.fcscs.android.mconnectv3;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.fcscs.android.mconnectv3.common.McConstants;
import com.fcscs.android.mconnectv3.common.ServiceExceptionHandler;
import com.fcscs.android.mconnectv3.common.util.PrefsUtil;

public class CheckLoginReceiver extends BaseCheckLoginReceiver {

    private Context ctx;
    private CheckLoginTask task;
    public static final int TYPE_CHECK_LOGIN = 3;

    public void setAlarm(Context context) {

        if (context == null) {
            return;
        }

        int interval = PrefsUtil.getJobPullingInterval(context);
        if (interval <= 0) {
            return;
        } else if (interval <= 20) {
            interval = 20;
        }

        Log.d(TAG, "check login timer interval=" + interval);
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, PullMsgReceiver.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, TYPE_CHECK_LOGIN, i, 0);
        am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 1000 * interval, pi);

    }

    void cancelAlarm(Context context) {

        if (context == null) {
            return;
        }

        int interval = PrefsUtil.getMsgPullingInterval(context);
        if (interval <= 0) {
            return;
        }

        Intent intent = new Intent(context, PullMsgReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, TYPE_CHECK_LOGIN, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if ("pull".equalsIgnoreCase(action)) {
            if (McConstants.OFFLINE_MODE) {
                return;
            }

            ctx = context;
            ServiceExceptionHandler.setHandler(ctx);

            Log.d(TAG, "repeat check login timer");
            if (task == null || AsyncTask.Status.RUNNING.equals(task.getStatus()) == false) {
                task = new CheckLoginTask(context, TYPE_CHECK_LOGIN);
                task.exec();
            }
        }
    }
}
