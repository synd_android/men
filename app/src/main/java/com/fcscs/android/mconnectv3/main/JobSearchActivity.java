package com.fcscs.android.mconnectv3.main;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.BaseActivity;
import com.fcscs.android.mconnectv3.common.McConstants;
import com.fcscs.android.mconnectv3.common.QRCodeParserFactory;
import com.fcscs.android.mconnectv3.common.McEnums.FilterType;
import com.fcscs.android.mconnectv3.common.McEnums.JobStatus;
import com.fcscs.android.mconnectv3.common.QRCodeParserFactory.LocationQRCodeParser;
import com.fcscs.android.mconnectv3.common.ui.HomeTopBar;
import com.fcscs.android.mconnectv3.common.ui.McDatePickerDialog;
import com.fcscs.android.mconnectv3.common.ui.McProgressAsyncTask;
import com.fcscs.android.mconnectv3.common.ui.RoundCornerListAdapter;
import com.fcscs.android.mconnectv3.common.util.DateTimeHelper;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.common.util.PrefsUtil;
import com.fcscs.android.mconnectv3.manager.model.JobModel;
import com.fcscs.android.mconnectv3.manager.model.StatusModel;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.SearchMyJobResponse;
import com.fcscs.android.mconnectv3.ws.econnect.ECService;
import com.fcscs.android.mconnectv3.zxing.CaptureActivity;

public class JobSearchActivity extends BaseActivity {


    private HomeTopBar homeTopBar;
    private EditText searchItemEt;
    private EditText dateEt;
    private Spinner filterSp;
    private Spinner filterTimeSp;
    private ListView statusLv;
    private TextView searchBtn;
    private LinearLayout llSpinnerTime;

    private List<StatusModel> statusModels = new ArrayList<StatusModel>();
    private JobStatus status;
    private static final int REQ_CODE_SEARCH_ITEMS = 2;
    private JobStatusListAdapter adapter;
    private SearchMyJobResponse jobSumResp;
    private String currFilterType;
    private Date date;

    private McDatePickerDialog datePickDialog;

    private Calendar cal = Calendar.getInstance(Locale.getDefault());
    private Button lastDay;
    private Button nextDay;

    private GetSummaryLoader task;
    ImageView qrButton;
    final static int QR_CODE = 1001;
    private int intPageIndex = 0;
    private boolean isEngineering = false;
    int[] searctTime;
    int timeSelect = 0;

    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.job_search);
        isEngineering = PrefsUtil.getEnableEng(this) == 1 ? true : false;
        homeTopBar = (HomeTopBar) findViewById(R.id.top_bar);
        homeTopBar.getTitleTv().setText(R.string.job_search);
        Resources r = getResources();
        searctTime = r.getIntArray(R.array.array_search_id);
        dateEt = (EditText) findViewById(R.id.et_date);

        dateEt.setText(DateTimeHelper.formatDateToStr(new Date(), DateTimeHelper.YYYY_MM_DD, ""));
        dateEt.setClickable(true);
        dateEt.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                popupDatePickDialog();
            }
        });
        qrButton = (ImageView) findViewById(R.id.qr_camera);
        qrButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(JobSearchActivity.this, CaptureActivity.class);
                intent.putExtra("type", QRCodeParserFactory.TYPE_NORMAL);
                startActivityForResult(intent, QR_CODE);
            }
        });
        qrButton.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                searchJob();
                return true;
            }
        });

        boolean enable = PrefsUtil.getQRCodeSearchJob(this);
        if (enable == false) {
            qrButton.setVisibility(View.GONE);
        }

        lastDay = (Button) findViewById(R.id.pre_day);
        lastDay.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                cal.add(Calendar.DAY_OF_YEAR, -1);
                dateEt.setText(DateTimeHelper.formatDateToStr(cal.getTime(), DateTimeHelper.YYYY_MM_DD, ""));
                if (dateEt.getText() != null) {
                    searchJob();
                }
            }
        });

        nextDay = (Button) findViewById(R.id.nxt_day);
        nextDay.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                cal.add(Calendar.DAY_OF_YEAR, 1);
                dateEt.setText(DateTimeHelper.formatDateToStr(cal.getTime(), DateTimeHelper.YYYY_MM_DD, ""));
                if (dateEt.getText() != null) {
                    searchJob();
                }
            }
        });
        llSpinnerTime = (LinearLayout) findViewById(R.id.job_search_ll_spinnertime);
        if (isEngineering) {
            llSpinnerTime.setVisibility(View.VISIBLE);
        }
        filterTimeSp = (Spinner) findViewById(R.id.jobSearch_searchType_time_Spinner);
//        if(isEngineering)
//        {
//        filterTimeSp.setVisibility(View.GONE);

        filterTimeSp.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                timeSelect = searctTime.length > i ? searctTime[i] : 0;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
//        }


        filterSp = (Spinner) findViewById(R.id.jobSearch_searchType_Spinner);
        filterSp.setPromptId(R.string.filter_by);
        final ArrayAdapter<String> filterAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item);
        filterAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        for (FilterType type : FilterType.values()) {
            if (!isEngineering && type.equals(FilterType.EQUIPMENTNO)) {
                continue;
            }
            filterAdapter.add(getResources().getString(type.getDispStr()));
        }
        filterSp.setAdapter(filterAdapter);
        filterSp.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                currFilterType = filterAdapter.getItem(pos);
                searchItemEt.setText(null);
                if (getString(FilterType.ROOMNO.getDispStr()).equals(currFilterType)) {
                    searchItemEt.setHint(R.string.input_room_no);
                } else if (getString(FilterType.GUESTNAME.getDispStr()).equals(currFilterType)) {
                    searchItemEt.setHint(R.string.input_guestName);
                } else if (getString(FilterType.ASSIGNEDBY.getDispStr()).equals(currFilterType)) {
                    searchItemEt.setHint(R.string.input_assignedBy);
                } else if (getString(FilterType.ASSIGNEDTO.getDispStr()).equals(currFilterType)) {
                    searchItemEt.setHint(R.string.input_assignedTo);
                } else if (getString(FilterType.LOCATION.getDispStr()).equals(currFilterType)) {
                    searchItemEt.setHint(R.string.input_location);
                } else if (getString(FilterType.JOBNO.getDispStr()).equals(currFilterType)) {
                    searchItemEt.setHint(R.string.jobno);
                } else if (getString(FilterType.EQUIPMENTNO.getDispStr()).equals(currFilterType)) {
                    searchItemEt.setHint(R.string.equipment_no);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        searchItemEt = (EditText) findViewById(R.id.jobSearch_searchItem_EditerText);
        searchItemEt.setOnKeyListener(new OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:
                            //searchJob();
                            intPageIndex = 1;
                            searchJob(intPageIndex);
                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        });

        statusLv = (ListView) findViewById(R.id.jobSearch_statusgridview);
        statusLv.setSelector(R.drawable.spacebar);
        statusLv.setClickable(true);
        statusLv.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {

                int total;
                total = statusModels.get(pos).getCount();
                status = statusModels.get(pos).getStatus();

                Date date = null;
                String dateVal = dateEt.getText().toString();
                if (McUtils.isNullOrEmpty(dateVal) == false) {
                    date = DateTimeHelper.parseDateStr(dateVal, DateTimeHelper.YYYY_MM_DD, new Date());
                }

                if (isEconnect()) {
                    findJobView(status, date, total);
                }
            }
        });
        adapter = new JobStatusListAdapter(JobSearchActivity.this, statusModels, R.layout.jobstatus_list_item);
        statusLv.setAdapter(adapter);

        searchBtn = (TextView) findViewById(R.id.job_search_searchBtn);
        searchBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isEconnect()) {
                    if (dateEt.getText() != null) {
                        searchJob();
                    }
                }
            }
        });

        if (dateEt.getText() != null) {
            searchJob();
        }

    }

    private synchronized void popupDatePickDialog() {

        if (datePickDialog != null && datePickDialog.isShowing()) {
            return;
        }
        Date currentTime = DateTimeHelper.getDateByTimeZone(cal.getTime());
        datePickDialog = new McDatePickerDialog(this, currentTime);
        datePickDialog.setPermanentTitle(getString(R.string.set_date));
        datePickDialog.setOnClickDone(new OnClickListener() {

            @Override
            public void onClick(View v) {
                DatePicker datePicker = datePickDialog.getDatePicker();
                cal.set(Calendar.YEAR, datePicker.getYear());
                cal.set(Calendar.MONTH, datePicker.getMonth());
                cal.set(Calendar.DAY_OF_MONTH, datePicker.getDayOfMonth());
                dateEt.setText(DateTimeHelper.formatDateToStr(cal.getTime(), DateTimeHelper.YYYY_MM_DD, ""));
                datePickDialog.dismiss();
            }
        });
        datePickDialog.show();
    }

    private void findJobView(JobStatus jobStatus, Date date, int total) {

        if (jobStatus == null) {
            return;
        }

        if (isEconnect()) {

            if (jobSumResp != null && jobSumResp.getJobViews().size() > 0) {

                final Intent si = new Intent(this, JobSummaryActivity.class);

                si.putExtra(JobSummaryActivity.DATE, date);
                si.putExtra(JobSummaryActivity.STATUS, jobStatus);
                ArrayList<JobModel> jobViewModels = new ArrayList<JobModel>();

                if (JobStatus.ALL.equals(jobStatus)) {
                    si.putExtra(JobSummaryActivity.TOTAL, jobSumResp.getTotal());
                    for (JobModel jobView : jobSumResp.getJobViews()) {
                        jobViewModels.add(jobView);
                    }
                } else {
                    int total1 = 0;
                    for (JobModel jobView : jobSumResp.getJobViews()) {
                        if (jobStatus.equals(jobView.getStatus())) {
                            jobViewModels.add(jobView);
                            total1++;
                        }
                    }
                    si.putExtra(JobSummaryActivity.TOTAL, total1);
                }

                if (jobViewModels != null && jobViewModels.size() > 0) {

                    si.putExtra(McConstants.KEY_RESULT, jobViewModels);
                    si.putExtra(JobDetailActivity.KEY_SEARCH, true);
                    startActivityForResult(si, REQ_CODE_SEARCH_ITEMS);

                } else {
                    toast(R.string.no_job_found);
                }

            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == REQ_CODE_SEARCH_ITEMS) {
            boolean change = data.getBooleanExtra("change", false);
            if (change) {
                if (task == null || AsyncTask.Status.RUNNING.equals(task.getStatus()) == false) {
                    task = new GetSummaryLoader(this);
                    task.exec();
                }
            }
        }

        if (resultCode == RESULT_OK && requestCode == QR_CODE) {
            LocationQRCodeParser parser = new LocationQRCodeParser();
            String qrcode = data.getStringExtra("result");
            if (parser.isCorrectFormat(qrcode)) {
                searchItemEt.setText(parser.room);
                searchJob();
            } else if (!TextUtils.isEmpty(qrcode)) {
                searchItemEt.setText(qrcode);
                searchJob();
            }
        }
    }

    private class JobStatusListAdapter extends RoundCornerListAdapter<StatusModel> {

        public JobStatusListAdapter(Context context, List<StatusModel> list, int layout) {
            super(context, list, layout);
        }

        @Override
        public void onBindView(View v, Context context, int position, StatusModel model) {

            TextView statusTv = (TextView) v.findViewById(R.id.jobstatus);
            TextView count = (TextView) v.findViewById(R.id.jobstatusCount);

            if (model.getStatus() != null) {
                statusTv.setText(model.getStatus().getResourceId());
            }
            StringBuffer sb = new StringBuffer();
            sb.append("(").append(model.getCount()).append(")");
            count.setText(sb.toString());
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            setResult(RESULT_OK);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    private class GetSummaryLoader extends McProgressAsyncTask {

        private int intPageIndex = 0;

        public GetSummaryLoader(Context context) {
            super(context);

            if (McUtils.isNullOrEmpty(dateEt.getText().toString()) == false) {
                date = DateTimeHelper.parseDateStr(dateEt.getText().toString(), DateTimeHelper.YYYY_MM_DD, (new Date()));
            }
        }

        //.. Create By	: Chua Kam Hoong
        public GetSummaryLoader(Context context, int PageIndex) {
            super(context);
            intPageIndex = PageIndex;
            if (McUtils.isNullOrEmpty(dateEt.getText().toString()) == false) {
                date = DateTimeHelper.parseDateStr(dateEt.getText().toString(), DateTimeHelper.YYYY_MM_DD, (new Date()));
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            for (StatusModel m : statusModels) {
                m.setCount(0);
            }
            adapter.notifyDataSetChanged();
        }

        @Override
        public void doInBackground() {
            String value = searchItemEt.getText().toString();
            String roomNum = null;
            String guestName = null;
            String assignedBy = null;
            String assignedTo = null;
            String location = null;
            String jobNo = null;
            String EquipmentNo = null;

            if (getString(FilterType.ROOMNO.getDispStr()).equals(currFilterType)) {
                roomNum = value;
            } else if (getString(FilterType.GUESTNAME.getDispStr()).equals(currFilterType)) {
                guestName = value;
            } else if (getString(FilterType.ASSIGNEDBY.getDispStr()).equals(currFilterType)) {
                assignedBy = value;
            } else if (getString(FilterType.ASSIGNEDTO.getDispStr()).equals(currFilterType)) {
                assignedTo = value;
            } else if (getString(FilterType.LOCATION.getDispStr()).equals(currFilterType)) {
                location = value;
            } else if (getString(FilterType.JOBNO.getDispStr()).equals(currFilterType)) {
                jobNo = value;
            } else if (getString(FilterType.EQUIPMENTNO.getDispStr()).equals(currFilterType)) {
                EquipmentNo = value;
            }
//            EquipmentNo = "CB_ELC-DIM-002-L06-2610";
            if (isEconnect()) {
                jobSumResp = ECService.getInstance().searchJobs(getCurrentContext(), intPageIndex, roomNum, guestName, assignedBy, assignedTo, location,
                        jobNo, date, EquipmentNo, true, timeSelect + "");
                if (jobSumResp != null) {
                    statusModels.clear();
                    statusModels.addAll(jobSumResp.getStatusModels());
                }
            }

        }

        @Override
        public void onPostExecute() {
            adapter.notifyDataSetChanged();
            hideSoftInput(searchItemEt);
        }
    }

    @Override
    public boolean isLoginActivity() {
        return true;
    }

    @Override
    public boolean isHomeActivity() {
        return false;
    }

    @SuppressWarnings("unused")
    private void searchJob() {

        String value = searchItemEt.getText().toString();
        String roomNum = null;
        String guestName = null;
        String assignedBy = null;
        String assignedTo = null;
        String location = null;

        if (getString(FilterType.ROOMNO.getDispStr()).equals(currFilterType)) {
            roomNum = value;
        } else if (getString(FilterType.GUESTNAME.getDispStr()).equals(currFilterType)) {
            guestName = value;
        } else if (getString(FilterType.ASSIGNEDBY.getDispStr()).equals(currFilterType)) {
            assignedBy = value;
        } else if (getString(FilterType.ASSIGNEDTO.getDispStr()).equals(currFilterType)) {
            assignedTo = value;
        } else if (getString(FilterType.LOCATION.getDispStr()).equals(currFilterType)) {
            location = value;
        }

        if (task == null || AsyncTask.Status.RUNNING.equals(task.getStatus()) == false) {
            task = new GetSummaryLoader(getCurrentContext());
            task.exec();
        }
    }

    /*
     *		Create By 	: Chua Kam Hoong
     *		Remarks		: Add support scroll paging.
     */
    @SuppressWarnings("unused")
    private void searchJob(int PageIndex) {

        int intPageIndex = PageIndex;
        String value = searchItemEt.getText().toString();
        String roomNum = null;
        String guestName = null;
        String assignedBy = null;
        String assignedTo = null;
        String location = null;

        if (getString(FilterType.ROOMNO.getDispStr()).equals(currFilterType)) {
            roomNum = value;
        } else if (getString(FilterType.GUESTNAME.getDispStr()).equals(currFilterType)) {
            guestName = value;
        } else if (getString(FilterType.ASSIGNEDBY.getDispStr()).equals(currFilterType)) {
            assignedBy = value;
        } else if (getString(FilterType.ASSIGNEDTO.getDispStr()).equals(currFilterType)) {
            assignedTo = value;
        } else if (getString(FilterType.LOCATION.getDispStr()).equals(currFilterType)) {
            location = value;
        }

        if (task == null || AsyncTask.Status.RUNNING.equals(task.getStatus()) == false) {
            task = new GetSummaryLoader(getCurrentContext(), intPageIndex);
            task.exec();
        }
    }

}
