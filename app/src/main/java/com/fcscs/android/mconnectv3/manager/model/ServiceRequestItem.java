package com.fcscs.android.mconnectv3.manager.model;

import java.io.File;
import java.io.Serializable;

/**
 * Created by FCS on 2014/6/19.
 */
public class ServiceRequestItem implements Serializable {

    private String name;
    private String itemId;

    private File capturedImage;
    private MediaLibraryConfig.Tag imageTag;

    private File recordedVoice;
    private MediaLibraryConfig.Tag voiceTag;

    private int intQuantity;
    private String remarks;

    public File getCapturedImage() {
        return capturedImage;
    }

    public void setCapturedImage(File capturedImage) {
        this.capturedImage = capturedImage;
    }

    public File getRecordedVoice() {
        return recordedVoice;
    }

    public void setRecordedVoice(File recordedVoice) {
        this.recordedVoice = recordedVoice;
    }

    public int getQuantity() {
        return intQuantity;
    }

    public void setQuantity(int quantity) {
        this.intQuantity = quantity;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    @Override
    public boolean equals(Object o) {
        if (o != null && o instanceof ServiceRequestItem && ((ServiceRequestItem) o).getItemId() != null) {
            if (((ServiceRequestItem) o).getItemId().equals(itemId)) {
                return true;
            }
        }
        return false;
    }

    public MediaLibraryConfig.Tag getImageTag() {
        return imageTag;
    }

    public void setImageTag(MediaLibraryConfig.Tag imageTag) {
        this.imageTag = imageTag;
    }

    public MediaLibraryConfig.Tag getVoiceTag() {
        return voiceTag;
    }

    public void setVoiceTag(MediaLibraryConfig.Tag voiceTag) {
        this.voiceTag = voiceTag;
    }


}
