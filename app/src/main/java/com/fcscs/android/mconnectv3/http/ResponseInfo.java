package com.fcscs.android.mconnectv3.http;

public interface ResponseInfo {

    int getResponseCode();

}