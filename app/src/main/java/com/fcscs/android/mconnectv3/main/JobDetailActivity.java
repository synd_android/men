package com.fcscs.android.mconnectv3.main;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.fcscs.android.mconnectv3.GCMNotification;
import com.fcscs.android.mconnectv3.McApplication;
import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.BaseActivity;
import com.fcscs.android.mconnectv3.common.McConstants;
import com.fcscs.android.mconnectv3.common.McEnums.JobStatus;
import com.fcscs.android.mconnectv3.common.SessionContext;
import com.fcscs.android.mconnectv3.common.ui.HomeTopBar;
import com.fcscs.android.mconnectv3.common.ui.ImagePagerLayout;
import com.fcscs.android.mconnectv3.common.ui.McBackgroundAsyncTask;
import com.fcscs.android.mconnectv3.common.ui.McDatePickerDialog;
import com.fcscs.android.mconnectv3.common.ui.McProgressAsyncTask;
import com.fcscs.android.mconnectv3.common.ui.McTimePickerDialog;
import com.fcscs.android.mconnectv3.common.ui.RecordPlayLayout;
import com.fcscs.android.mconnectv3.common.util.DateTimeHelper;
import com.fcscs.android.mconnectv3.common.util.DialogHelper;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.common.util.PrefsUtil;
import com.fcscs.android.mconnectv3.common.util.VibrateUtils;
import com.fcscs.android.mconnectv3.engineering.CheckListActivity;
import com.fcscs.android.mconnectv3.engineering.EnHomeActivity;
import com.fcscs.android.mconnectv3.engineering.EquipmentDetailActivity;
import com.fcscs.android.mconnectv3.engineering.PDFWebviewActivity;
import com.fcscs.android.mconnectv3.engineering.SignatureActivity;
import com.fcscs.android.mconnectv3.engineering.SignatureDialog;
import com.fcscs.android.mconnectv3.engineering.adapter.ConditionAdapter;
import com.fcscs.android.mconnectv3.engineering.adapter.RunnerAdapter;
import com.fcscs.android.mconnectv3.manager.model.JobModel;
import com.fcscs.android.mconnectv3.manager.model.MediaLibraryConfig;
import com.fcscs.android.mconnectv3.manager.model.PostUpdateEngineeringJobDetailsRequestJobDetails;
import com.fcscs.android.mconnectv3.manager.model.ServiceRequest;
import com.fcscs.android.mconnectv3.manager.model.ServiceRequestItem;
import com.fcscs.android.mconnectv3.ws.cconnect.GuestRequestService;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.SearchMyJobResponse;
import com.fcscs.android.mconnectv3.ws.econnect.ECService;
import com.fcscs.android.mconnectv3.ws.econnect.ECService.MediaWrapper;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.EngineeringRunnersDetails;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.GetEngineeringRunnersResponse;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.GetUpdateJobStatusResponse;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.PMCondition;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.UpdateableJobModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class JobDetailActivity extends BaseActivity {

    protected static final int RQ_TRANSLATE = 1009;
    private static final int RQ_CAPTURE_IMAGE = 1006;
    private static final int RQ_RECORD_VOICE = 1005;
    private static final int RQ_SIGNATURE = 1008;
    private static final String TAG = JobDetailActivity.class.getSimpleName();

    private TextView jobNoTv;
    private TextView deadlineTv;
    private TextView satusTv;
    private TextView priorityTv;
    private TextView escalationLevel;
    private EditText remarksEt;
    private ImageView btnTranslate;
    private TextView roomTv;
    private TextView serviceItemTv;
    private JobModel jm;
    private TextView startBtn;
    private TextView ackBtn;
    private TextView updateBtn;
    private TextView tvAccept;
    private TextView tvInspected;
    private TextView completedBtn;
    private TextView cancelBtn;
    private TextView ackByTv;
    private TextView completedByTv;
    private Button btnExtendDeadline;
    private Bundle bundle;
    private HomeTopBar topBar;

    private TextView acknowledgeTv;
    private TextView senderTv;
    private TextView sentTimeTv;
    private TextView completedTimeTv;
    private boolean autoAck;
    private boolean change = false;
    protected AckJob task;
    private CheckIsRunnerJob task1;
    protected CloseJob task2;
    private JobPhotos task3;
    protected UpdateJob task4;
    protected ReadNotification taskRead;
    private TextView requestByTv;
    private LinearLayout mLlRecordList;
    private ImagePagerLayout llImagePreview;
    private LinearLayout llAssign;
    private LinearLayout llCheckList;
    private LinearLayout llManhour;
    private LinearLayout llAckStatus;
    private LinearLayout llPmcondition;
    private Spinner spPmCondition;
    private LinearLayout llEquipment;
    private LinearLayout llEquipmentNo;
    private TextView tvEquipmentName;
    private TextView tvEquipmentNo;
    private ImageButton infoMsgRemark , infoMsgActionTaken;

    //    private EditText edJobAssign;
    private Spinner spManhours;
    private EditText edCost;
    private Button btEquipInfo;
    private Button btRunner;
    protected Spinner spRunner;

    private LinearLayout ll_detail_bottom;
    private ImageView ivCamera;
    private ImageView ivRecord;
    //    private ToggleButton tgJobcompleted;
    private EditText edName;
    private ImageButton edSignature;
    protected ServiceRequest request = null;

    private EditText edActionTaken;
    private ImageView btCheckList;
    private ImageView btTaskFile;
    private TextView tvAckStatus;
    private boolean isEngineering = false;
    public MediaWrapper wrapper;
    private RunnerListLoader runnerListLoader;
    private String urlPDF;//= "https://s3-ap-southeast-1.amazonaws.com/fcscconnect/1_FSDH/Report/Report/00f92e0e-1cdb-4875-a11f-4db2d0a18170.pdf";
    private final int[] SERVICE_TYPE_PM_JOB = {1, 2, 3, 4, 5};
    private final int SERVICE_TYPE_INTERDEPT_JOB = 7;
    private EngineeringRunnersDetails mAssignTo;
    private boolean isFromSearch = false;
    public static final String KEY_SEARCH = "FROM_SEARCH";
    File recordFile, capturedImageFile, signImage;
    PMCondition pmConditionSelected;
    String manHours;
    boolean isInspected = true;
    boolean isSupervisor;
    LinearLayout ll_detail_bottom_ack;
    public static final String KEY_PUSH_GCM = "KEY_PUSH_GCM";
    private LinearLayout llDeadline;
    private EditText edDate;
    private EditText edTime;
    private McDatePickerDialog datePickerDialog;
    private McTimePickerDialog timePickerDialog;
    protected Calendar deadlineCalendar = null;
    private final static int DELAY = 0; // Delay n mins;
    boolean isChangeDeadline = false;
    boolean isChangeAssign = false;
    private LinearLayout ll_detail_bottom_voice;
    private LinearLayout ll_detail_bottom_picture;
    private LinearLayout ll_job_detail_cost;
    private boolean isEnableCaptureImage = true;
    private boolean isEnableVoiceImage = true;

    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        if (SessionContext.getInstance().getUserType() != null && SessionContext.getInstance().getUserType().equals("S")) {
            isSupervisor = true;
        } else {
            isSupervisor = false;
        }
        setContentView(R.layout.job_detail);

        isEngineering = PrefsUtil.getEnableEng(this) == 1 ? true : false;
        bundle = getIntent().getExtras();
        autoAck = bundle.getBoolean(McConstants.KEY_AUTO_ACK_JOB, false);
        isFromSearch = bundle.getBoolean(KEY_SEARCH);
        if (bundle.get("JobModel") != null) {
            jm = (JobModel) bundle.get("JobModel");
        }
        if (bundle.get(McConstants.KEY_RESULT) != null) {
            jm = (JobModel) bundle.get(McConstants.KEY_RESULT);

        }
        ll_detail_bottom_ack = (LinearLayout) findViewById(R.id.ll_detail_bottom_ack);
        llDeadline = (LinearLayout) findViewById(R.id.ll_job_detail_deadline);
        edDate = (EditText) findViewById(R.id.commonWhenDate);
        edTime = (EditText) findViewById(R.id.commonWhenTime);
        ll_job_detail_cost = (LinearLayout) findViewById(R.id.ll_job_detail_cost);

        request = ServiceRequest.getInstance();
        ServiceRequest.getInstance().setItems(new ArrayList<ServiceRequestItem>());
        ServiceRequest.getInstance().setItem(new ServiceRequestItem());

        topBar = (HomeTopBar) findViewById(R.id.top_bar);
        ll_detail_bottom_voice = (LinearLayout) findViewById(R.id.ll_detail_bottom_voice);
        ll_detail_bottom_picture = (LinearLayout) findViewById(R.id.ll_detail_bottom_attach_picture);
        topBar.getTitleTv().setText(R.string.job_detail);
        jobNoTv = (TextView) findViewById(R.id.job_detail_jobNo);
        deadlineTv = (TextView) findViewById(R.id.job_detail_deadline);
        acknowledgeTv = (TextView) findViewById(R.id.acknowledgement_id);
        satusTv = (TextView) findViewById(R.id.job_detail_status);
        priorityTv = (TextView) findViewById(R.id.job_detail_priority);
        escalationLevel = (TextView) findViewById(R.id.job_detail_escalation_level);
        remarksEt = (EditText) findViewById(R.id.job_detail_remark);
        roomTv = (TextView) findViewById(R.id.job_detail_locatrequest);
        requestByTv = (TextView) findViewById(R.id.job_detail_request_by);
        serviceItemTv = (TextView) findViewById(R.id.job_detail_details);
        senderTv = (TextView) findViewById(R.id.job_detail_sender);
        sentTimeTv = (TextView) findViewById(R.id.job_detail_senttime);
        completedTimeTv = (TextView) findViewById(R.id.job_detail_completedtime);
        ackByTv = (TextView) findViewById(R.id.ack_by);
        completedByTv = (TextView) findViewById(R.id.completed_by);
        mLlRecordList = (LinearLayout) findViewById(R.id.ll_voice_list);
        llImagePreview = (ImagePagerLayout) findViewById(R.id.ll_image_preview);
        ackBtn = (TextView) findViewById(R.id.btn_ack_msg);
        updateBtn = (TextView) findViewById(R.id.btn_update_msg);
        completedBtn = (TextView) findViewById(R.id.btn_completed_msg);
        cancelBtn = (TextView) findViewById(R.id.btn_cancel_msg);
        startBtn = (TextView) findViewById(R.id.btn_start_msg);
        tvAccept = (TextView) findViewById(R.id.btn_accept_msg);
        tvInspected = (TextView) findViewById(R.id.btn_inspected);
        llAckStatus = (LinearLayout) findViewById(R.id.ll_ack_status);
        tvAckStatus = (TextView) findViewById(R.id.job_detail_ack_status);
        spManhours = (Spinner) findViewById(R.id.sp_job_detail_manhour);
        edCost = (EditText) findViewById(R.id.job_detail_edcost);
        llEquipment = (LinearLayout) findViewById(R.id.ll_equipment);
        tvEquipmentName = (TextView) findViewById(R.id.job_detail_equipmentName);
        llEquipmentNo = (LinearLayout) findViewById(R.id.ll_equipment_no);
        tvEquipmentNo = (TextView) findViewById(R.id.job_detail_equipmentNo);
        btnExtendDeadline = (Button) findViewById(R.id.btn_extend_deadline);
//        btEquipInfo = (Button) findViewById(R.id.job_detail_equipmentInfo);
        btEquipInfo = (Button) findViewById(R.id.job_detail_btnInfo);
//        btRunner = (Button)findViewById(R.id.job_detail_btn);
        btCheckList = (ImageView) findViewById(R.id.job_detail_btnChecklist);
        btTaskFile = (ImageView) findViewById(R.id.job_detail_btnTaskFile);
        spRunner = (Spinner) findViewById(R.id.sp_runner);

        llAssign = (LinearLayout) findViewById(R.id.job_detail_ll_assing);
        llCheckList = (LinearLayout) findViewById(R.id.job_detail_ll_checklist);
        llManhour = (LinearLayout) findViewById(R.id.job_detail_ll_manhours);
        edActionTaken = (EditText) findViewById(R.id.job_detail_actiontaken);

        // completion acknowledgement
        ll_detail_bottom = (LinearLayout) findViewById(R.id.ll_detail_bottom);
//        tgJobcompleted = (ToggleButton) findViewById(R.id.job_detail_tg_jobcompleted);
        edName = (EditText) findViewById(R.id.job_detail_ed_name);
        edSignature = (ImageButton) findViewById(R.id.job_detail_ed_esignature);
        ivRecord = (ImageView) findViewById(R.id.job_detail_iv_voiceMessage);
        ivCamera = (ImageView) findViewById(R.id.job_detail_iv_camera);
        llPmcondition = (LinearLayout) findViewById(R.id.job_detail_ll_pm_condition);
        spPmCondition = (Spinner) findViewById(R.id.sp_pm_condition);
        btnTranslate = (ImageView) findViewById(R.id.iv_translate);

        infoMsgRemark = (ImageButton) findViewById(R.id.info_msg_remark);
        infoMsgActionTaken = (ImageButton) findViewById(R.id.info_msg_action_taken);

        infoMsgRemark.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                toast(R.string.info_message);
            }
        });

        infoMsgActionTaken.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                toast(R.string.info_message);
            }
        });

        if (jm != null) {
            setContent(jm);
            setButtonVisibility(jm, false);
            new GetMediaLibraryConfig(getCurrentContext()).exec();

            if (bundle.get(KEY_PUSH_GCM) != null) {
                new LoadJobdetailAsync(getCurrentContext()).exec();
            } else {
                if (task1 == null || AsyncTask.Status.RUNNING.equals(task1.getStatus()) == false) {
                    task1 = new CheckIsRunnerJob(getCurrentContext(), jm);
                    task1.exec();
                }
            }

            if (JobStatus.PENDING.equals(jm.getStatus()) && autoAck) {

                if (task == null || AsyncTask.Status.RUNNING.equals(task.getStatus()) == false) {
                    task = new AckJob(this, jm);
                    task.exec();
                }
            }
        }

        if (PrefsUtil.isOfflineMode(this)) {
            llImagePreview.setDemoImage(getSupportFragmentManager());
            llImagePreview.setOnClickImage(new ImagePagerLayout.OnClickImageListener() {
                @Override
                public void onClick(View v, String imageFile) {
                    Intent i = new Intent(getCurrentContext(), ImagePreviewActivity.class);
                    i.putExtra("isDemo", true);
                    startActivity(i);
                }
            });
        } else {
            if (task3 == null || AsyncTask.Status.RUNNING.equals(task3.getStatus()) == false) {
                task3 = new JobPhotos(this, jm);
                task3.exec();
            }
        }


//        RecordPlayLayout llRecordPlay = new RecordPlayLayout(JobDetailActivity.this);
//        mLlRecordList.addView(llRecordPlay);
//        llRecordPlay.setDemoRecordFile();

        actionClick();
    }

    private void actionClick() {
        btTaskFile.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(urlPDF)) {
                    Intent intent = new Intent(JobDetailActivity.this, PDFWebviewActivity.class);
                    intent.putExtra(PDFWebviewActivity.KEY_PATH_FILE, urlPDF);
                    startActivity(intent);
                }
            }
        });
        btCheckList.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(JobDetailActivity.this, CheckListActivity.class);
                intent.putExtra(CheckListActivity.KEY_JOBMODEL, jm);
                startActivity(intent);
            }
        });
        btEquipInfo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(JobDetailActivity.this, EquipmentDetailActivity.class);
                intent.putExtra(EquipmentDetailActivity.KEY_JOBMODEL, jm);
                startActivity(intent);
            }
        });
        ivCamera.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(JobDetailActivity.this, ImageCaptureActivity.class);
                JobDetailActivity.this.startActivityForResult(i, RQ_CAPTURE_IMAGE);
            }
        });
        ivRecord.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (request.isItemSelected() == false) {
                    toast(getString(R.string.please_select_serviceitem));
                    serviceItemTv.requestFocus();
                } else {
                    Intent i = new Intent(JobDetailActivity.this, VoiceTagActivy.class);
                    JobDetailActivity.this.startActivityForResult(i, RQ_RECORD_VOICE);
                }
            }
        });
        edSignature.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                signature();
            }
        });

        tvAccept.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                new UpdateAcceptJob(JobDetailActivity.this).execute();
            }
        });

        ackBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (task == null || AsyncTask.Status.RUNNING.equals(task.getStatus()) == false) {
                    if (jm.getNotificationType() == 3 || jm.getStatus() == JobStatus.CANCELLED) {
                        taskRead = new ReadNotification(getCurrentContext(), jm);
                        taskRead.exec();
                    } else {
                        task = new AckJob(getCurrentContext(), jm);
                        task.exec();
                    }
                }
            }
        });
        btnTranslate.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(JobDetailActivity.this, TranslateActivity.class);
                if (remarksEt != null && remarksEt.getText().toString().length() > 0) {
                    i.putExtra("TEXT_TRANSLATE", remarksEt.getText().toString().trim());
                }
                JobDetailActivity.this.startActivityForResult(i, RQ_TRANSLATE);
            }
        });

        updateBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean updateRunner = false;
                if (isChangeAssign && !JobStatus.COMPLETED.equals(jm.getStatus()) && spRunner.getAdapter() != null) {
                    EngineeringRunnersDetails item = (EngineeringRunnersDetails) spRunner.getSelectedItem();
                    if (item != null) {
                        updateRunner = true;
                        new UpdateRunner(JobDetailActivity.this, item, ((RunnerAdapter) spRunner.getAdapter()).getList()).execute();
                    }
                }

                if (!updateRunner) {
                    String remark = remarksEt.getText().toString();

                    if (task4 == null || AsyncTask.Status.RUNNING.equals(task4.getStatus()) == false) {
                        task4 = new UpdateJob(getCurrentContext(), jm, remark, false);
                        task4.exec();
                    }
                }
            }
        });

        completedBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (task2 == null || AsyncTask.Status.RUNNING.equals(task2.getStatus()) == false) {
                    task2 = new CloseJob(getCurrentContext(), jm);
                    task2.exec();
                }
            }
        });

        cancelBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String remark = remarksEt.getText().toString();

                if (task4 == null || AsyncTask.Status.RUNNING.equals(task4.getStatus()) == false) {
                    task4 = new UpdateJob(getCurrentContext(), jm, remark, true);
                    task4.exec();
                }
            }
        });

        btnExtendDeadline.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                isChangeDeadline = true;
                popupDatePickDialog(true);
            }
        });

        tvInspected.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogConfirm();
            }
        });
        edDate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                isChangeDeadline = true;
                popupDatePickDialog(false);
            }
        });
        edTime.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                isChangeDeadline = true;
                popupTimePickDialog();
            }
        });
    }

    private synchronized void popupDatePickDialog(final boolean nextPickTime) {
        if (datePickerDialog != null && datePickerDialog.isShowing()) {
            return;
        }
        initDeadlineCalendar();

        datePickerDialog = new McDatePickerDialog(this, deadlineCalendar.getTime());
        datePickerDialog.setPermanentTitle(getString(R.string.set_date));
        datePickerDialog.setOnClickDone(new OnClickListener() {

            @Override
            public void onClick(View v) {
                DatePicker datePicker = datePickerDialog.getDatePicker();
                deadlineCalendar.set(Calendar.YEAR, datePicker.getYear());
                deadlineCalendar.set(Calendar.MONTH, datePicker.getMonth());
                deadlineCalendar.set(Calendar.DAY_OF_MONTH, datePicker.getDayOfMonth());
                deadlineCalendar.set(Calendar.HOUR_OF_DAY, deadlineCalendar.getTime().getHours());
                deadlineCalendar.set(Calendar.MINUTE, deadlineCalendar.getTime().getMinutes() + DELAY);
                datePickerDialog.dismiss();
                updateLabel();
                if (nextPickTime) {
                    popupTimePickDialog();
                }
            }
        });
        datePickerDialog.show();
    }

    private void updateLabel() {
        edDate.setText(DateTimeHelper.DATE_FORMATTER4.format(deadlineCalendar.getTime()));
        edTime.setText(DateTimeHelper.TIME_FORMATTER.format(deadlineCalendar.getTime()));

        if (btnExtendDeadline.isShown()) {
            deadlineTv.setText(McUtils.formatDateTime(deadlineCalendar.getTime()));
        }
    }

    private void initDeadlineCalendar() {
        if (deadlineCalendar == null) {
            deadlineCalendar = Calendar.getInstance(Locale.getDefault());
            deadlineCalendar.setTime(jm.getDeadline());
        }
    }

    private synchronized void popupTimePickDialog() {
        if (timePickerDialog != null && timePickerDialog.isShowing()) {
            return;
        }
        initDeadlineCalendar();

        timePickerDialog = new McTimePickerDialog(this, deadlineCalendar.getTime(), true);
        timePickerDialog.setOnClickDone(new OnClickListener() {

            @Override
            public void onClick(View v) {
                TimePicker picker = timePickerDialog.getTimePicker();
                picker.clearFocus();
                deadlineCalendar.set(Calendar.HOUR_OF_DAY, picker.getCurrentHour());
                deadlineCalendar.set(Calendar.MINUTE, picker.getCurrentMinute() + DELAY);
                timePickerDialog.dismiss();

                if (btnExtendDeadline.isShown()) {
                    deadlineTv.setTextColor(Color.YELLOW);
                }
                updateLabel();
            }
        });
        timePickerDialog.show();
    }

    private void showDialogConfirm() {
        DialogInterface.OnClickListener yes = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (jm.getIsInspected() != 1) {
                    new InspectedLoader(JobDetailActivity.this).execute();
                    goBackToHomeScreen(2);
//                    finish();
                } else {
                    new FailInspectionLoader(JobDetailActivity.this).execute();
                }
            }
        };
        DialogInterface.OnClickListener no = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
//                goBack();
//                finish();
            }
        };
        if (jm.getIsInspected() != 1) {
            DialogHelper.showYesNoDialog(this, R.string.inspection_confirmation, R.string.confirm_ask, yes, no);
        } else {
            DialogHelper.showYesNoDialog(this, R.string.fail_inspection, R.string.confirm_ask, yes, no);
        }
    }

    protected void initTemplateSpinner(Spinner spTemplate) {
        if (runnerListLoader == null || AsyncTask.Status.RUNNING.equals(runnerListLoader.getStatus()) == false) {
            runnerListLoader = new RunnerListLoader(getCurrentContext(), spTemplate);
            runnerListLoader.exec();
        }
    }

    class InspectedLoader extends McProgressAsyncTask {
        public InspectedLoader(Context context) {
            super(context);
        }

        boolean res;

        @Override
        public void doInBackground() {
            res = ECService.getInstance().updateInspectedJob(jm.getJobId() + "", "1", jm.getServiceTypeID() + "");
        }

        @Override
        public void onPostExecute() {
            if (res) {
                jm.setIsInspected(1);
                tvInspected.setText(getString(R.string.fail_inspection));
            }
        }
    }

    class FailInspectionLoader extends McProgressAsyncTask {
        public FailInspectionLoader(Context context) {
            super(context);
        }

        boolean isChange = false;


        boolean res;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (isChangeDeadline) {
                isChange = true;
            }
            if (!isChangeDeadline && !isChangeAssign && jm.getServiceTypeID() == 7) {
                this.cancel(true);
                dismisProgress();
                DialogHelper.showAlertDialog(getCurrentContext(), R.string.input_deadline_assign, null);
                return;
            }
            if (Arrays.binarySearch(SERVICE_TYPE_PM_JOB, jm.getServiceTypeID()) >= 0 && !isChangeDeadline) {
                this.cancel(true);
                dismisProgress();
                DialogHelper.showAlertDialog(getCurrentContext(), R.string.input_deadline_assign, null);
                return;
            }
            if (McUtils.isNullOrEmpty(edDate.getEditableText().toString()) == false) {
                da = deadlineCalendar.getTime();
            }
            if (isChangeAssign) {
                EngineeringRunnersDetails item = (EngineeringRunnersDetails) spRunner.getSelectedItem();
                if (item != null) {
                    assignedToID = item.getRunnerUserID() + "";
                    assignedToName = item.getRunnerUserName();
                    assignedToDepartment = item.getRunnerDepartment();
                }
            } else if (mAssignTo != null) {
                assignedToID = mAssignTo.getRunnerUserID() + "";
                assignedToName = mAssignTo.getRunnerUserName();
                assignedToDepartment = mAssignTo.getRunnerDepartment();
            }else if(mAssignTo == null){
                toast(R.string.no_free_runner);
            }
            serviceItemCode = jm.getServiceItemCode();
            locationCode = jm.getLocationCode();
            assignedByID = SessionContext.getInstance().getFcsUserId() + "";
        }

        String serviceItemCode;
        String locationCode;
        String assignedByID;
        String assignedToID = "";
        String assignedToName = "";
        String assignedToDepartment = "";
        Date da = null;

        @Override
        public void doInBackground() {
            if (isChangeDeadline) {

                res = ECService.getInstance().updateReAssignFailedInspectionJob(jm.getJobId() + "", jm.getServiceTypeID() + "", serviceItemCode,
                        locationCode, assignedByID, assignedToID, assignedToName, assignedToDepartment, da);
            } else {
                res = ECService.getInstance().updateInspectedJob(jm.getJobId() + "", "1", jm.getServiceTypeID() + "");
            }
        }

        @Override
        public void onPostExecute() {
            if (res) {
                jm.setIsInspected(1);
                if (isChangeDeadline && deadlineCalendar != null) {
                    jm.setDeadline(deadlineCalendar.getTime());
                }
                // change to mode ure
                goBackToHomeScreen(2);
//                finish();
            }
        }
    }

    class RunnerListLoader extends McBackgroundAsyncTask {

        public RunnerListLoader(Context context, Spinner spTemplate) {
            super(context);
            this.spRunner = spTemplate;
            this.engineeringDepartmentID = PrefsUtil.getEnDerpId(context);
        }

        List<EngineeringRunnersDetails> mList;
        int engineeringDepartmentID;
        protected GetEngineeringRunnersResponse res;
        protected RunnerAdapter adapter;
        protected Spinner spRunner;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        public void doInBackground() {
            if (Arrays.binarySearch(SERVICE_TYPE_PM_JOB, jm.getServiceTypeID()) >= 0) {
                res = ECService.getInstance().GetEngineeringRunners(getCurrentContext(), engineeringDepartmentID + "", jm.getJobId() + "");
            } else if (jm.getServiceTypeID() == 7) {
                String ServiceItemCode = jm.getServiceItemCode();
                String LocationCode = jm.getLocationCode();
                res = ECService.getInstance().getFreeRunners(getCurrentContext(), ServiceItemCode, LocationCode);
            }
        }

        @Override
        public void onPostExecute() {
            if (res != null) {
                final List<EngineeringRunnersDetails> temps = res.getList();
                if (temps != null && temps.size() > 0) {
                    int pos = 0;
                    for (int i = 0; i < temps.size(); i++) {
                        EngineeringRunnersDetails item = temps.get(i);
                        if (item.getRunnerUserFullName().equals(jm.getAssignTo())) {
                            pos = i;
                            mAssignTo = item;
                        }
                    }
                    if(mAssignTo == null ){
                        EngineeringRunnersDetails item = temps.get(0);
                        mAssignTo = item;
                    }
                    adapter = new RunnerAdapter(temps, JobDetailActivity.this);
                    spRunner.setAdapter(adapter);
                    spRunner.setSelection(pos);
                    spRunner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            spRunner.setSelection(i);
                            if (spRunner.getSelectedItem() != null) {
                                EngineeringRunnersDetails item = (EngineeringRunnersDetails) spRunner.getSelectedItem();
                                if (null != item && mAssignTo != null && (item.getRunnerUserID() != mAssignTo.getRunnerUserID())) {
                                    isChangeAssign = true;
                                }
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                    if (getSessionCtx().getReAssignRunner() == 1) {
                        spRunner.setEnabled(true);
                    } else {
                        spRunner.setEnabled(false);
                    }

                }
            }
        }
    }

    private class UpdateRunner extends McProgressAsyncTask {
        EngineeringRunnersDetails assignBy;
        EngineeringRunnersDetails assignTo;
        List<EngineeringRunnersDetails> mList;
        Context context;
        boolean result = false;
        private int oldPos;

        public UpdateRunner(Context context, EngineeringRunnersDetails assignTo, List<EngineeringRunnersDetails> list) {
            super(context);
            this.assignBy = new EngineeringRunnersDetails();
            this.assignBy.setRunnerUserID(SessionContext.getInstance().getFcsUserId());
            this.assignBy.setRunnerUserName(SessionContext.getInstance().getUsername());
            this.assignBy.setRunnerUserFullName(SessionContext.getInstance().getName());
            this.assignBy.setRunnerDepartment(SessionContext.getInstance().getUserDepartment());
            this.assignTo = assignTo;
            this.context = context;
            this.mList = list;
        }

        @Override
        public void doInBackground() {
            result = ECService.getInstance().updateReAssignRunner(context, jm.getJobId() + "", assignBy, assignTo);
            if (result) {
                mAssignTo = assignTo;
            } else {
                for (int i = 0; i < mList.size(); i++) {
                    EngineeringRunnersDetails item = mList.get(i);
                    if (item.getRunnerUserID() == mAssignTo.getRunnerUserID()) {
                        oldPos = i;
                    }
                }
            }
        }

        @Override
        public void onPostExecute() {
            if (!result) {
                spRunner.setSelection(oldPos);
            } else {
                jm.setAssignTo(mAssignTo.getRunnerUserFullName());

                String remark = remarksEt.getText().toString();

                if (task4 == null || AsyncTask.Status.RUNNING.equals(task4.getStatus()) == false) {
                    task4 = new UpdateJob(getCurrentContext(), jm, remark, false);
                    task4.exec();
                }
            }
        }
    }

    public static Intent createIntent(Context ctx, JobModel model) {
        Intent intent = new Intent(ctx, JobDetailActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(McConstants.KEY_RESULT, model);
        return intent;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        McApplication.setCachedPicture(null);
    }

    private void setContent(JobModel model) {

        String room = getString(R.string.room) + " " + model.getRoomNum();
        roomTv.setText(room);

        String requestor = getString(R.string.requested_by) + " ";
        if (model.getGuestName() != null && model.getGuestName().trim().length() > 0) {
            requestor += model.getGuestName();
        } else if (model.getReportedBy() != null && model.getReportedBy().trim().length() > 0) {
            requestor += model.getReportedBy();
        }
        requestByTv.setText(requestor);

        serviceItemTv.setText(model.getServiceItemName() + " * " + model.getQuantity());
        jobNoTv.setText(model.getJobNum());
        deadlineTv.setText(McUtils.formatDateTime(model.getDeadline()));
        priorityTv.setText(model.getPriorityName());
        escalationLevel.setText(Integer.toString(model.getEscalatedLevel()));
        sentTimeTv.setText(McUtils.formatDateTime(model.getRequestedDate()));
        senderTv.setText(model.getRequestorName());
        completedTimeTv.setText(McUtils.formatDateTime(model.getCompletedTime()));
        ackByTv.setText(model.getAcknowledgedBy());
        completedByTv.setText(model.getClosedByName());
        if (model.getNotificationType() == 3 || model.getStatus() == JobStatus.CANCELLED) {
            ackBtn.setText(R.string.read);
        }
        ServiceRequestItem item = new ServiceRequestItem();
        item.setItemId(model.getServiceTypeID() + "");
        item.setName(model.getServiceItemName());
        request.setItem(item);
        if (isEngineering) {
            String ack_status = "";
            if (model.getIsAcked() != null && model.getIsAcked()) {
                ack_status = getString(R.string.ack);
            } else {
                ack_status = getString(R.string.not_ack);
            }
            tvAckStatus.setText(ack_status);
            new loadManhours(JobDetailActivity.this, model).execute();
            edCost.setText(model.getCost());
            tvEquipmentName.setText(model.getEquipmentName());
            tvEquipmentNo.setText(model.getEquipmentNo());
            btTaskFile.setImageResource(R.drawable.ic_list_task);
            edActionTaken.setText(model.getActionTaken());
            new GetEngineeringTaskFile(JobDetailActivity.this, jm.getJobId() + "").execute();
            initTemplateSpinner(spRunner);
            new LoadListcondition(JobDetailActivity.this).execute();
            if (!isFromSearch) {
            } else {
                tvEquipmentName.setText(model.getEquipmentName());
                tvEquipmentNo.setText(model.getEquipmentNo());


            }
        } else {
        }
        if (jm.getServiceTypeID() == 7) {
            initTemplateSpinner(spRunner);
        }


    }

    private class loadManhours extends McProgressAsyncTask {
        JobModel jobModel;

        public loadManhours(Context context, JobModel model) {
            super(context);
            spinnerArray = new ArrayList<String>();
            jobModel = model;
        }

        List<String> spinnerArray;

        @Override
        public void doInBackground() {

            for (int i = 0; i <= 10; i++) {
//                if (i == 0) {
//                    spinnerArray.add((i + 0.5) + "");
//                } else
                if (i == 10) {
                    spinnerArray.add(i + "");
                } else {
                    spinnerArray.add(i + "");
                    spinnerArray.add((i + 0.5) + "");
                }

            }

        }

        @Override
        public void onPostExecute() {
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                    JobDetailActivity.this, android.R.layout.simple_spinner_item, spinnerArray);

            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spManhours.setAdapter(adapter);
            spManhours.setSelection(0);
            for (int i = 0; i < spinnerArray.size(); i++) {
                if (jobModel.getManHours().equals(spinnerArray.get(i))) {
                    spManhours.setSelection(i);
                }
            }
            spManhours.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    manHours = spinnerArray.get(i);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }
    }

    private class LoadListcondition extends McProgressAsyncTask {
        public LoadListcondition(Context context) {
            super(context);
        }

        ConditionAdapter adapter;
        List<PMCondition> listPMCondition = new ArrayList<>();

        @Override
        public void doInBackground() {
            String listJson = PrefsUtil.getConditionList(JobDetailActivity.this);
            Type listType = new TypeToken<List<PMCondition>>() {
            }.getType();
            listPMCondition = new Gson().fromJson(listJson, listType);
        }

        @Override
        public void onPostExecute() {
            if (listPMCondition != null && listPMCondition.size() > 0) {
                adapter = new ConditionAdapter(listPMCondition, JobDetailActivity.this);
                spPmCondition.setAdapter(adapter);
                spPmCondition.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        pmConditionSelected = listPMCondition.get(i);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
            }
        }
    }

    private void signature() {

        new SignatureDialog()
                .show(this, new SignatureDialog.SignatureEventListener() {

                    @Override
                    public void onSignatureEntered(File savedFile) {
                        if (savedFile != null) {
                            File file = new File(savedFile.getParentFile(), System.currentTimeMillis() + savedFile.getName() + ".png");
                            savedFile.renameTo(file);
                            signImage = file;
                            Bitmap bitmap = BitmapFactory.decodeFile(signImage.getPath());
                            edSignature.setImageBitmap(bitmap);
                            edSignature.setBackgroundResource(android.R.color.transparent);
                        }
                    }

                    @Override
                    public void onSignatureInputCanceled() {
                    }

                    @Override
                    public void onSignatureInputError(Throwable e) {
                    }
                });
    }

    private class UpdateAcceptJob extends McBackgroundAsyncTask {
        Context context;
        String assignedToID;
        String assignedToName;
        String assignedToDepartment;
        boolean isResult = false;

        public UpdateAcceptJob(Context context) {
            super(context);
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            SessionContext.getInstance();
            assignedToID = SessionContext.getInstance().getFcsUserId() + "";
            assignedToName = SessionContext.getInstance().getUsername() + "";
            assignedToDepartment = SessionContext.getInstance().getUserDepartment() + "";
        }

        @Override
        public void doInBackground() {
            isResult = ECService.getInstance().updateAcceptJob(context, jm.getJobId() + "", assignedToID, assignedToName, assignedToDepartment);
//            isResult= ECService.getInstance().updateAcceptJob(context,jm.getJobId()+"",66+"","runner 15", assignedToDepartment);
        }

        @Override
        public void onPostExecute() {
            if (isResult) {
                JobStatus status = JobStatus.getJobStatusFromStatusId(1);//pending job
                jm.setStatus(status);
                goBack();
                finish();
            }
        }
    }

    private class LoadJobdetailAsync extends McProgressAsyncTask {
        public LoadJobdetailAsync(Context context) {
            super(context);
        }

        SearchMyJobResponse res;

        @Override
        public void doInBackground() {
            if (deadlineCalendar == null) {
                initDeadlineCalendar();
            }
            Date da = deadlineCalendar.getTime();
            res = ECService.getInstance().searchJobs(getCurrentContext(), 0, "", "", "", "", "", jm.getJobNum() + "", da, "", true, "");
        }

        @Override
        public void onPostExecute() {
            if (res != null && res.getJobViews().get(0) != null) {
                jm = res.getJobViews().get(0);

                setContent(jm);
                setButtonVisibility(jm, false);

                if (task1 == null || AsyncTask.Status.RUNNING.equals(task1.getStatus()) == false) {
                    task1 = new CheckIsRunnerJob(getCurrentContext(), jm);
                    task1.exec();
                }
                if (task3 == null || AsyncTask.Status.RUNNING.equals(task3.getStatus()) == false) {
                    task3 = new JobPhotos(getCurrentContext(), jm);
                    task3.exec();
                }
            }
        }
    }

    private class GetEngineeringTaskFile extends McBackgroundAsyncTask {
        String url;
        String mJobId;
        Context mContext;

        public GetEngineeringTaskFile(Context context, String jobId) {
            super(context);
            mContext = context;
            mJobId = jobId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            btTaskFile.setImageResource(R.drawable.ic_list_task_black);
        }

        @Override
        public void doInBackground() {
            url = ECService.getInstance().GetEngineeringTaskFile(mContext, mJobId);
        }

        @Override
        public void onPostExecute() {
            if (!TextUtils.isEmpty(url)) {
                urlPDF = url;
                btTaskFile.setImageResource(R.drawable.ic_list_task);
            } else {
                btTaskFile.setImageResource(R.drawable.ic_list_task_black);
            }
        }
    }

    private class openTaskFile extends McProgressAsyncTask {
        public String mUrl;
        private Context mContext;
        private boolean isExits = false;

        public openTaskFile(Context context, String url) {
            super(context);
            this.mUrl = url;
            this.mContext = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (McUtils.fileExists(mContext, mUrl)) {
                isExits = true;
            }
        }

        @Override
        public void doInBackground() {
            if (isExits)
                return;
            File pdfFile = McUtils.getPDFFileByUrl(mContext, mUrl);

            try {
                pdfFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            McUtils.downloadFile(mUrl, pdfFile);
            return;


        }


        @Override
        public void onPostExecute() {
            Intent intent = new Intent(JobDetailActivity.this, PDFWebviewActivity.class);
            intent.putExtra(PDFWebviewActivity.KEY_PATH_FILE, mUrl);
            startActivity(intent);
        }
    }

    private class AckJob extends McProgressAsyncTask {

        JobModel m;
        private Boolean res;

        public AckJob(Context context, JobModel m) {
            super(context);
            this.m = m;
        }

        @Override
        public void doInBackground() {
            if (isEngineering && Arrays.binarySearch(SERVICE_TYPE_PM_JOB, jm.getServiceTypeID()) >= 0) {
                if (isCConnect()) {
                    res = GuestRequestService.getInstance().updateJobStatus(getCurrentContext(), m.getJobId(), m.getStatus(), true,
                            m.getRemarks());
                } else {
                    res = ECService.getInstance().ackEngineeringJobs(getCurrentContext(), Long.toString(m.getJobId()));
                }
            } else {
                if (isCConnect()) {
                    res = GuestRequestService.getInstance().updateJobStatus(getCurrentContext(), m.getJobId(), m.getStatus(), true,
                            m.getRemarks());
                } else {
                    res = ECService.getInstance().ackJob(getCurrentContext(), Long.toString(m.getJobId()));
                }
            }
        }

        @Override
        public void onPostExecute() {
            if (res != null && res) {
                toast(R.string.acknowledge_job_successfully);
                VibrateUtils.vibrate(getCurrentContext());
                m.setIsAcked(true);
                m.setAcknowledgedBy(SessionContext.getInstance().getUsername());
                setButtonVisibility(m, true);
                GCMNotification.cancelECJobNotification(getCurrentContext(), "" + m.getJobId());
                if (!autoAck) {
                    goBack();
                }

            } else {
                toast(R.string.fail_to_acknowledge_job);
            }
        }

    }

    private class ReadNotification extends McProgressAsyncTask {

        JobModel m;
        private Boolean res;

        public ReadNotification(Context context, JobModel m) {
            super(context);
            this.m = m;
        }

        @Override
        public void doInBackground() {
            if (isCConnect()) {
                res = GuestRequestService.getInstance().updateJobStatus(getCurrentContext(), m.getJobId(), m.getStatus(), true,
                        m.getRemarks());
            } else {
                res = ECService.getInstance().postUpdateNotificationStatus(getCurrentContext(), m.getJobNum(), 3, 1);
            }
        }

        @Override
        public void onPostExecute() {
            if (res != null && res) {
                toast(R.string.read_job_successfully);
                VibrateUtils.vibrate(getCurrentContext());
                GCMNotification.cancelECJobNotification(getCurrentContext(), "" + m.getJobId());
                jm.setIsRead(true);
                goBack();
            } else {
                toast(R.string.fail_to_read_job);
            }
        }

    }

    private class UpdateJob extends McProgressAsyncTask {

        JobModel m;
        private Boolean res;
        private String remarks;
        private boolean isCancel = false;

        public UpdateJob(Context context, JobModel m, String remarks, boolean isCancel) {
            super(context);
            this.m = m;
            this.remarks = remarks;
            this.isCancel = isCancel;
        }

        @Override
        public void doInBackground() {
            if (isEngineering && Arrays.binarySearch(SERVICE_TYPE_PM_JOB, jm.getServiceTypeID()) >= 0) {
                if (isCConnect()) {
                    res = GuestRequestService.getInstance().updateJobStatus(getCurrentContext(), m.getJobId(), m.getStatus(), true, remarks);
                } else {
                    if (isCancel) {
                        res = ECService.getInstance().updateEngineeringJob(getCurrentContext(), m.getJobId(), "Cancelled",true);

                        if (res != null && res) {
                            jm.setStatus(JobStatus.CANCELLED);
                        }

                    } else {
                        res = ECService.getInstance().updateEngineeringJobRemark(getCurrentContext(), m.getJobId(), remarks);
                        if (res != null && res) {
                            PostUpdateEngineeringJobDetailsRequestJobDetails detail = new PostUpdateEngineeringJobDetailsRequestJobDetails();
                            detail.setJobNo(m.getJobNum());
                            detail.setActionTaken(edActionTaken.getText().toString());
                            detail.setEquipmentID(m.getEquipmentNo());
                            if (pmConditionSelected != null) {
                                detail.setPmConditionID(pmConditionSelected.getConditionID() + "");
                            }
                            detail.setManHours(manHours);
                            detail.setCost(edCost.getText().toString());
                            if (deadlineCalendar != null) {
                                detail.setDeadline(deadlineCalendar.getTime());
                            }

                            List<PostUpdateEngineeringJobDetailsRequestJobDetails> _list = new ArrayList<>();
                            _list.add(detail);
                            res = ECService.getInstance().postUpdateEngineeringJobDetails(JobDetailActivity.this, _list);
                        }
                    }
                }
            } else {
                if (isCConnect()) {
                    res = GuestRequestService.getInstance().updateJobStatus(getCurrentContext(), m.getJobId(), m.getStatus(), true, remarks);
                } else {
                    if (isCancel) {
                        res = ECService.getInstance().updateEngineeringJob(getCurrentContext(), m.getJobId(), "Cancelled",false);

                        if (res != null && res) {
                            jm.setStatus(JobStatus.CANCELLED);
                        }

                    } else {
                        res = ECService.getInstance().updateJobRemark(getCurrentContext(), m.getJobId(), remarks);
                        if (res != null && res) {
                            PostUpdateEngineeringJobDetailsRequestJobDetails detail = new PostUpdateEngineeringJobDetailsRequestJobDetails();
                            detail.setJobNo(m.getJobNum());
                            detail.setActionTaken(edActionTaken.getText().toString());
                            detail.setEquipmentID(m.getEquipmentNo());
                            if (pmConditionSelected != null) {
                                detail.setPmConditionID(pmConditionSelected.getConditionID() + "");
                            }
                            detail.setManHours(manHours);
                            detail.setCost(edCost.getText().toString());
                            if (deadlineCalendar != null) {
                                detail.setDeadline(deadlineCalendar.getTime());
                            }

                            List<PostUpdateEngineeringJobDetailsRequestJobDetails> _list = new ArrayList<>();
                            _list.add(detail);
                            res = ECService.getInstance().PostUpdateJobDetails(JobDetailActivity.this, _list);
                        }
                    }
                }
            }
        }

        @Override
        public void onPostExecute() {
            if (res != null && res) {
                toast(getString(R.string.update_successfully));
                VibrateUtils.vibrate(getCurrentContext());
                m.setRemarks(remarks);
                m.setActionTaken(edActionTaken.getText().toString());
                if (deadlineCalendar != null) {
                    m.setDeadline(deadlineCalendar.getTime());
                }
                setButtonVisibility(m, true);
                change = true;
                if (recordFile != null || capturedImageFile != null) {
                    postMedia();
                } else {
                    goBack();
                }
            } else {
                toast(getString(R.string.update_fail));
            }
        }

    }

    class JobPhotos extends McBackgroundAsyncTask {

        private JobModel m;

        public JobPhotos(Context context, JobModel m) {
            super(context);
            this.m = m;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        public void doInBackground() {
            if (m != null) {
                boolean isPMJob = (isEngineering && Arrays.binarySearch(SERVICE_TYPE_PM_JOB, jm.getServiceTypeID()) >= 0);

                wrapper = ECService.getInstance().getMediaLibraryContent(getCurrentContext(), "" + m.getJobId(), isPMJob ? 3 : 2);
            }
        }

        @Override
        public void onPostExecute() {

//            llImagePreview.setImageFiles(wrapper.image);
//            llImagePreview.setOnClickImage(new OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent i = new Intent(getCurrentContext(), ImagePreviewActivity.class);
//                    i.putExtra("file", wrapper.image);
//                    startActivity(i);
//                }
//            });
            if (wrapper != null) {
                llImagePreview.setImageFiles(JobDetailActivity.this.getSupportFragmentManager(),
                        wrapper.getImages());

                llImagePreview.setOnClickImage(new ImagePagerLayout.OnClickImageListener() {
                    @Override
                    public void onClick(View v, String imageFile) {
                        Intent i = new Intent(getCurrentContext(), ImagePreviewActivity.class);
                        i.putExtra("file", new File(imageFile));
                        startActivity(i);
                    }
                });

                mLlRecordList.removeAllViews();
                for (File file : wrapper.voiceList) {
                    RecordPlayLayout llRecordPlay = new RecordPlayLayout(JobDetailActivity.this);
                    llRecordPlay.setRecordFile(file);
                    mLlRecordList.addView(llRecordPlay);

                }
                mLlRecordList.setVisibility(View.VISIBLE);
            }

//            llRecordPlay.setRecordFile(wrapper.voice);

        }


    }

    private class CloseJob extends McProgressAsyncTask {

        JobModel m;
        private Date res;
        private boolean isRequiredPicture = false;
        private boolean isRequiredSignName = false;
        private boolean isRequiredSign = false;

        public CloseJob(Context context, JobModel m) {
            super(context);
            this.m = m;
        }

        @Override
        public void doInBackground() {
            if (isEngineering && Arrays.binarySearch(SERVICE_TYPE_PM_JOB, jm.getServiceTypeID()) >= 0) {
//                res = ECService.getInstance().closeEngineeringJobs(getCurrentContext(), m.getJobId());

                // CRF-00002463
                res = ECService.getInstance().closeJobs2(getCurrentContext(), m.getJobId(), m.getJobNum(), edName.getText().toString().trim(),
                        capturedImageFile, signImage, true);

            } else if (jm.getServiceTypeID() == SERVICE_TYPE_INTERDEPT_JOB) {
                if (capturedImageFile == null && jm.getRequiredPicture() == 1) {
                    isRequiredPicture = true;
                    return;
                }
                if (signImage == null && jm.getRequiredESignature() == 1) {
                    isRequiredSign = true;
                    return;
                }
                if (signImage != null && jm.getRequiredESignature() == 1 && TextUtils.isEmpty(edName.getText().toString().trim())) {
                    isRequiredSignName = true;
                    return;
                }

                res = ECService.getInstance().closeJobs2(getCurrentContext(), m.getJobId(), m.getJobNum(), edName.getText().toString().trim(),
                        capturedImageFile, signImage, false);

            } else {
                res = ECService.getInstance().closeJobs(getCurrentContext(), m.getJobId(), null, null, null, null);
            }
        }

        @Override
        public void onPostExecute() {
            if (isRequiredPicture) {
                toast(getString(R.string.missing_picture));
                return;
            }
            if (isRequiredSign) {
                toast(getString(R.string.missing_signature));
                return;
            }
            if (isRequiredSignName) {
                toast(getString(R.string.missing_signature_name));
                return;
            }
            if (res != null) {
                change = true;
                toast(getString(R.string.job_completed));
                VibrateUtils.vibrate(getCurrentContext());
                if (JobStatus.TIMEOUT.equals(m.getStatus())) {
                    m.setStatus(JobStatus.DELAYED);
                } else {
                    m.setStatus(JobStatus.COMPLETED);
                }
                m.setCompletedTime(res);
                m.setClosedByName(SessionContext.getInstance().getUsername());
                setButtonVisibility(m, true);
                if (recordFile != null || capturedImageFile != null) {
                    postMedia();
                } else {
                    jm.setRemarks(remarksEt.getText().toString());
                    jm.setActionTaken(edActionTaken.getText().toString());
                    goBackToHomeScreen(1);
                }
            } else {
                toast(getString(R.string.update_fail));
            }
        }
    }


    private class CheckIsRunnerJob extends McBackgroundAsyncTask {

        private JobModel m;
        private GetUpdateJobStatusResponse resp;
        private int returnCode = -1;

        private CheckIsRunnerJob(Context context, JobModel m) {
            super(context);
            this.m = m;
        }

        @Override
        public void doInBackground() {
            if (isCConnect()) {

            } else {
                resp = ECService.getInstance().getUpdateJobStatus(getCurrentContext(), m.getJobNum());
            }
        }

        @Override
        public void onPostExecute() {
            if (isCConnect()) {

            } else {
                if (resp != null) {
                    returnCode = resp.getReturnCode();
                    if (returnCode != 1) {
                        List<UpdateableJobModel> list = resp.getList();
                        if (list != null && list.size() > 0) {
                            if (list.get(0).getUpdateable() == UpdateableJobModel.IS_RUNNER_JOB) {
                                setButtonVisibility(m, true);
                            }
                        }
                    }
                }
            }
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            goBack();
            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

    private void goBack() {
        Intent i = new Intent();
        i.putExtra("job", jm);
        i.putExtra("change", change);
        setResult(RESULT_OK, i);
        finish();
    }

    private void goBackToHomeScreen(int value) {

        if(value == 1){
            Intent i = new Intent(this , EnHomeActivity.class);
            i.putExtra("job", jm);
            i.putExtra("change", change);
            startActivity(i);
        } else if (value == 2){
            Intent i = new Intent(this , MyJobsActivity.class);
            i.putExtra("job", jm);
            i.putExtra("change", change);
            startActivity(i);
        }
        finish();
    }

    private void setButtonVisibility(JobModel m, boolean updatable) {
        jm = m;

        if (m == null || m.getJobId() == -1L) {
            return;
        }

        ackBtn.setVisibility(View.VISIBLE);
        updateBtn.setVisibility(View.VISIBLE);
        completedBtn.setVisibility(View.VISIBLE);
        if (SessionContext.getInstance().getUserType().equals("S") || SessionContext.getInstance().getUserType().equals("M")) {
            cancelBtn.setVisibility(View.VISIBLE);
        } else {
            cancelBtn.setVisibility(View.GONE);
        }
        llDeadline.setVisibility(View.GONE);
        btnExtendDeadline.setVisibility(View.GONE);

        ackBtn.setEnabled(false);
        updateBtn.setEnabled(false);
        completedBtn.setEnabled(false);
        cancelBtn.setEnabled(false);
        remarksEt.setEnabled(false);
        edName.setEnabled(false);
        edActionTaken.setEnabled(false);
        //
        spPmCondition.setEnabled(false);
        spRunner.setEnabled(false);
        edCost.setEnabled(false);
        spManhours.setEnabled(false);

//        ivRecord.setEnabled(false);
//        ivCamera.setEnabled(false);
//        edSignature.setEnabled(false);
        /**
         * Kha: enable if PENDING only
         */
        btnTranslate.setEnabled(false);
        if (isEngineering) {
            llAssign.setVisibility(View.VISIBLE);
            llCheckList.setVisibility(View.VISIBLE);
            llManhour.setVisibility(View.VISIBLE);
            llAckStatus.setVisibility(View.VISIBLE);

            if (!isFromSearch) {
                llEquipment.setVisibility(View.GONE);
                llEquipmentNo.setVisibility(View.GONE);
                llPmcondition.setVisibility(View.GONE);
            } else {
//                llPmcondition.setVisibility(View.GONE);
//                llAssign.setVisibility(View.GONE);
//                llCheckList.setVisibility(View.GONE);
//                llManhour.setVisibility(View.GONE);
//                llAckStatus.setVisibility(View.GONE);
//                ackBtn.setVisibility(View.GONE);
//                completedBtn.setVisibility(View.GONE);
//                updateBtn.setVisibility(View.GONE);
//                startBtn.setVisibility(View.GONE);
//                btEquipInfo.setVisibility(View.GONE);

                llEquipment.setVisibility(View.VISIBLE);
                llEquipmentNo.setVisibility(View.VISIBLE);
            }
        } else {
            llPmcondition.setVisibility(View.GONE);
            ll_detail_bottom.setVisibility(View.GONE);
            startBtn.setVisibility(View.GONE);
            llAssign.setVisibility(View.GONE);
            llCheckList.setVisibility(View.GONE);
            llManhour.setVisibility(View.GONE);
            llAckStatus.setVisibility(View.GONE);
            llEquipment.setVisibility(View.GONE);
            llEquipmentNo.setVisibility(View.GONE);
        }

        // 1-5 is PM Job, 6-8 is ECN Job
        if (JobStatus.PENDING.equals(m.getStatus())) {
            updateBtn.setEnabled(true);

            if (isSupervisor && Arrays.binarySearch(SERVICE_TYPE_PM_JOB, jm.getServiceTypeID()) >= 0) {
                spRunner.setEnabled(true);
            }

            if (updatable) {
                if (m.getIsAcked() == null || m.getIsAcked() == false) {
                    ackBtn.setEnabled(true);
                } else {
                    updateBtn.setEnabled(true);
                    completedBtn.setEnabled(true);
                    cancelBtn.setEnabled(true);
                    remarksEt.setEnabled(true);
                    edActionTaken.setEnabled(true);
                    edName.setVisibility(View.VISIBLE);
                    edName.setEnabled(true);
                    btnTranslate.setEnabled(true);
                }
                if (SessionContext.getInstance().getUserType().equals("S") || SessionContext.getInstance().getUserType().equals("M")) {
                    btnExtendDeadline.setVisibility(View.VISIBLE);
                }
            }
        }
        if (JobStatus.TIMEOUT.equals(m.getStatus())) {
            boolean isEnableCompleteBtn = PrefsUtil.getConnectJobStatus(this);
            completedBtn.setEnabled(isEnableCompleteBtn);
            completedBtn.setVisibility(View.VISIBLE);
            if (SessionContext.getInstance().getUserType().equals("S") || SessionContext.getInstance().getUserType().equals("M")) {
                cancelBtn.setEnabled(isEnableCompleteBtn);
                cancelBtn.setVisibility(View.VISIBLE);
            } else {
                cancelBtn.setVisibility(View.GONE);
            }
            ivRecord.setEnabled(false);
            ivCamera.setEnabled(false);
            edSignature.setEnabled(false);
        }
        JobStatus js = jm.getStatus();
        if (jm.getServiceTypeID() == 7 || Arrays.binarySearch(SERVICE_TYPE_PM_JOB, jm.getServiceTypeID()) >= 0) {
            isInspected = true;
        }
        if (m.getStatus() != null) {
            satusTv.setText(m.getStatus().getResourceId());
        }
        remarksEt.setText(m.getRemarks());
        completedTimeTv.setText(McUtils.formatDateTime(m.getCompletedTime()));

        if (m.getIsAcked() != null && m.getIsAcked()) {
            acknowledgeTv.setText(R.string.yes);
        } else {
            acknowledgeTv.setText(R.string.no);
        }

        if (m.getNotificationType() == 3 || m.getStatus() == JobStatus.CANCELLED) {
            if (m.getIsRead() != null) {
                ackBtn.setVisibility(m.getIsRead() ? View.GONE : View.VISIBLE);
            } else {
                ackBtn.setVisibility(View.GONE);
            }
            ackBtn.setEnabled(true);
            updateBtn.setVisibility(View.GONE);
            completedBtn.setVisibility(View.GONE);
            cancelBtn.setVisibility(View.GONE);
        }

        if (jm != null && jm.getServiceTypeID() == SERVICE_TYPE_INTERDEPT_JOB) {
            ll_detail_bottom.setVisibility(View.VISIBLE);
            llManhour.setVisibility(View.GONE);
            llPmcondition.setVisibility(View.GONE);
            ll_job_detail_cost.setVisibility(View.GONE);
            llCheckList.setVisibility(View.GONE);
        } else {
            ll_detail_bottom.setVisibility(View.VISIBLE);
            ll_detail_bottom_voice.setVisibility(View.GONE);
            ll_detail_bottom_ack.setVisibility(View.GONE);
        }


        if (JobStatus.OPEN.equals(m.getStatus())) {
            completedBtn.setVisibility(View.GONE);
            cancelBtn.setVisibility(View.GONE);
            ackBtn.setVisibility(View.GONE);
            updateBtn.setVisibility(View.GONE);
            ivRecord.setEnabled(false);
            ivCamera.setEnabled(false);
            tvAccept.setVisibility(View.VISIBLE);
            edSignature.setEnabled(false);
        } else {
            llEquipmentNo.setVisibility(View.GONE);
            llEquipment.setVisibility(View.GONE);
        }
        if (JobStatus.COMPLETED.equals(m.getStatus())) {
            ll_detail_bottom_ack.setVisibility(View.GONE);
            ll_detail_bottom_picture.setVisibility(View.GONE);
            ll_detail_bottom_voice.setVisibility(View.GONE);
        }
        if (JobStatus.COMPLETED.equals(js) && isInspected && isInspected && m.getIsInspected() == 1 && PrefsUtil.getAllowJobEditAfterCompleted(this) == 1) {
            if (isSupervisor) {
                llDeadline.setVisibility(View.VISIBLE);
                llAssign.setVisibility(View.VISIBLE);
                spRunner.setEnabled(true);
                tvInspected.setVisibility(View.VISIBLE);
                completedBtn.setVisibility(View.GONE);
                cancelBtn.setVisibility(View.GONE);
                ackBtn.setVisibility(View.GONE);
                updateBtn.setVisibility(View.GONE);
                updateBtn.setEnabled(true);
                tvAccept.setVisibility(View.GONE);
                if (jm != null && jm.getServiceTypeID() == SERVICE_TYPE_INTERDEPT_JOB) {
                    ll_detail_bottom_voice.setVisibility(View.VISIBLE);
                }
                ll_detail_bottom_picture.setVisibility(View.VISIBLE);
                ivRecord.setEnabled(false);
                ivCamera.setEnabled(false);
                edSignature.setEnabled(false);
                if (jm.getIsInspected() == 1) {
                    tvInspected.setText(getString(R.string.fail_inspection));
                }
            }
        }
        if (JobStatus.COMPLETED.equals(js) && isInspected && m.getIsInspected() == 0 && PrefsUtil.getAllowJobEditAfterCompleted(this) == 1) {
            if (isSupervisor) {
                llDeadline.setVisibility(View.VISIBLE);
                llAssign.setVisibility(View.VISIBLE);
                spRunner.setEnabled(true);
                tvInspected.setVisibility(View.VISIBLE);
                completedBtn.setVisibility(View.GONE);
                cancelBtn.setVisibility(View.GONE);
                ackBtn.setVisibility(View.GONE);
                updateBtn.setVisibility(View.VISIBLE);
                remarksEt.setEnabled(true);
                updateBtn.setEnabled(true);
                tvAccept.setVisibility(View.GONE);
                if (jm != null && jm.getServiceTypeID() == SERVICE_TYPE_INTERDEPT_JOB) {
                    ll_detail_bottom_voice.setVisibility(View.VISIBLE);
                }
                ll_detail_bottom_picture.setVisibility(View.VISIBLE);
                ivRecord.setEnabled(false);
                ivCamera.setEnabled(false);
                edSignature.setEnabled(false);
                if (jm.getIsInspected() == 1) {
                    tvInspected.setText(getString(R.string.fail_inspection));
                }
            }
        }
        if (PrefsUtil.getEnableConnectPlus(this)) {
            ll_detail_bottom_ack.setVisibility(View.GONE);
            llEquipmentNo.setVisibility(View.GONE);
            llEquipment.setVisibility(View.GONE);
        }

        boolean isPMJob = Arrays.binarySearch(SERVICE_TYPE_PM_JOB, jm.getServiceTypeID()) >= 0;
        if (isSupervisor && isPMJob) {
            llAssign.setVisibility(View.VISIBLE);
        } else {
            llAssign.setVisibility(View.GONE);
        }

        if (!isEnableCaptureImage) {
            ll_detail_bottom_picture.setVisibility(View.GONE);
        }
        if (!isEnableVoiceImage) {
            ll_detail_bottom_voice.setVisibility(View.GONE);
        }

    }

    @Override
    public boolean isLoginActivity() {
        return true;
    }

    @Override
    public boolean isHomeActivity() {
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode != RESULT_OK) {
            return;
        }
        /**
         * Fix #858 by Kha Tran
         * on 2015/07/10
         */
        if (requestCode == RQ_RECORD_VOICE) {
//            File recordFile = (File) data.getSerializableExtra("record");
            recordFile = (File) data.getSerializableExtra("record");
//            request.getItem().setRecordedVoice(recordFile);
        }

        if (requestCode == RQ_CAPTURE_IMAGE) {
            capturedImageFile = (File) data.getSerializableExtra("image");
            request.getItem().setCapturedImage(capturedImageFile);
            ivCamera.setImageDrawable(getResources().getDrawable(R.drawable.photo_icon_on));

        }
        if (requestCode == RQ_SIGNATURE) {
            byte[] data1 = data.getByteArrayExtra(SignatureActivity.BITMAP_IMAGE);
//            signImage= (File) data.getParcelableExtra(SignatureActivity.BITMAP_IMAGE);
//            signImage = (File) data.getSerializableExtra(SignatureActivity.BITMAP_IMAGE);
        }

        if (requestCode == RQ_TRANSLATE) {
            String remarkText = remarksEt.getText().toString();
            String translatedText = data.getStringExtra(TranslateActivity.EXTRA_TRANSLATED_TEXT);
            if (translatedText.length() > 0) {
                boolean isMerge = data.getBooleanExtra(TranslateActivity.EXTRA_TRANSLATED_TEXT_IS_MERGE, true);
                if (isMerge) {
                    remarkText = remarkText + "\n" + translatedText;
                } else {
                    remarkText = translatedText;
                }
            }
            remarksEt.setText(remarkText);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private class GetMediaLibraryConfig extends McBackgroundAsyncTask {

        public GetMediaLibraryConfig(Context context) {
            super(context);
        }

        @Override
        public void doInBackground() {
            MediaLibraryConfig.config = ECService.getInstance().getMediaLibraryConfig(getCurrentContext());
        }

        @Override
        public void onPostExecute() {
            if (MediaLibraryConfig.config != null) {
                isEnableCaptureImage = MediaLibraryConfig.config.isEnableCaptureImage();
                isEnableVoiceImage = MediaLibraryConfig.config.isEnableVoiceImage();

                setButtonVisibility(jm, true);
            }
        }
    }

    private void postMedia() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                boolean isPMJob = (isEngineering && Arrays.binarySearch(SERVICE_TYPE_PM_JOB, jm.getServiceTypeID()) >= 0);

                ECService.getInstance().postMediaLibraryContent(getCurrentContext(), ServiceRequest.getInstance().getItem().getImageTag(), capturedImageFile, isPMJob ? 3 : 2, 1,
                        jm.getJobNum());
                ECService.getInstance().postMediaLibraryContent(getCurrentContext(), ServiceRequest.getInstance().getItem().getVoiceTag(), recordFile, 2, 2,
                        jm.getJobNum());
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                goBackToHomeScreen(1);
            }
        }.execute();
    }
}

