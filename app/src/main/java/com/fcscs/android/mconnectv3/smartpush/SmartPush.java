package com.fcscs.android.mconnectv3.smartpush;

import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.fcscs.android.mconnectv3.GCMIntentService;
import com.fcscs.android.mconnectv3.GCMNotification;
import com.fcscs.android.mconnectv3.MQTTService;
import com.fcscs.android.mconnectv3.McApplication;
import com.fcscs.android.mconnectv3.common.McConstants;
import com.fcscs.android.mconnectv3.common.SessionContext;
import com.fcscs.android.mconnectv3.common.util.DateTimeHelper;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.dao.entity.NotificationModel;
import com.fcscs.android.mconnectv3.manager.model.JobModel;
import com.fcscs.android.mconnectv3.manager.model.MessageModel;

import java.util.Date;

/**
 * Created by FCS on 6/20/16.
 */
public class SmartPush {
    public static final String CONFIG_MQTT_URL = "tcp://210.184.6.233:1883";
    public static final int PUSH_TYPE_GCM = 0;
    public static final int PUSH_TYPE_MQTT = 1;
    private static final String CONFIG_MQTT_TOPIC = "CONFIG_MQTT_TOPIC";
    private static final String CONFIG_MQTT_URL_KEY = "MQTT_URL_KEY";
    private static final String CONFIG_PUSH_TYPE = "PUSH_TYPE";

    public static final String ACTION_UPDATE_COUNTER = "ACTION_UPDATE_COUNTER";
    private static final String TAG = "SmartPush";
    private Context mContext;
    private int mPushType;
    public MQTTService mMQTTService;
    boolean isBoundMQTT = false;
    String urlMQTT = "";
    String topicMQTT = "";
    Intent intentServiceMQTT;

    /// init data
    private static SharedPreferences getPrefs(Context context) {
        return context.getSharedPreferences("SmartPush.preference.data", Context.MODE_PRIVATE);
    }

    public static void saveConfigPushPrefs(Context context, String url, int pushType) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        if (url == null || url.equals("")) {
        } else {
            editor.putString(CONFIG_MQTT_URL_KEY, url);
        }
        editor.putInt(CONFIG_PUSH_TYPE, pushType);
        editor.commit();
    }

    public static String getConfigMQTTURLPrefs(Context context) {

        SharedPreferences prefs = getPrefs(context);
        return prefs.getString(CONFIG_MQTT_URL_KEY, CONFIG_MQTT_URL);
    }

    public static int getConfigPushTypePrefs(Context context) {

        SharedPreferences prefs = getPrefs(context);
        return prefs.getInt(CONFIG_PUSH_TYPE, PUSH_TYPE_GCM);
    }

    public static void saveMQTTTopicPrefs(Context context, String topic) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(CONFIG_MQTT_TOPIC, topic);
        editor.commit();
    }

    public static String getMQTTTopicPrefs(Context context) {
        SharedPreferences prefs = getPrefs(context);
        return prefs.getString(CONFIG_MQTT_TOPIC, "");
    }

    private static SmartPush mInstance;

    public static SmartPush getInstance() {
        if (mInstance == null) {
            mInstance = new SmartPush();
        }
        return mInstance;
    }

    //MQTT
    private void subscribeMQTT(String topicMQTT) {
        this.topicMQTT = topicMQTT;
    }

    private void setUrlMQTT(String urlMQTT) {
        this.urlMQTT = urlMQTT;
    }

    public void registerPush(Context context) {
        if (!SessionContext.getInstance().isLogin() || isReady()) {
            return;
        }
        mContext = context;
        if (SmartPush.getConfigPushTypePrefs(context) == PUSH_TYPE_GCM) {
            mPushType = PUSH_TYPE_GCM;
            startGCM(mContext);
        } else {
            mPushType = PUSH_TYPE_MQTT;
            startMQTT(mContext);
        }
    }

    public void unRegisterPush(Context context) {
        if (mPushType == PUSH_TYPE_MQTT) {
            stopMQTT(context);
        } else {
            stopGCM();
        }
    }

    private boolean isReady() {
        if (mContext == null) {
            return false;
        }
        if (SmartPush.getConfigPushTypePrefs(mContext) == PUSH_TYPE_GCM) {
            return GCMIntentService.getCurrentRegId() != null;
        } else {
            return mMQTTService != null && isBoundMQTT && mMQTTService.isConnected();
        }
    }

    private void startGCM(Context context) {
        GCMIntentService.registerGCM(context, mHandlerGCM);
    }

    private void stopGCM() {
        GCMIntentService.reset();
        GCMIntentService.unregisterGCM();
    }

    private void startMQTT(Context context) {
        String url = SmartPush.getConfigMQTTURLPrefs(context);
        String topic = SmartPush.getMQTTTopicPrefs(context);

        subscribeMQTT(topic);
        setUrlMQTT(url);

        intentServiceMQTT = new Intent(context, MQTTService.class);
        context.bindService(intentServiceMQTT, mConnection, Context.BIND_AUTO_CREATE);
    }

    private void stopMQTT(Context context) {
        if (isBoundMQTT) {
            isBoundMQTT = false;
            if (mMQTTService != null) {
                mMQTTService.disconnect();
                mMQTTService = null;
            }
            if (mConnection != null) {
                context.unbindService(mConnection);
            }
        }
    }

    ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            MQTTService.MqttBinder binder = (MQTTService.MqttBinder) service;
            mMQTTService = binder.getService();
            isBoundMQTT = true;

            if (mMQTTService != null) {
                Log.i("ServiceConnection", "connected");
                mMQTTService.setHandler(mHandlerMQTT);
                mMQTTService.setmURL(urlMQTT);
                mMQTTService.setmTopic(topicMQTT);
                mMQTTService.doConnect();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            isBoundMQTT = false;
            Log.i("ServiceConnection", "Disconnected");
        }
    };

    Handler mHandlerGCM = new Handler(Looper.getMainLooper()) {

        @Override
        public void handleMessage(Message msg) {
            handleDataReceive(msg.getData());
        }
    };

    Handler mHandlerMQTT = new Handler(Looper.getMainLooper()) {

        @Override
        public void handleMessage(Message msg) {
            handleDataReceive(msg.getData());
        }
    };

    private void handleDataReceive(Bundle bundle) {

        if (McConstants.OFFLINE_MODE) {
            return;
        }

        Log.i("handleDataReceive", bundle.toString());
        McUtils.saveLogToSDCard("GCM Push Notification: " + bundle.toString());

        if (!SessionContext.getInstance().isLogin()) {
            return;
        }

        NotificationModel m = new NotificationModel();
        m.setUserId(bundle.getString("user_id"));
        m.setDataId(bundle.getString("data_id"));
        m.setDataType(bundle.getString("data_type"));
        m.setProduct(bundle.getString("product"));
        m.setNotificationTitle(bundle.getString("notification_title"));
        m.setNotificationContent(bundle.getString("notification_content"));
        m.setCreated(DateTimeHelper.getDateByTimeZone(new Date()));

        if (m.getDataType() != null) {

            Runnable sendUpdatesToUI = new Runnable() {
                public void run() {
                    Intent intent = new Intent(ACTION_UPDATE_COUNTER);
                    mContext.sendBroadcast(intent);
                }
            };
            new Thread(sendUpdatesToUI).start();

            try {
                if ("JOB".equals(m.getDataType())) {
                    JobModel model = new JobModel();
                    String ns = Context.NOTIFICATION_SERVICE;
                    model.setNotificationContent(m.getNotificationContent());
                    model.setJobId(bundle.getLong("google.sent_time", -1L));
                    model.setJobNum(m.getDataId());
                    NotificationManager mManager =
                            (NotificationManager) McApplication.application.getSystemService(ns);
                    GCMNotification.showPushECJobNotification(mContext, mManager, model, "", false);
                } else if ("MSG".equals(m.getDataType())) {
                    MessageModel model = new MessageModel();
                    String ns = Context.NOTIFICATION_SERVICE;
                    model.setNotificationContent(m.getNotificationContent());
                    model.setMsgDoneId(Long.parseLong(m.getDataId()));
                    NotificationManager mManager =
                            (NotificationManager) McApplication.application.getSystemService(ns);
                    GCMNotification.showPushECMsgNotification(mContext, mManager, model, "");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void reset() {
        GCMIntentService.reset();
    }
}
