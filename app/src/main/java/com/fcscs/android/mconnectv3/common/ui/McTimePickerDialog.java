package com.fcscs.android.mconnectv3.common.ui;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TimePicker;
import android.widget.TimePicker.OnTimeChangedListener;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.util.DateTimeHelper;
import com.fcscs.android.mconnectv3.common.util.PrefsUtil;

public class McTimePickerDialog extends AlertDialog implements OnClickListener, OnTimeChangedListener {

    private static final String HOUR = "hour";
    private static final String MINUTE = "minute";
    private static final String IS_24_HOUR = "is24hour";

    private final TimePicker mTimePicker;

    int mInitialHourOfDay;
    int mInitialMinute;
    boolean mIs24HourView;
    private android.view.View.OnClickListener mListener;

    public McTimePickerDialog(Context context, Date date, boolean is24HourView) {
        this(context, 0, date, is24HourView);
    }

    public McTimePickerDialog(Context context, int theme, Date date, boolean is24HourView) {
        super(context, theme);
        // convert to date server by timezone -- hanhmh1203
//		date = DateTimeHelper.getDateByTimeZone(date);
        Locale mLocale = PrefsUtil.getLocale();
        Configuration config = context.getResources().getConfiguration();
        if (mLocale.equals(config.locale) == false) {
            config.locale = mLocale;
            context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        }

        Calendar cal = Calendar.getInstance(Locale.US);
        cal.setTime(date);

        mInitialHourOfDay = cal.get(Calendar.HOUR_OF_DAY);
        mInitialMinute = cal.get(Calendar.MINUTE);
        mIs24HourView = is24HourView;

        setIcon(0);
        setTitle(context.getString(R.string.set_time));

        Context themeContext = getContext();
        setButton(BUTTON_POSITIVE, themeContext.getText(R.string.done), this);
        setButton(BUTTON_NEGATIVE, themeContext.getText(R.string.cancel), this);

        LayoutInflater inflater = (LayoutInflater) themeContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.time_picker_dialog, null);
        setView(view);
        mTimePicker = (TimePicker) view.findViewById(R.id.timePicker);

        mTimePicker.setIs24HourView(mIs24HourView);
        mTimePicker.setCurrentHour(mInitialHourOfDay);
        mTimePicker.setCurrentMinute(mInitialMinute);
        mTimePicker.setOnTimeChangedListener(this);

    }

    public void onClick(DialogInterface dialog, int which) {
    }

    public void setOnClickDone(android.view.View.OnClickListener listener) {
        this.mListener = listener;
    }

    @Override
    public void show() {
        super.show();
        getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(this.mListener);
    }

    public void updateTime(int hourOfDay, int minutOfHour) {
        mTimePicker.setCurrentHour(hourOfDay);
        mTimePicker.setCurrentMinute(minutOfHour);
    }

    public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
        updateTime(hourOfDay, minute);
//		mTimePicker.setCurrentMinute(minute);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public Bundle onSaveInstanceState() {
        Bundle state = super.onSaveInstanceState();
        state.putInt(HOUR, mTimePicker.getCurrentHour());
        state.putInt(MINUTE, mTimePicker.getCurrentMinute());
        state.putBoolean(IS_24_HOUR, mTimePicker.is24HourView());
        return state;
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        int hour = savedInstanceState.getInt(HOUR);
        int minute = savedInstanceState.getInt(MINUTE);
        mTimePicker.setIs24HourView(savedInstanceState.getBoolean(IS_24_HOUR));
        mTimePicker.setCurrentHour(hour);
        mTimePicker.setCurrentMinute(minute);
    }

    public TimePicker getTimePicker() {
        return mTimePicker;
    }
}
