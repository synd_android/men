package com.fcscs.android.mconnectv3.common;


public interface QRCodeParser {

    public abstract boolean isCorrectFormat(String qrcode);

}
