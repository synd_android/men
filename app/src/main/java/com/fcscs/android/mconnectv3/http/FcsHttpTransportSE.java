package com.fcscs.android.mconnectv3.http;

import java.io.IOException;
import java.io.InputStream;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.ksoap2.transport.ServiceConnection;
import org.xmlpull.v1.XmlPullParserException;

import com.fcscs.android.mconnectv3.common.McConstants;

import android.util.Log;

public class FcsHttpTransportSE extends HttpTransportSE implements ResponseInfo {

    static final String TAG = FcsHttpTransportSE.class.getSimpleName();

    private FcsServiceConnectionSE connection = null;
    private int connectTimeout = McConstants.WS_CONNECT_TIMEOUT;
    private int readTimeout = McConstants.WS_READ_TIMEOUT;

    public FcsHttpTransportSE(String url) {
        super(url);
    }

    public FcsHttpTransportSE(String url, int connectTimeout, int readTimeout) {
        super(url);
        this.connectTimeout = connectTimeout;
        this.readTimeout = readTimeout;
    }

    @Override
    public ServiceConnection getServiceConnection() throws IOException {
        connection = new FcsServiceConnectionSE(url, connectTimeout, readTimeout);
        if (debug) {
            Log.d(TAG, "Request(" + (requestDump == null ? 0 : requestDump.length()) + "): " + requestDump);
        }

        return connection;
    }

    @Override
    protected void parseResponse(SoapEnvelope envelope, InputStream is) throws XmlPullParserException, IOException {
        if (debug) {
            Log.d(TAG, "Response(" + (responseDump == null ? 0 : responseDump.length()) + "): " + responseDump);
        }
        super.parseResponse(envelope, is);
    }

    @Override
    public int getResponseCode() {
        if (connection != null) {
            try {
                return connection.getResponseCode();
            } catch (IOException e) {
            }
        }
        return 0;
    }

}
