package com.fcscs.android.mconnectv3.ws.cconnect.model.guestrequestservice;

import com.fcscs.android.mconnectv3.common.util.SoapHelper;

import org.ksoap2.serialization.SoapObject;

import java.io.Serializable;

/**
 * Created by NguyenMinhTuan on 11/6/15.
 */
public class EngineeringRequestResponse implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 0L;
    private String returnCode = "";
    private String errorMessage = "";
    private String returnCode1 = "";
    private String errorMessage1 = "";
    private String returnCode2 = "";
    private String errorMessage2 = "";
    private String returnCode3 = "";
    private String errorMessage3 = "";

    public EngineeringRequestResponse() {
    }

    public EngineeringRequestResponse(SoapObject soap) {
        returnCode = SoapHelper.getStringProperty(soap, "ReturnCode", "");
        errorMessage = SoapHelper.getStringProperty(soap, "ErrorMsg", "");
        returnCode1 = SoapHelper.getStringProperty(soap, "ReturnCode1", "");
        errorMessage1 = SoapHelper.getStringProperty(soap, "ErrorMessage1", "");
        returnCode2 = SoapHelper.getStringProperty(soap, "ReturnCode2", "");
        errorMessage2 = SoapHelper.getStringProperty(soap, "ErrorMessage2", "");
        returnCode3 = SoapHelper.getStringProperty(soap, "ReturnCode3", "");
        errorMessage3 = SoapHelper.getStringProperty(soap, "ErrorMessage3", "");
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getReturnCode3() {
        return returnCode3;
    }

    public void setReturnCode3(String returnCode3) {
        this.returnCode3 = returnCode3;
    }

    public String getErrorMessage3() {
        return errorMessage3;
    }

    public void setErrorMessage3(String errorMessage3) {
        this.errorMessage3 = errorMessage3;
    }

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getReturnCode1() {
        return returnCode1;
    }

    public void setReturnCode1(String returnCode1) {
        this.returnCode1 = returnCode1;
    }

    public String getErrorMessage1() {
        return errorMessage1;
    }

    public void setErrorMessage1(String errorMessage1) {
        this.errorMessage1 = errorMessage1;
    }

    public String getReturnCode2() {
        return returnCode2;
    }

    public void setReturnCode2(String returnCode2) {
        this.returnCode2 = returnCode2;
    }

    public String getErrorMessage2() {
        return errorMessage2;
    }

    public void setErrorMessage2(String errorMessage2) {
        this.errorMessage2 = errorMessage2;
    }
}