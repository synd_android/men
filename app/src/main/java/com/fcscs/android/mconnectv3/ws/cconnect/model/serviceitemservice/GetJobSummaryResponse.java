package com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ksoap2.serialization.SoapObject;

import com.fcscs.android.mconnectv3.common.McEnums.JobStatus;
import com.fcscs.android.mconnectv3.common.util.SoapHelper;
import com.fcscs.android.mconnectv3.manager.model.StatusModel;

public class GetJobSummaryResponse implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private List<StatusModel> statusModels = new ArrayList<StatusModel>();

    //econnect
    private Map<JobStatus, Integer> statusIdMap = new HashMap<JobStatus, Integer>();

    public GetJobSummaryResponse() {
    }

    public GetJobSummaryResponse(SoapObject response) {

//		int open = 0;
        int pending = 0;
        int complete = 0;
        int cancel = 0;
        int escalate = 0;
        int timeout = 0;
//		int schedule = 0;

        if (response.getPropertyCount() > 0) {
            for (int i = 0; i < response.getPropertyCount(); i++) {
                SoapObject soap = (SoapObject) response.getProperty(i);
                String statusName = SoapHelper.getStringProperty(soap, "statusName", "");
                int count = SoapHelper.getLongProperty(soap, "count", 0L).intValue();
                JobStatus st = JobStatus.convertFromCCStatus(statusName);

                if (JobStatus.PENDING.equals(st)) {
                    pending = count;
                } else if (JobStatus.COMPLETED.equals(st)) {
                    complete = count;
                } else if (JobStatus.CANCELLED.equals(st)) {
                    cancel = count;
                } else if (JobStatus.TIMEOUT.equals(st)) {
                    timeout = count;
                }
            }
        }

        int all = pending + complete + cancel + escalate + timeout;
        statusModels.add(new StatusModel(pending, JobStatus.PENDING));
        statusModels.add(new StatusModel(complete, JobStatus.COMPLETED));
        statusModels.add(new StatusModel(cancel, JobStatus.CANCELLED));
        statusModels.add(new StatusModel(timeout, JobStatus.TIMEOUT));
        statusModels.add(new StatusModel(all, JobStatus.ALL));
    }

    public void setStatusModels(List<StatusModel> statusModels) {
        this.statusModels = statusModels;
    }

    public List<StatusModel> getStatusModels() {
        return statusModels;
    }

    public Map<JobStatus, Integer> getStatusIdMap() {
        return statusIdMap;
    }

    public void setStatusIdMap(Map<JobStatus, Integer> statusIdMap) {
        this.statusIdMap = statusIdMap;
    }

}
