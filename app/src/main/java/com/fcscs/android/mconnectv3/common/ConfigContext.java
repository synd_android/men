package com.fcscs.android.mconnectv3.common;

import java.io.Serializable;

import android.content.Context;

import com.fcscs.android.mconnectv3.McApplication;
import com.fcscs.android.mconnectv3.common.McEnums.ServiceType;
import com.fcscs.android.mconnectv3.common.util.DateTimeHelper;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.common.util.PrefsUtil;
import com.fcscs.android.mconnectv3.dao.entity.DBConfigContext;
import com.fcscs.android.mconnectv3.dao.entity.DBHelper;

public class ConfigContext implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1797069320241980707L;
    private static ConfigContext instance;

    private McEnums.ServiceType serviceType;
    private String serviceUrl;
    //add hotelId
    private String hotelId;
    private long propertyId;
    private long pullPeriod;
    //	private String vibrate;
//	private String autoAck;
    private String dateFormat; //no need persist

    private boolean enableOnLoginPage;
    private boolean enableOnMainPage;
    private boolean enablePanicCall;
    private boolean enablePanicJob;

    public boolean isAutoAlert() {
        return autoAlert;
    }

    public void setAutoAlert(boolean autoAlert) {
        this.autoAlert = autoAlert;
    }

    private boolean autoAlert;
    private String panicCallContactNumber;

    private ConfigContext() {
        serviceType = ServiceType.CCONNECT;
    }

    public static ConfigContext getInstance() {
        if (instance == null) {
            instance = new ConfigContext();
            instance.populate(McApplication.application);
        }
        return instance;
    }

    public void populate(Context context) {

        DBConfigContext cc = DBHelper.getConfigContext(context);
        this.serviceType = ServiceType.valueOf(cc.getServiceType());
        this.serviceUrl = cc.getServiceUrl();
        this.propertyId = McUtils.isNullOrEmpty(cc.getPropertyId()) ? 0L : Long.valueOf(cc.getPropertyId());
        this.pullPeriod = McUtils.isNullOrEmpty(cc.getPullPeriod()) ? McConstants.PULL_PERIOD : Long.valueOf(cc.getPullPeriod());
        this.enableOnLoginPage = cc.isEnableOnLoginPage();
        this.enableOnMainPage = cc.isEnableOnMainPage();
        this.enablePanicCall = cc.isEnablePanicCall();
        this.enablePanicJob = cc.isEnablePanicJob();
        this.panicCallContactNumber = cc.getPanicCallContactNumber();
//		this.vibrate = McUtils.isNullOrEmpty(cc.getVibrate()) ? "True"	 : cc.getVibrate();
//		this.autoAck = McUtils.isNullOrEmpty(cc.getAutoAck()) ? "True"	 : cc.getAutoAck();
        //add hotelId
        PrefsUtil.setHotelId(context, this.hotelId);
    }

    public void persist(Context context) {
        DBConfigContext cc = DBHelper.getConfigContext(context);
        cc.setServiceType(this.serviceType.toString());
        cc.setServiceUrl(serviceUrl);
        cc.setPropertyId("" + propertyId);
        cc.setPullPeriod("" + pullPeriod);
        cc.setEnableOnLoginPage(this.isEnableOnLoginPage());
        cc.setEnableOnMainPage(this.isEnableOnMainPage());
        cc.setEnablePanicCall(this.isEnablePanicCall());
        cc.setEnablePanicJob(this.isEnablePanicJob());
        cc.setPanicCallContactNumber(this.getPanicCallContactNumber());
//		cc.setVibrate(vibrate);
//		cc.setAutoAck(autoAck);
        DBHelper.saveConfigContext(context, cc);

    }

    public String getDateFormat() {
        if (dateFormat == null || "".equalsIgnoreCase(dateFormat)) {
            return DateTimeHelper.DEFAULT_FORMAT;
        }
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

//	public String getAutoAck(){
//		return autoAck;
//	}
//	
//	public void setAutoAck(String autoAck){
//		this.autoAck = autoAck;
//	}
//	
//	public String getVibrate() {
//		return vibrate;
//	}
//
//	public void setVibrate(String vibrate) {
//		this.vibrate = vibrate;
//	}

    public long getPullPeriod() {
        return pullPeriod;
    }

    public void setPullPeriod(long pullPeriod) {
        this.pullPeriod = pullPeriod;
    }

    public long getPropertyId() {
        return propertyId < 1 ? 1 : propertyId;
    }

    public void setPropertyId(long propertyId) {

        this.propertyId = propertyId;
    }

    public String getServiceUrl() {
        return serviceUrl;
    }

    public void setServiceUrl(String contextUrl) {
        this.serviceUrl = contextUrl;
    }

    public String gethotelId() {
        return hotelId;
    }

    public void sethotelId(String hotelId) {
        this.hotelId = hotelId;
    }


    public McEnums.ServiceType getServiceType() {
        return serviceType;
    }

    public void setServiceType(McEnums.ServiceType serviceType) {
        this.serviceType = serviceType;
    }

    public boolean isEnableOnLoginPage() {
        return enableOnLoginPage;
    }

    public void setEnableOnLoginPage(boolean enableOnLoginPage) {
        this.enableOnLoginPage = enableOnLoginPage;
    }

    public boolean isEnableOnMainPage() {
        return enableOnMainPage;
    }

    public void setEnableOnMainPage(boolean enableOnMainPage) {
        this.enableOnMainPage = enableOnMainPage;
    }

    public boolean isEnablePanicCall() {
        return enablePanicCall;
    }

    public void setEnablePanicCall(boolean enablePanicCall) {
        this.enablePanicCall = enablePanicCall;
    }

    public boolean isEnablePanicJob() {
        return enablePanicJob;
    }

    public void setEnablePanicJob(boolean enablePanicJob) {
        this.enablePanicJob = enablePanicJob;
    }

    public String getPanicCallContactNumber() {
        return panicCallContactNumber;
    }

    public void setPanicCallContactNumber(String panicCallContactNumber) {
        this.panicCallContactNumber = panicCallContactNumber;
    }
}
