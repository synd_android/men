package com.fcscs.android.mconnectv3.main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.BaseActivity;
import com.fcscs.android.mconnectv3.common.McConstants;
import com.fcscs.android.mconnectv3.common.McEnums;
import com.fcscs.android.mconnectv3.common.McEnums.JobStatus;
import com.fcscs.android.mconnectv3.common.SessionContext;
import com.fcscs.android.mconnectv3.common.ui.HomeTopBar;
import com.fcscs.android.mconnectv3.common.ui.McProgressAsyncTask;
import com.fcscs.android.mconnectv3.common.ui.RoundCornerListAdapter;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.common.util.PrefsUtil;
import com.fcscs.android.mconnectv3.manager.model.JobModel;
import com.fcscs.android.mconnectv3.ws.cconnect.GuestRequestService;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.JobViewResponse;
import com.fcscs.android.mconnectv3.ws.econnect.ECService;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.GetNotificationStatusResponse;

public class JobSummaryActivity extends BaseActivity implements OnClickListener {

    public static final String DATE = "date";
    public static final String STATUS = "status";
    public static final String TOTAL = "total";
    public static final String SEARCHDAY = "SEARCH_DAYS";

    private ListView jobSummaryLv;
    private List<JobModel> jobViewList = new ArrayList<JobModel>();
    private HomeTopBar topBar;

    private Bundle bundle;
    private JobSummaryRoundAdapter adp;
    private boolean autoAck = false;
    private boolean change = false;
    private TextView ackBtn;
    private LinearLayout llBottom;
    private GetNotificationStatus notiTask;
    private boolean isFromSearch = false;
    public boolean isCompleted = false;
    public boolean isAll = false;
    Button btnRightInsp;
    Date mDate;
    String searchDays;


    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.job_summary);

        bundle = getIntent().getExtras();
        autoAck = bundle.getBoolean(McConstants.KEY_AUTO_ACK_JOB, false);
        isFromSearch = bundle.getBoolean(JobDetailActivity.KEY_SEARCH);
        final JobStatus status = (JobStatus) bundle.getSerializable(STATUS);
        mDate = (Date) bundle.getSerializable(DATE);
        if (status != null && status.equals(JobStatus.COMPLETED)) {
            isCompleted = true;
        }
        if (status != null && status.equals(JobStatus.ALL)) {
            isAll = true;
        }

        topBar = (HomeTopBar) findViewById(R.id.top_bar);
        topBar.getTitleTv().setText(R.string.job_summary);

//        if (PrefsUtil.getEnableEng(this)==1) {
//            topBar.showBtnNotInsp(true);
//        }


        jobSummaryLv = (ListView) findViewById(R.id.job_summ_statusgridview);
        ackBtn = (TextView) findViewById(R.id.btn_ack_all);
        ackBtn.setOnClickListener(this);
        llBottom = (LinearLayout) findViewById(R.id.ll_bottom);

        jobViewList = (ArrayList<JobModel>) bundle.get(McConstants.KEY_RESULT);
        if (isCompleted) {
            topBar.showBtnNotInsp(true);
        }
        if (jobViewList == null) {
            jobViewList = new ArrayList<JobModel>();
        }

        Boolean isCancelJob = false;
        for (JobModel j : jobViewList) {
            if (j.getStatus() == McEnums.JobStatus.CANCELLED) {
                isCancelJob = true;
                break;
            }
        }

        if (isCancelJob) {
            if (notiTask == null || AsyncTask.Status.RUNNING.equals(notiTask.getStatus()) == false) {
                notiTask = new GetNotificationStatus(getCurrentContext(), this, jobViewList, autoAck);
                notiTask.exec();
            }
        } else {
            adp = new JobSummaryRoundAdapter(this, jobViewList, autoAck, isCompleted);
            jobSummaryLv.setAdapter(adp);
        }
        Button btnRight = topBar.getbtnRightInsp();
        if (btnRight != null) {
            btnRight.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    new FindJobAsyncTask(JobSummaryActivity.this, status, mDate).execute();
                }
            });
        }
        setBottomVisibility();
    }

    private class FindJobAsyncTask extends McProgressAsyncTask {

        private JobStatus status;
        private Date date;
        private JobViewResponse res;

        public FindJobAsyncTask(Context context, JobStatus status, Date date) {
            super(context);
            this.status = status;
            this.date = date;
        }

        @Override
        public void doInBackground() {
            res = ECService.getInstance().getMyJobsList(getCurrentContext(), date, status.getStatusId(), searchDays);
        }

        @Override
        public void onPostExecute() {

            if (res == null || res.getJobViews().size() == 0) {
                toast(getString(R.string.no_job_found));
            } else {
                ArrayList<JobModel> jobViewModels = new ArrayList<JobModel>();
                jobViewList.clear();
                for (JobModel jobView : res.getJobViews()) {
                    if (!status.equals(JobStatus.ALL) && !status.equals(jobView.getStatus())) {
                        continue;
                    }
                    if (jobView.getServiceTypeID() == 6 || jobView.getServiceTypeID() == 8) {
                        continue;
                    }
                    if (JobStatus.COMPLETED.equals(jobView.getStatus())) {
                        jobViewList.add(jobView);
                    }

                }
                adp = new JobSummaryRoundAdapter(JobSummaryActivity.this, jobViewList, autoAck, isCompleted);
                jobSummaryLv.setAdapter(adp);
            }

        }

    }

    private void setBottomVisibility() {
        boolean show = false;
        if (jobViewList != null && jobViewList.size() > 0) {
            for (JobModel j : jobViewList) {
                if (j.getStatus() != JobStatus.PENDING) {
                    continue;
                }
                if (j.getIsAcked() == null || j.getIsAcked() == false) {
                    show = true;
                    break;
                }
            }
        }
        if (show) {
            llBottom.setVisibility(View.VISIBLE);
        } else {
            llBottom.setVisibility(View.GONE);
        }
    }

    public static class JobSummaryRoundAdapter extends RoundCornerListAdapter<JobModel> {

        private Activity activity;
        private boolean autoAck;
        public static final int RC_DETAIL = 10001;
        private final int[] SERVICETYPE_ENGINERRING = {1, 2, 3, 4, 5};
        private final int SERVICETYPE_INTER_DEPT = 7;
        private boolean isCompleted = false;

        public JobSummaryRoundAdapter(Activity activity, List<JobModel> list, boolean autoAck, boolean _iscompleted) {
            super(activity, list, R.layout.job_summary_list_item);
            this.activity = activity;
            this.autoAck = autoAck;
            isCompleted = _iscompleted;
        }

        @Override
        public void onBindView(View view, final Context context, int position, JobModel item) {

            final Tag tag = (Tag) view.getTag();
            final JobModel model = (JobModel) getItem(position);

            McEnums.StatusColor statusColor = McEnums.StatusColor.BLACK;
            Iterator itColor = SessionContext.getInstance().getStatusColorMap().keySet().iterator();
            while (itColor.hasNext()) {
                Integer key = (Integer) itColor.next();
                if (key == model.getStatus().getStatusId()) {
                    statusColor = (McEnums.StatusColor) SessionContext.getInstance().getStatusColorMap().get(key);
                    break;
                }
            }
            String status1;
            if (model.getStatus() != null) {
                status1 = model.getJobNum() + "(" + context.getString(model.getStatus().getResourceId()) + ")";
            } else {
                status1 = model.getJobNum() + "()";
            }
            String status2 = "";
            if (model.getIsAcked() != null && model.getIsAcked()) {
                status2 = status2 + "(" + context.getString(R.string.ack) + ")";
            } else {
                status2 = status2 + "(" + context.getString(R.string.not_ack) + ")";
            }
            if (model.getIsRead() != null && model.getIsRead()) {
                status2 = status2 + "(" + context.getString(R.string.read) + ")";
            }
            if (model.getEscalatedLevel() > 0) {
                status2 = status2 + "(ESC)";
            }
            if (model.getCcjob() > 0) {
                status2 = status2 + "(CC)";
            }
            String status = status1 + status2;
            Spannable spanStatus = new SpannableString(status);
            spanStatus.setSpan(new ForegroundColorSpan(statusColor.getColor()), 0, status1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            spanStatus.setSpan(new StyleSpan(Typeface.BOLD), status1.length(), status.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            tag.jobNumStatus.setText(spanStatus, TextView.BufferType.SPANNABLE);

            String deadline = context.getString(R.string.date) + ": ";
            if (model.getDate() != null) {
                deadline = deadline + McUtils.formatShortDate(model.getDate());
            }
            deadline = deadline + "\t\t" + context.getString(R.string.deadline) + ": ";
            if (model.getDeadline() != null) {
                deadline = deadline + McUtils.formatShortDateTime(model.getDeadline());
            }
            tag.deadLine.setText(deadline);
            String name = "";
            if (PrefsUtil.getEnableEng(context) == 0) {
                name = "(" + model.getServiceItemName() + " * " + model.getQuantity() + ")";
                if (model.getRoomNum() != null && !"".equalsIgnoreCase(model.getRoomNum())) {
                    name = model.getRoomNum() + " " + name;
                }
            } else {
                name = "(" + model.getEquipmentName() + " * " + model.getQuantity() + ")";
                if (model.getRoomNum() != null && !"".equalsIgnoreCase(model.getRoomNum())) {
                    name = context.getString(R.string.title_site) + " / " + model.getRoomNum() + " " + name;
                }
            }
//            if (model.getRoomNum() != null && !"".equalsIgnoreCase(model.getRoomNum())) {
//                name = model.getRoomNum() + " " + name;
//            }

            tag.serviceItemQty.setPadding(5, 10, 5, 10);
            tag.serviceItemQty.setText(name);

            tag.jobIdTv = new TextView(context);
//            tag.jobIdTv.setText(String.valueOf(model.getJobId()));
            if (isCompleted && (Arrays.binarySearch(SERVICETYPE_ENGINERRING, model.getServiceTypeID()) >= 0 || model.getServiceTypeID() == SERVICETYPE_INTER_DEPT)) {
//            if (isCompleted ) {
                tag.tvInsp.setVisibility(View.VISIBLE);
                if (model.getIsInspected() == 1) {
                    tag.tvInsp.setText(activity.getResources().getString(R.string.inspected));
                    tag.tvInsp.setTextColor(activity.getResources().getColor(android.R.color.black));
                } else {
                    tag.tvInsp.setText(activity.getResources().getString(R.string.not_insp));
                    tag.tvInsp.setTextColor(activity.getResources().getColor(R.color.button_insp));
                }
            } else {
                tag.tvInsp.setVisibility(View.GONE);
            }

//            if (PrefsUtil.getEnableEng(context)==1) {
//                tag.tvInsp.setVisibility(View.VISIBLE);
//                tag.tvInsp.setText(context.getString(R.string.not_insp));
//                tag.tvInsp.setTextColor(context.getResources().getColor(R.color.button_insp));
//            }

            view.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Intent si = JobDetailActivity.createIntent(activity, model);
                    si.putExtra(McConstants.KEY_AUTO_ACK_JOB, autoAck);
                    if(!activity.getClass().getSimpleName().equals("GuestHistoryActivity")){
                        si.putExtra(JobDetailActivity.KEY_SEARCH, ((JobSummaryActivity) activity).isFromSearch); // Nima Disabled it for Error FE41MW-11 Jira
                    }
                    activity.startActivityForResult(si, JobSummaryRoundAdapter.RC_DETAIL);
                }
            });

        }

        @Override
        public View newView(Context context, int position, ViewGroup parent) {
            View v = super.newView(context, position, parent);
            Tag tag = new Tag();

            tag.jobNumStatus = (TextView) v.findViewById(R.id.jobnumsdatus);
            tag.deadLine = (TextView) v.findViewById(R.id.deadline);
            tag.serviceItemQty = (TextView) v.findViewById(R.id.serviceItemqty);
            tag.tvInsp = (TextView) v.findViewById(R.id.tv_insp);
            v.setTag(tag);
            return v;
        }

        class Tag {
            TextView jobNumStatus;
            TextView deadLine;
            TextView serviceItemQty;
            TextView jobIdTv;
            TextView tvInsp;
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            goBack();
            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

    private void goBack() {
        Intent i = new Intent();
        i.putExtra("change", change);
        setResult(RESULT_OK, i);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == JobSummaryRoundAdapter.RC_DETAIL) {
            JobModel job = (JobModel) data.getSerializableExtra("job");
            change = change || data.getBooleanExtra("change", false);
            int i = -1;
            for (JobModel j : jobViewList) {
                i++;
                if (j.getJobId() == job.getJobId()) {
                    jobViewList.remove(i);
                    if (j.getStatus() == job.getStatus() || isAll) {
                        jobViewList.add(i, job);
                    }
                    break;
                }
            }
            setBottomVisibility();
            adp.notifyDataSetChanged();
        }
    }

    @Override
    public boolean isLoginActivity() {
        return true;
    }

    @Override
    public boolean isHomeActivity() {
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_ack_all:
                if (McConstants.NON_HOTEL_PRODUCT) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(JobSummaryActivity.this);
                    alertDialogBuilder.setMessage(getString(R.string.confirm) + "?");

                    alertDialogBuilder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            new AckAllJobTask(JobSummaryActivity.this).exec();
                        }
                    });
                    alertDialogBuilder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Do nothing
                        }
                    });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                } else {
                    new AckAllJobTask(this).exec();
                }
                break;
        }
    }

    class AckAllJobTask extends McProgressAsyncTask {

        public AckAllJobTask(Context context) {
            super(context);
        }

        private Boolean result;

        @Override
        public void doInBackground() {
            StringBuffer sb = new StringBuffer();
            if (jobViewList != null) {
                for (JobModel j : jobViewList) {
                    if (j.getStatus() != JobStatus.PENDING) {
                        continue;
                    }
                    if (j.getIsAcked() == null || j.getIsAcked() == false) {
                        if (sb.length() == 0) {
                            sb.append(j.getJobId());
                        } else {
                            sb.append("|");
                            sb.append(j.getJobId());
                        }
                    }
                }
            }
            if (sb.length() > 0) {
                result = ECService.getInstance().ackJob(getCurrentContext(), sb.toString());
            }
        }

        @Override
        public void onPostExecute() {
            if (result != null && result) {
                toast(R.string.acknowledge_job_successfully);
                finish();
            } else {
                toast(R.string.fail_to_acknowledge_job);
            }
        }

    }

    private class GetNotificationStatus extends McProgressAsyncTask {

        JobModel m;
        private Context context;
        private int returnCode = -1;
        private List<JobModel> jobList;
        private boolean autoAck;
        private Activity activity;

        public GetNotificationStatus(Context context, Activity activity, List<JobModel> list, boolean autoAck) {
            super(context);
            this.context = context;
            this.jobList = list;
            this.autoAck = autoAck;
            this.activity = activity;
        }

        @Override
        public void doInBackground() {
            try {
                List<JobModel> dataForNoti = jobList;
                for (JobModel iterDataForNoti : dataForNoti) {
                    iterDataForNoti.setNotificationType(1); // 1 for Job, 2 for Msg
                }
                GetNotificationStatusResponse resp = getNotificationStatus(context, dataForNoti);
                if (resp != null) {
                    returnCode = resp.getReturnCode();
                    if (returnCode == 0) {
                        List<JobModel> list = resp.getList();
                        if (list != null && list.size() > 0) {
                            for (JobModel m : list) {
                                for (JobModel j : jobList) {
                                    if (j.getStatus() == McEnums.JobStatus.CANCELLED && j.getJobNum().equals(m.getJobNum())) {
                                        j.setIsRead(m.getIsRead());
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (Throwable e) {
            }
        }

        @Override
        public void onPostExecute() {
            adp = new JobSummaryRoundAdapter(activity, jobList, autoAck, isCompleted);
            jobSummaryLv.setAdapter(adp);
        }

        private GetNotificationStatusResponse getNotificationStatus(Context context, List<JobModel> listJobInfo) {
            GetNotificationStatusResponse resp = null;
            try {
                resp = ECService.getInstance().getNotificationStatus(context, listJobInfo);
            } catch (Throwable e) {
            }
            return resp;
        }
    }
}
