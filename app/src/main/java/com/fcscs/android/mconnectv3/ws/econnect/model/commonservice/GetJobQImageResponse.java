package com.fcscs.android.mconnectv3.ws.econnect.model.commonservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.ksoap2.serialization.SoapObject;

import com.fcscs.android.mconnectv3.common.util.SoapHelper;

public class GetJobQImageResponse implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 211816267213693004L;
    public final static int ERROR = 1;
    private int returnCode;
    private String errorMsg;
    private List<ImageDetails> list;

    public GetJobQImageResponse() {

    }

    public GetJobQImageResponse(SoapObject soap) {
        SoapObject status = (SoapObject) soap.getProperty("ResponseStatus");
        returnCode = SoapHelper.getIntegerProperty(status, "ReturnCode", 1);
        errorMsg = SoapHelper.getStringProperty(status, "ErrorMsg", "");

        list = new ArrayList<ImageDetails>();
        //SoapObject listing = (SoapObject)soap.getProperty("NotificationsListing");
        for (int i = 0; i < soap.getPropertyCount(); i++) {
            if ("ImageListing".equalsIgnoreCase(SoapHelper.getPropertyName(soap, i))) {
                SoapObject s = (SoapObject) soap.getProperty(i);
                for (int j = 0; j < s.getPropertyCount(); j++) {
                    if ("ImageDetails".equalsIgnoreCase(SoapHelper.getPropertyName(s, j))) {
                        SoapObject l = (SoapObject) s.getProperty(j);
                        String JobNo = SoapHelper.getStringProperty(l, "JobNo", "");
                        String image = SoapHelper.getStringProperty(l, "Image", "");

                        ImageDetails m = new ImageDetails();
                        m.setJobNo(JobNo);
                        m.setImage(image);
                        list.add(m);
                    }
                }
            }
        }
    }

    public int getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(int returnCode) {
        this.returnCode = returnCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public List<ImageDetails> getList() {
        return list;
    }

    public void setList(List<ImageDetails> list) {
        this.list = list;
    }
}
