package com.fcscs.android.mconnectv3.ws.econnect.model.commonservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.ksoap2.serialization.SoapObject;

import com.fcscs.android.mconnectv3.common.util.SoapHelper;

public class GetPendingNotificationResponse implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2846090543654867439L;

    private int returnCode;
    private String errorMsg;
    private List<PullNotificationModel> list;


    public GetPendingNotificationResponse() {

    }

    public GetPendingNotificationResponse(SoapObject soap) {
        SoapObject status = (SoapObject) soap.getProperty("ResponseStatus");
        returnCode = SoapHelper.getIntegerProperty(status, "ReturnCode", 1);
        errorMsg = SoapHelper.getStringProperty(status, "ErrorMsg", "");

        list = new ArrayList<PullNotificationModel>();
        //SoapObject listing = (SoapObject)soap.getProperty("NotificationsListing");
        for (int i = 0; i < soap.getPropertyCount(); i++) {
            if ("NotificationsListing".equalsIgnoreCase(SoapHelper.getPropertyName(soap, i))) {
                SoapObject s = (SoapObject) soap.getProperty(i);
                for (int j = 0; j < s.getPropertyCount(); j++) {
                    if ("NotificationsDetails".equalsIgnoreCase(SoapHelper.getPropertyName(s, j))) {
                        SoapObject l = (SoapObject) s.getProperty(j);
                        String date = SoapHelper.getStringProperty(l, "CreatedDate", "");
                        String jobNo = SoapHelper.getStringProperty(l, "JobNo", "");
                        int notificationId = SoapHelper.getIntegerProperty(l, "NotificationID", 0);
                        int type = SoapHelper.getIntegerProperty(l, "NotificationType", 0);
                        String notificationContent = SoapHelper.getStringProperty(l, "NotificationContent", "");

                        if (!"".equalsIgnoreCase(date) && !"".equalsIgnoreCase(jobNo) && type != 0) {
                            PullNotificationModel m = new PullNotificationModel();
                            m.setCreatedDate(date);
                            m.setJobNo(jobNo);
                            m.setNotificationId(notificationId);
                            m.setMessageType(type);
                            m.setNotificationContent(notificationContent);
                            list.add(m);
                        }
                    }
                }
            }
        }
    }

    public int getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(int returnCode) {
        this.returnCode = returnCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public List<PullNotificationModel> getList() {
        return list;
    }

    public void setList(List<PullNotificationModel> list) {
        this.list = list;
    }
}
