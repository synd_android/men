package com.fcscs.android.mconnectv3.main;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.BaseActivity;
import com.fcscs.android.mconnectv3.common.QRCodeParserFactory;
import com.fcscs.android.mconnectv3.common.ui.BottomBar;
import com.fcscs.android.mconnectv3.common.ui.ComboBox;
import com.fcscs.android.mconnectv3.common.ui.HomeTopBar;
import com.fcscs.android.mconnectv3.common.ui.McBackgroundAsyncTask;
import com.fcscs.android.mconnectv3.common.ui.McDatePickerDialog;
import com.fcscs.android.mconnectv3.common.ui.McProgressAsyncTask;
import com.fcscs.android.mconnectv3.common.ui.McTimePickerDialog;
import com.fcscs.android.mconnectv3.common.ui.RoundCornerListAdapter;
import com.fcscs.android.mconnectv3.common.util.DateTimeHelper;
import com.fcscs.android.mconnectv3.common.util.DialogHelper;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.dao.entity.Location;
import com.fcscs.android.mconnectv3.manager.model.GroupLocationInfo;
import com.fcscs.android.mconnectv3.manager.model.ItemGroupInfo;
import com.fcscs.android.mconnectv3.manager.model.ItemNameInfo;
import com.fcscs.android.mconnectv3.manager.model.ItemTopInfo;
import com.fcscs.android.mconnectv3.manager.model.LocationInfo;
import com.fcscs.android.mconnectv3.manager.model.MediaLibraryConfig;
import com.fcscs.android.mconnectv3.manager.model.ServiceRequest;
import com.fcscs.android.mconnectv3.manager.model.ServiceRequestItem;
import com.fcscs.android.mconnectv3.ws.cconnect.model.guestrequestservice.EngineeringRequestResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.guestrequestservice.FindGroupLocationResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.guestrequestservice.FindItemNameResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.guestrequestservice.FindItemResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.guestrequestservice.FindLocationResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.guestrequestservice.GuestRequestResponse;
import com.fcscs.android.mconnectv3.ws.econnect.ECService;
import com.fcscs.android.mconnectv3.zxing.CaptureActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import info.hoang8f.android.segmented.SegmentedGroup;

/**
 * Created by Tuan Nguyen on 10/27/15.
 */

public class EngineeringRequestActivity extends BaseActivity {

    //region DECLARE VALUE
    protected static final int RQ_TRANSLATE = 1009;
    private static final int RQ_SEARCH_ITEMS = 1000;
    private static final int RQ_SEARCH_LOCATIONS = 1001;
    private static final int RQ_RECORD_VOICE = 1005;
    private static final int RQ_CAPTURE_IMAGE = 1006;
    private final static int DELAY = 0; // Delay n mins;SCANNIN_GROUP_LOCATION
    private final static int SCANNIN_GROUP_LOCATION = 1010; // Delay n mins;
    protected EditText where;
    //    protected EditText serviceItemTv;
    protected EditText date;
    protected Calendar dateAndTime = null;
    //    protected ServiceRequest request = null;
    List<GroupLocationInfo> groupLocationInfoList;
    List<LocationInfo> locationInfoList;
    //    boolean isQRCode = false;
//    List<LocationInfo> locationInfoQRCodeList;
    List<ItemTopInfo> itemTopInfoList;
    List<ItemGroupInfo> itemGroupInfoList;
    List<ItemNameInfo> itemNameInfos;
    ItemNameInfo mCurrenctItemName;
    ComboBox mGroupLocation;
    ComboBox mLocation;
    EditText mWho;
    Spinner mGroupItem;
    Spinner mTopItem;
    ComboBox mItemName;
    Spinner mQty;
    SegmentedGroup mTypeSegment;
    RadioButton mRBGuestRequest;
    RadioButton mRBInterRequest;
    String mCurrecntGroupLocationCode = "";
    String mCurrecntLocationCode = "";
    HashMap<String, List<LocationInfo>> listDataOfLocation;
    //    ArrayAdapter<String> adapterTop;
//    ArrayAdapter<String> adapterGroup;
//    ArrayAdapter<String> adapterName;
    String mRequestor = "";
    EditText remarks;
    boolean isOnlySetDataTop, isOnlySetDataGroup, isOnlySetDataName;

    GuestRequestSender task6;
    File capturedImageFile;
    private BottomBar footBar;
    private ImageView dateClear;
    private EditText time;
    private McDatePickerDialog datePickerDialog;
    private McTimePickerDialog timePickerDialog;
    private ImageView ivMic;
    private ImageView ivCam;
    private ListView serviceItemLv;
    private ItemRoundAdapter serviceItemAdapter;
    private HomeTopBar headBar;
    private GroupLocationLoader task1;
    private LocationLoader task2;
    private ItemLoader task3;
    private EngineeringRequestLoader task5;
    private ItemNameLoader task4;
    ArrayAdapter<String> adapterTop;
    ArrayAdapter<String> adapterGroup;
    //region HANLDE LISTENER
    ComboBox.OnItemComboboxClickListener comboboxClickListener = new ComboBox.OnItemComboboxClickListener() {
        @Override
        public void beginEdit(ComboBox cbx) {
            if (cbx == mGroupLocation)
                getGroupLocation(mGroupLocation.getText(), true);
            else if (cbx == mLocation) {
                mCurrecntGroupLocationCode = "";
                mCurrecntLocationCode = cbx.getText();
                getListLocation(mCurrecntGroupLocationCode, mCurrecntLocationCode);
            }
        }

        public void getListLocation(String groupLoccationCode, String locationCode) {
            String text = mGroupLocation.getText();
            GroupLocationInfo groupLocationInfo = null;
            for (GroupLocationInfo index : groupLocationInfoList) {
                if (index.getName().equalsIgnoreCase(text)) groupLocationInfo = index;
            }
            if (groupLocationInfo != null) {
                mCurrecntGroupLocationCode = groupLocationInfo.getCode();
                mLocation.setText("");
                locationInfoList = null;
                qrCode = "";
            }
            getLocation(mCurrecntGroupLocationCode, locationCode);
        }

        @Override
        public void clickDropdown(ComboBox cbx) {
            if (cbx == mGroupLocation) {
                getGroupLocation("", true);
            } else if (cbx == mLocation) {
                mCurrecntLocationCode = cbx.getText();
                getListLocation(mCurrecntGroupLocationCode, mCurrecntLocationCode);
            }
        }

        @Override
        public void searchButton(ComboBox cbx) {
            if (cbx == mGroupLocation) {
                getGroupLocation(mGroupLocation.getText(), true);
            } else if (cbx == mLocation) {

                getListLocation("", mLocation.getText());
            } else if (cbx == mItemName) {
                selectedItem(cbx, 0);
            }
        }

        @Override
        public void selectedItem(ComboBox cbx, int pos) {
            if (cbx == mGroupLocation) {
                if (groupLocationInfoList != null) {
                    String text = mGroupLocation.getText();
                    GroupLocationInfo info = null;
                    for (GroupLocationInfo index : groupLocationInfoList) {
                        if (index.getName().equalsIgnoreCase(text)) info = index;
                    }
                    if (info != null) {
                        mCurrecntGroupLocationCode = info.getCode();
                        mLocation.setText("");
                        locationInfoList = null;
                        qrCode = "";
                    }
                }
            } else if (cbx == mLocation) {

                if (locationInfoList != null) {
                    LocationInfo info = locationInfoList.get(pos);
                    mCurrecntLocationCode = info.getLocationCodeCode();
                    mGroupLocation.setText(info.getGroupName());
                    mCurrecntGroupLocationCode = info.getGroupCode();
                    getItemTopGroup(mCurrecntLocationCode);
                    ServiceRequest.getInstance().setRoomNo(mCurrecntLocationCode);
                    mGroupItem.setSelection(0);
                    mItemName.setText("");
                    String[] emptySource = new String[0];
                    mItemName.setSuggestionSource(emptySource);
                }
            } else if (cbx == mItemName) {
                if (mTopItem.getSelectedItemPosition() != 0) {
                    isOnlySetDataTop = true;
                    mTopItem.setSelection(0);
                }
                if (itemNameInfos != null && itemNameInfos.size() > 0) {
                    mCurrenctItemName = itemNameInfos.get(pos);

                    int index = -1;
                    for (int i = 0; i < itemGroupInfoList.size(); i++) {
                        ItemGroupInfo itemGroupInfo = itemGroupInfoList.get(i);
                        if (itemGroupInfo.getID().equalsIgnoreCase(mCurrenctItemName.getItemGroupID())) {
                            index = i;
                            break;
                        }
                    }
                    if (index != -1) {
                        //isOnlySetDataGroup = true;
                        mGroupItem.setSelection(index + 1);
                    }
                }

            }
        }
    };
    //    boolean noNeedToGetItemName = false;
    AdapterView.OnItemSelectedListener itemNameClickListener = new AdapterView.OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            if (itemNameInfos != null && itemNameInfos.size() > 0 && pos > 0)
                mCurrenctItemName = itemNameInfos.get(pos - 1);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };
    AdapterView.OnItemSelectedListener itemTopClickListener = new AdapterView.OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            if (isOnlySetDataTop) {
                isOnlySetDataTop = false;
                return;
            }
            if (itemTopInfoList != null && itemTopInfoList.size() > 0 && pos > 0) {
                ItemTopInfo topInfo = itemTopInfoList.get(pos - 1);
                mCurrenctItemName = new ItemNameInfo();
                mCurrenctItemName.setItemID(topInfo.getItemID());
                mCurrenctItemName.setItemName(topInfo.getItemName());
            }

            if (mGroupItem.getSelectedItemPosition() != 0) {
                isOnlySetDataGroup = true;
                mGroupItem.setSelection(0);
            }

            mItemName.setText("");
            String[] emptySource = new String[0];
            mItemName.setSuggestionSource(emptySource);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };
    AdapterView.OnItemSelectedListener itemGroupClickListener = new AdapterView.OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            if (isOnlySetDataGroup) {
                isOnlySetDataGroup = false;
                return;
            }
            if (itemGroupInfoList != null && itemGroupInfoList.size() > 0 && pos > 0) {
                ItemGroupInfo itemGroupInfo = itemGroupInfoList.get(pos - 1);
                mItemName.setText("");
                getItemName(mCurrecntLocationCode, itemGroupInfo.getID());
            }
            if (mTopItem.getSelectedItemPosition() != 0) {
                isOnlySetDataTop = true;
                mTopItem.setSelection(0);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };
    //endregion
    //endregion

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount() && i < 5; i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    //region LIFECYCLE
    private void clearDataSelectRadio() {
        groupLocationInfoList = null;
        locationInfoList = null;
        mLocation.setSuggestionSource(new String[0]);
        mGroupLocation.setSuggestionSource(new String[0]);
        mGroupItem.setSelection(0);
        mTopItem.setSelection(0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.engineering_request);
        listDataOfLocation = new HashMap<String, List<LocationInfo>>();
        mRBGuestRequest = (RadioButton) findViewById(R.id.rb_guest);
        mRBInterRequest = (RadioButton) findViewById(R.id.rb_inter_department);
        mRBGuestRequest.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    mWho.setEnabled(false);
                    mWho.setText("");
                }
                clearDataSelectRadio();
            }
        });
        mRBInterRequest.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    mWho.setEnabled(true);
                }
                clearDataSelectRadio();
            }
        });
        dateClear = (ImageView) findViewById(R.id.commonDateClear);
        dateClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateAndTime = null;
                date.setText("");
                time.setText("");
            }
        });
        mTypeSegment = (SegmentedGroup) findViewById(R.id.rg_service_type);
        mTypeSegment.setTintColor(Color.parseColor("#5894d2"));
        TextView W = (TextView) findViewById(R.id.textView7);
        date = (EditText) findViewById(R.id.commonWhenDate);
        time = (EditText) findViewById(R.id.commonWhenTime);
        date.setHint(getString(R.string.date));
        time.setHint(getString(R.string.time));
        if (isCConnect()) {
            W.setVisibility(View.GONE);
            date.setVisibility(View.GONE);
            time.setVisibility(View.GONE);
            dateClear.setVisibility(View.GONE);
        } else {
            date.setHint(getString(R.string.please_select));
            date.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popupDatePickDialog();
                }
            });
            time.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popupTimePickDialog();
                }
            });
        }
        Button btnQRCode = (Button) findViewById(R.id.btn_qrcode);
        btnQRCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(EngineeringRequestActivity.this, CaptureActivity.class);
                intent.putExtra("type", QRCodeParserFactory.GROUP_LOCATION_REQUEST);
                startActivityForResult(intent, SCANNIN_GROUP_LOCATION);
            }
        });
        serviceItemLv = (ListView) findViewById(R.id.service_item_list);
        serviceItemAdapter = new ItemRoundAdapter(this);
        serviceItemLv.setAdapter(serviceItemAdapter);

        // button bar
        footBar = (BottomBar) findViewById(R.id.footBarView);
        footBar.getLeftTv().setClickable(true);
        footBar.getLeftTv().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAddButtonClick();
            }
        });

        footBar.getMiddleTv().setClickable(true);
        footBar.getMiddleTv().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSumitButtonClick();
            }
        });

        footBar.getRightTv().setClickable(true);
        footBar.getRightTv().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCancelButtonClick();
            }
        });

        ivMic = (ImageView) findViewById(R.id.btn_mic);
        ivMic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(EngineeringRequestActivity.this, VoiceTagActivy.class);
                EngineeringRequestActivity.this.startActivityForResult(i, RQ_RECORD_VOICE);
            }
        });
        ivMic.setVisibility(View.GONE);

        ivCam = (ImageView) findViewById(R.id.camera);
        ivCam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(EngineeringRequestActivity.this, ImageCaptureActivity.class);
                EngineeringRequestActivity.this.startActivityForResult(i, RQ_CAPTURE_IMAGE);
            }
        });
        ivCam.setVisibility(View.INVISIBLE);
        new GetMediaLibraryConfig(this).exec();
        headBar = (HomeTopBar) findViewById(R.id.top_bar);
        headBar.getTitleTv().setText(R.string.engineering_request);
        mGroupLocation = (ComboBox) findViewById(R.id.et_group_location);
        String[] as1 = new String[0];
        mLocation = (ComboBox) findViewById(R.id.et_location);
        mLocation.setSuggestionSource(as1);
        mLocation.setOnItemComboboxClickListener(comboboxClickListener);
        mGroupLocation.setSuggestionSource(as1);
        mGroupLocation.setOnItemComboboxClickListener(comboboxClickListener);

        mTopItem = (Spinner) findViewById(R.id.et_item_top);
        mGroupItem = (Spinner) findViewById(R.id.et_item_group);

//        mTopItem.setSuggestionSource(as1);
//        mTopItem.setOnItemComboboxClickListener(comboboxClickListener);
//        mGroupItem.setSuggestionSource(as1);
//        mGroupItem.setOnItemComboboxClickListener(comboboxClickListener);
        mItemName = (ComboBox) findViewById(R.id.et_item_name);
        mItemName.setSuggestionSource(as1);
        mItemName.setOnItemComboboxClickListener(comboboxClickListener);
        mWho = (EditText) findViewById(R.id.commonWhoEtId);
        mQty = (Spinner) findViewById(R.id.commonQuantitySpId);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(R.layout.spinnerstyle);
        for (int i = 1; i < 100; i++) {
            adapter.add(i + "");
        }
        mQty.setAdapter(adapter);
        adapterTop = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item);
        adapterTop.setDropDownViewResource(R.layout.spinnerstyle);
        adapterTop.add(getString(R.string.request_item_top1));
        mTopItem.setAdapter(adapterTop);
        mTopItem.setOnItemSelectedListener(itemTopClickListener);
        adapterGroup = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item);
        adapterGroup.setDropDownViewResource(R.layout.spinnerstyle);
        adapterGroup.add(getString(R.string.request_item_group1));
        mGroupItem.setAdapter(adapterGroup);
        mGroupItem.setOnItemSelectedListener(itemGroupClickListener);
//        adapterTop = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item);
//        adapterTop.setDropDownViewResource(R.layout.spinnerstyle);
//        adapterTop.add(getString(R.string.request_item_top1));
//        mTopItem.setAdapter(adapterTop);
//        adapterGroup = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item);
//        adapterGroup.setDropDownViewResource(R.layout.spinnerstyle);
//        adapterGroup.add(getString(R.string.request_item_group1));
//        mGroupItem.setAdapter(adapterGroup);
//        adapterName = new ArrayAdapter<String>(EngineeringRequestActivity.this, android.R.layout.simple_spinner_item);
//        adapterName.setDropDownViewResource(R.layout.spinnerstyle);
//        adapterName.add(getString(R.string.request_item_name1));
//        mItemName.setAdapter(adapterName);
//        mTopItem.setOnItemSelectedListener(itemTopClickListener);
//        mGroupItem.setOnItemSelectedListener(itemGroupClickListener);
//        mItemName.setOnItemSelectedListener(itemNameClickListener);
        ServiceRequest.getInstance().setItems(new ArrayList<ServiceRequestItem>());
        ServiceRequest.getInstance().setItem(new ServiceRequestItem());
        ImageView btnTranslate = (ImageView) findViewById(R.id.iv_translate);
        btnTranslate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(EngineeringRequestActivity.this, TranslateActivity.class);
                if (remarks != null && remarks.getText().toString().length() > 0) {
                    i.putExtra("TEXT_TRANSLATE", remarks.getText().toString().trim());
                }
                EngineeringRequestActivity.this.startActivityForResult(i, RQ_TRANSLATE);
            }
        });
        remarks = (EditText) findViewById(R.id.commonRemarkEtId);
        remarks.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                validaterLength(remarks, 255);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        getGroupLocation(mGroupLocation.getText(), false);
    }

    @Override
    protected void onDestroy() {
        ServiceRequest.destroyInstance();
        super.onDestroy();
    }

    private synchronized void popupDatePickDialog() {

        if (datePickerDialog != null && datePickerDialog.isShowing()) {
            return;
        }
        Date currentTime;
        if (dateAndTime == null) {
            initDateAndTime();
            currentTime = DateTimeHelper.getDateByTimeZone(new Date());
        } else {
            currentTime = dateAndTime.getTime();
        }

        datePickerDialog = new McDatePickerDialog(this, currentTime);
        datePickerDialog.setPermanentTitle(getString(R.string.set_date));
        datePickerDialog.setOnClickDone(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                DatePicker datePicker = datePickerDialog.getDatePicker();
                dateAndTime.set(Calendar.YEAR, datePicker.getYear());
                dateAndTime.set(Calendar.MONTH, datePicker.getMonth());
                dateAndTime.set(Calendar.DAY_OF_MONTH, datePicker.getDayOfMonth());
                dateAndTime.set(Calendar.HOUR_OF_DAY, DateTimeHelper.getDateByTimeZone(new Date()).getHours());
                dateAndTime.set(Calendar.MINUTE, DateTimeHelper.getDateByTimeZone(new Date()).getMinutes() + DELAY);
                datePickerDialog.dismiss();
                updateLabel();
            }
        });
        datePickerDialog.show();
    }

    private void initDateAndTime() {
        dateAndTime = Calendar.getInstance(Locale.getDefault());
    }

    private synchronized void popupTimePickDialog() {

        if (timePickerDialog != null && timePickerDialog.isShowing()) {
            return;
        }

        Date currentTime = null;
        if (dateAndTime == null) {
            currentTime = DateTimeHelper.getDateByTimeZone(new Date());
        } else {
            currentTime = dateAndTime.getTime();
        }
        timePickerDialog = new McTimePickerDialog(this, currentTime, true);
        timePickerDialog.setOnClickDone(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (dateAndTime == null) {
                    initDateAndTime();
                }
                TimePicker picker = timePickerDialog.getTimePicker();
                picker.clearFocus();
                dateAndTime.set(Calendar.HOUR_OF_DAY, picker.getCurrentHour());
                dateAndTime.set(Calendar.MINUTE, picker.getCurrentMinute() + DELAY);
                timePickerDialog.dismiss();
                updateLabel();
            }
        });
        timePickerDialog.show();
    }

    private void updateLabel() {
        date.setText(DateTimeHelper.DATE_FORMATTER4.format(dateAndTime.getTime()));
        time.setText(DateTimeHelper.TIME_FORMATTER.format(dateAndTime.getTime()));
        ServiceRequest.getInstance().setScheduledDate(dateAndTime.getTime());
    }

    protected void onAddButtonClick() {
        int intQty = ServiceRequest.getInstance().getItems().size();

        if (ServiceRequest.getInstance().getItems().size() == 3) {
            toast(getString(R.string.max_3_items));
            //..No need by default it is 1...
            //} else if (request.getItem().getQuantity() <= 0) {
            //toast(getString(R.string.please_select_quantity));
        } else {
            String strRem = remarks.getText().toString().trim();
            addServiceItem(strRem);
        }

    }

    private void onCancelButtonClick() {

        if (ServiceRequest.getInstance().getItems().size() > 0) {

            DialogHelper.showYesNoDialog(getCurrentContext(), R.string.confirm_cancel, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    setResult(Activity.RESULT_OK);
                    finish();
                }
            });

        } else {
            setResult(Activity.RESULT_OK);
            finish();
        }

        hideSoftInput(remarks);

    }

    protected void onSumitButtonClick() {
//        if (dateAndTime == null || dateAndTime.getTime() == null) {
//            toast(getString(R.string.please_select_schedule));
//            return;
//        } else
        if (ServiceRequest.getInstance().getItems().size() == 0) {
            toast(getString(R.string.please_select_serviceitem));
            return;
        } else if (mCurrecntLocationCode == null || mCurrecntLocationCode.length() == 0) {
            toast(getString(R.string.please_select_location));
            return;
        } else {
            Date da = null;
            if (McUtils.isNullOrEmpty(date.getEditableText().toString()) == false) {
                if (DateTimeHelper.getDateByTimeZone(new Date()).after(dateAndTime.getTime())) {
                    toast(getString(R.string.please_select_future_time));
                    return;
                }
                da = dateAndTime.getTime();
            }
            submitEngineeringRequest();
        }
    }

    private void submitEngineeringRequest() {
        if (task5 == null || AsyncTask.Status.RUNNING.equals(task5.getStatus()) == false) {
            task5 = new EngineeringRequestLoader(getCurrentContext());
            task5.exec();
        }
    }

//    protected void onServiceItemSelected(ServiceItemViewModel item) {
////		request.getItem().setItemId(item.getCode());
//        request.getItem().setItemId("" + item.getServiceItemId());
//        request.getItem().setName(item.getName());
//        if (item != null) {
//            serviceItemTv.setText(item.getName());
//        }
//    }

    private void addServiceItem(String remarks) {
        if (mCurrenctItemName == null) {
            toast(getString(R.string.please_select_serviceitem));
            return;
        }
        boolean isEx = false;
        ServiceRequestItem mo = ServiceRequest.getInstance().getItem();
        if (mo == null) {
            ServiceRequest.getInstance().setItem(new ServiceRequestItem());
            mo = ServiceRequest.getInstance().getItem();
        }
        if (mCurrenctItemName != null) {
            mo.setItemId(mCurrenctItemName.getItemID());
        }
        mo.setQuantity(mQty.getSelectedItemPosition() + 1);
        mo.setName(mCurrenctItemName.getItemName());
        if (capturedImageFile != null) {
            mo.setCapturedImage(capturedImageFile);
        }
        for (ServiceRequestItem sm : ServiceRequest.getInstance().getItems()) {
            if (McUtils.isNullOrEmpty(sm.getItemId()) == false && sm.getItemId().equals(mo.getItemId())) {
                isEx = true;
                break;
            }
        }

        if (!isEx) {
            //..Modify By Chua :
            if (mo.getQuantity() == 0) {
                mo.setQuantity(1);
            }
            mo.setRemarks(remarks);
            ServiceRequest.getInstance().getItems().add(mo);


            serviceItemAdapter.setItemList(ServiceRequest.getInstance().getItems());
            serviceItemAdapter.notifyDataSetChanged();
            /**
             * Kha fix bug: Add New job  overlap
             */
            // A work around to enlarge the listview in scrollview
            // enlarge
            setListViewHeightBasedOnChildren(serviceItemLv);
            // end bug fix
            clearStatus();
            toast(R.string.add_item_successfully);
        } else {
            toast(R.string.item_has_added);
            clearStatus();
        }
    }

    private void clearStatus() {
        ServiceRequest.getInstance().setItem(new ServiceRequestItem());
//        mItemName.setSelection(0);
        mTopItem.setSelection(0);
        mGroupItem.setSelection(0);
        mItemName.setText("");
        mQty.setSelection(0);
        remarks.setText("");
        capturedImageFile = null;
        mCurrenctItemName = null;

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }
        if (requestCode == SCANNIN_GROUP_LOCATION) {
            String text = data.getStringExtra("result");
            qrCode = text;
            locationInfoList = null;
            getLocation("", "");
            mTopItem.setSelection(0);
            mGroupItem.setSelection(0);
            mItemName.setText("");
            String[] emptySource = new String[0];
            mItemName.setSuggestionSource(emptySource);
//            ServiceItemViewModel item = (ServiceItemViewModel) data.getSerializableExtra("ServiceItemViewModel");
//            if (item != null) {
//                onServiceItemSelected(item);
//            }
        }

        if (requestCode == RQ_SEARCH_LOCATIONS) {
            Location loc = (Location) data.getSerializableExtra("Location");
            if (loc != null) {
                onLocationSelected(loc);
            }
        }

//        if (requestCode == RQ_RECORD_VOICE) {
//            File recordFile = (File) data.getSerializableExtra("record");
//            request.getItem().setRecordedVoice(recordFile);
//        }
//
        if (requestCode == RQ_CAPTURE_IMAGE) {
            capturedImageFile = (File) data.getSerializableExtra("image");
        }

        /**
         * Fix #858 by Kha Tran
         * on 2015/07/10
         */
        if (requestCode == RQ_TRANSLATE) {
            String remarkText = remarks.getText().toString();
            String translatedText = data.getStringExtra(TranslateActivity.EXTRA_TRANSLATED_TEXT);
            if (translatedText != null && translatedText.length() > 0) {
                boolean isMerge = data.getBooleanExtra(TranslateActivity.EXTRA_TRANSLATED_TEXT_IS_MERGE, true);
                if (isMerge) {
                    remarkText = remarkText + "\n" + translatedText;
                } else {
                    remarkText = translatedText;
                }
            }
            remarks.setText(remarkText);
        }

    }

    protected void validaterLength(final EditText qtyEt, int length) {
        Editable editable = qtyEt.getText();
        int len = editable.length();
        if (len > length) {
            int selEndIndex = Selection.getSelectionEnd(editable);
            String str = editable.toString().trim();
            String newStr = str.substring(0, length);
            qtyEt.setText(newStr);
            editable = qtyEt.getText();
            int newLen = editable.length();
            if (selEndIndex > newLen) {
                selEndIndex = editable.length();
            }
            Selection.setSelection(editable, selEndIndex);
        }
    }

    protected void onLocationSelected(Location loc) {
//        if (loc != null) {
//            request.setLocationCode(loc.getLocationCode());
//            where.setText(loc.getName());
//        } else {
//            where.setText("");
//            request.setLocationCode(null);
//        }
    }

    @Override
    public boolean isLoginActivity() {
        return true;
    }

    @Override
    public boolean isHomeActivity() {
        return false;
    }
    //endregion

    //region REQUEST DATA
    protected void getItemName(String locationCode, String groupID) {
        if (task4 == null || AsyncTask.Status.RUNNING.equals(task4.getStatus()) == false) {
            task4 = new ItemNameLoader(getCurrentContext(), locationCode, groupID);
            task4.exec();
        }
    }

    protected void getItemTopGroup(String locationCode) {
        if (task3 == null || AsyncTask.Status.RUNNING.equals(task3.getStatus()) == false) {
            task3 = new ItemLoader(getCurrentContext(), locationCode);
            task3.exec();
        }
    }

    String qrCode = "";

    protected void getLocation(String groupCode, String nameCode) {
//        if (qrCode.length() == 0)
//            locationInfoList = listDataOfLocation.get(groupCode);
//        if (locationInfoList != null) {
//            initDataIntoLocationBox();
//            return;
//        }
        if (task2 == null || AsyncTask.Status.RUNNING.equals(task2.getStatus()) == false) {
            task2 = new LocationLoader(getCurrentContext(), groupCode, nameCode, qrCode);
            task2.exec();
        }
    }

    protected void getGroupLocation(String groupLocation, boolean showDropdown) {
//        if (groupLocationInfoList != null)
//            return;
        if (task1 == null || AsyncTask.Status.RUNNING.equals(task1.getStatus()) == false) {
            task1 = new GroupLocationLoader(getCurrentContext(), groupLocation, showDropdown);
            task1.exec();
        }
    }

    protected void sumitGuestRequest() {

        if (task6 == null || AsyncTask.Status.RUNNING.equals(task6.getStatus()) == false) {
            task6 = new GuestRequestSender(getCurrentContext());
            task6.exec();
        }

    }

    protected String getSuccessmessage(String info, String error) {
        String dt = McUtils.formatDateTime(DateTimeHelper.getDateByTimeZone(new Date()));
        String msg = (info != null && info.length() > 0) ? getString(R.string.job) + " " + info + " " + getString(R.string.created) + " \n" + dt : "";
        return msg + " \n" + error;
    }

    protected String getSuccessmessage(String info) {
        String dt = McUtils.formatDateTime(DateTimeHelper.getDateByTimeZone(new Date()));
        String msg = getString(R.string.job) + " " + info + " " + getString(R.string.created) + " \n" + dt;
        return msg;
    }

    public static class ItemRoundAdapter extends RoundCornerListAdapter<ServiceRequestItem> {

        private Activity activity;

        public ItemRoundAdapter(Activity activity) {
            super(activity, new ArrayList<ServiceRequestItem>(), R.layout.common_service_item);
            this.activity = activity;
        }

        @Override
        public void onBindView(View view, final Context context, int position, final ServiceRequestItem item) {

            final Tag tag = (Tag) view.getTag();
            final ServiceRequestItem model = (ServiceRequestItem) getItem(position);

            String status = String.format("%s X %s", model.getName(), model.getQuantity());
            tag.name.setText(status);

            tag.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ServiceRequest.getInstance().getItems().remove(item);
                    mList = ServiceRequest.getInstance().getItems();
                    notifyDataSetChanged();
                }
            });

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(activity, NewRequestItemDetailActivity.class);
                    i.putExtra("index", ServiceRequest.getInstance().getItems().indexOf(model));
                    activity.startActivity(i);
                }
            });

        }

        @Override
        public View newView(Context context, int position, ViewGroup parent) {
            View v = super.newView(context, position, parent);
            Tag tag = new Tag();

            tag.name = (TextView) v.findViewById(R.id.name);
            tag.delete = (ImageView) v.findViewById(R.id.delete);

            v.setTag(tag);
            return v;
        }

        class Tag {
            TextView name;
            ImageView delete;
        }
    }

    private class GetMediaLibraryConfig extends McBackgroundAsyncTask {

        public GetMediaLibraryConfig(Context context) {
            super(context);
        }

        @Override
        public void doInBackground() {
            MediaLibraryConfig.config = ECService.getInstance().getMediaLibraryConfig(getCurrentContext());
        }

        @Override
        public void onPostExecute() {
            if (MediaLibraryConfig.config != null) {
                if (MediaLibraryConfig.config.isEnableCaptureImage()) {
                    ivCam.setVisibility(View.VISIBLE);
                } else {
                    ivCam.setVisibility(View.INVISIBLE);
                }
                if (MediaLibraryConfig.config.isEnableVoiceImage()) {
                    ivMic.setVisibility(View.GONE);
                } else {
                    ivMic.setVisibility(View.GONE);
                }
            }
        }
    }

    protected class GroupLocationLoader extends McProgressAsyncTask {

        private FindGroupLocationResponse res;
        private String term;
        private boolean showDropdown;

        public GroupLocationLoader(Context context, String term, boolean showDropdown) {
            super(context);
            this.term = term;
            this.showDropdown = showDropdown;
        }

        @Override
        public void doInBackground() {
            res = ECService.getInstance().getGroupLocationListing(getCurrentContext(), term);
        }

        @Override
        public void onPostExecute() {
            if (res != null) {
                groupLocationInfoList = res.getGroupLocationInfoList();
                String[] data = new String[groupLocationInfoList.size()];
                for (int i = 0; i < groupLocationInfoList.size(); i++) {
                    GroupLocationInfo group = groupLocationInfoList.get(i);
                    data[i] = group.getName();
                }
                mGroupLocation.setSuggestionSource(data);
                if (showDropdown)
                    mGroupLocation.showDropdow();
                if (groupLocationInfoList.size() == 0) {
                    mGroupLocation.showToast(getString(R.string.no_groups_found));
                }
            } else {
                mGroupLocation.showToast(getString(R.string.no_groups_found));
            }
        }
    }

    protected class LocationLoader extends McProgressAsyncTask {

        private String termGroup;
        private String termName;
        private String qrCode;
        private FindLocationResponse res;
        Context mContext;

        public LocationLoader(Context context, String termGroup, String termName, String qrCode) {
            super(context);
            this.termGroup = termGroup;
            this.termName = termName;
            this.qrCode = qrCode;
            mContext = context;
        }

        @Override
        public void doInBackground() {
            res = ECService.getInstance().getLocationListing(getCurrentContext(), mRBGuestRequest.isChecked() == true ? 1 : 2, termGroup, termName, qrCode);
        }

        @Override
        public void onPostExecute() {
            if (res != null) {
                locationInfoList = res.getLocationInfoList();
                if (locationInfoList != null && locationInfoList.size() > 0) {
                    if (qrCode.length() == 0)
                        listDataOfLocation.put(mCurrecntGroupLocationCode, locationInfoList);
                    initDataIntoLocationBox();
                    if (qrCode.length() > 0) {
                        int index = -1;
                        for (int i = 0; i < locationInfoList.size(); i++) {
                            LocationInfo group = locationInfoList.get(i);
                            if (group.getLocationCodeQrCode().equalsIgnoreCase(qrCode)) {
                                index = i;
                                break;
                            }
                        }
                        if (index != -1) {
                            LocationInfo infoIndex = locationInfoList.get(index);
                            mCurrecntLocationCode = infoIndex.getLocationCodeCode();
                            mGroupLocation.setText(infoIndex.getGroupName());
                            mCurrecntGroupLocationCode = infoIndex.getGroupCode();
                            getItemTopGroup(mCurrecntLocationCode);
                            ServiceRequest.getInstance().setRoomNo(mCurrecntLocationCode);
                            mLocation.setText(infoIndex.getLocationCodeName());
                        } else {
                            mLocation.showDropdow();
                        }
                    }
                } else {
                    mLocation.showToast(getString(R.string.no_location_found));
                }


            }
        }
    }

    private void initDataIntoLocationBox() {
        String[] data = new String[locationInfoList.size()];
        for (int i = 0; i < locationInfoList.size(); i++) {
            LocationInfo group = locationInfoList.get(i);
            data[i] = group.getLocationCodeName();
        }
        mLocation.setSuggestionSource(data);
        LocationInfo info = locationInfoList.get(0);
        mCurrecntGroupLocationCode = info.getGroupCode();
        if (qrCode.length() == 0)
            mLocation.showDropdow();
    }

    protected class ItemLoader extends McProgressAsyncTask {

        private String locationCode;
        private FindItemResponse res;

        public ItemLoader(Context context, String locationCode) {
            super(context);
            this.locationCode = locationCode;
        }

        @Override
        public void doInBackground() {
            res = ECService.getInstance().getItemListing(getCurrentContext(), mRBGuestRequest.isChecked() == true ? 0 : 1, locationCode);
        }

        @Override
        public void onPostExecute() {
            if (res != null) {
                itemGroupInfoList = res.getItemGroupInfoList();
                itemTopInfoList = res.getItemTopInfoList();
                adapterTop.clear();
                adapterTop.add(getString(R.string.request_item_top1));
                adapterGroup.clear();
                adapterGroup.add(getString(R.string.request_item_group1));
                if (itemTopInfoList.size() > 0) {
                    for (int i = 0; i < itemTopInfoList.size(); i++) {
                        ItemTopInfo index = itemTopInfoList.get(i);
                        adapterTop.add(index.getItemGroupName() + " - " + index.getItemName());
                    }
                    adapterTop.notifyDataSetChanged();
                    mTopItem.setSelection(1);
                }
                if (itemGroupInfoList.size() > 0) {
                    for (int i = 0; i < itemGroupInfoList.size(); i++) {
                        ItemGroupInfo index = itemGroupInfoList.get(i);
                        adapterGroup.add(index.getName());
                    }
                    adapterGroup.notifyDataSetChanged();
                    mTopItem.setSelection(1);
                }
                mRequestor = res.getRequestor();
                if (mRBInterRequest.isChecked()) {
                    mWho.setEnabled(true);
                } else {
                    if (mRequestor == null || mRequestor.length() == 0) {
                        mWho.setEnabled(true);
                    } else {
                        mWho.setEnabled(false);
                    }
                }
                mWho.setText(mRequestor);
                ServiceRequest.getInstance().setGuestName(mRequestor);
            }
//            if (res != null) {
//                itemGroupInfoList = res.getItemGroupInfoList();
//                itemTopInfoList = res.getItemTopInfoList();
//                if (itemTopInfoList.size() > 0) {
//                    String[] itemTopList = new String[itemTopInfoList.size()];
//                    for (int i = 0; i < itemTopInfoList.size(); i++) {
//                        ItemTopInfo index = itemTopInfoList.get(i);
//                        itemTopList[i] = index.getItemName();
//                    }
//                    mTopItem.setSuggestionSource(itemTopList);
//                }
//                if (itemGroupInfoList.size() > 0) {
//                    String[] itemGroupList = new String[itemGroupInfoList.size()];
//                    for (int i = 0; i < itemGroupInfoList.size(); i++) {
//                        ItemGroupInfo index = itemGroupInfoList.get(i);
//                        itemGroupList[i] = index.getName();
//                    }
//                    mGroupItem.setSuggestionSource(itemGroupList);
//                }
//                mRequestor = res.getRequestor();
//                mWho.setText(mRequestor);
//                ServiceRequest.getInstance().setGuestName(mRequestor);
//            }
        }
    }

    protected class ItemNameLoader extends McProgressAsyncTask {

        private String locationCode;
        private String itemGroupID;
        private FindItemNameResponse res;

        public ItemNameLoader(Context context, String locationCode, String itemGroupID) {
            super(context);
            this.locationCode = locationCode;
            this.itemGroupID = itemGroupID;
        }

        @Override
        public void doInBackground() {
            res = ECService.getInstance().getItemNameListing(getCurrentContext(), mRBGuestRequest.isChecked() == true ? 1 : 2, locationCode, itemGroupID);
        }

        @Override
        public void onPostExecute() {
            if (res != null) {
                itemNameInfos = res.getItemNameInfos();
//                adapterName.clear();
//                adapterName.add(getString(R.string.request_item_name1));
                if (itemNameInfos.size() > 0) {
                    String[] itemNameList = new String[itemNameInfos.size()];
                    for (int i = 0; i < itemNameInfos.size(); i++) {
                        ItemNameInfo index = itemNameInfos.get(i);
                        itemNameList[i] = index.getItemName();
//                        adapterName.add(index.getItemName());
                    }
                    mItemName.setSuggestionSource(itemNameList);
                    mItemName.showDropdow();
                    if (itemNameInfos.size() == 1) {
                        mCurrenctItemName = itemNameInfos.get(0);
                    }
//                    adapterName.notifyDataSetChanged();
                } else {
                    mItemName.showToast(getString(R.string.no_item_found));
                }
            } else {
                mItemName.showToast(getString(R.string.no_item_added));
            }
        }
    }

    protected class EngineeringRequestLoader extends McProgressAsyncTask {

        private EngineeringRequestResponse res;

        public EngineeringRequestLoader(Context context) {
            super(context);
        }

        @Override
        public void doInBackground() {
            res = ECService.getInstance().createEngineeringRequest(getCurrentContext(), ServiceRequest.getInstance().getItems(), ((dateAndTime != null && dateAndTime.getTime() != null) ? dateAndTime.getTime() : null), mRequestor, mCurrecntLocationCode, /*mRBGuestRequest.isChecked() == true ? "1" : */"2");
        }

        @Override
        public void onPostExecute() {
            if (res != null) {
                if (res.getReturnCode().equalsIgnoreCase("0")) {
                    final String returnCode1 = res.getReturnCode1();
                    final String returnCode2 = res.getReturnCode2();
                    final String returnCode3 = res.getReturnCode3();
                    if (returnCode1.equalsIgnoreCase("1")
                            || returnCode2.equalsIgnoreCase("1")
                            || returnCode3.equalsIgnoreCase("1")) {
                        StringBuffer errMsg = new StringBuffer();
                        if (returnCode1.equalsIgnoreCase("1")) {
                            ServiceRequestItem item = ServiceRequest.getInstance().getItems().get(0);
                            errMsg.append(", " + item.getName());
                        }
                        if (returnCode2.equalsIgnoreCase("1")) {
                            ServiceRequestItem item = ServiceRequest.getInstance().getItems().get(1);
                            errMsg.append(", " + item.getName());
                        }
                        if (returnCode3.equalsIgnoreCase("1")) {
                            ServiceRequestItem item = ServiceRequest.getInstance().getItems().get(2);
                            errMsg.append(", " + item.getName());
                        }
                        toast(getString(R.string.engineering_error_1) + errMsg.toString());
                    } else if (returnCode1.equalsIgnoreCase("4")
                            || returnCode2.equalsIgnoreCase("4")
                            || returnCode3.equalsIgnoreCase("4")) {
                        StringBuffer errMsg = new StringBuffer();
                        if (returnCode1.equalsIgnoreCase("4")) {
                            ServiceRequestItem item = ServiceRequest.getInstance().getItems().get(0);
                            errMsg.append(", " + item.getName());
                        }
                        if (returnCode2.equalsIgnoreCase("4")) {
                            ServiceRequestItem item = ServiceRequest.getInstance().getItems().get(1);
                            errMsg.append(", " + item.getName());
                        }
                        if (returnCode3.equalsIgnoreCase("4")) {
                            ServiceRequestItem item = ServiceRequest.getInstance().getItems().get(2);
                            errMsg.append(", " + item.getName());
                        }
                        toast(getString(R.string.engineering_error_4) + errMsg.toString());
                    } else if (returnCode1.equalsIgnoreCase("3") || returnCode2.equalsIgnoreCase("3") || returnCode3.equalsIgnoreCase("3")) {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(EngineeringRequestActivity.this);
                        alertDialogBuilder.setMessage(getString(R.string.engineering_request_duplicate));

                        alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                ArrayList<ServiceRequestItem> listItemSubmit = getListItemCorrect(returnCode1, returnCode2, returnCode3, true);
                                ServiceRequest.getInstance().setItems(listItemSubmit);
                                sumitGuestRequest();
                            }
                        });
                        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // Do nothing
                                ArrayList<ServiceRequestItem> listItemSubmit = getListItemCorrect(returnCode1, returnCode2, returnCode3, false);
                                if (listItemSubmit.size() > 0) {
                                    ServiceRequest.getInstance().setItems(listItemSubmit);
                                    sumitGuestRequest();
                                }
                            }
                        });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    } else {
                        ArrayList<ServiceRequestItem> listItemSubmit = getListItemCorrect(returnCode1, returnCode2, returnCode3, false);

                        ServiceRequest.getInstance().setItems(listItemSubmit);
                        sumitGuestRequest();
                    }

                } else if (res.getReturnCode().equalsIgnoreCase("3")) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(EngineeringRequestActivity.this);
                    alertDialogBuilder.setMessage(getString(R.string.engineering_request_duplicate));

                    alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            sumitGuestRequest();
                        }
                    });
                    alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Do nothing
                        }
                    });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();

                } else {
                    if (res.getErrorMessage() != null && res.getErrorMessage().length() > 0)
                        toast(res.getErrorMessage());
                    else if (res.getReturnCode().equalsIgnoreCase("1")) {
                        toast(getString(R.string.engineering_error_1));
                    } else if (res.getReturnCode().equalsIgnoreCase("2")) {
                        toast(getString(R.string.engineering_error_2));
                    } else if (res.getReturnCode().equalsIgnoreCase("4")) {
                        toast(getString(R.string.engineering_error_4));
                    } else {
                        toast(getString(R.string.engineering_error_all));
                    }
                }
            }
        }
    }

    protected ArrayList<ServiceRequestItem> getListItemCorrect(String returnCode1, String returnCode2, String returnCode3, boolean isGetDuclicate) {
        ArrayList<ServiceRequestItem> listItemSubmit = new ArrayList<ServiceRequestItem>();
        ArrayList<ServiceRequestItem> listItemCheck = ServiceRequest.getInstance().getItems();
        if (listItemCheck.size() > 0 && (returnCode1.equalsIgnoreCase("0") || (returnCode1.equalsIgnoreCase("3") && isGetDuclicate))) {
            listItemSubmit.add(listItemCheck.get(0));
        }
        if (listItemCheck.size() > 1 && (returnCode2.equalsIgnoreCase("0") || (returnCode2.equalsIgnoreCase("3") && isGetDuclicate))) {
            listItemSubmit.add(listItemCheck.get(1));
        }
        if (listItemCheck.size() > 2 && (returnCode3.equalsIgnoreCase("0") || (returnCode3.equalsIgnoreCase("3") && isGetDuclicate))) {
            listItemSubmit.add(listItemCheck.get(2));
        }
        return listItemSubmit;
    }

    protected class GuestRequestSender extends McProgressAsyncTask {

        private GuestRequestResponse res;

        public GuestRequestSender(Context context) {
            super(context);
        }

        @Override
        public void doInBackground() {
            res = ECService.getInstance().createGuestLocationRequest(getCurrentContext(), mCurrecntLocationCode, ServiceRequest.getInstance().getItems(), ((dateAndTime != null && dateAndTime.getTime() != null) ? dateAndTime.getTime() : null), mRequestor, mRBGuestRequest.isChecked());
            if (res != null && res.getReturnCode().equals("0")) {
                int count = ServiceRequest.getInstance().getItems().size();
                ServiceRequestItem item = null;
                String jobNo = null;
                if (count >= 1) {
                    item = ServiceRequest.getInstance().getItems().get(0);
                    jobNo = res.getErrorMessage1();
                    ECService.getInstance().postMediaLibraryContent(getCurrentContext(), item.getImageTag(), item.getCapturedImage(), 2, 1,
                            jobNo);
                    ECService.getInstance().postMediaLibraryContent(getCurrentContext(), item.getVoiceTag(), item.getRecordedVoice(), 2, 2,
                            jobNo);
                }
                if (count >= 2) {
                    item = ServiceRequest.getInstance().getItems().get(1);
                    jobNo = res.getErrorMessage2();
                    ECService.getInstance().postMediaLibraryContent(getCurrentContext(), item.getImageTag(), item.getCapturedImage(), 2, 1,
                            jobNo);
                    ECService.getInstance().postMediaLibraryContent(getCurrentContext(), item.getVoiceTag(), item.getRecordedVoice(), 2, 2,
                            jobNo);
                }
                if (count >= 3) {
                    item = ServiceRequest.getInstance().getItems().get(2);
                    jobNo = res.getErrorMessage3();
                    ECService.getInstance().postMediaLibraryContent(getCurrentContext(), item.getImageTag(), item.getCapturedImage(), 2, 1,
                            jobNo);
                    ECService.getInstance().postMediaLibraryContent(getCurrentContext(), item.getVoiceTag(), item.getRecordedVoice(), 2, 2,
                            jobNo);
                }
            }

        }

        @Override
        public void onPostExecute() {
            if (res != null) {
                if (isCConnect()) {
                    if (res.getJobNos() != null) {
                        if (res.getJobNos().size() > 0) {
                            StringBuilder sb1 = new StringBuilder();
                            for (int i = 0; i < res.getJobNos().size(); i++) {
                                sb1.append(res.getJobNos().get(i));
                                if (i != (res.getJobNos().size() - 1)) {
                                    sb1.append(" " + ",");
                                }
                            }
                            toast(getSuccessmessage(sb1.toString()));
                            setResult(Activity.RESULT_OK);
                            finish();
                        }
                    } else {
                        toast(R.string.request_create_error);
                    }
                } else {
                    if (res.getReturnCode().equals("0")) {
                        StringBuffer msg = new StringBuffer();
                        StringBuffer errMsg = new StringBuffer();
                        ServiceRequest request = ServiceRequest.getInstance();
                        if (!res.getErrorMessage3().equals("") && res.getReturnCode3().equals("0")) {
                            msg.append(res.getErrorMessage3());
                            request.removeItemAtIndex(2);
                        } else if (request.getItems().size() >= 3 && !res.getErrorMessage3().equals("") && !res.getReturnCode3().equals("0")) {
                            ServiceRequestItem item = request.getItems().get(1);
                            errMsg.append(res.getErrorMessage3() + " - " + item.getName());
                        }
                        if (!res.getErrorMessage2().equals("") && res.getReturnCode2().equals("0")) {
                            msg.append(msg.toString().length() > 0 ? ", " + res.getErrorMessage2() : "" + res.getErrorMessage2());
                            request.removeItemAtIndex(1);
                        } else if (request.getItems().size() >= 2 && !res.getErrorMessage2().equals("") && !res.getReturnCode2().equals("0")) {
                            ServiceRequestItem item = request.getItems().get(1);
                            errMsg.append(errMsg.toString().length() > 0 ? " \n" : "" + res.getErrorMessage2() + " - " + item.getName());
                        }
                        if (!res.getErrorMessage1().equals("") && res.getReturnCode1().equals("0")) {
                            msg.append(msg.toString().length() > 0 ? ", " + res.getErrorMessage1() : "" + res.getErrorMessage1());
                            request.removeItemAtIndex(0);
                        } else if (request.getItems().size() >= 1 && !res.getErrorMessage1().equals("") && !res.getReturnCode1().equals("0")) {
                            ServiceRequestItem item = request.getItems().get(0);
                            errMsg.append(errMsg.toString().length() > 0 ? " \n" : "" + res.getErrorMessage1() + " - " + item.getName());
                        }
                        int count = request.getItems().size();
                        if (count == 0) {
                            toast(getSuccessmessage(msg.toString()));
                            setResult(Activity.RESULT_OK);
                            finish();
                        } else {
                            toast(getSuccessmessage(msg.toString(), errMsg.toString()));
                            serviceItemAdapter.setItemList(request.getItems());
                            serviceItemAdapter.notifyDataSetChanged();
                        }
                    } else {
                        StringBuffer msg = new StringBuffer();
                        if (!res.getErrorMessage1().equals("")) {
                            msg.append(res.getErrorMessage1());
                        }
                        if (!res.getErrorMessage2().equals("")) {
                            msg.append("," + res.getErrorMessage2());
                        }
                        if (!res.getErrorMessage3().equals("")) {
                            msg.append("," + res.getErrorMessage3());
                        }
                        toast(R.string.request_create_error);
                    }
                }
            }
        }

    }
    //endregion
}

