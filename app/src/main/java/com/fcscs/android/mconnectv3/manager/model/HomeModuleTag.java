package com.fcscs.android.mconnectv3.manager.model;

import java.io.Serializable;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class HomeModuleTag implements Serializable {

    public interface OnViewBindListener {
        void onViewBind(int position, View convertView, ViewGroup parent);
    }

    /**
     *
     */
    private static final long serialVersionUID = -6073356493217002003L;
    private int iconResId;
    private int textResId;
    private View.OnClickListener clickListener;
    private OnViewBindListener bindListener;

    private TextView messageCount = null;
    private TextView text = null;
    private ImageView icon = null;

    public HomeModuleTag() {
    }

    public HomeModuleTag(int iconResId, int textResId) {
        super();
        this.iconResId = iconResId;
        this.textResId = textResId;
    }

    public View.OnClickListener getClickListener() {
        return clickListener;
    }

    public void setClickListener(View.OnClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public int getIconResId() {
        return iconResId;
    }

    public void setIconResId(int iconResId) {
        this.iconResId = iconResId;
    }

    public int getTextResId() {
        return textResId;
    }

    public void setTextResId(int textResId) {
        this.textResId = textResId;
    }

    public TextView getMessageCount() {
        return messageCount;
    }

    public void setMessageCount(TextView messageCount) {
        this.messageCount = messageCount;
    }

    public TextView getText() {
        return text;
    }

    public void setText(TextView text) {
        this.text = text;
    }

    public ImageView getIcon() {
        return icon;
    }

    public void setIcon(ImageView icon) {
        this.icon = icon;
    }

    public OnViewBindListener getBindListener() {
        return bindListener;
    }

    public void setBindListener(OnViewBindListener bindListener) {
        this.bindListener = bindListener;
    }

}
