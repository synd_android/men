package com.fcscs.android.mconnectv3;

import java.util.ArrayList;
import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.fcscs.android.mconnectv3.common.McConstants;
import com.fcscs.android.mconnectv3.common.ServiceExceptionHandler;
import com.fcscs.android.mconnectv3.common.util.McUtils;

public class CheckNetworkConnectivityReceiver extends BroadcastReceiver {
    private static List<CheckConnectivity> _list = new ArrayList<CheckConnectivity>();
    final private static String TAG = "CheckNetworkConnectivityReceiver";

    public static void register(CheckConnectivity c) {
        if (!_list.contains(c)) {
            _list.add(c);
            Log.d(TAG, "Adding.....");
        }
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {
        String action = intent.getAction();
        if ("android.net.conn.CONNECTIVITY_CHANGE".equalsIgnoreCase(action) ||
                "android.net.wifi.WIFI_STATE_CHANGED".equalsIgnoreCase(action)) {
            ServiceExceptionHandler.setHandler(context);
            boolean status = !McUtils.isNetworkOff(context);
            if (McConstants.ENABLE_DEBUG_MODE && !status) {
                McUtils.saveLogToSDCard("network connection unavailable");
            }
            Log.d("CheckNetwork", "OnReceive");
            if (_list != null && _list.size() > 0) {
                Log.d("CheckNetwork", "Size: " + _list.size());
                for (CheckConnectivity c : _list) {
                    c.hasConenction(status);
                }
            }
        }
    }

    public interface CheckConnectivity {
        public void hasConenction(boolean hasConnection);
    }
}
