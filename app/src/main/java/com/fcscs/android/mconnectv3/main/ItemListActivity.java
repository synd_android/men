package com.fcscs.android.mconnectv3.main;

import java.util.ArrayList;
import java.util.HashSet;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils.TruncateAt;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;

import com.fcscs.android.mconnectv3.R;

import com.fcscs.android.mconnectv3.common.BaseActivity;
import com.fcscs.android.mconnectv3.common.McConstants;
import com.fcscs.android.mconnectv3.common.ui.HomeTopBar;
import com.fcscs.android.mconnectv3.common.ui.NumberPicker;
import com.fcscs.android.mconnectv3.common.ui.ObservableScrollView;
import com.fcscs.android.mconnectv3.common.ui.ObservableScrollView.ScrollViewListener;
import com.fcscs.android.mconnectv3.manager.model.ServiceItemIdModel;
import com.fcscs.android.mconnectv3.manager.model.ServiceItemModel;
import com.fcscs.android.mconnectv3.ws.cconnect.ServiceItemService;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.SearchServiceItemResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.ServiceItemViewModel;

public class ItemListActivity extends BaseActivity {

    private TableLayout mainTl;
    private TextView saveBtn;

    private ArrayList<ServiceItemViewModel> serviceItems = null;
    private ArrayList<ServiceItemModel> result = null;
    private HashSet<ServiceItemIdModel> addedItems = new HashSet<ServiceItemIdModel>();

    private HomeTopBar homeTopBar;
    private ObservableScrollView itemsSv;

    private int total = 0;
    private int type;
    private String term = null;
    private Long departmentId = null;

    @SuppressWarnings("unchecked")
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_items);
        Bundle bundle = getIntent().getExtras();

        type = bundle.getInt(McConstants.KEY_TYPE, 0);
        total = bundle.getInt(McConstants.KEY_TOTAL, 0);
        term = bundle.getString(McConstants.KEY_TERM);
        departmentId = bundle.get(McConstants.KEY_DEP_ID) != null ? bundle.getLong(McConstants.KEY_DEP_ID) : null;

        mainTl = (TableLayout) findViewById(R.id.tl_service_items);
        mainTl.removeAllViews();

        homeTopBar = (HomeTopBar) findViewById(R.id.top_bar);
        homeTopBar.getTitleTv().setText(bundle.get(McConstants.KEY_TITLE).toString());

        itemsSv = (ObservableScrollView) findViewById(R.id.sv_item_list);
        itemsSv.setScrollViewListener(new ScrollViewListener() {

            @Override
            public void onScrollChanged(ObservableScrollView scrollView, int x, int y, int oldx, int oldy) {

                int childCount = scrollView.getChildCount();
                if (childCount > 0 && type == McConstants.TYPE_SEARCH_ITEMS) {
                    View view = (View) scrollView.getChildAt(childCount - 1);
                    int diff = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));
                    if (diff == 0) {
                        // bottom is hit
                        if (serviceItems != null && serviceItems.size() < total) {
                            new ServiceItemLoader().execute();
                        }
                    }
                }

            }
        });

        serviceItems = (ArrayList<ServiceItemViewModel>) bundle.get(McConstants.KEY_RESULT);
        if (serviceItems != null) {
            for (ServiceItemViewModel model : serviceItems) {
                addServiceItem(model);
            }
        }

        saveBtn = (TextView) findViewById(R.id.save_btn);
        saveBtn.setClickable(true);
        saveBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                result = new ArrayList<ServiceItemModel>();
                for (int i = 0; i < mainTl.getChildCount(); i++) {
                    TableRow tr = (TableRow) mainTl.getChildAt(i);
                    NumberPicker num = (NumberPicker) tr.getChildAt(1);
                    if (serviceItems != null && i < serviceItems.size() && num.getValue() > 0) {
                        result.add(new ServiceItemModel(serviceItems.get(i), num.getValue()));
                    }
                }
                Intent i = new Intent();
                i.putExtra(McConstants.KEY_RESULT, result);
                setResult(RESULT_OK, i);
                finish();
            }
        });
    }

    public void addServiceItem(ServiceItemViewModel model) {
        TextView tv = new TextView(this);
        tv.setLayoutParams(new LayoutParams(230, LayoutParams.WRAP_CONTENT, 1));
        if (isCConnect()) {
            tv.setText(model.getName() + "(" + model.getCategory() + ")");
        } else {
            tv.setText(model.getName());
        }
        tv.setTextColor(android.graphics.Color.WHITE);
        tv.setEllipsize(TruncateAt.MIDDLE);
        tv.setMaxLines(1);
        tv.setSingleLine(true);
        tv.setPadding(8, 30, 8, 0);

        NumberPicker np = new NumberPicker(this);
        np.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT, 1));
        np.setPadding(0, 12, 0, 12);
        TableRow tr = new TableRow(this);
        tr.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
        tr.setBackgroundDrawable(getResources().getDrawable(R.drawable.guest_request_gradient));
        tr.addView(tv);
        tr.addView(np);
        mainTl.addView(tr);

        addedItems.add(new ServiceItemIdModel(model.getServiceItemId(), model.getCategoryId()));
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent i = new Intent();
            i.putExtra(McConstants.KEY_RESULT, new ArrayList<ServiceItemModel>());
            setResult(RESULT_OK, i);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    private class ServiceItemLoader extends AsyncTask<Void, Void, SearchServiceItemResponse> {
        Dialog progress;
        private ServiceItemService siInst;

        @Override
        protected void onPreExecute() {
            progress = ProgressDialog.show(ItemListActivity.this, "Loading Items", "Please wait...");
            super.onPreExecute();
        }

        @Override
        protected SearchServiceItemResponse doInBackground(Void... params) {
            siInst = ServiceItemService.getInstanceNonUI();
            return siInst.findServiceItems(ItemListActivity.this, term, departmentId, addedItems.size(), 10);
        }

        @Override
        protected void onPostExecute(SearchServiceItemResponse res) {
            super.onPostExecute(res);
            progress.dismiss();

            if (res != null) {
                total = res.getTotal();
                if (res.getServiceItems().size() > 0) {
                    for (ServiceItemViewModel model : res.getServiceItems()) {
                        ServiceItemIdModel id = new ServiceItemIdModel(model.getServiceItemId(), model.getCategoryId());
                        if (!addedItems.contains(id)) {
                            serviceItems.add(model);
                            addServiceItem(model);
                        }
                    }
                }
            }

        }

    }

    @Override
    public boolean isLoginActivity() {
        return true;
    }

    @Override
    public boolean isHomeActivity() {
        return false;
    }
}
