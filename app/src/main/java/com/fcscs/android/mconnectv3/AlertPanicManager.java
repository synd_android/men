package com.fcscs.android.mconnectv3;

import android.content.Context;
import android.media.MediaPlayer;

/**
 * Created by NguyenMinhTuan on 1/12/16.
 */
public class AlertPanicManager {
    private static AlertPanicManager instance;
    Context mContext;
    MediaPlayer mp;

    public static AlertPanicManager getInstance() {
        if (instance == null) {
            instance = new AlertPanicManager();
        }
        instance.mContext = McApplication.application;
        return instance;
    }

    public void playSoundAlert() {
        if (mp != null) {
            if (mp.isPlaying()) {
                mp.stop();
                mp.reset();
                mp.release();
            }
            mp = null;

        }
        mp = MediaPlayer.create(mContext, R.raw.alert);
        mp.setLooping(true);
        mp.start();
    }

    public void stopSoundAlert() {
        if (mp != null && mp.isPlaying()) {
            mp.stop();
            mp.reset();
            mp.release();
            mp = null;
        }
    }
}
