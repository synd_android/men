package com.fcscs.android.mconnectv3.ws.econnect.model.commonservice;

import com.fcscs.android.mconnectv3.common.util.SoapHelper;

import org.ksoap2.serialization.SoapObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GetFreeRunnersResponse extends GetEngineeringRunnersResponse {

    /**
     *
     */
    private static final long serialVersionUID = 2846090543654867439L;

//    private int returnCode;
//    private String errorMsg;
//    private List<EngineeringRunnersDetails> list;


    public GetFreeRunnersResponse() {

    }

    public GetFreeRunnersResponse(SoapObject soap) {
        SoapObject status = (SoapObject) soap.getProperty("ResponseStatus");
        returnCode = SoapHelper.getIntegerProperty(status, "ReturnCode", 1);
        errorMsg = SoapHelper.getStringProperty(status, "ErrorMsg", "");

        list = new ArrayList<EngineeringRunnersDetails>();
        for (int i = 0; i < soap.getPropertyCount(); i++) {
            if ("FreeRunnersDetailsListing".equalsIgnoreCase(SoapHelper.getPropertyName(soap, i))) {
                SoapObject s = (SoapObject) soap.getProperty(i);
                for (int j = 0; j < s.getPropertyCount(); j++) {
                    if ("FreeRunnersDetails".equalsIgnoreCase(SoapHelper.getPropertyName(s, j))) {
                        SoapObject l = (SoapObject) s.getProperty(j);
                        int RunnerUserID = SoapHelper.getIntegerProperty(l, "RunnerUserID", 0);
                        String RunnerUserName = SoapHelper.getStringProperty(l, "RunnerUserName", "");
                        String RunnerUserFullName = SoapHelper.getStringProperty(l, "RunnerUserFullName", "");
                        String RunnerDepartment = SoapHelper.getStringProperty(l, "RunnerDepartment", "");
                        if (!"".equalsIgnoreCase(RunnerUserName) && RunnerUserID != 0) {
                            EngineeringRunnersDetails m = new EngineeringRunnersDetails();
                            m.setRunnerUserID(RunnerUserID);
                            m.setRunnerUserName(RunnerUserName);
                            m.setRunnerUserFullName(RunnerUserFullName);
                            m.setRunnerDepartment(RunnerDepartment);
                            list.add(m);
                        }
                    }
                }
            }
        }
    }
//
//    public int getReturnCode() {
//        return returnCode;
//    }
//
//    public void setReturnCode(int returnCode) {
//        this.returnCode = returnCode;
//    }
//
//    public String getErrorMsg() {
//        return errorMsg;
//    }
//
//    public void setErrorMsg(String errorMsg) {
//        this.errorMsg = errorMsg;
//    }

//    public List<EngineeringRunnersDetails> getList() {
//        return list;
//    }
//
//    public void setList(List<EngineeringRunnersDetails> list) {
//        this.list = list;
//    }
}
