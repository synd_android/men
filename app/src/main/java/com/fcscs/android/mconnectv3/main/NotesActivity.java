package com.fcscs.android.mconnectv3.main;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.fcscs.android.mconnectv3.R;

import com.fcscs.android.mconnectv3.common.BaseActivity;
import com.fcscs.android.mconnectv3.common.ui.HomeTopBar;
import com.fcscs.android.mconnectv3.common.ui.RoundCornerListAdapter;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.ProfileNote;

public class NotesActivity extends BaseActivity {
    public static final String NOTES = "Notes";
    private Bundle bundle;
    private ArrayList<ProfileNote> list;

    private ListView listview;
    private HomeTopBar topBar;
    private NotesRoundAdapter adp;

    public static Intent createIntent(Context ctx, ArrayList<ProfileNote> notes) {
        Intent intent = new Intent(ctx, NotesActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(NOTES, notes);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.notes);
        bundle = getIntent().getExtras();
        topBar = (HomeTopBar) findViewById(R.id.top_bar);
        topBar.getTitleTv().setText(R.string.note);

        listview = (ListView) findViewById(R.id.notes_list);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void onStart() {
        super.onStart();
        if (bundle.get(NOTES) != null) {
            list = (ArrayList<ProfileNote>) bundle.get(NOTES);
            adp = new NotesRoundAdapter(this, list, R.layout.notes_list_item);
            listview.setAdapter(adp);
        }
    }


    @Override
    public boolean isLoginActivity() {
        return true;
    }

    @Override
    public boolean isHomeActivity() {
        return false;
    }

    private class NotesRoundAdapter extends RoundCornerListAdapter<ProfileNote> {

        public NotesRoundAdapter(Context context, List<ProfileNote> list, int layout) {
            super(context, list, layout);
        }

        @Override
        public void onBindView(View view, Context context, int position, ProfileNote item) {

            final Tag tag = (Tag) view.getTag();
            final ProfileNote model = (ProfileNote) getItem(position);

            tag.typeCode.setText(model.getTypeCode());
            tag.typeDescription.setPadding(5, 10, 5, 10);
            tag.typeDescription.setText(model.getTypeDesc());
            tag.noteDescription.setText(model.getNoteDesc());
        }

        @Override
        public View newView(Context context, int position, ViewGroup parent) {
            View v = super.newView(context, position, parent);
            Tag tag = new Tag();

            tag.typeCode = (TextView) v.findViewById(R.id.notes_typecode);
            tag.typeDescription = (TextView) v.findViewById(R.id.notes_typedesc);
            tag.noteDescription = (TextView) v.findViewById(R.id.notes_desc);

            v.setTag(tag);
            return v;
        }

        class Tag {
            TextView typeCode;
            TextView typeDescription;
            TextView noteDescription;
        }

    }
}
