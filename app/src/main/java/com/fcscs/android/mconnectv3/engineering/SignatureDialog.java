package com.fcscs.android.mconnectv3.engineering;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View;

import com.fcscs.android.mconnectv3.R;
import com.guardanis.sigcap.NoSignatureException;
import com.guardanis.sigcap.SignatureDialogBuilder;
import com.guardanis.sigcap.SignatureInputView;

import java.io.File;

/**
 * Created by FCS on 7/6/17.
 */

public class SignatureDialog {
    public interface SignatureEventListener {
        public void onSignatureEntered(File savedFile);

        public void onSignatureInputCanceled();

        public void onSignatureInputError(Throwable e);
    }

    public void show(Activity activity, final SignatureDialog.SignatureEventListener eventListener) {
        final View view = buildView(activity);

        final SignatureInputView inputView = (SignatureInputView) view.findViewById(com.guardanis.sigcap.R.id.sig__input_view);

        new AlertDialog.Builder(activity)
                .setTitle(com.guardanis.sigcap.R.string.sig__default_dialog_title)
                .setView(view)
                .setPositiveButton(R.string.done, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        try {
                            if (!inputView.isSignatureInputAvailable())
                                throw new NoSignatureException("No signature found");

                            File saved = inputView.saveSignature();

                            eventListener.onSignatureEntered(saved);
                        } catch (Exception e) {
                            e.printStackTrace();

                            eventListener.onSignatureInputError(e);
                        }
                    }
                })
                .setNegativeButton(R.string.clear, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        inputView.undoLastSignaturePath();
                    }
                })
                .show();

        view.findViewById(com.guardanis.sigcap.R.id.sig__action_undo)
                .setVisibility(View.GONE);
//                .setOnClickListener(new View.OnClickListener() {
//                    public void onClick(View view) {
//                        inputView.undoLastSignaturePath();
//                    }
//                });
    }

    protected View buildView(Activity activity) {
        return activity.getLayoutInflater()
                .inflate(com.guardanis.sigcap.R.layout.sig__default_dialog, null, false);
    }

}
