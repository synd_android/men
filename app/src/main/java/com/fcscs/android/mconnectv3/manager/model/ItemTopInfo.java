package com.fcscs.android.mconnectv3.manager.model;

import com.fcscs.android.mconnectv3.common.util.SoapHelper;

import org.ksoap2.serialization.SoapObject;

import java.io.Serializable;

/**
 * Created by NguyenMinhTuan on 11/4/15.
 */
public class ItemTopInfo implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String ItemGroupID = "";
    private String ItemGroupCode = "";
    private String ItemGroupName = "";
    private String ItemID = "";
    private String ItemCode = "";
    private String ItemName = "";
    private String ItemJobTrackingDuration = "";
    private String ServiceCodeCode = "";
    private String HTID = "";

    public ItemTopInfo() {
    }

    public ItemTopInfo(SoapObject soap) {
        ItemGroupID = SoapHelper.getStringProperty(soap, "ItemGroupID", "");
        ItemGroupCode = SoapHelper.getStringProperty(soap, "ItemGroupCode", "");
        ItemGroupName = SoapHelper.getStringProperty(soap, "ItemGroupName", "");
        ItemID = SoapHelper.getStringProperty(soap, "ItemID", "");
        ItemCode = SoapHelper.getStringProperty(soap, "ItemCode", "");
        ItemName = SoapHelper.getStringProperty(soap, "ItemName", "");
        ItemJobTrackingDuration = SoapHelper.getStringProperty(soap, "ItemJobTrackingDuration", "");
        ServiceCodeCode = SoapHelper.getStringProperty(soap, "ServiceCodeCode", "");
        HTID = SoapHelper.getStringProperty(soap, "HTID", "");
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getItemGroupID() {
        return ItemGroupID;
    }

    public void setItemGroupID(String itemGroupID) {
        ItemGroupID = itemGroupID;
    }

    public String getItemGroupCode() {
        return ItemGroupCode;
    }

    public void setItemGroupCode(String itemGroupCode) {
        ItemGroupCode = itemGroupCode;
    }

    public String getItemGroupName() {
        return ItemGroupName;
    }

    public void setItemGroupName(String itemGroupName) {
        ItemGroupName = itemGroupName;
    }

    public String getItemID() {
        return ItemID;
    }

    public void setItemID(String itemID) {
        ItemID = itemID;
    }

    public String getItemCode() {
        return ItemCode;
    }

    public void setItemCode(String itemCode) {
        ItemCode = itemCode;
    }

    public String getItemName() {
        return ItemName;
    }

    public void setItemName(String itemName) {
        ItemName = itemName;
    }

    public String getItemJobTrackingDuration() {
        return ItemJobTrackingDuration;
    }

    public void setItemJobTrackingDuration(String itemJobTrackingDuration) {
        ItemJobTrackingDuration = itemJobTrackingDuration;
    }

    public String getServiceCodeCode() {
        return ServiceCodeCode;
    }

    public void setServiceCodeCode(String serviceCodeCode) {
        ServiceCodeCode = serviceCodeCode;
    }

    public String getHTID() {
        return HTID;
    }

    public void setHTID(String HTID) {
        this.HTID = HTID;
    }

}