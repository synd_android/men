package com.fcscs.android.mconnectv3.ws.econnect.model.commonservice;

import java.io.Serializable;

public class AckAdHocResponse implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4902163054265113609L;

    private String returnCode;
    private boolean success;

    public AckAdHocResponse() {
    }

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

}
