package com.fcscs.android.mconnectv3;

import java.util.Iterator;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttAsyncClient;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttAsyncClient;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;

import com.fcscs.android.mconnectv3.common.util.McUtils;

import java.util.HashMap;

public class MQTTService extends Service {

    private static final String TAG = "MQTTService";
    private static boolean hasWifi = false;
    private static boolean hasMmobile = false;
    private Thread thread;
    private ConnectivityManager mConnMan;
    private volatile IMqttAsyncClient mqttClient;
    private String deviceId;

    //    public static final int DISCONNECTED = 0;
    //    public static final int CONNECTING = 1;
    //    public static final int CONNECTED = 2;
    //    private int state = DISCONNECTED;
    private HashMap<String, Integer> subscriptions = new HashMap<String, Integer>();

    private Handler mHandler;
    public static final int STATE_CHANGE = 0;
    //    public static final String TOPIC = "testMQTT";
    //    public static final String URL = "tcp://14.161.36.97:1884";
    /**
     * Defines at what time the last action was taken by the client (this is
     * used to determine if a ping should be sent or not)
     */
    private String mURL;
    private String mTopic;

    public void setmURL(String mURL) {
        this.mURL = mURL;
    }

    public void setmTopic(String mTopic) {
        this.mTopic = mTopic;
    }

    private long last_action = 0;
    MQTTBroadcastReceiver receiver = null;

    class MQTTBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (ConnectivityManager.CONNECTIVITY_ACTION.equalsIgnoreCase(action)) {
                IMqttToken token;
                boolean hasConnectivity = false;
                boolean hasChanged = false;
                NetworkInfo infos[] = mConnMan.getAllNetworkInfo();

                for (int i = 0; i < infos.length; i++) {
                    if (infos[i].getTypeName().equalsIgnoreCase("MOBILE")) {
                        if ((infos[i].isConnected() != hasMmobile)) {
                            hasChanged = true;
                            hasMmobile = infos[i].isConnected();
                        }
                        Log.d(TAG, infos[i].getTypeName() + " is " + infos[i].isConnected());
                    } else if (infos[i].getTypeName().equalsIgnoreCase("WIFI")) {
                        if ((infos[i].isConnected() != hasWifi)) {
                            hasChanged = true;
                            hasWifi = infos[i].isConnected();
                        }
                        Log.d(TAG, infos[i].getTypeName() + " is " + infos[i].isConnected());
                    }
                }

                hasConnectivity = hasMmobile || hasWifi;
                Log.v(TAG,
                        "hasConn: " + hasConnectivity + " hasChange: " + hasChanged + " - " + (mqttClient == null
                                || !mqttClient.isConnected()));
                if (hasConnectivity && hasChanged && (mqttClient == null || !mqttClient.isConnected())) {
                    doConnect();
                } else if (!hasConnectivity && mqttClient != null && mqttClient.isConnected()) {
                    Log.d(TAG, "doDisconnect()");
                    try {
                        token = mqttClient.disconnect();
                        token.waitForCompletion(1000);
                    } catch (MqttException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    ;

    public class MqttBinder extends Binder {
        public MQTTService getService() {
            return MQTTService.this;
        }
    }

    @Override
    public void onCreate() {
        IntentFilter intentf = new IntentFilter();
        setClientID();
        intentf.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(receiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        mConnMan = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
    }

    public void onDestroy() {
        if (receiver != null) unregisterReceiver(receiver);
    }

    private void setClientID() {
        String address = McUtils.getMacAddr();
        if (address != null) {
            deviceId = (System.currentTimeMillis() + address.replace(":", "")).substring(0, 23);
        } else {
            deviceId = MqttAsyncClient.generateClientId();
        }
    }

    int callbacks = 1;

    public void subscribe(String topic) {
        mTopic = topic;
        if (mqttClient != null && mqttClient.isConnected()) {
            IMqttToken token;
            try {
                token = mqttClient.subscribe(topic, 0);
                token.waitForCompletion(5000);

                last_action = System.currentTimeMillis();

                registerSubscription(topic, callbacks++);
                Log.i("subscribe", "subscribe ok topic:" + topic);
            } catch (MqttException e) {
                e.printStackTrace();
            }
        } else {
            Log.e(TAG, "You need to be connected first!");
        }
    }

    /**
     * @param topic
     * @return
     */
    private boolean registerSubscription(String topic, int callback) {
        subscriptions.put(topic, callback);
        return true;
    }

    public void publish(String payload) {
        publish(mTopic, payload);
    }

    public void publish(String topic, String payload) {
        try {
            if (mqttClient != null && mqttClient.isConnected()) {

                payload = deviceId + "|" + payload;
                mqttClient.publish(topic, payload.getBytes(), 1, false, null, new IMqttActionListener() {

                    @Override
                    public void onFailure(IMqttToken arg0, Throwable arg1) {
                        System.out.println("mqtt publish failed");
                    }

                    @Override
                    public void onSuccess(IMqttToken arg0) {
                        System.out.println("mqtt publish success");
                        last_action = System.currentTimeMillis();
                    }
                });
            }
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    private synchronized void setState(int state, String param2) {

        if (mHandler != null)
        // Give the new state to the Handler so the UI Activity can update
        {
            mHandler.obtainMessage(STATE_CHANGE, state, -1, param2).sendToTarget();
        }
    }

    public void doConnect() {
        Log.d(TAG, "doConnect()");
        IMqttToken token;
        MqttConnectOptions options = new MqttConnectOptions();
        options.setCleanSession(true);
        try {
            mqttClient = new MqttAsyncClient(mURL, deviceId, new MemoryPersistence());
            token = mqttClient.connect();
            token.waitForCompletion(3500);

            mqttClient.setCallback(new MqttEventCallback());

            token = mqttClient.subscribe(mTopic, 0);
            token.waitForCompletion(5000);
        } catch (MqttSecurityException e) {
            e.printStackTrace();
        } catch (MqttException e) {
            switch (e.getReasonCode()) {
                case MqttException.REASON_CODE_BROKER_UNAVAILABLE:
                case MqttException.REASON_CODE_CLIENT_TIMEOUT:
                case MqttException.REASON_CODE_CONNECTION_LOST:
                case MqttException.REASON_CODE_SERVER_CONNECT_ERROR:
                    Log.v(TAG, "c" + e.getMessage());
                    e.printStackTrace();
                    break;
                case MqttException.REASON_CODE_FAILED_AUTHENTICATION:
                    Intent i = new Intent("RAISEALLARM");
                    i.putExtra("ALLARM", e);
                    Log.e(TAG, "b" + e.getMessage());
                    break;
                default:
                    Log.e(TAG, "a" + e.getMessage());
            }
        }
    }

    /**
     * Send the disconnect message.
     */
    public void disconnect() {
        if (mqttClient != null && mqttClient.isConnected()) {
            try {
                mqttClient.disconnect();
            } catch (MqttException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Set the UI handler for this Service.
     */
    public void setHandler(Handler mHandler) {
        this.mHandler = mHandler;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.v(TAG, "onStartCommand()");
        return START_STICKY;
    }

    private class MqttEventCallback implements MqttCallback {

        @Override
        public void connectionLost(Throwable arg0) {

        }

        @Override
        public void deliveryComplete(IMqttDeliveryToken arg0) {

        }

        @Override
        @SuppressLint("NewApi")
        public void messageArrived(String topic, final MqttMessage msg) throws Exception {
            String payload = new String(msg.getPayload());
            if (payload == null || TextUtils.isEmpty(payload)) {
                return;
            }
            Log.i("messageArrived", payload);

            if (!isJSONValid(payload)) {
                return;
            }

            Bundle mUIMessageBundle = new Bundle();
            try {
                // map json to bundle

                JSONObject jsonObject = new JSONObject(payload);
                Iterator<String> keys = jsonObject.keys();
                while (keys.hasNext()) {
                    String key = (String) keys.next();
                    if (jsonObject.get(key) instanceof JSONObject) {
                        JSONObject jsonObject1 = ((JSONObject) jsonObject.get(key));
                        Iterator<String> key1s = jsonObject1.keys();
                        while (key1s.hasNext()) {
                            String key1 = (String) key1s.next();
                            if (jsonObject1.get(key1) instanceof String) {
                                mUIMessageBundle.putString(key1, jsonObject.getString(key));
                            }
                        }
                    } else if (jsonObject.get(key) instanceof String) {
                        mUIMessageBundle.putString(key, jsonObject.getString(key));
                    } else if (jsonObject.get(key) instanceof JSONArray) {
                        JSONArray array = jsonObject.getJSONArray(key);
                        for (int j = 0; j < array.length(); j++) {
                            if (array.get(j) instanceof JSONObject) {
                                JSONObject jsonObject1 = array.getJSONObject(j);
                                Iterator<String> key1s = jsonObject1.keys();
                                while (key1s.hasNext()) {
                                    String key1 = (String) key1s.next();
                                    if (jsonObject1.get(key1) instanceof String) {
                                        mUIMessageBundle.putString(key1, jsonObject.getString(key));
                                    }
                                }
                            }
                        }
                    }
                }
                Message message = mHandler.obtainMessage(); // get a message
                message.setData(mUIMessageBundle); // attach bundle
                mHandler.sendMessage(message);

                //handleDataReceive(mUIMessageBundle);
            } catch (JSONException ex) {
                Log.i("MQTTService", "JSONException " + ex.getMessage());
            }
        }
    }

    private boolean isJSONValid(String text) {
        try {
            new JSONObject(text);
        } catch (JSONException ex) {
            // edited, to include @Arthur's comment
            // e.g. in case JSONArray is valid as well...
            try {
                new JSONArray(text);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }

    public boolean isConnected() {
        return mqttClient != null && mqttClient.isConnected();
    }

    public String getThread() {
        return Long.valueOf(thread.getId()).toString();
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.i(TAG, "onBind");
        return mBinder;
    }

    // This is the object that receives interactions from clients. See
    // RemoteService for a more complete example.
    private final IBinder mBinder = new MqttBinder();
}