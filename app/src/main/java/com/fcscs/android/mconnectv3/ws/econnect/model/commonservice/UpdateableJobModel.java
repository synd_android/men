package com.fcscs.android.mconnectv3.ws.econnect.model.commonservice;


public class UpdateableJobModel {
    public static final int IS_RUNNER_JOB = 1;
    public static final int IS_NOT_RUNNER_JOB = 0;

    private String jobNumber;
    private int updateable;

    public String getJobNumber() {
        return jobNumber;
    }

    public void setJobNumber(String jobNumber) {
        this.jobNumber = jobNumber;
    }

    public int getUpdateable() {
        return updateable;
    }

    public void setUpdateable(int updateable) {
        this.updateable = updateable;
    }


}
