package com.fcscs.android.mconnectv3.engineering;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.BaseActivity;
import com.fcscs.android.mconnectv3.common.ui.HomeTopBar;
import com.fcscs.android.mconnectv3.common.ui.McProgressAsyncTask;
import com.fcscs.android.mconnectv3.common.ui.RoundCornerListAdapter;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.dao.entity.Building;
import com.fcscs.android.mconnectv3.dao.entity.Floor;
import com.fcscs.android.mconnectv3.dao.entity.Location;
import com.fcscs.android.mconnectv3.manager.model.EquipmentDetails;
import com.fcscs.android.mconnectv3.ws.cconnect.CommonRequestService;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.GetBuildingsResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.GetFloorsResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.GetLocationsResponse;
import com.fcscs.android.mconnectv3.ws.econnect.ECService;
import com.fcscs.android.mconnectv3.ws.econnect.model.commonservice.GetEquipmentsResponse;

import java.util.ArrayList;
import java.util.List;

public class EquipmentListActivity extends BaseActivity {

    private HomeTopBar headBar;
    private ListView listView;
    //    private EditText searchFiled;
//    private Spinner buildingsp;
    private Building currentBuilding;
    //    private Spinner floorsp;
    private Floor currentFloor;
    protected List<EquipmentDetails> mListEquipment;
    private String backterm = null;
    protected LocationAdapter adapter;
    //    private ArrayAdapter<Building> buildingAdapter;
//    private ArrayAdapter<Floor> floorAdapter;
    private boolean firstLoading;
//    private BuildingFloorTask task;
//    protected FindLocationsTask task1;

    private String strStop4Debug = "";
    private android.widget.Toast toast;
    private String strToastMsg = "";
    private int intPageSize = 10;
    private int intPageIndex = 0;
    private boolean blnIsScrollAtTopMostPosition = false;
    private boolean blnIsScrollDown = false;
    private boolean blnLoadingMore = false;
    private int intLastFirstVisibleItem;
    public static final String KEY_EQUIPMENT = "EQUIPMENT";
    public static final String KEY_LOCATION = "LOCATION";
    public static final String KEY_LIST_EQUIPMENT = "LIST_EQUIPMENT";
    private String mLocation = "";

    @SuppressWarnings("unchecked")
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.equipment_list);
        firstLoading = true;

        Bundle bundle = getIntent().getExtras();
//        mListEquipment = (ArrayList<EquipmentDetails>) bundle.get("ResultList");
        mLocation = (String) bundle.get(KEY_LOCATION);
        if (mListEquipment == null) {
            mListEquipment = new ArrayList<EquipmentDetails>();
        }

        headBar = (HomeTopBar) findViewById(R.id.top_bar);
        headBar.getTitleTv().setText(R.string.inter_dept_request);


        listView = (ListView) findViewById(R.id.locationListviewId);
        //..Get listview current position - used to maintain scroll position...
        int intCurrentPosition = listView.getFirstVisiblePosition();
        adapter = new LocationAdapter(getCurrentContext(), mListEquipment, R.layout.search_location_list_item);
        listView.setAdapter(adapter);
        listView.requestFocus();
        McUtils.hideIME(this);
        firstLoading = false;
        listView.setSelectionFromTop(intCurrentPosition + 1, 0);

        //..Created By 	: Chua Kam Hoong
        //..Date 		: 20 May 2015
        listView.setOnScrollListener(new android.widget.AbsListView.OnScrollListener() {
            //@Override
            public void onScrollStateChanged(android.widget.AbsListView view, int scrollState) {
                if (blnIsScrollDown == true) {
                    blnIsScrollDown = false;
                    int intTotalCount = view.getCount() - 1;
                    if (view.getLastVisiblePosition() == intTotalCount) {
                        intPageIndex = intPageIndex + 1;
                        //..For Debug page index...
                        //strToastMsg = "Page Index :" + intPageIndex + " When scroll down.";
                        //toast = android.widget.Toast.makeText(getCurrentContext(), strToastMsg, android.widget.Toast.LENGTH_SHORT);
                        //toast.show();

                        if (!firstLoading) {
//                            if (task1 == null || AsyncTask.Status.RUNNING.equals(task1.getStatus()) == false) {
                            //..Modify By 	: Chua Kam Hoong
                            //..Date 		: 20 May 2015
                            //task1 = new FindLocationsTask(getCurrentContext(), searchFiled.getText().toString(), null, currentFloor);
                            //task1 = new FindLocationsTask(getCurrentContext(), intPageSize, intPageIndex, searchFiled.getText().toString(), null, currentFloor);
//                                task1 = new FindLocationsTask(getCurrentContext(), intPageIndex, searchFiled.getText().toString(), null, currentFloor);
//                                task1.exec();
//                            }
                        }//..End of firstLoading...
                    }
                }
            }

            @Override
            public void onScroll(android.widget.AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (intLastFirstVisibleItem < firstVisibleItem) {
                    //android.util.Log.i("SCROLLING DOWN","TRUE");
                    //toast = android.widget.Toast.makeText(mContex, "You are scrolling down", android.widget.Toast.LENGTH_SHORT);
                    //toast.show();
                    int intLastInScreen = firstVisibleItem + visibleItemCount;
                    if ((intLastInScreen == totalItemCount) && !(blnLoadingMore)) {
                        blnIsScrollDown = true;
                        //android.widget.Toast.makeText(mContex, "Bottom has been reached, Page Index : " + intPageIndex, android.widget.Toast.LENGTH_SHORT).show();
                    }
                }
                if (intLastFirstVisibleItem > firstVisibleItem) {
                    //blnIsScrollUp = true;
                    //android.util.Log.i("SCROLLING UP","TRUE");
                    //toast = android.widget.Toast.makeText(mContex, "You are scrolling up", android.widget.Toast.LENGTH_SHORT);
                    //toast.show();
                    if (firstVisibleItem == 0) {
                        blnIsScrollAtTopMostPosition = true;
                    }
                }
                intLastFirstVisibleItem = firstVisibleItem;
            }    //..End of onScroll...
        });
        new LoadListEquipment(this, mLocation).execute();

    }

    private class LoadListEquipment extends McProgressAsyncTask {
        Context mContext;
        GetEquipmentsResponse mRes;
        String location;

        public LoadListEquipment(Context context, String location) {
            super(context);
            mContext = context;
            this.location = location;
        }

        @Override
        public void doInBackground() {
            if (!TextUtils.isEmpty(location)) {
                mRes = ECService.getInstance().getEquipments(mContext, mLocation);
            }
            if (mRes != null) {
                mListEquipment.addAll(mRes.getList());
            }
        }

        @Override
        public void onPostExecute() {
//            adapter = new LocationAdapter(getCurrentContext(), mListEquipment, R.layout.search_location_list_item);
//            listView.setAdapter(adapter);
//            listView.requestFocus();
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }


    class LocationAdapter extends RoundCornerListAdapter<EquipmentDetails> {

        public LocationAdapter(Context context, List<EquipmentDetails> list, int layout) {
            super(context, list, layout);
        }

        @Override
        public void onBindView(View view, Context context, int position, EquipmentDetails item) {

            final Tag tag = (Tag) view.getTag();
            final EquipmentDetails model = getItem(position);
            if (model == null) return;
            tag.name.setText(model.getEquipmentName());
            tag.code.setText(model.getEquipmentNumber());

            view.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent i = new Intent();
                    i.putExtra(KEY_EQUIPMENT, model);
                    setResult(RESULT_OK, i);
                    finish();
                }
            });
        }

        @Override
        public View newView(Context context, int position, ViewGroup parent) {
            View v = super.newView(context, position, parent);
            Tag tag = new Tag();

            tag.name = (TextView) v.findViewById(R.id.item_name);
            tag.code = (TextView) v.findViewById(R.id.item_code);

            v.setTag(tag);
            return v;
        }

        class Tag {
            TextView name;
            TextView code;
        }

    }

    @Override
    public boolean isLoginActivity() {
        return true;
    }

    @Override
    public boolean isHomeActivity() {
        return false;
    }

}