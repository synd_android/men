package com.fcscs.android.mconnectv3.main;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.BaseActivity;
import com.fcscs.android.mconnectv3.common.ui.HomeTopBar;
import com.fcscs.android.mconnectv3.common.util.PrefsUtil;

public class MinibarActivity extends BaseActivity {
    private HomeTopBar homeTopBar;
    private static final int REQ_CODE_SEARCH_ITEMS = 2;
    private ListView minibarLv;
    private List<String> minbarList = null;

    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.minbar);

        homeTopBar = (HomeTopBar) findViewById(R.id.top_bar);
        homeTopBar.getTitleTv().setText(R.string.minibar);
        prepareData();

        minibarLv = (ListView) findViewById(R.id.minbar_listview);
        minibarLv.setSelector(R.drawable.spacebar);
        minibarLv.setClickable(true);
        minibarLv.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                String selReq = minbarList.get(pos);
                Intent i = null;
                if (getString(R.string.total_minibar).equals(selReq)) {
                    i = new Intent(MinibarActivity.this, TotalMinbarActivity.class);
                } else if (getString(R.string.itemized_minibar).equals(selReq)) {
                    i = new Intent(MinibarActivity.this, ItemizedMinbarActivity.class);
                }
                if (i != null) {
                    startActivityForResult(i, REQ_CODE_SEARCH_ITEMS);
                }
            }
        });
        MinbarAdapter adapter = new MinbarAdapter(this, minbarList);
        minibarLv.setAdapter(adapter);

    }

    public void prepareData() {
        minbarList = new ArrayList<String>();
        int type = PrefsUtil.getMinibarType(this);
        if (type == 0 || type == 1) {
            minbarList.add(getString(R.string.total_minibar));
        }
        if (type == 0 || type == 2) {
            minbarList.add(getString(R.string.itemized_minibar));
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            setResult(RESULT_OK);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    private class MinbarAdapter extends BaseAdapter {
        private List<String> list;
        private Context context;

        public MinbarAdapter(Context context, List<String> list) {
            this.context = context;
            this.list = list;
        }

        public int getCount() {
            return list.size();
        }

        public Object getItem(int position) {
            return list.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(context);
                convertView = inflater.inflate(R.layout.simple_list_item_2, null);
            }
            TextView textView0 = (TextView) convertView.findViewById(R.id.simple_item_0);
            textView0.setText(list.get(position));
            ImageView imageview = (ImageView) convertView.findViewById(R.id.simple_item_1);
            switch (position) {
                case 0:
                    imageview.setBackgroundResource(R.drawable.minibartotal_btn);
                    break;
                case 1:
                    imageview.setBackgroundResource(R.drawable.minibaritem_btn);
                    break;
            }

            ImageView imageview2 = (ImageView) convertView.findViewById(R.id.list_icon);
            imageview2.setBackgroundResource(R.drawable.list_icon);
            return convertView;
        }

    }

    @Override
    public boolean isLoginActivity() {
        return true;
    }

    @Override
    public boolean isHomeActivity() {
        return false;
    }

}
