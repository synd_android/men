package com.fcscs.android.mconnectv3.main;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.BaseActivity;
import com.fcscs.android.mconnectv3.common.ui.HomeTopBar;
import com.fcscs.android.mconnectv3.common.ui.McProgressAsyncTask;
import com.fcscs.android.mconnectv3.common.ui.RoundCornerListAdapter;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.dao.entity.Building;
import com.fcscs.android.mconnectv3.dao.entity.Floor;
import com.fcscs.android.mconnectv3.dao.entity.Location;
import com.fcscs.android.mconnectv3.ws.cconnect.CommonRequestService;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.GetBuildingsResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.GetFloorsResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice.GetLocationsResponse;
import com.fcscs.android.mconnectv3.ws.econnect.ECService;

public class SearchLocationActivity extends BaseActivity {

    private HomeTopBar headBar;
    private ListView listView;
    private EditText searchFiled;
    private Spinner buildingsp;
    private Building currentBuilding;
    private Spinner floorsp;
    private Floor currentFloor;
    protected List<Location> locationList;
    private String backterm = null;
    protected LocationAdapter adapter;
    private ArrayAdapter<Building> buildingAdapter;
    private ArrayAdapter<Floor> floorAdapter;
    private boolean firstLoading;
    private int countFirstLoading = 0;
    private BuildingFloorTask task;
    protected FindLocationsTask task1;

    private String strStop4Debug = "";
    private android.widget.Toast toast;
    private String strToastMsg = "";
    private int intPageSize = 10;
    private int intPageIndex = 0;
    private boolean blnIsScrollAtTopMostPosition = false;
    private boolean blnIsScrollDown = false;
    private boolean blnLoadingMore = false;
    private int intLastFirstVisibleItem;

    @SuppressWarnings("unchecked")
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_location);
        firstLoading = true;

        Bundle bundle = getIntent().getExtras();
        locationList = (ArrayList<Location>) bundle.get("ResultList");
        if (locationList == null) {
            locationList = new ArrayList<Location>();
        }

        backterm = bundle.getString("searchTerm");

        headBar = (HomeTopBar) findViewById(R.id.top_bar);
        headBar.getTitleTv().setText(R.string.searchlocation);

        searchFiled = (EditText) findViewById(R.id.searchLocationEtId);
        searchFiled.setHint(R.string.location);
        searchFiled.setText(backterm == null ? "" : backterm);
        searchFiled.setOnKeyListener(new OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:
                            if (task1 == null || AsyncTask.Status.RUNNING.equals(task1.getStatus()) == false) {
                                //..Modify By 	: Chua Kam Hoong
                                //..Date 		: 20 May 2015
                                intPageIndex = 1;
                                //task1 = new FindLocationsTask(getCurrentContext(), intPageSize, intPageIndex, searchFiled.getText().toString(), currentBuilding, currentFloor);
                                task1 = new FindLocationsTask(getCurrentContext(), intPageIndex, searchFiled.getText().toString(), currentBuilding, currentFloor);
                                task1.exec();
                            }
                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        });

        buildingAdapter = new ArrayAdapter<Building>(getCurrentContext(), android.R.layout.simple_spinner_item);
        buildingAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Building b = new Building();
        b.setName(getString(R.string.allbuilding));
        buildingAdapter.add(b);

        buildingsp = (Spinner) findViewById(R.id.buildingSpId);
        buildingsp.setAdapter(buildingAdapter);
        buildingsp.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int pos, long arg3) {

                currentBuilding = buildingAdapter.getItem(pos);
                if (pos == 0) {
                    currentBuilding = null;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        if (isEconnect()) {
            buildingsp.setVisibility(View.GONE);
        }

        floorAdapter = new ArrayAdapter<Floor>(getCurrentContext(), android.R.layout.simple_spinner_item);
        floorAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Floor f = new Floor();
        f.setName(getString(R.string.allfloor));
        floorAdapter.add(f);

        floorsp = (Spinner) findViewById(R.id.floorSpId);
        floorsp.setAdapter(floorAdapter);
        floorsp.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                currentFloor = floorAdapter.getItem(pos);
                if (pos == 0) {
                    currentFloor = null;
                }
                if (++countFirstLoading > 1) {
                    if (task1 == null || AsyncTask.Status.RUNNING.equals(task1.getStatus()) == false) {
                        intPageIndex = 1;
                        //..Modify By 	: Chua Kam Hoong
                        //..Date 		: 20 May 2015
                        //task1 = new FindLocationsTask(getCurrentContext(), searchFiled.getText().toString(), null, currentFloor);
                        //task1 = new FindLocationsTask(getCurrentContext(), intPageSize, intPageIndex, searchFiled.getText().toString(), null, currentFloor);
                        task1 = new FindLocationsTask(getCurrentContext(), intPageIndex, searchFiled.getText().toString(), null, currentFloor);
                        task1.exec();
                    }
                }//..End of firstLoading..
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });


        listView = (ListView) findViewById(R.id.locationListviewId);
        //..Get listview current position - used to maintain scroll position...
        int intCurrentPosition = listView.getFirstVisiblePosition();
        adapter = new LocationAdapter(getCurrentContext(), locationList, R.layout.search_location_list_item);
        listView.setAdapter(adapter);
        listView.requestFocus();
        McUtils.hideIME(this);
        firstLoading = false;
        listView.setSelectionFromTop(intCurrentPosition + 1, 0);

        //..Created By 	: Chua Kam Hoong
        //..Date 		: 20 May 2015
        listView.setOnScrollListener(new android.widget.AbsListView.OnScrollListener() {
            //@Override
            public void onScrollStateChanged(android.widget.AbsListView view, int scrollState) {
                if (blnIsScrollDown == true) {
                    blnIsScrollDown = false;
                    int intTotalCount = view.getCount() - 1;
                    if (view.getLastVisiblePosition() == intTotalCount) {
                        intPageIndex = intPageIndex + 1;
                        //..For Debug page index...
                        //strToastMsg = "Page Index :" + intPageIndex + " When scroll down.";
                        //toast = android.widget.Toast.makeText(getCurrentContext(), strToastMsg, android.widget.Toast.LENGTH_SHORT);
                        //toast.show();

                        if (++countFirstLoading > 1) {
                            if (task1 == null || AsyncTask.Status.RUNNING.equals(task1.getStatus()) == false) {
                                //..Modify By 	: Chua Kam Hoong
                                //..Date 		: 20 May 2015
                                //task1 = new FindLocationsTask(getCurrentContext(), searchFiled.getText().toString(), null, currentFloor);
                                //task1 = new FindLocationsTask(getCurrentContext(), intPageSize, intPageIndex, searchFiled.getText().toString(), null, currentFloor);
                                task1 = new FindLocationsTask(getCurrentContext(), intPageIndex, searchFiled.getText().toString(), null, currentFloor);
                                task1.exec();
                            }
                        }//..End of firstLoading...
                    }
                }
            }

            @Override
            public void onScroll(android.widget.AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (intLastFirstVisibleItem < firstVisibleItem) {
                    //android.util.Log.i("SCROLLING DOWN","TRUE");
                    //toast = android.widget.Toast.makeText(mContex, "You are scrolling down", android.widget.Toast.LENGTH_SHORT);
                    //toast.show();
                    int intLastInScreen = firstVisibleItem + visibleItemCount;
                    if ((intLastInScreen == totalItemCount) && !(blnLoadingMore)) {
                        blnIsScrollDown = true;
                        //android.widget.Toast.makeText(mContex, "Bottom has been reached, Page Index : " + intPageIndex, android.widget.Toast.LENGTH_SHORT).show();
                    }
                }
                if (intLastFirstVisibleItem > firstVisibleItem) {
                    //blnIsScrollUp = true;
                    //android.util.Log.i("SCROLLING UP","TRUE");
                    //toast = android.widget.Toast.makeText(mContex, "You are scrolling up", android.widget.Toast.LENGTH_SHORT);
                    //toast.show();
                    if (firstVisibleItem == 0) {
                        blnIsScrollAtTopMostPosition = true;
                    }
                }
                intLastFirstVisibleItem = firstVisibleItem;
            }    //..End of onScroll...
        });


        if (task == null || AsyncTask.Status.RUNNING.equals(task.getStatus()) == false) {
            task = new BuildingFloorTask(getCurrentContext());
            task.exec();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    class BuildingFloorTask extends McProgressAsyncTask {

        private GetBuildingsResponse buildingRes;
        private GetFloorsResponse floorRes;

        public BuildingFloorTask(Context context) {
            super(context);
        }

        @Override
        public void doInBackground() {
            if (isCConnect()) {
                buildingRes = CommonRequestService.getInstance().findBuildings(getCurrentContext());
                floorRes = CommonRequestService.getInstance().findFloors(getCurrentContext());
            } else {
                floorRes = ECService.getInstance().findFloors(getCurrentContext());
            }
        }

        @Override
        public void onPostExecute() {

            if (isCConnect()) {

                buildingAdapter.clear();
                Building b = new Building();
                b.setName(getString(R.string.allbuilding));
                buildingAdapter.add(b);
                if (buildingRes != null && buildingRes.getBuildingList() != null) {
                    for (Building b1 : buildingRes.getBuildingList()) {
                        buildingAdapter.add(b1);
                    }
                }
                buildingAdapter.notifyDataSetChanged();

            }

            floorAdapter.clear();
            Floor b = new Floor();
            b.setName(getString(R.string.allfloor));
            floorAdapter.add(b);
            if (floorRes != null && floorRes.getFloorList() != null) {
                for (Floor b1 : floorRes.getFloorList()) {
                    floorAdapter.add(b1);
                }
            }
            floorAdapter.notifyDataSetChanged();

        }

    }

    class LocationAdapter extends RoundCornerListAdapter<Location> {

        public LocationAdapter(Context context, List<Location> list, int layout) {
            super(context, list, layout);
        }

        @Override
        public void onBindView(View view, Context context, int position, Location item) {

            final Tag tag = (Tag) view.getTag();
            final Location model = getItem(position);
            if (model == null) return;
            tag.name.setText(model.getName());
            tag.code.setText(model.getLocationCode());

            view.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent i = new Intent();
                    i.putExtra("Location", model);
                    setResult(RESULT_OK, i);
                    finish();
                }
            });
        }

        @Override
        public View newView(Context context, int position, ViewGroup parent) {
            View v = super.newView(context, position, parent);
            Tag tag = new Tag();

            tag.name = (TextView) v.findViewById(R.id.item_name);
            tag.code = (TextView) v.findViewById(R.id.item_code);

            v.setTag(tag);
            return v;
        }

        class Tag {
            TextView name;
            TextView code;
        }

    }

    class FindLocationsTask extends McProgressAsyncTask {

        private String term;
        private GetLocationsResponse res;
        private Floor floor;
        private Building building;

        //..Modify By 	: Chua Kam Hoong
        //..Date 		: 20 May 2015
        //public FindLocationsTask(Context context, String term, Building building, Floor floor) {
        //public FindLocationsTask(Context context, int PageSize, int PageIndex, String term, Building building, Floor floor) {
        public FindLocationsTask(Context context, int PageIndex, String term, Building building, Floor floor) {
            super(context);
            //intPageSize = PageSize;
            intPageIndex = PageIndex;
            this.term = term;
            this.floor = floor;
            this.building = building;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            locationList.clear();
            adapter.notifyDataSetChanged();

        }

        @Override
        public void doInBackground() {
            if (isCConnect()) {
                res = CommonRequestService.getInstance().findLocations(getCurrentContext(), intPageIndex, term,
                        (this.building == null ? null : this.building.getBuildId()),
                        (this.floor == null ? null : this.floor.getFloorId()));
            } else {
                //..Modify By 	: Chua Kam Hoong
                //..Date 		: 20 May 2015
                //res = ECService.getInstance().findLocations(getCurrentContext(), term, (this.floor == null ? "" : this.floor.getName()));
                //res = ECService.getInstance().findLocations(getCurrentContext(), intPageSize, intPageIndex, term, (this.floor == null ? "" : this.floor.getName()));
                res = ECService.getInstance().findLocations(getCurrentContext(), intPageIndex, term, (this.floor == null ? "" : this.floor.getName()));
            }
        }

        @Override
        public void onPostExecute() {
            if (res == null || res.getLocationList() == null || res.getLocationList().size() == 0) {
                toast(R.string.no_location_found);
            } else {
                locationList.addAll(res.getLocationList());
            }
            adapter.notifyDataSetChanged();
        }

    }

    @Override
    public boolean isLoginActivity() {
        return true;
    }

    @Override
    public boolean isHomeActivity() {
        return false;
    }

}