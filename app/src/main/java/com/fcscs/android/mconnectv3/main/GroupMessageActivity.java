package com.fcscs.android.mconnectv3.main;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.ui.McProgressAsyncTask;
import com.fcscs.android.mconnectv3.common.ui.RoundCornerListAdapter;
import com.fcscs.android.mconnectv3.common.util.DialogHelper;
import com.fcscs.android.mconnectv3.common.util.McUtils;
import com.fcscs.android.mconnectv3.dao.entity.Runner;
import com.fcscs.android.mconnectv3.manager.model.RunnerModel;
import com.fcscs.android.mconnectv3.ws.cconnect.AdhocService;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.FindRunnerGroupResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.GetGroupRunnerListResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.GetTemplateResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.Group;
import com.fcscs.android.mconnectv3.ws.cconnect.model.schema.adhocservice.Template;
import com.fcscs.android.mconnectv3.ws.econnect.ECService;

public class GroupMessageActivity extends NewMessageBaseActivity {

    private static final int RC_RUNNER_GROUP = 1;
    private GetTemplateResponse curTemp;
    //	private List<GroupMessageIdModel> idModels;
//	private List<String> groupIds;
//	private HashSet<Long> addedRunners = new HashSet<Long>();
    private GroupRunnerListAdapter adapter;
    //	private List<RunnerModel> adapterRunners = new ArrayList<RunnerModel>();
    private GroupRunnerLoader task;
    private GroupLoader task1;
    private GroupMesageSender task2;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        homeTopBar.getTitleTv().setText(getString(R.string.group_message));

        adapter = new GroupRunnerListAdapter(this, R.layout.common_message_adapter_item);
        runnerListView.setAdapter(adapter);
        runnerListView.setEmptyView(findViewById(R.id.tv_empty));

        etRunner.setHint(R.string.input_group_name);
        etRunner.setOnKeyListener(new OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:
                            findGroups();
                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        });


        btnAdd.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                sendGroupMsg();
            }
        });


        btnCancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                confirmCancel();
            }
        });

//		setTextViewString(R.id.tv_createdby, "createdby", R.string.createdby);
//		setTextViewString(R.id.tv_msg_template, "msg_template", R.string.msg_template);
//		setTextViewString(R.id.tv_message, "message", R.string.message);
//		setTextViewString(R.id.tv_recipient, "recipient", R.string.recipient);
    }

    public boolean confirmCancel() {
        if (!"".equals(etMessage.getText().toString().trim())) {

            DialogHelper.showYesNoDialog(getCurrentContext(), R.string.confirm_cancel,
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            setResult(RESULT_OK);
                            finish();
                        }
                    });

        } else {
            setResult(RESULT_CANCELED);
            finish();
        }
        hideSoftInput(etMessage);
        return true;
    }

    protected void sendGroupMsg() {

        if (!validateMessage() || !validateRunner()) {
            return;
        }

        if (task2 == null || AsyncTask.Status.RUNNING.equals(task2.getStatus()) == false) {
            task2 = new GroupMesageSender(getCurrentContext());
            task2.exec();
        }

    }

    protected void findGroups() {
        String term = etRunner.getText().toString().trim();

        if (task1 == null || AsyncTask.Status.RUNNING.equals(task1.getStatus()) == false) {
            task1 = new GroupLoader(getCurrentContext(), term);
            task1.exec();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (RC_RUNNER_GROUP == requestCode && resultCode == RESULT_OK) {
            Group group = (Group) data.getExtras().get(SearchRunnerGroupActivity.KEY_RESULT);

            if (task == null || AsyncTask.Status.RUNNING.equals(task.getStatus()) == false) {
                task = new GroupRunnerLoader(getCurrentContext(), group.getGroupId());
                task.exec();
            }
        }
    }

    public void addRunner(Long groupId, long runnerId, String name) {

        RunnerModel item = new RunnerModel();
        item.setId(runnerId);
        item.setGroupId(groupId);
        item.setName(name);

        if (message.getRunners().contains(item)) {
            return;
        }

        message.getRunners().add(item);
        TextView tv = new TextView(GroupMessageActivity.this);
        tv.setText(name);
    }

    public boolean validateRunner() {
        if (message.getRunners() == null || message.getRunners().size() == 0) {
            toast(R.string.no_recipient);
            return false;
        }
        return true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return confirmCancel();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onTemplateSelected(AdapterView<?> parent, View view, int position, long id, Template temp) {
        if (temp != null) {
            if (isCConnect()) {

                curTemp = AdhocService.getInstance().getMsgTemplate(getCurrentContext(), temp.getTemplateId());
                if (curTemp != null) {
                    etMessage.setText(curTemp.getDetail());
                    for (GetTemplateResponse.Runner r : curTemp.getRunnerList()) {
                        addRunner(curTemp.getGroupId(), r.getRunnerId(), r.getUsername());
                    }
                    adapter.notifyDataSetChanged();
                }

            } else {
                etMessage.setText(temp.getDetail());
            }
        } else {
            if (curTemp != null) {
                etMessage.setText("");
            }
            curTemp = null;
        }
    }

    class GroupRunnerListAdapter extends RoundCornerListAdapter<RunnerModel> {

        public GroupRunnerListAdapter(Context context, int layout) {
            super(context, new ArrayList<RunnerModel>(), layout);
        }

        public void refresh() {
            mList = message.getRunners();
            notifyDataSetChanged();
        }

        @Override
        public void onBindView(View view, Context context, int position, final RunnerModel item) {

            TextView tv = (TextView) view.findViewById(R.id.individualRunner);
            ImageView iv = (ImageView) view.findViewById(R.id.individualRunner_delete);
            tv.setText(item.getName());
            iv.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    message.getRunners().remove(item);
                    refresh();
                }
            });
        }

        @Override
        public boolean areAllItemsEnabled() {
            return true;
        }

    }

    public class GroupLoader extends McProgressAsyncTask {

        public GroupLoader(Context currentContext, String term) {
            super(currentContext);
            this.term = term;
        }

        private String term;
        private FindRunnerGroupResponse response;

        @Override
        public void doInBackground() {
            if (isCConnect()) {
                response = AdhocService.getInstance().findRunnerGroups(getCurrentContext(), term);
            } else {
                response = ECService.getInstance().getAdHocGroupList(getCurrentContext(), term);
            }
        }

        @Override
        public void onPostExecute() {
            if (response != null) {
                final List<Group> groups = response.getGroupList();
                if (groups.size() == 0) {
                    toast(getString(R.string.no_groups_found));
                } else {
                    Intent i = new Intent(GroupMessageActivity.this, SearchRunnerGroupActivity.class);
                    i.putExtra(SearchRunnerGroupActivity.KEY_GROUP_LIST, response);
                    i.putExtra(SearchRunnerGroupActivity.KEY_TERM, term);
                    startActivityForResult(i, RC_RUNNER_GROUP);
                }
            }
        }

    }

    class GroupRunnerLoader extends McProgressAsyncTask {

        private Long groupId;
        private GetGroupRunnerListResponse response;

        public GroupRunnerLoader(Context context, Long groupId) {
            super(context);
            this.groupId = groupId;
        }

        @Override
        public void doInBackground() {
            if (isCConnect()) {
                response = AdhocService.getInstance().getGroupRunners(getCurrentContext(), groupId);
            } else {
                response = ECService.getInstance().getAdhocRunenrListForGroup(getCurrentContext(), groupId);
            }
        }

        @Override
        public void onPostExecute() {
            if (response != null) {
                if (response.getRunnerList().size() > 0) {
                    for (Runner r : response.getRunnerList()) {
                        addRunner(groupId, r.getRunnerId(), r.getFullname());
                    }
                    adapter.refresh();
                    McUtils.adjustListViewHeight(runnerListView, adapter);
                } else {
                    toast(R.string.no_runner_in_group);
                }
            }
            etRunner.setText("");
        }

    }


}
