package com.fcscs.android.mconnectv3.main;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

//import com.csipsimple.utils.Log;
import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.BaseActivity;
import com.fcscs.android.mconnectv3.common.McConstants;
import com.fcscs.android.mconnectv3.common.QRCodeParserFactory;
import com.fcscs.android.mconnectv3.common.SessionContext;
import com.fcscs.android.mconnectv3.common.ui.HomeTopBar;
import com.fcscs.android.mconnectv3.common.ui.McProgressAsyncTask;
import com.fcscs.android.mconnectv3.common.ui.RoomStatusAdapter;
import com.fcscs.android.mconnectv3.common.util.PrefsUtil;
import com.fcscs.android.mconnectv3.ws.cconnect.GuestRequestService;
import com.fcscs.android.mconnectv3.ws.cconnect.model.guestrequestservice.FindRoomGuestResponse;
import com.fcscs.android.mconnectv3.ws.cconnect.model.guestrequestservice.Guest;
import com.fcscs.android.mconnectv3.ws.econnect.ECService;
import com.fcscs.android.mconnectv3.zxing.CaptureActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RoomStatusActivity extends BaseActivity {

    static final String TAG = RoomStatusActivity.class.getSimpleName();

    private HomeTopBar homeTopBar;
    private EditText where;
    private EditText who;
    private Guest currentGuest;
    private TextView submit;
    private GridView grid;
    private RoomStatusAdapter adapter;
    protected RoomStatusSender task;
    private GuestLoader task1;
    ImageView qrButton;
    List<RoomStatus> mListRoomStatus;

    public static class RoomStatus {
        public String code;
        public String name;

        public RoomStatus(String code, String name) {
            this.code = code;
            this.name = name;
        }
    }

    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.room_status);

        homeTopBar = (HomeTopBar) findViewById(R.id.top_bar);
        homeTopBar.getTitleTv().setText(R.string.room_status);
        where = (EditText) findViewById(R.id.room_status_roomNum);
        // where.setHint(getString(R.string.location));
        where.setClickable(true);
        where.setHint(R.string.room_num);
        where.setInputType(InputType.TYPE_CLASS_NUMBER);
        where.setOnKeyListener(new OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                String term = where.getText().toString().trim();
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:
                            findGuestByRoomnum(term);
                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        });
        qrButton = (ImageView) findViewById(R.id.qr_camera);
        qrButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(RoomStatusActivity.this, CaptureActivity.class);
                i.putExtra("type", QRCodeParserFactory.ROOM_STATUS_REQUEST);
                startActivityForResult(i, QR_CODE);
            }
        });
        qrButton.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                boolean searchAll = where.getText().toString().length() == 0;
                findGuestByRoomnum(where.getText().toString());
                return true;
            }
        });

        boolean enable = PrefsUtil.getQRCodeRoomStatus(this);
        if (enable == false) {
            qrButton.setVisibility(View.GONE);
        }
        who = (EditText) findViewById(R.id.room_status_guestName);
        who.setFocusable(false);
        who.setHint(getString(R.string.guestname_isvip));
        who.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (currentGuest == null) {
                    toast(getString(R.string.please_select_guest));
                } else {
                    Intent i = new Intent(RoomStatusActivity.this, GuestDetails.class);
                    i.putExtra("guest", currentGuest);
                    startActivity(i);
                }
            }
        });
        who.setEnabled(false);
        String roomStatusMsg = PrefsUtil.getRoomStatusMsg(this);
        if (roomStatusMsg.length() > 0) {
            toast(roomStatusMsg);
        }
//		List<RoomStatus> statuses = new ArrayList<McEnums.RoomStatus>();

        try {
            mListRoomStatus = new ArrayList<RoomStatus>();
            JSONArray jsonArray = PrefsUtil.getRoomStatus(this);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                RoomStatus roomStatus = new RoomStatus(jsonObject.getString("Code"), jsonObject.getString("Name"));
                mListRoomStatus.add(roomStatus);
            }
        } catch (JSONException e) {
            Log.d(TAG, e.getMessage());
        }

//		for (McEnums.RoomStatus e : McEnums.RoomStatus.values()) {
//			statuses.add(e);
//		}
        grid = (GridView) findViewById(R.id.gv_data);
        adapter = new RoomStatusAdapter(getCurrentContext(), mListRoomStatus);
        grid.setAdapter(adapter);

        submit = (TextView) findViewById(R.id.room_status_submit);
        submit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (currentGuest == null) {
                    where.requestFocus();
                    toast(R.string.please_choose_a_valid_guest);
                    return;
                }

                RoomStatus st = adapter.getSelected();
                if (st == null) {
                    toast(R.string.please_select_a_room_status);
                    return;
                }

                String status = st.code;

                if (task == null || AsyncTask.Status.RUNNING.equals(task.getStatus()) == false) {
                    task = new RoomStatusSender(getCurrentContext(), currentGuest.getRoomNum(), status);
                    task.exec();
                }

            }
        });

    }

    final static int QR_CODE = 1001;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == QR_CODE && resultCode == RESULT_OK) {
            Bundle bundle = data.getExtras();
            String result = bundle.getString("result");
            result = result.replace("roomstatus|LOC", "");
            findGuestByRoomnum(result);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    class RoomStatusSender extends McProgressAsyncTask {

        private String roomNum;
        private String status;
        private Boolean result;

        public RoomStatusSender(Context context, String roomNum, String status) {
            super(context);
            this.roomNum = roomNum;
            this.status = status;
        }

        @Override
        public void doInBackground() {
            if (isEconnect()) {
                result = ECService.getInstance().updateRoomStatus(RoomStatusActivity.this, roomNum, status);

            }
        }

        @Override
        public void onPostExecute() {
            if (isEconnect()) {
                if (result == true) {
                    hideSoftInput(who);
                    toast(getString(R.string.update_successfully));
                    finish();
                } else {
                    toast(getString(R.string.update_fail));
                }
            }
        }

    }

    private void findGuestByRoomnum(String term) {

//		if(term == null || term.trim().length() < 1){
//			populateInfo(null);
//			return;
//		}

        if (task1 == null || AsyncTask.Status.RUNNING.equals(task1.getStatus()) == false) {
            task1 = new GuestLoader(getCurrentContext(), term);
            task1.exec();
        }

    }

    class GuestLoader extends McProgressAsyncTask {

        private String roomNo;
        private FindRoomGuestResponse res;

        public GuestLoader(Context context, String roomNo) {
            super(context);
            this.roomNo = roomNo;
            currentGuest = null;
            who.setText("");
        }

        @Override
        public void doInBackground() {
            if (isCConnect()) {
                res = GuestRequestService.getInstance().findRoomGuest(getCurrentContext(), roomNo);
            } else {
                //res = ECService.getInstance().getRoomList(getCurrentContext(), roomNo, true);
                if (McConstants.UNIQUE_GETROOMSLISTING) {
                    if (roomNo.matches("[0-9]+")) {
                        res = ECService.getInstance().getRoomList(getCurrentContext(), SessionContext.getInstance().getFcsUserId(), roomNo, "");
                    } else {
                        res = ECService.getInstance().getRoomList(getCurrentContext(), SessionContext.getInstance().getFcsUserId(), "", roomNo);
                    }
                } else {
                    res = ECService.getInstance().getRoomList(getCurrentContext(), SessionContext.getInstance().getFcsUserId(), roomNo, roomNo); //According to Phua, both RoomNo and GuestName should pass RoomNo
                }
            }

        }

        @Override
        public void onPostExecute() {
            if (res != null) {
                final List<Guest> guests = res.getGuests();
                if (guests.size() > 1) {
                    List<String> list = new ArrayList<String>();
                    for (Guest guest : guests) {

                        String name = guest.getGuestName().trim();
                        if (name != null && !name.equalsIgnoreCase("")) {
                            list.add("(" + guest.getRoomNum() + ")\n" + name);
                        } else {
                            list.add("(" + guest.getRoomNum() + ")");
                        }
                    }
                    String[] items = new String[list.size()];
                    items = (String[]) list.toArray(items);

                    AlertDialog.Builder builder = new AlertDialog.Builder(getCurrentContext());
                    builder.setTitle(getString(R.string.label_select_guest));
                    builder.setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (which > -1 && which < guests.size()) {
                                populateInfo(guests.get(which));
                                dialog.cancel();
                                hideSoftInput(where);
                            }
                        }
                    });
                    builder.show();
                } else if (guests.size() == 1) {
                    populateInfo(guests.get(0));
                    hideSoftInput(where);
                } else if (guests.size() == 0) {
                    populateInfo(null);
                    toast(R.string.please_input_correctnum);
                }
            }
        }

    }

    private void populateInfo(Guest guest) {
        if (guest != null) {
            currentGuest = guest;
            if (isCConnect()) {
                where.setText(guest.getExtNo());
            } else {
                where.setText(guest.getRoomNum());
            }
            who.setText(guest.getGuestName() + "(" + guest.getVipMsg() + ")");
        } else {

            who.setText("");
            where.requestFocus();
            currentGuest = null;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            setResult(RESULT_OK);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean isLoginActivity() {
        return true;
    }

    @Override
    public boolean isHomeActivity() {
        return false;
    }
}
