package com.fcscs.android.mconnectv3.common.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fcscs.android.mconnectv3.R;

public class BottomBar extends LinearLayout {

    private TextView leftTv, middleTv, rightTv;

    public BottomBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.bottom_bar, this, true);
        leftTv = (TextView) findViewById(R.id.btn_bottom_left);
        rightTv = (TextView) findViewById(R.id.btn_bottom_right);
        middleTv = (TextView) findViewById(R.id.btn_bottom_middle);
    }

    public TextView getLeftTv() {
        return leftTv;
    }

    public TextView getMiddleTv() {
        return middleTv;
    }

    public TextView getRightTv() {
        return rightTv;
    }

}
