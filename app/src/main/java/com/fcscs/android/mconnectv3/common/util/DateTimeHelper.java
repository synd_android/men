package com.fcscs.android.mconnectv3.common.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import android.util.Log;

import com.fcscs.android.mconnectv3.McApplication;
import com.fcscs.android.mconnectv3.common.McEnums;
import com.fcscs.android.mconnectv3.common.SessionContext;

public class DateTimeHelper {

    static final String TAG = DateTimeHelper.class.getSimpleName();

    private static Map<String, SimpleDateFormat> formatters = new HashMap<String, SimpleDateFormat>();

    public final static DateFormat FORMATTER_DATETIME_24H = new SimpleDateFormat(DateTimeHelper.DEFAULT_FORMATTER_DATETIME_24H, Locale.US);
    public final static DateFormat FORMATTER_DATE = new SimpleDateFormat("yyyy-MM-dd");

    public static final SimpleDateFormat DATE_FORMATTER4 = new SimpleDateFormat("yyyy-MM-dd");
    public static final SimpleDateFormat TIME_FORMATTER = new SimpleDateFormat("HH:mm");

    public static final String DEFAULT_FORMAT = "dd/MMM";
    public static final String DEFAULT_FORMATTER_DATETIME_24H = "yyyy-MM-dd HH:mm:ss";
    public static final String DEFAULT_TIME_ZONE = "UTC";

    public static final String DEFAULT_FORMATTER_DATETIME_24H_T = "yyyy-MM-dd'T'HH:mm:ss";

    public static final String FORMAT_DATETIME_FULL_UPDATE_REASSIGN = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String FORMAT_DATETIME_FULL = "dd/MM/yyyy HH:mm:ss";
    public static final String FORMAT_DATETIME_FULL_2 = "dd-MM-yyyy HH:mm:ss";
    public static final String FORMAT_DATETIME_NONSECOND = "dd/MM/yyyy HH:mm";
    public static final String FORMAT_DATETIME_AA = "dd/MM/yyyy HH:mm:ss aa";
    public static final String FORMAT_DATE_AA = "M/d/yyyy hh:mm:ss aa";

    public static final String FORMAT_DATEYEAR = "dd/MM/yyyy";
    public static final String FORMAT_DATEYEAR_NON = "yyyyMMdd";

    public static final String YYYY_MM_DD = "yyyy-MM-dd";
    public static final String FORMAT_DATETIME_NON = "yyyyMMddHHmmssSSS";
    public static final String FORMAT_DATETIME_NON_S = "yyyyMMddHHmmss";
    public static final String FORMAT_DATETIME_DASH = "yyyy-MM-dd HH:mm:ss.SSS";
    public static final String FORMAT_DATETIME_FULL_DASH = "dd-MMM-yyyy HH:mm:ss";
    public static final String FORMAT_DATE_DASH = "dd-MMM-yyy";


    static {
        getFormatter(DEFAULT_FORMATTER_DATETIME_24H);
    }

    private static SimpleDateFormat getFormatter(String pattern) {
        if (formatters == null) {
            formatters = new HashMap<String, SimpleDateFormat>();
        }
        SimpleDateFormat f = formatters.get(pattern);
        if (f == null) {
            f = new SimpleDateFormat(pattern, Locale.US);
            formatters.put(pattern, f);
        }
        return f;
    }

    public static Date parseDateStr(String dateStr, String pattern, Date defVal) {
        SimpleDateFormat f = getFormatter(pattern);
        try {
            return f.parse(dateStr);
        } catch (Exception e) {
            Log.w(TAG, "parseDateStr:" + dateStr + ", pattern:" + pattern);
        }
        return defVal;
    }

    public static String formatDateToStr(Date date, String pattern, String defVal) {
        if (date == null) {
            return defVal;
        }
        SimpleDateFormat f = getFormatter(pattern);
        try {
            return f.format(date);
        } catch (Exception e) {
            Log.w(TAG, "formatDateToStr:" + date + ", pattern=" + pattern);
        }
        return defVal;
    }

    public static Date getDateByTimeZone(Date date) {
        String userTimeZone = SessionContext.getInstance().getUserTimeZone();
        String timeZoneID = mapTimeZone.get(userTimeZone);
        if (timeZoneID == null) {
            return date;
        }
        TimeZone timeZone = TimeZone.getTimeZone(timeZoneID);

        SimpleDateFormat sdfDefault = new SimpleDateFormat(FORMAT_DATETIME_FULL);
        String defaultDate = sdfDefault.format(date);

        SimpleDateFormat sdfTimeZone = new SimpleDateFormat(FORMAT_DATETIME_FULL);
        if (PrefsUtil.getSettingServiceType(McApplication.application) == McEnums.ServiceType.CCONNECT)
            sdfTimeZone.setTimeZone(timeZone);
        try {
            String strdateByTimeZone = sdfTimeZone.format(date);
            Date dateByTimeZone = sdfDefault.parse(strdateByTimeZone);
            return dateByTimeZone;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String getStrDateByTimeZone(Date date) {

        String time = "";
        SimpleDateFormat sdfDefault = new SimpleDateFormat(FORMAT_DATETIME_FULL);
        String defaultDate = sdfDefault.format(date);
        String userTimeZone = SessionContext.getInstance().getUserTimeZone();
        String timeZoneID = mapTimeZone.get(userTimeZone);
        if (timeZoneID == null) {
            return defaultDate;
        }
        TimeZone timeZone = TimeZone.getTimeZone(timeZoneID);


        SimpleDateFormat sdfTimeZone = new SimpleDateFormat(FORMAT_DATETIME_FULL);
        if (PrefsUtil.getSettingServiceType(McApplication.application) == McEnums.ServiceType.CCONNECT)
            sdfTimeZone.setTimeZone(timeZone);
        try {
            String strdateByTimeZone = sdfTimeZone.format(date);
            Date dateByTimeZone = sdfTimeZone.parse(strdateByTimeZone);
            time = sdfTimeZone.format(dateByTimeZone);
            return time;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return defaultDate;
    }

    public static String getStrDateByTimeZone(Date date, String formatDate) {

        String time = "";
        SimpleDateFormat sdfDefault = new SimpleDateFormat(formatDate);
        String defaultDate = sdfDefault.format(date);
        String userTimeZone = SessionContext.getInstance().getUserTimeZone();
        String timeZoneID = mapTimeZone.get(userTimeZone);
        if (timeZoneID == null) {
            return defaultDate;
        }
        TimeZone timeZone = TimeZone.getTimeZone(timeZoneID);


        SimpleDateFormat sdfTimeZone = new SimpleDateFormat(formatDate);
        if (PrefsUtil.getSettingServiceType(McApplication.application) == McEnums.ServiceType.CCONNECT)
            sdfTimeZone.setTimeZone(timeZone);
        try {
            String strdateByTimeZone = sdfTimeZone.format(date);
            Date dateByTimeZone = sdfTimeZone.parse(strdateByTimeZone);
            time = sdfTimeZone.format(dateByTimeZone);
            return time;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return defaultDate;
    }

    public static String getStrDateFromDateByTimeZone(Date date, String formatDate) {

        String time = "";
        SimpleDateFormat sdfDefault = new SimpleDateFormat(formatDate);
        String defaultDate = sdfDefault.format(date);

        String userTimeZone = SessionContext.getInstance().getUserTimeZone();
        String timeZoneID = mapTimeZone.get(userTimeZone);
        TimeZone timeZone = TimeZone.getTimeZone(timeZoneID);

        SimpleDateFormat sdfTimeZone = new SimpleDateFormat(formatDate);
        if (PrefsUtil.getSettingServiceType(McApplication.application) == McEnums.ServiceType.CCONNECT)
            sdfTimeZone.setTimeZone(timeZone);
        String strdateByTimeZone = sdfTimeZone.format(date);
        return strdateByTimeZone;
    }


    static Map<String, String> mapTimeZone = new HashMap<String, String>();

    static {
        mapTimeZone.put("Samoa Standard Time", "Pacific/Apia");
        mapTimeZone.put("Hawaiian Standard Time", "America/Adak");
        mapTimeZone.put("Alaskan Standard Time", "Pacific/Gambier");
        mapTimeZone.put("Pacific Standard Time", "Pacific/Pitcairn");
        mapTimeZone.put("Mountain Standard Time", "America/Chihuahua");
        mapTimeZone.put("Central America Standard Time", " America/Chicago");
        mapTimeZone.put("Eastern Standard Time", " America/Atikokan");
        mapTimeZone.put("SA Pacific Standard Time", " America/Bogota");
        mapTimeZone.put("US Eastern Standard Time", " America/Indiana/Indianapolis");
        mapTimeZone.put("Eastern Standard Time", "America/Jamaica");
        mapTimeZone.put("Venezuela Standard Time", " America/Caracas");
        mapTimeZone.put("Pacific SA Standard Time", " America/Anguilla");
        mapTimeZone.put("Atlantic Standard Time", "America/Antigua");
        mapTimeZone.put("Paraguay Standard Time", " America/Asuncion");
        mapTimeZone.put("Atlantic Standard Time", "America/Blanc-Sablon");
        mapTimeZone.put("Central Brazilian Standard Time", "America/Cuiaba");
        mapTimeZone.put("SA Western Standard Time", "America/La_Paz");
        mapTimeZone.put("Newfoundland Standard Time", "America/St_Johns");
        mapTimeZone.put("E. South America Standard Time", " America/Araguaina");
        mapTimeZone.put("Argentina Standard Time", " America/Araguaina");
        mapTimeZone.put("Greenland Standard Time", "America/Godthab");
        mapTimeZone.put("Montevideo Standard Time", " America/Montevideo");
        mapTimeZone.put("SA Eastern Standard Time", " America/Paramaribo");
        mapTimeZone.put("Mid-Atlantic Standard Time", " America/Noronha");
        mapTimeZone.put("Azores Standard Time", "America/Scoresbysund");
        mapTimeZone.put("Cape Verde Standard Time", " Atlantic/Cape_Verde");
        mapTimeZone.put("GMT Standard Time", " GMT");
        mapTimeZone.put("Morocco Standard Time", " Africa/Casablanca");
        mapTimeZone.put("Greenwich Standard Time", " Atlantic/Reykjavik");
        mapTimeZone.put("UTC", "UTC");
        mapTimeZone.put("W. Central Africa Standard Time", " Africa/Algiers");
        mapTimeZone.put("Namibia Standard Time", "Africa/Windhoek");
        mapTimeZone.put("W. Europe Standard Time", "Arctic/Longyearbyen");
        mapTimeZone.put("Central Europe Standard Time", " Europe/Belgrade");
        mapTimeZone.put("Romance Standard Time", " Europe/Brussels");
        mapTimeZone.put("South Africa Standard Time", " Africa/Blantyre");
        mapTimeZone.put("Egypt Standard Time", " Africa/Cairo");
        mapTimeZone.put("Middle East Standard Time", " Africa/Tripoli");
        mapTimeZone.put("Jordan Standard Time", " Asia/Amman");
        mapTimeZone.put("Syria Standard Time", "Asia/Damascus");
        mapTimeZone.put("Israel Standard Time", "Asia/Jerusalem");
        mapTimeZone.put("GTB Standard Time", "Asia/Nicosia");
        mapTimeZone.put("E. Europe Standard Time", "Europe/Chisinau");
        mapTimeZone.put("FLE Standard Time", "Europe/Helsinki");
        mapTimeZone.put("Turkey Standard Time", " Europe/Istanbul");
        mapTimeZone.put("E. Africa Standard Time", "Africa/Addis_Ababa");
        mapTimeZone.put("Kaliningrad Standard Time", "Antarctica/Syowa");
        mapTimeZone.put("Arabic Standard Time", "Asia/Baghdad");
        mapTimeZone.put("Arab Standard Time", "Asia/Kuwait");
        mapTimeZone.put("Iran Standard Time", "Asia/Tehran");
        mapTimeZone.put("Caucasus Standard Time", " Asia/Baku");
        mapTimeZone.put("Arabian Standard Time", "Asia/Muscat");
        mapTimeZone.put("Georgian Standard Time", "Asia/Tbilisi");
        mapTimeZone.put("Russian Standard Time", "Europe/Moscow");
        mapTimeZone.put("Mauritius Standard Time", "Indian/Mahe");
        mapTimeZone.put("Afghanistan Standard Time", "Asia/Kabul");
        mapTimeZone.put("West Asia Standard Time", "Asia/Aqtau");
        mapTimeZone.put("Pakistan Standard Time", "Asia/Karachi");
        mapTimeZone.put("Sri Lanka Standard Time", "Asia/Colombo");
        mapTimeZone.put("India Standard Time", "Asia/Kolkata");
        mapTimeZone.put("Nepal Standard Time", "Asia/Kathmandu");
        mapTimeZone.put("Central Asia Standard Time", "Antarctica/Mawson");
        mapTimeZone.put("Bangladesh Standard Time", "Asia/Dhaka");
        mapTimeZone.put("Ekaterinburg Standard Time", "Asia/Yekaterinburg");
        mapTimeZone.put("Myanmar Standard Time", "Asia/Rangoon");
        mapTimeZone.put("SE Asia Standard Time", "Asia/Bangkok");
        mapTimeZone.put("N. Central Asia Standard Time", " Asia/Novokuznetsk");
        mapTimeZone.put("W. Australia Standard Time", "Antarctica/Casey");
        mapTimeZone.put("North Asia Standard Time", "Asia/Brunei");
        mapTimeZone.put("China Standard Time", "Asia/Choibalsan");
        mapTimeZone.put("Singapore Standard Time", "Asia/Singapore");
    }


}
