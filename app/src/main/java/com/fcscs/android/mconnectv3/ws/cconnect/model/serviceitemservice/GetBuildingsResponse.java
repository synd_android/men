package com.fcscs.android.mconnectv3.ws.cconnect.model.serviceitemservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.ksoap2.serialization.SoapObject;

import com.fcscs.android.mconnectv3.dao.entity.Building;

public class GetBuildingsResponse implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1972590495857288159L;
    private List<Building> buildingList;

    public GetBuildingsResponse() {
        buildingList = new ArrayList<Building>();
    }

    public GetBuildingsResponse(SoapObject soap) {
        buildingList = new ArrayList<Building>();
        if (soap.getPropertyCount() > 0) {
            for (int i = 0; i < soap.getPropertyCount(); i++) {
                buildingList.add(new Building((SoapObject) soap.getProperty(i)));
            }
        }
    }

    public List<Building> getBuildingList() {
        return buildingList;
    }

    public void setBuildingList(List<Building> buildingList) {
        this.buildingList = buildingList;
    }

}
