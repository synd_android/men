package com.fcscs.android.mconnectv3.ws.econnect.model.commonservice;

public class PullNotificationModel {
    private String createdDate;
    private String jobNo;
    private int notificationId;
    private int messageType;
    private String notificationContent;

    public PullNotificationModel() {
    }

    public PullNotificationModel(String jobNo, String notificationContent) {
        this.jobNo = jobNo;
        this.notificationContent = notificationContent;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getJobNo() {
        return jobNo;
    }

    public void setJobNo(String jobNo) {
        this.jobNo = jobNo;
    }

    public int getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(int notificationId) {
        this.notificationId = notificationId;
    }

    public int getMessageType() {
        return messageType;
    }

    public void setMessageType(int messageType) {
        this.messageType = messageType;
    }

    public String getNotificationContent() {
        return notificationContent;
    }

    public void setNotificationContent(String notificationContent) {
        this.notificationContent = notificationContent;
    }
}