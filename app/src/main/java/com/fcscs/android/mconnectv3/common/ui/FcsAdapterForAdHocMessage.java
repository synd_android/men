package com.fcscs.android.mconnectv3.common.ui;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.fcscs.android.mconnectv3.R;

public class FcsAdapterForAdHocMessage extends BaseAdapter {
    private List<String> list;
    private Context context;

    public FcsAdapterForAdHocMessage(Context context, List<String> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.simple_list_item_2, null);
        }
        TextView textView0 = (TextView) convertView.findViewById(R.id.simple_item_0);
        textView0.setText(list.get(position));
        ImageView imageview = (ImageView) convertView.findViewById(R.id.simple_item_1);
        switch (position) {
            case 0:
                imageview.setBackgroundResource(R.drawable.individual_message);
                break;
            case 1:
                imageview.setBackgroundResource(R.drawable.group_message);
                break;
        }

        ImageView imageview2 = (ImageView) convertView.findViewById(R.id.list_icon);
        imageview2.setBackgroundResource(R.drawable.list_icon);
        return convertView;
    }

}
