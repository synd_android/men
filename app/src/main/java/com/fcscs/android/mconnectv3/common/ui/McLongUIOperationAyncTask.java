package com.fcscs.android.mconnectv3.common.ui;

import android.os.AsyncTask;
import android.util.Log;

public class McLongUIOperationAyncTask extends AsyncTask<Void, Void, Void> {
    private static final String TAG = McLongUIOperationAyncTask.class.getSimpleName();

    public interface McUIUpdatable {
        void update();
    }

    private McUIUpdatable exe;

    public McLongUIOperationAyncTask(McUIUpdatable exe) {
        super();
        this.exe = exe;
    }

    @Override
    protected Void doInBackground(Void... params) {
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        if (exe != null) {
            long start = System.currentTimeMillis();
            Log.d(TAG, "start UI operation");
            try {
                exe.update();
            } catch (Throwable e) {
                Log.e(TAG, "Failed to execute", e);
            }
            long end = System.currentTimeMillis();
            Log.d(TAG, "end UI operation. time=" + (end - start));
        }
    }

    public void exec() {
        execute();
    }
}
