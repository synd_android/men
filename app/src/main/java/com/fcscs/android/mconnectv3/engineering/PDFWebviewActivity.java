package com.fcscs.android.mconnectv3.engineering;

import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.fcscs.android.mconnectv3.R;
import com.fcscs.android.mconnectv3.common.util.McUtils;

public class PDFWebviewActivity extends Activity {
    WebView webView;
    public static final String KEY_PATH_FILE = "KEY_PATH_FILE_PDF";
    String urlFile;
    String urlDoc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        webView = (WebView) findViewById(R.id.webview);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.setWebViewClient(new PdfWebViewClient());

        Bundle bundle = getIntent().getExtras();
        urlFile = bundle.getString(KEY_PATH_FILE);
        urlDoc = "'http://docs.google.com/gview?embedded=true&url=" + urlFile + "'";
        String doc = "<iframe src=" + urlDoc +
                "width='100%' height='100%'" +
                "style='border: none;'></iframe>";

        webView.loadData(doc, "text/html", "UTF-8");
    }

    private class PdfWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return true;
        }

    }
}
