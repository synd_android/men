package com.fcscs.android.mconnectv3.common.ui;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.fcscs.android.mconnectv3.McApplication;

public class HomeTopBar extends TopBar {

    private Context mContext;

    public HomeTopBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            this.mContext = context;
        }
    }

    @Override
    public void onClickRightBtn(View view) {
        if (mContext instanceof Activity) {
            hideSoftInput(view);
            ((Activity) mContext).finish();
            McApplication app = (McApplication) mContext.getApplicationContext();
            app.setRedirect(true);
        }
    }

    protected void hideSoftInput(View v) {
        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public void onClickLeftBtn(View view) {
    }


}
